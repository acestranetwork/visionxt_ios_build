(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["aluminisignup-aluminisignup-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/aluminisignup/aluminisignup.page.html":
/*!*********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/aluminisignup/aluminisignup.page.html ***!
  \*********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header translucent no-border>\r\n    <ion-toolbar>\r\n        <ion-buttons slot=\"start\">\r\n            <ion-back-button defaultHref=\"/\"></ion-back-button>\r\n        </ion-buttons>\r\n        <ion-title>Signup</ion-title>\r\n    </ion-toolbar>\r\n</ion-header>\r\n<ion-content style=\"overflow-y: hidden;\">\r\n    <div class=\"inner-div\">\r\n        <form [formGroup]=\"signupFg\" autocomplete=\"off\" #f=\"ngForm\" (ngSubmit)=\"signupFg.valid\">\r\n            <ion-grid>\r\n                <ion-row>\r\n                    <ion-col size=\"1\"></ion-col>\r\n                    <ion-col align-self-center>\r\n                        <ion-img src=\"../../assets/image/logo-removebg.png\" class=\"mx-auto mt-3 w-45\"></ion-img>\r\n                        <div class=\"details ion-text-center\">\r\n                            <ion-label>Fill below details for alumini</ion-label>\r\n                        </div>\r\n                    </ion-col>\r\n                    <ion-col size=\"1\"></ion-col>\r\n                </ion-row>\r\n                <ion-row class=\"mt-3\">\r\n                    <ion-col align-self-center>\r\n                        <ion-item header-menu (click)=\"uploadImage()\" lines=\"none\">\r\n                            <ion-label class=\"upd-img mt-0\">\r\n                                Upload Image\r\n                            </ion-label>\r\n                            <ion-thumbnail slot=\"end\">\r\n                                <img [src]=\"currentImage\" *ngIf=\"currentImage\">\r\n                                <img *ngIf=\"!currentImage\" src=\"../../assets/image/avatar_up.jpg\">\r\n                            </ion-thumbnail>\r\n                        </ion-item>\r\n                        <div class=\"error\" *ngIf=\"signupFg.controls['base64String'].errors?.required && f.submitted\">\r\n                            Upload Image is Required.</div>\r\n                    </ion-col>\r\n                </ion-row>\r\n                <ion-row>\r\n                    <ion-col align-self-center>\r\n                        <ion-item input-text-color>\r\n                            <ion-label color=\"medium\" position=\"floating\">Name\r\n                            </ion-label>\r\n                            <ion-input formControlName=\"stud_name\" type=\"text\" required></ion-input>\r\n                        </ion-item>\r\n                        <div class=\"error\"\r\n                            *ngIf=\"signupFg.controls['stud_name'].errors?.required && (signupFg.get('stud_name').touched || f.submitted)\">\r\n                            Name is Required.</div>\r\n                    </ion-col>\r\n                </ion-row>\r\n                <ion-row>\r\n                    <ion-col align-self-center>\r\n                        <ion-item input-text-color>\r\n                            <ion-label color=\"medium\" position=\"floating\">Select Your Degree</ion-label>\r\n                            <ion-select formControlName=\"name_of_deg\" value=\"\" interface=\"popover\" required>\r\n                                <ion-select-option *ngFor=\"let degree of degList\" value=\"{{degree.degree_name}}\">\r\n                                    {{degree.degree_name}}</ion-select-option>\r\n                            </ion-select>\r\n                        </ion-item>\r\n                        <div class=\"error\"\r\n                            *ngIf=\"signupFg.controls['name_of_deg'].errors?.required && (signupFg.get('name_of_deg').touched || f.submitted)\">\r\n                            Degree is Required.</div>\r\n                    </ion-col>\r\n                </ion-row>\r\n                <ion-row>\r\n                    <ion-col align-self-center>\r\n                        <ion-item input-text-color>\r\n                            <ion-label color=\"medium\" position=\"floating\">Year Of Completion</ion-label>\r\n                            <ion-datetime formControlName=\"year_of_comp\" displayFormat=\"YYYY\" min=\"1987\" max=\"2020\"\r\n                                required></ion-datetime>\r\n                        </ion-item>\r\n                        <div class=\"error\"\r\n                            *ngIf=\"signupFg.controls['year_of_comp'].errors?.required && (signupFg.get('year_of_comp').touched || f.submitted)\">\r\n                            Year Of Completion is Required.</div>\r\n                    </ion-col>\r\n                </ion-row>\r\n                <ion-row>\r\n                    <ion-col align-self-center>\r\n                        <ion-item input-text-color>\r\n                            <ion-label color=\"medium\" position=\"floating\">Select Your Campus</ion-label>\r\n                            <ion-select formControlName=\"campus\" value=\"\" interface=\"popover\" required>\r\n                                <ion-select-option *ngFor=\"let campus of campusList\" value=\"{{campus.centrename}}\">\r\n                                    {{campus.centrename}}</ion-select-option>\r\n                            </ion-select>\r\n                        </ion-item>\r\n                        <!-- <ion-item input-text-color>\r\n              <ion-label label color=\"medium\" position=\"floating\">Campus\r\n              </ion-label>\r\n              <ion-input formControlName=\"campus\" type=\"text\" required></ion-input>\r\n            </ion-item> -->\r\n                        <div class=\"error\"\r\n                            *ngIf=\"signupFg.controls['campus'].errors?.required && (signupFg.get('campus').touched || f.submitted)\">\r\n                            Campus is Required.</div>\r\n                    </ion-col>\r\n                </ion-row>\r\n                <ion-row>\r\n                    <ion-col align-self-center>\r\n                        <ion-item input-text-color>\r\n                            <ion-label color=\"medium\" position=\"floating\">Date Of Birth</ion-label>\r\n                            <ion-datetime formControlName=\"dob_ddmmyyyy\" value=\"\" required></ion-datetime>\r\n                        </ion-item>\r\n                        <div class=\"error\"\r\n                            *ngIf=\"signupFg.controls['dob_ddmmyyyy'].errors?.required && (signupFg.get('dob_ddmmyyyy').touched || f.submitted)\">\r\n                            Date Of Birth is Required.</div>\r\n                    </ion-col>\r\n                </ion-row>\r\n                <ion-label class=\"gend\" color=\"medium\" position=\"floating\">Gender</ion-label>\r\n                <ion-row>\r\n                    <ion-col align-self-center size=\"4\" *ngFor=\"let g of genderList\" (click)=\"selectGender(g)\">\r\n                        <div [ngClass]=\"g.isSelected ? 'selected-gender': 'not-selected-gender'\">{{g.name}} </div>\r\n                    </ion-col>\r\n                    <div class=\"error\" *ngIf=\"inValidGender\">\r\n                        Gender is Required.</div>\r\n                </ion-row>\r\n                <ion-row>\r\n                    <ion-col align-self-center>\r\n                        <ion-item input-text-color>\r\n                            <ion-label label color=\"medium\" position=\"floating\">Email\r\n                            </ion-label>\r\n                            <ion-input formControlName=\"emailid\" type=\"email\" (ionBlur)=\"validateEmailUnique()\"\r\n                                required></ion-input>\r\n                        </ion-item>\r\n                        <div class=\"error\"\r\n                            *ngIf=\"!emailidErrMessage && signupFg.controls['emailid'].errors?.required && (signupFg.get('emailid').touched || f.submitted)\">\r\n                            Email is Required.</div>\r\n                        <div class=\"error\" *ngIf=\"!emailidErrMessage && signupFg.controls['emailid'].errors?.pattern &&\r\n                              (signupFg.get('emailid').touched || f.submitted)\">\r\n                            Email is Invalid.</div>\r\n                        <div class=\"error\"\r\n                            *ngIf=\"emailidErrMessage && (signupFg.get('emailid').touched || f.submitted)\">\r\n                            {{emailidErrMessage}}</div>\r\n                    </ion-col>\r\n                </ion-row>\r\n                <ion-row>\r\n                    <ion-col align-self-center>\r\n                        <ion-item input-text-color>\r\n                            <ion-label label color=\"medium\" position=\"floating\">Mobile Number\r\n                            </ion-label>\r\n                            <ion-input formControlName=\"mobileno\" minlength=10 maxlength=10 type=\"tel\"\r\n                                (ionBlur)=\"validateMobileUnique()\" required></ion-input>\r\n                        </ion-item>\r\n                        <div class=\"error\"\r\n                            *ngIf=\"!mobilenoErrMessage && signupFg.controls['mobileno'].errors?.required && (signupFg.get('mobileno').touched || f.submitted)\">\r\n                            Mobile Number is Required.</div>\r\n                        <div class=\"error\" *ngIf=\"mobilenoErrMessage && (signupFg.get('mobileno').touched ||\r\n              f.submitted)\">\r\n                            {{mobilenoErrMessage}}</div>\r\n                    </ion-col>\r\n                </ion-row>\r\n                <ion-row>\r\n                    <ion-col align-self-center>\r\n                        <ion-item input-text-color>\r\n                            <ion-label label color=\"medium\" position=\"floating\">Area Of Interest\r\n                            </ion-label>\r\n                            <ion-input formControlName=\"interests\" type=\"text\" required></ion-input>\r\n                        </ion-item>\r\n                        <div class=\"error\"\r\n                            *ngIf=\"signupFg.controls['interests'].errors?.required && (signupFg.get('interests').touched || f.submitted)\">\r\n                            Area Of Interest is Required.</div>\r\n                    </ion-col>\r\n                </ion-row>\r\n                <ion-row>\r\n                    <ion-col align-self-center>\r\n                        <ion-item input-text-color>\r\n                            <ion-label label color=\"medium\" position=\"floating\">Hobby\r\n                            </ion-label>\r\n                            <ion-input formControlName=\"hobby\" type=\"text\" required></ion-input>\r\n                        </ion-item>\r\n                        <div class=\"error\"\r\n                            *ngIf=\"signupFg.controls['hobby'].errors?.required && (signupFg.get('hobby').touched || f.submitted)\">\r\n                            Hobby is Required.</div>\r\n                    </ion-col>\r\n                </ion-row>\r\n                <ion-row>\r\n                    <ion-col align-self-center>\r\n                        <ion-item input-text-color>\r\n                            <ion-label label color=\"medium\" position=\"floating\">Address\r\n                            </ion-label>\r\n                            <ion-input formControlName=\"address\" type=\"text\" required></ion-input>\r\n                        </ion-item>\r\n                        <div class=\"error\"\r\n                            *ngIf=\"signupFg.controls['address'].errors?.required && (signupFg.get('address').touched || f.submitted)\">\r\n                            Address is Required.</div>\r\n                    </ion-col>\r\n                </ion-row>\r\n                <ion-row>\r\n                    <ion-col align-self-center>\r\n                        <ion-item input-text-color>\r\n                            <ion-label label color=\"medium\" position=\"floating\">City\r\n                            </ion-label>\r\n                            <ion-input formControlName=\"city\" type=\"text\" required></ion-input>\r\n                        </ion-item>\r\n                        <div class=\"error\"\r\n                            *ngIf=\"signupFg.controls['city'].errors?.required && (signupFg.get('city').touched || f.submitted)\">\r\n                            City is Required.</div>\r\n                    </ion-col>\r\n                </ion-row>\r\n                <ion-row>\r\n                    <ion-col align-self-center>\r\n                        <ion-item input-text-color>\r\n                            <ion-label label color=\"medium\" position=\"floating\">Pin Code\r\n                            </ion-label>\r\n                            <ion-input formControlName=\"pincode\" type=\"number\" required></ion-input>\r\n                        </ion-item>\r\n                        <div class=\"error\"\r\n                            *ngIf=\"signupFg.controls['pincode'].errors?.required && (signupFg.get('pincode').touched || f.submitted)\">\r\n                            Pin Code is Required.</div>\r\n                    </ion-col>\r\n                </ion-row>\r\n                <ion-row>\r\n                    <ion-col align-self-center>\r\n                        <ion-item input-text-color>\r\n                            <ion-label label color=\"medium\" position=\"floating\">Designation\r\n                            </ion-label>\r\n                            <ion-input formControlName=\"designation\" type=\"text\" required></ion-input>\r\n                        </ion-item>\r\n                        <div class=\"error\"\r\n                            *ngIf=\"signupFg.controls['designation'].errors?.required && (signupFg.get('designation').touched || f.submitted)\">\r\n                            Designation is Required.</div>\r\n                    </ion-col>\r\n                </ion-row>\r\n                <ion-row>\r\n                    <ion-col align-self-center>\r\n                        <ion-item input-text-color>\r\n                            <ion-label label color=\"medium\" position=\"floating\">Company\r\n                            </ion-label>\r\n                            <ion-input formControlName=\"company\" type=\"text\" required></ion-input>\r\n                        </ion-item>\r\n                        <div class=\"error\"\r\n                            *ngIf=\"signupFg.controls['company'].errors?.required && (signupFg.get('company').touched || f.submitted)\">\r\n                            Company is Required.</div>\r\n                    </ion-col>\r\n                </ion-row>\r\n\r\n                <div class=\"separator\">Next section</div>\r\n                <ion-row>\r\n                    <ion-col align-self-center>\r\n                        <ion-item input-text-color>\r\n                            <ion-label label color=\"medium\" position=\"floating\">Username\r\n                            </ion-label>\r\n                            <ion-input formControlName=\"username\" type=\"text\" (ionBlur)=\"validateUsernameUnique()\"\r\n                                max=\"10\" required></ion-input>\r\n                        </ion-item>\r\n                        <div class=\"error\" *ngIf=\"!usernameErrMessage && signupFg.controls['username'].errors?.required && (signupFg.get('username').touched ||\r\n              f.submitted)\">\r\n                            Username is Required.</div>\r\n                        <div class=\"error\" *ngIf=\"usernameErrMessage && (signupFg.get('username').touched ||\r\n              f.submitted)\">\r\n                            {{usernameErrMessage}}</div>\r\n                    </ion-col>\r\n                </ion-row>\r\n                <ion-row>\r\n                    <ion-col align-self-center>\r\n                        <ion-item input-text-color>\r\n                            <ion-label label color=\"medium\" position=\"floating\">Password\r\n                            </ion-label>\r\n                            <ion-input [type]=\"passwordType\" formControlName=\"pwd\" required></ion-input>\r\n                            <ion-icon [name]=\"passwordIcon\" color=\"light\" size=\"small\" (click)='hideShowPassword()'\r\n                                class=\"ion-align-self-end\" slot=\"end\">\r\n                            </ion-icon>\r\n                        </ion-item>\r\n                        <div class=\"error\"\r\n                            *ngIf=\"signupFg.controls['pwd'].errors?.required && (signupFg.get('pwd').touched || f.submitted)\">\r\n                            Password is Required.</div>\r\n                        <div class=\"error\" *ngIf=\"signupFg.controls['pwd'].errors?.pattern &&\r\n                              (signupFg.get('pwd').touched || f.submitted)\">\r\n\r\n                            Password must contains one uppercase,one lowercase,one special character and one number.\r\n                        </div>\r\n                    </ion-col>\r\n                </ion-row>\r\n                <ion-row>\r\n                    <ion-col align-self-center>\r\n                        <ion-item input-text-color>\r\n                            <ion-label label color=\"medium\" position=\"floating\">Confirm Password\r\n                            </ion-label>\r\n                            <ion-input [type]=\"cpasswordType\" formControlName=\"confirmpassword\"\r\n                                (ionBlur)=\"MatchPassword()\" required></ion-input>\r\n                            <ion-icon [name]=\"cpasswordIcon\" color=\"light\" size=\"small\" (click)='hideShowcPassword()'\r\n                                class=\"ion-align-self-end\" slot=\"end\">\r\n                            </ion-icon>\r\n                        </ion-item>\r\n                        <div class=\"error\"\r\n                            *ngIf=\"!cpwdErrMessage && signupFg.controls['confirmpassword'].errors?.required && (signupFg.get('confirmpassword').touched || f.submitted)\">\r\n                            Confirm Password is Required.</div>\r\n                        <div class=\"error\" *ngIf=\"!cpwdErrMessage && signupFg.controls['confirmpassword'].errors?.pattern &&\r\n                              (signupFg.get('confirmpassword').touched || f.submitted)\">\r\n                            Password must contains one uppercase,one lowercase,one special character and one number\r\n                        </div>\r\n                        <div class=\"error\" *ngIf=\"cpwdErrMessage &&\r\n                              (signupFg.get('confirmpassword').touched || f.submitted)\">\r\n                            {{cpwdErrMessage}}</div>\r\n                    </ion-col>\r\n                </ion-row>\r\n\r\n\r\n\r\n                <!-- <ion-row>\r\n          <ion-col align-self-center>\r\n            <img [src]=\"currentImage\" *ngIf=\"currentImage\" style=\"height: 100px; width:100px;\">\r\n            <ion-button ion-fab (click)=\"uploadImage()\" required expand=\"block\" color=\"yellow\" size=\"medium\" btn-yellow\r\n              class=\"mt-0\">Upload Image\r\n              &emsp;<ion-icon name=\"camera\"></ion-icon>\r\n            </ion-button>\r\n          </ion-col>\r\n        </ion-row> -->\r\n                <ion-row>\r\n                    <ion-col align-self-center class=\"col-padd\">\r\n                        <ion-button type=\"submit\" expand=\"block\" (click)=\"signupFunction()\" color=\"btn-yellow\"\r\n                            size=\"large\" btn-yellow class=\"mt-15 btn-height-singup\">Send OTP\r\n                        </ion-button>\r\n                    </ion-col>\r\n                </ion-row>\r\n            </ion-grid>\r\n        </form>\r\n    </div>\r\n</ion-content>");

/***/ }),

/***/ "./src/app/aluminisignup/aluminisignup-routing.module.ts":
/*!***************************************************************!*\
  !*** ./src/app/aluminisignup/aluminisignup-routing.module.ts ***!
  \***************************************************************/
/*! exports provided: AluminisignupPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AluminisignupPageRoutingModule", function() { return AluminisignupPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _aluminisignup_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./aluminisignup.page */ "./src/app/aluminisignup/aluminisignup.page.ts");




const routes = [
    {
        path: '',
        component: _aluminisignup_page__WEBPACK_IMPORTED_MODULE_3__["AluminisignupPage"]
    }
];
let AluminisignupPageRoutingModule = class AluminisignupPageRoutingModule {
};
AluminisignupPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], AluminisignupPageRoutingModule);



/***/ }),

/***/ "./src/app/aluminisignup/aluminisignup.module.ts":
/*!*******************************************************!*\
  !*** ./src/app/aluminisignup/aluminisignup.module.ts ***!
  \*******************************************************/
/*! exports provided: AluminisignupPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AluminisignupPageModule", function() { return AluminisignupPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _aluminisignup_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./aluminisignup-routing.module */ "./src/app/aluminisignup/aluminisignup-routing.module.ts");
/* harmony import */ var _aluminisignup_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./aluminisignup.page */ "./src/app/aluminisignup/aluminisignup.page.ts");







let AluminisignupPageModule = class AluminisignupPageModule {
};
AluminisignupPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _aluminisignup_routing_module__WEBPACK_IMPORTED_MODULE_5__["AluminisignupPageRoutingModule"]
        ],
        declarations: [_aluminisignup_page__WEBPACK_IMPORTED_MODULE_6__["AluminisignupPage"]]
    })
], AluminisignupPageModule);



/***/ }),

/***/ "./src/app/aluminisignup/aluminisignup.page.scss":
/*!*******************************************************!*\
  !*** ./src/app/aluminisignup/aluminisignup.page.scss ***!
  \*******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-title {\n  color: #000;\n  font-family: \"Poppins\", sans-serif;\n}\n\nion-back-button {\n  color: #000;\n}\n\n.bton-group input {\n  display: none;\n}\n\nion-label {\n  font-family: \"Poppins\", sans-serif;\n}\n\n.details {\n  margin-top: 1rem;\n  font-weight: 600;\n  color: var(--ion-color-yellow);\n  font-size: 14px;\n}\n\n.gender {\n  color: #616161;\n  font-weight: 500;\n}\n\n.bton {\n  background: #1e1e1e;\n  border: 1px solid #616161;\n  padding: 6px 13px;\n  height: 40px;\n  cursor: pointer;\n  border-radius: 0;\n  color: #616161;\n  text-decoration: none;\n  font-size: 1rem;\n  text-align: center;\n  margin-top: 0.5rem;\n}\n\n.bton-group input:checked + label,\n.bton-group input:checked + label:active {\n  background-color: var(--ion-color-yellow);\n  color: #030303;\n}\n\n.selected-gender {\n  background: var(--ion-color-yellow);\n  border: solid 1px;\n  color: black;\n  padding: 8px 0px;\n  font-size: 12px;\n  text-transform: capitalize;\n  text-align: center;\n  cursor: pointer;\n}\n\n.not-selected-gender {\n  padding: 8px 0px;\n  border: solid 1px;\n  border-color: #c5c5c5;\n  color: #ffffff;\n  font-size: 12px;\n  text-transform: capitalize;\n  text-align: center;\n  cursor: pointer;\n}\n\n.error {\n  color: var(--ion-color-yellow);\n  font-size: 12px;\n}\n\n.gend {\n  margin-left: 0.3rem;\n}\n\nh5 {\n  font-weight: lighter !important;\n  font-size: 1em !important;\n  color: var(--ion-color-light);\n}\n\n.btn-height-singup {\n  height: 85%;\n  border-radius: 10px;\n}\n\n.col-padd {\n  padding: 15px 25px;\n}\n\n.item.sc-ion-label-md-h, .item .sc-ion-label-md-h {\n  white-space: inherit;\n}\n\n.item.sc-ion-label-ios-h, .item .sc-ion-label-ios-h {\n  white-space: inherit;\n}\n\n.sc-ion-popover-md-h {\n  --width: 90%;\n}\n\n.sc-ion-popover-ios-h {\n  --width: 90%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvYWx1bWluaXNpZ251cC9hbHVtaW5pc2lnbnVwLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLFdBQUE7RUFDQSxrQ0FBQTtBQUNKOztBQUVBO0VBQ0ksV0FBQTtBQUNKOztBQUVBO0VBQ0ksYUFBQTtBQUNKOztBQVFBO0VBQ0ksa0NBQUE7QUFMSjs7QUFPQTtFQUNJLGdCQUFBO0VBQ0EsZ0JBQUE7RUFDQSw4QkFBQTtFQUNBLGVBQUE7QUFKSjs7QUFPQTtFQUNJLGNBQUE7RUFDQSxnQkFBQTtBQUpKOztBQU9BO0VBQ0ksbUJBQUE7RUFDQSx5QkFBQTtFQUNBLGlCQUFBO0VBQ0EsWUFBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtFQUNBLGNBQUE7RUFDQSxxQkFBQTtFQUNBLGVBQUE7RUFDQSxrQkFBQTtFQUNBLGtCQUFBO0FBSko7O0FBT0E7O0VBRUUseUNBQUE7RUFDQSxjQUFBO0FBSkY7O0FBT0E7RUFDUSxtQ0FBQTtFQUNBLGlCQUFBO0VBRUEsWUFBQTtFQUNBLGdCQUFBO0VBQ0EsZUFBQTtFQUNBLDBCQUFBO0VBQ0Esa0JBQUE7RUFDQSxlQUFBO0FBTFI7O0FBT0E7RUFDUSxnQkFBQTtFQUNBLGlCQUFBO0VBQ0EscUJBQUE7RUFDQSxjQUFBO0VBQ0EsZUFBQTtFQUNBLDBCQUFBO0VBQ0Esa0JBQUE7RUFDQSxlQUFBO0FBSlI7O0FBTUE7RUFDUSw4QkFBQTtFQUNBLGVBQUE7QUFIUjs7QUFLQTtFQUNRLG1CQUFBO0FBRlI7O0FBS0E7RUFDUSwrQkFBQTtFQUNBLHlCQUFBO0VBQ0EsNkJBQUE7QUFGUjs7QUFNQTtFQUNNLFdBQUE7RUFDQSxtQkFBQTtBQUhOOztBQUtBO0VBQ00sa0JBQUE7QUFGTjs7QUFLQTtFQUNJLG9CQUFBO0FBRko7O0FBSUE7RUFDSSxvQkFBQTtBQURKOztBQUdBO0VBQ0ksWUFBQTtBQUFKOztBQUVBO0VBQ0ksWUFBQTtBQUNKIiwiZmlsZSI6InNyYy9hcHAvYWx1bWluaXNpZ251cC9hbHVtaW5pc2lnbnVwLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImlvbi10aXRsZSB7XHJcbiAgICBjb2xvcjogIzAwMDtcclxuICAgIGZvbnQtZmFtaWx5OidQb3BwaW5zJywgc2Fucy1zZXJpZjtcclxufVxyXG5cclxuaW9uLWJhY2stYnV0dG9uIHtcclxuICAgIGNvbG9yOiAjMDAwO1xyXG59XHJcblxyXG4uYnRvbi1ncm91cCBpbnB1dCB7XHJcbiAgICBkaXNwbGF5OiBub25lO1xyXG59XHJcbiAgXHJcbiBcclxuLy8gaW9uLXNlbGVjdCB7XHJcbi8vICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbi8vICAgICBib3R0b206IDFweDtcclxuLy8gICAgIG1hcmdpbi1ib3R0b206IDFyZW07XHJcbi8vIH1cclxuaW9uLWxhYmVse1xyXG4gICAgZm9udC1mYW1pbHk6J1BvcHBpbnMnLCBzYW5zLXNlcmlmO1xyXG59XHJcbi5kZXRhaWxzIHtcclxuICAgIG1hcmdpbi10b3A6IDFyZW07XHJcbiAgICBmb250LXdlaWdodDogNjAwO1xyXG4gICAgY29sb3I6IHZhcigtLWlvbi1jb2xvci15ZWxsb3cpOy8vI0Y2Qzc2MjtcclxuICAgIGZvbnQtc2l6ZToxNHB4O1xyXG59XHJcblxyXG4uZ2VuZGVyIHtcclxuICAgIGNvbG9yOiAjNjE2MTYxO1xyXG4gICAgZm9udC13ZWlnaHQ6IDUwMDtcclxufVxyXG5cclxuLmJ0b257XHJcbiAgICBiYWNrZ3JvdW5kOiAjMWUxZTFlO1xyXG4gICAgYm9yZGVyOiAxcHggc29saWQgIzYxNjE2MTtcclxuICAgIHBhZGRpbmc6IDZweCAxM3B4O1xyXG4gICAgaGVpZ2h0OiA0MHB4O1xyXG4gICAgY3Vyc29yOiBwb2ludGVyO1xyXG4gICAgYm9yZGVyLXJhZGl1czogMDtcclxuICAgIGNvbG9yOiAjNjE2MTYxO1xyXG4gICAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xyXG4gICAgZm9udC1zaXplOiAxcmVtO1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgbWFyZ2luLXRvcDogMC41cmVtO1xyXG59XHJcblxyXG4uYnRvbi1ncm91cCBpbnB1dDpjaGVja2VkICsgbGFiZWwsXHJcbi5idG9uLWdyb3VwIGlucHV0OmNoZWNrZWQgKyBsYWJlbDphY3RpdmUge1xyXG4gIGJhY2tncm91bmQtY29sb3I6IHZhcigtLWlvbi1jb2xvci15ZWxsb3cpOy8vI0Y2Qzc2MjtcclxuICBjb2xvcjogIzAzMDMwMztcclxufVxyXG5cclxuLnNlbGVjdGVkLWdlbmRlciB7XHJcbiAgICAgICAgYmFja2dyb3VuZDogdmFyKC0taW9uLWNvbG9yLXllbGxvdyk7Ly8jRjZDNzYyO1xyXG4gICAgICAgIGJvcmRlcjogc29saWQgMXB4O1xyXG4gICAgICAgLy9ib3JkZXItY29sb3I6IHZhcigtLWlvbi1jb2xvci1wcmltYXJ5KTtcclxuICAgICAgICBjb2xvcjogcmdiKDAsIDAsIDApO1xyXG4gICAgICAgIHBhZGRpbmc6IDhweCAwcHg7XHJcbiAgICAgICAgZm9udC1zaXplOiAxMnB4O1xyXG4gICAgICAgIHRleHQtdHJhbnNmb3JtOiBjYXBpdGFsaXplO1xyXG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICAgICBjdXJzb3I6IHBvaW50ZXI7XHJcbn1cclxuLm5vdC1zZWxlY3RlZC1nZW5kZXIge1xyXG4gICAgICAgIHBhZGRpbmc6IDhweCAwcHg7XHJcbiAgICAgICAgYm9yZGVyOiBzb2xpZCAxcHg7XHJcbiAgICAgICAgYm9yZGVyLWNvbG9yOiAjYzVjNWM1O1xyXG4gICAgICAgIGNvbG9yOiAjZmZmZmZmO1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMTJweDtcclxuICAgICAgICB0ZXh0LXRyYW5zZm9ybTogY2FwaXRhbGl6ZTtcclxuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgICAgY3Vyc29yOiBwb2ludGVyO1xyXG59XHJcbi5lcnJvcntcclxuICAgICAgICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLXllbGxvdyk7Ly8jZDM5ZTAwO1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMTJweDtcclxufVxyXG4uZ2VuZHtcclxuICAgICAgICBtYXJnaW4tbGVmdDogMC4zcmVtO1xyXG59XHJcblxyXG5oNSB7XHJcbiAgICAgICAgZm9udC13ZWlnaHQ6IGxpZ2h0ZXIgIWltcG9ydGFudDtcclxuICAgICAgICBmb250LXNpemU6IDFlbSAhaW1wb3J0YW50O1xyXG4gICAgICAgIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItbGlnaHQpO1xyXG59XHJcblxyXG5cclxuLmJ0bi1oZWlnaHQtc2luZ3VwIHtcclxuICAgICAgaGVpZ2h0OiA4NSU7XHJcbiAgICAgIGJvcmRlci1yYWRpdXM6IDEwcHg7XHJcbn1cclxuLmNvbC1wYWRkIHtcclxuICAgICAgcGFkZGluZzogMTVweCAyNXB4O1xyXG59XHJcblxyXG4uaXRlbS5zYy1pb24tbGFiZWwtbWQtaCwgLml0ZW0gLnNjLWlvbi1sYWJlbC1tZC1oe1xyXG4gICAgd2hpdGUtc3BhY2U6IGluaGVyaXQ7XHJcbn1cclxuLml0ZW0uc2MtaW9uLWxhYmVsLWlvcy1oLCAuaXRlbSAuc2MtaW9uLWxhYmVsLWlvcy1oe1xyXG4gICAgd2hpdGUtc3BhY2U6IGluaGVyaXQ7IFxyXG59XHJcbi5zYy1pb24tcG9wb3Zlci1tZC1oe1xyXG4gICAgLS13aWR0aDogOTAlO1xyXG59XHJcbi5zYy1pb24tcG9wb3Zlci1pb3MtaHtcclxuICAgIC0td2lkdGg6IDkwJTtcclxufSJdfQ== */");

/***/ }),

/***/ "./src/app/aluminisignup/aluminisignup.page.ts":
/*!*****************************************************!*\
  !*** ./src/app/aluminisignup/aluminisignup.page.ts ***!
  \*****************************************************/
/*! exports provided: AluminisignupPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AluminisignupPage", function() { return AluminisignupPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic-native/camera/ngx */ "./node_modules/@ionic-native/camera/__ivy_ngcc__/ngx/index.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_native_ionic_webview_ngx__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic-native/ionic-webview/ngx */ "./node_modules/@ionic-native/ionic-webview/__ivy_ngcc__/ngx/index.js");
/* harmony import */ var _global_services_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../global-services.service */ "./src/app/global-services.service.ts");
/* harmony import */ var _ionic_native_base64_ngx__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic-native/base64/ngx */ "./node_modules/@ionic-native/base64/__ivy_ngcc__/ngx/index.js");








let AluminisignupPage = class AluminisignupPage {
    constructor(navCtrl, camera, actionSheetCtrl, fb, webview, service, alertController, base64) {
        this.navCtrl = navCtrl;
        this.camera = camera;
        this.actionSheetCtrl = actionSheetCtrl;
        this.fb = fb;
        this.webview = webview;
        this.service = service;
        this.alertController = alertController;
        this.base64 = base64;
        this.inValidGender = false;
        // =====================GENDER VALIDATION=====================
        this.genderList = [
            {
                name: 'male',
                isSelected: false
            },
            {
                name: 'female',
                isSelected: false
            },
            {
                name: 'Others',
                isSelected: false
            }
        ];
        // =====================End of UPLOAD PROFILE IMAGE=====================
        // =====================Get Degree list=====================
        this.getDegreeList = () => {
            let obj = {};
            this.service.post_data("getDegreeList", obj)
                .subscribe((result) => {
                let json = this.service.parserToJSON(result);
                this.degList = JSON.parse(json.string);
            }, (error) => {
                console.log(error);
            });
        };
        this.getCampusList = () => {
            let obj = {};
            this.service.post_data("getCampusList", obj)
                .subscribe((result) => {
                let json = this.service.parserToJSON(result);
                this.campusList = JSON.parse(json.string);
            }, (error) => {
                console.log(error);
            });
        };
        this.validateEmailUnique = () => {
            const regex = new RegExp("^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$");
            const valid = regex.test(this.signupFg.controls['emailid'].value);
            if (valid) {
                let obj = {};
                obj.emailid = this.signupFg.controls['emailid'].value;
                this.service.post_data("getEmailExist", obj)
                    .subscribe((result) => {
                    let json = this.service.parserToJSON(result);
                    var parsedJSON = JSON.parse(json.string);
                    if (parsedJSON[0].Status != "NOTEXIST") {
                        this.emailidErrMessage = parsedJSON[0].Message;
                        this.signupFg.controls['emailid'].setValue(undefined);
                    }
                    else {
                        this.emailidErrMessage = undefined;
                    }
                }, (error) => {
                    console.log(error);
                });
            }
        };
        this.validateMobileUnique = () => {
            if (this.signupFg.controls['mobileno'].value != null || this.signupFg.controls['mobileno'].value != undefined) {
                var monbilnoLength = (this.signupFg.controls['mobileno'].value).toString().length;
                if (monbilnoLength > 1 && monbilnoLength == 10) {
                    this.mobilenoErrMessage = "";
                    let obj = {};
                    obj.mobno = this.signupFg.controls['mobileno'].value;
                    this.service.post_data("getMobileNoExist", obj)
                        .subscribe((result) => {
                        let json = this.service.parserToJSON(result);
                        var parsedJSON = JSON.parse(json.string);
                        if (parsedJSON[0].Status != "NOTEXIST") {
                            this.mobilenoErrMessage = parsedJSON[0].Message;
                            this.signupFg.controls['mobileno'].setValue(undefined);
                        }
                        else {
                            this.mobilenoErrMessage = undefined;
                        }
                    }, (error) => {
                        console.log(error);
                    });
                }
                else {
                    this.mobilenoErrMessage = "Mobile Number is Invalid.";
                }
            }
            else {
                this.mobilenoErrMessage = undefined;
            }
        };
        this.validateUsernameUnique = () => {
            let obj = {};
            obj.username = this.signupFg.controls['username'].value;
            this.service.post_data("getUsrNameExist", obj)
                .subscribe((result) => {
                let json = this.service.parserToJSON(result);
                var parsedJSON = [];
                parsedJSON = JSON.parse(json.string);
                if (parsedJSON[0].Status != "NOTEXIST") {
                    this.usernameErrMessage = parsedJSON[0].Message;
                    this.signupFg.controls['username'].setValue(undefined);
                }
                else {
                    this.usernameErrMessage = undefined;
                }
            }, (error) => {
                console.log(error);
            });
        };
        this.MatchPassword = () => {
            var passwordControl = this.signupFg.controls['pwd'].value;
            var confirmPasswordControl = this.signupFg.controls['confirmpassword'].value;
            if (passwordControl !== confirmPasswordControl) {
                this.cpwdErrMessage = "Password and Confirm Password fields should match.";
            }
            else {
                this.cpwdErrMessage = undefined;
            }
        };
        // =====================End of Confirm Password Validation=====================
        // =====================HIDE PASSWORD=====================
        this.passwordType = 'password';
        this.passwordIcon = 'eye-off';
        this.cpasswordType = 'password';
        this.cpasswordIcon = 'eye-off';
        // =====================end of HIDE PASSWORD=====================
        // =====================Send OTP=====================
        this.sendOTP = (mobileno) => {
            let obj = {};
            obj.mobileno = mobileno;
            this.service.post_data_otp("sendOtpViaSMS", obj)
                .subscribe((result) => {
                let json = this.service.parserToJSON(result);
                var parsedJSON = JSON.parse(json.string);
                this.navCtrl.navigateRoot(['/otp', { 'otp': parsedJSON[0]['OTP'], 'mobileno': mobileno }]);
            }, (error) => {
                console.log(error);
            });
        };
    }
    ngOnInit() {
        if (localStorage.getItem('deviceId')) {
            this.deviceId = localStorage.getItem('deviceId');
        }
        if (localStorage.getItem('platform')) {
            this.devicePlatform = localStorage.getItem('platform');
        }
        this.formInit();
        this.getCampusList();
        this.getDegreeList();
    }
    // =====================FORM INIT=====================
    formInit() {
        this.signupFg = this.fb.group({
            stud_name: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required],
            name_of_deg: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required],
            year_of_comp: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required],
            campus: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required],
            dob_ddmmyyyy: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required],
            gender: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required],
            emailid: ['', { validators: [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$")], updateOn: 'blur' }],
            mobileno: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required],
            interests: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required],
            hobby: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required],
            address: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required],
            city: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required],
            pincode: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required],
            designation: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required],
            company: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required],
            file_ext: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required],
            contentType: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required],
            base64String: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required],
            linkedin_id: [''],
            fb_id: [''],
            instagram_id: [''],
            device_id: [this.deviceId],
            device: [this.devicePlatform],
            trend_spotter_type: 'NIFT-ALUMNI',
            username: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required],
            pwd: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].pattern("^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9]).{6,15}$")]],
            confirmpassword: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].pattern("^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9]).{6,15}$")]],
        });
    }
    selectGender(gender) {
        this.genderList.forEach(g => {
            if (g.name === gender.name) {
                g.isSelected = !g.isSelected;
                this.inValidGender = false;
            }
            else {
                g.isSelected = false;
            }
        });
    }
    // =====================SIGN IN=====================
    signupFunction() {
        const data = this.signupFg.value;
        data[`gender`] = this.genderList.filter(gender => gender.isSelected).map(g => g.name)[0];
        if (data.gender != null) {
            this.inValidGender = false;
            this.signupFg.controls['gender'].setValue(data.gender);
        }
        else {
            this.inValidGender = true;
        }
        if (this.signupFg.valid) {
            let obj = {};
            obj.mobileno = this.signupFg.controls['mobileno'].value;
            this.service.post_data_otp("sendOtpViaSMS", obj)
                .subscribe((result) => {
                let json = this.service.parserToJSON(result);
                var parsedJSON = JSON.parse(json.string);
                this.navCtrl.navigateRoot(['/otp', { 'otp': parsedJSON[0]['OTP'], 'mobileno': obj.mobileno, 'data': JSON.stringify(data), 'page': 'signup' }]);
            }, (error) => {
                console.log(error);
            });
        }
        else if (!this.signupFg.get('base64String').valid || !this.signupFg.get('file_ext').valid || !this.signupFg.get('contentType').valid) {
            this.alertMessage('Upload Image to Proceed Further');
        }
        else {
            this.validateAllFormFields(this.signupFg);
        }
    }
    validateAllFormFields(formGroup) {
        Object.keys(formGroup.controls).forEach(field => {
            const control = formGroup.get(field);
            if (control instanceof _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"]) {
                if (!control.valid) {
                    control.markAsTouched({ onlySelf: true });
                }
            }
            else if (control instanceof _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormGroup"]) {
                this.validateAllFormFields(control);
            }
        });
    }
    // =====================UPLOAD PROFILE IMAGE=====================
    uploadImage() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            let actionSheet = yield this.actionSheetCtrl.create({
                buttons: [
                    {
                        text: 'Take photo',
                        role: 'destructive',
                        icon: 'camera',
                        handler: () => {
                            this.captureImage('camera');
                        }
                    },
                    {
                        text: 'Choose photo from Gallery',
                        icon: 'images',
                        handler: () => {
                            this.captureImage('gallery');
                        }
                    },
                ]
            });
            actionSheet.present();
        });
    }
    //CAPTURE IMAGE FUNCTION
    captureImage(data) {
        const options = {
            quality: 50,
            destinationType: this.camera.DestinationType.DATA_URL,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE,
            correctOrientation: true
        };
        if (data == 'camera') {
            options.sourceType = this.camera.PictureSourceType.CAMERA;
            this.camera.getPicture(options).then((imageData) => {
                this.currentImage = 'data:image/jpeg;base64,' + imageData;
                this.uploadFile(imageData);
            }, (err) => {
                this.imagesService.create(err, 'top', 'error');
                // Handle error
            });
        }
        else {
            if (data == 'gallery') {
                options.sourceType = this.camera.PictureSourceType.PHOTOLIBRARY;
                this.camera.getPicture(options).then((imageData) => {
                    this.currentImage = 'data:image/jpeg;base64,' + imageData;
                    this.uploadFile(imageData);
                }, (err) => {
                    // Handle error
                    this.imagesService.create(err, 'top', 'error');
                });
            }
        }
    }
    //SUBMIT IMAGE DATA TO SERVER
    uploadFile(imageData) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.signupFg.controls['base64String'].setValue(imageData);
            this.signupFg.controls['contentType'].setValue("image/jpeg");
            this.signupFg.controls['file_ext'].setValue("jpeg");
        });
    }
    hideShowPassword() {
        this.passwordType = this.passwordType === 'text' ? 'password' : 'text';
        this.passwordIcon = this.passwordIcon === 'eye-off' ? 'eye' : 'eye-off';
    }
    hideShowcPassword() {
        this.cpasswordType = this.cpasswordType === 'text' ? 'password' : 'text';
        this.cpasswordIcon = this.cpasswordIcon === 'eye-off' ? 'eye' : 'eye-off';
    }
    alertMessage(message) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const alert = yield this.alertController.create({
                cssClass: 'alert-custom-class',
                message: message,
                buttons: [{
                        text: 'OK',
                        cssClass: 'warning',
                    },]
            });
            yield alert.present();
        });
    }
};
AluminisignupPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavController"] },
    { type: _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_2__["Camera"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ActionSheetController"] },
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormBuilder"] },
    { type: _ionic_native_ionic_webview_ngx__WEBPACK_IMPORTED_MODULE_5__["WebView"] },
    { type: _global_services_service__WEBPACK_IMPORTED_MODULE_6__["GlobalServicesService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["AlertController"] },
    { type: _ionic_native_base64_ngx__WEBPACK_IMPORTED_MODULE_7__["Base64"] }
];
AluminisignupPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-aluminisignup',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./aluminisignup.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/aluminisignup/aluminisignup.page.html")).default,
        encapsulation: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewEncapsulation"].None,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./aluminisignup.page.scss */ "./src/app/aluminisignup/aluminisignup.page.scss")).default]
    })
], AluminisignupPage);



/***/ })

}]);
//# sourceMappingURL=aluminisignup-aluminisignup-module-es2015.js.map