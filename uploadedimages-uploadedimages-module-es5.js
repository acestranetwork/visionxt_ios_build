(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["uploadedimages-uploadedimages-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/uploadedimages/uploadedimages.page.html":
    /*!***********************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/uploadedimages/uploadedimages.page.html ***!
      \***********************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppUploadedimagesUploadedimagesPageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<app-header>\r\n\r\n</app-header>\r\n<ion-content>\r\n    <div class=\"inner-div\">\r\n        <ion-grid>\r\n            <ion-row class=\"ion-text-center mt-3 mb-2\">\r\n                <ion-col>\r\n                    <h5 header-text>Uploaded Images</h5>\r\n                </ion-col>\r\n            </ion-row>\r\n            <ion-row search-box *ngIf=\"isEnableSearch ==  true\">\r\n                <ion-col>\r\n                    <div class=\"text-center\">\r\n                        <ion-icon color=\"light\" size=\"large\" (click)=\"enableSearch(false)\" name=\"close-circle-outline\">\r\n                        </ion-icon>\r\n                    </div>\r\n                    <ion-searchbar color=\"light\" showCancelButton=\"focus\" [(ngModel)]='search'\r\n                        cancelButtonText=\"Custom Cancel\" type=\"text\" debounce=\"500\" (ionChange)=\"searchList()\">\r\n                    </ion-searchbar>\r\n                    <ion-list *ngIf=\"isItemAvailable\">\r\n                        <ion-item (click)=\"ItemAvailableClose(item)\" *ngFor=\"let item of items\">{{ item }}</ion-item>\r\n                    </ion-list>\r\n                </ion-col>\r\n            </ion-row>\r\n            <ion-row *ngIf=\"uploadImageList.length == 0\">\r\n                <ion-col>\r\n                    <ion-card class=\"ion-margin-vertical\">\r\n                        <ion-card-content>\r\n                            No Record Found\r\n                        </ion-card-content>\r\n                    </ion-card>\r\n                </ion-col>\r\n            </ion-row>\r\n            <ion-row *ngIf=\"uploadImageList.length > 0\">\r\n                <ion-col *ngFor=\"let image of uploadImageList\" size=\"6\">\r\n                    <img [src]=\"image.upload_image\" class=\"img_size\" (click)='navigateMediaInfo(image)'>\r\n                </ion-col>\r\n            </ion-row>\r\n            <!-- <ion-infinite-scroll threshold=\"100px\" (ionInfinite)=\"loadData()\">\r\n                <ion-infinite-scroll-content class=\"loadingspinner\" loadingSpinner=\"bubbles\" loadingText=\"Loading more data...\">\r\n                </ion-infinite-scroll-content>\r\n            </ion-infinite-scroll> -->\r\n        </ion-grid>\r\n        <ion-grid *ngIf=\"loadMoreBtn\">\r\n            <ion-row class=\"ion-justify-content-center\">\r\n                <ion-col>\r\n                    <div class=\"ion-text-center\">\r\n                        <ion-button color=\"yellow btn-curve\" size=\"small\" btn-yellow class=\"mt-10\" (click)=\"loadMore()\">\r\n                            Load\r\n                            More &nbsp;<ion-icon name=\"arrow-down-outline\"></ion-icon>\r\n                        </ion-button>\r\n                    </div>\r\n                </ion-col>\r\n            </ion-row>\r\n        </ion-grid>\r\n    </div>\r\n</ion-content>\r\n<ion-toolbar *ngIf=\"showToolbar == true\" color=\"yellow\" slot=\"bottom\">\r\n    <ion-tabs>\r\n        <ion-tab-bar color=\"yellow\" slot=\"bottom\">\r\n            <ion-tab-button [routerLink]=\"['/home']\">\r\n                <ion-icon name=\"caret-back-outline\"></ion-icon>\r\n                <ion-label>Back</ion-label>\r\n            </ion-tab-button>\r\n            <ion-tab-button (click)=\"enableSearch(true)\">\r\n                <ion-icon name=\"search-outline\"></ion-icon>\r\n                <ion-label>Search</ion-label>\r\n            </ion-tab-button>\r\n        </ion-tab-bar>\r\n    </ion-tabs>\r\n</ion-toolbar>";
      /***/
    },

    /***/
    "./src/app/uploadedimages/uploadedimages-routing.module.ts":
    /*!*****************************************************************!*\
      !*** ./src/app/uploadedimages/uploadedimages-routing.module.ts ***!
      \*****************************************************************/

    /*! exports provided: UploadedimagesPageRoutingModule */

    /***/
    function srcAppUploadedimagesUploadedimagesRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "UploadedimagesPageRoutingModule", function () {
        return UploadedimagesPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _uploadedimages_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./uploadedimages.page */
      "./src/app/uploadedimages/uploadedimages.page.ts");

      var routes = [{
        path: '',
        component: _uploadedimages_page__WEBPACK_IMPORTED_MODULE_3__["UploadedimagesPage"]
      }];

      var UploadedimagesPageRoutingModule = function UploadedimagesPageRoutingModule() {
        _classCallCheck(this, UploadedimagesPageRoutingModule);
      };

      UploadedimagesPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], UploadedimagesPageRoutingModule);
      /***/
    },

    /***/
    "./src/app/uploadedimages/uploadedimages.module.ts":
    /*!*********************************************************!*\
      !*** ./src/app/uploadedimages/uploadedimages.module.ts ***!
      \*********************************************************/

    /*! exports provided: UploadedimagesPageModule */

    /***/
    function srcAppUploadedimagesUploadedimagesModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "UploadedimagesPageModule", function () {
        return UploadedimagesPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _uploadedimages_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./uploadedimages-routing.module */
      "./src/app/uploadedimages/uploadedimages-routing.module.ts");
      /* harmony import */


      var _uploadedimages_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./uploadedimages.page */
      "./src/app/uploadedimages/uploadedimages.page.ts");
      /* harmony import */


      var _header_header_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! ../header/header.module */
      "./src/app/header/header.module.ts");

      var UploadedimagesPageModule = function UploadedimagesPageModule() {
        _classCallCheck(this, UploadedimagesPageModule);
      };

      UploadedimagesPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _uploadedimages_routing_module__WEBPACK_IMPORTED_MODULE_5__["UploadedimagesPageRoutingModule"], _header_header_module__WEBPACK_IMPORTED_MODULE_7__["HeaderPageModule"]],
        declarations: [_uploadedimages_page__WEBPACK_IMPORTED_MODULE_6__["UploadedimagesPage"]]
      })], UploadedimagesPageModule);
      /***/
    },

    /***/
    "./src/app/uploadedimages/uploadedimages.page.scss":
    /*!*********************************************************!*\
      !*** ./src/app/uploadedimages/uploadedimages.page.scss ***!
      \*********************************************************/

    /*! exports provided: default */

    /***/
    function srcAppUploadedimagesUploadedimagesPageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = ".img_size {\n  width: 100%;\n  -o-object-fit: cover;\n     object-fit: cover;\n  -o-object-position: center;\n     object-position: center;\n  background-color: transparent;\n  height: 150px;\n}\n\n[search-box] {\n  background: #ffffff1f;\n  padding: 10px;\n  margin: 5px;\n  border: 1px solid #ffffff3b;\n}\n\n[search-box] ion-list {\n  margin: -5px 7px;\n  width: 94%;\n  padding: 0;\n}\n\n.btn-curve {\n  border-radius: 10px;\n  --border-radius: 10px;\n  --ion-color-contrast: #212721;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvdXBsb2FkZWRpbWFnZXMvdXBsb2FkZWRpbWFnZXMucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksV0FBQTtFQUNBLG9CQUFBO0tBQUEsaUJBQUE7RUFDQSwwQkFBQTtLQUFBLHVCQUFBO0VBRUEsNkJBQUE7RUFDQSxhQUFBO0FBQUo7O0FBR0E7RUFDSSxxQkFBQTtFQUNBLGFBQUE7RUFDQSxXQUFBO0VBQ0EsMkJBQUE7QUFBSjs7QUFDSTtFQUNBLGdCQUFBO0VBQ0EsVUFBQTtFQUNBLFVBQUE7QUFDSjs7QUFHQTtFQUNJLG1CQUFBO0VBQ0EscUJBQUE7RUFDQSw2QkFBQTtBQUFKIiwiZmlsZSI6InNyYy9hcHAvdXBsb2FkZWRpbWFnZXMvdXBsb2FkZWRpbWFnZXMucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmltZ19zaXplIHtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgb2JqZWN0LWZpdDogY292ZXI7XHJcbiAgICBvYmplY3QtcG9zaXRpb246IGNlbnRlcjtcclxuICAgIC8vIGJhY2tncm91bmQtY29sb3I6IGxpZ2h0ZW4od2hpdGUsIDcwJSk7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDtcclxuICAgIGhlaWdodDogMTUwcHg7XHJcbiAgICAvLyBib3JkZXItcmFkaXVzOiAxMHB4O1xyXG59XHJcbltzZWFyY2gtYm94XXtcclxuICAgIGJhY2tncm91bmQ6ICNmZmZmZmYxZjtcclxuICAgIHBhZGRpbmc6IDEwcHg7XHJcbiAgICBtYXJnaW46IDVweDtcclxuICAgIGJvcmRlcjogMXB4IHNvbGlkICNmZmZmZmYzYjtcclxuICAgIGlvbi1saXN0e1xyXG4gICAgbWFyZ2luOiAtNXB4IDdweDtcclxuICAgIHdpZHRoOiA5NCU7XHJcbiAgICBwYWRkaW5nOiAwO1xyXG4gICAgfVxyXG59XHJcblxyXG4uYnRuLWN1cnZle1xyXG4gICAgYm9yZGVyLXJhZGl1czogMTBweDtcclxuICAgIC0tYm9yZGVyLXJhZGl1czogMTBweDtcclxuICAgIC0taW9uLWNvbG9yLWNvbnRyYXN0OiAjMjEyNzIxO1xyXG4gfSJdfQ== */";
      /***/
    },

    /***/
    "./src/app/uploadedimages/uploadedimages.page.ts":
    /*!*******************************************************!*\
      !*** ./src/app/uploadedimages/uploadedimages.page.ts ***!
      \*******************************************************/

    /*! exports provided: UploadedimagesPage */

    /***/
    function srcAppUploadedimagesUploadedimagesPageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "UploadedimagesPage", function () {
        return UploadedimagesPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _global_services_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ../global-services.service */
      "./src/app/global-services.service.ts");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _ionic_native_keyboard__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @ionic-native/keyboard */
      "./node_modules/@ionic-native/keyboard/index.js");

      var UploadedimagesPage = /*#__PURE__*/function () {
        function UploadedimagesPage(menuCtrl, service, navCtrl, popoverController, alertController, router) {
          _classCallCheck(this, UploadedimagesPage);

          this.menuCtrl = menuCtrl;
          this.service = service;
          this.navCtrl = navCtrl;
          this.popoverController = popoverController;
          this.alertController = alertController;
          this.router = router;
          this.pageNo = 0;
          this.search = '';
          this.loadMoreBtn = false; // =====================Hide Toolbar when keypad is open=====================

          this.showToolbar = true;
          this.url = 'getTrendSpotterImagesVideosWithPaging';
          this.uploadImageList = [];
          this.uploadImageListCount = 0; //=====================Search With Auto-Complete=====================
          // Declare the variable (in this case and initialize it with false)

          this.isItemAvailable = false;
          this.items = [];
          this.selectedItem = "";
          this.userName = localStorage.getItem('username'); // this.userMobileNumber = localStorage.getItem('mobileno');
        }

        _createClass(UploadedimagesPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            var _this = this;

            _ionic_native_keyboard__WEBPACK_IMPORTED_MODULE_5__["Keyboard"].onKeyboardShow().subscribe(function () {
              _this.showToolbar = false;
            });

            _ionic_native_keyboard__WEBPACK_IMPORTED_MODULE_5__["Keyboard"].onKeyboardHide().subscribe(function () {
              _this.showToolbar = true;
            });

            this.loadMore();
          }
        }, {
          key: "ionViewWillEnter",
          value: function ionViewWillEnter() {
            this.service.sendMessage('startNotificationCountFunction');
          } //=====================Open Menu=====================

        }, {
          key: "openMenu",
          value: function openMenu() {
            this.menuCtrl.open();
          } //=====================Move to Inner Page of Selected Image=====================

        }, {
          key: "navigateMediaInfo",
          value: function navigateMediaInfo(data) {
            this.router.navigate(['/media-info', {
              'data': JSON.stringify(data),
              'page': 'image'
            }]);
          }
        }, {
          key: "getItems",
          value: function getItems(ev) {}
        }, {
          key: "enableSearch",
          value: function enableSearch(value) {
            this.isEnableSearch = value;

            if (!value && this.search) {
              // this.infiniteScroll.disabled = false;
              this.search = '';
              this.pageNo = 1;
              this.uploadImageList = [];
              this.getTrendSpottersImageList();
            }

            this.Content.scrollToTop();
          }
        }, {
          key: "ItemAvailableClose",
          value: function ItemAvailableClose(selected) {
            this.isItemAvailable = false;
            this.selectedItem = selected;
          }
        }, {
          key: "searchList",
          value: function searchList() {
            this.pageNo = 1;
            this.uploadImageList = [];
            this.getTrendSpottersImageList();
          }
        }, {
          key: "getTrendSpottersImageList",
          value: function getTrendSpottersImageList() {
            var _this2 = this;

            var obj = {
              pageid: this.pageNo,
              rows_in_page: 12,
              username: this.userName,
              image_video: 'IMAGE',
              image_category: this.search
            };
            this.service.post_data(this.url, obj).subscribe(function (result) {
              // let res: any = this.service.parserToJSON(result);
              var json = _this2.service.parserToJSON(result);

              var jons1 = JSON.stringify(json);
              var json2 = jons1.replace(/\\n/g, '');
              var parsedJSON = JSON.parse(JSON.parse(json2).string);
              _this2.uploadImageListCount = parsedJSON.Total_Records;

              if (_this2.uploadImageListCount >= _this2.uploadImageList.length) {
                _this2.uploadImageList = _this2.uploadImageList.concat(parsedJSON.Details);
                setTimeout(function () {
                  if (_this2.uploadImageListCount > _this2.uploadImageList.length) {
                    _this2.loadMoreBtn = true;
                  } else {
                    _this2.loadMoreBtn = false;
                  }
                }, 500);
              }
            });
          } // loadData() {
          //   setTimeout(() => {
          //     this.pageNo = this.pageNo + 1;
          //     this.getTrendSpottersImageList();
          //     this.infiniteScroll.complete();
          //     if (this.uploadImageListCount <= this.uploadImageList.length) {
          //       this.infiniteScroll.disabled = true;
          //     }
          //   }, 500);
          // }

        }, {
          key: "loadMore",
          value: function loadMore() {
            this.pageNo = this.pageNo + 1;
            this.loadMoreBtn = false;
            this.getTrendSpottersImageList();
          }
        }]);

        return UploadedimagesPage;
      }();

      UploadedimagesPage.ctorParameters = function () {
        return [{
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["MenuController"]
        }, {
          type: _global_services_service__WEBPACK_IMPORTED_MODULE_2__["GlobalServicesService"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavController"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["PopoverController"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["AlertController"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]
        }];
      };

      UploadedimagesPage.propDecorators = {
        infiniteScroll: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"],
          args: [_ionic_angular__WEBPACK_IMPORTED_MODULE_3__["IonInfiniteScroll"]]
        }],
        Content: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"],
          args: [_ionic_angular__WEBPACK_IMPORTED_MODULE_3__["IonContent"]]
        }]
      };
      UploadedimagesPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-uploadedimages',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./uploadedimages.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/uploadedimages/uploadedimages.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./uploadedimages.page.scss */
        "./src/app/uploadedimages/uploadedimages.page.scss"))["default"]]
      })], UploadedimagesPage);
      /***/
    }
  }]);
})();
//# sourceMappingURL=uploadedimages-uploadedimages-module-es5.js.map