(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["terms-terms-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/terms/terms.page.html":
    /*!*****************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/terms/terms.page.html ***!
      \*****************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppTermsTermsPageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-header translucent no-border>\r\n  <ion-toolbar>\r\n      <ion-buttons slot=\"start\">\r\n          <ion-back-button defaultHref=\"/\"></ion-back-button>\r\n      </ion-buttons>\r\n      <ion-title>Terms & Conditions</ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n<ion-content style=\"overflow-y: hidden;\">\r\n  <div class=\"inner-div\">\r\n\r\n      <ion-grid>\r\n          <ion-row>\r\n            <ion-col size=\"1\"></ion-col>\r\n              <ion-col align-self-center>\r\n                  <ion-img src=\"../../assets/image/VidioNxtLogo.png\" class=\"mx-auto mt-3 w-45\"></ion-img>\r\n              </ion-col>\r\n              <ion-col size=\"1\"></ion-col>\r\n          </ion-row>\r\n          <ion-card class=\"card ion-padding\" align-self-center>\r\n              \r\n                <p class=\"start\">Please read these <b>Terms and Conditions</b> carefully before using this mobile application. we have send an OTP to ur Msite and all contents are intended for personal and non-commercial use.\r\n                (the \"Service\") operated by <b>visionxt</b> .Your access to and use of the Service is conditioned on your acceptance of and compliance with these Terms. \r\n                These Terms apply to all visitors, users and others who access or use the Service.</p>\r\n                <p class=\"middle\">By accessing or using the Service you agree to be bound by these Terms. If you disagree with any part of the terms then you may not access the Service.</p>\r\n                <p class=\"start\">Our Service allows you to post, link, store, share and otherwise make available certain information, text, graphics, videos, or other material. You are responsible for the \r\n                The Content section is for businesses that allow users to create, edit, share, make content on their websites or apps. For the full disclosure section, create your own <b>Terms and Conditions</b>.</p>\r\n                <p class=\"start\">Our Service may contain links to third-party web sites or services that are not owned or controlled by <b>visionxt</b>.<br>\r\n                  visionxt has no control over, and assumes no responsibility for, the content, privacy policies, or practices of any third party web sites or services. \r\n                  You further acknowledge and agree that <b>visionxt</b> shall not be responsible or liable, directly or indirectly, for any damage or loss caused or alleged to be \r\n                  caused by or in connection with use of or reliance on any such content, goods or services available on or through any such web sites or services.</p>                   \r\n                <p class=\"start\">We reserve the right, at our sole discretion, to modify or replace these Terms at any time. If a revision is material we will try to provide at least 30 days' \r\n                  notice prior to any new terms taking effect. What constitutes a material change will be determined at our sole discretion.</p>\r\n                <p class=\"start\">If you have any questions about these Terms, please contact us.</p>\r\n            \r\n          </ion-card>\r\n\r\n          <ion-row>\r\n           <ion-col class=\"butn ion-justify-content-center\"  >\r\n             <ion-button (click)=\"signupFunction()\" expand=\"block\" color=\"yellow  btn-1\" size=\"medium\" btn-yellow >Agree</ion-button>\r\n           </ion-col>\r\n           <ion-col class=\"butn ion-justify-content-center\" >\r\n             <ion-button (click)=\"declineAlert()\" expand=\"block\" color=\"transparent\" size=\"medium\" class=\"btn-2\" >Decline</ion-button>\r\n           </ion-col>\r\n          </ion-row>\r\n\r\n      </ion-grid>\r\n      </div>\r\n    </ion-content>";
      /***/
    },

    /***/
    "./src/app/terms/terms-routing.module.ts":
    /*!***********************************************!*\
      !*** ./src/app/terms/terms-routing.module.ts ***!
      \***********************************************/

    /*! exports provided: TermsPageRoutingModule */

    /***/
    function srcAppTermsTermsRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "TermsPageRoutingModule", function () {
        return TermsPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _terms_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./terms.page */
      "./src/app/terms/terms.page.ts");

      var routes = [{
        path: '',
        component: _terms_page__WEBPACK_IMPORTED_MODULE_3__["TermsPage"]
      }];

      var TermsPageRoutingModule = function TermsPageRoutingModule() {
        _classCallCheck(this, TermsPageRoutingModule);
      };

      TermsPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], TermsPageRoutingModule);
      /***/
    },

    /***/
    "./src/app/terms/terms.module.ts":
    /*!***************************************!*\
      !*** ./src/app/terms/terms.module.ts ***!
      \***************************************/

    /*! exports provided: TermsPageModule */

    /***/
    function srcAppTermsTermsModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "TermsPageModule", function () {
        return TermsPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _terms_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./terms-routing.module */
      "./src/app/terms/terms-routing.module.ts");
      /* harmony import */


      var _terms_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./terms.page */
      "./src/app/terms/terms.page.ts");

      var TermsPageModule = function TermsPageModule() {
        _classCallCheck(this, TermsPageModule);
      };

      TermsPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _terms_routing_module__WEBPACK_IMPORTED_MODULE_5__["TermsPageRoutingModule"]],
        declarations: [_terms_page__WEBPACK_IMPORTED_MODULE_6__["TermsPage"]]
      })], TermsPageModule);
      /***/
    },

    /***/
    "./src/app/terms/terms.page.scss":
    /*!***************************************!*\
      !*** ./src/app/terms/terms.page.scss ***!
      \***************************************/

    /*! exports provided: default */

    /***/
    function srcAppTermsTermsPageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = ".btn-1 {\n  background: #212721;\n  --border-radius: 30px;\n}\n\n.btn-2 {\n  border: 1px solid #fff;\n  border-radius: 30px;\n}\n\nion-title {\n  color: #000;\n  font-family: \"Poppins\", sans-serif;\n}\n\n.mt-10 {\n  font-family: \"Poppins\", sans-serif;\n}\n\nion-back-button {\n  color: #000;\n}\n\n.butn {\n  display: flex;\n}\n\n.card {\n  border-radius: 0;\n  font-family: \"Poppins\", sans-serif;\n  background-color: var(--ion-color-yellow);\n  color: #000;\n  overflow-y: scroll;\n  height: 420px;\n  text-align: justify;\n}\n\n.start {\n  font-size: 12px;\n}\n\n.middle {\n  font-size: 12px;\n  font-weight: bold;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvdGVybXMvdGVybXMucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUNBO0VBQ0ksbUJBQUE7RUFDQSxxQkFBQTtBQUFKOztBQUVBO0VBQ0ksc0JBQUE7RUFDQSxtQkFBQTtBQUNKOztBQUVBO0VBQ0ksV0FBQTtFQUNBLGtDQUFBO0FBQ0o7O0FBRUE7RUFDSSxrQ0FBQTtBQUNKOztBQUVBO0VBQ0ksV0FBQTtBQUNKOztBQUVBO0VBQ0ksYUFBQTtBQUNKOztBQUlBO0VBQ0ksZ0JBQUE7RUFDQSxrQ0FBQTtFQUNBLHlDQUFBO0VBQ0EsV0FBQTtFQUNBLGtCQUFBO0VBQ0EsYUFBQTtFQUNBLG1CQUFBO0FBREo7O0FBSUE7RUFDSSxlQUFBO0FBREo7O0FBSUE7RUFDSSxlQUFBO0VBQ0EsaUJBQUE7QUFESiIsImZpbGUiOiJzcmMvYXBwL3Rlcm1zL3Rlcm1zLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIlxyXG4uYnRuLTEge1xyXG4gICAgYmFja2dyb3VuZDogIzIxMjcyMTtcclxuICAgIC0tYm9yZGVyLXJhZGl1czogMzBweDtcclxufVxyXG4uYnRuLTIge1xyXG4gICAgYm9yZGVyOiAxcHggc29saWQgI2ZmZjtcclxuICAgIGJvcmRlci1yYWRpdXM6IDMwcHg7XHJcbn1cclxuXHJcbmlvbi10aXRsZSB7XHJcbiAgICBjb2xvcjogIzAwMDtcclxuICAgIGZvbnQtZmFtaWx5OiAnUG9wcGlucycsIHNhbnMtc2VyaWY7XHJcbn1cclxuXHJcbi5tdC0xMCB7XHJcbiAgICBmb250LWZhbWlseTogJ1BvcHBpbnMnLCBzYW5zLXNlcmlmO1xyXG59XHJcblxyXG5pb24tYmFjay1idXR0b24ge1xyXG4gICAgY29sb3I6ICMwMDA7XHJcbn1cclxuXHJcbi5idXRuIHtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAvLyBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICAvLyBsZWZ0OiAwLjhyZW07XHJcbn1cclxuXHJcbi5jYXJkIHtcclxuICAgIGJvcmRlci1yYWRpdXM6IDA7XHJcbiAgICBmb250LWZhbWlseTogJ1BvcHBpbnMnLCBzYW5zLXNlcmlmO1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogdmFyKC0taW9uLWNvbG9yLXllbGxvdyk7Ly8jRjZDNzYyO1xyXG4gICAgY29sb3I6ICMwMDA7XHJcbiAgICBvdmVyZmxvdy15OiBzY3JvbGw7XHJcbiAgICBoZWlnaHQ6IDQyMHB4O1xyXG4gICAgdGV4dC1hbGlnbjoganVzdGlmeTtcclxufVxyXG5cclxuLnN0YXJ0IHtcclxuICAgIGZvbnQtc2l6ZTogMTJweDtcclxufVxyXG5cclxuLm1pZGRsZSB7XHJcbiAgICBmb250LXNpemU6IDEycHg7XHJcbiAgICBmb250LXdlaWdodDogYm9sZDtcclxufSJdfQ== */";
      /***/
    },

    /***/
    "./src/app/terms/terms.page.ts":
    /*!*************************************!*\
      !*** ./src/app/terms/terms.page.ts ***!
      \*************************************/

    /*! exports provided: TermsPage */

    /***/
    function srcAppTermsTermsPageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "TermsPage", function () {
        return TermsPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _global_services_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! ../global-services.service */
      "./src/app/global-services.service.ts");

      var TermsPage = /*#__PURE__*/function () {
        function TermsPage(alertController, route, navCtrl, router, service) {
          var _this = this;

          _classCallCheck(this, TermsPage);

          this.alertController = alertController;
          this.route = route;
          this.navCtrl = navCtrl;
          this.router = router;
          this.service = service;
          this.data = {};
          this.route.params.subscribe(function (params) {
            _this.data = params['data'];
          });
        }

        _createClass(TermsPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }, {
          key: "acceptAlert",
          value: function acceptAlert(message) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
              var _this2 = this;

              var alert;
              return regeneratorRuntime.wrap(function _callee$(_context) {
                while (1) {
                  switch (_context.prev = _context.next) {
                    case 0:
                      _context.next = 2;
                      return this.alertController.create({
                        cssClass: 'alert-custom-class',
                        message: message,
                        buttons: [{
                          text: 'OK',
                          cssClass: 'warning',
                          handler: function handler() {
                            _this2.navCtrl.navigateForward(['/login']);
                          }
                        }]
                      });

                    case 2:
                      alert = _context.sent;
                      _context.next = 5;
                      return alert.present();

                    case 5:
                    case "end":
                      return _context.stop();
                  }
                }
              }, _callee, this);
            }));
          }
        }, {
          key: "signupFunction",
          value: function signupFunction() {
            var _this3 = this;

            this.service.post_data("studRegisBase64", JSON.parse(this.data)).subscribe(function (result) {
              var json = _this3.service.parserToJSON(result);

              var parsedJSON = JSON.parse(json.string);
              _this3.Message = parsedJSON[0].Message;

              _this3.acceptAlert(_this3.Message);
            }, function (error) {
              console.log(error);
            });
          }
        }, {
          key: "declineAlert",
          value: function declineAlert() {
            this.navCtrl.navigateForward(['aluminisignup']);
          }
        }]);

        return TermsPage;
      }();

      TermsPage.ctorParameters = function () {
        return [{
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]
        }, {
          type: _global_services_service__WEBPACK_IMPORTED_MODULE_4__["GlobalServicesService"]
        }];
      };

      TermsPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-terms',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./terms.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/terms/terms.page.html"))["default"],
        encapsulation: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewEncapsulation"].None,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./terms.page.scss */
        "./src/app/terms/terms.page.scss"))["default"]]
      })], TermsPage);
      /***/
    }
  }]);
})();
//# sourceMappingURL=terms-terms-module-es5.js.map