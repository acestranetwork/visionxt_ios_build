(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["niftsignup-niftsignup-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/niftsignup/niftsignup.page.html":
/*!***************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/niftsignup/niftsignup.page.html ***!
  \***************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header translucent no-border>\r\n  <ion-toolbar>\r\n    <ion-buttons slot=\"start\">\r\n      <ion-back-button defaultHref=\"/\"></ion-back-button>\r\n    </ion-buttons>\r\n    <ion-title>Signup</ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n<ion-content style=\"overflow-y: hidden;\">\r\n  <div class=\"inner-div\">\r\n    <form [formGroup]=\"signupFg\" autocomplete=\"off\" #f=\"ngForm\" (ngSubmit)=\"signupFg.valid && signupFunction()\">\r\n      <ion-grid>\r\n        <ion-row>\r\n          <ion-col size=\"1\"></ion-col>\r\n          <ion-col align-self-center>\r\n            <ion-img src=\"../../assets/image/logo-removebg.png\" class=\"mx-auto mt-3 w-45\"></ion-img>\r\n            <div class=\"details ion-text-center\">\r\n              <ion-label>Fill below details for Others</ion-label>\r\n            </div>\r\n          </ion-col>\r\n          <ion-col size=\"1\"></ion-col>\r\n        </ion-row>\r\n        <ion-row class=\"mt-3\">\r\n          <ion-col align-self-center>\r\n            <ion-item header-menu (click)=\"uploadImage()\" lines=\"none\">\r\n              <ion-label class=\"upd-img mt-0\">\r\n                Upload Image\r\n              </ion-label>\r\n              <ion-thumbnail slot=\"end\">\r\n                <img [src]=\"currentImage\" *ngIf=\"imageUrlPath\">\r\n                <img *ngIf=\"!imageUrlPath\" src=\"../../assets/image/avatar_up.jpg\">\r\n              </ion-thumbnail>\r\n            </ion-item>\r\n          </ion-col>\r\n        </ion-row>\r\n        <ion-row>\r\n          <ion-col align-self-center>\r\n            <ion-item input-text-color>\r\n              <ion-label color=\"medium\" position=\"floating\">Name\r\n              </ion-label>\r\n              <ion-input formControlName=\"stud_name\" type=\"text\" required></ion-input>\r\n            </ion-item>\r\n            <div class=\"error\"\r\n              *ngIf=\"signupFg.controls['stud_name'].errors?.required && (signupFg.get('stud_name').touched || f.submitted)\">\r\n              Username is Required.</div>\r\n          </ion-col>\r\n        </ion-row>\r\n\r\n        <ion-row>\r\n          <ion-col align-self-center>\r\n            <ion-item input-text-color>\r\n              <ion-label label color=\"medium\" position=\"floating\">Type Your Degree\r\n              </ion-label>\r\n              <ion-input type=\"text\" formControlName=\"name_of_deg\" required></ion-input>\r\n            </ion-item>\r\n            <div class=\"error\"\r\n              *ngIf=\"signupFg.controls['name_of_deg'].errors?.required && (signupFg.get('name_of_deg').touched || f.submitted)\">\r\n              Degree is Required.</div>\r\n          </ion-col>\r\n        </ion-row>\r\n        <ion-row>\r\n          <ion-col align-self-center>\r\n            <ion-item input-text-color>\r\n              <ion-label color=\"medium\" position=\"floating\">Year Of Completion</ion-label>\r\n              <ion-datetime formControlName=\"year_of_comp\" displayFormat=\"YYYY\" [(ngModel)]=\"Date\" min=\"1987\" max=\"2020\"\r\n                required></ion-datetime>\r\n            </ion-item>\r\n            <div class=\"error\"\r\n              *ngIf=\"signupFg.controls['year_of_comp'].errors?.required && (signupFg.get('year_of_comp').touched || f.submitted)\">\r\n              Year Of Completion is Required.</div>\r\n          </ion-col>\r\n        </ion-row>\r\n        <ion-row>\r\n          <ion-col align-self-center>\r\n            <ion-item input-text-color>\r\n              <ion-label label color=\"medium\" position=\"floating\">University\r\n              </ion-label>\r\n              <ion-input formControlName=\"campus\" type=\"text\" required></ion-input>\r\n            </ion-item>\r\n            <div class=\"error\"\r\n              *ngIf=\"signupFg.controls['campus'].errors?.required && (signupFg.get('campus').touched || f.submitted)\">\r\n              Campus is Required.</div>\r\n          </ion-col>\r\n        </ion-row>\r\n        <ion-row>\r\n          <ion-col align-self-center>\r\n            <ion-item input-text-color>\r\n              <ion-label color=\"medium\" position=\"floating\">Date Of Birth</ion-label>\r\n              <ion-datetime formControlName=\"dob_ddmmyyyy\" value=\"\" required></ion-datetime>\r\n            </ion-item>\r\n            <div class=\"error\"\r\n              *ngIf=\"signupFg.controls['dob_ddmmyyyy'].errors?.required && (signupFg.get('dob_ddmmyyyy').touched || f.submitted)\">\r\n              Date Of Birth is Required.</div>\r\n          </ion-col>\r\n        </ion-row>\r\n        <ion-label class=\"gend\" color=\"medium\" position=\"floating\">Gender</ion-label>\r\n        <ion-row>\r\n          <ion-col align-self-center size=\"4\" *ngFor=\"let g of genderList\" (click)=\"selectGender(g)\">\r\n            <div [ngClass]=\"g.isSelected ? 'selected-gender': 'not-selected-gender'\">{{g.name}} </div>\r\n          </ion-col>\r\n          <div class=\"error\" *ngIf=\"inValidGender\">\r\n            Gender is Required.</div>\r\n        </ion-row>\r\n        <ion-row>\r\n          <ion-col align-self-center>\r\n            <ion-item input-text-color>\r\n              <ion-label label color=\"medium\" position=\"floating\">Email\r\n              </ion-label>\r\n              <ion-input formControlName=\"emailid\" type=\"email\" (ionBlur)=\"mailVerification()\" required></ion-input>\r\n            </ion-item>\r\n            <div class=\"error\"\r\n              *ngIf=\"!emailidErrMessage && signupFg.controls['emailid'].errors?.required && (signupFg.get('emailid').touched || f.submitted)\">\r\n              Email is Required.</div>\r\n            <div class=\"error\"\r\n              *ngIf=\"!emailidErrMessage && signupFg.controls['emailid'].errors?.pattern &&  (signupFg.get('emailid').touched || f.submitted)\">\r\n              Email invalid.</div>\r\n            <div class=\"error\" *ngIf=\"emailidErrMessage && (signupFg.get('mobileno').touched ||\r\n              f.submitted)\">\r\n              {{emailidErrMessage}}</div>\r\n\r\n          </ion-col>\r\n        </ion-row>\r\n        <ion-row>\r\n          <ion-col align-self-center>\r\n            <ion-item input-text-color>\r\n              <ion-label label color=\"medium\" position=\"floating\">Mobile Number\r\n              </ion-label>\r\n              <ion-input formControlName=\"mobileno\" minlength=10 maxlength=10 type=\"tel\"\r\n                (ionBlur)=\"mobileNumberVerification()\" required>\r\n              </ion-input>\r\n            </ion-item>\r\n            <div class=\"error\"\r\n              *ngIf=\"!mobilenoErrMessage && signupFg.controls['mobileno'].errors?.required && (signupFg.get('mobileno').touched || f.submitted)\">\r\n              Mobile Number is Required.</div>\r\n            <div class=\"error\"\r\n              *ngIf=\"!mobilenoErrMessage && (signupFg.controls['mobileno'].errors?.min || signupFg.controls['mobileno'].errors?.max)  && (signupFg.get('mobileno').touched || f.submitted)\">\r\n              Mobile Number is Invalid.</div>\r\n            <div class=\"error\" *ngIf=\"mobilenoErrMessage && (signupFg.get('mobileno').touched ||\r\n              f.submitted)\">\r\n              {{mobilenoErrMessage}}</div>\r\n          </ion-col>\r\n        </ion-row>\r\n        <ion-row>\r\n          <ion-col align-self-center>\r\n            <ion-item input-text-color>\r\n              <ion-label label color=\"medium\" position=\"floating\">Area Of Interest\r\n              </ion-label>\r\n              <ion-input formControlName=\"interests\" type=\"text\" required></ion-input>\r\n            </ion-item>\r\n            <div class=\"error\"\r\n              *ngIf=\"signupFg.controls['interests'].errors?.required && (signupFg.get('interests').touched || f.submitted)\">\r\n              Area Of Interest is Required.</div>\r\n          </ion-col>\r\n        </ion-row>\r\n        <ion-row>\r\n          <ion-col align-self-center>\r\n            <ion-item input-text-color>\r\n              <ion-label label color=\"medium\" position=\"floating\">Hobby\r\n              </ion-label>\r\n              <ion-input formControlName=\"hobby\" type=\"text\" required></ion-input>\r\n            </ion-item>\r\n            <div class=\"error\"\r\n              *ngIf=\"signupFg.controls['hobby'].errors?.required && (signupFg.get('hobby').touched || f.submitted)\">\r\n              Hobby is Required.</div>\r\n          </ion-col>\r\n        </ion-row>\r\n        <ion-row>\r\n          <ion-col align-self-center>\r\n            <ion-item input-text-color>\r\n              <ion-label label color=\"medium\" position=\"floating\">Address\r\n              </ion-label>\r\n              <ion-input formControlName=\"address\" type=\"text\" required></ion-input>\r\n            </ion-item>\r\n            <div class=\"error\"\r\n              *ngIf=\"signupFg.controls['address'].errors?.required && (signupFg.get('address').touched || f.submitted)\">\r\n              Address is Required.</div>\r\n          </ion-col>\r\n        </ion-row>\r\n        <ion-row>\r\n          <ion-col align-self-center>\r\n            <ion-item input-text-color>\r\n              <ion-label label color=\"medium\" position=\"floating\">City\r\n              </ion-label>\r\n              <ion-input formControlName=\"city\" type=\"text\" required></ion-input>\r\n            </ion-item>\r\n            <div class=\"error\"\r\n              *ngIf=\"signupFg.controls['city'].errors?.required && (signupFg.get('city').touched || f.submitted)\">\r\n              City is Required.</div>\r\n          </ion-col>\r\n        </ion-row>\r\n        <ion-row>\r\n          <ion-col align-self-center>\r\n            <ion-item input-text-color>\r\n              <ion-label label color=\"medium\" position=\"floating\">Pin Code\r\n              </ion-label>\r\n              <ion-input formControlName=\"pinCode\" type=\"number\" required></ion-input>\r\n            </ion-item>\r\n            <div class=\"error\"\r\n              *ngIf=\"signupFg.controls['pinCode'].errors?.required && (signupFg.get('pinCode').touched || f.submitted)\">\r\n              Pin Code is Required.</div>\r\n          </ion-col>\r\n        </ion-row>\r\n        <ion-row>\r\n          <ion-col align-self-center>\r\n            <ion-item input-text-color>\r\n              <ion-label label color=\"medium\" position=\"floating\">Designation\r\n              </ion-label>\r\n              <ion-input formControlName=\"designation\" type=\"text\" required></ion-input>\r\n            </ion-item>\r\n            <div class=\"error\"\r\n              *ngIf=\"signupFg.controls['designation'].errors?.required && (signupFg.get('designation').touched || f.submitted)\">\r\n              Designation is Required.</div>\r\n          </ion-col>\r\n        </ion-row>\r\n        <ion-row>\r\n          <ion-col align-self-center>\r\n            <ion-item input-text-color>\r\n              <ion-label label color=\"medium\" position=\"floating\">Company\r\n              </ion-label>\r\n              <ion-input formControlName=\"company\" type=\"text\" required></ion-input>\r\n            </ion-item>\r\n            <div class=\"error\"\r\n              *ngIf=\"signupFg.controls['company'].errors?.required && (signupFg.get('company').touched || f.submitted)\">\r\n              Company is Required.</div>\r\n          </ion-col>\r\n        </ion-row>\r\n\r\n        <div class=\"separator\">Next section</div>\r\n        <ion-row>\r\n          <ion-col align-self-center>\r\n            <ion-item input-text-color>\r\n              <ion-label label color=\"medium\" position=\"floating\">Username\r\n              </ion-label>\r\n              <ion-input formControlName=\"username\" type=\"text\" (ionBlur)=\"validateUsernameUnique()\" max=\"10\" required>\r\n              </ion-input>\r\n            </ion-item>\r\n            <div class=\"error\" *ngIf=\"!usernameErrMessage && signupFg.controls['username'].errors?.required && (signupFg.get('username').touched ||\r\n            f.submitted)\">\r\n              Username is Required.</div>\r\n            <div class=\"error\" *ngIf=\"usernameErrMessage && (signupFg.get('username').touched ||\r\n            f.submitted)\">\r\n              {{usernameErrMessage}}</div>\r\n          </ion-col>\r\n        </ion-row>\r\n        <ion-row>\r\n          <ion-col align-self-center>\r\n            <ion-item input-text-color>\r\n              <ion-label label color=\"medium\" position=\"floating\">Password\r\n              </ion-label>\r\n              <ion-input [type]=\"passwordType\" formControlName=\"pwd\" required></ion-input>\r\n              <ion-icon [name]=\"passwordIcon\" color=\"light\" size=\"small\" (click)='hideShowPassword()'\r\n                class=\"ion-align-self-end\" slot=\"end\">\r\n              </ion-icon>\r\n            </ion-item>\r\n            <div class=\"error\"\r\n              *ngIf=\"signupFg.controls['pwd'].errors?.required && (signupFg.get('pwd').touched || f.submitted)\">\r\n              Password is Required.</div>\r\n            <div class=\"error\" *ngIf=\"signupFg.controls['pwd'].errors?.pattern &&\r\n                (signupFg.get('pwd').touched || f.submitted)\">\r\n              Password must be a capital letter, a lowercase letter, a special letter, a numeric value.</div>\r\n          </ion-col>\r\n        </ion-row>\r\n        <ion-row>\r\n          <ion-col align-self-center>\r\n            <ion-item input-text-color>\r\n              <ion-label label color=\"medium\" position=\"floating\">Confirm Password\r\n              </ion-label>\r\n              <ion-input [type]=\"cpasswordType\" formControlName=\"confirmpassword\" (ionBlur)=\"MatchPassword()\" required>\r\n              </ion-input>\r\n              <ion-icon [name]=\"cpasswordIcon\" color=\"light\" size=\"small\" (click)='hideShowcPassword()'\r\n                class=\"ion-align-self-end\" slot=\"end\">\r\n              </ion-icon>\r\n            </ion-item>\r\n            <div class=\"error\"\r\n              *ngIf=\"!cpwdErrMessage && signupFg.controls['confirmpassword'].errors?.required && (signupFg.get('confirmpassword').touched || f.submitted)\">\r\n              Confirm Password is Required.</div>\r\n            <div class=\"error\" *ngIf=\"!cpwdErrMessage && signupFg.controls['confirmpassword'].errors?.pattern &&\r\n                (signupFg.get('confirmpassword').touched || f.submitted)\">\r\n              Confirm Password must be a capital letter, a lowercase letter, a special letter, a numeric value.</div>\r\n            <div class=\"error\" *ngIf=\"cpwdErrMessage &&\r\n                (signupFg.get('confirmpassword').touched || f.submitted)\">\r\n              {{cpwdErrMessage}}</div>\r\n          </ion-col>\r\n        </ion-row>\r\n\r\n        <!-- <ion-row>\r\n          <ion-col align-self-center>\r\n            <img [src]=\"currentImage\" *ngIf=\"currentImage\" style=\"height: 100px; width:100px;\">\r\n            <ion-button ion-fab (click)=\"uploadImage()\" required expand=\"block\" color=\"yellow\" size=\"medium\" btn-yellow\r\n              class=\"mt-0\">Upload Image\r\n              &emsp;<ion-icon name=\"camera\"></ion-icon>\r\n            </ion-button>\r\n          </ion-col>\r\n        </ion-row> -->\r\n        <ion-row>\r\n          <ion-col align-self-center class=\"col-padd\">\r\n            <ion-button type=\"submit\" (click)=\"signupFunction()\" expand=\"block\" color=\"btn-yellow btn-height\"\r\n              size=\"large\" btn-yellow class=\"mt-15\">Send OTP\r\n            </ion-button>\r\n          </ion-col>\r\n        </ion-row>\r\n      </ion-grid>\r\n    </form>\r\n  </div>\r\n</ion-content>");

/***/ }),

/***/ "./src/app/niftsignup/niftsignup-routing.module.ts":
/*!*********************************************************!*\
  !*** ./src/app/niftsignup/niftsignup-routing.module.ts ***!
  \*********************************************************/
/*! exports provided: NiftsignupPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NiftsignupPageRoutingModule", function() { return NiftsignupPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _niftsignup_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./niftsignup.page */ "./src/app/niftsignup/niftsignup.page.ts");




const routes = [
    {
        path: '',
        component: _niftsignup_page__WEBPACK_IMPORTED_MODULE_3__["NiftsignupPage"]
    }
];
let NiftsignupPageRoutingModule = class NiftsignupPageRoutingModule {
};
NiftsignupPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], NiftsignupPageRoutingModule);



/***/ }),

/***/ "./src/app/niftsignup/niftsignup.module.ts":
/*!*************************************************!*\
  !*** ./src/app/niftsignup/niftsignup.module.ts ***!
  \*************************************************/
/*! exports provided: NiftsignupPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NiftsignupPageModule", function() { return NiftsignupPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _niftsignup_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./niftsignup-routing.module */ "./src/app/niftsignup/niftsignup-routing.module.ts");
/* harmony import */ var _niftsignup_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./niftsignup.page */ "./src/app/niftsignup/niftsignup.page.ts");







let NiftsignupPageModule = class NiftsignupPageModule {
};
NiftsignupPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _niftsignup_routing_module__WEBPACK_IMPORTED_MODULE_5__["NiftsignupPageRoutingModule"]
        ],
        declarations: [_niftsignup_page__WEBPACK_IMPORTED_MODULE_6__["NiftsignupPage"]]
    })
], NiftsignupPageModule);



/***/ }),

/***/ "./src/app/niftsignup/niftsignup.page.scss":
/*!*************************************************!*\
  !*** ./src/app/niftsignup/niftsignup.page.scss ***!
  \*************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-title {\n  color: #000;\n  font-family: \"Poppins\", sans-serif;\n}\n\nion-back-button {\n  color: #000;\n}\n\n.bton-group input {\n  display: none;\n}\n\nion-select {\n  position: relative;\n  bottom: 1px;\n  margin-bottom: 1rem;\n}\n\nion-label {\n  font-family: \"Poppins\", sans-serif;\n}\n\n.details {\n  margin-top: 1rem;\n  font-weight: 600;\n  color: var(--ion-color-yellow);\n  font-size: 14px;\n}\n\n.gender {\n  color: #616161;\n  font-weight: 500;\n}\n\n.bton {\n  background: #1e1e1e;\n  border: 1px solid #616161;\n  padding: 6px 13px;\n  height: 40px;\n  cursor: pointer;\n  border-radius: 0;\n  color: #616161;\n  text-decoration: none;\n  font-size: 1rem;\n  text-align: center;\n  margin-top: 0.5rem;\n}\n\n.bton-group input:checked + label,\n.bton-group input:checked + label:active {\n  background-color: var(--ion-color-yellow);\n  color: #030303;\n}\n\n.selected-gender {\n  background: var(--ion-color-yellow);\n  border: solid 1px;\n  color: black;\n  padding: 8px 0px;\n  font-size: 12px;\n  text-transform: capitalize;\n  text-align: center;\n  cursor: pointer;\n}\n\n.not-selected-gender {\n  padding: 8px 0px;\n  border: solid 1px;\n  border-color: #c5c5c5;\n  color: #ffffff;\n  font-size: 12px;\n  text-transform: capitalize;\n  text-align: center;\n  cursor: pointer;\n}\n\n.error {\n  color: #d39e00;\n  font-size: 12px;\n}\n\n.gend {\n  margin-left: 0.3rem;\n}\n\nh5 {\n  font-weight: lighter !important;\n  font-size: 1em !important;\n  color: var(--ion-color-light);\n}\n\n.btn-height {\n  height: 85%;\n  border-radius: 10px;\n}\n\n.col-padd {\n  padding: 15px 25px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbmlmdHNpZ251cC9uaWZ0c2lnbnVwLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLFdBQUE7RUFDQSxrQ0FBQTtBQUNGOztBQUVBO0VBQ0UsV0FBQTtBQUNGOztBQUNBO0VBQ0UsYUFBQTtBQUVGOztBQUVBO0VBQ0Usa0JBQUE7RUFDQSxXQUFBO0VBQ0EsbUJBQUE7QUFDRjs7QUFDQTtFQUNFLGtDQUFBO0FBRUY7O0FBQUE7RUFDRSxnQkFBQTtFQUNBLGdCQUFBO0VBQ0EsOEJBQUE7RUFDQSxlQUFBO0FBR0Y7O0FBQUE7RUFDRSxjQUFBO0VBQ0EsZ0JBQUE7QUFHRjs7QUFBQTtFQUNFLG1CQUFBO0VBQ0EseUJBQUE7RUFDQSxpQkFBQTtFQUNBLFlBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7RUFDQSxjQUFBO0VBQ0EscUJBQUE7RUFDQSxlQUFBO0VBQ0Esa0JBQUE7RUFDQSxrQkFBQTtBQUdGOztBQURBOztFQUVBLHlDQUFBO0VBQ0EsY0FBQTtBQUlBOztBQVdFO0VBQ0ksbUNBQUE7RUFDQSxpQkFBQTtFQUVBLFlBQUE7RUFDQSxnQkFBQTtFQUNBLGVBQUE7RUFDQSwwQkFBQTtFQUNBLGtCQUFBO0VBQ0EsZUFBQTtBQVROOztBQVdJO0VBQ0UsZ0JBQUE7RUFDQSxpQkFBQTtFQUNBLHFCQUFBO0VBQ0EsY0FBQTtFQUNBLGVBQUE7RUFDQSwwQkFBQTtFQUNBLGtCQUFBO0VBQ0EsZUFBQTtBQVJOOztBQVVJO0VBQ0UsY0FBQTtFQUNBLGVBQUE7QUFQTjs7QUFTRTtFQUNJLG1CQUFBO0FBTk47O0FBU0k7RUFDRSwrQkFBQTtFQUNBLHlCQUFBO0VBQ0EsNkJBQUE7QUFOTjs7QUFTRTtFQUNFLFdBQUE7RUFDQSxtQkFBQTtBQU5KOztBQVFBO0VBQ0ksa0JBQUE7QUFMSiIsImZpbGUiOiJzcmMvYXBwL25pZnRzaWdudXAvbmlmdHNpZ251cC5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24tdGl0bGUge1xyXG4gIGNvbG9yOiAjMDAwO1xyXG4gIGZvbnQtZmFtaWx5OidQb3BwaW5zJywgc2Fucy1zZXJpZjtcclxufVxyXG5cclxuaW9uLWJhY2stYnV0dG9uIHtcclxuICBjb2xvcjogIzAwMDtcclxufVxyXG4uYnRvbi1ncm91cCBpbnB1dCB7XHJcbiAgZGlzcGxheTogbm9uZTtcclxufVxyXG5cclxuXHJcbmlvbi1zZWxlY3Qge1xyXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICBib3R0b206IDFweDtcclxuICBtYXJnaW4tYm90dG9tOiAxcmVtO1xyXG59XHJcbmlvbi1sYWJlbHtcclxuICBmb250LWZhbWlseTonUG9wcGlucycsIHNhbnMtc2VyaWY7XHJcbn1cclxuLmRldGFpbHMge1xyXG4gIG1hcmdpbi10b3A6IDFyZW07XHJcbiAgZm9udC13ZWlnaHQ6IDYwMDtcclxuICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLXllbGxvdyk7Ly8jRjZDNzYyO1xyXG4gIGZvbnQtc2l6ZToxNHB4O1xyXG59XHJcblxyXG4uZ2VuZGVyIHtcclxuICBjb2xvcjogIzYxNjE2MTtcclxuICBmb250LXdlaWdodDogNTAwO1xyXG59XHJcblxyXG4uYnRvbntcclxuICBiYWNrZ3JvdW5kOiAjMWUxZTFlO1xyXG4gIGJvcmRlcjogMXB4IHNvbGlkICM2MTYxNjE7XHJcbiAgcGFkZGluZzogNnB4IDEzcHg7XHJcbiAgaGVpZ2h0OiA0MHB4O1xyXG4gIGN1cnNvcjogcG9pbnRlcjtcclxuICBib3JkZXItcmFkaXVzOiAwO1xyXG4gIGNvbG9yOiAjNjE2MTYxO1xyXG4gIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcclxuICBmb250LXNpemU6IDFyZW07XHJcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gIG1hcmdpbi10b3A6IDAuNXJlbTtcclxufVxyXG4uYnRvbi1ncm91cCBpbnB1dDpjaGVja2VkICsgbGFiZWwsXHJcbi5idG9uLWdyb3VwIGlucHV0OmNoZWNrZWQgKyBsYWJlbDphY3RpdmUge1xyXG5iYWNrZ3JvdW5kLWNvbG9yOiB2YXIoLS1pb24tY29sb3IteWVsbG93KTsvLyNGNkM3NjI7XHJcbmNvbG9yOiAjMDMwMzAzO1xyXG59XHJcblxyXG4gIC8vIC5idG5fY2FtMSB7XHJcbiAgLy8gICAgIHBhZGRpbmc6IDBweCAxM3B4IDBweCAxM3B4ICFpbXBvcnRhbnQ7XHJcbiAgLy8gICAgIG1hcmdpbjogMDtcclxuICAvLyAgICAgYmFja2dyb3VuZC1jb2xvcjogI0Y2Qzc2MjtcclxuICAvLyAgICAgY29sb3I6ICMwMzAzMDM7XHJcbiAgLy8gICAgIGhlaWdodDogMi41cmVtO1xyXG4gIC8vICAgICAuaW9uLWljb24ge1xyXG4gIC8vICAgICAgICAgbWFyZ2luOiAwO1xyXG4gIC8vICAgICAgICAgcGFkZGluZzogMDtcclxuICAvLyAgICAgICAgIGZvbnQtc2l6ZTogN3B4XHJcbiAgLy8gICAgIH1cclxuICAvLyB9XHJcbiAgLnNlbGVjdGVkLWdlbmRlciB7XHJcbiAgICAgIGJhY2tncm91bmQ6IHZhcigtLWlvbi1jb2xvci15ZWxsb3cpOy8vI0Y2Qzc2MjtcclxuICAgICAgYm9yZGVyOiBzb2xpZCAxcHg7XHJcbiAgICAgLy9ib3JkZXItY29sb3I6IHZhcigtLWlvbi1jb2xvci1wcmltYXJ5KTtcclxuICAgICAgY29sb3I6IHJnYigwLCAwLCAwKTtcclxuICAgICAgcGFkZGluZzogOHB4IDBweDtcclxuICAgICAgZm9udC1zaXplOiAxMnB4O1xyXG4gICAgICB0ZXh0LXRyYW5zZm9ybTogY2FwaXRhbGl6ZTtcclxuICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgICBjdXJzb3I6IHBvaW50ZXI7XHJcbiAgICB9XHJcbiAgICAubm90LXNlbGVjdGVkLWdlbmRlciB7XHJcbiAgICAgIHBhZGRpbmc6IDhweCAwcHg7XHJcbiAgICAgIGJvcmRlcjogc29saWQgMXB4O1xyXG4gICAgICBib3JkZXItY29sb3I6ICNjNWM1YzU7XHJcbiAgICAgIGNvbG9yOiAjZmZmZmZmO1xyXG4gICAgICBmb250LXNpemU6IDEycHg7XHJcbiAgICAgIHRleHQtdHJhbnNmb3JtOiBjYXBpdGFsaXplO1xyXG4gICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgIGN1cnNvcjogcG9pbnRlcjtcclxuICAgIH1cclxuICAgIC5lcnJvcntcclxuICAgICAgY29sb3I6ICNkMzllMDA7XHJcbiAgICAgIGZvbnQtc2l6ZTogMTJweDtcclxuICB9XHJcbiAgLmdlbmR7XHJcbiAgICAgIG1hcmdpbi1sZWZ0OiAwLjNyZW07XHJcbiAgICB9XHJcblxyXG4gICAgaDUge1xyXG4gICAgICBmb250LXdlaWdodDogbGlnaHRlciAhaW1wb3J0YW50O1xyXG4gICAgICBmb250LXNpemU6IDFlbSAhaW1wb3J0YW50O1xyXG4gICAgICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLWxpZ2h0KTtcclxuICB9XHJcblxyXG4gIC5idG4taGVpZ2h0IHtcclxuICAgIGhlaWdodDogODUlO1xyXG4gICAgYm9yZGVyLXJhZGl1czogMTBweDtcclxufVxyXG4uY29sLXBhZGQge1xyXG4gICAgcGFkZGluZzogMTVweCAyNXB4O1xyXG59Il19 */");

/***/ }),

/***/ "./src/app/niftsignup/niftsignup.page.ts":
/*!***********************************************!*\
  !*** ./src/app/niftsignup/niftsignup.page.ts ***!
  \***********************************************/
/*! exports provided: NiftsignupPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NiftsignupPage", function() { return NiftsignupPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic-native/camera/ngx */ "./node_modules/@ionic-native/camera/__ivy_ngcc__/ngx/index.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_native_ionic_webview_ngx__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic-native/ionic-webview/ngx */ "./node_modules/@ionic-native/ionic-webview/__ivy_ngcc__/ngx/index.js");
/* harmony import */ var _global_services_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../global-services.service */ "./src/app/global-services.service.ts");
/* harmony import */ var _ionic_native_file_ngx__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic-native/file/ngx */ "./node_modules/@ionic-native/file/__ivy_ngcc__/ngx/index.js");








let NiftsignupPage = class NiftsignupPage {
    constructor(camera, navCtrl, actionSheetCtrl, fb, webview, Service, file, alertController) {
        this.camera = camera;
        this.navCtrl = navCtrl;
        this.actionSheetCtrl = actionSheetCtrl;
        this.fb = fb;
        this.webview = webview;
        this.Service = Service;
        this.file = file;
        this.alertController = alertController;
        this.inValidGender = false;
        this.url = 'studRegisBase64';
        this.mobileVerifyUrl = 'getMobileNoExist';
        this.mailVerfyUrl = 'getEmailExist';
        this.genderList = [
            {
                name: 'male',
                isSelected: false
            },
            {
                name: 'female',
                isSelected: false
            },
            {
                name: 'Others',
                isSelected: false
            }
        ];
        this.validateUsernameUnique = () => {
            if (this.signupFg.get('username').valid) {
                let obj = {};
                obj.username = this.signupFg.controls['username'].value;
                this.Service.post_data("getUsrNameExist", obj)
                    .subscribe((result) => {
                    let json = this.Service.parserToJSON(result);
                    var parsedJSON = [];
                    parsedJSON = JSON.parse(json.string);
                    if (parsedJSON[0].Status != "NOTEXIST") {
                        this.usernameErrMessage = parsedJSON[0].Message;
                        this.signupFg.controls['username'].setValue(undefined);
                    }
                    else {
                        this.usernameErrMessage = undefined;
                    }
                }, (error) => {
                    console.log(error);
                });
            }
        };
        this.MatchPassword = () => {
            var passwordControl = this.signupFg.controls['pwd'].value;
            var confirmPasswordControl = this.signupFg.controls['confirmpassword'].value;
            if (passwordControl !== confirmPasswordControl) {
                this.cpwdErrMessage = "Password and Confirm Password fields should match.";
            }
            else {
                this.cpwdErrMessage = undefined;
            }
        };
        // =====================HIDE PASSWORD=====================
        this.passwordType = 'password';
        this.passwordIcon = 'eye-off';
        this.cpasswordType = 'password';
        this.cpasswordIcon = 'eye-off';
    }
    ngOnInit() {
        if (localStorage.getItem('deviceId')) {
            this.deviceId = localStorage.getItem('deviceId');
        }
        if (localStorage.getItem('platform')) {
            this.devicePlatform = localStorage.getItem('platform');
        }
        this.formInit();
    }
    // ========================================
    // =====================FORM INIT=====================
    formInit() {
        this.signupFg = this.fb.group({
            trend_spotter_type: 'OTHERS',
            stud_name: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required],
            name_of_deg: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required],
            year_of_comp: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required],
            campus: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required],
            dob_ddmmyyyy: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required],
            gender: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required],
            emailid: ['', { validators: [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$")] }],
            mobileno: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].min(1000000000), _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].max(9999999999)]],
            interests: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required],
            hobby: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required],
            address: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required],
            city: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required],
            pinCode: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required],
            designation: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required],
            company: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required],
            username: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required],
            pwd: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].pattern("^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9]).{6,15}$")]],
            confirmpassword: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].pattern("^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9]).{6,15}$")]],
            linkedin_id: [''],
            fb_id: [''],
            instagram_id: [''],
            device_id: [this.deviceId],
            device: [this.devicePlatform],
            file_ext: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required],
            contentType: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required],
            base64String: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required],
        });
    }
    // ==================
    // =====================VALIDATION=====================
    validateAllFormFields(formGroup) {
        Object.keys(formGroup.controls).forEach(field => {
            const control = formGroup.get(field);
            if (control instanceof _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"]) {
                if (!control.valid) {
                    control.markAsTouched({ onlySelf: true });
                }
            }
            else if (control instanceof _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormGroup"]) {
                this.validateAllFormFields(control);
            }
        });
    }
    selectGender(gender) {
        this.genderList.forEach(g => {
            if (g.name === gender.name) {
                g.isSelected = !g.isSelected;
                this.signupFg.get('gender').patchValue(g.name);
                this.inValidGender = false;
            }
            else {
                g.isSelected = false;
            }
        });
    }
    mobileNumberVerification() {
        if (this.signupFg.get('mobileno').valid) {
            this.mobilenoErrMessage = '';
            const obj = {
                mobno: this.signupFg.get('mobileno').value
            };
            this.Service.post_data(this.mobileVerifyUrl, obj).subscribe((result) => {
                let json = this.Service.parserToJSON(result);
                var parsedJSON = JSON.parse(json.string);
                if (parsedJSON[0].Status != "NOTEXIST") {
                    this.mobilenoErrMessage = parsedJSON[0].Message;
                    this.signupFg.controls['mobileno'].setValue(undefined);
                }
                else {
                    this.mobilenoErrMessage = undefined;
                }
            });
        }
    }
    mailVerification() {
        if (this.signupFg.get('emailid').valid) {
            this.emailidErrMessage = '';
            const obj = {
                emailid: this.signupFg.get('emailid').value
            };
            this.Service.post_data(this.mailVerfyUrl, obj).subscribe((result) => {
                let json = this.Service.parserToJSON(result);
                var parsedJSON = JSON.parse(json.string);
                if (parsedJSON[0].Status != "NOTEXIST") {
                    this.emailidErrMessage = parsedJSON[0].Message;
                    this.signupFg.controls['emailid'].setValue(undefined);
                }
                else {
                    this.emailidErrMessage = undefined;
                }
            });
        }
    }
    // =====================VALIDATION END=====================
    // =====================UPLOAD PROFILE IMAGE=====================
    uploadImage() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            let actionSheet = yield this.actionSheetCtrl.create({
                buttons: [
                    {
                        text: 'Take photo',
                        role: 'destructive',
                        icon: 'camera',
                        handler: () => {
                            this.captureImage('camera');
                        }
                    },
                    {
                        text: 'Choose photo from Gallery',
                        icon: 'images',
                        handler: () => {
                            this.captureImage('gallery');
                        }
                    },
                ]
            });
            actionSheet.present();
        });
    }
    captureImage(data) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const options = {
                quality: 50,
                destinationType: this.camera.DestinationType.DATA_URL,
                encodingType: this.camera.EncodingType.JPEG,
                mediaType: this.camera.MediaType.PICTURE,
                correctOrientation: true
            };
            if (data == 'camera') {
                options.sourceType = this.camera.PictureSourceType.CAMERA;
                this.camera.getPicture(options).then((imageData) => {
                    // this.file.resolveLocalFilesystemUrl(imageData).then(fileEntry => {
                    //   fileEntry.getMetadata((metadata) => {
                    //     let fileSize = metadata.size / 1024 / 1024;
                    //     if (fileSize <= 8) {
                    this.imageUrlPath = imageData;
                    this.currentImage = 'data:image/jpeg;base64,' + imageData;
                    this.uploadFile(imageData);
                    //     }
                    //     else {
                    //       alert('file too big');
                    //     }
                    //   });
                    // });
                }, (err) => {
                    this.service.create(err, 'top', 'error');
                    // Handle error
                });
            }
            else if (data == 'gallery') {
                options.sourceType = this.camera.PictureSourceType.PHOTOLIBRARY;
                this.camera.getPicture(options).then((imageData) => {
                    // this.file.resolveLocalFilesystemUrl(imageData).then(fileEntry => {
                    //   fileEntry.getMetadata((metadata) => {
                    //     let fileSize = metadata.size / 1024 / 1024;
                    //     if (fileSize <= 8) {
                    this.imageUrlPath = imageData;
                    this.currentImage = 'data:image/jpeg;base64,' + imageData;
                    this.uploadFile(imageData);
                    //     }
                    //     else {
                    //       alert('file too big');
                    //     }
                    //   });
                    // });
                }, (err) => {
                    // Handle error
                    this.service.create(err, 'top', 'error');
                });
            }
        });
    }
    uploadFile(imageData) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.signupFg.controls['base64String'].setValue(imageData);
            this.signupFg.controls['contentType'].setValue("image/jpeg");
            this.signupFg.controls['file_ext'].setValue("jpeg");
        });
    }
    // =========================UPLOAD PROFILE IMAGE END====================================
    // =====================SIGN IN=====================
    signupFunction() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const data = this.signupFg.value;
            data[`gender`] = this.genderList.filter(gender => gender.isSelected).map(g => g.name)[0];
            if (data.gender != null) {
                if (this.signupFg.valid) {
                    this.signupFg.removeControl('');
                    this.inValidGender = false;
                    let obj = {};
                    obj.mobileno = this.signupFg.controls['mobileno'].value;
                    this.Service.post_data_otp("sendOtpViaSMS", obj)
                        .subscribe((result) => {
                        let json = this.Service.parserToJSON(result);
                        var parsedJSON = JSON.parse(json.string);
                        this.navCtrl.navigateRoot(['/otp', { 'otp': parsedJSON[0]['OTP'], 'mobileno': obj.mobileno, 'data': JSON.stringify(data), 'page': 'signup' }]);
                    }, (error) => {
                        console.log(error);
                    });
                }
                else if (!this.signupFg.get('base64String').valid || !this.signupFg.get('file_ext').valid || !this.signupFg.get('contentType').valid) {
                    this.alertMessage('Upload Image to Proceed Further');
                }
                else {
                    this.validateAllFormFields(this.signupFg);
                }
            }
            else {
                this.inValidGender = true;
            }
        });
    }
    hideShowPassword() {
        this.passwordType = this.passwordType === 'text' ? 'password' : 'text';
        this.passwordIcon = this.passwordIcon === 'eye-off' ? 'eye' : 'eye-off';
    }
    hideShowcPassword() {
        this.cpasswordType = this.cpasswordType === 'text' ? 'password' : 'text';
        this.cpasswordIcon = this.cpasswordIcon === 'eye-off' ? 'eye' : 'eye-off';
    }
    // =====================end of HIDE PASSWORD=====================
    alertMessage(message) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const alert = yield this.alertController.create({
                cssClass: 'alert-custom-class',
                message: message,
                buttons: [{
                        text: 'OK',
                        cssClass: 'warning',
                    },]
            });
            yield alert.present();
        });
    }
};
NiftsignupPage.ctorParameters = () => [
    { type: _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_2__["Camera"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ActionSheetController"] },
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormBuilder"] },
    { type: _ionic_native_ionic_webview_ngx__WEBPACK_IMPORTED_MODULE_5__["WebView"] },
    { type: _global_services_service__WEBPACK_IMPORTED_MODULE_6__["GlobalServicesService"] },
    { type: _ionic_native_file_ngx__WEBPACK_IMPORTED_MODULE_7__["File"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["AlertController"] }
];
NiftsignupPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-niftsignup',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./niftsignup.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/niftsignup/niftsignup.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./niftsignup.page.scss */ "./src/app/niftsignup/niftsignup.page.scss")).default]
    })
], NiftsignupPage);



/***/ })

}]);
//# sourceMappingURL=niftsignup-niftsignup-module-es2015.js.map