(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass2(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck2(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["preview-filter-preview-filter-module"], {
    /***/
    "./node_modules/glfx-es6/es/filters/adjust/brightnesscontrast.js":
    /*!***********************************************************************!*\
      !*** ./node_modules/glfx-es6/es/filters/adjust/brightnesscontrast.js ***!
      \***********************************************************************/

    /*! no static exports found */

    /***/
    function node_modulesGlfxEs6EsFiltersAdjustBrightnesscontrastJs(module, exports, __webpack_require__) {
      "use strict";

      Object.defineProperty(exports, "__esModule", {
        value: true
      });

      exports["default"] = function (brightness, contrast) {
        var gl = store.get('gl');
        gl.brightnessContrast = gl.brightnessContrast || new _shader2["default"](null, '\
    uniform sampler2D texture;\
    uniform float brightness;\
    uniform float contrast;\
    varying vec2 texCoord;\
    void main() {\
      vec4 color = texture2D(texture, texCoord);\
      color.rgb += brightness;\
      if (contrast > 0.0) {\
        color.rgb = (color.rgb - 0.5) / (1.0 - contrast) + 0.5;\
      } else {\
        color.rgb = (color.rgb - 0.5) * (1.0 + contrast) + 0.5;\
      }\
      gl_FragColor = color;\
    }\
  ');

        _util.simpleShader.call(this, gl.brightnessContrast, {
          brightness: (0, _util.clamp)(-1, brightness, 1),
          contrast: (0, _util.clamp)(-1, contrast, 1)
        });

        return this;
      };

      var _shader = __webpack_require__(
      /*! ../../shader */
      "./node_modules/glfx-es6/es/shader.js");

      var _shader2 = _interopRequireDefault(_shader);

      var _util = __webpack_require__(
      /*! ../../util */
      "./node_modules/glfx-es6/es/util.js");

      var _store = __webpack_require__(
      /*! ../../store */
      "./node_modules/glfx-es6/es/store.js");

      var store = _interopRequireWildcard(_store);

      function _interopRequireWildcard(obj) {
        if (obj && obj.__esModule) {
          return obj;
        } else {
          var newObj = {};

          if (obj != null) {
            for (var key in obj) {
              if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key];
            }
          }

          newObj["default"] = obj;
          return newObj;
        }
      }

      function _interopRequireDefault(obj) {
        return obj && obj.__esModule ? obj : {
          "default": obj
        };
      }
      /***/

    },

    /***/
    "./node_modules/glfx-es6/es/filters/adjust/curves.js":
    /*!***********************************************************!*\
      !*** ./node_modules/glfx-es6/es/filters/adjust/curves.js ***!
      \***********************************************************/

    /*! no static exports found */

    /***/
    function node_modulesGlfxEs6EsFiltersAdjustCurvesJs(module, exports, __webpack_require__) {
      "use strict";

      Object.defineProperty(exports, "__esModule", {
        value: true
      });

      exports["default"] = function (red, green, blue) {
        var gl = store.get('gl'); // Create the ramp texture

        red = (0, _util.splineInterpolate)(red);

        if (arguments.length == 1) {
          green = blue = red;
        } else {
          green = (0, _util.splineInterpolate)(green);
          blue = (0, _util.splineInterpolate)(blue);
        }

        var array = [];

        for (var i = 0; i < 256; i++) {
          array.splice(array.length, 0, red[i], green[i], blue[i], 255);
        }

        this._.extraTexture.initFromBytes(256, 1, array);

        this._.extraTexture.use(1);

        gl.curves = gl.curves || new _shader2["default"](null, '\
    uniform sampler2D texture;\
    uniform sampler2D map;\
    varying vec2 texCoord;\
    void main() {\
      vec4 color = texture2D(texture, texCoord);\
      color.r = texture2D(map, vec2(color.r)).r;\
      color.g = texture2D(map, vec2(color.g)).g;\
      color.b = texture2D(map, vec2(color.b)).b;\
      gl_FragColor = color;\
    }\
  ');
        gl.curves.textures({
          map: 1
        });

        _util.simpleShader.call(this, gl.curves, {});

        return this;
      };

      var _shader = __webpack_require__(
      /*! ../../shader */
      "./node_modules/glfx-es6/es/shader.js");

      var _shader2 = _interopRequireDefault(_shader);

      var _util = __webpack_require__(
      /*! ../../util */
      "./node_modules/glfx-es6/es/util.js");

      var _store = __webpack_require__(
      /*! ../../store */
      "./node_modules/glfx-es6/es/store.js");

      var store = _interopRequireWildcard(_store);

      function _interopRequireWildcard(obj) {
        if (obj && obj.__esModule) {
          return obj;
        } else {
          var newObj = {};

          if (obj != null) {
            for (var key in obj) {
              if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key];
            }
          }

          newObj["default"] = obj;
          return newObj;
        }
      }

      function _interopRequireDefault(obj) {
        return obj && obj.__esModule ? obj : {
          "default": obj
        };
      }
      /***/

    },

    /***/
    "./node_modules/glfx-es6/es/filters/adjust/denoise.js":
    /*!************************************************************!*\
      !*** ./node_modules/glfx-es6/es/filters/adjust/denoise.js ***!
      \************************************************************/

    /*! no static exports found */

    /***/
    function node_modulesGlfxEs6EsFiltersAdjustDenoiseJs(module, exports, __webpack_require__) {
      "use strict";

      Object.defineProperty(exports, "__esModule", {
        value: true
      });

      exports["default"] = function (exponent) {
        var gl = store.get('gl'); // Do a 9x9 bilateral box filter

        gl.denoise = gl.denoise || new _shader2["default"](null, '\
    uniform sampler2D texture;\
    uniform float exponent;\
    uniform float strength;\
    uniform vec2 texSize;\
    varying vec2 texCoord;\
    void main() {\
      vec4 center = texture2D(texture, texCoord);\
      vec4 color = vec4(0.0);\
      float total = 0.0;\
      for (float x = -4.0; x <= 4.0; x += 1.0) {\
        for (float y = -4.0; y <= 4.0; y += 1.0) {\
          vec4 sample = texture2D(texture, texCoord + vec2(x, y) / texSize);\
          float weight = 1.0 - abs(dot(sample.rgb - center.rgb, vec3(0.25)));\
          weight = pow(weight, exponent);\
          color += sample * weight;\
          total += weight;\
        }\
      }\
      gl_FragColor = color / total;\
    }\
  '); // Perform two iterations for stronger results

        for (var i = 0; i < 2; i++) {
          _util.simpleShader.call(this, gl.denoise, {
            exponent: Math.max(0, exponent),
            texSize: [this.width, this.height]
          });
        }

        return this;
      };

      var _shader = __webpack_require__(
      /*! ../../shader */
      "./node_modules/glfx-es6/es/shader.js");

      var _shader2 = _interopRequireDefault(_shader);

      var _util = __webpack_require__(
      /*! ../../util */
      "./node_modules/glfx-es6/es/util.js");

      var _store = __webpack_require__(
      /*! ../../store */
      "./node_modules/glfx-es6/es/store.js");

      var store = _interopRequireWildcard(_store);

      function _interopRequireWildcard(obj) {
        if (obj && obj.__esModule) {
          return obj;
        } else {
          var newObj = {};

          if (obj != null) {
            for (var key in obj) {
              if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key];
            }
          }

          newObj["default"] = obj;
          return newObj;
        }
      }

      function _interopRequireDefault(obj) {
        return obj && obj.__esModule ? obj : {
          "default": obj
        };
      }
      /***/

    },

    /***/
    "./node_modules/glfx-es6/es/filters/adjust/huesaturation.js":
    /*!******************************************************************!*\
      !*** ./node_modules/glfx-es6/es/filters/adjust/huesaturation.js ***!
      \******************************************************************/

    /*! no static exports found */

    /***/
    function node_modulesGlfxEs6EsFiltersAdjustHuesaturationJs(module, exports, __webpack_require__) {
      "use strict";

      Object.defineProperty(exports, "__esModule", {
        value: true
      });

      exports["default"] = function (hue, saturation) {
        var gl = store.get('gl');
        gl.hueSaturation = gl.hueSaturation || new _shader2["default"](null, '\
    uniform sampler2D texture;\
    uniform float hue;\
    uniform float saturation;\
    varying vec2 texCoord;\
    void main() {\
      vec4 color = texture2D(texture, texCoord);\
      \
      /* hue adjustment, wolfram alpha: RotationTransform[angle, {1, 1, 1}][{x, y, z}] */\
      float angle = hue * 3.14159265;\
      float s = sin(angle), c = cos(angle);\
      vec3 weights = (vec3(2.0 * c, -sqrt(3.0) * s - c, sqrt(3.0) * s - c) + 1.0) / 3.0;\
      float len = length(color.rgb);\
      color.rgb = vec3(\
        dot(color.rgb, weights.xyz),\
        dot(color.rgb, weights.zxy),\
        dot(color.rgb, weights.yzx)\
      );\
      \
      /* saturation adjustment */\
      float average = (color.r + color.g + color.b) / 3.0;\
      if (saturation > 0.0) {\
        color.rgb += (average - color.rgb) * (1.0 - 1.0 / (1.001 - saturation));\
      } else {\
        color.rgb += (average - color.rgb) * (-saturation);\
      }\
      \
      gl_FragColor = color;\
    }\
  ');

        _util.simpleShader.call(this, gl.hueSaturation, {
          hue: (0, _util.clamp)(-1, hue, 1),
          saturation: (0, _util.clamp)(-1, saturation, 1)
        });

        return this;
      };

      var _shader = __webpack_require__(
      /*! ../../shader */
      "./node_modules/glfx-es6/es/shader.js");

      var _shader2 = _interopRequireDefault(_shader);

      var _util = __webpack_require__(
      /*! ../../util */
      "./node_modules/glfx-es6/es/util.js");

      var _store = __webpack_require__(
      /*! ../../store */
      "./node_modules/glfx-es6/es/store.js");

      var store = _interopRequireWildcard(_store);

      function _interopRequireWildcard(obj) {
        if (obj && obj.__esModule) {
          return obj;
        } else {
          var newObj = {};

          if (obj != null) {
            for (var key in obj) {
              if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key];
            }
          }

          newObj["default"] = obj;
          return newObj;
        }
      }

      function _interopRequireDefault(obj) {
        return obj && obj.__esModule ? obj : {
          "default": obj
        };
      }
      /***/

    },

    /***/
    "./node_modules/glfx-es6/es/filters/adjust/noise.js":
    /*!**********************************************************!*\
      !*** ./node_modules/glfx-es6/es/filters/adjust/noise.js ***!
      \**********************************************************/

    /*! no static exports found */

    /***/
    function node_modulesGlfxEs6EsFiltersAdjustNoiseJs(module, exports, __webpack_require__) {
      "use strict";

      Object.defineProperty(exports, "__esModule", {
        value: true
      });

      exports["default"] = function (amount) {
        var gl = store.get('gl');
        gl.noise = gl.noise || new _shader2["default"](null, '\
    uniform sampler2D texture;\
    uniform float amount;\
    varying vec2 texCoord;\
    float rand(vec2 co) {\
      return fract(sin(dot(co.xy ,vec2(12.9898,78.233))) * 43758.5453);\
    }\
    void main() {\
      vec4 color = texture2D(texture, texCoord);\
      \
      float diff = (rand(texCoord) - 0.5) * amount;\
      color.r += diff;\
      color.g += diff;\
      color.b += diff;\
      \
      gl_FragColor = color;\
    }\
  ');

        _util.simpleShader.call(this, gl.noise, {
          amount: (0, _util.clamp)(0, amount, 1)
        });

        return this;
      };

      var _shader = __webpack_require__(
      /*! ../../shader */
      "./node_modules/glfx-es6/es/shader.js");

      var _shader2 = _interopRequireDefault(_shader);

      var _util = __webpack_require__(
      /*! ../../util */
      "./node_modules/glfx-es6/es/util.js");

      var _store = __webpack_require__(
      /*! ../../store */
      "./node_modules/glfx-es6/es/store.js");

      var store = _interopRequireWildcard(_store);

      function _interopRequireWildcard(obj) {
        if (obj && obj.__esModule) {
          return obj;
        } else {
          var newObj = {};

          if (obj != null) {
            for (var key in obj) {
              if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key];
            }
          }

          newObj["default"] = obj;
          return newObj;
        }
      }

      function _interopRequireDefault(obj) {
        return obj && obj.__esModule ? obj : {
          "default": obj
        };
      }
      /***/

    },

    /***/
    "./node_modules/glfx-es6/es/filters/adjust/sepia.js":
    /*!**********************************************************!*\
      !*** ./node_modules/glfx-es6/es/filters/adjust/sepia.js ***!
      \**********************************************************/

    /*! no static exports found */

    /***/
    function node_modulesGlfxEs6EsFiltersAdjustSepiaJs(module, exports, __webpack_require__) {
      "use strict";

      Object.defineProperty(exports, "__esModule", {
        value: true
      });

      exports["default"] = function (amount) {
        var gl = store.get('gl');
        gl.sepia = gl.sepia || new _shader2["default"](null, '\
    uniform sampler2D texture;\
    uniform float amount;\
    varying vec2 texCoord;\
    void main() {\
      vec4 color = texture2D(texture, texCoord);\
      float r = color.r;\
      float g = color.g;\
      float b = color.b;\
      \
      color.r = min(1.0, (r * (1.0 - (0.607 * amount))) + (g * (0.769 * amount)) + (b * (0.189 * amount)));\
      color.g = min(1.0, (r * 0.349 * amount) + (g * (1.0 - (0.314 * amount))) + (b * 0.168 * amount));\
      color.b = min(1.0, (r * 0.272 * amount) + (g * 0.534 * amount) + (b * (1.0 - (0.869 * amount))));\
      \
      gl_FragColor = color;\
    }\
  ');

        _util.simpleShader.call(this, gl.sepia, {
          amount: (0, _util.clamp)(0, amount, 1)
        });

        return this;
      };

      var _shader = __webpack_require__(
      /*! ../../shader */
      "./node_modules/glfx-es6/es/shader.js");

      var _shader2 = _interopRequireDefault(_shader);

      var _util = __webpack_require__(
      /*! ../../util */
      "./node_modules/glfx-es6/es/util.js");

      var _store = __webpack_require__(
      /*! ../../store */
      "./node_modules/glfx-es6/es/store.js");

      var store = _interopRequireWildcard(_store);

      function _interopRequireWildcard(obj) {
        if (obj && obj.__esModule) {
          return obj;
        } else {
          var newObj = {};

          if (obj != null) {
            for (var key in obj) {
              if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key];
            }
          }

          newObj["default"] = obj;
          return newObj;
        }
      }

      function _interopRequireDefault(obj) {
        return obj && obj.__esModule ? obj : {
          "default": obj
        };
      }
      /***/

    },

    /***/
    "./node_modules/glfx-es6/es/filters/adjust/unsharpmask.js":
    /*!****************************************************************!*\
      !*** ./node_modules/glfx-es6/es/filters/adjust/unsharpmask.js ***!
      \****************************************************************/

    /*! no static exports found */

    /***/
    function node_modulesGlfxEs6EsFiltersAdjustUnsharpmaskJs(module, exports, __webpack_require__) {
      "use strict";

      Object.defineProperty(exports, "__esModule", {
        value: true
      });

      exports["default"] = function (radius, strength) {
        var gl = store.get('gl');
        gl.unsharpMask = gl.unsharpMask || new _shader2["default"](null, '\
    uniform sampler2D blurredTexture;\
    uniform sampler2D originalTexture;\
    uniform float strength;\
    uniform float threshold;\
    varying vec2 texCoord;\
    void main() {\
      vec4 blurred = texture2D(blurredTexture, texCoord);\
      vec4 original = texture2D(originalTexture, texCoord);\
      gl_FragColor = mix(blurred, original, 1.0 + strength);\
    }\
  '); // Store a copy of the current texture in the second texture unit

        this._.extraTexture.ensureFormat(this._.texture);

        this._.texture.use();

        this._.extraTexture.drawTo(function () {
          _shader2["default"].getDefaultShader().drawRect();
        }); // Blur the current texture, then use the stored texture to detect edges


        this._.extraTexture.use(1);

        this.triangleBlur(radius);
        gl.unsharpMask.textures({
          originalTexture: 1
        });

        _util.simpleShader.call(this, gl.unsharpMask, {
          strength: strength
        });

        this._.extraTexture.unuse(1);

        return this;
      };

      var _shader = __webpack_require__(
      /*! ../../shader */
      "./node_modules/glfx-es6/es/shader.js");

      var _shader2 = _interopRequireDefault(_shader);

      var _util = __webpack_require__(
      /*! ../../util */
      "./node_modules/glfx-es6/es/util.js");

      var _store = __webpack_require__(
      /*! ../../store */
      "./node_modules/glfx-es6/es/store.js");

      var store = _interopRequireWildcard(_store);

      function _interopRequireWildcard(obj) {
        if (obj && obj.__esModule) {
          return obj;
        } else {
          var newObj = {};

          if (obj != null) {
            for (var key in obj) {
              if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key];
            }
          }

          newObj["default"] = obj;
          return newObj;
        }
      }

      function _interopRequireDefault(obj) {
        return obj && obj.__esModule ? obj : {
          "default": obj
        };
      }
      /***/

    },

    /***/
    "./node_modules/glfx-es6/es/filters/adjust/vibrance.js":
    /*!*************************************************************!*\
      !*** ./node_modules/glfx-es6/es/filters/adjust/vibrance.js ***!
      \*************************************************************/

    /*! no static exports found */

    /***/
    function node_modulesGlfxEs6EsFiltersAdjustVibranceJs(module, exports, __webpack_require__) {
      "use strict";

      Object.defineProperty(exports, "__esModule", {
        value: true
      });

      exports["default"] = function (amount) {
        var gl = store.get('gl');
        gl.vibrance = gl.vibrance || new _shader2["default"](null, '\
    uniform sampler2D texture;\
    uniform float amount;\
    varying vec2 texCoord;\
    void main() {\
      vec4 color = texture2D(texture, texCoord);\
      float average = (color.r + color.g + color.b) / 3.0;\
      float mx = max(color.r, max(color.g, color.b));\
      float amt = (mx - average) * (-amount * 3.0);\
      color.rgb = mix(color.rgb, vec3(mx), amt);\
      gl_FragColor = color;\
    }\
  ');

        _util.simpleShader.call(this, gl.vibrance, {
          amount: (0, _util.clamp)(-1, amount, 1)
        });

        return this;
      };

      var _shader = __webpack_require__(
      /*! ../../shader */
      "./node_modules/glfx-es6/es/shader.js");

      var _shader2 = _interopRequireDefault(_shader);

      var _util = __webpack_require__(
      /*! ../../util */
      "./node_modules/glfx-es6/es/util.js");

      var _store = __webpack_require__(
      /*! ../../store */
      "./node_modules/glfx-es6/es/store.js");

      var store = _interopRequireWildcard(_store);

      function _interopRequireWildcard(obj) {
        if (obj && obj.__esModule) {
          return obj;
        } else {
          var newObj = {};

          if (obj != null) {
            for (var key in obj) {
              if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key];
            }
          }

          newObj["default"] = obj;
          return newObj;
        }
      }

      function _interopRequireDefault(obj) {
        return obj && obj.__esModule ? obj : {
          "default": obj
        };
      }
      /***/

    },

    /***/
    "./node_modules/glfx-es6/es/filters/adjust/vignette.js":
    /*!*************************************************************!*\
      !*** ./node_modules/glfx-es6/es/filters/adjust/vignette.js ***!
      \*************************************************************/

    /*! no static exports found */

    /***/
    function node_modulesGlfxEs6EsFiltersAdjustVignetteJs(module, exports, __webpack_require__) {
      "use strict";

      Object.defineProperty(exports, "__esModule", {
        value: true
      });

      exports["default"] = function (size, amount) {
        var gl = store.get('gl');
        gl.vignette = gl.vignette || new _shader2["default"](null, '\
    uniform sampler2D texture;\
    uniform float size;\
    uniform float amount;\
    varying vec2 texCoord;\
    void main() {\
      vec4 color = texture2D(texture, texCoord);\
      \
      float dist = distance(texCoord, vec2(0.5, 0.5));\
      color.rgb *= smoothstep(0.8, size * 0.799, dist * (amount + size));\
      \
      gl_FragColor = color;\
    }\
  ');

        _util.simpleShader.call(this, gl.vignette, {
          size: (0, _util.clamp)(0, size, 1),
          amount: (0, _util.clamp)(0, amount, 1)
        });

        return this;
      };

      var _shader = __webpack_require__(
      /*! ../../shader */
      "./node_modules/glfx-es6/es/shader.js");

      var _shader2 = _interopRequireDefault(_shader);

      var _util = __webpack_require__(
      /*! ../../util */
      "./node_modules/glfx-es6/es/util.js");

      var _store = __webpack_require__(
      /*! ../../store */
      "./node_modules/glfx-es6/es/store.js");

      var store = _interopRequireWildcard(_store);

      function _interopRequireWildcard(obj) {
        if (obj && obj.__esModule) {
          return obj;
        } else {
          var newObj = {};

          if (obj != null) {
            for (var key in obj) {
              if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key];
            }
          }

          newObj["default"] = obj;
          return newObj;
        }
      }

      function _interopRequireDefault(obj) {
        return obj && obj.__esModule ? obj : {
          "default": obj
        };
      }
      /***/

    },

    /***/
    "./node_modules/glfx-es6/es/filters/blur/lensblur.js":
    /*!***********************************************************!*\
      !*** ./node_modules/glfx-es6/es/filters/blur/lensblur.js ***!
      \***********************************************************/

    /*! no static exports found */

    /***/
    function node_modulesGlfxEs6EsFiltersBlurLensblurJs(module, exports, __webpack_require__) {
      "use strict";

      Object.defineProperty(exports, "__esModule", {
        value: true
      });

      exports["default"] = function (radius, brightness, angle) {
        var gl = store.get('gl'); // All averaging is done on values raised to a power to make more obvious bokeh
        // (we will raise the average to the inverse power at the end to compensate).
        // Without this the image looks almost like a normal blurred image. This hack is
        // obviously not realistic, but to accurately simulate this we would need a high
        // dynamic range source photograph which we don't have.

        gl.lensBlurPrePass = gl.lensBlurPrePass || new _shader2["default"](null, '\
    uniform sampler2D texture;\
    uniform float power;\
    varying vec2 texCoord;\
    void main() {\
      vec4 color = texture2D(texture, texCoord);\
      color = pow(color, vec4(power));\
      gl_FragColor = vec4(color);\
    }\
  ');
        var common = '\
    uniform sampler2D texture0;\
    uniform sampler2D texture1;\
    uniform vec2 delta0;\
    uniform vec2 delta1;\
    uniform float power;\
    varying vec2 texCoord;\
    ' + _common.randomShaderFunc + '\
    vec4 sample(vec2 delta) {\
      /* randomize the lookup values to hide the fixed number of samples */\
      float offset = random(vec3(delta, 151.7182), 0.0);\
      \
      vec4 color = vec4(0.0);\
      float total = 0.0;\
      for (float t = 0.0; t <= 30.0; t++) {\
        float percent = (t + offset) / 30.0;\
        color += texture2D(texture0, texCoord + delta * percent);\
        total += 1.0;\
      }\
      return color / total;\
    }\
  ';
        gl.lensBlur0 = gl.lensBlur0 || new _shader2["default"](null, common + '\
    void main() {\
      gl_FragColor = sample(delta0);\
    }\
  ');
        gl.lensBlur1 = gl.lensBlur1 || new _shader2["default"](null, common + '\
    void main() {\
      gl_FragColor = (sample(delta0) + sample(delta1)) * 0.5;\
    }\
  ');
        gl.lensBlur2 = gl.lensBlur2 || new _shader2["default"](null, common + '\
    void main() {\
      vec4 color = (sample(delta0) + 2.0 * texture2D(texture1, texCoord)) / 3.0;\
      gl_FragColor = pow(color, vec4(power));\
    }\
  ').textures({
          texture1: 1
        }); // Generate

        var dir = [];

        for (var i = 0; i < 3; i++) {
          var a = angle + i * Math.PI * 2 / 3;
          dir.push([radius * Math.sin(a) / this.width, radius * Math.cos(a) / this.height]);
        }

        var power = Math.pow(10, (0, _util.clamp)(-1, brightness, 1)); // Remap the texture values, which will help make the bokeh effect

        _util.simpleShader.call(this, gl.lensBlurPrePass, {
          power: power
        }); // Blur two rhombi in parallel into extraTexture


        this._.extraTexture.ensureFormat(this._.texture);

        _util.simpleShader.call(this, gl.lensBlur0, {
          delta0: dir[0]
        }, this._.texture, this._.extraTexture);

        _util.simpleShader.call(this, gl.lensBlur1, {
          delta0: dir[1],
          delta1: dir[2]
        }, this._.extraTexture, this._.extraTexture); // Blur the last rhombus and combine with extraTexture


        _util.simpleShader.call(this, gl.lensBlur0, {
          delta0: dir[1]
        });

        this._.extraTexture.use(1);

        _util.simpleShader.call(this, gl.lensBlur2, {
          power: 1 / power,
          delta0: dir[2]
        });

        return this;
      };

      var _shader = __webpack_require__(
      /*! ../../shader */
      "./node_modules/glfx-es6/es/shader.js");

      var _shader2 = _interopRequireDefault(_shader);

      var _util = __webpack_require__(
      /*! ../../util */
      "./node_modules/glfx-es6/es/util.js");

      var _common = __webpack_require__(
      /*! ../common */
      "./node_modules/glfx-es6/es/filters/common.js");

      var _store = __webpack_require__(
      /*! ../../store */
      "./node_modules/glfx-es6/es/store.js");

      var store = _interopRequireWildcard(_store);

      function _interopRequireWildcard(obj) {
        if (obj && obj.__esModule) {
          return obj;
        } else {
          var newObj = {};

          if (obj != null) {
            for (var key in obj) {
              if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key];
            }
          }

          newObj["default"] = obj;
          return newObj;
        }
      }

      function _interopRequireDefault(obj) {
        return obj && obj.__esModule ? obj : {
          "default": obj
        };
      }
      /***/

    },

    /***/
    "./node_modules/glfx-es6/es/filters/blur/tiltshift.js":
    /*!************************************************************!*\
      !*** ./node_modules/glfx-es6/es/filters/blur/tiltshift.js ***!
      \************************************************************/

    /*! no static exports found */

    /***/
    function node_modulesGlfxEs6EsFiltersBlurTiltshiftJs(module, exports, __webpack_require__) {
      "use strict";

      Object.defineProperty(exports, "__esModule", {
        value: true
      });

      exports["default"] = function (startX, startY, endX, endY, blurRadius, gradientRadius) {
        var gl = store.get('gl');
        gl.tiltShift = gl.tiltShift || new _shader2["default"](null, '\
    uniform sampler2D texture;\
    uniform float blurRadius;\
    uniform float gradientRadius;\
    uniform vec2 start;\
    uniform vec2 end;\
    uniform vec2 delta;\
    uniform vec2 texSize;\
    varying vec2 texCoord;\
    ' + _common.randomShaderFunc + '\
    void main() {\
      vec4 color = vec4(0.0);\
      float total = 0.0;\
      \
      /* randomize the lookup values to hide the fixed number of samples */\
      float offset = random(vec3(12.9898, 78.233, 151.7182), 0.0);\
      \
      vec2 normal = normalize(vec2(start.y - end.y, end.x - start.x));\
      float radius = smoothstep(0.0, 1.0, abs(dot(texCoord * texSize - start, normal)) / gradientRadius) * blurRadius;\
      for (float t = -30.0; t <= 30.0; t++) {\
        float percent = (t + offset - 0.5) / 30.0;\
        float weight = 1.0 - abs(percent);\
        vec4 sample = texture2D(texture, texCoord + delta / texSize * percent * radius);\
        \
        /* switch to pre-multiplied alpha to correctly blur transparent images */\
        sample.rgb *= sample.a;\
        \
        color += sample * weight;\
        total += weight;\
      }\
      \
      gl_FragColor = color / total;\
      \
      /* switch back from pre-multiplied alpha */\
      gl_FragColor.rgb /= gl_FragColor.a + 0.00001;\
    }\
  ');
        var dx = endX - startX;
        var dy = endY - startY;
        var d = Math.sqrt(dx * dx + dy * dy);

        _util.simpleShader.call(this, gl.tiltShift, {
          blurRadius: blurRadius,
          gradientRadius: gradientRadius,
          start: [startX, startY],
          end: [endX, endY],
          delta: [dx / d, dy / d],
          texSize: [this.width, this.height]
        });

        _util.simpleShader.call(this, gl.tiltShift, {
          blurRadius: blurRadius,
          gradientRadius: gradientRadius,
          start: [startX, startY],
          end: [endX, endY],
          delta: [-dy / d, dx / d],
          texSize: [this.width, this.height]
        });

        return this;
      };

      var _shader = __webpack_require__(
      /*! ../../shader */
      "./node_modules/glfx-es6/es/shader.js");

      var _shader2 = _interopRequireDefault(_shader);

      var _util = __webpack_require__(
      /*! ../../util */
      "./node_modules/glfx-es6/es/util.js");

      var _common = __webpack_require__(
      /*! ../common */
      "./node_modules/glfx-es6/es/filters/common.js");

      var _store = __webpack_require__(
      /*! ../../store */
      "./node_modules/glfx-es6/es/store.js");

      var store = _interopRequireWildcard(_store);

      function _interopRequireWildcard(obj) {
        if (obj && obj.__esModule) {
          return obj;
        } else {
          var newObj = {};

          if (obj != null) {
            for (var key in obj) {
              if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key];
            }
          }

          newObj["default"] = obj;
          return newObj;
        }
      }

      function _interopRequireDefault(obj) {
        return obj && obj.__esModule ? obj : {
          "default": obj
        };
      }
      /***/

    },

    /***/
    "./node_modules/glfx-es6/es/filters/blur/triangleblur.js":
    /*!***************************************************************!*\
      !*** ./node_modules/glfx-es6/es/filters/blur/triangleblur.js ***!
      \***************************************************************/

    /*! no static exports found */

    /***/
    function node_modulesGlfxEs6EsFiltersBlurTriangleblurJs(module, exports, __webpack_require__) {
      "use strict";

      Object.defineProperty(exports, "__esModule", {
        value: true
      });

      exports["default"] = function (radius) {
        var gl = store.get('gl');
        gl.triangleBlur = gl.triangleBlur || new _shader2["default"](null, '\
    uniform sampler2D texture;\
    uniform vec2 delta;\
    varying vec2 texCoord;\
    ' + _common.randomShaderFunc + '\
    void main() {\
      vec4 color = vec4(0.0);\
      float total = 0.0;\
      \
      /* randomize the lookup values to hide the fixed number of samples */\
      float offset = random(vec3(12.9898, 78.233, 151.7182), 0.0);\
      \
      for (float t = -30.0; t <= 30.0; t++) {\
        float percent = (t + offset - 0.5) / 30.0;\
        float weight = 1.0 - abs(percent);\
        vec4 sample = texture2D(texture, texCoord + delta * percent);\
        \
        /* switch to pre-multiplied alpha to correctly blur transparent images */\
        sample.rgb *= sample.a;\
        \
        color += sample * weight;\
        total += weight;\
      }\
      \
      gl_FragColor = color / total;\
      \
      /* switch back from pre-multiplied alpha */\
      gl_FragColor.rgb /= gl_FragColor.a + 0.00001;\
    }\
  ');

        _util.simpleShader.call(this, gl.triangleBlur, {
          delta: [radius / this.width, 0]
        });

        _util.simpleShader.call(this, gl.triangleBlur, {
          delta: [0, radius / this.height]
        });

        return this;
      };

      var _shader = __webpack_require__(
      /*! ../../shader */
      "./node_modules/glfx-es6/es/shader.js");

      var _shader2 = _interopRequireDefault(_shader);

      var _util = __webpack_require__(
      /*! ../../util */
      "./node_modules/glfx-es6/es/util.js");

      var _common = __webpack_require__(
      /*! ../common */
      "./node_modules/glfx-es6/es/filters/common.js");

      var _store = __webpack_require__(
      /*! ../../store */
      "./node_modules/glfx-es6/es/store.js");

      var store = _interopRequireWildcard(_store);

      function _interopRequireWildcard(obj) {
        if (obj && obj.__esModule) {
          return obj;
        } else {
          var newObj = {};

          if (obj != null) {
            for (var key in obj) {
              if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key];
            }
          }

          newObj["default"] = obj;
          return newObj;
        }
      }

      function _interopRequireDefault(obj) {
        return obj && obj.__esModule ? obj : {
          "default": obj
        };
      }
      /***/

    },

    /***/
    "./node_modules/glfx-es6/es/filters/blur/zoomblur.js":
    /*!***********************************************************!*\
      !*** ./node_modules/glfx-es6/es/filters/blur/zoomblur.js ***!
      \***********************************************************/

    /*! no static exports found */

    /***/
    function node_modulesGlfxEs6EsFiltersBlurZoomblurJs(module, exports, __webpack_require__) {
      "use strict";

      Object.defineProperty(exports, "__esModule", {
        value: true
      });

      exports["default"] = function (centerX, centerY, strength) {
        var gl = store.get('gl');
        gl.zoomBlur = gl.zoomBlur || new _shader2["default"](null, '\
    uniform sampler2D texture;\
    uniform vec2 center;\
    uniform float strength;\
    uniform vec2 texSize;\
    varying vec2 texCoord;\
    ' + _common.randomShaderFunc + '\
    void main() {\
      vec4 color = vec4(0.0);\
      float total = 0.0;\
      vec2 toCenter = center - texCoord * texSize;\
      \
      /* randomize the lookup values to hide the fixed number of samples */\
      float offset = random(vec3(12.9898, 78.233, 151.7182), 0.0);\
      \
      for (float t = 0.0; t <= 40.0; t++) {\
        float percent = (t + offset) / 40.0;\
        float weight = 4.0 * (percent - percent * percent);\
        vec4 sample = texture2D(texture, texCoord + toCenter * percent * strength / texSize);\
        \
        /* switch to pre-multiplied alpha to correctly blur transparent images */\
        sample.rgb *= sample.a;\
        \
        color += sample * weight;\
        total += weight;\
      }\
      \
      gl_FragColor = color / total;\
      \
      /* switch back from pre-multiplied alpha */\
      gl_FragColor.rgb /= gl_FragColor.a + 0.00001;\
    }\
  ');

        _util.simpleShader.call(this, gl.zoomBlur, {
          center: [centerX, centerY],
          strength: strength,
          texSize: [this.width, this.height]
        });

        return this;
      };

      var _shader = __webpack_require__(
      /*! ../../shader */
      "./node_modules/glfx-es6/es/shader.js");

      var _shader2 = _interopRequireDefault(_shader);

      var _util = __webpack_require__(
      /*! ../../util */
      "./node_modules/glfx-es6/es/util.js");

      var _common = __webpack_require__(
      /*! ../common */
      "./node_modules/glfx-es6/es/filters/common.js");

      var _store = __webpack_require__(
      /*! ../../store */
      "./node_modules/glfx-es6/es/store.js");

      var store = _interopRequireWildcard(_store);

      function _interopRequireWildcard(obj) {
        if (obj && obj.__esModule) {
          return obj;
        } else {
          var newObj = {};

          if (obj != null) {
            for (var key in obj) {
              if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key];
            }
          }

          newObj["default"] = obj;
          return newObj;
        }
      }

      function _interopRequireDefault(obj) {
        return obj && obj.__esModule ? obj : {
          "default": obj
        };
      }
      /***/

    },

    /***/
    "./node_modules/glfx-es6/es/filters/common.js":
    /*!****************************************************!*\
      !*** ./node_modules/glfx-es6/es/filters/common.js ***!
      \****************************************************/

    /*! no static exports found */

    /***/
    function node_modulesGlfxEs6EsFiltersCommonJs(module, exports, __webpack_require__) {
      "use strict";

      Object.defineProperty(exports, "__esModule", {
        value: true
      });
      exports.randomShaderFunc = undefined;
      exports.warpShader = warpShader;

      var _shader = __webpack_require__(
      /*! ../shader */
      "./node_modules/glfx-es6/es/shader.js");

      var _shader2 = _interopRequireDefault(_shader);

      function _interopRequireDefault(obj) {
        return obj && obj.__esModule ? obj : {
          "default": obj
        };
      }

      function warpShader(uniforms, warp) {
        return new _shader2["default"](null, uniforms + '\
    uniform sampler2D texture;\
    uniform vec2 texSize;\
    varying vec2 texCoord;\
    void main() {\
      vec2 coord = texCoord * texSize;\
      ' + warp + '\
      gl_FragColor = texture2D(texture, coord / texSize);\
      vec2 clampedCoord = clamp(coord, vec2(0.0), texSize);\
      if (coord != clampedCoord) {\
        /* fade to transparent if we are outside the image */\
        gl_FragColor.a *= max(0.0, 1.0 - length(coord - clampedCoord));\
      }\
    }');
      } // returns a random number between 0 and 1


      var randomShaderFunc = exports.randomShaderFunc = '\
  float random(vec3 scale, float seed) {\
    /* use the fragment position for a different seed per-pixel */\
    return fract(sin(dot(gl_FragCoord.xyz + seed, scale)) * 43758.5453 + seed);\
  }\
';
      /***/
    },

    /***/
    "./node_modules/glfx-es6/es/filters/fun/colorhalftone.js":
    /*!***************************************************************!*\
      !*** ./node_modules/glfx-es6/es/filters/fun/colorhalftone.js ***!
      \***************************************************************/

    /*! no static exports found */

    /***/
    function node_modulesGlfxEs6EsFiltersFunColorhalftoneJs(module, exports, __webpack_require__) {
      "use strict";

      Object.defineProperty(exports, "__esModule", {
        value: true
      });

      exports["default"] = function (centerX, centerY, angle, size) {
        var gl = store.get('gl');
        gl.colorHalftone = gl.colorHalftone || new _shader2["default"](null, '\
    uniform sampler2D texture;\
    uniform vec2 center;\
    uniform float angle;\
    uniform float scale;\
    uniform vec2 texSize;\
    varying vec2 texCoord;\
    \
    float pattern(float angle) {\
      float s = sin(angle), c = cos(angle);\
      vec2 tex = texCoord * texSize - center;\
      vec2 point = vec2(\
        c * tex.x - s * tex.y,\
        s * tex.x + c * tex.y\
      ) * scale;\
      return (sin(point.x) * sin(point.y)) * 4.0;\
    }\
    \
    void main() {\
      vec4 color = texture2D(texture, texCoord);\
      vec3 cmy = 1.0 - color.rgb;\
      float k = min(cmy.x, min(cmy.y, cmy.z));\
      cmy = (cmy - k) / (1.0 - k);\
      cmy = clamp(cmy * 10.0 - 3.0 + vec3(pattern(angle + 0.26179), pattern(angle + 1.30899), pattern(angle)), 0.0, 1.0);\
      k = clamp(k * 10.0 - 5.0 + pattern(angle + 0.78539), 0.0, 1.0);\
      gl_FragColor = vec4(1.0 - cmy - k, color.a);\
    }\
  ');

        _util.simpleShader.call(this, gl.colorHalftone, {
          center: [centerX, centerY],
          angle: angle,
          scale: Math.PI / size,
          texSize: [this.width, this.height]
        });

        return this;
      };

      var _shader = __webpack_require__(
      /*! ../../shader */
      "./node_modules/glfx-es6/es/shader.js");

      var _shader2 = _interopRequireDefault(_shader);

      var _util = __webpack_require__(
      /*! ../../util */
      "./node_modules/glfx-es6/es/util.js");

      var _store = __webpack_require__(
      /*! ../../store */
      "./node_modules/glfx-es6/es/store.js");

      var store = _interopRequireWildcard(_store);

      function _interopRequireWildcard(obj) {
        if (obj && obj.__esModule) {
          return obj;
        } else {
          var newObj = {};

          if (obj != null) {
            for (var key in obj) {
              if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key];
            }
          }

          newObj["default"] = obj;
          return newObj;
        }
      }

      function _interopRequireDefault(obj) {
        return obj && obj.__esModule ? obj : {
          "default": obj
        };
      }
      /***/

    },

    /***/
    "./node_modules/glfx-es6/es/filters/fun/dotscreen.js":
    /*!***********************************************************!*\
      !*** ./node_modules/glfx-es6/es/filters/fun/dotscreen.js ***!
      \***********************************************************/

    /*! no static exports found */

    /***/
    function node_modulesGlfxEs6EsFiltersFunDotscreenJs(module, exports, __webpack_require__) {
      "use strict";

      Object.defineProperty(exports, "__esModule", {
        value: true
      });

      exports["default"] = function (centerX, centerY, angle, size) {
        var gl = store.get('gl');
        gl.dotScreen = gl.dotScreen || new _shader2["default"](null, '\
    uniform sampler2D texture;\
    uniform vec2 center;\
    uniform float angle;\
    uniform float scale;\
    uniform vec2 texSize;\
    varying vec2 texCoord;\
    \
    float pattern() {\
      float s = sin(angle), c = cos(angle);\
      vec2 tex = texCoord * texSize - center;\
      vec2 point = vec2(\
        c * tex.x - s * tex.y,\
        s * tex.x + c * tex.y\
      ) * scale;\
      return (sin(point.x) * sin(point.y)) * 4.0;\
    }\
    \
    void main() {\
      vec4 color = texture2D(texture, texCoord);\
      float average = (color.r + color.g + color.b) / 3.0;\
      gl_FragColor = vec4(vec3(average * 10.0 - 5.0 + pattern()), color.a);\
    }\
  ');

        _util.simpleShader.call(this, gl.dotScreen, {
          center: [centerX, centerY],
          angle: angle,
          scale: Math.PI / size,
          texSize: [this.width, this.height]
        });

        return this;
      };

      var _shader = __webpack_require__(
      /*! ../../shader */
      "./node_modules/glfx-es6/es/shader.js");

      var _shader2 = _interopRequireDefault(_shader);

      var _util = __webpack_require__(
      /*! ../../util */
      "./node_modules/glfx-es6/es/util.js");

      var _store = __webpack_require__(
      /*! ../../store */
      "./node_modules/glfx-es6/es/store.js");

      var store = _interopRequireWildcard(_store);

      function _interopRequireWildcard(obj) {
        if (obj && obj.__esModule) {
          return obj;
        } else {
          var newObj = {};

          if (obj != null) {
            for (var key in obj) {
              if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key];
            }
          }

          newObj["default"] = obj;
          return newObj;
        }
      }

      function _interopRequireDefault(obj) {
        return obj && obj.__esModule ? obj : {
          "default": obj
        };
      }
      /***/

    },

    /***/
    "./node_modules/glfx-es6/es/filters/fun/edgework.js":
    /*!**********************************************************!*\
      !*** ./node_modules/glfx-es6/es/filters/fun/edgework.js ***!
      \**********************************************************/

    /*! no static exports found */

    /***/
    function node_modulesGlfxEs6EsFiltersFunEdgeworkJs(module, exports, __webpack_require__) {
      "use strict";

      Object.defineProperty(exports, "__esModule", {
        value: true
      });

      exports["default"] = function (radius) {
        var gl = store.get('gl');
        gl.edgeWork1 = gl.edgeWork1 || new _shader2["default"](null, '\
    uniform sampler2D texture;\
    uniform vec2 delta;\
    varying vec2 texCoord;\
    ' + _common.randomShaderFunc + '\
    void main() {\
      vec2 color = vec2(0.0);\
      vec2 total = vec2(0.0);\
      \
      /* randomize the lookup values to hide the fixed number of samples */\
      float offset = random(vec3(12.9898, 78.233, 151.7182), 0.0);\
      \
      for (float t = -30.0; t <= 30.0; t++) {\
        float percent = (t + offset - 0.5) / 30.0;\
        float weight = 1.0 - abs(percent);\
        vec3 sample = texture2D(texture, texCoord + delta * percent).rgb;\
        float average = (sample.r + sample.g + sample.b) / 3.0;\
        color.x += average * weight;\
        total.x += weight;\
        if (abs(t) < 15.0) {\
          weight = weight * 2.0 - 1.0;\
          color.y += average * weight;\
          total.y += weight;\
        }\
      }\
      gl_FragColor = vec4(color / total, 0.0, 1.0);\
    }\
  ');
        gl.edgeWork2 = gl.edgeWork2 || new _shader2["default"](null, '\
    uniform sampler2D texture;\
    uniform vec2 delta;\
    varying vec2 texCoord;\
    ' + _common.randomShaderFunc + '\
    void main() {\
      vec2 color = vec2(0.0);\
      vec2 total = vec2(0.0);\
      \
      /* randomize the lookup values to hide the fixed number of samples */\
      float offset = random(vec3(12.9898, 78.233, 151.7182), 0.0);\
      \
      for (float t = -30.0; t <= 30.0; t++) {\
        float percent = (t + offset - 0.5) / 30.0;\
        float weight = 1.0 - abs(percent);\
        vec2 sample = texture2D(texture, texCoord + delta * percent).xy;\
        color.x += sample.x * weight;\
        total.x += weight;\
        if (abs(t) < 15.0) {\
          weight = weight * 2.0 - 1.0;\
          color.y += sample.y * weight;\
          total.y += weight;\
        }\
      }\
      float c = clamp(10000.0 * (color.y / total.y - color.x / total.x) + 0.5, 0.0, 1.0);\
      gl_FragColor = vec4(c, c, c, 1.0);\
    }\
  ');

        _util.simpleShader.call(this, gl.edgeWork1, {
          delta: [radius / this.width, 0]
        });

        _util.simpleShader.call(this, gl.edgeWork2, {
          delta: [0, radius / this.height]
        });

        return this;
      };

      var _shader = __webpack_require__(
      /*! ../../shader */
      "./node_modules/glfx-es6/es/shader.js");

      var _shader2 = _interopRequireDefault(_shader);

      var _util = __webpack_require__(
      /*! ../../util */
      "./node_modules/glfx-es6/es/util.js");

      var _common = __webpack_require__(
      /*! ../common */
      "./node_modules/glfx-es6/es/filters/common.js");

      var _store = __webpack_require__(
      /*! ../../store */
      "./node_modules/glfx-es6/es/store.js");

      var store = _interopRequireWildcard(_store);

      function _interopRequireWildcard(obj) {
        if (obj && obj.__esModule) {
          return obj;
        } else {
          var newObj = {};

          if (obj != null) {
            for (var key in obj) {
              if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key];
            }
          }

          newObj["default"] = obj;
          return newObj;
        }
      }

      function _interopRequireDefault(obj) {
        return obj && obj.__esModule ? obj : {
          "default": obj
        };
      }
      /***/

    },

    /***/
    "./node_modules/glfx-es6/es/filters/fun/hexagonalpixelate.js":
    /*!*******************************************************************!*\
      !*** ./node_modules/glfx-es6/es/filters/fun/hexagonalpixelate.js ***!
      \*******************************************************************/

    /*! no static exports found */

    /***/
    function node_modulesGlfxEs6EsFiltersFunHexagonalpixelateJs(module, exports, __webpack_require__) {
      "use strict";

      Object.defineProperty(exports, "__esModule", {
        value: true
      });

      exports["default"] = function (centerX, centerY, scale) {
        var gl = store.get('gl');
        gl.hexagonalPixelate = gl.hexagonalPixelate || new _shader2["default"](null, '\
    uniform sampler2D texture;\
    uniform vec2 center;\
    uniform float scale;\
    uniform vec2 texSize;\
    varying vec2 texCoord;\
    void main() {\
      vec2 tex = (texCoord * texSize - center) / scale;\
      tex.y /= 0.866025404;\
      tex.x -= tex.y * 0.5;\
      \
      vec2 a;\
      if (tex.x + tex.y - floor(tex.x) - floor(tex.y) < 1.0) a = vec2(floor(tex.x), floor(tex.y));\
      else a = vec2(ceil(tex.x), ceil(tex.y));\
      vec2 b = vec2(ceil(tex.x), floor(tex.y));\
      vec2 c = vec2(floor(tex.x), ceil(tex.y));\
      \
      vec3 TEX = vec3(tex.x, tex.y, 1.0 - tex.x - tex.y);\
      vec3 A = vec3(a.x, a.y, 1.0 - a.x - a.y);\
      vec3 B = vec3(b.x, b.y, 1.0 - b.x - b.y);\
      vec3 C = vec3(c.x, c.y, 1.0 - c.x - c.y);\
      \
      float alen = length(TEX - A);\
      float blen = length(TEX - B);\
      float clen = length(TEX - C);\
      \
      vec2 choice;\
      if (alen < blen) {\
        if (alen < clen) choice = a;\
        else choice = c;\
      } else {\
        if (blen < clen) choice = b;\
        else choice = c;\
      }\
      \
      choice.x += choice.y * 0.5;\
      choice.y *= 0.866025404;\
      choice *= scale / texSize;\
      gl_FragColor = texture2D(texture, choice + center / texSize);\
    }\
  ');

        _util.simpleShader.call(this, gl.hexagonalPixelate, {
          center: [centerX, centerY],
          scale: scale,
          texSize: [this.width, this.height]
        });

        return this;
      };

      var _shader = __webpack_require__(
      /*! ../../shader */
      "./node_modules/glfx-es6/es/shader.js");

      var _shader2 = _interopRequireDefault(_shader);

      var _util = __webpack_require__(
      /*! ../../util */
      "./node_modules/glfx-es6/es/util.js");

      var _store = __webpack_require__(
      /*! ../../store */
      "./node_modules/glfx-es6/es/store.js");

      var store = _interopRequireWildcard(_store);

      function _interopRequireWildcard(obj) {
        if (obj && obj.__esModule) {
          return obj;
        } else {
          var newObj = {};

          if (obj != null) {
            for (var key in obj) {
              if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key];
            }
          }

          newObj["default"] = obj;
          return newObj;
        }
      }

      function _interopRequireDefault(obj) {
        return obj && obj.__esModule ? obj : {
          "default": obj
        };
      }
      /***/

    },

    /***/
    "./node_modules/glfx-es6/es/filters/fun/ink.js":
    /*!*****************************************************!*\
      !*** ./node_modules/glfx-es6/es/filters/fun/ink.js ***!
      \*****************************************************/

    /*! no static exports found */

    /***/
    function node_modulesGlfxEs6EsFiltersFunInkJs(module, exports, __webpack_require__) {
      "use strict";

      Object.defineProperty(exports, "__esModule", {
        value: true
      });

      exports["default"] = function (strength) {
        var gl = store.get('gl');
        gl.ink = gl.ink || new _shader2["default"](null, '\
    uniform sampler2D texture;\
    uniform float strength;\
    uniform vec2 texSize;\
    varying vec2 texCoord;\
    void main() {\
      vec2 dx = vec2(1.0 / texSize.x, 0.0);\
      vec2 dy = vec2(0.0, 1.0 / texSize.y);\
      vec4 color = texture2D(texture, texCoord);\
      float bigTotal = 0.0;\
      float smallTotal = 0.0;\
      vec3 bigAverage = vec3(0.0);\
      vec3 smallAverage = vec3(0.0);\
      for (float x = -2.0; x <= 2.0; x += 1.0) {\
        for (float y = -2.0; y <= 2.0; y += 1.0) {\
          vec3 sample = texture2D(texture, texCoord + dx * x + dy * y).rgb;\
          bigAverage += sample;\
          bigTotal += 1.0;\
          if (abs(x) + abs(y) < 2.0) {\
            smallAverage += sample;\
            smallTotal += 1.0;\
          }\
        }\
      }\
      vec3 edge = max(vec3(0.0), bigAverage / bigTotal - smallAverage / smallTotal);\
      gl_FragColor = vec4(color.rgb - dot(edge, edge) * strength * 100000.0, color.a);\
    }\
  ');

        _util.simpleShader.call(this, gl.ink, {
          strength: strength * strength * strength * strength * strength,
          texSize: [this.width, this.height]
        });

        return this;
      };

      var _shader = __webpack_require__(
      /*! ../../shader */
      "./node_modules/glfx-es6/es/shader.js");

      var _shader2 = _interopRequireDefault(_shader);

      var _util = __webpack_require__(
      /*! ../../util */
      "./node_modules/glfx-es6/es/util.js");

      var _store = __webpack_require__(
      /*! ../../store */
      "./node_modules/glfx-es6/es/store.js");

      var store = _interopRequireWildcard(_store);

      function _interopRequireWildcard(obj) {
        if (obj && obj.__esModule) {
          return obj;
        } else {
          var newObj = {};

          if (obj != null) {
            for (var key in obj) {
              if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key];
            }
          }

          newObj["default"] = obj;
          return newObj;
        }
      }

      function _interopRequireDefault(obj) {
        return obj && obj.__esModule ? obj : {
          "default": obj
        };
      }
      /***/

    },

    /***/
    "./node_modules/glfx-es6/es/filters/index.js":
    /*!***************************************************!*\
      !*** ./node_modules/glfx-es6/es/filters/index.js ***!
      \***************************************************/

    /*! no static exports found */

    /***/
    function node_modulesGlfxEs6EsFiltersIndexJs(module, exports, __webpack_require__) {
      "use strict";

      Object.defineProperty(exports, "__esModule", {
        value: true
      });

      var _brightnesscontrast = __webpack_require__(
      /*! ./adjust/brightnesscontrast */
      "./node_modules/glfx-es6/es/filters/adjust/brightnesscontrast.js");

      Object.defineProperty(exports, 'brightnessContrast', {
        enumerable: true,
        get: function get() {
          return _interopRequireDefault(_brightnesscontrast)["default"];
        }
      });

      var _curves = __webpack_require__(
      /*! ./adjust/curves */
      "./node_modules/glfx-es6/es/filters/adjust/curves.js");

      Object.defineProperty(exports, 'curves', {
        enumerable: true,
        get: function get() {
          return _interopRequireDefault(_curves)["default"];
        }
      });

      var _denoise = __webpack_require__(
      /*! ./adjust/denoise */
      "./node_modules/glfx-es6/es/filters/adjust/denoise.js");

      Object.defineProperty(exports, 'denoise', {
        enumerable: true,
        get: function get() {
          return _interopRequireDefault(_denoise)["default"];
        }
      });

      var _huesaturation = __webpack_require__(
      /*! ./adjust/huesaturation */
      "./node_modules/glfx-es6/es/filters/adjust/huesaturation.js");

      Object.defineProperty(exports, 'hueSaturation', {
        enumerable: true,
        get: function get() {
          return _interopRequireDefault(_huesaturation)["default"];
        }
      });

      var _noise = __webpack_require__(
      /*! ./adjust/noise */
      "./node_modules/glfx-es6/es/filters/adjust/noise.js");

      Object.defineProperty(exports, 'noise', {
        enumerable: true,
        get: function get() {
          return _interopRequireDefault(_noise)["default"];
        }
      });

      var _sepia = __webpack_require__(
      /*! ./adjust/sepia */
      "./node_modules/glfx-es6/es/filters/adjust/sepia.js");

      Object.defineProperty(exports, 'sepia', {
        enumerable: true,
        get: function get() {
          return _interopRequireDefault(_sepia)["default"];
        }
      });

      var _unsharpmask = __webpack_require__(
      /*! ./adjust/unsharpmask */
      "./node_modules/glfx-es6/es/filters/adjust/unsharpmask.js");

      Object.defineProperty(exports, 'unsharpMask', {
        enumerable: true,
        get: function get() {
          return _interopRequireDefault(_unsharpmask)["default"];
        }
      });

      var _vibrance = __webpack_require__(
      /*! ./adjust/vibrance */
      "./node_modules/glfx-es6/es/filters/adjust/vibrance.js");

      Object.defineProperty(exports, 'vibrance', {
        enumerable: true,
        get: function get() {
          return _interopRequireDefault(_vibrance)["default"];
        }
      });

      var _vignette = __webpack_require__(
      /*! ./adjust/vignette */
      "./node_modules/glfx-es6/es/filters/adjust/vignette.js");

      Object.defineProperty(exports, 'vignette', {
        enumerable: true,
        get: function get() {
          return _interopRequireDefault(_vignette)["default"];
        }
      });

      var _lensblur = __webpack_require__(
      /*! ./blur/lensblur */
      "./node_modules/glfx-es6/es/filters/blur/lensblur.js");

      Object.defineProperty(exports, 'lensBlur', {
        enumerable: true,
        get: function get() {
          return _interopRequireDefault(_lensblur)["default"];
        }
      });

      var _tiltshift = __webpack_require__(
      /*! ./blur/tiltshift */
      "./node_modules/glfx-es6/es/filters/blur/tiltshift.js");

      Object.defineProperty(exports, 'tiltShift', {
        enumerable: true,
        get: function get() {
          return _interopRequireDefault(_tiltshift)["default"];
        }
      });

      var _triangleblur = __webpack_require__(
      /*! ./blur/triangleblur */
      "./node_modules/glfx-es6/es/filters/blur/triangleblur.js");

      Object.defineProperty(exports, 'triangleBlur', {
        enumerable: true,
        get: function get() {
          return _interopRequireDefault(_triangleblur)["default"];
        }
      });

      var _zoomblur = __webpack_require__(
      /*! ./blur/zoomblur */
      "./node_modules/glfx-es6/es/filters/blur/zoomblur.js");

      Object.defineProperty(exports, 'zoomBlur', {
        enumerable: true,
        get: function get() {
          return _interopRequireDefault(_zoomblur)["default"];
        }
      });

      var _colorhalftone = __webpack_require__(
      /*! ./fun/colorhalftone */
      "./node_modules/glfx-es6/es/filters/fun/colorhalftone.js");

      Object.defineProperty(exports, 'colorHalftone', {
        enumerable: true,
        get: function get() {
          return _interopRequireDefault(_colorhalftone)["default"];
        }
      });

      var _dotscreen = __webpack_require__(
      /*! ./fun/dotscreen */
      "./node_modules/glfx-es6/es/filters/fun/dotscreen.js");

      Object.defineProperty(exports, 'dotScreen', {
        enumerable: true,
        get: function get() {
          return _interopRequireDefault(_dotscreen)["default"];
        }
      });

      var _edgework = __webpack_require__(
      /*! ./fun/edgework */
      "./node_modules/glfx-es6/es/filters/fun/edgework.js");

      Object.defineProperty(exports, 'edgeWork', {
        enumerable: true,
        get: function get() {
          return _interopRequireDefault(_edgework)["default"];
        }
      });

      var _hexagonalpixelate = __webpack_require__(
      /*! ./fun/hexagonalpixelate */
      "./node_modules/glfx-es6/es/filters/fun/hexagonalpixelate.js");

      Object.defineProperty(exports, 'hexagonalPixelate', {
        enumerable: true,
        get: function get() {
          return _interopRequireDefault(_hexagonalpixelate)["default"];
        }
      });

      var _ink = __webpack_require__(
      /*! ./fun/ink */
      "./node_modules/glfx-es6/es/filters/fun/ink.js");

      Object.defineProperty(exports, 'ink', {
        enumerable: true,
        get: function get() {
          return _interopRequireDefault(_ink)["default"];
        }
      });

      var _bulgepinch = __webpack_require__(
      /*! ./warp/bulgepinch */
      "./node_modules/glfx-es6/es/filters/warp/bulgepinch.js");

      Object.defineProperty(exports, 'bulgePinch', {
        enumerable: true,
        get: function get() {
          return _interopRequireDefault(_bulgepinch)["default"];
        }
      });

      var _matrixwarp = __webpack_require__(
      /*! ./warp/matrixwarp */
      "./node_modules/glfx-es6/es/filters/warp/matrixwarp.js");

      Object.defineProperty(exports, 'matrixWarp', {
        enumerable: true,
        get: function get() {
          return _interopRequireDefault(_matrixwarp)["default"];
        }
      });

      var _perspective = __webpack_require__(
      /*! ./warp/perspective */
      "./node_modules/glfx-es6/es/filters/warp/perspective.js");

      Object.defineProperty(exports, 'perspective', {
        enumerable: true,
        get: function get() {
          return _interopRequireDefault(_perspective)["default"];
        }
      });

      var _swirl = __webpack_require__(
      /*! ./warp/swirl */
      "./node_modules/glfx-es6/es/filters/warp/swirl.js");

      Object.defineProperty(exports, 'swirl', {
        enumerable: true,
        get: function get() {
          return _interopRequireDefault(_swirl)["default"];
        }
      });

      function _interopRequireDefault(obj) {
        return obj && obj.__esModule ? obj : {
          "default": obj
        };
      }
      /***/

    },

    /***/
    "./node_modules/glfx-es6/es/filters/warp/bulgepinch.js":
    /*!*************************************************************!*\
      !*** ./node_modules/glfx-es6/es/filters/warp/bulgepinch.js ***!
      \*************************************************************/

    /*! no static exports found */

    /***/
    function node_modulesGlfxEs6EsFiltersWarpBulgepinchJs(module, exports, __webpack_require__) {
      "use strict";

      Object.defineProperty(exports, "__esModule", {
        value: true
      });

      exports["default"] = function (centerX, centerY, radius, strength) {
        var gl = store.get('gl');
        gl.bulgePinch = gl.bulgePinch || (0, _common.warpShader)('\
    uniform float radius;\
    uniform float strength;\
    uniform vec2 center;\
  ', '\
    coord -= center;\
    float distance = length(coord);\
    if (distance < radius) {\
      float percent = distance / radius;\
      if (strength > 0.0) {\
        coord *= mix(1.0, smoothstep(0.0, radius / distance, percent), strength * 0.75);\
      } else {\
        coord *= mix(1.0, pow(percent, 1.0 + strength * 0.75) * radius / distance, 1.0 - percent);\
      }\
    }\
    coord += center;\
  ');

        _util.simpleShader.call(this, gl.bulgePinch, {
          radius: radius,
          strength: (0, _util.clamp)(-1, strength, 1),
          center: [centerX, centerY],
          texSize: [this.width, this.height]
        });

        return this;
      };

      var _common = __webpack_require__(
      /*! ../common */
      "./node_modules/glfx-es6/es/filters/common.js");

      var _util = __webpack_require__(
      /*! ../../util */
      "./node_modules/glfx-es6/es/util.js");

      var _store = __webpack_require__(
      /*! ../../store */
      "./node_modules/glfx-es6/es/store.js");

      var store = _interopRequireWildcard(_store);

      function _interopRequireWildcard(obj) {
        if (obj && obj.__esModule) {
          return obj;
        } else {
          var newObj = {};

          if (obj != null) {
            for (var key in obj) {
              if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key];
            }
          }

          newObj["default"] = obj;
          return newObj;
        }
      }
      /***/

    },

    /***/
    "./node_modules/glfx-es6/es/filters/warp/matrixwarp.js":
    /*!*************************************************************!*\
      !*** ./node_modules/glfx-es6/es/filters/warp/matrixwarp.js ***!
      \*************************************************************/

    /*! no static exports found */

    /***/
    function node_modulesGlfxEs6EsFiltersWarpMatrixwarpJs(module, exports, __webpack_require__) {
      "use strict";

      Object.defineProperty(exports, "__esModule", {
        value: true
      });

      exports["default"] = function (matrix, inverse, useTextureSpace) {
        var gl = store.get('gl');
        gl.matrixWarp = gl.matrixWarp || (0, _common.warpShader)('\
    uniform mat3 matrix;\
    uniform bool useTextureSpace;\
  ', '\
    if (useTextureSpace) coord = coord / texSize * 2.0 - 1.0;\
    vec3 warp = matrix * vec3(coord, 1.0);\
    coord = warp.xy / warp.z;\
    if (useTextureSpace) coord = (coord * 0.5 + 0.5) * texSize;\
  '); // Flatten all members of matrix into one big list

        matrix = Array.prototype.concat.apply([], matrix); // Extract a 3x3 matrix out of the arguments

        if (matrix.length == 4) {
          matrix = [matrix[0], matrix[1], 0, matrix[2], matrix[3], 0, 0, 0, 1];
        } else if (matrix.length != 9) {
          throw 'can only warp with 2x2 or 3x3 matrix';
        }

        _util.simpleShader.call(this, gl.matrixWarp, {
          matrix: inverse ? (0, _matrix.getInverse)(matrix) : matrix,
          texSize: [this.width, this.height],
          useTextureSpace: useTextureSpace | 0
        });

        return this;
      };

      var _common = __webpack_require__(
      /*! ../common */
      "./node_modules/glfx-es6/es/filters/common.js");

      var _matrix = __webpack_require__(
      /*! ../../matrix */
      "./node_modules/glfx-es6/es/matrix.js");

      var _util = __webpack_require__(
      /*! ../../util */
      "./node_modules/glfx-es6/es/util.js");

      var _store = __webpack_require__(
      /*! ../../store */
      "./node_modules/glfx-es6/es/store.js");

      var store = _interopRequireWildcard(_store);

      function _interopRequireWildcard(obj) {
        if (obj && obj.__esModule) {
          return obj;
        } else {
          var newObj = {};

          if (obj != null) {
            for (var key in obj) {
              if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key];
            }
          }

          newObj["default"] = obj;
          return newObj;
        }
      }
      /***/

    },

    /***/
    "./node_modules/glfx-es6/es/filters/warp/perspective.js":
    /*!**************************************************************!*\
      !*** ./node_modules/glfx-es6/es/filters/warp/perspective.js ***!
      \**************************************************************/

    /*! no static exports found */

    /***/
    function node_modulesGlfxEs6EsFiltersWarpPerspectiveJs(module, exports, __webpack_require__) {
      "use strict";

      Object.defineProperty(exports, "__esModule", {
        value: true
      });

      exports["default"] = function (before, after) {
        var a = _matrix.getSquareToQuad.apply(null, after);

        var b = _matrix.getSquareToQuad.apply(null, before);

        var c = (0, _matrix.multiply)((0, _matrix.getInverse)(a), b);
        return this.matrixWarp(c);
      };

      var _matrix = __webpack_require__(
      /*! ../../matrix */
      "./node_modules/glfx-es6/es/matrix.js");
      /***/

    },

    /***/
    "./node_modules/glfx-es6/es/filters/warp/swirl.js":
    /*!********************************************************!*\
      !*** ./node_modules/glfx-es6/es/filters/warp/swirl.js ***!
      \********************************************************/

    /*! no static exports found */

    /***/
    function node_modulesGlfxEs6EsFiltersWarpSwirlJs(module, exports, __webpack_require__) {
      "use strict";

      Object.defineProperty(exports, "__esModule", {
        value: true
      });

      exports["default"] = function (centerX, centerY, radius, angle) {
        var gl = store.get('gl');
        gl.swirl = gl.swirl || (0, _common.warpShader)('\
    uniform float radius;\
    uniform float angle;\
    uniform vec2 center;\
  ', '\
    coord -= center;\
    float distance = length(coord);\
    if (distance < radius) {\
      float percent = (radius - distance) / radius;\
      float theta = percent * percent * angle;\
      float s = sin(theta);\
      float c = cos(theta);\
      coord = vec2(\
        coord.x * c - coord.y * s,\
        coord.x * s + coord.y * c\
      );\
    }\
    coord += center;\
  ');

        _util.simpleShader.call(this, gl.swirl, {
          radius: radius,
          center: [centerX, centerY],
          angle: angle,
          texSize: [this.width, this.height]
        });

        return this;
      };

      var _common = __webpack_require__(
      /*! ../common */
      "./node_modules/glfx-es6/es/filters/common.js");

      var _util = __webpack_require__(
      /*! ../../util */
      "./node_modules/glfx-es6/es/util.js");

      var _store = __webpack_require__(
      /*! ../../store */
      "./node_modules/glfx-es6/es/store.js");

      var store = _interopRequireWildcard(_store);

      function _interopRequireWildcard(obj) {
        if (obj && obj.__esModule) {
          return obj;
        } else {
          var newObj = {};

          if (obj != null) {
            for (var key in obj) {
              if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key];
            }
          }

          newObj["default"] = obj;
          return newObj;
        }
      }
      /***/

    },

    /***/
    "./node_modules/glfx-es6/es/index.js":
    /*!*******************************************!*\
      !*** ./node_modules/glfx-es6/es/index.js ***!
      \*******************************************/

    /*! no static exports found */

    /***/
    function node_modulesGlfxEs6EsIndexJs(module, exports, __webpack_require__) {
      "use strict";

      Object.defineProperty(exports, "__esModule", {
        value: true
      });
      exports.splineInterpolate = undefined;

      var _util = __webpack_require__(
      /*! ./util */
      "./node_modules/glfx-es6/es/util.js");

      Object.defineProperty(exports, 'splineInterpolate', {
        enumerable: true,
        get: function get() {
          return _util.splineInterpolate;
        }
      });
      exports.canvas = canvas;

      var _store = __webpack_require__(
      /*! ./store */
      "./node_modules/glfx-es6/es/store.js");

      var store = _interopRequireWildcard(_store);

      var _texture = __webpack_require__(
      /*! ./texture */
      "./node_modules/glfx-es6/es/texture.js");

      var _texture2 = _interopRequireDefault(_texture);

      var _shader = __webpack_require__(
      /*! ./shader */
      "./node_modules/glfx-es6/es/shader.js");

      var _shader2 = _interopRequireDefault(_shader);

      var _filters = __webpack_require__(
      /*! ./filters */
      "./node_modules/glfx-es6/es/filters/index.js");

      var filters = _interopRequireWildcard(_filters);

      function _interopRequireDefault(obj) {
        return obj && obj.__esModule ? obj : {
          "default": obj
        };
      }

      function _interopRequireWildcard(obj) {
        if (obj && obj.__esModule) {
          return obj;
        } else {
          var newObj = {};

          if (obj != null) {
            for (var key in obj) {
              if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key];
            }
          }

          newObj["default"] = obj;
          return newObj;
        }
      }

      function wrapTexture(texture) {
        return {
          _: texture,
          loadContentsOf: function loadContentsOf(element) {
            // Make sure that we're using the correct global WebGL context
            store.set({
              gl: this._.gl
            });

            this._.loadContentsOf(element);
          },
          destroy: function destroy() {
            // Make sure that we're using the correct global WebGL context
            store.set({
              gl: this._.gl
            });

            this._.destroy();
          }
        };
      }

      function texture(element) {
        return wrapTexture(_texture2["default"].fromElement(element));
      }

      function initialize(width, height) {
        var gl = store.get('gl');
        var type = gl.UNSIGNED_BYTE; // Go for floating point buffer textures if we can, it'll make the bokeh
        // filter look a lot better. Note that on Windows, ANGLE does not let you
        // render to a floating-point texture when linear filtering is enabled.
        // See http://crbug.com/172278 for more information.

        if (gl.getExtension('OES_texture_float') && gl.getExtension('OES_texture_float_linear')) {
          var testTexture = new _texture2["default"](100, 100, gl.RGBA, gl.FLOAT);

          try {
            // Only use gl.FLOAT if we can render to it
            testTexture.drawTo(function () {
              type = gl.FLOAT;
            });
          } catch (e) {}

          testTexture.destroy();
        }

        if (this._.texture) this._.texture.destroy();
        if (this._.spareTexture) this._.spareTexture.destroy();
        this.width = width;
        this.height = height;
        this._.texture = new _texture2["default"](width, height, gl.RGBA, type);
        this._.spareTexture = new _texture2["default"](width, height, gl.RGBA, type);
        this._.extraTexture = this._.extraTexture || new _texture2["default"](0, 0, gl.RGBA, type);
        this._.flippedShader = this._.flippedShader || new _shader2["default"](null, '\
    uniform sampler2D texture;\
    varying vec2 texCoord;\
    void main() {\
      gl_FragColor = texture2D(texture, vec2(texCoord.x, 1.0 - texCoord.y));\
    }\
  ');
        this._.isInitialized = true;
      }
      /*
         Draw a texture to the canvas, with an optional width and height to scale to.
         If no width and height are given then the original texture width and height
         are used.
      */


      function draw(texture, width, height) {
        if (!this._.isInitialized || texture._.width != this.width || texture._.height != this.height) {
          initialize.call(this, width ? width : texture._.width, height ? height : texture._.height);
        }

        texture._.use();

        this._.texture.drawTo(function () {
          _shader2["default"].getDefaultShader().drawRect();
        });

        return this;
      }

      function update() {
        this._.texture.use();

        this._.flippedShader.drawRect();

        return this;
      }

      function replace(node) {
        node.parentNode.insertBefore(this, node);
        node.parentNode.removeChild(node);
        return this;
      }

      function contents() {
        var gl = store.get('gl');
        var texture = new _texture2["default"](this._.texture.width, this._.texture.height, gl.RGBA, gl.UNSIGNED_BYTE);

        this._.texture.use();

        texture.drawTo(function () {
          _shader2["default"].getDefaultShader().drawRect();
        });
        return wrapTexture(texture);
      }
      /*
         Get a Uint8 array of pixel values: [r, g, b, a, r, g, b, a, ...]
         Length of the array will be width * height * 4.
      */


      function getPixelArray() {
        var gl = store.get('gl');
        var w = this._.texture.width;
        var h = this._.texture.height;
        var array = new Uint8Array(w * h * 4);

        this._.texture.drawTo(function () {
          gl.readPixels(0, 0, w, h, gl.RGBA, gl.UNSIGNED_BYTE, array);
        });

        return array;
      }

      function wrap(func) {
        return function () {
          // Make sure that we're using the correct global WebGL context
          store.set({
            gl: this._.gl
          }); // Now that the context has been switched, we can call the wrapped function

          return func.apply(this, arguments);
        };
      }

      function canvas() {
        var canvas = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : document.createElement('canvas');

        try {
          store.set({
            gl: canvas.getContext('experimental-webgl', {
              premultipliedAlpha: false
            })
          });
        } catch (e) {
          store.set({
            gl: null
          });
        }

        var gl = store.get('gl');

        if (!gl) {
          throw 'This browser does not support WebGL';
        }

        canvas._ = {
          gl: gl,
          isInitialized: false,
          texture: null,
          spareTexture: null,
          flippedShader: null
        }; // Core methods

        canvas.texture = wrap(texture);
        canvas.draw = wrap(draw);
        canvas.update = wrap(update);
        canvas.replace = wrap(replace);
        canvas.contents = wrap(contents);
        canvas.getPixelArray = wrap(getPixelArray); // // Filter methods

        canvas.brightnessContrast = wrap(filters.brightnessContrast);
        canvas.hexagonalPixelate = wrap(filters.hexagonalPixelate);
        canvas.hueSaturation = wrap(filters.hueSaturation);
        canvas.colorHalftone = wrap(filters.colorHalftone);
        canvas.triangleBlur = wrap(filters.triangleBlur);
        canvas.unsharpMask = wrap(filters.unsharpMask);
        canvas.perspective = wrap(filters.perspective);
        canvas.matrixWarp = wrap(filters.matrixWarp);
        canvas.bulgePinch = wrap(filters.bulgePinch);
        canvas.tiltShift = wrap(filters.tiltShift);
        canvas.dotScreen = wrap(filters.dotScreen);
        canvas.edgeWork = wrap(filters.edgeWork);
        canvas.lensBlur = wrap(filters.lensBlur);
        canvas.zoomBlur = wrap(filters.zoomBlur);
        canvas.noise = wrap(filters.noise);
        canvas.denoise = wrap(filters.denoise);
        canvas.curves = wrap(filters.curves);
        canvas.swirl = wrap(filters.swirl);
        canvas.ink = wrap(filters.ink);
        canvas.vignette = wrap(filters.vignette);
        canvas.vibrance = wrap(filters.vibrance);
        canvas.sepia = wrap(filters.sepia);
        return canvas;
      }
      /***/

    },

    /***/
    "./node_modules/glfx-es6/es/matrix.js":
    /*!********************************************!*\
      !*** ./node_modules/glfx-es6/es/matrix.js ***!
      \********************************************/

    /*! no static exports found */

    /***/
    function node_modulesGlfxEs6EsMatrixJs(module, exports, __webpack_require__) {
      "use strict";

      Object.defineProperty(exports, "__esModule", {
        value: true
      });
      exports.getSquareToQuad = getSquareToQuad;
      exports.getInverse = getInverse;
      exports.multiply = multiply; // from javax.media.jai.PerspectiveTransform

      function getSquareToQuad(x0, y0, x1, y1, x2, y2, x3, y3) {
        var dx1 = x1 - x2;
        var dy1 = y1 - y2;
        var dx2 = x3 - x2;
        var dy2 = y3 - y2;
        var dx3 = x0 - x1 + x2 - x3;
        var dy3 = y0 - y1 + y2 - y3;
        var det = dx1 * dy2 - dx2 * dy1;
        var a = (dx3 * dy2 - dx2 * dy3) / det;
        var b = (dx1 * dy3 - dx3 * dy1) / det;
        return [x1 - x0 + a * x1, y1 - y0 + a * y1, a, x3 - x0 + b * x3, y3 - y0 + b * y3, b, x0, y0, 1];
      }

      function getInverse(m) {
        var a = m[0],
            b = m[1],
            c = m[2];
        var d = m[3],
            e = m[4],
            f = m[5];
        var g = m[6],
            h = m[7],
            i = m[8];
        var det = a * e * i - a * f * h - b * d * i + b * f * g + c * d * h - c * e * g;
        return [(e * i - f * h) / det, (c * h - b * i) / det, (b * f - c * e) / det, (f * g - d * i) / det, (a * i - c * g) / det, (c * d - a * f) / det, (d * h - e * g) / det, (b * g - a * h) / det, (a * e - b * d) / det];
      }

      function multiply(a, b) {
        return [a[0] * b[0] + a[1] * b[3] + a[2] * b[6], a[0] * b[1] + a[1] * b[4] + a[2] * b[7], a[0] * b[2] + a[1] * b[5] + a[2] * b[8], a[3] * b[0] + a[4] * b[3] + a[5] * b[6], a[3] * b[1] + a[4] * b[4] + a[5] * b[7], a[3] * b[2] + a[4] * b[5] + a[5] * b[8], a[6] * b[0] + a[7] * b[3] + a[8] * b[6], a[6] * b[1] + a[7] * b[4] + a[8] * b[7], a[6] * b[2] + a[7] * b[5] + a[8] * b[8]];
      }
      /***/

    },

    /***/
    "./node_modules/glfx-es6/es/shader.js":
    /*!********************************************!*\
      !*** ./node_modules/glfx-es6/es/shader.js ***!
      \********************************************/

    /*! no static exports found */

    /***/
    function node_modulesGlfxEs6EsShaderJs(module, exports, __webpack_require__) {
      "use strict";

      Object.defineProperty(exports, "__esModule", {
        value: true
      });

      var _createClass = function () {
        function defineProperties(target, props) {
          for (var i = 0; i < props.length; i++) {
            var descriptor = props[i];
            descriptor.enumerable = descriptor.enumerable || false;
            descriptor.configurable = true;
            if ("value" in descriptor) descriptor.writable = true;
            Object.defineProperty(target, descriptor.key, descriptor);
          }
        }

        return function (Constructor, protoProps, staticProps) {
          if (protoProps) defineProperties(Constructor.prototype, protoProps);
          if (staticProps) defineProperties(Constructor, staticProps);
          return Constructor;
        };
      }();

      var _store = __webpack_require__(
      /*! ./store */
      "./node_modules/glfx-es6/es/store.js");

      var store = _interopRequireWildcard(_store);

      function _interopRequireWildcard(obj) {
        if (obj && obj.__esModule) {
          return obj;
        } else {
          var newObj = {};

          if (obj != null) {
            for (var key in obj) {
              if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key];
            }
          }

          newObj["default"] = obj;
          return newObj;
        }
      }

      function _classCallCheck(instance, Constructor) {
        if (!(instance instanceof Constructor)) {
          throw new TypeError("Cannot call a class as a function");
        }
      }

      var defaultVertexSource = '\
attribute vec2 vertex;\
attribute vec2 _texCoord;\
varying vec2 texCoord;\
void main() {\
  texCoord = _texCoord;\
  gl_Position = vec4(vertex * 2.0 - 1.0, 0.0, 1.0);\
}';
      var defaultFragmentSource = '\
uniform sampler2D texture;\
varying vec2 texCoord;\
void main() {\
  gl_FragColor = texture2D(texture, texCoord);\
}';

      var Shader = function () {
        _createClass(Shader, null, [{
          key: 'getDefaultShader',
          value: function getDefaultShader() {
            var gl = store.get('gl');
            gl.defaultShader = gl.defaultShader || new Shader();
            return gl.defaultShader;
          }
        }]);

        function Shader(vertexSource, fragmentSource) {
          _classCallCheck(this, Shader);

          var gl = store.get('gl');
          this.vertexAttribute = null;
          this.texCoordAttribute = null;
          this.program = gl.createProgram();
          vertexSource = vertexSource || defaultVertexSource;
          fragmentSource = fragmentSource || defaultFragmentSource;
          fragmentSource = 'precision highp float;' + fragmentSource; // annoying requirement is annoying

          gl.attachShader(this.program, compileSource(gl.VERTEX_SHADER, vertexSource));
          gl.attachShader(this.program, compileSource(gl.FRAGMENT_SHADER, fragmentSource));
          gl.linkProgram(this.program);

          if (!gl.getProgramParameter(this.program, gl.LINK_STATUS)) {
            throw 'link error: ' + gl.getProgramInfoLog(this.program);
          }
        }

        _createClass(Shader, [{
          key: 'destroy',
          value: function destroy() {
            var gl = store.get('gl');
            gl.deleteProgram(this.program);
            this.program = null;
          }
        }, {
          key: 'uniforms',
          value: function uniforms(_uniforms) {
            var gl = store.get('gl');
            gl.useProgram(this.program);

            for (var name in _uniforms) {
              if (!_uniforms.hasOwnProperty(name)) continue;
              var location = gl.getUniformLocation(this.program, name);
              if (location === null) continue; // will be null if the uniform isn't used in the shader

              var value = _uniforms[name];

              if (isArray(value)) {
                switch (value.length) {
                  case 1:
                    gl.uniform1fv(location, new Float32Array(value));
                    break;

                  case 2:
                    gl.uniform2fv(location, new Float32Array(value));
                    break;

                  case 3:
                    gl.uniform3fv(location, new Float32Array(value));
                    break;

                  case 4:
                    gl.uniform4fv(location, new Float32Array(value));
                    break;

                  case 9:
                    gl.uniformMatrix3fv(location, false, new Float32Array(value));
                    break;

                  case 16:
                    gl.uniformMatrix4fv(location, false, new Float32Array(value));
                    break;

                  default:
                    throw 'dont\'t know how to load uniform "' + name + '" of length ' + value.length;
                }
              } else if (isNumber(value)) {
                gl.uniform1f(location, value);
              } else {
                throw 'attempted to set uniform "' + name + '" to invalid value ' + (value || 'undefined').toString();
              }
            } // allow chaining


            return this;
          } // textures are uniforms too but for some reason can't be specified by gl.uniform1f,
          // even though floating point numbers represent the integers 0 through 7 exactly

        }, {
          key: 'textures',
          value: function textures(_textures) {
            var gl = store.get('gl');
            gl.useProgram(this.program);

            for (var name in _textures) {
              if (!_textures.hasOwnProperty(name)) continue;
              gl.uniform1i(gl.getUniformLocation(this.program, name), _textures[name]);
            } // allow chaining


            return this;
          }
        }, {
          key: 'drawRect',
          value: function drawRect(left, top, right, bottom) {
            var gl = store.get('gl');
            var viewport = gl.getParameter(gl.VIEWPORT);
            top = top !== undefined ? (top - viewport[1]) / viewport[3] : 0;
            left = left !== undefined ? (left - viewport[0]) / viewport[2] : 0;
            right = right !== undefined ? (right - viewport[0]) / viewport[2] : 1;
            bottom = bottom !== undefined ? (bottom - viewport[1]) / viewport[3] : 1;

            if (gl.vertexBuffer == null) {
              gl.vertexBuffer = gl.createBuffer();
            }

            gl.bindBuffer(gl.ARRAY_BUFFER, gl.vertexBuffer);
            gl.bufferData(gl.ARRAY_BUFFER, new Float32Array([left, top, left, bottom, right, top, right, bottom]), gl.STATIC_DRAW);

            if (gl.texCoordBuffer == null) {
              gl.texCoordBuffer = gl.createBuffer();
              gl.bindBuffer(gl.ARRAY_BUFFER, gl.texCoordBuffer);
              gl.bufferData(gl.ARRAY_BUFFER, new Float32Array([0, 0, 0, 1, 1, 0, 1, 1]), gl.STATIC_DRAW);
            }

            if (this.vertexAttribute == null) {
              this.vertexAttribute = gl.getAttribLocation(this.program, 'vertex');
              gl.enableVertexAttribArray(this.vertexAttribute);
            }

            if (this.texCoordAttribute == null) {
              this.texCoordAttribute = gl.getAttribLocation(this.program, '_texCoord');
              gl.enableVertexAttribArray(this.texCoordAttribute);
            }

            gl.useProgram(this.program);
            gl.bindBuffer(gl.ARRAY_BUFFER, gl.vertexBuffer);
            gl.vertexAttribPointer(this.vertexAttribute, 2, gl.FLOAT, false, 0, 0);
            gl.bindBuffer(gl.ARRAY_BUFFER, gl.texCoordBuffer);
            gl.vertexAttribPointer(this.texCoordAttribute, 2, gl.FLOAT, false, 0, 0);
            gl.drawArrays(gl.TRIANGLE_STRIP, 0, 4);
          }
        }]);

        return Shader;
      }();

      exports["default"] = Shader;

      function isArray(obj) {
        return Object.prototype.toString.call(obj) == '[object Array]';
      }

      function isNumber(obj) {
        return Object.prototype.toString.call(obj) == '[object Number]';
      }

      function compileSource(type, source) {
        var gl = store.get('gl');
        var shader = gl.createShader(type);
        gl.shaderSource(shader, source);
        gl.compileShader(shader);

        if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {
          throw 'compile error: ' + gl.getShaderInfoLog(shader);
        }

        return shader;
      }
      /***/

    },

    /***/
    "./node_modules/glfx-es6/es/spline.js":
    /*!********************************************!*\
      !*** ./node_modules/glfx-es6/es/spline.js ***!
      \********************************************/

    /*! no static exports found */

    /***/
    function node_modulesGlfxEs6EsSplineJs(module, exports, __webpack_require__) {
      "use strict";

      Object.defineProperty(exports, "__esModule", {
        value: true
      });

      var _createClass = function () {
        function defineProperties(target, props) {
          for (var i = 0; i < props.length; i++) {
            var descriptor = props[i];
            descriptor.enumerable = descriptor.enumerable || false;
            descriptor.configurable = true;
            if ("value" in descriptor) descriptor.writable = true;
            Object.defineProperty(target, descriptor.key, descriptor);
          }
        }

        return function (Constructor, protoProps, staticProps) {
          if (protoProps) defineProperties(Constructor.prototype, protoProps);
          if (staticProps) defineProperties(Constructor, staticProps);
          return Constructor;
        };
      }();

      function _classCallCheck(instance, Constructor) {
        if (!(instance instanceof Constructor)) {
          throw new TypeError("Cannot call a class as a function");
        }
      } // from SplineInterpolator.cs in the Paint.NET source code


      var SplineInterpolator = function () {
        function SplineInterpolator(points) {
          _classCallCheck(this, SplineInterpolator);

          var n = points.length;
          this.xa = [];
          this.ya = [];
          this.u = [];
          this.y2 = [];
          points.sort(function (a, b) {
            return a[0] - b[0];
          });

          for (var i = 0; i < n; i++) {
            this.xa.push(points[i][0]);
            this.ya.push(points[i][1]);
          }

          this.u[0] = 0;
          this.y2[0] = 0;

          for (var i = 1; i < n - 1; ++i) {
            // This is the decomposition loop of the tridiagonal algorithm.
            // y2 and u are used for temporary storage of the decomposed factors.
            var wx = this.xa[i + 1] - this.xa[i - 1];
            var sig = (this.xa[i] - this.xa[i - 1]) / wx;
            var p = sig * this.y2[i - 1] + 2.0;
            this.y2[i] = (sig - 1.0) / p;
            var ddydx = (this.ya[i + 1] - this.ya[i]) / (this.xa[i + 1] - this.xa[i]) - (this.ya[i] - this.ya[i - 1]) / (this.xa[i] - this.xa[i - 1]);
            this.u[i] = (6.0 * ddydx / wx - sig * this.u[i - 1]) / p;
          }

          this.y2[n - 1] = 0; // This is the backsubstitution loop of the tridiagonal algorithm

          for (var i = n - 2; i >= 0; --i) {
            this.y2[i] = this.y2[i] * this.y2[i + 1] + this.u[i];
          }
        }

        _createClass(SplineInterpolator, [{
          key: "interpolate",
          value: function interpolate(x) {
            var n = this.ya.length;
            var klo = 0;
            var khi = n - 1; // We will find the right place in the table by means of
            // bisection. This is optimal if sequential calls to this
            // routine are at random values of x. If sequential calls
            // are in order, and closely spaced, one would do better
            // to store previous values of klo and khi.

            while (khi - klo > 1) {
              var k = khi + klo >> 1;

              if (this.xa[k] > x) {
                khi = k;
              } else {
                klo = k;
              }
            }

            var h = this.xa[khi] - this.xa[klo];
            var a = (this.xa[khi] - x) / h;
            var b = (x - this.xa[klo]) / h; // Cubic spline polynomial is now evaluated.

            return a * this.ya[klo] + b * this.ya[khi] + ((a * a * a - a) * this.y2[klo] + (b * b * b - b) * this.y2[khi]) * (h * h) / 6.0;
          }
        }]);

        return SplineInterpolator;
      }();

      exports["default"] = SplineInterpolator;
      /***/
    },

    /***/
    "./node_modules/glfx-es6/es/store.js":
    /*!*******************************************!*\
      !*** ./node_modules/glfx-es6/es/store.js ***!
      \*******************************************/

    /*! no static exports found */

    /***/
    function node_modulesGlfxEs6EsStoreJs(module, exports, __webpack_require__) {
      "use strict";

      Object.defineProperty(exports, "__esModule", {
        value: true
      });
      exports.set = set;
      exports.get = get;
      var store = {};

      function set(obj) {
        store = Object.assign(store, obj);
      }

      function get(key) {
        return store[key];
      }
      /***/

    },

    /***/
    "./node_modules/glfx-es6/es/texture.js":
    /*!*********************************************!*\
      !*** ./node_modules/glfx-es6/es/texture.js ***!
      \*********************************************/

    /*! no static exports found */

    /***/
    function node_modulesGlfxEs6EsTextureJs(module, exports, __webpack_require__) {
      "use strict";

      Object.defineProperty(exports, "__esModule", {
        value: true
      });

      var _createClass = function () {
        function defineProperties(target, props) {
          for (var i = 0; i < props.length; i++) {
            var descriptor = props[i];
            descriptor.enumerable = descriptor.enumerable || false;
            descriptor.configurable = true;
            if ("value" in descriptor) descriptor.writable = true;
            Object.defineProperty(target, descriptor.key, descriptor);
          }
        }

        return function (Constructor, protoProps, staticProps) {
          if (protoProps) defineProperties(Constructor.prototype, protoProps);
          if (staticProps) defineProperties(Constructor, staticProps);
          return Constructor;
        };
      }();

      var _store = __webpack_require__(
      /*! ./store */
      "./node_modules/glfx-es6/es/store.js");

      var store = _interopRequireWildcard(_store);

      var _shader = __webpack_require__(
      /*! ./shader */
      "./node_modules/glfx-es6/es/shader.js");

      var _shader2 = _interopRequireDefault(_shader);

      function _interopRequireDefault(obj) {
        return obj && obj.__esModule ? obj : {
          "default": obj
        };
      }

      function _interopRequireWildcard(obj) {
        if (obj && obj.__esModule) {
          return obj;
        } else {
          var newObj = {};

          if (obj != null) {
            for (var key in obj) {
              if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key];
            }
          }

          newObj["default"] = obj;
          return newObj;
        }
      }

      function _classCallCheck(instance, Constructor) {
        if (!(instance instanceof Constructor)) {
          throw new TypeError("Cannot call a class as a function");
        }
      }

      var canvas = null;

      var Texture = function () {
        _createClass(Texture, null, [{
          key: 'fromElement',
          value: function fromElement(element) {
            var gl = store.get('gl');
            var texture = new Texture(0, 0, gl.RGBA, gl.UNSIGNED_BYTE);
            texture.loadContentsOf(element);
            return texture;
          }
        }]);

        function Texture(width, height, format, type) {
          _classCallCheck(this, Texture);

          var gl = store.get('gl');
          this.gl = gl;
          this.id = gl.createTexture();
          this.width = width;
          this.height = height;
          this.format = format;
          this.type = type;
          gl.bindTexture(gl.TEXTURE_2D, this.id);
          gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR);
          gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
          gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
          gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
          if (width && height) gl.texImage2D(gl.TEXTURE_2D, 0, this.format, width, height, 0, this.format, this.type, null);
        }

        _createClass(Texture, [{
          key: 'loadContentsOf',
          value: function loadContentsOf(element) {
            var gl = store.get('gl');
            this.width = element.width || element.videoWidth;
            this.height = element.height || element.videoHeight;
            gl.bindTexture(gl.TEXTURE_2D, this.id);
            gl.texImage2D(gl.TEXTURE_2D, 0, this.format, this.format, this.type, element);
          }
        }, {
          key: 'initFromBytes',
          value: function initFromBytes(width, height, data) {
            var gl = store.get('gl');
            this.width = width;
            this.height = height;
            this.format = gl.RGBA;
            this.type = gl.UNSIGNED_BYTE;
            gl.bindTexture(gl.TEXTURE_2D, this.id);
            gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, width, height, 0, gl.RGBA, this.type, new Uint8Array(data));
          }
        }, {
          key: 'destroy',
          value: function destroy() {
            var gl = store.get('gl');
            gl.deleteTexture(this.id);
            this.id = null;
          }
        }, {
          key: 'use',
          value: function use(unit) {
            var gl = store.get('gl');
            gl.activeTexture(gl.TEXTURE0 + (unit || 0));
            gl.bindTexture(gl.TEXTURE_2D, this.id);
          }
        }, {
          key: 'unuse',
          value: function unuse(unit) {
            var gl = store.get('gl');
            gl.activeTexture(gl.TEXTURE0 + (unit || 0));
            gl.bindTexture(gl.TEXTURE_2D, null);
          }
        }, {
          key: 'ensureFormat',
          value: function ensureFormat(width, height, format, type) {
            // allow passing an existing texture instead of individual arguments
            if (arguments.length == 1) {
              var texture = arguments[0];
              width = texture.width;
              height = texture.height;
              format = texture.format;
              type = texture.type;
            } // change the format only if required


            if (width != this.width || height != this.height || format != this.format || type != this.type) {
              var gl = store.get('gl');
              this.width = width;
              this.height = height;
              this.format = format;
              this.type = type;
              gl.bindTexture(gl.TEXTURE_2D, this.id);
              gl.texImage2D(gl.TEXTURE_2D, 0, this.format, width, height, 0, this.format, this.type, null);
            }
          }
        }, {
          key: 'drawTo',
          value: function drawTo(callback) {
            var gl = store.get('gl'); // start rendering to this texture

            gl.framebuffer = gl.framebuffer || gl.createFramebuffer();
            gl.bindFramebuffer(gl.FRAMEBUFFER, gl.framebuffer);
            gl.framebufferTexture2D(gl.FRAMEBUFFER, gl.COLOR_ATTACHMENT0, gl.TEXTURE_2D, this.id, 0);

            if (gl.checkFramebufferStatus(gl.FRAMEBUFFER) !== gl.FRAMEBUFFER_COMPLETE) {
              throw new Error('incomplete framebuffer');
            }

            gl.viewport(0, 0, this.width, this.height); // do the drawing

            callback(); // stop rendering to this texture

            gl.bindFramebuffer(gl.FRAMEBUFFER, null);
          }
        }, {
          key: 'fillUsingCanvas',
          value: function fillUsingCanvas(callback) {
            callback(getCanvas(this));
            var gl = store.get('gl');
            this.format = gl.RGBA;
            this.type = gl.UNSIGNED_BYTE;
            gl.bindTexture(gl.TEXTURE_2D, this.id);
            gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, canvas);
            return this;
          }
        }, {
          key: 'toImage',
          value: function toImage(image) {
            this.use();

            _shader2["default"].getDefaultShader().drawRect();

            var gl = store.get('gl');
            var size = this.width * this.height * 4;
            var pixels = new Uint8Array(size);
            var c = getCanvas(this);
            var data = c.createImageData(this.width, this.height);
            gl.readPixels(0, 0, this.width, this.height, gl.RGBA, gl.UNSIGNED_BYTE, pixels);

            for (var i = 0; i < size; i++) {
              data.data[i] = pixels[i];
            }

            c.putImageData(data, 0, 0);
            image.src = canvas.toDataURL();
          }
        }, {
          key: 'swapWith',
          value: function swapWith(other) {
            var temp;
            temp = other.id;
            other.id = this.id;
            this.id = temp;
            temp = other.width;
            other.width = this.width;
            this.width = temp;
            temp = other.height;
            other.height = this.height;
            this.height = temp;
            temp = other.format;
            other.format = this.format;
            this.format = temp;
          }
        }]);

        return Texture;
      }();

      exports["default"] = Texture;

      function getCanvas(texture) {
        if (canvas == null) canvas = document.createElement('canvas');
        canvas.width = texture.width;
        canvas.height = texture.height;
        var c = canvas.getContext('2d');
        c.clearRect(0, 0, canvas.width, canvas.height);
        return c;
      }
      /***/

    },

    /***/
    "./node_modules/glfx-es6/es/util.js":
    /*!******************************************!*\
      !*** ./node_modules/glfx-es6/es/util.js ***!
      \******************************************/

    /*! no static exports found */

    /***/
    function node_modulesGlfxEs6EsUtilJs(module, exports, __webpack_require__) {
      "use strict";

      Object.defineProperty(exports, "__esModule", {
        value: true
      });
      exports.simpleShader = simpleShader;
      exports.clamp = clamp;
      exports.splineInterpolate = splineInterpolate;

      var _spline = __webpack_require__(
      /*! ./spline */
      "./node_modules/glfx-es6/es/spline.js");

      var _spline2 = _interopRequireDefault(_spline);

      function _interopRequireDefault(obj) {
        return obj && obj.__esModule ? obj : {
          "default": obj
        };
      }

      function simpleShader(shader, uniforms, textureIn, textureOut) {
        (textureIn || this._.texture).use();

        this._.spareTexture.drawTo(function () {
          shader.uniforms(uniforms).drawRect();
        });

        this._.spareTexture.swapWith(textureOut || this._.texture);
      }

      function clamp(lo, value, hi) {
        return Math.max(lo, Math.min(value, hi));
      }

      function splineInterpolate(points) {
        var interpolator = new _spline2["default"](points);
        var array = [];

        for (var i = 0; i < 256; i++) {
          array.push(clamp(0, Math.floor(interpolator.interpolate(i / 255) * 256), 255));
        }

        return array;
      }
      /***/

    },

    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/preview-filter/preview-filter.page.html":
    /*!***********************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/preview-filter/preview-filter.page.html ***!
      \***********************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppPreviewFilterPreviewFilterPageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<app-header>\r\n\r\n</app-header>\r\n<ion-content>\r\n    <!-- <img [src]=\"originalImage\" *ngIf=\"originalImage\" style=\"image-rendering: pixelated;height: 100%; width:100%;object-fit: initial;\"> -->\r\n    <div id=\"container\" class=\"img-container\" *ngIf=\"type == 'image'\">\r\n        <!-- <img id=\"image\" src=\"../../assets/image/4.jpg\" /> -->\r\n        <!-- <img [src]=\"canvasImage\" /> -->\r\n        <img id=\"imageSrc\" #imageSrc [src]=\"originalImage\" />\r\n        <ion-item lines=\"none\" class=\"ws-100\" *ngIf=\"property && enableEditMode == true\" id=\"save-options\" mode=\"md\">\r\n            <ion-button fill=\"clear\" slot=\"end\" class=\"save_btn\" (click)=\"saveChanges()\" mode=\"md\">Save</ion-button>\r\n        </ion-item>\r\n        <div id=\"options\" *ngIf=\"property == 'Filters' && enableEditMode == true\">\r\n            <!-- <ion-item lines=\"none\" mode=\"md\">\r\n                <span>Contrast</span>\r\n                <ion-range id=\"brightness-slider\" name=\"brightness\" min=\"1\" max=\"200\" value=\"50\" color=\"yellow\" mode=\"md\"\r\n                    (ionChange)=\"setFilter('contrast',$event.target.value,'%','filter')\" data-sizing=\"%\" step=\"2\">\r\n                </ion-range>\r\n            </ion-item>\r\n            <ion-item lines=\"none\" mode=\"md\">\r\n                <span>Saturation</span>\r\n                <ion-range id=\"brightness-slider\" name=\"brightness\" min=\"1\" max=\"200\" value=\"50\" color=\"yellow\"\r\n                    (ionChange)=\"setFilter('saturate',$event.target.value,'%','filter')\" data-sizing=\"%\" step=\"2\" mode=\"md\">\r\n                </ion-range>\r\n            </ion-item>\r\n            <ion-item lines=\"none\" mode=\"md\">\r\n                <span>Brightness</span>\r\n                <ion-range id=\"brightness-slider\" name=\"brightness\" min=\"20\" max=\"200\" value=\"50\" color=\"yellow\"\r\n                    (ionChange)=\"setFilter('brightness',$event.target.value,'%','filter')\" data-sizing=\"%\" step=\"2\" mode=\"md\">\r\n                </ion-range>\r\n            </ion-item>\r\n            <ion-item lines=\"none\" mode=\"md\">\r\n                <span>Hue Rotate</span>\r\n                <ion-range id=\"brightness-slider\" name=\"brightness\" min=\"1\" max=\"200\" value=\"50\" color=\"yellow\"\r\n                    (ionChange)=\"setFilter('hue-rotate',$event.target.value,'deg','filter')\" data-sizing=\"%\" step=\"2\" mode=\"md\">\r\n                </ion-range>\r\n            </ion-item> -->\r\n\r\n            <ion-item lines=\"none\" mode=\"md\">\r\n                <!-- <span>Contrast {{contrast.value}}</span> -->\r\n                <ion-range id=\"brightness-slider\" #contrast1 name=\"Contrast\" min=\"-100\" max=\"100\" [(ngModel)]=\"contrast\"\r\n                    color=\"yellow\" mode=\"md\" (ionChange)=\"imageFilter(contrast, saturation, brightness, hue, false)\"\r\n                    step=\"1\">\r\n                    <ion-label slot=\"start\" class=\"label-width\">Contrast</ion-label>\r\n                </ion-range>\r\n            </ion-item>\r\n            <ion-item lines=\"none\" mode=\"md\">\r\n                <!-- <span>Saturation {{saturation}}</span> -->\r\n                <ion-range id=\"brightness-slider\" #saturate1 name=\"Saturate\" min=\"-100\" max=\"100\"\r\n                    [(ngModel)]=\"saturation\" color=\"yellow\"\r\n                    (ionChange)=\"imageFilter(contrast, saturation, brightness, hue, false)\" step=\"1\" mode=\"md\">\r\n                    <ion-label slot=\"start\" class=\"label-width\">Saturation</ion-label>\r\n\r\n                </ion-range>\r\n            </ion-item>\r\n            <ion-item lines=\"none\" mode=\"md\">\r\n                <!-- <span>Brightness {{brightness}}</span> -->\r\n                <ion-range id=\"brightness-slider\" #brightness1 name=\"Brightness\" min=\"-100\" max=\"100\"\r\n                    [(ngModel)]=\"brightness\" color=\"yellow\"\r\n                    (ionChange)=\"imageFilter(contrast, saturation, brightness, hue, false)\" step=\"1\" mode=\"md\">\r\n                    <ion-label slot=\"start\" class=\"label-width\">Brightness</ion-label>\r\n\r\n                </ion-range>\r\n            </ion-item>\r\n            <ion-item lines=\"none\" mode=\"md\">\r\n                <!-- <span>Hue {{hue}}</span> -->\r\n                <ion-range id=\"brightness-slider\" #hue1 name=\"Hue\" min=\"-100\" max=\"100\" [(ngModel)]=\"hue\" color=\"yellow\"\r\n                    (ionChange)=\"imageFilter(contrast, saturation, brightness, hue, false)\" step=\"1\" mode=\"md\">\r\n                    <ion-label slot=\"start\" class=\"label-width\">Hue</ion-label>\r\n\r\n                </ion-range>\r\n            </ion-item>\r\n        </div>\r\n\r\n        <div id=\"options\" *ngIf=\"property == 'Orientation'\">\r\n            <ion-item lines=\"none\" mode=\"md\">\r\n                <ion-label (click)=\"setFilter('rotate','0','deg','transform')\" mode=\"md\">\r\n                    <span>Rotate</span></ion-label>\r\n                <ion-icon name=\"arrow-undo-circle-outline\" (click)=\"setFilter('rotate','0','deg','transform')\"\r\n                    slot=\"end\" mode=\"md\"></ion-icon>\r\n                <!-- <ion-label *ngIf=\"cntValue && cntValue == '-90'\" mode=\"md\"><span>Rotate</span></ion-label>\r\n                <ion-label *ngIf=\"cntValue && cntValue != '-90'\" (click)=\"setFilter('rotate','-90','deg','transform')\" mode=\"md\">\r\n                    <span>Rotate</span></ion-label>\r\n                <ion-icon name=\"arrow-undo-circle-outline\" (click)=\"setFilter('rotate','0','deg','transform')\"\r\n                    slot=\"end\" mode=\"md\"></ion-icon> -->\r\n            </ion-item>\r\n            <ion-item lines=\"none\">\r\n                <ion-label (click)=\"setFilter('rotateY','180','deg','transform')\" mode=\"md\"><span>Mirror</span>\r\n                </ion-label>\r\n                <ion-icon name=\"arrow-undo-circle-outline\" (click)=\"setFilter('rotateY','180','deg','transform')\"\r\n                    slot=\"end\" mode=\"md\"></ion-icon>\r\n                <!-- <ion-label *ngIf=\"cntValue && cntValue == '180'\" mode=\"md\"><span>Mirror</span></ion-label>\r\n                <ion-label *ngIf=\"cntValue && cntValue != '180'\" (click)=\"setFilter('rotateY','180','deg','transform')\" mode=\"md\">\r\n                    <span>Mirror</span></ion-label>\r\n                <ion-icon name=\"arrow-undo-circle-outline\" (click)=\"setFilter('rotateY','360','deg','transform')\"\r\n                    slot=\"end\" mode=\"md\"></ion-icon> -->\r\n            </ion-item>\r\n        </div>\r\n        <ion-toolbar *ngIf=\"enableEditMode == true\" id=\"text\" mode=\"md\">\r\n            <ion-tabs mode=\"md\">\r\n                <ion-tab-bar mode=\"md\">\r\n                    <ion-tab-button [ngClass]=\"{'selected-filter' : property == 'Filters'}\"\r\n                        (click)=\"selectedFilterProperty('Filters')\" mode=\"md\" class=\"background-color\">\r\n                        <ion-icon name=\"contrast-outline\" mode=\"md\"></ion-icon>\r\n                        <ion-label mode=\"md\">Filters</ion-label>\r\n                    </ion-tab-button>\r\n                    <ion-tab-button [ngClass]=\"{'selected-filter' : property == 'Crop'}\"\r\n                        (click)=\"cropImage(originalImage);\" mode=\"md\" class=\"background-color\">\r\n                        <ion-icon name=\"crop-outline\" mode=\"md\"></ion-icon>\r\n                        <ion-label mode=\"md\">Crop & Orientation</ion-label>\r\n                    </ion-tab-button>\r\n                    <!-- <ion-tab-button [ngClass]=\"{'selected-filter' : property == 'Orientation'}\"\r\n                        (click)=\"selectedFilterProperty('Orientation')\" mode=\"md\">\r\n                        <ion-icon name=\"camera-reverse-outline\" mode=\"md\"></ion-icon>\r\n                        <ion-label mode=\"md\">Orientation</ion-label>\r\n                    </ion-tab-button> -->\r\n                </ion-tab-bar>\r\n            </ion-tabs>\r\n        </ion-toolbar>\r\n    </div>\r\n    <div class=\"video-player\" *ngIf=\"type == 'video'\">\r\n        <video #videoElement id=\"videoElement\" [poster]=\"posterImage\" controls playsinline webkit-playsinline\r\n            preload=\"auto\" muted=\"true\" controlsList=\"nodownload\">\r\n            <source [src]=\"originalImage\" type=\"video/mp4\" />\r\n            <!-- <source src=\"../../assets/videos/sample.mp4\" type=\"video/mp4\" /> -->\r\n        </video>\r\n    </div>\r\n\r\n</ion-content>\r\n\r\n<ion-toolbar color=\"yellow\" slot=\"bottom\">\r\n    <ion-tabs>\r\n        <ion-tab-bar color=\"yellow\" slot=\"bottom\">\r\n\r\n            <ion-tab-button (click)=\"goBack()\">\r\n                <ion-icon name=\"caret-back-outline\"></ion-icon>\r\n                <ion-label>Back</ion-label>\r\n            </ion-tab-button>\r\n            <ion-tab-button (click)=\"submitFileData()\">\r\n                <ion-icon name=\"checkmark-circle-outline\"></ion-icon>\r\n                <ion-label>Next</ion-label>\r\n            </ion-tab-button>\r\n            <ion-tab-button *ngIf=\"type == 'video'\">\r\n            </ion-tab-button>\r\n            <ion-tab-button *ngIf=\"type == 'image'\" (click)=\"enableEdit()\">\r\n                <ion-icon name=\"create-outline\"></ion-icon>\r\n                <ion-label>Edit</ion-label>\r\n            </ion-tab-button>\r\n        </ion-tab-bar>\r\n    </ion-tabs>\r\n</ion-toolbar>";
      /***/
    },

    /***/
    "./src/app/preview-filter/preview-filter-routing.module.ts":
    /*!*****************************************************************!*\
      !*** ./src/app/preview-filter/preview-filter-routing.module.ts ***!
      \*****************************************************************/

    /*! exports provided: PreviewFilterPageRoutingModule */

    /***/
    function srcAppPreviewFilterPreviewFilterRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "PreviewFilterPageRoutingModule", function () {
        return PreviewFilterPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _preview_filter_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./preview-filter.page */
      "./src/app/preview-filter/preview-filter.page.ts");

      var routes = [{
        path: '',
        component: _preview_filter_page__WEBPACK_IMPORTED_MODULE_3__["PreviewFilterPage"]
      }];

      var PreviewFilterPageRoutingModule = function PreviewFilterPageRoutingModule() {
        _classCallCheck2(this, PreviewFilterPageRoutingModule);
      };

      PreviewFilterPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], PreviewFilterPageRoutingModule);
      /***/
    },

    /***/
    "./src/app/preview-filter/preview-filter.module.ts":
    /*!*********************************************************!*\
      !*** ./src/app/preview-filter/preview-filter.module.ts ***!
      \*********************************************************/

    /*! exports provided: PreviewFilterPageModule */

    /***/
    function srcAppPreviewFilterPreviewFilterModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "PreviewFilterPageModule", function () {
        return PreviewFilterPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _preview_filter_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./preview-filter-routing.module */
      "./src/app/preview-filter/preview-filter-routing.module.ts");
      /* harmony import */


      var _preview_filter_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./preview-filter.page */
      "./src/app/preview-filter/preview-filter.page.ts");
      /* harmony import */


      var _header_header_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! ../header/header.module */
      "./src/app/header/header.module.ts"); //


      var PreviewFilterPageModule = function PreviewFilterPageModule() {
        _classCallCheck2(this, PreviewFilterPageModule);
      };

      PreviewFilterPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _preview_filter_routing_module__WEBPACK_IMPORTED_MODULE_5__["PreviewFilterPageRoutingModule"], _header_header_module__WEBPACK_IMPORTED_MODULE_7__["HeaderPageModule"]],
        declarations: [_preview_filter_page__WEBPACK_IMPORTED_MODULE_6__["PreviewFilterPage"]]
      })], PreviewFilterPageModule);
      /***/
    },

    /***/
    "./src/app/preview-filter/preview-filter.page.scss":
    /*!*********************************************************!*\
      !*** ./src/app/preview-filter/preview-filter.page.scss ***!
      \*********************************************************/

    /*! exports provided: default */

    /***/
    function srcAppPreviewFilterPreviewFilterPageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "#container {\n  height: 100%;\n  width: 100%;\n}\n#container ion-toolbar,\n#container ion-item {\n  --background: #252523d6;\n}\n#container ion-toolbar *,\n#container ion-toolbar *::before,\n#container ion-toolbar *::after,\n#container ion-item *,\n#container ion-item *::before,\n#container ion-item *::after {\n  box-sizing: border-box;\n  --background: #252523d6;\n  color: #f6c762;\n}\n#container .selected-filter {\n  --background: #f6c76200;\n}\n#container .selected-filter *,\n#container .selected-filter *::before,\n#container .selected-filter *::after {\n  box-sizing: border-box;\n  --background: #3535356e;\n  color: #f8f9fa;\n}\n#container .save_btn {\n  --background: #f6c762 !important;\n  color: #101010 !important;\n  width: 20% !important;\n  font-weight: bold !important;\n}\n#text {\n  z-index: 100;\n  position: absolute;\n  color: white;\n  font-weight: bold;\n  font-weight: bold;\n  left: 0;\n  bottom: 0;\n}\n#options {\n  position: absolute;\n  z-index: 100;\n  left: 0;\n  bottom: 55px;\n  width: 100%;\n  background: #35373938;\n  transform: translate(0, 0);\n}\n.save-options {\n  position: absolute;\n  z-index: 100;\n  left: 0;\n  top: 0;\n  width: 100%;\n  background: #35373938;\n}\n.sliders {\n  display: block;\n  margin: 5px 0;\n  text-align: left;\n}\n.video-player {\n  background: #1f1e1d;\n  width: 100%;\n  height: 100%;\n}\n.video-player video {\n  width: 100%;\n  height: 100%;\n  position: absolute;\n}\n.img-container {\n  background: #1f1e1d;\n  /* image-rendering: pixelated; */\n  height: 100%;\n  width: 100%;\n  -o-object-fit: contain;\n     object-fit: contain;\n  /* object-fit: contain; */\n  display: flex;\n  flex-direction: row;\n}\n.img-container img {\n  height: auto;\n  width: 100%;\n  -o-object-fit: contain;\n     object-fit: contain;\n  -o-object-position: center;\n     object-position: center;\n  visibility: hidden;\n}\n.canvas {\n  -o-object-fit: contain !important;\n     object-fit: contain !important;\n  -o-object-position: center !important;\n     object-position: center !important;\n}\n.label-width {\n  width: 20%;\n}\n/********************\n    Apply Variables\n********************/\n#image {\n  margin: auto;\n  vertical-align: bottom;\n  -webkit-clip-path: var(--clip-path);\n          clip-path: var(--clip-path);\n  border-color: var(--border-color);\n  border-radius: var(--border-radius);\n  filter: blur(var(--blur)) brightness(var(--brightness)) contrast(var(--contrast));\n}\n.ws-100 {\n  width: 100% !important;\n  position: absolute;\n}\nion-tab-button.background-color {\n  background: transparent;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcHJldmlldy1maWx0ZXIvcHJldmlldy1maWx0ZXIucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksWUFBQTtFQUNBLFdBQUE7QUFDSjtBQUNJOztFQUVJLHVCQUFBO0FBQ1I7QUFBUTs7Ozs7O0VBR0ksc0JBQUE7RUFDQSx1QkFBQTtFQUNBLGNBQUE7QUFLWjtBQUZJO0VBQ0ksdUJBQUE7QUFJUjtBQUhROzs7RUFHSSxzQkFBQTtFQUNBLHVCQUFBO0VBQ0EsY0FBQTtBQUtaO0FBRkk7RUFDSSxnQ0FBQTtFQUNBLHlCQUFBO0VBQ0EscUJBQUE7RUFDQSw0QkFBQTtBQUlSO0FBTUE7RUFDSSxZQUFBO0VBQ0Esa0JBQUE7RUFDQSxZQUFBO0VBQ0EsaUJBQUE7RUFDQSxpQkFBQTtFQUNBLE9BQUE7RUFDQSxTQUFBO0FBSEo7QUFNQTtFQUNJLGtCQUFBO0VBQ0EsWUFBQTtFQUNBLE9BQUE7RUFDQSxZQUFBO0VBQ0EsV0FBQTtFQUNBLHFCQUFBO0VBQ0EsMEJBQUE7QUFISjtBQU1BO0VBQ0ksa0JBQUE7RUFDQSxZQUFBO0VBQ0EsT0FBQTtFQUNBLE1BQUE7RUFDQSxXQUFBO0VBQ0EscUJBQUE7QUFISjtBQU1BO0VBQ0ksY0FBQTtFQUNBLGFBQUE7RUFDQSxnQkFBQTtBQUhKO0FBTUE7RUFDSSxtQkFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0FBSEo7QUFNSTtFQUNJLFdBQUE7RUFDQSxZQUFBO0VBQ0Esa0JBQUE7QUFKUjtBQVFBO0VBTUksbUJBQUE7RUFDQSxnQ0FBQTtFQUNBLFlBQUE7RUFDQSxXQUFBO0VBQ0Esc0JBQUE7S0FBQSxtQkFBQTtFQUNBLHlCQUFBO0VBRUEsYUFBQTtFQUNBLG1CQUFBO0FBWEo7QUFZSTtFQUNJLFlBQUE7RUFHQSxXQUFBO0VBR0Esc0JBQUE7S0FBQSxtQkFBQTtFQUNBLDBCQUFBO0tBQUEsdUJBQUE7RUFDQSxrQkFBQTtBQWRSO0FBcUJBO0VBQ0ksaUNBQUE7S0FBQSw4QkFBQTtFQUNBLHFDQUFBO0tBQUEsa0NBQUE7QUFsQko7QUFxQkE7RUFDSSxVQUFBO0FBbEJKO0FBb0JBOztvQkFBQTtBQUlBO0VBQ0ksWUFBQTtFQUNBLHNCQUFBO0VBQ0EsbUNBQUE7VUFBQSwyQkFBQTtFQUNBLGlDQUFBO0VBQ0EsbUNBQUE7RUFHQSxpRkFBQTtBQXBCSjtBQXVCQTtFQUNJLHNCQUFBO0VBQ0Esa0JBQUE7QUFwQko7QUF3Qkk7RUFDSSx1QkFBQTtBQXJCUiIsImZpbGUiOiJzcmMvYXBwL3ByZXZpZXctZmlsdGVyL3ByZXZpZXctZmlsdGVyLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIiNjb250YWluZXIge1xyXG4gICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICAvLyBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICBpb24tdG9vbGJhcixcclxuICAgIGlvbi1pdGVtIHtcclxuICAgICAgICAtLWJhY2tncm91bmQ6ICMyNTI1MjNkNjtcclxuICAgICAgICAqLFxyXG4gICAgICAgICo6OmJlZm9yZSxcclxuICAgICAgICAqOjphZnRlciB7XHJcbiAgICAgICAgICAgIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XHJcbiAgICAgICAgICAgIC0tYmFja2dyb3VuZDogIzI1MjUyM2Q2O1xyXG4gICAgICAgICAgICBjb2xvcjogI2Y2Yzc2MjtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbiAgICAuc2VsZWN0ZWQtZmlsdGVyIHtcclxuICAgICAgICAtLWJhY2tncm91bmQ6ICNmNmM3NjIwMDtcclxuICAgICAgICAqLFxyXG4gICAgICAgICo6OmJlZm9yZSxcclxuICAgICAgICAqOjphZnRlciB7XHJcbiAgICAgICAgICAgIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XHJcbiAgICAgICAgICAgIC0tYmFja2dyb3VuZDogIzM1MzUzNTZlO1xyXG4gICAgICAgICAgICBjb2xvcjogI2Y4ZjlmYTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbiAgICAuc2F2ZV9idG4ge1xyXG4gICAgICAgIC0tYmFja2dyb3VuZDogI2Y2Yzc2MiAhaW1wb3J0YW50O1xyXG4gICAgICAgIGNvbG9yOiAjMTAxMDEwICFpbXBvcnRhbnQ7XHJcbiAgICAgICAgd2lkdGg6IDIwJSAhaW1wb3J0YW50O1xyXG4gICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkICFpbXBvcnRhbnQ7XHJcbiAgICB9XHJcbn1cclxuXHJcbiNpbWFnZSB7XHJcbiAgICAvLyBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAvLyBsZWZ0OiAwO1xyXG4gICAgLy8gdG9wOiAwO1xyXG59XHJcblxyXG4jdGV4dCB7XHJcbiAgICB6LWluZGV4OiAxMDA7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICBjb2xvcjogd2hpdGU7XHJcbiAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgbGVmdDogMDtcclxuICAgIGJvdHRvbTogMDtcclxufVxyXG5cclxuI29wdGlvbnMge1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgei1pbmRleDogMTAwO1xyXG4gICAgbGVmdDogMDtcclxuICAgIGJvdHRvbTogNTVweDtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgYmFja2dyb3VuZDogIzM1MzczOTM4O1xyXG4gICAgdHJhbnNmb3JtOiB0cmFuc2xhdGUoMCwgMCk7XHJcbn1cclxuXHJcbi5zYXZlLW9wdGlvbnMge1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgei1pbmRleDogMTAwO1xyXG4gICAgbGVmdDogMDtcclxuICAgIHRvcDogMDtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgYmFja2dyb3VuZDogIzM1MzczOTM4O1xyXG59XHJcblxyXG4uc2xpZGVycyB7XHJcbiAgICBkaXNwbGF5OiBibG9jaztcclxuICAgIG1hcmdpbjogNXB4IDA7XHJcbiAgICB0ZXh0LWFsaWduOiBsZWZ0O1xyXG59XHJcblxyXG4udmlkZW8tcGxheWVyIHtcclxuICAgIGJhY2tncm91bmQ6ICMxZjFlMWQ7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGhlaWdodDogMTAwJTtcclxuXHJcbiAgICBcclxuICAgIHZpZGVvIHtcclxuICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgfVxyXG59XHJcblxyXG4uaW1nLWNvbnRhaW5lciB7XHJcbiAgICAvLyBiYWNrZ3JvdW5kOiAjMWYxZTFkO1xyXG4gICAgLy8gaW1hZ2UtcmVuZGVyaW5nOiBwaXhlbGF0ZWQ7XHJcbiAgICAvLyBoZWlnaHQ6IDEwMCU7XHJcbiAgICAvLyB3aWR0aDogMTAwJTtcclxuICAgIC8vIG9iamVjdC1maXQ6IGNvbnRhaW47XHJcbiAgICBiYWNrZ3JvdW5kOiAjMWYxZTFkO1xyXG4gICAgLyogaW1hZ2UtcmVuZGVyaW5nOiBwaXhlbGF0ZWQ7ICovXHJcbiAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIG9iamVjdC1maXQ6IGNvbnRhaW47XHJcbiAgICAvKiBvYmplY3QtZml0OiBjb250YWluOyAqL1xyXG4gICAgLy8gcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XHJcbiAgICBpbWcge1xyXG4gICAgICAgIGhlaWdodDogYXV0bztcclxuICAgICAgICAvLyBtYXgtaGVpZ2h0OiAxMDAlO1xyXG4gICAgICAgIC8vIG1pbi1oZWlnaHQ6IG1heC1jb250ZW50O1xyXG4gICAgICAgIHdpZHRoOiAxMDAlO1xyXG5cclxuICAgICAgICAvLyB2ZXJ0aWNhbC1hbGlnbjogbWlkZGxlICFpbXBvcnRhbnQ7XHJcbiAgICAgICAgb2JqZWN0LWZpdDogY29udGFpbjtcclxuICAgICAgICBvYmplY3QtcG9zaXRpb246IGNlbnRlcjtcclxuICAgICAgICB2aXNpYmlsaXR5OiBoaWRkZW47XHJcblxyXG4gICAgXHJcbiAgICB9XHJcblxyXG59XHJcblxyXG4uY2FudmFze1xyXG4gICAgb2JqZWN0LWZpdDogY29udGFpbiFpbXBvcnRhbnQ7XHJcbiAgICBvYmplY3QtcG9zaXRpb246IGNlbnRlciFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbi5sYWJlbC13aWR0aHtcclxuICAgIHdpZHRoOiAyMCU7XHJcbn1cclxuLyoqKioqKioqKioqKioqKioqKioqXHJcbiAgICBBcHBseSBWYXJpYWJsZXNcclxuKioqKioqKioqKioqKioqKioqKiovXHJcblxyXG4jaW1hZ2Uge1xyXG4gICAgbWFyZ2luOiBhdXRvO1xyXG4gICAgdmVydGljYWwtYWxpZ246IGJvdHRvbTtcclxuICAgIGNsaXAtcGF0aDogdmFyKC0tY2xpcC1wYXRoKTtcclxuICAgIGJvcmRlci1jb2xvcjogdmFyKC0tYm9yZGVyLWNvbG9yKTtcclxuICAgIGJvcmRlci1yYWRpdXM6IHZhcigtLWJvcmRlci1yYWRpdXMpO1xyXG4gICAgLy8gZmlsdGVyOiBicmlnaHRuZXNzKC0tYnJpZ2h0bmVzcyk7XHJcbiAgICAvLyB0cmFuc2Zvcm06IHJvdGF0ZSh2YXIoLS1yb3RhdGUpKSBzY2FsZVgodmFyKC0tc2NhbGVYKSkgc2NhbGVZKHZhcigtLXNjYWxlWSkpIHNjYWxlKHZhcigtLXNjYWxlWFkpKSB0cmFuc2xhdGVYKHZhcigtLXRyYW5zbGF0ZVgpKSB0cmFuc2xhdGVZKHZhcigtLXRyYW5zbGF0ZVkpKSBza2V3WCh2YXIoLS1za2V3WCkpIHNrZXdZKHZhcigtLXNrZXdZKSk7XHJcbiAgICBmaWx0ZXI6IGJsdXIodmFyKC0tYmx1cikpIGJyaWdodG5lc3ModmFyKC0tYnJpZ2h0bmVzcykpIGNvbnRyYXN0KHZhcigtLWNvbnRyYXN0KSk7XHJcbn1cclxuXHJcbi53cy0xMDAge1xyXG4gICAgd2lkdGg6IDEwMCUgIWltcG9ydGFudDtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxufVxyXG5cclxuaW9uLXRhYi1idXR0b257XHJcbiAgICAmLmJhY2tncm91bmQtY29sb3J7XHJcbiAgICAgICAgYmFja2dyb3VuZDogdHJhbnNwYXJlbnQ7XHJcbiAgICB9XHJcbn0iXX0= */";
      /***/
    },

    /***/
    "./src/app/preview-filter/preview-filter.page.ts":
    /*!*******************************************************!*\
      !*** ./src/app/preview-filter/preview-filter.page.ts ***!
      \*******************************************************/

    /*! exports provided: PreviewFilterPage */

    /***/
    function srcAppPreviewFilterPreviewFilterPageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "PreviewFilterPage", function () {
        return PreviewFilterPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _global_services_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ../global-services.service */
      "./src/app/global-services.service.ts");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _ionic_native_android_permissions_ngx__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @ionic-native/android-permissions/ngx */
      "./node_modules/@ionic-native/android-permissions/__ivy_ngcc__/ngx/index.js");
      /* harmony import */


      var _ionic_native_File_ngx__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! @ionic-native/File/ngx */
      "./node_modules/@ionic-native/file/__ivy_ngcc__/ngx/index.js");
      /* harmony import */


      var _ionic_native_ionic_webview_ngx__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! @ionic-native/ionic-webview/ngx */
      "./node_modules/@ionic-native/ionic-webview/__ivy_ngcc__/ngx/index.js");
      /* harmony import */


      var _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
      /*! @ionic-native/geolocation/ngx */
      "./node_modules/@ionic-native/geolocation/__ivy_ngcc__/ngx/index.js");
      /* harmony import */


      var _ionic_native_native_geocoder_ngx__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
      /*! @ionic-native/native-geocoder/ngx */
      "./node_modules/@ionic-native/native-geocoder/__ivy_ngcc__/ngx/index.js");
      /* harmony import */


      var _image_crop_modal_image_crop_modal_page__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
      /*! ../image-crop-modal/image-crop-modal.page */
      "./src/app/image-crop-modal/image-crop-modal.page.ts");
      /* harmony import */


      var glfx_es6__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(
      /*! glfx-es6 */
      "./node_modules/glfx-es6/es/index.js");
      /* harmony import */


      var glfx_es6__WEBPACK_IMPORTED_MODULE_11___default = /*#__PURE__*/__webpack_require__.n(glfx_es6__WEBPACK_IMPORTED_MODULE_11__);
      /* harmony import */


      var _ionic_native_video_editor__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(
      /*! @ionic-native/video-editor */
      "./node_modules/@ionic-native/video-editor/index.js");

      var PreviewFilterPage = /*#__PURE__*/function () {
        function PreviewFilterPage(file, actionSheetController, alertController, service, menu, navCtrl, route, router, androidPermissions, webview, geolocation, nativeGeocoder, platform, modalController) {
          _classCallCheck2(this, PreviewFilterPage);

          this.file = file;
          this.actionSheetController = actionSheetController;
          this.alertController = alertController;
          this.service = service;
          this.menu = menu;
          this.navCtrl = navCtrl;
          this.route = route;
          this.router = router;
          this.androidPermissions = androidPermissions;
          this.webview = webview;
          this.geolocation = geolocation;
          this.nativeGeocoder = nativeGeocoder;
          this.platform = platform;
          this.modalController = modalController;
          this.fileInfo = {};
          this.enableEditMode = false;
          this.UploadMode = false;
          this.croppedImagepath = "";
          this.isLoading = false;
          this.brightness = 0;
          this.contrast = 0;
          this.hue = 0;
          this.saturation = 0;
          this.unsharpMask = {
            radius: 100,
            strength: 2
          };
          this.propertyTrue = false;
          this.posterImage = '';
          this.locationTraces = [];
          this.latitude = 0; //latitude

          this.longitude = 0; //longitude
          // geocoder options

          this.nativeGeocoderOptions = {
            useLocale: true,
            maxResults: 5
          };
        }

        _createClass2(PreviewFilterPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            var _this = this;

            // =====================Check app permissions===================== 
            this.checkPermissions(); // =====================Get params value===================== 

            this.route.params.subscribe(function (params) {
              // alert(params['tempPath']);
              _this.location = params['location'];
              _this.type = params['type'];
              _this.tempPath = params['tempPath'];
              _this.page = params['page'];
              _this.fileInfo = params['fileinfo'];

              if (params['tempPath'] != undefined) {
                _this.originalImg = params['tempPath'];
                _this.originalImage = params['tempPath'];
                localStorage.setItem('originalImg', _this.originalImg);
              } else if (localStorage.getItem('originalImg') != undefined) {
                _this.originalImg = localStorage.getItem('originalImg');
                _this.originalImage = localStorage.getItem('originalImg');
              }
            });
            this.getUserPosition();
          }
        }, {
          key: "ionViewWillEnter",
          value: function ionViewWillEnter() {
            var _this2 = this;

            this.brightness = 0;
            this.contrast = 0;
            this.hue = 0;
            this.saturation = 0;

            if (this.type == 'video') {
              // this.originalImage = this.convertFileuritoBase64(this.tempPath);
              this.originalImage = this.service.checkDomSanitizerForIOS(this.tempPath); // if (localStorage.getItem('videoThumbnailPosterImage')) {
              //   this.posterImage = this.service.checkDomSanitizerForIOS(this.webview.convertFileSrc(JSON.parse(localStorage.getItem('videoThumbnailPosterImage'))));
              // } else {

              var option = {
                fileUri: this.tempPath,
                width: this.fileInfo.width,
                height: this.fileInfo.height,
                atTime: 0,
                outputFileName: this.fileInfo.fileName,
                quality: 100
              };

              _ionic_native_video_editor__WEBPACK_IMPORTED_MODULE_12__["VideoEditor"].createThumbnail(option).then(function (res) {
                _this2.posterImageUrl = "file://" + res;

                _this2.convertFileuritoBase64("file://" + res); // let base64 = this.service.imagetobase64("file://" + res)
                // setTimeout(() => {
                //   base64.then((r) => {
                //     localStorage.setItem('videoThumbnailPosterImage', JSON.stringify(r));
                //   })
                // }, 1000);
                // localStorage.setItem('videoThumbnailPosterImage', res);


                _this2.posterImage = _this2.service.checkDomSanitizerForIOS(_this2.webview.convertFileSrc(res));
              }, function (error) {
                console.log(error);
              }); // }

            } else {
              this.service.currentMessage.subscribe(function (message) {
                if (message != '') {
                  _this2.originalImage = message;

                  _this2.convertFileuritoBase64(message);
                }
              });
              this.imageTag = this.imageSrc.nativeElement;
            } // else {
            //   this.originalImage = this.convertFileuritoBase64(this.imageTag.src); // convert to weburl
            // }


            this.service.sendMessage('startNotificationCountFunction');
          }
        }, {
          key: "ionViewWillLeave",
          value: function ionViewWillLeave() {
            if (this.posterImageUrl) {
              this.file.resolveLocalFilesystemUrl(this.posterImageUrl).then(function (fileEntry) {
                fileEntry.remove(function () {}, function (error) {
                  console.log(error);
                });
              })["catch"](function (e) {
                console.log(e);
              });
            }
          }
        }, {
          key: "ngAfterViewInit",
          value: function ngAfterViewInit() {
            if (this.type == 'video') {
              this.originalImage = this.service.checkDomSanitizerForIOS(this.webview.convertFileSrc(this.tempPath)); // this.convertFileuritoBase64(this.tempPath);
            }
          } // =====================Check app permissions===================== 

        }, {
          key: "checkPermissions",
          value: function checkPermissions() {
            this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE, this.androidPermissions.PERMISSION.WRITE_EXTERNAL_STORAGE]);
          } // =====================Edit the Image===================== 
          // When Sliders Values Change, Update the CSS Variables
          //  Select the filter property

        }, {
          key: "selectedFilterProperty",
          value: function selectedFilterProperty(value) {
            this.propertyTrue = true; // alert(value);

            this.property = value; // alert(this.property);
          } // =====================Image Crop===================== 
          // Crop the image

        }, {
          key: "cropImage",
          value: function cropImage(imgPath) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
              var _this3 = this;

              var modal, result;
              return regeneratorRuntime.wrap(function _callee$(_context) {
                while (1) {
                  switch (_context.prev = _context.next) {
                    case 0:
                      _context.next = 2;
                      return this.modalController.create({
                        component: _image_crop_modal_image_crop_modal_page__WEBPACK_IMPORTED_MODULE_10__["ImageCropModalPage"],
                        backdropDismiss: false,
                        componentProps: {
                          imageBase64: imgPath
                        }
                      });

                    case 2:
                      modal = _context.sent;
                      _context.next = 5;
                      return modal.present();

                    case 5:
                      _context.next = 7;
                      return modal.onWillDismiss();

                    case 7:
                      result = _context.sent;

                      if (result.data && result.data.croppedImageBase64) {
                        this.originalImage = result.data.croppedImageBase64;
                        this.newImage = new Image();

                        this.newImage.onload = function () {
                          _this3.imageFilter(_this3.contrast, _this3.saturation, _this3.brightness, _this3.hue, false);
                        };

                        this.newImage.src = result.data.croppedImageBase64;
                      }

                      return _context.abrupt("return", null);

                    case 10:
                    case "end":
                      return _context.stop();
                  }
                }
              }, _callee, this);
            }));
          } // Show and save the cropped image

        }, {
          key: "convertFileuritoBase64",
          value: function convertFileuritoBase64(ImagePath) {
            var _this4 = this;

            this.isLoading = true;

            if (ImagePath.indexOf("data:") !== -1) {
              if (this.type != 'video') {
                this.newImage = new Image();

                this.newImage.onload = function () {
                  _this4.imageFilter(_this4.contrast, _this4.saturation, _this4.brightness, _this4.hue, false);
                };

                this.newImage.src = ImagePath;
                this.originalImage = ImagePath;
              }
            } else {
              var copyPath = ImagePath;
              var splitPath = copyPath.split('/');
              var imageName = splitPath[splitPath.length - 1];
              var filePath = ImagePath.split(imageName)[0];
              this.tempPath = ImagePath; // alert(ImagePath); 

              this.file.readAsDataURL(filePath, imageName).then(function (base64) {
                if (_this4.type == 'video') {
                  localStorage.setItem('videoThumbnailPosterImage', JSON.stringify(base64));
                } else {
                  _this4.newImage = new Image();

                  _this4.newImage.onload = function () {
                    _this4.imageFilter(_this4.contrast, _this4.saturation, _this4.brightness, _this4.hue, false);
                  };

                  _this4.newImage.src = base64;
                  _this4.originalImage = base64; // this.createCanvasElement(base64);
                }
              }, function (error) {
                console.log("CROP ERROR -> " + JSON.stringify(error));
              });
            }
          }
        }, {
          key: "imageFilter",
          value: function imageFilter(contrast, saturate, brightness, hue, savefilter) {
            var contrastFilter = contrast / 100;
            var saturateFilter = saturate / 100;
            var brightnessFilter = brightness / 100;
            var hueFilter = hue / 100;
            this.contrast = contrast;
            this.saturation = saturate;
            this.brightness = brightness;
            this.hue = hue;

            try {
              this.canvas = glfx_es6__WEBPACK_IMPORTED_MODULE_11__["canvas"]();
            } catch (e) {
              alert(e);
              return;
            }

            this.imageElem = this.imageSrc.nativeElement;
            this.texture = this.canvas.texture(this.imageElem);
            this.canvas.draw(this.texture).update();
            this.canvas.setAttribute("style", "object-fit:contain;object-position:center;");
            var previousSibling = this.imageElem.previousSibling;

            while (previousSibling && previousSibling.nodeType !== 1) {
              previousSibling = previousSibling.previousSibling;
            }

            if (previousSibling) {
              previousSibling.parentNode.removeChild(previousSibling);
            }

            this.imageElem.parentNode.insertBefore(this.canvas, this.imageElem); /// filters applied to clean text

            this.canvas.draw(this.texture).hueSaturation(hueFilter, saturateFilter) //grayscale
            .brightnessContrast(brightnessFilter, contrastFilter).update(); /// replace image src 

            if (savefilter == true) {
              this.originalImage = this.canvas.toDataURL("image/jpg", 1.0);
              this.brightness = 0;
              this.contrast = 0;
              this.hue = 0;
              this.saturation = 0;
            }
          } // Save the changes and convert to the image

        }, {
          key: "saveChanges",
          value: function saveChanges() {
            this.propertyTrue = true;
            this.property = null;
            this.imageFilter(this.contrast, this.saturation, this.brightness, this.hue, true);
          }
        }, {
          key: "restoreImage",
          value: function restoreImage() {
            if (this.originalImage) {
              this.imageTag.nativeElement.src = this.originalImage;
            }
          } // =====================Enable Image Edit ===================== 

        }, {
          key: "enableEdit",
          value: function enableEdit() {
            this.enableEditMode = !this.enableEditMode;
          } // =====================Forward the video data to next page =====================  

        }, {
          key: "submitFileData",
          value: function submitFileData() {
            var media;

            if (this.originalVideo) {
              media = this.originalVideo;
            } else {
              media = this.originalImage;
            }

            var fileinfo = JSON.parse(this.fileInfo);

            if (this.type == 'video') {
              this.router.navigate(['/category-selection', {
                'fileinfo': JSON.stringify(fileinfo),
                'location': this.location,
                'latitude': this.latitude,
                'longitude': this.longitude,
                'mediaPath': media,
                'type': this.type,
                'page': this.page
              }]);
            } else {
              this.router.navigate(['/category-selection', {
                'fileinfo': JSON.stringify(fileinfo),
                'location': this.location,
                'latitude': this.latitude,
                'longitude': this.longitude,
                'mediaPath': media,
                'type': this.type,
                'page': this.page
              }]);
            }
          }
        }, {
          key: "presentAlert5",
          value: function presentAlert5(type, data) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
              var alert;
              return regeneratorRuntime.wrap(function _callee2$(_context2) {
                while (1) {
                  switch (_context2.prev = _context2.next) {
                    case 0:
                      _context2.next = 2;
                      return this.alertController.create({
                        cssClass: 'my-custom-class',
                        header: 'Error',
                        subHeader: type,
                        message: data,
                        buttons: ['OK']
                      });

                    case 2:
                      alert = _context2.sent;
                      _context2.next = 5;
                      return alert.present();

                    case 5:
                    case "end":
                      return _context2.stop();
                  }
                }
              }, _callee2, this);
            }));
          } //=====================Open Menu=====================

        }, {
          key: "openMenu",
          value: function openMenu() {
            this.menu.open();
          } //=====================Goto Back=====================

        }, {
          key: "goBack",
          value: function goBack() {
            if (this.type != 'video') {
              this.imageElem = this.imageSrc.nativeElement;
              var previousSibling = this.imageElem.previousSibling;

              while (previousSibling && previousSibling.nodeType !== 1) {
                previousSibling = previousSibling.previousSibling;
              }

              if (previousSibling) {
                previousSibling.parentNode.removeChild(previousSibling);
              }
            }

            this.originalImage = "";
            this.property = '';
            this.type = '';
            this.navCtrl.back();
          }
        }, {
          key: "getUserPosition",
          value: function getUserPosition() {
            var _this5 = this;

            this.options = {
              enableHighAccuracy: false
            };
            this.geolocation.getCurrentPosition(this.options).then(function (pos) {
              _this5.currentPos = pos;
              _this5.latitude = pos.coords.latitude;
              _this5.longitude = pos.coords.longitude;

              _this5.getAddress(_this5.latitude, _this5.longitude);
            }, function (err) {
              console.log("error : " + err.message);
              ;
            });
          } // get address using coordinates

        }, {
          key: "getAddress",
          value: function getAddress(lat, _long) {
            var _this6 = this;

            this.nativeGeocoder.reverseGeocode(lat, _long, this.nativeGeocoderOptions).then(function (res) {
              _this6.address = _this6.pretifyAddress(res[0]);
              _this6.location = _this6.pretifyAddress(res[0]);
            })["catch"](function (error) {
              alert('Error getting location' + error + JSON.stringify(error));
            });
          } // address

        }, {
          key: "pretifyAddress",
          value: function pretifyAddress(addressObj) {
            var obj = [];
            var address = "";

            for (var key in addressObj) {
              obj.push(addressObj[key]);
            }

            obj.reverse();

            for (var val in obj) {
              if (obj[val].length) address += obj[val] + ', ';
            }

            return address.slice(0, -2);
          }
        }]);

        return PreviewFilterPage;
      }();

      PreviewFilterPage.ctorParameters = function () {
        return [{
          type: _ionic_native_File_ngx__WEBPACK_IMPORTED_MODULE_6__["File"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ActionSheetController"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["AlertController"]
        }, {
          type: _global_services_service__WEBPACK_IMPORTED_MODULE_2__["GlobalServicesService"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["MenuController"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavController"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]
        }, {
          type: _ionic_native_android_permissions_ngx__WEBPACK_IMPORTED_MODULE_5__["AndroidPermissions"]
        }, {
          type: _ionic_native_ionic_webview_ngx__WEBPACK_IMPORTED_MODULE_7__["WebView"]
        }, {
          type: _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_8__["Geolocation"]
        }, {
          type: _ionic_native_native_geocoder_ngx__WEBPACK_IMPORTED_MODULE_9__["NativeGeocoder"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["Platform"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ModalController"]
        }];
      };

      PreviewFilterPage.propDecorators = {
        imageSrc: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"],
          args: ['imageSrc']
        }],
        canvasElement: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"],
          args: ['canvasElement']
        }]
      };
      PreviewFilterPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-preview-filter',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./preview-filter.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/preview-filter/preview-filter.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./preview-filter.page.scss */
        "./src/app/preview-filter/preview-filter.page.scss"))["default"]]
      })], PreviewFilterPage);
      /***/
    }
  }]);
})();
//# sourceMappingURL=preview-filter-preview-filter-module-es5.js.map