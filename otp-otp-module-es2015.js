(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["otp-otp-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/otp/otp.page.html":
/*!*************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/otp/otp.page.html ***!
  \*************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header translucent no-border>\r\n  <ion-toolbar>\r\n    <ion-buttons slot=\"start\">\r\n      <ion-back-button defaultHref=\"/\"></ion-back-button>\r\n    </ion-buttons>\r\n    <ion-title>Verify OTP</ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n<ion-content style=\"overflow-y: hidden;\">\r\n  <div class=\"inner-div\">\r\n    <ion-grid>\r\n      <ion-row>\r\n        <ion-col size=\"1\"></ion-col>\r\n        <ion-col align-self-center>\r\n          <ion-img src=\"../../assets/image/logo-removebg.png\" class=\"mx-auto mt-3 w-45\"></ion-img>\r\n          <div class=\"details ion-text-center\">\r\n            <ion-label *ngIf=\"!isClick\">OTP has been sent to your<br> Mobile Number and Email ID</ion-label>\r\n            <ion-label *ngIf=\"isClick\">Your OTP Timer has been <br> expired</ion-label>\r\n          </div>\r\n          <div class=\"detail\" align-self-center>\r\n            <countdown *ngIf=\"!isClick\" #countdown [config]=\"config\" (event)=\"handleEvent($event)\"></countdown>\r\n          </div>\r\n        </ion-col>\r\n        <ion-col size=\"1\"></ion-col>\r\n      </ion-row>\r\n      <ion-row class=\"mt-5\">\r\n        <ion-col>\r\n          <div class=\"row otp-div\">\r\n            <ion-input class=\"otp-input\" type=\"tel\" #otp1 required=\"true\" maxLength=\"1\" [disabled]=\"otpDisabled\"\r\n              [(ngModel)]=\"OTP.first\" (keyup)=\"otpController($event,otp2,'')\">\r\n            </ion-input>\r\n            <ion-input class=\"otp-input\" type=\"tel\" #otp2 required=\"true\" maxLength=\"1\" [disabled]=\"otpDisabled\"\r\n              [(ngModel)]=\"OTP.second\" (keyup)=\"otpController($event,otp3,otp1)\">\r\n            </ion-input>\r\n            <ion-input class=\"otp-input\" type=\"tel\" #otp3 required=\"true\" maxLength=\"1\" [disabled]=\"otpDisabled\"\r\n              [(ngModel)]=\"OTP.third\" (keyup)=\"otpController($event,otp4,otp2)\">\r\n            </ion-input>\r\n            <ion-input class=\"otp-input\" type=\"tel\" #otp4 required=\"true\" maxLength=\"1\" [disabled]=\"otpDisabled\"\r\n              [(ngModel)]=\"OTP.fourth\" (keyup)=\"otpController($event,otp5,otp3)\">\r\n            </ion-input>\r\n            <ion-input class=\"otp-input\" type=\"tel\" #otp5 required=\"true\" maxLength=\"1\" [disabled]=\"otpDisabled\"\r\n              [(ngModel)]=\"OTP.fifth\" (keyup)=\"otpController($event,null,otp4)\">\r\n            </ion-input>\r\n          </div>\r\n        </ion-col>\r\n      </ion-row>\r\n      <ion-row>\r\n        <ion-col size=\"4\" offset=\"4\">\r\n          <div class=\"size\" text-center>\r\n            <ion-button *ngIf=\"isClick\" color=\"btn-yellow\" size=\"medium\" (click)=\"rsendOTP(mobileno)\" btn-yellow\r\n              class=\"mt-10 btn-height1\">Resend\r\n            </ion-button>\r\n          </div>\r\n          <div class=\"size\" text-center>\r\n            <ion-button (click)=\"validateOTP(mobileno,enteredOTP)\" size=\"medium\"  *ngIf=\"!isClick\" color=\"btn-yellow\" \r\n              btn-yellow class=\"mt-10 btn-height1\">Verify\r\n            </ion-button>\r\n          </div>\r\n        </ion-col>\r\n      </ion-row>\r\n    </ion-grid>\r\n  </div>\r\n</ion-content>");

/***/ }),

/***/ "./src/app/otp/otp-routing.module.ts":
/*!*******************************************!*\
  !*** ./src/app/otp/otp-routing.module.ts ***!
  \*******************************************/
/*! exports provided: OtpPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OtpPageRoutingModule", function() { return OtpPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _otp_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./otp.page */ "./src/app/otp/otp.page.ts");




const routes = [
    {
        path: '',
        component: _otp_page__WEBPACK_IMPORTED_MODULE_3__["OtpPage"]
    }
];
let OtpPageRoutingModule = class OtpPageRoutingModule {
};
OtpPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], OtpPageRoutingModule);



/***/ }),

/***/ "./src/app/otp/otp.module.ts":
/*!***********************************!*\
  !*** ./src/app/otp/otp.module.ts ***!
  \***********************************/
/*! exports provided: OtpPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OtpPageModule", function() { return OtpPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _otp_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./otp-routing.module */ "./src/app/otp/otp-routing.module.ts");
/* harmony import */ var _otp_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./otp.page */ "./src/app/otp/otp.page.ts");
/* harmony import */ var ngx_countdown__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ngx-countdown */ "./node_modules/ngx-countdown/__ivy_ngcc__/fesm2015/ngx-countdown.js");








let OtpPageModule = class OtpPageModule {
};
OtpPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _otp_routing_module__WEBPACK_IMPORTED_MODULE_5__["OtpPageRoutingModule"], ngx_countdown__WEBPACK_IMPORTED_MODULE_7__["CountdownModule"]
        ],
        declarations: [_otp_page__WEBPACK_IMPORTED_MODULE_6__["OtpPage"]]
    })
], OtpPageModule);



/***/ }),

/***/ "./src/app/otp/otp.page.scss":
/*!***********************************!*\
  !*** ./src/app/otp/otp.page.scss ***!
  \***********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-title {\n  color: #000;\n  font-family: \"Poppins\", sans-serif;\n}\n\n.details {\n  margin-top: 1rem;\n  font-weight: 600;\n  color: var(--ion-color-yellow);\n  font-size: 14px;\n}\n\n.detail {\n  font-weight: 500;\n  color: #ffffff;\n  margin-top: 0.5rem;\n  font-size: 14px;\n  text-align: center;\n}\n\n.size {\n  text-align: center;\n  width: 100%;\n}\n\n.mt-10 {\n  margin-top: 2rem;\n  font-family: \"Poppins\", sans-serif;\n}\n\n.text {\n  font-weight: 500;\n  position: relative;\n  bottom: 10px;\n  left: 0.8rem;\n  font-size: 14px;\n  color: #ffffff;\n}\n\nbody {\n  display: flex;\n  align-items: center;\n  justify-content: center;\n  flex-direction: column;\n}\n\n.otp-input {\n  width: 45px;\n  height: 45px;\n  margin: 7px;\n  /* border-radius: 10px; */\n  text-align: center;\n  border: 1px solid #212721;\n  border-bottom-color: #fff;\n  color: #fff;\n}\n\n.otp-div {\n  padding: 0px 30px;\n}\n\n.btn-height1 {\n  border-radius: 5px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvb3RwL290cC5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxXQUFBO0VBQ0Esa0NBQUE7QUFDSjs7QUFFQTtFQUNDLGdCQUFBO0VBQ0csZ0JBQUE7RUFDSCw4QkFBQTtFQUNBLGVBQUE7QUFDRDs7QUFDQTtFQUNDLGdCQUFBO0VBQ0EsY0FBQTtFQUNBLGtCQUFBO0VBQ0EsZUFBQTtFQUNHLGtCQUFBO0FBRUo7O0FBQUE7RUFDQyxrQkFBQTtFQUNBLFdBQUE7QUFHRDs7QUFBQTtFQUNJLGdCQUFBO0VBQ0Esa0NBQUE7QUFHSjs7QUFEQTtFQUNJLGdCQUFBO0VBQ0Esa0JBQUE7RUFDSCxZQUFBO0VBQ0EsWUFBQTtFQUNBLGVBQUE7RUFDRyxjQUFBO0FBSUo7O0FBZ0NDO0VBQ0MsYUFBQTtFQUNBLG1CQUFBO0VBQ0EsdUJBQUE7RUFDQSxzQkFBQTtBQTdCRjs7QUFxRkM7RUFDQyxXQUFBO0VBQ0EsWUFBQTtFQUNBLFdBQUE7RUFDQSx5QkFBQTtFQUVBLGtCQUFBO0VBQ0EseUJBQUE7RUFDQSx5QkFBQTtFQUNBLFdBQUE7QUFuRkY7O0FBcUZDO0VBQ0MsaUJBQUE7QUFsRkY7O0FBcUZDO0VBQ0Msa0JBQUE7QUFsRkYiLCJmaWxlIjoic3JjL2FwcC9vdHAvb3RwLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImlvbi10aXRsZSB7XHJcbiAgICBjb2xvcjogIzAwMDtcclxuICAgIGZvbnQtZmFtaWx5OidQb3BwaW5zJywgc2Fucy1zZXJpZjtcclxufVxyXG5cclxuLmRldGFpbHMge1xyXG5cdG1hcmdpbi10b3A6IDFyZW07XHJcbiAgICBmb250LXdlaWdodDogNjAwO1xyXG5cdGNvbG9yOiB2YXIoLS1pb24tY29sb3IteWVsbG93KTsvLyNGNkM3NjI7XHJcblx0Zm9udC1zaXplOjE0cHg7XHJcbn1cclxuLmRldGFpbHtcclxuXHRmb250LXdlaWdodDogNTAwO1xyXG5cdGNvbG9yOiAjZmZmZmZmO1xyXG5cdG1hcmdpbi10b3A6MC41cmVtO1xyXG5cdGZvbnQtc2l6ZToxNHB4O1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG59XHJcbi5zaXple1xyXG5cdHRleHQtYWxpZ246IGNlbnRlcjtcclxuXHR3aWR0aDogMTAwJTtcclxuXHQvLyBwYWRkaW5nOiA1cHggNXB4IWltcG9ydGFudDtcclxufVxyXG4ubXQtMTB7XHJcbiAgICBtYXJnaW4tdG9wOiAycmVtO1xyXG4gICAgZm9udC1mYW1pbHk6J1BvcHBpbnMnLCBzYW5zLXNlcmlmO1xyXG59XHJcbi50ZXh0e1xyXG4gICAgZm9udC13ZWlnaHQ6IDUwMDtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuXHRib3R0b206IDEwcHg7XHJcblx0bGVmdDowLjhyZW07XHJcblx0Zm9udC1zaXplOiAxNHB4O1xyXG4gICAgY29sb3I6ICNmZmZmZmY7XHJcbn1cclxuXHQvLyAjZm9ybXtcclxuXHQvLyBcdGRpc3BsYXk6IGZsZXg7XHJcblx0Ly8gXHRqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XHJcblx0Ly8gXHRhbGlnbi1pdGVtczogY2VudGVyO1xyXG5cdC8vIFx0d2lkdGg6IGF1dG87XHJcblx0Ly8gXHRtYXJnaW46IDJweCAxNXB4O1xyXG5cdC8vIFx0cG9zaXRpb246IHJlbGF0aXZlO1xyXG5cdC8vIFx0cmlnaHQ6IDAuMnJlbTtcclxuXHQvLyBcdGlucHV0e1xyXG5cdC8vIFx0XHR3aWR0aDogNTBweDtcclxuXHQvLyBcdFx0aGVpZ2h0OiA1MHB4O1xyXG5cdC8vIFx0XHRwYWRkaW5nOiAwO1xyXG5cdC8vIFx0XHR0ZXh0LWFsaWduOiBjZW50ZXI7XHJcblx0Ly8gXHRcdGNvbG9yOiNmZmY7XHJcbiAgICAvLyAgICAgICAgIGJvcmRlcjogMXB4IHNvbGlkIHJnYigyNTUsIDI1NSwgMjU1KTtcclxuICAgIC8vICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XHJcblxyXG5cdC8vIFx0XHQmOmxhc3QtY2hpbGR7XHJcblx0Ly8gXHRcdFx0bWFyZ2luLXJpZ2h0OiAwO1xyXG5cdC8vIFx0XHR9XHJcblx0Ly8gXHRcdCY6Oi13ZWJraXQtaW5uZXItc3Bpbi1idXR0b24sIFxyXG5cdC8vIFx0XHQmOjotd2Via2l0LW91dGVyLXNwaW4tYnV0dG9uIHsgXHJcblx0Ly8gXHRcdFx0LXdlYmtpdC1hcHBlYXJhbmNlOiBub25lO1xyXG5cdC8vIFx0XHRcdGFwcGVhcmFuY2U6IG5vbmU7XHJcblx0Ly8gXHRcdFx0bWFyZ2luOiAwO1xyXG5cdC8vIFx0XHR9XHJcblx0Ly8gXHRcdCY6Zm9jdXMsXHJcblx0Ly8gXHRcdCYuZm9jdXN7XHJcblx0Ly8gXHRcdFx0Ym9yZGVyLWNvbG9yOiNGNkM3NjI7XHJcblx0Ly8gXHRcdFx0b3V0bGluZTogbm9uZTtcclxuXHQvLyBcdFx0XHRib3gtc2hhZG93OiBub25lO1xyXG5cdC8vIFx0XHR9XHJcblx0Ly8gXHR9XHJcblx0Ly8gfVxyXG5cdGJvZHkge1xyXG5cdFx0ZGlzcGxheTogZmxleDtcclxuXHRcdGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcblx0XHRqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuXHRcdGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcblx0ICB9XHJcblx0ICBcclxuXHQvLyAgIC5kaWdpdC1ncm91cCBpbnB1dCB7XHJcblx0Ly8gXHR3aWR0aDogNDVweDtcclxuXHQvLyBcdGhlaWdodDogNDVweDtcclxuXHQvLyBcdG92ZXJmbG93OiBoaWRkZW47XHJcblx0Ly8gXHRvdmVyZmxvdy15OiBoaWRkZW47IFxyXG5cdC8vIFx0YmFja2dyb3VuZC1jb2xvcjogIzE4MTgyYTAwO1xyXG5cdC8vIFx0Ym9yZGVyOiAxcHggc29saWQgI2ZmZjtcclxuXHQvLyBcdGJvcmRlci1yYWRpdXM6IDA7XHJcblx0Ly8gXHRsaW5lLWhlaWdodDogNTBweDtcclxuXHQvLyBcdHRleHQtYWxpZ246IGNlbnRlcjtcclxuXHQvLyBcdGZvbnQtc2l6ZTogMTRweDtcclxuXHQvLyBcdGZvbnQtd2VpZ2h0OiAyMDA7XHJcblx0Ly8gXHRjb2xvcjogd2hpdGU7XHJcblx0Ly8gXHRtYXJnaW46IDBweCA1cHg7XHJcblx0Ly8gXHQmOmZvY3VzLFxyXG5cdC8vIFx0XHQmLmZvY3Vze1xyXG5cdC8vIFx0XHRcdGJvcmRlci1jb2xvcjojRjZDNzYyO1xyXG5cdC8vIFx0XHRcdG91dGxpbmU6IG5vbmU7XHJcblx0Ly8gXHRcdFx0Ym94LXNoYWRvdzogbm9uZTtcclxuXHQvLyBcdFx0XHRvdmVyZmxvdzogaGlkZGVuO1xyXG5cdC8vIFx0XHRcdG92ZXJmbG93LXk6IGhpZGRlbjsgXHJcblx0Ly8gXHRcdH1cclxuXHQvLyAgIH1cclxuXHQvLyAgIC5kaWdpdC1ncm91cCAuc3BsaXR0ZXIge1xyXG5cdC8vIFx0cGFkZGluZzogMCA1cHg7XHJcblx0Ly8gXHRjb2xvcjogd2hpdGU7XHJcblx0Ly8gXHRmb250LXNpemU6IDE0cHg7XHJcblx0Ly8gICB9XHJcblxyXG5cdC8vICAgaW9uLWlucHV0e1xyXG5cdC8vIFx0ZGlzcGxheTppbmxpbmUtYmxvY2s7XHJcblx0Ly8gXHRtYXJnaW46OHB4O1xyXG5cdC8vIFx0LS1wYWRkaW5nLXN0YXJ0OjVweDtcclxuXHQvLyBcdHdpZHRoOiA0MHB4O1xyXG5cdC8vIFx0aGVpZ2h0OiA0MHB4O1xyXG5cdC8vIFx0b3ZlcmZsb3c6IGhpZGRlbjtcclxuXHQvLyBcdG92ZXJmbG93LXk6IGhpZGRlbjsgXHJcblx0Ly8gXHRiYWNrZ3JvdW5kLWNvbG9yOiAjMTgxODJhMDA7XHJcblx0Ly8gXHRib3JkZXI6IDFweCBzb2xpZCAjZmZmO1xyXG5cdC8vIFx0Ym9yZGVyLXJhZGl1czogMDtcclxuXHQvLyBcdC8vIGxpbmUtaGVpZ2h0OiA1MHB4O1xyXG5cdC8vIFx0dGV4dC1hbGlnbjogY2VudGVyO1xyXG5cdC8vIFx0Zm9udC1zaXplOiAxNHB4O1xyXG5cdC8vIFx0Zm9udC13ZWlnaHQ6IDIwMDtcclxuXHQvLyBcdGNvbG9yOiB3aGl0ZTtcdFxyXG5cdC8vIFx0XHQmLmZvY3Vze3hcclxuXHQvLyBcdFx0XHRib3JkZXItY29sb3I6I0Y2Qzc2MjtcclxuXHQvLyBcdFx0XHRvdXRsaW5lOiBub25lO1xyXG5cdC8vIFx0XHRcdGJveC1zaGFkb3c6IG5vbmU7XHJcblx0Ly8gXHRcdFx0b3ZlcmZsb3c6IGhpZGRlbjtcclxuXHQvLyBcdFx0XHRvdmVyZmxvdy15OiBoaWRkZW47IFxyXG5cdC8vIFx0XHR9XHJcblx0Ly8gXHR9XHJcblx0Lm90cC1pbnB1dHtcclxuXHRcdHdpZHRoOiA0NXB4O1xyXG5cdFx0aGVpZ2h0OiA0NXB4O1xyXG5cdFx0bWFyZ2luOiA3cHg7XHJcblx0XHQvKiBib3JkZXItcmFkaXVzOiAxMHB4OyAqL1xyXG5cdFx0Ly8gZm9udC1zaXplOiB4eC1sYXJnZTtcclxuXHRcdHRleHQtYWxpZ246IGNlbnRlcjtcclxuXHRcdGJvcmRlcjogMXB4IHNvbGlkICMyMTI3MjE7XHJcblx0XHRib3JkZXItYm90dG9tLWNvbG9yOiAjZmZmO1xyXG5cdFx0Y29sb3I6ICNmZmY7XHJcblx0fVxyXG5cdC5vdHAtZGl2e1xyXG5cdFx0cGFkZGluZzogMHB4IDMwcHg7XHJcblx0fVxyXG5cclxuXHQuYnRuLWhlaWdodDEge1xyXG5cdFx0Ym9yZGVyLXJhZGl1czogNXB4O1xyXG5cdH0iXX0= */");

/***/ }),

/***/ "./src/app/otp/otp.page.ts":
/*!*********************************!*\
  !*** ./src/app/otp/otp.page.ts ***!
  \*********************************/
/*! exports provided: OtpPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OtpPage", function() { return OtpPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _global_services_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../global-services.service */ "./src/app/global-services.service.ts");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");







let OtpPage = class OtpPage {
    constructor(navCtrl, service, route, router, toastController, platform, location) {
        this.navCtrl = navCtrl;
        this.service = service;
        this.route = route;
        this.router = router;
        this.toastController = toastController;
        this.platform = platform;
        this.location = location;
        this.isClick = false;
        this.timeLeft = 45;
        this.OTP = {
            first: '',
            second: '',
            third: '',
            fourth: '',
            fifth: ''
        };
        this.config = {
            format: 'mm:ss',
            leftTime: 45
        };
        this.otpDisabled = false;
        // =====================read SMS plugin for OTP Verification=====================
        // OTP: string = '';
        this.showOTPInput = false;
        this.OTPmessage = 'An OTP is sent to your number. You should receive it in 15 s';
        // =====================Mobile Number  Validation=====================
        this.validateOTP = (mobileno) => {
            let obj = {};
            obj.mobileno = mobileno;
            obj.otp = Object.values(this.OTP).join('');
            if (Object.values(this.OTP).length == 5) {
                this.service.post_data_otp("ValidateOtp", obj)
                    .subscribe((result) => {
                    let json = this.service.parserToJSON(result);
                    var parsedJSON = JSON.parse(json.string);
                    if (parsedJSON[0]['Status'] == "SUCCESS") {
                        if (this.page == 'signup') {
                            this.navCtrl.navigateRoot(['/terms', { 'data': this.data }]);
                        }
                        if (this.page == 'login') {
                            this.navCtrl.navigateRoot(['/home']);
                            localStorage.setItem('login_token', '5$/7@nift)8G63');
                        }
                    }
                    else {
                        this.presentToastOTP(parsedJSON[0]['Message'], 'top', 1500);
                    }
                }, (error) => {
                    console.log(error);
                });
            }
            else {
                this.presentToastOTP('OTP is invalid', 'top', 1500);
            }
        };
    }
    ngOnInit() {
        this.route.params.subscribe((params) => {
            this.otpReceived = params['otp'];
            this.mobileno = params['mobileno'];
            this.page = params['page'];
            this.data = params['data'];
            // this.enteredOTP = params['otp'];
        });
    }
    ionViewDidEnter() {
        this.subscription = this.platform.backButton.subscribeWithPriority(9999, () => {
            // do nothing
        });
        this.smsReadStart();
    }
    ionViewWillLeave() {
        this.subscription.unsubscribe();
    }
    presentToastOTP(message, position, duration) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const toast = yield this.toastController.create({
                message: message,
                position: position,
                duration: duration
            });
            toast.present();
        });
    }
    // =====================OTP Controller to move next=====================
    otpController(event, next, prev, index) {
        if (index == 6) {
        }
        if (event.target.value.length < 1 && prev) {
            prev.setFocus();
        }
        else if (next && event.target.value.length > 0) {
            next.setFocus();
        }
        else {
            return 0;
        }
    }
    // =====================End of Mobile Number Unique Validation=====================
    // =====================Resend OTP=====================
    rsendOTP(m) {
        let obj = {};
        obj.mobileno = this.mobileno; //mobileno;
        this.service.post_data_otp("ReSendOtpViaSMS", obj)
            .subscribe((result) => {
            let json = this.service.parserToJSON(result);
            var parsedJSON = JSON.parse(json.string)[0];
            if (parsedJSON.Status == 'SUCCESS') {
                this.presentToast(parsedJSON.Message);
                this.isClick = false;
                this.otpDisabled = false;
                this.smsReadStart();
                this.timeLeft = 45;
            }
        }, (error) => {
            console.log(error);
        });
    }
    presentToast(message) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const toast = yield this.toastController.create({
                message: message,
                duration: 2000,
                position: 'top'
            });
            toast.present();
        });
    }
    // =====================OTP Expired (Timer)=====================
    handleEvent(e) {
        if (e.action == "done") {
            this.enteredOTP = '';
            this.isClick = true;
            this.OTP = {};
            this.otpDisabled = true;
            this.color = '#1e2023';
            this.config.leftTime = 45;
            this.countdown.begin();
        }
    }
    smsReadStart() {
        SMSReceive.startWatch(() => {
            document.addEventListener('onSMSArrive', (args) => {
                if (args) {
                    if (args.data.body && args.data.body.indexOf('VisioNxt') == 5) {
                        let messageArray = args.data.body.split(' ');
                        // this.enteredOTP = messageArray[5];
                        this.patchValue(messageArray[5]);
                        this.smsReadStop();
                    }
                }
            });
        }, (error) => {
            console.warn(error);
        });
    }
    smsReadStop() {
        SMSReceive.stopWatch(() => {
        }, (error) => {
            console.warn(error);
        });
    }
    patchValue(val) {
        let splitVal = val.split('');
        this.OTP.first = splitVal[0];
        this.OTP.second = splitVal[1];
        this.OTP.third = splitVal[2];
        this.OTP.fourth = splitVal[3];
        this.OTP.fifth = splitVal[4];
    }
    goBack() {
        this.location.back();
    }
};
OtpPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"] },
    { type: _global_services_service__WEBPACK_IMPORTED_MODULE_4__["GlobalServicesService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["Platform"] },
    { type: _angular_common__WEBPACK_IMPORTED_MODULE_5__["Location"] }
];
OtpPage.propDecorators = {
    countdown: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"], args: ['countdown', { static: false },] }]
};
OtpPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-otp',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./otp.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/otp/otp.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./otp.page.scss */ "./src/app/otp/otp.page.scss")).default]
    })
], OtpPage);



/***/ })

}]);
//# sourceMappingURL=otp-otp-module-es2015.js.map