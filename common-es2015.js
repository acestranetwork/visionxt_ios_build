(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["common"],{

/***/ "./node_modules/@ionic/core/dist/esm/button-active-a6787d69.js":
/*!*********************************************************************!*\
  !*** ./node_modules/@ionic/core/dist/esm/button-active-a6787d69.js ***!
  \*********************************************************************/
/*! exports provided: c */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return createButtonActiveGesture; });
/* harmony import */ var _index_e806d1f6_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./index-e806d1f6.js */ "./node_modules/@ionic/core/dist/esm/index-e806d1f6.js");
/* harmony import */ var _index_f49d994d_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./index-f49d994d.js */ "./node_modules/@ionic/core/dist/esm/index-f49d994d.js");
/* harmony import */ var _haptic_27b3f981_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./haptic-27b3f981.js */ "./node_modules/@ionic/core/dist/esm/haptic-27b3f981.js");




const createButtonActiveGesture = (el, isButton) => {
  let currentTouchedButton;
  let initialTouchedButton;
  const activateButtonAtPoint = (x, y, hapticFeedbackFn) => {
    if (typeof document === 'undefined') {
      return;
    }
    const target = document.elementFromPoint(x, y);
    if (!target || !isButton(target)) {
      clearActiveButton();
      return;
    }
    if (target !== currentTouchedButton) {
      clearActiveButton();
      setActiveButton(target, hapticFeedbackFn);
    }
  };
  const setActiveButton = (button, hapticFeedbackFn) => {
    currentTouchedButton = button;
    if (!initialTouchedButton) {
      initialTouchedButton = currentTouchedButton;
    }
    const buttonToModify = currentTouchedButton;
    Object(_index_e806d1f6_js__WEBPACK_IMPORTED_MODULE_0__["c"])(() => buttonToModify.classList.add('ion-activated'));
    hapticFeedbackFn();
  };
  const clearActiveButton = (dispatchClick = false) => {
    if (!currentTouchedButton) {
      return;
    }
    const buttonToModify = currentTouchedButton;
    Object(_index_e806d1f6_js__WEBPACK_IMPORTED_MODULE_0__["c"])(() => buttonToModify.classList.remove('ion-activated'));
    /**
     * Clicking on one button, but releasing on another button
     * does not dispatch a click event in browsers, so we
     * need to do it manually here. Some browsers will
     * dispatch a click if clicking on one button, dragging over
     * another button, and releasing on the original button. In that
     * case, we need to make sure we do not cause a double click there.
     */
    if (dispatchClick && initialTouchedButton !== currentTouchedButton) {
      currentTouchedButton.click();
    }
    currentTouchedButton = undefined;
  };
  return Object(_index_f49d994d_js__WEBPACK_IMPORTED_MODULE_1__["createGesture"])({
    el,
    gestureName: 'buttonActiveDrag',
    threshold: 0,
    onStart: ev => activateButtonAtPoint(ev.currentX, ev.currentY, _haptic_27b3f981_js__WEBPACK_IMPORTED_MODULE_2__["a"]),
    onMove: ev => activateButtonAtPoint(ev.currentX, ev.currentY, _haptic_27b3f981_js__WEBPACK_IMPORTED_MODULE_2__["b"]),
    onEnd: () => {
      clearActiveButton(true);
      Object(_haptic_27b3f981_js__WEBPACK_IMPORTED_MODULE_2__["h"])();
      initialTouchedButton = undefined;
    }
  });
};




/***/ }),

/***/ "./node_modules/@ionic/core/dist/esm/framework-delegate-4584ab5a.js":
/*!**************************************************************************!*\
  !*** ./node_modules/@ionic/core/dist/esm/framework-delegate-4584ab5a.js ***!
  \**************************************************************************/
/*! exports provided: a, d */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return attachComponent; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "d", function() { return detachComponent; });
const attachComponent = async (delegate, container, component, cssClasses, componentProps) => {
  if (delegate) {
    return delegate.attachViewToDom(container, component, componentProps, cssClasses);
  }
  if (typeof component !== 'string' && !(component instanceof HTMLElement)) {
    throw new Error('framework delegate is missing');
  }
  const el = (typeof component === 'string')
    ? container.ownerDocument && container.ownerDocument.createElement(component)
    : component;
  if (cssClasses) {
    cssClasses.forEach(c => el.classList.add(c));
  }
  if (componentProps) {
    Object.assign(el, componentProps);
  }
  container.appendChild(el);
  if (el.componentOnReady) {
    await el.componentOnReady();
  }
  return el;
};
const detachComponent = (delegate, element) => {
  if (element) {
    if (delegate) {
      const container = element.parentElement;
      return delegate.removeViewFromDom(container, element);
    }
    element.remove();
  }
  return Promise.resolve();
};




/***/ }),

/***/ "./node_modules/@ionic/core/dist/esm/haptic-27b3f981.js":
/*!**************************************************************!*\
  !*** ./node_modules/@ionic/core/dist/esm/haptic-27b3f981.js ***!
  \**************************************************************/
/*! exports provided: a, b, c, d, h */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return hapticSelectionStart; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return hapticSelectionChanged; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return hapticSelection; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "d", function() { return hapticImpact; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "h", function() { return hapticSelectionEnd; });
const HapticEngine = {
  getEngine() {
    const win = window;
    return (win.TapticEngine) || (win.Capacitor && win.Capacitor.isPluginAvailable('Haptics') && win.Capacitor.Plugins.Haptics);
  },
  available() {
    return !!this.getEngine();
  },
  isCordova() {
    return !!window.TapticEngine;
  },
  isCapacitor() {
    const win = window;
    return !!win.Capacitor;
  },
  impact(options) {
    const engine = this.getEngine();
    if (!engine) {
      return;
    }
    const style = this.isCapacitor() ? options.style.toUpperCase() : options.style;
    engine.impact({ style });
  },
  notification(options) {
    const engine = this.getEngine();
    if (!engine) {
      return;
    }
    const style = this.isCapacitor() ? options.style.toUpperCase() : options.style;
    engine.notification({ style });
  },
  selection() {
    this.impact({ style: 'light' });
  },
  selectionStart() {
    const engine = this.getEngine();
    if (!engine) {
      return;
    }
    if (this.isCapacitor()) {
      engine.selectionStart();
    }
    else {
      engine.gestureSelectionStart();
    }
  },
  selectionChanged() {
    const engine = this.getEngine();
    if (!engine) {
      return;
    }
    if (this.isCapacitor()) {
      engine.selectionChanged();
    }
    else {
      engine.gestureSelectionChanged();
    }
  },
  selectionEnd() {
    const engine = this.getEngine();
    if (!engine) {
      return;
    }
    if (this.isCapacitor()) {
      engine.selectionEnd();
    }
    else {
      engine.gestureSelectionEnd();
    }
  }
};
/**
 * Trigger a selection changed haptic event. Good for one-time events
 * (not for gestures)
 */
const hapticSelection = () => {
  HapticEngine.selection();
};
/**
 * Tell the haptic engine that a gesture for a selection change is starting.
 */
const hapticSelectionStart = () => {
  HapticEngine.selectionStart();
};
/**
 * Tell the haptic engine that a selection changed during a gesture.
 */
const hapticSelectionChanged = () => {
  HapticEngine.selectionChanged();
};
/**
 * Tell the haptic engine we are done with a gesture. This needs to be
 * called lest resources are not properly recycled.
 */
const hapticSelectionEnd = () => {
  HapticEngine.selectionEnd();
};
/**
 * Use this to indicate success/failure/warning to the user.
 * options should be of the type `{ style: 'light' }` (or `medium`/`heavy`)
 */
const hapticImpact = (options) => {
  HapticEngine.impact(options);
};




/***/ }),

/***/ "./node_modules/@ionic/core/dist/esm/spinner-configs-cd7845af.js":
/*!***********************************************************************!*\
  !*** ./node_modules/@ionic/core/dist/esm/spinner-configs-cd7845af.js ***!
  \***********************************************************************/
/*! exports provided: S */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "S", function() { return SPINNERS; });
const spinners = {
  'bubbles': {
    dur: 1000,
    circles: 9,
    fn: (dur, index, total) => {
      const animationDelay = `${(dur * index / total) - dur}ms`;
      const angle = 2 * Math.PI * index / total;
      return {
        r: 5,
        style: {
          'top': `${9 * Math.sin(angle)}px`,
          'left': `${9 * Math.cos(angle)}px`,
          'animation-delay': animationDelay,
        }
      };
    }
  },
  'circles': {
    dur: 1000,
    circles: 8,
    fn: (dur, index, total) => {
      const step = index / total;
      const animationDelay = `${(dur * step) - dur}ms`;
      const angle = 2 * Math.PI * step;
      return {
        r: 5,
        style: {
          'top': `${9 * Math.sin(angle)}px`,
          'left': `${9 * Math.cos(angle)}px`,
          'animation-delay': animationDelay,
        }
      };
    }
  },
  'circular': {
    dur: 1400,
    elmDuration: true,
    circles: 1,
    fn: () => {
      return {
        r: 20,
        cx: 48,
        cy: 48,
        fill: 'none',
        viewBox: '24 24 48 48',
        transform: 'translate(0,0)',
        style: {}
      };
    }
  },
  'crescent': {
    dur: 750,
    circles: 1,
    fn: () => {
      return {
        r: 26,
        style: {}
      };
    }
  },
  'dots': {
    dur: 750,
    circles: 3,
    fn: (_, index) => {
      const animationDelay = -(110 * index) + 'ms';
      return {
        r: 6,
        style: {
          'left': `${9 - (9 * index)}px`,
          'animation-delay': animationDelay,
        }
      };
    }
  },
  'lines': {
    dur: 1000,
    lines: 12,
    fn: (dur, index, total) => {
      const transform = `rotate(${30 * index + (index < 6 ? 180 : -180)}deg)`;
      const animationDelay = `${(dur * index / total) - dur}ms`;
      return {
        y1: 17,
        y2: 29,
        style: {
          'transform': transform,
          'animation-delay': animationDelay,
        }
      };
    }
  },
  'lines-small': {
    dur: 1000,
    lines: 12,
    fn: (dur, index, total) => {
      const transform = `rotate(${30 * index + (index < 6 ? 180 : -180)}deg)`;
      const animationDelay = `${(dur * index / total) - dur}ms`;
      return {
        y1: 12,
        y2: 20,
        style: {
          'transform': transform,
          'animation-delay': animationDelay,
        }
      };
    }
  }
};
const SPINNERS = spinners;




/***/ }),

/***/ "./node_modules/@ionic/core/dist/esm/theme-ff3fc52f.js":
/*!*************************************************************!*\
  !*** ./node_modules/@ionic/core/dist/esm/theme-ff3fc52f.js ***!
  \*************************************************************/
/*! exports provided: c, g, h, o */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return createColorClasses; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "g", function() { return getClassMap; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "h", function() { return hostContext; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "o", function() { return openURL; });
const hostContext = (selector, el) => {
  return el.closest(selector) !== null;
};
/**
 * Create the mode and color classes for the component based on the classes passed in
 */
const createColorClasses = (color, cssClassMap) => {
  return (typeof color === 'string' && color.length > 0) ? Object.assign({ 'ion-color': true, [`ion-color-${color}`]: true }, cssClassMap) : cssClassMap;
};
const getClassList = (classes) => {
  if (classes !== undefined) {
    const array = Array.isArray(classes) ? classes : classes.split(' ');
    return array
      .filter(c => c != null)
      .map(c => c.trim())
      .filter(c => c !== '');
  }
  return [];
};
const getClassMap = (classes) => {
  const map = {};
  getClassList(classes).forEach(c => map[c] = true);
  return map;
};
const SCHEME = /^[a-z][a-z0-9+\-.]*:/;
const openURL = async (url, ev, direction, animation) => {
  if (url != null && url[0] !== '#' && !SCHEME.test(url)) {
    const router = document.querySelector('ion-router');
    if (router) {
      if (ev != null) {
        ev.preventDefault();
      }
      return router.push(url, direction, animation);
    }
  }
  return false;
};




/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/advertisement-modalpop/advertisement-modalpop.page.html":
/*!***************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/advertisement-modalpop/advertisement-modalpop.page.html ***!
  \***************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"notification-popup-container\">\r\n      <ion-icon name=\"close-circle-outline\" *ngIf=\"imageContent\" class=\"close-icon\" (click)=\"dismiss()\"></ion-icon>\r\n      <ion-img class=\"imgContent\" *ngIf=\"imageContent\" width=\"100%\" height=\"100%\" [src]=\"imageContent\"></ion-img>\r\n</div>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/file-information/file-information.page.html":
/*!***************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/file-information/file-information.page.html ***!
  \***************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\r\n    <ion-toolbar>\r\n        <ion-row>\r\n            <ion-col size=\"8\" class=\"title\">\r\n                <ion-label>File Information</ion-label>\r\n            </ion-col>\r\n            <ion-col size=\"4\" class=\"close\">\r\n                <ion-label (click)=\"dismiss()\">Close</ion-label>\r\n            </ion-col>\r\n        </ion-row>\r\n    </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n    <ion-grid *ngIf=\"data.image_video && data.image_video=='IMAGE'\">\r\n        <ion-row *ngIf=\"fileinfo.fileName && fileinfo.fileName!=undefined\">\r\n            <ion-col size=\"5\">\r\n                <small><b>File Name :</b></small>\r\n            </ion-col>\r\n            <ion-col size=\"7\">\r\n                <small>{{fileinfo.fileName}}</small>\r\n            </ion-col>\r\n        </ion-row>\r\n        <ion-row *ngIf=\"fileinfo.time && fileinfo.time!=undefined\">\r\n            <ion-col size=\"5\">\r\n                <small><b>Time :</b></small>\r\n            </ion-col>\r\n            <ion-col size=\"7\">\r\n                <small>{{fileinfo.time}}</small>\r\n            </ion-col>\r\n        </ion-row>\r\n        <ion-row *ngIf=\"fileinfo.resolution && fileinfo.resolution!=undefined\">\r\n            <ion-col size=\"5\">\r\n                <small><b>Resolution :</b></small>\r\n            </ion-col>\r\n            <ion-col size=\"7\">\r\n                <small>{{fileinfo.resolution}}</small>\r\n            </ion-col>\r\n        </ion-row>\r\n        <ion-row *ngIf=\"fileinfo.size && fileinfo.size!=undefined\">\r\n            <ion-col size=\"5\">\r\n                <small><b>Size :</b></small>\r\n            </ion-col>\r\n            <ion-col size=\"7\">\r\n                <small>{{fileinfo.size}}</small>\r\n            </ion-col>\r\n        </ion-row>\r\n        <ion-row *ngIf=\"fileinfo.device && fileinfo.device!=undefined\">\r\n            <ion-col size=\"5\">\r\n                <small><b>Device :</b></small>\r\n            </ion-col>\r\n            <ion-col size=\"7\">\r\n                <small>{{fileinfo.device}}</small>\r\n            </ion-col>\r\n        </ion-row>\r\n        <ion-row *ngIf=\"fileinfo.model && fileinfo.model!=undefined\">\r\n            <ion-col size=\"5\">\r\n                <small><b>Model :</b></small>\r\n            </ion-col>\r\n            <ion-col size=\"7\">\r\n                <small>{{fileinfo.model}}</small>\r\n            </ion-col>\r\n        </ion-row>\r\n        <ion-row *ngIf=\"fileinfo.flash && fileinfo.flash!=undefined\">\r\n            <ion-col size=\"5\">\r\n                <small><b>Flash :</b></small>\r\n            </ion-col>\r\n            <ion-col size=\"7\">\r\n                <small>{{fileinfo.flash}}</small>\r\n            </ion-col>\r\n        </ion-row>\r\n        <ion-row *ngIf=\"fileinfo.whiteBalance && fileinfo.whiteBalance!=undefined\">\r\n            <ion-col size=\"5\">\r\n                <small><b>White Blance :</b></small>\r\n            </ion-col>\r\n            <ion-col size=\"7\">\r\n                <small>{{fileinfo.whiteBalance}}</small>\r\n            </ion-col>\r\n        </ion-row>\r\n        <ion-row *ngIf=\"fileinfo.iso && fileinfo.iso!=undefined\">\r\n            <ion-col size=\"5\">\r\n                <small><b>ISO :</b></small>\r\n            </ion-col>\r\n            <ion-col size=\"7\">\r\n                <small>{{fileinfo.iso}}</small>\r\n            </ion-col>\r\n        </ion-row>\r\n        <ion-row *ngIf=\"fileinfo.orientation && fileinfo.orientation!=undefined\">\r\n            <ion-col size=\"5\">\r\n                <small><b>Orientation :</b></small>\r\n            </ion-col>\r\n            <ion-col size=\"7\">\r\n                <small>{{fileinfo.orientation}}</small>\r\n            </ion-col>\r\n        </ion-row>\r\n        <ion-row *ngIf=\"data.location && data.location!=undefined && data.location!='undefined,undefined'\">\r\n            <ion-col size=\"5\">\r\n                <small><b>Location :</b></small>\r\n            </ion-col>\r\n            <ion-col size=\"7\">\r\n                <small>{{data.location?data.location:'No location found'}}</small>\r\n            </ion-col>\r\n        </ion-row>\r\n        <ion-row *ngIf=\"latitude\">\r\n            <ion-col size=\"5\">\r\n                <small><b>Latitude :</b></small>\r\n            </ion-col>\r\n            <ion-col size=\"7\">\r\n                <small>{{latitude}}</small>\r\n            </ion-col>\r\n        </ion-row>\r\n        <ion-row *ngIf=\"longitude\">\r\n            <ion-col size=\"5\">\r\n                <small><b>Longitude :</b></small>\r\n            </ion-col>\r\n            <ion-col size=\"7\">\r\n\r\n                <small>{{longitude}}</small>\r\n            </ion-col>\r\n        </ion-row>\r\n    </ion-grid>\r\n    <ion-grid *ngIf=\"data.image_video && data.image_video=='VIDEO'\">\r\n        <ion-row *ngIf=\"fileinfo.fileName && fileinfo.fileName!=undefined\">\r\n            <ion-col size=\"5\">\r\n                <small><b>File Name :</b></small>\r\n            </ion-col>\r\n            <ion-col size=\"7\">\r\n                <small>{{fileinfo.fileName}}</small>\r\n            </ion-col>\r\n        </ion-row>\r\n        <ion-row *ngIf=\"fileinfo.time && fileinfo.time!=undefined\">\r\n            <ion-col size=\"5\">\r\n                <small><b>Time :</b></small>\r\n            </ion-col>\r\n            <ion-col size=\"7\">\r\n                <small>{{fileinfo.time}}</small>\r\n            </ion-col>\r\n        </ion-row>\r\n        <ion-row *ngIf=\"fileinfo.resolution && fileinfo.resolution!=undefined\">\r\n            <ion-col size=\"5\">\r\n                <small><b>Resolution :</b></small>\r\n            </ion-col>\r\n            <ion-col size=\"7\">\r\n                <small>{{fileinfo.resolution}}</small>\r\n            </ion-col>\r\n        </ion-row>\r\n        <ion-row *ngIf=\"fileinfo.size && fileinfo.size!=undefined\">\r\n            <ion-col size=\"5\">\r\n                <small><b>Size :</b></small>\r\n            </ion-col>\r\n            <ion-col size=\"7\">\r\n                <small>{{fileinfo.size}}</small>\r\n            </ion-col>\r\n        </ion-row>\r\n        <ion-row *ngIf=\"data.location && data.location!=undefined && data.location!='undefined,undefined'\">\r\n            <ion-col size=\"5\">\r\n                <small><b>Location :</b></small>\r\n            </ion-col>\r\n            <ion-col size=\"7\">\r\n                <small>{{data.location?data.location:'No location found'}}</small>\r\n            </ion-col>\r\n        </ion-row>\r\n        <ion-row *ngIf=\"latitude\">\r\n            <ion-col size=\"5\">\r\n                <small><b>Latitude :</b></small>\r\n            </ion-col>\r\n            <ion-col size=\"7\">\r\n                <small>{{latitude}}</small>\r\n            </ion-col>\r\n        </ion-row>\r\n        <ion-row *ngIf=\"longitude\">\r\n            <ion-col size=\"5\">\r\n                <small><b>Longitude :</b></small>\r\n            </ion-col>\r\n            <ion-col size=\"7\">\r\n\r\n                <small>{{longitude}}</small>\r\n            </ion-col>\r\n        </ion-row>\r\n    </ion-grid>\r\n</ion-content>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/image-crop-modal/image-crop-modal.page.html":
/*!***************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/image-crop-modal/image-crop-modal.page.html ***!
  \***************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\r\n  <ion-toolbar color=\"yellow\" mode=\"md\">\r\n    <ion-buttons slot=\"end\">\r\n      <ion-button (click)=\"dismissModal()\">\r\n        <ion-icon slot=\"icon-only\" name=\"close\"></ion-icon>\r\n      </ion-button>\r\n    </ion-buttons>\r\n    <ion-title>Crop & Orientation</ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content class=\"content-background\">\r\n  <ion-row>\r\n    <ion-col>\r\n      <image-cropper #imgcropper\r\n              [imageBase64]=\"imageBase64\"\r\n              [maintainAspectRatio]=\"maintainAspectRatio\"\r\n              [aspectRatio]=\"9 / 16\"\r\n              [resizeToWidth]=\"0\"\r\n              format=\"jpeg\"\r\n              (imageCropped)=\"imageCropped($event)\"\r\n              [canvasRotation]=\"canvasRotation\"\r\n              [transform]=\"transform\"\r\n              [alignImage]=\"'left'\"\r\n              [style.display]=\"showCropper ? null : 'none'\"\r\n              (imageLoaded)=\"imageLoaded()\"\r\n              [hideResizeSquares]=\"true\">\r\n\r\n      </image-cropper>\r\n    </ion-col>\r\n  </ion-row>\r\n</ion-content>\r\n\r\n<ion-footer>\r\n  <ion-toolbar color=\"yellow\" mode=\"md\">\r\n    <ion-buttons slot=\"start\">\r\n      <ion-button fill=\"clear\" (click)=\"rotateLeft()\">\r\n        <ion-icon slot=\"icon-only\" name=\"reload\" class=\"reloadLeftTransform\"></ion-icon>\r\n      </ion-button>\r\n      <ion-button fill=\"clear\" (click)=\"rotateRight()\">\r\n        <ion-icon slot=\"icon-only\" name=\"reload\"></ion-icon>\r\n      </ion-button>\r\n      <ion-button fill=\"clear\" (click)=\"flipHorizontal()\">\r\n        <ion-icon slot=\"icon-only\" name=\"swap-horizontal\"></ion-icon>\r\n      </ion-button>\r\n      <ion-button fill=\"clear\" (click)=\"toggleMaintainAspectRatio()\">\r\n        <ion-icon slot=\"icon-only\" name=\"contract\" *ngIf=\"maintainAspectRatio == false\"></ion-icon>\r\n        <ion-icon slot=\"icon-only\" name=\"expand\" *ngIf=\"maintainAspectRatio == true\"></ion-icon>\r\n      </ion-button>\r\n    </ion-buttons>\r\n    <ion-buttons slot=\"end\">\r\n      <ion-button color=\"dark\" fill=\"clear\"\r\n                  [disabled]=\"!imageBase64\"\r\n                  (click)=\"dismissModal(croppedImageBase64)\">\r\n        Save\r\n      </ion-button>\r\n    </ion-buttons>\r\n  </ion-toolbar>\r\n</ion-footer>\r\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/modal/preview/preview.page.html":
/*!***************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/modal/preview/preview.page.html ***!
  \***************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<!-- <ion-header>\r\n    <ion-toolbar>\r\n        <ion-title>Preview</ion-title>\r\n    </ion-toolbar>\r\n</ion-header> -->\r\n\r\n<ion-content padding color=\"white\">\r\n    <ion-grid>\r\n        <ion-row>\r\n            <ion-col>\r\n                <ion-button color=\"dark\" class=\"ion-float-right\" fill=\"clear\" (click)=\"ClosePopover()\">\r\n                    <ion-icon name=\"close-outline\"></ion-icon>\r\n                </ion-button>\r\n            </ion-col>\r\n        </ion-row>\r\n        <ion-row>\r\n            <ion-col>\r\n                <h5 class=\"ion-text-center\">Preview</h5>\r\n            </ion-col>\r\n        </ion-row>\r\n        <ion-row>\r\n            <ion-col>\r\n                <div>\r\n                    <img src=\"../../assets/image/2.png\" class=\"img_size\">\r\n                </div>\r\n\r\n            </ion-col>\r\n        </ion-row>\r\n\r\n    </ion-grid>\r\n</ion-content>");

/***/ }),

/***/ "./src/app/advertisement-modalpop/advertisement-modalpop-routing.module.ts":
/*!*********************************************************************************!*\
  !*** ./src/app/advertisement-modalpop/advertisement-modalpop-routing.module.ts ***!
  \*********************************************************************************/
/*! exports provided: AdvertisementModalpopPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AdvertisementModalpopPageRoutingModule", function() { return AdvertisementModalpopPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _advertisement_modalpop_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./advertisement-modalpop.page */ "./src/app/advertisement-modalpop/advertisement-modalpop.page.ts");




const routes = [
    {
        path: '',
        component: _advertisement_modalpop_page__WEBPACK_IMPORTED_MODULE_3__["AdvertisementModalpopPage"]
    }
];
let AdvertisementModalpopPageRoutingModule = class AdvertisementModalpopPageRoutingModule {
};
AdvertisementModalpopPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], AdvertisementModalpopPageRoutingModule);



/***/ }),

/***/ "./src/app/advertisement-modalpop/advertisement-modalpop.module.ts":
/*!*************************************************************************!*\
  !*** ./src/app/advertisement-modalpop/advertisement-modalpop.module.ts ***!
  \*************************************************************************/
/*! exports provided: AdvertisementModalpopPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AdvertisementModalpopPageModule", function() { return AdvertisementModalpopPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _advertisement_modalpop_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./advertisement-modalpop-routing.module */ "./src/app/advertisement-modalpop/advertisement-modalpop-routing.module.ts");
/* harmony import */ var _advertisement_modalpop_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./advertisement-modalpop.page */ "./src/app/advertisement-modalpop/advertisement-modalpop.page.ts");







let AdvertisementModalpopPageModule = class AdvertisementModalpopPageModule {
};
AdvertisementModalpopPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _advertisement_modalpop_routing_module__WEBPACK_IMPORTED_MODULE_5__["AdvertisementModalpopPageRoutingModule"]
        ],
        declarations: [_advertisement_modalpop_page__WEBPACK_IMPORTED_MODULE_6__["AdvertisementModalpopPage"]],
        exports: [_advertisement_modalpop_page__WEBPACK_IMPORTED_MODULE_6__["AdvertisementModalpopPage"]]
    })
], AdvertisementModalpopPageModule);



/***/ }),

/***/ "./src/app/advertisement-modalpop/advertisement-modalpop.page.scss":
/*!*************************************************************************!*\
  !*** ./src/app/advertisement-modalpop/advertisement-modalpop.page.scss ***!
  \*************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".notification-popup-container {\n  position: relative;\n  display: flex;\n  flex-direction: row;\n  align-items: center;\n  justify-content: center;\n}\n.notification-popup-container .close-icon {\n  position: absolute;\n  right: 5px;\n  font-size: 25px;\n  cursor: pointer;\n  top: -15px;\n  background: #ffd700;\n  border-radius: 50%;\n  padding: 2px;\n  z-index: 1111;\n}\n.notification-popup-container ion-img {\n  padding: 0px 20px;\n  position: relative;\n}\n.notification-popup-container ion-img:after {\n  content: \"\";\n  position: absolute;\n}\n.toolbarContent {\n  position: absolute;\n}\n.imgContent {\n  position: absolute;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvYWR2ZXJ0aXNlbWVudC1tb2RhbHBvcC9hZHZlcnRpc2VtZW50LW1vZGFscG9wLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLGtCQUFBO0VBQ0EsYUFBQTtFQUNBLG1CQUFBO0VBQ0EsbUJBQUE7RUFDQSx1QkFBQTtBQUNGO0FBQUU7RUFDRSxrQkFBQTtFQUNBLFVBQUE7RUFDQSxlQUFBO0VBQ0EsZUFBQTtFQUNBLFVBQUE7RUFDQSxtQkFBQTtFQUNBLGtCQUFBO0VBQ0EsWUFBQTtFQUNBLGFBQUE7QUFFSjtBQUFJO0VBQ0UsaUJBQUE7RUFDQSxrQkFBQTtBQUVOO0FBRE07RUFDRSxXQUFBO0VBQ0Esa0JBQUE7QUFHUjtBQUdBO0VBQ0Usa0JBQUE7QUFBRjtBQUdBO0VBQ0Usa0JBQUE7QUFBRiIsImZpbGUiOiJzcmMvYXBwL2FkdmVydGlzZW1lbnQtbW9kYWxwb3AvYWR2ZXJ0aXNlbWVudC1tb2RhbHBvcC5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIubm90aWZpY2F0aW9uLXBvcHVwLWNvbnRhaW5lcntcclxuICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgZGlzcGxheTogZmxleDtcclxuICBmbGV4LWRpcmVjdGlvbjogcm93O1xyXG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgLmNsb3NlLWljb257XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICByaWdodDogNXB4O1xyXG4gICAgZm9udC1zaXplOiAyNXB4O1xyXG4gICAgY3Vyc29yOiBwb2ludGVyO1xyXG4gICAgdG9wOiAtMTVweDtcclxuICAgIGJhY2tncm91bmQ6ICNmZmQ3MDA7XHJcbiAgICBib3JkZXItcmFkaXVzOiA1MCU7XHJcbiAgICBwYWRkaW5nOiAycHg7XHJcbiAgICB6LWluZGV4OiAxMTExO1xyXG4gIH1cclxuICAgIGlvbi1pbWd7XHJcbiAgICAgIHBhZGRpbmc6IDBweCAyMHB4O1xyXG4gICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICAgICY6YWZ0ZXJ7XHJcbiAgICAgICAgY29udGVudDogJyc7XHJcbiAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG5cclxuICAgICAgfVxyXG4gICAgfVxyXG59XHJcblxyXG4udG9vbGJhckNvbnRlbnR7XHJcbiAgcG9zaXRpb246IGFic29sdXRlO1xyXG59XHJcblxyXG4uaW1nQ29udGVudHtcclxuICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbn0iXX0= */");

/***/ }),

/***/ "./src/app/advertisement-modalpop/advertisement-modalpop.page.ts":
/*!***********************************************************************!*\
  !*** ./src/app/advertisement-modalpop/advertisement-modalpop.page.ts ***!
  \***********************************************************************/
/*! exports provided: AdvertisementModalpopPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AdvertisementModalpopPage", function() { return AdvertisementModalpopPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");



let AdvertisementModalpopPage = class AdvertisementModalpopPage {
    constructor(modalController) {
        this.modalController = modalController;
        this.notificationUrl = 'getNotificationImage';
        this.imageContent = '';
        this.userName = localStorage.getItem('username');
    }
    ngOnInit() {
        this.imageContent = localStorage.getItem('notification_img_url');
    }
    ngAfterViewInit() {
        // this.getnotificationContent();
    }
    dismiss() {
        // using the injected ModalController this page
        // can "dismiss" itself and optionally pass back data
        this.modalController.dismiss({
            'dismissed': true
        });
        localStorage.removeItem('notification_img_url');
    }
};
AdvertisementModalpopPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"] }
];
AdvertisementModalpopPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-advertisement-modalpop',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./advertisement-modalpop.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/advertisement-modalpop/advertisement-modalpop.page.html")).default,
        encapsulation: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewEncapsulation"].None,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./advertisement-modalpop.page.scss */ "./src/app/advertisement-modalpop/advertisement-modalpop.page.scss")).default]
    })
], AdvertisementModalpopPage);



/***/ }),

/***/ "./src/app/file-information/file-information.page.scss":
/*!*************************************************************!*\
  !*** ./src/app/file-information/file-information.page.scss ***!
  \*************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-content {\n  --background: #fff;\n}\n\nion-grid {\n  padding: 25px;\n  font-size: 18px;\n}\n\n.title {\n  text-align: left;\n  font-size: 20px;\n  font-weight: 500;\n}\n\n.close {\n  text-align: right;\n  font-size: 15px;\n  margin-top: 5px;\n}\n\n.close:hover {\n  cursor: pointer;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvZmlsZS1pbmZvcm1hdGlvbi9maWxlLWluZm9ybWF0aW9uLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFDQTtFQUNJLGtCQUFBO0FBQUo7O0FBRUE7RUFDSSxhQUFBO0VBQ0EsZUFBQTtBQUNKOztBQUNBO0VBQ0ksZ0JBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7QUFFSjs7QUFBQTtFQUNJLGlCQUFBO0VBQ0EsZUFBQTtFQUNBLGVBQUE7QUFHSjs7QUFEQTtFQUNJLGVBQUE7QUFJSiIsImZpbGUiOiJzcmMvYXBwL2ZpbGUtaW5mb3JtYXRpb24vZmlsZS1pbmZvcm1hdGlvbi5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJcclxuaW9uLWNvbnRlbnQge1xyXG4gICAgLS1iYWNrZ3JvdW5kOiAjZmZmO1xyXG59XHJcbmlvbi1ncmlkIHtcclxuICAgIHBhZGRpbmc6IDI1cHg7XHJcbiAgICBmb250LXNpemU6IDE4cHg7XHJcbn1cclxuLnRpdGxlIHtcclxuICAgIHRleHQtYWxpZ246IGxlZnQ7XHJcbiAgICBmb250LXNpemU6IDIwcHg7XHJcbiAgICBmb250LXdlaWdodDogNTAwO1xyXG59XHJcbi5jbG9zZSB7XHJcbiAgICB0ZXh0LWFsaWduOiByaWdodDtcclxuICAgIGZvbnQtc2l6ZTogMTVweDtcclxuICAgIG1hcmdpbi10b3A6IDVweDtcclxufVxyXG4uY2xvc2U6aG92ZXIge1xyXG4gICAgY3Vyc29yOiBwb2ludGVyO1xyXG59XHJcbiJdfQ== */");

/***/ }),

/***/ "./src/app/file-information/file-information.page.ts":
/*!***********************************************************!*\
  !*** ./src/app/file-information/file-information.page.ts ***!
  \***********************************************************/
/*! exports provided: FileInformationPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FileInformationPage", function() { return FileInformationPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");



let FileInformationPage = class FileInformationPage {
    constructor(modalController) {
        this.modalController = modalController;
        this.fileinfo = {};
        this.data = {};
    }
    ngOnInit() {
    }
    dismiss() {
        // using the injected ModalController this page
        // can "dismiss" itself and optionally pass back data
        this.modalController.dismiss({
            'dismissed': true
        });
    }
};
FileInformationPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"] }
];
FileInformationPage.propDecorators = {
    fileinfo: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"] }],
    data: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"] }],
    latitude: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"] }],
    longitude: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"] }]
};
FileInformationPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-file-information',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./file-information.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/file-information/file-information.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./file-information.page.scss */ "./src/app/file-information/file-information.page.scss")).default]
    })
], FileInformationPage);



/***/ }),

/***/ "./src/app/image-crop-modal/image-crop-modal.page.scss":
/*!*************************************************************!*\
  !*** ./src/app/image-crop-modal/image-crop-modal.page.scss ***!
  \*************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".content-background {\n  --background: #1f1e1d;\n}\n.content-background image-cropper {\n  --background: #1f1e1d;\n  --cropper-overlay-color:#1f1e1d;\n  --cropper-outline-color: none;\n  align-items: center;\n  height: calc(100vh - 117px);\n}\n.content-background image-cropper .cropper .move {\n  border: 1px solid #ffe000 !important;\n}\n.reloadLeftTransform {\n  transform: scaleX(-1);\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvaW1hZ2UtY3JvcC1tb2RhbC9pbWFnZS1jcm9wLW1vZGFsLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUVJLHFCQUFBO0FBQUo7QUFHSTtFQUNJLHFCQUFBO0VBQ0EsK0JBQUE7RUFDQSw2QkFBQTtFQUNBLG1CQUFBO0VBQ0EsMkJBQUE7QUFEUjtBQUdZO0VBQ0ksb0NBQUE7QUFEaEI7QUFTQTtFQUNJLHFCQUFBO0FBTkoiLCJmaWxlIjoic3JjL2FwcC9pbWFnZS1jcm9wLW1vZGFsL2ltYWdlLWNyb3AtbW9kYWwucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmNvbnRlbnQtYmFja2dyb3VuZCB7XHJcblxyXG4gICAgLS1iYWNrZ3JvdW5kOiAjMWYxZTFkO1xyXG5cclxuXHJcbiAgICBpbWFnZS1jcm9wcGVye1xyXG4gICAgICAgIC0tYmFja2dyb3VuZDogIzFmMWUxZDtcclxuICAgICAgICAtLWNyb3BwZXItb3ZlcmxheS1jb2xvcjojMWYxZTFkOyBcclxuICAgICAgICAtLWNyb3BwZXItb3V0bGluZS1jb2xvcjogbm9uZTtcclxuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgICAgIGhlaWdodDogY2FsYygxMDB2aCAtIDExN3B4KTtcclxuICAgICAgICAuY3JvcHBlcntcclxuICAgICAgICAgICAgLm1vdmV7XHJcbiAgICAgICAgICAgICAgICBib3JkZXI6IDFweCBzb2xpZCByZ2IoMjU1LCAyMjQsIDApIWltcG9ydGFudDtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcblxyXG59XHJcblxyXG4ucmVsb2FkTGVmdFRyYW5zZm9ybXtcclxuICAgIHRyYW5zZm9ybTogc2NhbGVYKC0xKTtcclxufSJdfQ== */");

/***/ }),

/***/ "./src/app/image-crop-modal/image-crop-modal.page.ts":
/*!***********************************************************!*\
  !*** ./src/app/image-crop-modal/image-crop-modal.page.ts ***!
  \***********************************************************/
/*! exports provided: ImageCropModalPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ImageCropModalPage", function() { return ImageCropModalPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");



let ImageCropModalPage = class ImageCropModalPage {
    constructor(modalController, platform) {
        this.modalController = modalController;
        this.platform = platform;
        this.croppedImageBase64 = '';
        this.showCropper = false;
        this.canvasRotation = 0;
        this.maintainAspectRatio = true;
        this.transform = {};
        this.imageBase64 = '';
    }
    ngOnInit() {
    }
    ionViewDidEnter() {
        this.subscription = this.platform.backButton.subscribeWithPriority(9999, () => {
            this.dismissModal();
        });
    }
    ionViewWillLeave() {
        this.subscription.unsubscribe();
    }
    imageLoaded() {
        this.showCropper = true;
    }
    imageCropped(event) {
        this.croppedImageBase64 = event.base64;
    }
    dismissModal(croppedImageBase64) {
        this.modalController.dismiss({ croppedImageBase64 });
    }
    rotateLeft() {
        this.canvasRotation--;
        this.flipAfterRotate();
    }
    rotateRight() {
        this.canvasRotation++;
        this.flipAfterRotate();
    }
    toggleMaintainAspectRatio() {
        setTimeout(() => {
            this.imgcropper.resetCropperPosition();
        }, 100);
        this.maintainAspectRatio = !this.maintainAspectRatio;
    }
    flipAfterRotate() {
        const flippedH = this.transform.flipH;
        const flippedV = this.transform.flipV;
        this.transform = Object.assign(Object.assign({}, this.transform), { flipH: flippedV, flipV: flippedH });
    }
    flipHorizontal() {
        this.transform = Object.assign(Object.assign({}, this.transform), { flipH: !this.transform.flipH });
    }
    flipVertical() {
        this.transform = Object.assign(Object.assign({}, this.transform), { flipV: !this.transform.flipV });
    }
};
ImageCropModalPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["Platform"] }
];
ImageCropModalPage.propDecorators = {
    imgcropper: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"], args: ['imgcropper', { static: false },] }],
    imageBase64: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"] }]
};
ImageCropModalPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-image-crop-modal',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./image-crop-modal.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/image-crop-modal/image-crop-modal.page.html")).default,
        changeDetection: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectionStrategy"].OnPush,
        encapsulation: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewEncapsulation"].None,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./image-crop-modal.page.scss */ "./src/app/image-crop-modal/image-crop-modal.page.scss")).default]
    })
], ImageCropModalPage);



/***/ }),

/***/ "./src/app/modal/preview/preview.page.scss":
/*!*************************************************!*\
  !*** ./src/app/modal/preview/preview.page.scss ***!
  \*************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".img_size {\n  width: 100%;\n  -o-object-fit: cover;\n     object-fit: cover;\n  -o-object-position: center;\n     object-position: center;\n  background-color: white;\n  height: 250px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbW9kYWwvcHJldmlldy9wcmV2aWV3LnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLFdBQUE7RUFDQSxvQkFBQTtLQUFBLGlCQUFBO0VBQ0EsMEJBQUE7S0FBQSx1QkFBQTtFQUNBLHVCQUFBO0VBQ0EsYUFBQTtBQUNKIiwiZmlsZSI6InNyYy9hcHAvbW9kYWwvcHJldmlldy9wcmV2aWV3LnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5pbWdfc2l6ZSB7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIG9iamVjdC1maXQ6IGNvdmVyO1xyXG4gICAgb2JqZWN0LXBvc2l0aW9uOiBjZW50ZXI7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiBsaWdodGVuKHdoaXRlLCA3MCUpO1xyXG4gICAgaGVpZ2h0OiAyNTBweDtcclxufSJdfQ== */");

/***/ }),

/***/ "./src/app/modal/preview/preview.page.ts":
/*!***********************************************!*\
  !*** ./src/app/modal/preview/preview.page.ts ***!
  \***********************************************/
/*! exports provided: PreviewPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PreviewPage", function() { return PreviewPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");



let PreviewPage = class PreviewPage {
    constructor(popover) {
        this.popover = popover;
    }
    ngOnInit() {
    }
    ClosePopover() {
        this.popover.dismiss();
    }
};
PreviewPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["PopoverController"] }
];
PreviewPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-preview',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./preview.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/modal/preview/preview.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./preview.page.scss */ "./src/app/modal/preview/preview.page.scss")).default]
    })
], PreviewPage);



/***/ })

}]);
//# sourceMappingURL=common-es2015.js.map