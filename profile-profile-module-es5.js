(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["profile-profile-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/profile/profile.page.html":
    /*!*********************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/profile/profile.page.html ***!
      \*********************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppProfileProfilePageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<!-- <ion-header translucent no-border>\r\n    <ion-toolbar>\r\n        <ion-buttons slot=\"start\">\r\n            <ion-back-button defaultHref=\"/\"></ion-back-button>\r\n        </ion-buttons>\r\n        <ion-title>Profile</ion-title>\r\n    </ion-toolbar>\r\n</ion-header> -->\r\n<ion-content>\r\n\r\n    <ion-list class=\"ion-no-padding\">\r\n\r\n        <ion-img [src]=\"loggedDetails.upload_image\" class=\"img_size\" *ngIf=\"loggedDetails.upload_image\"></ion-img>\r\n        <ion-img src=\"../../assets/image/avatar_up.jpg\" class=\"img_size\" *ngIf=\"!loggedDetails.upload_image\">\r\n        </ion-img>\r\n        <ion-buttons back_funct slot=\"start\">\r\n            <ion-back-button color=\"white\" defaultHref=\"home\"></ion-back-button>\r\n        </ion-buttons>\r\n        <!-- <ion-grid>\r\n            <ion-row class=\"ion-justify-content-center\">\r\n                <ion-col>\r\n                   \r\n                </ion-col>\r\n            </ion-row> -->\r\n        <ion-item header-menu role_funct lines=\"none\">\r\n            <ion-label>\r\n                <h3 color=\"yellow\" class=\"ion-text-capitalize\">{{loggedDetails.stud_name}}</h3>\r\n                <!-- <p color=\"white\">{{loggedDetails.city}}</p> -->\r\n                <p color=\"white\">{{City}}</p>\r\n            </ion-label>\r\n        </ion-item>\r\n        <!-- </ion-grid> -->\r\n    </ion-list>\r\n\r\n    <div class=\"profile-details\">\r\n        <ion-grid class=\"mt-5\">\r\n            <ion-row>\r\n                <ion-col>\r\n                    <ion-item header-menu lines=\"none\">\r\n                        <ion-label class=\"d-flex\">\r\n                            <ion-icon name=\"person\" color=\"yellow\"></ion-icon> &nbsp;\r\n                            <h5 color=\"light\" class=\"ion-text-capitalize\">Profile</h5>\r\n                        </ion-label>\r\n                    </ion-item>\r\n                </ion-col>\r\n            </ion-row>\r\n            <ion-row>\r\n                <ion-col>\r\n                    <ion-item header-menu lines=\"none\">\r\n                        <ion-label>\r\n                            <h3 color=\"yellow\" class=\"ion-text-capitalize\">{{loggedDetails.mobileno}}</h3>\r\n                            <p color=\"white\">Mobile #</p>\r\n                        </ion-label>\r\n                    </ion-item>\r\n                </ion-col>\r\n            </ion-row>\r\n            <ion-row>\r\n                <ion-col>\r\n                    <ion-item header-menu lines=\"none\">\r\n                        <ion-label>\r\n                            <h3 color=\"yellow\" class=\"ion-text-capitalize\">{{loggedDetails.emailid}}</h3>\r\n                            <p color=\"white\">Email</p>\r\n                        </ion-label>\r\n                    </ion-item>\r\n                </ion-col>\r\n            </ion-row>\r\n            <ion-row>\r\n                <ion-col>\r\n                    <ion-item header-menu lines=\"none\">\r\n                        <ion-label>\r\n                            <h3 color=\"yellow\" class=\"ion-text-capitalize\">{{age}}</h3>\r\n                            <p color=\"white\">Age</p>\r\n                        </ion-label>\r\n                    </ion-item>\r\n                </ion-col>\r\n                <ion-col>\r\n                    <ion-item header-menu lines=\"none\">\r\n                        <ion-label>\r\n                            <h3 color=\"yellow\" class=\"ion-text-capitalize\">{{loggedDetails.gender}}</h3>\r\n                            <p color=\"white\">Gender</p>\r\n                        </ion-label>\r\n                    </ion-item>\r\n                </ion-col>\r\n            </ion-row>\r\n            <ion-row>\r\n                <ion-col>\r\n                    <ion-item header-menu lines=\"none\">\r\n                        <ion-label>\r\n                            <h3 color=\"yellow\" class=\"ion-text-capitalize\">{{loggedDetails.interests}}</h3>\r\n                            <p color=\"white\">Interest</p>\r\n                        </ion-label>\r\n                    </ion-item>\r\n                </ion-col>\r\n            </ion-row>\r\n            <ion-row>\r\n                <ion-col>\r\n                    <ion-item header-menu lines=\"none\">\r\n                        <ion-label>\r\n                            <h3 color=\"yellow\" class=\"ion-text-capitalize\">{{loggedDetails.hobby}}</h3>\r\n                            <p color=\"white\">Hobbies</p>\r\n                        </ion-label>\r\n                    </ion-item>\r\n                </ion-col>\r\n            </ion-row>\r\n            <ion-row>\r\n                <ion-col>\r\n                    <ion-item header-menu lines=\"none\">\r\n                        <ion-label class=\"d-flex\">\r\n                            <ion-icon color=\"yellow\" name=\"apps\"></ion-icon> &nbsp;\r\n                            <h5 color=\"light\" class=\"ion-text-capitalize\">Social Media</h5>\r\n                        </ion-label>\r\n                    </ion-item>\r\n                </ion-col>\r\n            </ion-row>\r\n            <ion-row>\r\n                <ion-col align-self-center>\r\n                    <ion-item header-menu lines=\"none\" *ngIf=\"LinkedInEdit == false\">\r\n                        <!-- <ion-icon slot=\"start\" name=\"logo-linkedin\" color=\"light\" size=\"small\"  class=\"ion-align-self-end\">\r\n                        </ion-icon> -->\r\n                        <ion-label color=\"yellow\">\r\n                            <ion-icon slot=\"start\" name=\"logo-linkedin\" color=\"light\" size=\"small\"\r\n                                class=\"ion-align-self-end\">\r\n                            </ion-icon> &nbsp; {{loggedDetails.linkedin_id}}\r\n                        </ion-label>\r\n                        <ion-button color=\"yellow\" (click)='editProfile(\"linkedin\")' slot=\"end\">\r\n                            <ion-icon name=\"pencil-outline\"></ion-icon>\r\n                        </ion-button>\r\n                    </ion-item>\r\n                    <ion-item class=\"margin-rl\" input-text-color *ngIf=\"LinkedInEdit == true\">\r\n                        <ion-label label color=\"light\" position=\"floating\">Linkedin\r\n                        </ion-label>\r\n                        <ion-input type=\"text\" #linkedin [ngModel]=\"loggedDetails.linkedin_id\"\r\n                            placeholder=\" xyz.linkedin@gmail.com\"></ion-input>\r\n                        <ion-icon name=\"save-outline\" color=\"yellow\" size=\"small\"\r\n                            (click)='saveProfile(\"linkedin\",linkedin.value)' class=\"ion-align-self-end\" slot=\"end\">\r\n                        </ion-icon>\r\n                    </ion-item>\r\n                </ion-col>\r\n            </ion-row>\r\n            <ion-row>\r\n                <ion-col align-self-center>\r\n                    <ion-item header-menu lines=\"none\" *ngIf=\"instaEdit == false\">\r\n                        <!-- <ion-icon slot=\"start\" name=\"logo-linkedin\" color=\"light\" size=\"small\"  class=\"ion-align-self-end\">\r\n                        </ion-icon> -->\r\n                        <ion-label color=\"yellow\">\r\n                            <ion-icon slot=\"start\" name=\"logo-instagram\" color=\"light\" size=\"small\"\r\n                                class=\"ion-align-self-end\">\r\n                            </ion-icon> &nbsp; {{loggedDetails.insta_id}}\r\n                        </ion-label>\r\n                        <ion-button color=\"yellow\" (click)='editProfile(\"insta\")' slot=\"end\">\r\n                            <ion-icon name=\"pencil-outline\"></ion-icon>\r\n                        </ion-button>\r\n                    </ion-item>\r\n                    <ion-item class=\"margin-rl\" input-text-color *ngIf=\"instaEdit == true\">\r\n                        <ion-label label color=\"light\" position=\"floating\">Instagram\r\n                        </ion-label>\r\n                        <ion-input #insta type=\"text\" [ngModel]=\"loggedDetails.insta_id\"\r\n                            placeholder=\" xyz.fashionIndia\">\r\n                        </ion-input>\r\n                        <ion-icon name=\"save-outline\" color=\"yellow\" size=\"small\"\r\n                            (click)='saveProfile(\"insta\",insta.value)' class=\"ion-align-self-end\" slot=\"end\">\r\n                        </ion-icon>\r\n                    </ion-item>\r\n                </ion-col>\r\n            </ion-row>\r\n            <ion-row>\r\n                <ion-col align-self-center>\r\n                    <ion-item header-menu lines=\"none\" *ngIf=\"fbEdit == false\">\r\n                        <ion-label color=\"yellow\">\r\n                            <ion-icon slot=\"start\" name=\"logo-facebook\" color=\"light\" size=\"small\"\r\n                                class=\"ion-align-self-end\">\r\n                            </ion-icon> &nbsp; {{loggedDetails.fb_id}}\r\n                        </ion-label>\r\n                        <ion-button color=\"yellow\" (click)='editProfile(\"fb\")' slot=\"end\">\r\n                            <ion-icon name=\"pencil-outline\"></ion-icon>\r\n                        </ion-button>\r\n                    </ion-item>\r\n                    <ion-item class=\"margin-rl\" input-text-color *ngIf=\"fbEdit == true\">\r\n                        <ion-label label color=\"light\" position=\"floating\">Facebook\r\n                        </ion-label>\r\n                        <ion-input #fb type=\"text\" [ngModel]=\"loggedDetails.fb_id\" placeholder=\"xyz@gmail.com\">\r\n                        </ion-input>\r\n                        <ion-icon name=\"save-outline\" color=\"yellow\" size=\"small\" (click)='saveProfile(\"fb\",fb.value)'\r\n                            class=\"ion-align-self-end\" slot=\"end\">\r\n                        </ion-icon>\r\n                    </ion-item>\r\n                </ion-col>\r\n            </ion-row>\r\n        </ion-grid>\r\n    </div>\r\n</ion-content>";
      /***/
    },

    /***/
    "./src/app/profile/profile-routing.module.ts":
    /*!***************************************************!*\
      !*** ./src/app/profile/profile-routing.module.ts ***!
      \***************************************************/

    /*! exports provided: ProfilePageRoutingModule */

    /***/
    function srcAppProfileProfileRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ProfilePageRoutingModule", function () {
        return ProfilePageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _profile_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./profile.page */
      "./src/app/profile/profile.page.ts");

      var routes = [{
        path: '',
        component: _profile_page__WEBPACK_IMPORTED_MODULE_3__["ProfilePage"]
      }];

      var ProfilePageRoutingModule = function ProfilePageRoutingModule() {
        _classCallCheck(this, ProfilePageRoutingModule);
      };

      ProfilePageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], ProfilePageRoutingModule);
      /***/
    },

    /***/
    "./src/app/profile/profile.module.ts":
    /*!*******************************************!*\
      !*** ./src/app/profile/profile.module.ts ***!
      \*******************************************/

    /*! exports provided: ProfilePageModule */

    /***/
    function srcAppProfileProfileModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ProfilePageModule", function () {
        return ProfilePageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _profile_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./profile-routing.module */
      "./src/app/profile/profile-routing.module.ts");
      /* harmony import */


      var _profile_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./profile.page */
      "./src/app/profile/profile.page.ts");

      var ProfilePageModule = function ProfilePageModule() {
        _classCallCheck(this, ProfilePageModule);
      };

      ProfilePageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _profile_routing_module__WEBPACK_IMPORTED_MODULE_5__["ProfilePageRoutingModule"]],
        declarations: [_profile_page__WEBPACK_IMPORTED_MODULE_6__["ProfilePage"]]
      })], ProfilePageModule);
      /***/
    },

    /***/
    "./src/app/profile/profile.page.scss":
    /*!*******************************************!*\
      !*** ./src/app/profile/profile.page.scss ***!
      \*******************************************/

    /*! exports provided: default */

    /***/
    function srcAppProfileProfilePageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = ".img_size {\n  width: 100%;\n  -o-object-fit: cover;\n     object-fit: cover;\n  -o-object-position: center;\n     object-position: center;\n  background-color: var(--ion-color-pantoneblack);\n  height: 25rem;\n}\n\n[back_funct] {\n  position: absolute;\n  top: 5%;\n  z-index: 999;\n}\n\n[role_funct] {\n  position: absolute;\n  top: 15rem;\n  z-index: 999;\n}\n\n[role_funct] p {\n  color: var(--ion-color-white);\n}\n\n[role_funct] h3 {\n  font-weight: bolder !important;\n  font-size: 1.2em !important;\n  color: var(--ion-color-yellow);\n}\n\n.profile-details {\n  margin-top: -80px;\n  background: #292927;\n  border-radius: 30px 30px 0 0;\n  position: relative;\n  bottom: 0;\n  box-shadow: 2px 6px 25px -9px black;\n}\n\n.profile-details p {\n  color: var(--ion-color-white);\n}\n\n.profile-details h3 {\n  font-weight: bolder !important;\n  font-size: 1em !important;\n  color: var(--ion-color-yellow);\n}\n\n.profile-details h5 {\n  font-weight: lighter !important;\n  font-size: 1em !important;\n  color: var(--ion-color-light);\n}\n\n.margin-rl {\n  margin: 0 15px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcHJvZmlsZS9wcm9maWxlLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBRTtFQUNJLFdBQUE7RUFFQSxvQkFBQTtLQUFBLGlCQUFBO0VBQ0EsMEJBQUE7S0FBQSx1QkFBQTtFQUNBLCtDQUFBO0VBRUYsYUFBQTtBQURKOztBQUtFO0VBQ0ksa0JBQUE7RUFDQSxPQUFBO0VBQ0EsWUFBQTtBQUZOOztBQUtFO0VBQ0ksa0JBQUE7RUFFQSxVQUFBO0VBQ0EsWUFBQTtBQUhOOztBQUlNO0VBQ0ksNkJBQUE7QUFGVjs7QUFJTTtFQUNJLDhCQUFBO0VBQ0EsMkJBQUE7RUFDQSw4QkFBQTtBQUZWOztBQU1FO0VBRUksaUJBQUE7RUFDQSxtQkFBQTtFQUNBLDRCQUFBO0VBQ0Esa0JBQUE7RUFDQSxTQUFBO0VBQ0EsbUNBQUE7QUFKTjs7QUFLTTtFQUNJLDZCQUFBO0FBSFY7O0FBS007RUFDSSw4QkFBQTtFQUNBLHlCQUFBO0VBQ0EsOEJBQUE7QUFIVjs7QUFLTTtFQUNFLCtCQUFBO0VBQ0EseUJBQUE7RUFDQSw2QkFBQTtBQUhSOztBQU9FO0VBQ0ksY0FBQTtBQUpOIiwiZmlsZSI6InNyYy9hcHAvcHJvZmlsZS9wcm9maWxlLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIiAgLmltZ19zaXplIHtcclxuICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAvLyAgIG9iamVjdC1maXQ6IGNvbnRhaW47XHJcbiAgICAgIG9iamVjdC1maXQ6IGNvdmVyO1xyXG4gICAgICBvYmplY3QtcG9zaXRpb246IGNlbnRlcjtcclxuICAgICAgYmFja2dyb3VuZC1jb2xvcjp2YXIoLS1pb24tY29sb3ItcGFudG9uZWJsYWNrKTtcclxuICAgIC8vICAgaGVpZ2h0OiAyNzBweDtcclxuICAgIGhlaWdodDogMjVyZW07XHJcblxyXG4gIH1cclxuICBcclxuICBbYmFja19mdW5jdF0ge1xyXG4gICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgIHRvcDogNSU7XHJcbiAgICAgIHotaW5kZXg6IDk5OTtcclxuICB9XHJcbiAgXHJcbiAgW3JvbGVfZnVuY3RdIHtcclxuICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgLy8gICBib3R0b206IDMwJTtcclxuICAgICAgdG9wOiAxNXJlbTtcclxuICAgICAgei1pbmRleDogOTk5O1xyXG4gICAgICBwIHtcclxuICAgICAgICAgIGNvbG9yOiB2YXIoLS1pb24tY29sb3Itd2hpdGUpO1xyXG4gICAgICB9XHJcbiAgICAgIGgzIHtcclxuICAgICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkZXIgIWltcG9ydGFudDtcclxuICAgICAgICAgIGZvbnQtc2l6ZTogMS4yZW0gIWltcG9ydGFudDtcclxuICAgICAgICAgIGNvbG9yOiB2YXIoLS1pb24tY29sb3IteWVsbG93KTtcclxuICAgICAgfVxyXG4gIH1cclxuICBcclxuICAucHJvZmlsZS1kZXRhaWxzIHtcclxuICAgIC8vICAgcGFkZGluZzogMTBweDtcclxuICAgICAgbWFyZ2luLXRvcDogLTgwcHg7XHJcbiAgICAgIGJhY2tncm91bmQ6ICMyOTI5Mjc7XHJcbiAgICAgIGJvcmRlci1yYWRpdXM6IDMwcHggMzBweCAwIDA7XHJcbiAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgICAgYm90dG9tOiAwO1xyXG4gICAgICBib3gtc2hhZG93OiAycHggNnB4IDI1cHggLTlweCBibGFjaztcclxuICAgICAgcCB7XHJcbiAgICAgICAgICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLXdoaXRlKTtcclxuICAgICAgfVxyXG4gICAgICBoMyB7XHJcbiAgICAgICAgICBmb250LXdlaWdodDogYm9sZGVyICFpbXBvcnRhbnQ7XHJcbiAgICAgICAgICBmb250LXNpemU6IDFlbSAhaW1wb3J0YW50O1xyXG4gICAgICAgICAgY29sb3I6IHZhcigtLWlvbi1jb2xvci15ZWxsb3cpO1xyXG4gICAgICB9XHJcbiAgICAgIGg1IHtcclxuICAgICAgICBmb250LXdlaWdodDogbGlnaHRlciAhaW1wb3J0YW50O1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMWVtICFpbXBvcnRhbnQ7XHJcbiAgICAgICAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1saWdodCk7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICAubWFyZ2luLXJse1xyXG4gICAgICBtYXJnaW46IDAgMTVweDtcclxuICB9Il19 */";
      /***/
    },

    /***/
    "./src/app/profile/profile.page.ts":
    /*!*****************************************!*\
      !*** ./src/app/profile/profile.page.ts ***!
      \*****************************************/

    /*! exports provided: ProfilePage */

    /***/
    function srcAppProfileProfilePageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ProfilePage", function () {
        return ProfilePage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _global_services_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ../global-services.service */
      "./src/app/global-services.service.ts");

      var ProfilePage = /*#__PURE__*/function () {
        function ProfilePage(service) {
          _classCallCheck(this, ProfilePage);

          this.service = service; //=====================Edit Profile=====================

          this.LinkedInEdit = false;
          this.instaEdit = false;
          this.fbEdit = false; //=====================get Login Details=====================

          this.loggedDetails = {};
          this.City = localStorage.getItem('location');
        }

        _createClass(ProfilePage, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }, {
          key: "ionViewWillEnter",
          value: function ionViewWillEnter() {
            this.userInfo();
          }
        }, {
          key: "editProfile",
          value: function editProfile(type) {
            if (type == 'linkedin') {
              this.LinkedInEdit = true;
            }

            if (type == 'insta') {
              this.instaEdit = true;
            }

            if (type == 'fb') {
              this.fbEdit = true;
            }
          } //=====================Save Profile=====================

        }, {
          key: "saveProfile",
          value: function saveProfile(type, value) {
            var _this = this;

            var obj = {};

            if (type == 'linkedin') {
              this.LinkedInEdit = false;
              obj = {
                user_name: localStorage.getItem('username'),
                linkedin_id: value,
                fb_id: this.loggedDetails.fb_id,
                instagram_id: this.loggedDetails.insta_id
              };
            }

            if (type == 'insta') {
              this.instaEdit = false;
              obj = {
                user_name: localStorage.getItem('username'),
                linkedin_id: this.loggedDetails.linkedin_id,
                fb_id: this.loggedDetails.fb_id,
                instagram_id: value
              };
            }

            if (type == 'fb') {
              this.fbEdit = false;
              obj = {
                user_name: localStorage.getItem('username'),
                linkedin_id: this.loggedDetails.linkedin_id,
                fb_id: value,
                instagram_id: this.loggedDetails.insta_id
              };
            }

            if (localStorage.getItem('username')) {
              this.service.post_data("updateSocialMedia", obj).subscribe(function (result) {
                var json = _this.service.parserToJSON(result);

                var parsedJSON = JSON.parse(json.string);

                _this.userInfo();
              }, function (error) {
                console.log(error);
              });
            }
          }
        }, {
          key: "userInfo",
          value: function userInfo() {
            var _this2 = this;

            if (localStorage.getItem('username')) {
              var obj = {
                username: localStorage.getItem('username')
              };
              this.service.post_data("getAlumniOthersInfo", obj).subscribe(function (result) {
                var json = _this2.service.parserToJSON(result);

                var parsedJSON = JSON.parse(json.string);
                _this2.loggedDetails = parsedJSON[0];
                var timeDiff = Math.abs(Date.now() - new Date(_this2.loggedDetails.dob).getTime());
                _this2.age = Math.floor(timeDiff / (1000 * 3600 * 24) / 365.25);
              }, function (error) {
                console.log(error);
              });
            }
          }
        }]);

        return ProfilePage;
      }();

      ProfilePage.ctorParameters = function () {
        return [{
          type: _global_services_service__WEBPACK_IMPORTED_MODULE_2__["GlobalServicesService"]
        }];
      };

      ProfilePage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-profile',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./profile.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/profile/profile.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./profile.page.scss */
        "./src/app/profile/profile.page.scss"))["default"]]
      })], ProfilePage);
      /***/
    }
  }]);
})();
//# sourceMappingURL=profile-profile-module-es5.js.map