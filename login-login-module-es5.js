(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["login-login-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/login/login.page.html":
    /*!*****************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/login/login.page.html ***!
      \*****************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppLoginLoginPageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-header translucent no-border>\r\n    <ion-toolbar>\r\n        <ion-buttons slot=\"start\">\r\n            <ion-back-button defaultHref=\"/\"></ion-back-button>\r\n        </ion-buttons>\r\n        <ion-title>Sign In</ion-title>\r\n    </ion-toolbar>\r\n</ion-header>\r\n<ion-content>\r\n    <div class=\"inner-div\">\r\n        <form [formGroup]=\"signinFg\" autocomplete=\"off\" #f=\"ngForm\" (ngSubmit)=\"signinFg.valid && signinFunction()\">\r\n            <ion-grid>\r\n                <ion-row>\r\n                    <ion-col size=\"1\"></ion-col>\r\n                    <ion-col align-self-center size=\"10\">\r\n                        <ion-img src=\"../../assets/image/logo-removebg.png\" class=\"w-45 mx-auto mt-3\"></ion-img>\r\n                    </ion-col>\r\n                    <ion-col size=\"1\"></ion-col>\r\n                </ion-row>\r\n                <ion-row class=\"mt-5\">\r\n                    <ion-col align-self-center>\r\n                        <ion-item input-text-color>\r\n                            <ion-label color=\"light\" position=\"floating\">Username / Roll number\r\n                            </ion-label>\r\n                            <ion-input formControlName=\"username\" type=\"text\" required></ion-input>\r\n                            <ion-icon name=\"person-outline\" color=\"light\" size=\"small\" class=\"ion-align-self-end\"\r\n                                slot=\"end\"></ion-icon>\r\n                        </ion-item>\r\n                        <div class=\"error\"\r\n                            *ngIf=\"signinFg.controls['username'].errors?.required && (signinFg.get('username').touched || f.submitted)\">\r\n                            Username / Roll number is Required.</div>\r\n                    </ion-col>\r\n                </ion-row>\r\n                <ion-row>\r\n                    <ion-col align-self-center>\r\n                        <ion-item input-text-color>\r\n                            <ion-label label color=\"light\" position=\"floating\">Password / CMS password\r\n                            </ion-label>\r\n                            <ion-input [type]=\"passwordType\" formControlName=\"pwd\" required></ion-input>\r\n                            <ion-icon [name]=\"passwordIcon\" color=\"light\" size=\"small\" (click)='hideShowPassword()'\r\n                                class=\"ion-align-self-end\" slot=\"end\">\r\n                            </ion-icon>\r\n                        </ion-item>\r\n                        <div class=\"error\"\r\n                            *ngIf=\"signinFg.controls['pwd'].errors?.required && (signinFg.get('pwd').touched || f.submitted)\">\r\n                            Password / CMS password is Required.</div>\r\n                    </ion-col>\r\n                </ion-row>\r\n                <ion-row>\r\n                    <ion-col align-self-center class=\"col-padd\">\r\n                        <ion-button type=\"submit\" expand=\"block\" color=\"btn-yellow btn-height\" size=\"large\" btn-yellow\r\n                            class=\"mt-15\">\r\n                            Sign In\r\n                        </ion-button>\r\n                    </ion-col>\r\n                </ion-row>\r\n                <ion-row>\r\n                    <ion-col class=\"ion-text-center mar-top\">\r\n                        <ion-router-link [routerLink]=\"['/forgot-password']\" color=\"light\" class=\"underline\">\r\n                            Forgot Password ?\r\n                        </ion-router-link>\r\n                    </ion-col>\r\n                </ion-row>\r\n            </ion-grid>\r\n        </form>\r\n    </div>\r\n</ion-content>";
      /***/
    },

    /***/
    "./src/app/login/login-routing.module.ts":
    /*!***********************************************!*\
      !*** ./src/app/login/login-routing.module.ts ***!
      \***********************************************/

    /*! exports provided: LoginPageRoutingModule */

    /***/
    function srcAppLoginLoginRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "LoginPageRoutingModule", function () {
        return LoginPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _login_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./login.page */
      "./src/app/login/login.page.ts");

      var routes = [{
        path: '',
        component: _login_page__WEBPACK_IMPORTED_MODULE_3__["LoginPage"]
      }];

      var LoginPageRoutingModule = function LoginPageRoutingModule() {
        _classCallCheck(this, LoginPageRoutingModule);
      };

      LoginPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], LoginPageRoutingModule);
      /***/
    },

    /***/
    "./src/app/login/login.module.ts":
    /*!***************************************!*\
      !*** ./src/app/login/login.module.ts ***!
      \***************************************/

    /*! exports provided: LoginPageModule */

    /***/
    function srcAppLoginLoginModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "LoginPageModule", function () {
        return LoginPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _login_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./login-routing.module */
      "./src/app/login/login-routing.module.ts");
      /* harmony import */


      var _login_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./login.page */
      "./src/app/login/login.page.ts");

      var LoginPageModule = function LoginPageModule() {
        _classCallCheck(this, LoginPageModule);
      };

      LoginPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _login_routing_module__WEBPACK_IMPORTED_MODULE_5__["LoginPageRoutingModule"]],
        declarations: [_login_page__WEBPACK_IMPORTED_MODULE_6__["LoginPage"]]
      })], LoginPageModule);
      /***/
    },

    /***/
    "./src/app/login/login.page.scss":
    /*!***************************************!*\
      !*** ./src/app/login/login.page.scss ***!
      \***************************************/

    /*! exports provided: default */

    /***/
    function srcAppLoginLoginPageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = ".error {\n  color: var(--ion-color-yellow);\n  font-size: 12px;\n}\n\n.inner-div {\n  position: absolute !important;\n  min-height: 95% !important;\n  display: block;\n}\n\n.btn-height {\n  height: 85%;\n  border-radius: 10px;\n}\n\n.col-padd {\n  padding: 15px 25px;\n}\n\n.mar-top {\n  margin-top: 15px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbG9naW4vbG9naW4ucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksOEJBQUE7RUFDQSxlQUFBO0FBQ0o7O0FBRUE7RUFDSSw2QkFBQTtFQUNBLDBCQUFBO0VBQ0EsY0FBQTtBQUNKOztBQUVBO0VBQ0ksV0FBQTtFQUNBLG1CQUFBO0FBQ0o7O0FBQ0E7RUFDSSxrQkFBQTtBQUVKOztBQUFBO0VBQ0ksZ0JBQUE7QUFHSiIsImZpbGUiOiJzcmMvYXBwL2xvZ2luL2xvZ2luLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5lcnJvciB7XHJcbiAgICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLXllbGxvdyk7IC8vI2QzOWUwMDtcclxuICAgIGZvbnQtc2l6ZTogMTJweDtcclxufVxyXG5cclxuLmlubmVyLWRpdiB7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGUgIWltcG9ydGFudDtcclxuICAgIG1pbi1oZWlnaHQ6IDk1JSAhaW1wb3J0YW50O1xyXG4gICAgZGlzcGxheTogYmxvY2s7XHJcbn1cclxuXHJcbi5idG4taGVpZ2h0IHtcclxuICAgIGhlaWdodDogODUlO1xyXG4gICAgYm9yZGVyLXJhZGl1czogMTBweDtcclxufVxyXG4uY29sLXBhZGQge1xyXG4gICAgcGFkZGluZzogMTVweCAyNXB4O1xyXG59XHJcbi5tYXItdG9we1xyXG4gICAgbWFyZ2luLXRvcDogMTVweDtcclxufVxyXG4iXX0= */";
      /***/
    },

    /***/
    "./src/app/login/login.page.ts":
    /*!*************************************!*\
      !*** ./src/app/login/login.page.ts ***!
      \*************************************/

    /*! exports provided: LoginPage */

    /***/
    function srcAppLoginLoginPageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "LoginPage", function () {
        return LoginPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_native_ionic_webview_ngx__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @ionic-native/ionic-webview/ngx */
      "./node_modules/@ionic-native/ionic-webview/__ivy_ngcc__/ngx/index.js");
      /* harmony import */


      var _global_services_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ../global-services.service */
      "./src/app/global-services.service.ts");
      /* harmony import */


      var _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! @ionic-native/geolocation/ngx */
      "./node_modules/@ionic-native/geolocation/__ivy_ngcc__/ngx/index.js");
      /* harmony import */


      var _ionic_native_native_geocoder_ngx__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
      /*! @ionic-native/native-geocoder/ngx */
      "./node_modules/@ionic-native/native-geocoder/__ivy_ngcc__/ngx/index.js");

      var LoginPage = /*#__PURE__*/function () {
        function LoginPage(alertController, navCtrl, router, fb, webview, service, geolocation, nativeGeocoder, platform) {
          var _this = this;

          _classCallCheck(this, LoginPage);

          this.alertController = alertController;
          this.navCtrl = navCtrl;
          this.router = router;
          this.fb = fb;
          this.webview = webview;
          this.service = service;
          this.geolocation = geolocation;
          this.nativeGeocoder = nativeGeocoder;
          this.platform = platform;
          this.input_password = 'password'; // =====================Send OTP=====================

          this.sendOTP = function (mobileno) {
            var obj = {};
            obj.mobileno = mobileno;

            _this.service.post_data_otp("sendOtpViaSMS", obj).subscribe(function (result) {
              var json = _this.service.parserToJSON(result);

              var parsedJSON = JSON.parse(json.string);

              _this.navCtrl.navigateRoot(['/otp', {
                'otp': parsedJSON[0].OTP,
                'mobileno': _this.loggedDetails.mobileno,
                'data': _this.loggedDetails,
                'page': 'login'
              }]);
            }, function (error) {
              console.log(error);
            });
          }; // =====================HIDE PASSWORD=====================


          this.passwordType = 'password';
          this.passwordIcon = 'eye-off'; // geocoder options

          this.nativeGeocoderOptions = {
            useLocale: true,
            maxResults: 5
          };
        }

        _createClass(LoginPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            this.formInit();
            this.getUserPosition();
          } // =====================FORM INIT=====================

        }, {
          key: "formInit",
          value: function formInit() {
            this.signinFg = this.fb.group({
              username: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required],
              pwd: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required]
            });
          } // =====================SIGNIN PASSWORD=====================

        }, {
          key: "signinFunction",
          value: function signinFunction() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
              var _this2 = this;

              return regeneratorRuntime.wrap(function _callee$(_context) {
                while (1) {
                  switch (_context.prev = _context.next) {
                    case 0:
                      if (this.signinFg.valid) {
                        this.service.post_data("validateStudLogin", this.signinFg.value).subscribe(function (result) {
                          var json = _this2.service.parserToJSON(result);

                          _this2.verifyList = JSON.parse(json.string);

                          if (_this2.verifyList[0].Status == "FAIL") {
                            _this2.signupAlert(_this2.verifyList[0].Message);
                          } else {
                            _this2.loggedDetails = _this2.verifyList[0];

                            _this2.service.publishSomeData({
                              user: _this2.loggedDetails.usr_name
                            });

                            localStorage.setItem('username', _this2.loggedDetails.usr_name);
                            localStorage.setItem('mobileno', _this2.loggedDetails.mobileno);
                            localStorage.setItem('stud_type', _this2.loggedDetails.trendspoter_type);
                            localStorage.setItem('upload_image', _this2.loggedDetails.upload_image); // this.navCtrl.navigateRoot(['/otp', { 'otp': parsedJSON[0]['OTP'], 'mobileno': obj.mobileno, 'data': JSON.stringify(data), 'page': 'signup' }]);
                            // this.navCtrl.navigateRoot(['/home', { 'data': this.loggedDetails }]);

                            _this2.sendOTP(_this2.verifyList[0].mobileno);

                            _this2.service.callMenuFunction(_this2.city);
                          }
                        }, function (error) {
                          console.log(error);
                        });
                      } else {}

                    case 1:
                    case "end":
                      return _context.stop();
                  }
                }
              }, _callee, this);
            }));
          } // =====================PAGE FORWARD=====================

        }, {
          key: "signupAlert",
          value: function signupAlert(message_content) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
              var alert;
              return regeneratorRuntime.wrap(function _callee2$(_context2) {
                while (1) {
                  switch (_context2.prev = _context2.next) {
                    case 0:
                      _context2.next = 2;
                      return this.alertController.create({
                        cssClass: 'alert-custom-class',
                        message: message_content,
                        backdropDismiss: false,
                        buttons: [{
                          text: 'Ok',
                          cssClass: 'warning',
                          handler: function handler() {}
                        }]
                      });

                    case 2:
                      alert = _context2.sent;
                      _context2.next = 5;
                      return alert.present();

                    case 5:
                      this.signinFgReset();

                    case 6:
                    case "end":
                      return _context2.stop();
                  }
                }
              }, _callee2, this);
            }));
          }
        }, {
          key: "hideShowPassword",
          value: function hideShowPassword() {
            this.passwordType = this.passwordType === 'text' ? 'password' : 'text';
            this.passwordIcon = this.passwordIcon === 'eye-off' ? 'eye' : 'eye-off';
          }
        }, {
          key: "signinFgReset",
          value: function signinFgReset() {
            this.signinFg.reset();
          }
        }, {
          key: "getUserPosition",
          value: function getUserPosition() {
            var _this3 = this;

            this.options = {
              enableHighAccuracy: false
            };
            this.geolocation.getCurrentPosition(this.options).then(function (pos) {
              _this3.currentPos = pos;
              _this3.latitude = pos.coords.latitude;
              _this3.longitude = pos.coords.longitude;

              _this3.getAddress(_this3.latitude, _this3.longitude);
            }, function (err) {
              console.log("error : " + err.message);
              ;
            });
          } // get address using coordinates

        }, {
          key: "getAddress",
          value: function getAddress(lat, _long) {
            var _this4 = this;

            this.nativeGeocoder.reverseGeocode(lat, _long, this.nativeGeocoderOptions).then(function (res) {
              var address = _this4.pretifyAddress(res[0]);

              var value = address.split(",");
              var count = value.length; // country = value[count - 1];
              // state = value[count - 2];

              if (_this4.platform.is('ios')) {
                _this4.city = value[value.length - 2];
              } else {
                _this4.city = value[4];
              } // alert(address);
              // alert(this.city);

            })["catch"](function (error) {
              alert('Error getting location' + error + JSON.stringify(error));
            });
          } // address

        }, {
          key: "pretifyAddress",
          value: function pretifyAddress(addressObj) {
            var obj = [];
            var address = "";

            for (var key in addressObj) {
              obj.push(addressObj[key]);
            }

            obj.reverse();

            for (var val in obj) {
              if (obj[val].length) address += obj[val] + ', ';
            }

            return address.slice(0, -2);
          }
        }]);

        return LoginPage;
      }();

      LoginPage.ctorParameters = function () {
        return [{
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]
        }, {
          type: _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormBuilder"]
        }, {
          type: _ionic_native_ionic_webview_ngx__WEBPACK_IMPORTED_MODULE_5__["WebView"]
        }, {
          type: _global_services_service__WEBPACK_IMPORTED_MODULE_6__["GlobalServicesService"]
        }, {
          type: _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_7__["Geolocation"]
        }, {
          type: _ionic_native_native_geocoder_ngx__WEBPACK_IMPORTED_MODULE_8__["NativeGeocoder"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["Platform"]
        }];
      };

      LoginPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-login',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./login.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/login/login.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./login.page.scss */
        "./src/app/login/login.page.scss"))["default"]]
      })], LoginPage);
      /***/
    }
  }]);
})();
//# sourceMappingURL=login-login-module-es5.js.map