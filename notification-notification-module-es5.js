(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["notification-notification-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/notification/notification.page.html":
    /*!*******************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/notification/notification.page.html ***!
      \*******************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppNotificationNotificationPageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-header>\r\n  <ion-toolbar>\r\n    <ion-buttons slot=\"start\">\r\n      <ion-back-button defaultHref=\"/home\"></ion-back-button>\r\n    </ion-buttons>\r\n    <ion-title>notification</ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n  <div class=\"inner-div\">\r\n    <!-- <ion-grid>\r\n      <ion-row class=\"ion-justify-content-end\">\r\n        <ion-col *ngIf=\"totalListCount>notificationList.length\">\r\n          <div class=\"ion-text-end\">\r\n            <ion-button color=\"yellow\" size=\"small\" btn-yellow class=\"mt-10\" (click)=\"loadMore()\">Load\r\n              More &nbsp;<ion-icon name=\"arrow-down-outline\"></ion-icon>\r\n            </ion-button>\r\n          </div>\r\n        </ion-col>\r\n      </ion-row>\r\n    </ion-grid> -->\r\n    <ion-grid>\r\n      <ion-row class=\"ion-justify-content-center\" *ngFor=\"let notification of notificationList; let i=index\">\r\n        <ion-col>\r\n          <ion-card tappable (click)=\"get_read_notification(notification.msg_id,notification.read_sts,i)\"\r\n            [ngClass]=\"notification.read_sts=='True'? 'card-header-read':'card-header-unread'\">\r\n            <ion-card-header class=\"card-header-color\">\r\n              <ion-grid>\r\n                <ion-row>\r\n                  <ion-col size=\"11\">\r\n                    <ion-label class=\"card-header-text\"> <b>{{notification.msg_title}}</b></ion-label>\r\n                  </ion-col>\r\n                  <ion-col size=\"1\">\r\n                    <ion-icon [name]=\"isGroupShown(i) ? 'caret-down-outline' :  'caret-forward-outline'\">\r\n                    </ion-icon>\r\n                  </ion-col>\r\n                </ion-row>\r\n              </ion-grid>\r\n            </ion-card-header>\r\n            <ion-card-content class=\"card-content-color\" *ngIf=\"isGroupShown(i)\">\r\n              <ion-row>\r\n                <ion-col>\r\n                  <ion-label class=\"card-content-text\">{{notification.txt_msg}} </ion-label>\r\n                </ion-col>\r\n              </ion-row>\r\n              <ion-row class=\"ion-text-end\">\r\n                <ion-col>\r\n                  <ion-text class=\"card-content-date\" color=\"primary\">{{notification.creationdate}}</ion-text>\r\n                </ion-col>\r\n              </ion-row>\r\n            </ion-card-content>\r\n          </ion-card>\r\n        </ion-col>\r\n      </ion-row>\r\n    </ion-grid>\r\n    <ion-grid *ngIf=\"notificationList.length == 0\">\r\n      <ion-row>\r\n        <ion-col>\r\n          No Records Found!\r\n        </ion-col>\r\n      </ion-row>\r\n    </ion-grid>\r\n    <ion-grid *ngIf=\"loadMoreBtn\">\r\n      <ion-row class=\"ion-justify-content-center\">\r\n        <ion-col>\r\n          <div class=\"ion-text-center\">\r\n            <ion-button color=\"yellow btn-curve\" size=\"small\" btn-yellow class=\"mt-10\" (click)=\"loadMore()\">Load\r\n              More &nbsp;<ion-icon name=\"arrow-down-outline\"></ion-icon>\r\n            </ion-button>\r\n          </div>\r\n        </ion-col>\r\n      </ion-row>\r\n    </ion-grid>\r\n  </div>\r\n</ion-content>";
      /***/
    },

    /***/
    "./src/app/notification/notification-routing.module.ts":
    /*!*************************************************************!*\
      !*** ./src/app/notification/notification-routing.module.ts ***!
      \*************************************************************/

    /*! exports provided: NotificationPageRoutingModule */

    /***/
    function srcAppNotificationNotificationRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "NotificationPageRoutingModule", function () {
        return NotificationPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _notification_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./notification.page */
      "./src/app/notification/notification.page.ts");

      var routes = [{
        path: '',
        component: _notification_page__WEBPACK_IMPORTED_MODULE_3__["NotificationPage"]
      }];

      var NotificationPageRoutingModule = function NotificationPageRoutingModule() {
        _classCallCheck(this, NotificationPageRoutingModule);
      };

      NotificationPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], NotificationPageRoutingModule);
      /***/
    },

    /***/
    "./src/app/notification/notification.module.ts":
    /*!*****************************************************!*\
      !*** ./src/app/notification/notification.module.ts ***!
      \*****************************************************/

    /*! exports provided: NotificationPageModule */

    /***/
    function srcAppNotificationNotificationModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "NotificationPageModule", function () {
        return NotificationPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _notification_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./notification-routing.module */
      "./src/app/notification/notification-routing.module.ts");
      /* harmony import */


      var _notification_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./notification.page */
      "./src/app/notification/notification.page.ts");

      var NotificationPageModule = function NotificationPageModule() {
        _classCallCheck(this, NotificationPageModule);
      };

      NotificationPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _notification_routing_module__WEBPACK_IMPORTED_MODULE_5__["NotificationPageRoutingModule"]],
        declarations: [_notification_page__WEBPACK_IMPORTED_MODULE_6__["NotificationPage"]]
      })], NotificationPageModule);
      /***/
    },

    /***/
    "./src/app/notification/notification.page.scss":
    /*!*****************************************************!*\
      !*** ./src/app/notification/notification.page.scss ***!
      \*****************************************************/

    /*! exports provided: default */

    /***/
    function srcAppNotificationNotificationPageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = ".card-header-unread, .card-header-unread-ios {\n  background-color: #ffd700;\n  border-radius: 40px 0px 40px 0px;\n}\n\n.card-header-read, .card-header-read-ios {\n  background-color: beige;\n  border-radius: 40px 0px 40px 0px;\n}\n\n.card-content-color {\n  background-color: azure;\n}\n\n.card-header-text {\n  font-size: 14px;\n}\n\n.card-content-text {\n  font-size: 15px;\n}\n\n.card-content-date {\n  font-size: 11px;\n}\n\n.size {\n  width: 4rem;\n}\n\n.btn-curve {\n  border-radius: 10px;\n  --border-radius: 10px;\n  --ion-color-contrast: #212721;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbm90aWZpY2F0aW9uL25vdGlmaWNhdGlvbi5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRyx5QkFBQTtFQUNBLGdDQUFBO0FBQ0g7O0FBQ0E7RUFDSSx1QkFBQTtFQUNBLGdDQUFBO0FBRUo7O0FBQUE7RUFDSSx1QkFBQTtBQUdKOztBQURDO0VBQ0csZUFBQTtBQUlKOztBQUZDO0VBQ0csZUFBQTtBQUtKOztBQUhDO0VBQ0csZUFBQTtBQU1KOztBQUpDO0VBQ0EsV0FBQTtBQU9EOztBQUxBO0VBQ0csbUJBQUE7RUFDQSxxQkFBQTtFQUNBLDZCQUFBO0FBUUgiLCJmaWxlIjoic3JjL2FwcC9ub3RpZmljYXRpb24vbm90aWZpY2F0aW9uLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5jYXJkLWhlYWRlci11bnJlYWQsLmNhcmQtaGVhZGVyLXVucmVhZC1pb3N7XHJcbiAgIGJhY2tncm91bmQtY29sb3I6ICNmZmQ3MDA7XHJcbiAgIGJvcmRlci1yYWRpdXM6IDQwcHggMHB4IDQwcHggMHB4O1xyXG59XHJcbi5jYXJkLWhlYWRlci1yZWFkLC5jYXJkLWhlYWRlci1yZWFkLWlvc3tcclxuICAgIGJhY2tncm91bmQtY29sb3I6IGJlaWdlO1xyXG4gICAgYm9yZGVyLXJhZGl1czogNDBweCAwcHggNDBweCAwcHg7XHJcbiB9XHJcbi5jYXJkLWNvbnRlbnQtY29sb3J7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiBhenVyZTtcclxuIH1cclxuIC5jYXJkLWhlYWRlci10ZXh0e1xyXG4gICAgZm9udC1zaXplOiAxNHB4O1xyXG4gfVxyXG4gLmNhcmQtY29udGVudC10ZXh0e1xyXG4gICAgZm9udC1zaXplOiAxNXB4O1xyXG4gfVxyXG4gLmNhcmQtY29udGVudC1kYXRle1xyXG4gICAgZm9udC1zaXplOiAxMXB4O1xyXG4gfVxyXG4gLnNpemV7XHJcblx0d2lkdGg6NHJlbTtcclxufVxyXG4uYnRuLWN1cnZle1xyXG4gICBib3JkZXItcmFkaXVzOiAxMHB4O1xyXG4gICAtLWJvcmRlci1yYWRpdXM6IDEwcHg7XHJcbiAgIC0taW9uLWNvbG9yLWNvbnRyYXN0OiAjMjEyNzIxO1xyXG59Il19 */";
      /***/
    },

    /***/
    "./src/app/notification/notification.page.ts":
    /*!***************************************************!*\
      !*** ./src/app/notification/notification.page.ts ***!
      \***************************************************/

    /*! exports provided: NotificationPage */

    /***/
    function srcAppNotificationNotificationPageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "NotificationPage", function () {
        return NotificationPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _global_services_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ../global-services.service */
      "./src/app/global-services.service.ts");

      var NotificationPage = /*#__PURE__*/function () {
        function NotificationPage(service) {
          _classCallCheck(this, NotificationPage);

          this.service = service;
          this.notificationUrl = 'getNotificationText';
          this.notificationStatusUrl = 'setNotificationTextAsRead';
          this.notificationList = [];
          this.shownGroup = null;
          this.page = 0;
          this.loadMoreBtn = false;
          this.userName = localStorage.getItem('username');
        }

        _createClass(NotificationPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            this.loadMore();
          }
        }, {
          key: "getnotificationsList",
          value: function getnotificationsList() {
            var _this = this;

            var obj = {
              msg_id: 0,
              username: this.userName,
              rows_in_page: 10,
              pageid: this.page
            };
            this.service.post_data(this.notificationUrl, obj).subscribe(function (result) {
              var json = _this.service.parserToJSON(result);

              var list = JSON.parse(json.string);

              if (list) {
                _this.totalListCount = list.Total_Records;

                if (_this.totalListCount >= _this.notificationList.length) {
                  _this.notificationList = _this.notificationList.concat(list.Details);
                  setTimeout(function () {
                    if (_this.totalListCount > _this.notificationList.length) {
                      _this.loadMoreBtn = true;
                    } else {
                      _this.loadMoreBtn = false;
                    }
                  }, 500);
                }
              }
            });
          }
        }, {
          key: "ionViewDidLoad",
          value: function ionViewDidLoad() {}
        }, {
          key: "get_read_notification",
          value: function get_read_notification(id, status, group) {
            var _this2 = this;

            if (this.isGroupShown(group)) {
              this.shownGroup = null;
            } else {
              this.shownGroup = group;
            }

            if (status == 'False') {
              var obj = {
                msg_id: id,
                username: this.userName
              };
              this.service.post_data(this.notificationStatusUrl, obj).subscribe(function (res) {
                var json = _this2.service.parserToJSON(res);

                var response = JSON.parse(json.string)[0];

                if (response.Status == 'SUCCESS') {
                  _this2.notificationList.forEach(function (element, i) {
                    if (i == group) {
                      element.read_sts = 'True';
                    }
                  });
                }
              }, function (error) {
                console.log(error);
              });
            }
          }
        }, {
          key: "isGroupShown",
          value: function isGroupShown(group) {
            return this.shownGroup === group;
          }
        }, {
          key: "loadMore",
          value: function loadMore() {
            this.page = this.page + 1;
            this.loadMoreBtn = false;
            this.getnotificationsList();
          }
        }]);

        return NotificationPage;
      }();

      NotificationPage.ctorParameters = function () {
        return [{
          type: _global_services_service__WEBPACK_IMPORTED_MODULE_2__["GlobalServicesService"]
        }];
      };

      NotificationPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-notification',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./notification.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/notification/notification.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./notification.page.scss */
        "./src/app/notification/notification.page.scss"))["default"]]
      })], NotificationPage);
      /***/
    }
  }]);
})();
//# sourceMappingURL=notification-notification-module-es5.js.map