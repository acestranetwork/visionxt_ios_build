(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["uploadedvideos-uploadedvideos-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/uploadedvideos/uploadedvideos.page.html":
    /*!***********************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/uploadedvideos/uploadedvideos.page.html ***!
      \***********************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppUploadedvideosUploadedvideosPageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<app-header>\r\n\r\n</app-header>\r\n<ion-content>\r\n    <div class=\"inner-div\">\r\n        <ion-grid>\r\n            <ion-row class=\"ion-text-center mt-3 mb-2\">\r\n                <ion-col>\r\n                    <h5 header-text>Uploaded Videos</h5>\r\n                </ion-col>\r\n            </ion-row>\r\n            <ion-row *ngIf=\"isEnableSearch ==  true\" search-box>\r\n                <ion-col>\r\n                    <div class=\"text-center\">\r\n                        <ion-icon color=\"light\" size=\"large\" (click)=\"enableSearch(false)\" name=\"close-circle-outline\">\r\n                        </ion-icon>\r\n                    </div>\r\n                    <!-- <ion-searchbar color=\"light\" showCancelButton=\"focus\" [(ngModel)]='selectedItem'\r\n                        (ionChange)=\"onUploadedVideos()\" type=\"text\" debounce=\"500\" (ionChange)=\"getItems($event)\">\r\n                    </ion-searchbar> -->\r\n                    <ion-searchbar color=\"light\" showCancelButton=\"focus\" [(ngModel)]='search' cancelButtonText=\"Cancel\"\r\n                        type=\"text\" debounce=\"500\" (ionChange)=\"searchList()\">\r\n                    </ion-searchbar>\r\n                    <ion-list *ngIf=\"isItemAvailable\">\r\n                        <ion-item (click)=\"ItemAvailableClose(item)\" *ngFor=\"let item of items\">{{ item }}</ion-item>\r\n                    </ion-list>\r\n                </ion-col>\r\n            </ion-row>\r\n            <ion-row *ngIf=\"UploadedVideosLists.length == 0\">\r\n                <ion-col>\r\n                    <ion-card class=\"ion-margin-vertical\">\r\n                        <ion-card-content>\r\n                            No Record Found\r\n                        </ion-card-content>\r\n                    </ion-card>\r\n                </ion-col>\r\n            </ion-row>\r\n            <ion-row *ngIf=\"UploadedVideosLists.length > 0\">\r\n                <ion-col size=\"6\" (click)=\"gotoInnerPage(post)\" size=6 *ngFor=\"let post of UploadedVideosLists\"\r\n                    ion-col-height>\r\n                    <ion-content>\r\n                        <img [src]=\"post.upload_thump\" crossorigin class=\"video_tag\">\r\n                        <!-- <ion-item class=\"fs-12 black-bg\" black-bg fill=\"clear\">\r\n                            <ion-icon name=\"play-circle-outline\"></ion-icon> <span> &nbsp; Play</span>\r\n                        </ion-item> -->\r\n                        <!-- <ion-icon name=\"play-circle-outline\" class=\"videoicon\"></ion-icon> -->\r\n                        <div class=\"overlay\">\r\n                            <ion-icon name=\"play-circle-outline\" class=\"videoicon\"></ion-icon>\r\n                        </div>\r\n                    </ion-content>\r\n                    <!-- <ion-content>\r\n                        <video poster=\"\" preload=\"auto\" crossorigin class=\"video_tag\">\r\n                            <source [src]=\"post.upload_image\" type=\"\">\r\n                        </video>\r\n                        <ion-item class=\"fs-12 black-bg\" black-bg fill=\"clear\" (click)=\"play(post.upload_image);$event.stopPropagation()\">\r\n                            <ion-icon name=\"play-circle-outline\"></ion-icon> <span> &nbsp; Play</span>\r\n                        </ion-item>\r\n                    </ion-content> -->\r\n                </ion-col>\r\n            </ion-row>\r\n        </ion-grid>\r\n        <!-- <ion-infinite-scroll threshold=\"100px\" (ionInfinite)=\"loadData($event)\">\r\n            <ion-infinite-scroll-content class=\"loadingspinner\" loadingSpinner=\"bubbles\" color=\"yellow\"\r\n                loadingText=\"Loading more data...\">\r\n            </ion-infinite-scroll-content>\r\n        </ion-infinite-scroll> -->\r\n        <ion-grid *ngIf=\"loadMoreBtn\">\r\n            <ion-row class=\"ion-justify-content-center\">\r\n                <ion-col>\r\n                    <div class=\"ion-text-center\">\r\n                        <ion-button color=\"yellow btn-curve\" size=\"small\" btn-yellow class=\"mt-10\" (click)=\"loadMore()\">\r\n                            Load\r\n                            More &nbsp;<ion-icon name=\"arrow-down-outline\"></ion-icon>\r\n                        </ion-button>\r\n                    </div>\r\n                </ion-col>\r\n            </ion-row>\r\n        </ion-grid>\r\n    </div>\r\n\r\n</ion-content>\r\n<ion-toolbar *ngIf=\"showToolbar == true\" color=\"yellow\" slot=\"bottom\">\r\n    <ion-tabs>\r\n        <ion-tab-bar color=\"yellow\" slot=\"bottom\">\r\n            <ion-tab-button [routerLink]=\"['/home']\">\r\n                <ion-icon name=\"caret-back-outline\"></ion-icon>\r\n                <ion-label>Back</ion-label>\r\n            </ion-tab-button>\r\n            <ion-tab-button (click)=\"enableSearch(true)\">\r\n                <ion-icon name=\"search-outline\"></ion-icon>\r\n                <ion-label>Search</ion-label>\r\n            </ion-tab-button>\r\n        </ion-tab-bar>\r\n    </ion-tabs>\r\n</ion-toolbar>";
      /***/
    },

    /***/
    "./src/app/uploadedvideos/uploadedvideos-routing.module.ts":
    /*!*****************************************************************!*\
      !*** ./src/app/uploadedvideos/uploadedvideos-routing.module.ts ***!
      \*****************************************************************/

    /*! exports provided: UploadedvideosPageRoutingModule */

    /***/
    function srcAppUploadedvideosUploadedvideosRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "UploadedvideosPageRoutingModule", function () {
        return UploadedvideosPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _uploadedvideos_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./uploadedvideos.page */
      "./src/app/uploadedvideos/uploadedvideos.page.ts");

      var routes = [{
        path: '',
        component: _uploadedvideos_page__WEBPACK_IMPORTED_MODULE_3__["UploadedvideosPage"]
      }];

      var UploadedvideosPageRoutingModule = function UploadedvideosPageRoutingModule() {
        _classCallCheck(this, UploadedvideosPageRoutingModule);
      };

      UploadedvideosPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], UploadedvideosPageRoutingModule);
      /***/
    },

    /***/
    "./src/app/uploadedvideos/uploadedvideos.module.ts":
    /*!*********************************************************!*\
      !*** ./src/app/uploadedvideos/uploadedvideos.module.ts ***!
      \*********************************************************/

    /*! exports provided: UploadedvideosPageModule */

    /***/
    function srcAppUploadedvideosUploadedvideosModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "UploadedvideosPageModule", function () {
        return UploadedvideosPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _uploadedvideos_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./uploadedvideos-routing.module */
      "./src/app/uploadedvideos/uploadedvideos-routing.module.ts");
      /* harmony import */


      var _uploadedvideos_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./uploadedvideos.page */
      "./src/app/uploadedvideos/uploadedvideos.page.ts");
      /* harmony import */


      var _header_header_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! ../header/header.module */
      "./src/app/header/header.module.ts");

      var UploadedvideosPageModule = function UploadedvideosPageModule() {
        _classCallCheck(this, UploadedvideosPageModule);
      };

      UploadedvideosPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _uploadedvideos_routing_module__WEBPACK_IMPORTED_MODULE_5__["UploadedvideosPageRoutingModule"], _header_header_module__WEBPACK_IMPORTED_MODULE_7__["HeaderPageModule"]],
        declarations: [_uploadedvideos_page__WEBPACK_IMPORTED_MODULE_6__["UploadedvideosPage"]]
      })], UploadedvideosPageModule);
      /***/
    },

    /***/
    "./src/app/uploadedvideos/uploadedvideos.page.scss":
    /*!*********************************************************!*\
      !*** ./src/app/uploadedvideos/uploadedvideos.page.scss ***!
      \*********************************************************/

    /*! exports provided: default */

    /***/
    function srcAppUploadedvideosUploadedvideosPageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = ".video_tag {\n  width: 100%;\n  height: 100%;\n  -o-object-fit: cover;\n     object-fit: cover;\n  overflow: hidden;\n  position: absolute;\n  z-index: 0;\n}\n\n.overlay {\n  position: absolute;\n  bottom: 0;\n  left: 0;\n  z-index: 1;\n  height: 20%;\n}\n\n.overlay p {\n  margin-top: 0;\n  margin-bottom: 1rem;\n  background: var(--ion-color-yellow);\n  padding: 10px;\n  position: absolute;\n  bottom: 0;\n  text-overflow: ellipsis;\n  /* width: 128%; */\n}\n\n[ion-col-height] {\n  width: 100%;\n  height: 150px;\n}\n\n[ion-col-height] ion-content {\n  --background: #212721;\n}\n\n[ion-col-height] ion-content ion-button {\n  font-size: 30px;\n  position: absolute;\n  left: 50%;\n  top: 50%;\n  transform: translate(-50%, -50%);\n}\n\n[font-small] {\n  font-weight: bold !important;\n  --border-radius: 0px;\n}\n\n[margin-1] {\n  margin: 1.5rem;\n}\n\n.icon {\n  display: flex;\n  font-size: 45px;\n  color: var(--ion-color-yellow);\n  vertical-align: middle;\n}\n\n.video-card {\n  background: var(--ion-color-yellow);\n  height: 66%;\n  margin: 0;\n  padding: 10px;\n  overflow: hidden;\n}\n\n.video-card p {\n  height: 35px;\n  overflow: hidden;\n  display: -webkit-box;\n  -webkit-line-clamp: 2;\n  -webkit-box-orient: vertical;\n  color: #222428;\n  font-size: small;\n}\n\n.fs-12 {\n  font-size: 11px;\n}\n\n[search-box] {\n  background: #ffffff1f;\n  padding: 10px;\n  margin: 5px;\n  border: 1px solid #ffffff3b;\n}\n\n[search-box] ion-list {\n  margin: -5px 7px;\n  width: 94%;\n  padding: 0;\n}\n\n.videoicon {\n  color: white;\n  font-size: 60px;\n  position: absolute;\n  top: 50%;\n  left: 50%;\n  transform: translate(-50%, -50%);\n  -ms-transform: translate(-50%, -50%);\n  text-align: center;\n}\n\n.overlay {\n  position: absolute;\n  top: 0;\n  bottom: 0;\n  left: 0;\n  right: 0;\n  height: 100%;\n  width: 100%;\n  opacity: 0;\n  transition: 0.3s ease;\n  opacity: 1;\n}\n\n.btn-curve {\n  border-radius: 10px;\n  --border-radius: 10px;\n  --ion-color-contrast: #212721;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvdXBsb2FkZWR2aWRlb3MvdXBsb2FkZWR2aWRlb3MucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUNBO0VBQ0ksV0FBQTtFQUNBLFlBQUE7RUFDQSxvQkFBQTtLQUFBLGlCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxrQkFBQTtFQUNBLFVBQUE7QUFBSjs7QUFHQTtFQUNJLGtCQUFBO0VBQ0EsU0FBQTtFQUNBLE9BQUE7RUFDQSxVQUFBO0VBQ0EsV0FBQTtBQUFKOztBQUNJO0VBQ0ksYUFBQTtFQUNBLG1CQUFBO0VBQ0EsbUNBQUE7RUFDQSxhQUFBO0VBQ0Esa0JBQUE7RUFDQSxTQUFBO0VBQ0EsdUJBQUE7RUFDQSxpQkFBQTtBQUNSOztBQUlBO0VBQ0ksV0FBQTtFQUNBLGFBQUE7QUFESjs7QUFFSTtFQUNJLHFCQUFBO0FBQVI7O0FBQ1E7RUFDSSxlQUFBO0VBQ0Esa0JBQUE7RUFDQSxTQUFBO0VBQ0EsUUFBQTtFQUNBLGdDQUFBO0FBQ1o7O0FBSUE7RUFFSSw0QkFBQTtFQUNBLG9CQUFBO0FBRko7O0FBS0E7RUFDSSxjQUFBO0FBRko7O0FBS0E7RUFDSSxhQUFBO0VBQ0EsZUFBQTtFQUNBLDhCQUFBO0VBQ0Esc0JBQUE7QUFGSjs7QUFLQTtFQUNJLG1DQUFBO0VBQ0EsV0FBQTtFQUNBLFNBQUE7RUFDQSxhQUFBO0VBQ0EsZ0JBQUE7QUFGSjs7QUFHSTtFQUNJLFlBQUE7RUFDQSxnQkFBQTtFQUNBLG9CQUFBO0VBQ0EscUJBQUE7RUFDQSw0QkFBQTtFQUNBLGNBQUE7RUFDQSxnQkFBQTtBQURSOztBQUtBO0VBQ0EsZUFBQTtBQUZBOztBQUtBO0VBQ0kscUJBQUE7RUFDQSxhQUFBO0VBQ0EsV0FBQTtFQUNBLDJCQUFBO0FBRko7O0FBR0k7RUFDQSxnQkFBQTtFQUNBLFVBQUE7RUFDQSxVQUFBO0FBREo7O0FBV0E7RUFLSSxZQUFBO0VBQ0EsZUFBQTtFQUNBLGtCQUFBO0VBQ0EsUUFBQTtFQUNBLFNBQUE7RUFDQSxnQ0FBQTtFQUNBLG9DQUFBO0VBQ0Esa0JBQUE7QUFaSjs7QUFjSTtFQUVBLGtCQUFBO0VBQ0EsTUFBQTtFQUNBLFNBQUE7RUFDQSxPQUFBO0VBQ0EsUUFBQTtFQUNBLFlBQUE7RUFDQSxXQUFBO0VBQ0EsVUFBQTtFQUNBLHFCQUFBO0VBQ0EsVUFBQTtBQVpKOztBQWNJO0VBQ0ksbUJBQUE7RUFDQSxxQkFBQTtFQUNBLDZCQUFBO0FBWFIiLCJmaWxlIjoic3JjL2FwcC91cGxvYWRlZHZpZGVvcy91cGxvYWRlZHZpZGVvcy5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJcclxuLnZpZGVvX3RhZyB7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGhlaWdodDogMTAwJTtcclxuICAgIG9iamVjdC1maXQ6IGNvdmVyO1xyXG4gICAgb3ZlcmZsb3c6IGhpZGRlbjtcclxuICAgIHBvc2l0aW9uOmFic29sdXRlO1xyXG4gICAgei1pbmRleDowO1xyXG4gICAgLy8gYm9yZGVyLXJhZGl1czogMTBweDtcclxufVxyXG4ub3ZlcmxheSB7XHJcbiAgICBwb3NpdGlvbjphYnNvbHV0ZTtcclxuICAgIGJvdHRvbTogMDtcclxuICAgIGxlZnQ6IDA7XHJcbiAgICB6LWluZGV4OjE7XHJcbiAgICBoZWlnaHQ6IDIwJTtcclxuICAgIHB7XHJcbiAgICAgICAgbWFyZ2luLXRvcDogMDtcclxuICAgICAgICBtYXJnaW4tYm90dG9tOiAxcmVtO1xyXG4gICAgICAgIGJhY2tncm91bmQ6IHZhcigtLWlvbi1jb2xvci15ZWxsb3cpOy8vI2Y2Yzc2MjtcclxuICAgICAgICBwYWRkaW5nOiAxMHB4O1xyXG4gICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgICAgICBib3R0b206IDA7XHJcbiAgICAgICAgdGV4dC1vdmVyZmxvdzogZWxsaXBzaXM7XHJcbiAgICAgICAgLyogd2lkdGg6IDEyOCU7ICovXHJcbiAgICB9XHJcbn1cclxuXHJcblxyXG5baW9uLWNvbC1oZWlnaHRdIHtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgaGVpZ2h0OiAxNTBweDtcclxuICAgIGlvbi1jb250ZW50IHtcclxuICAgICAgICAtLWJhY2tncm91bmQ6ICMyMTI3MjE7XHJcbiAgICAgICAgaW9uLWJ1dHRvbiB7XHJcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMzBweDtcclxuICAgICAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICAgICAgICBsZWZ0OiA1MCU7XHJcbiAgICAgICAgICAgIHRvcDogNTAlO1xyXG4gICAgICAgICAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZSgtNTAlLCAtNTAlKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuXHJcbltmb250LXNtYWxsXSB7XHJcbiAgICAvLyBmb250LXNpemU6IDEycHghaW1wb3J0YW50O1xyXG4gICAgZm9udC13ZWlnaHQ6IGJvbGQgIWltcG9ydGFudDtcclxuICAgIC0tYm9yZGVyLXJhZGl1czogMHB4O1xyXG59XHJcblxyXG5bbWFyZ2luLTFdIHtcclxuICAgIG1hcmdpbjogMS41cmVtO1xyXG59XHJcblxyXG4uaWNvbiB7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgZm9udC1zaXplOiA0NXB4O1xyXG4gICAgY29sb3I6IHZhcigtLWlvbi1jb2xvci15ZWxsb3cpOy8vI2ZmQzk3NztcclxuICAgIHZlcnRpY2FsLWFsaWduOiBtaWRkbGU7XHJcbn1cclxuXHJcbi52aWRlby1jYXJke1xyXG4gICAgYmFja2dyb3VuZDogdmFyKC0taW9uLWNvbG9yLXllbGxvdyk7Ly8jZjZjNzYyO1xyXG4gICAgaGVpZ2h0OiA2NiU7XHJcbiAgICBtYXJnaW46IDA7XHJcbiAgICBwYWRkaW5nOiAxMHB4O1xyXG4gICAgb3ZlcmZsb3c6IGhpZGRlbjtcclxuICAgIHB7XHJcbiAgICAgICAgaGVpZ2h0OiAzNXB4O1xyXG4gICAgICAgIG92ZXJmbG93OiBoaWRkZW47XHJcbiAgICAgICAgZGlzcGxheTogLXdlYmtpdC1ib3g7XHJcbiAgICAgICAgLXdlYmtpdC1saW5lLWNsYW1wOiAyO1xyXG4gICAgICAgIC13ZWJraXQtYm94LW9yaWVudDogdmVydGljYWw7XHJcbiAgICAgICAgY29sb3I6ICMyMjI0Mjg7XHJcbiAgICAgICAgZm9udC1zaXplOiBzbWFsbDtcclxuICAgIH1cclxufVxyXG5cclxuLmZzLTEye1xyXG5mb250LXNpemU6IDExcHg7XHJcbn1cclxuXHJcbltzZWFyY2gtYm94XXtcclxuICAgIGJhY2tncm91bmQ6ICNmZmZmZmYxZjtcclxuICAgIHBhZGRpbmc6IDEwcHg7XHJcbiAgICBtYXJnaW46IDVweDtcclxuICAgIGJvcmRlcjogMXB4IHNvbGlkICNmZmZmZmYzYjtcclxuICAgIGlvbi1saXN0e1xyXG4gICAgbWFyZ2luOiAtNXB4IDdweDtcclxuICAgIHdpZHRoOiA5NCU7XHJcbiAgICBwYWRkaW5nOiAwO1xyXG4gICAgfVxyXG59XHJcblxyXG4vLyAudmlkZW9pY29ue1xyXG4vLyAgICAgZm9udC1zaXplOiA2MHB4O1xyXG4vLyAgICAgY29sb3I6IHdoaXRlO1xyXG4vLyAgICAgbWFyZ2luOiA0NXB4IDY1cHg7XHJcbi8vIH1cclxuXHJcbi52aWRlb2ljb25cclxue1xyXG4gICAgLy8gZm9udC1zaXplOiA2MHB4O1xyXG4gICAgLy8gY29sb3I6IHdoaXRlO1xyXG4gICAgLy8gbWFyZ2luOiAyNSUgMjUlO1xyXG4gICAgY29sb3I6IHdoaXRlO1xyXG4gICAgZm9udC1zaXplOiA2MHB4O1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgdG9wOiA1MCU7XHJcbiAgICBsZWZ0OiA1MCU7XHJcbiAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZSgtNTAlLCAtNTAlKTtcclxuICAgIC1tcy10cmFuc2Zvcm06IHRyYW5zbGF0ZSgtNTAlLCAtNTAlKTtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxufVxyXG4gICAgLm92ZXJsYXkgXHJcbiAgICB7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICB0b3A6IDA7XHJcbiAgICBib3R0b206IDA7XHJcbiAgICBsZWZ0OiAwO1xyXG4gICAgcmlnaHQ6IDA7XHJcbiAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIG9wYWNpdHk6IDA7XHJcbiAgICB0cmFuc2l0aW9uOiAuM3MgZWFzZTtcclxuICAgIG9wYWNpdHk6IDE7XHJcbiAgICB9XHJcbiAgICAuYnRuLWN1cnZle1xyXG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDEwcHg7XHJcbiAgICAgICAgLS1ib3JkZXItcmFkaXVzOiAxMHB4O1xyXG4gICAgICAgIC0taW9uLWNvbG9yLWNvbnRyYXN0OiAjMjEyNzIxO1xyXG4gICAgIH0iXX0= */";
      /***/
    },

    /***/
    "./src/app/uploadedvideos/uploadedvideos.page.ts":
    /*!*******************************************************!*\
      !*** ./src/app/uploadedvideos/uploadedvideos.page.ts ***!
      \*******************************************************/

    /*! exports provided: UploadedvideosPage */

    /***/
    function srcAppUploadedvideosUploadedvideosPageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "UploadedvideosPage", function () {
        return UploadedvideosPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _global_services_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ../global-services.service */
      "./src/app/global-services.service.ts");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _ionic_native_streaming_media_ngx__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic-native/streaming-media/ngx */
      "./node_modules/@ionic-native/streaming-media/__ivy_ngcc__/ngx/index.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _ionic_native_keyboard__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! @ionic-native/keyboard */
      "./node_modules/@ionic-native/keyboard/index.js");

      var UploadedvideosPage = /*#__PURE__*/function () {
        function UploadedvideosPage(streamingMedia, service, menu, navCtrl, router) {
          var _this = this;

          _classCallCheck(this, UploadedvideosPage);

          this.streamingMedia = streamingMedia;
          this.service = service;
          this.menu = menu;
          this.navCtrl = navCtrl;
          this.router = router; // =====================Hide Toolbar when keypad is open=====================

          this.showToolbar = true;
          this.UploadedVideosLists = [];
          this.page = 0;
          this.search = '';
          this.loadMoreBtn = false; ////=====================Video Player=====================

          this.videoUrl = this.service.STREAMING_VIDEO_URL; // ===================== UPLOADED VIDEO LIST =====================

          this.onUploadedVideos = function () {
            var username = localStorage.getItem('username');
            var obj = {
              'pageid': _this.page,
              'rows_in_page': 12,
              'username': username,
              'image_video': 'VIDEO',
              'image_category': _this.search
            }; // if (this.isEnableSearch == true) {
            //   obj.image_category = this.selectedItem;
            // }

            _this.service.post_data("getTrendSpotterImagesVideosWithPaging", obj).subscribe(function (result) {
              // let json: any = this.service.parserToJSON(result);
              var json = _this.service.parserToJSON(result);

              var jons1 = JSON.stringify(json);
              var json2 = jons1.replace(/\\n/g, '');
              var parsedJSON = JSON.parse(JSON.parse(json2).string);
              _this.totalRecordCount = parsedJSON.Total_Records;

              if (_this.totalRecordCount >= _this.UploadedVideosLists.length) {
                _this.UploadedVideosLists = _this.UploadedVideosLists.concat(parsedJSON.Details);
                setTimeout(function () {
                  if (_this.totalRecordCount > _this.UploadedVideosLists.length) {
                    _this.loadMoreBtn = true;
                  } else {
                    _this.loadMoreBtn = false;
                  }
                }, 500);
              }
            }, function (error) {
              console.log(error);
            });
          }; //=====================Search With Auto-Complete=====================
          // Declare the variable (in this case and initialize it with false)


          this.isItemAvailable = false;
          this.items = [];
        }

        _createClass(UploadedvideosPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            var _this2 = this;

            this.onUploadedVideos();

            _ionic_native_keyboard__WEBPACK_IMPORTED_MODULE_6__["Keyboard"].onKeyboardShow().subscribe(function () {
              _this2.showToolbar = false;
            });

            _ionic_native_keyboard__WEBPACK_IMPORTED_MODULE_6__["Keyboard"].onKeyboardHide().subscribe(function () {
              _this2.showToolbar = true;
            });

            this.loadMore();
          }
        }, {
          key: "ionViewWillEnter",
          value: function ionViewWillEnter() {
            this.service.sendMessage('startNotificationCountFunction');
          }
        }, {
          key: "play",
          value: function play() {
            var options = {
              successCallback: function successCallback() {},
              errorCallback: function errorCallback(e) {
                console.log('Error streaming');
              },
              orientation: 'portrait',
              shouldAutoClose: true,
              controls: true
            };
            this.streamingMedia.playVideo(this.videoUrl, options);
          } // loadData(event) {
          //   setTimeout(() => {
          //     this.itemsPerPage = this.itemsPerPage + 12;
          //     this.onUploadedVideos();
          //     event.target.complete();
          //     if (this.UploadedVideosLists.length == this.totalRecordCount) {
          //       event.target.disabled = true;
          //     }
          //   }, 700);
          // }

        }, {
          key: "loadMore",
          value: function loadMore() {
            this.page = this.page + 1;
            this.loadMoreBtn = false;
            this.onUploadedVideos();
          } //=====================Move to Inner Page of Selected Image=====================

        }, {
          key: "navigateMediaInfo",
          value: function navigateMediaInfo() {
            this.router.navigate(['/media-info', {
              'page': 'video'
            }]);
          } //=====================Open Menu=====================

        }, {
          key: "openMenu",
          value: function openMenu() {
            // this.menu.enable(true, 'first');
            this.menu.open();
          }
        }, {
          key: "gotoInnerPage",
          value: function gotoInnerPage(data) {
            this.router.navigate(['/media-info', {
              'data': JSON.stringify(data),
              'page': 'video'
            }]);
          }
        }, {
          key: "initializeItems",
          value: function initializeItems() {
            this.items = ["Campus ch1 | Chennai", "Campus ch2 | Chennai", "Campus bg1| Bangalore"];
          }
        }, {
          key: "getItems",
          value: function getItems(ev) {
            // Reset items back to all of the items
            this.initializeItems(); // set val to the value of the searchbar

            var val = ev.target.value; // if the value is an empty string don't filter the items

            if (val && val.trim() !== '') {
              this.isItemAvailable = true;
              this.items = this.items.filter(function (item) {
                return item.toLowerCase().indexOf(val.toLowerCase()) > -1;
              });
            } else {
              this.isItemAvailable = false;
            }
          }
        }, {
          key: "searchList",
          value: function searchList() {
            this.page = 1;
            this.UploadedVideosLists = [];
            this.onUploadedVideos();
          }
        }, {
          key: "enableSearch",
          value: function enableSearch(value) {
            this.isEnableSearch = value;

            if (!value && this.search) {
              // this.infiniteScroll.disabled = false;
              this.search = '';
              this.page = 1;
              this.UploadedVideosLists = [];
              this.onUploadedVideos();
            }

            this.Content.scrollToTop();
          }
        }, {
          key: "ItemAvailableClose",
          value: function ItemAvailableClose(selected) {
            this.isItemAvailable = false;
            this.selectedItem = selected;
          }
        }]);

        return UploadedvideosPage;
      }();

      UploadedvideosPage.ctorParameters = function () {
        return [{
          type: _ionic_native_streaming_media_ngx__WEBPACK_IMPORTED_MODULE_4__["StreamingMedia"]
        }, {
          type: _global_services_service__WEBPACK_IMPORTED_MODULE_2__["GlobalServicesService"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["MenuController"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavController"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"]
        }];
      };

      UploadedvideosPage.propDecorators = {
        infiniteScroll: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"],
          args: [_ionic_angular__WEBPACK_IMPORTED_MODULE_3__["IonInfiniteScroll"]]
        }],
        Content: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"],
          args: [_ionic_angular__WEBPACK_IMPORTED_MODULE_3__["IonContent"]]
        }]
      };
      UploadedvideosPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-uploadedvideos',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./uploadedvideos.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/uploadedvideos/uploadedvideos.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./uploadedvideos.page.scss */
        "./src/app/uploadedvideos/uploadedvideos.page.scss"))["default"]]
      })], UploadedvideosPage);
      /***/
    }
  }]);
})();
//# sourceMappingURL=uploadedvideos-uploadedvideos-module-es5.js.map