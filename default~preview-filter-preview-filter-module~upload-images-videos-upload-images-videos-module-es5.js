(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["default~preview-filter-preview-filter-module~upload-images-videos-upload-images-videos-module"], {
    /***/
    "./node_modules/@ionic-native/video-editor/index.js":
    /*!**********************************************************!*\
      !*** ./node_modules/@ionic-native/video-editor/index.js ***!
      \**********************************************************/

    /*! exports provided: VideoEditor */

    /***/
    function node_modulesIonicNativeVideoEditorIndexJs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "VideoEditor", function () {
        return VideoEditor;
      });
      /* harmony import */


      var _ionic_native_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @ionic-native/core */
      "./node_modules/@ionic-native/core/__ivy_ngcc__/index.js");

      var __extends = undefined && undefined.__extends || function () {
        var _extendStatics = function extendStatics(d, b) {
          _extendStatics = Object.setPrototypeOf || {
            __proto__: []
          } instanceof Array && function (d, b) {
            d.__proto__ = b;
          } || function (d, b) {
            for (var p in b) {
              if (b.hasOwnProperty(p)) d[p] = b[p];
            }
          };

          return _extendStatics(d, b);
        };

        return function (d, b) {
          _extendStatics(d, b);

          function __() {
            this.constructor = d;
          }

          d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
        };
      }();

      var VideoEditorOriginal =
      /** @class */
      function (_super) {
        __extends(VideoEditorOriginal, _super);

        function VideoEditorOriginal() {
          var _this = _super !== null && _super.apply(this, arguments) || this;

          _this.OptimizeForNetworkUse = {
            NO: 0,
            YES: 1
          };
          _this.OutputFileType = {
            M4V: 0,
            MPEG4: 1,
            M4A: 2,
            QUICK_TIME: 3
          };
          return _this;
        }

        VideoEditorOriginal.prototype.transcodeVideo = function (options) {
          return Object(_ionic_native_core__WEBPACK_IMPORTED_MODULE_0__["cordova"])(this, "transcodeVideo", {
            "callbackOrder": "reverse"
          }, arguments);
        };

        VideoEditorOriginal.prototype.trim = function (options) {
          return Object(_ionic_native_core__WEBPACK_IMPORTED_MODULE_0__["cordova"])(this, "trim", {
            "callbackOrder": "reverse",
            "platforms": ["iOS"]
          }, arguments);
        };

        VideoEditorOriginal.prototype.createThumbnail = function (options) {
          return Object(_ionic_native_core__WEBPACK_IMPORTED_MODULE_0__["cordova"])(this, "createThumbnail", {
            "callbackOrder": "reverse"
          }, arguments);
        };

        VideoEditorOriginal.prototype.getVideoInfo = function (options) {
          return Object(_ionic_native_core__WEBPACK_IMPORTED_MODULE_0__["cordova"])(this, "getVideoInfo", {
            "callbackOrder": "reverse"
          }, arguments);
        };

        VideoEditorOriginal.pluginName = "VideoEditor";
        VideoEditorOriginal.plugin = "cordova-plugin-video-editor";
        VideoEditorOriginal.pluginRef = "VideoEditor";
        VideoEditorOriginal.repo = "https://github.com/jbavari/cordova-plugin-video-editor";
        VideoEditorOriginal.platforms = ["Android", "iOS", "Windows", "Windows Phone 8"];
        return VideoEditorOriginal;
      }(_ionic_native_core__WEBPACK_IMPORTED_MODULE_0__["IonicNativePlugin"]);

      var VideoEditor = new VideoEditorOriginal(); //# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi8uLi9zcmMvQGlvbmljLW5hdGl2ZS9wbHVnaW5zL3ZpZGVvLWVkaXRvci9pbmRleC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7O0FBQ0EsT0FBTyw4QkFBc0MsTUFBTSxvQkFBb0IsQ0FBQzs7SUFtSnZDLCtCQUFpQjs7O1FBQ2hELDJCQUFxQixHQUFHO1lBQ3RCLEVBQUUsRUFBRSxDQUFDO1lBQ0wsR0FBRyxFQUFFLENBQUM7U0FDUCxDQUFDO1FBRUYsb0JBQWMsR0FBRztZQUNmLEdBQUcsRUFBRSxDQUFDO1lBQ04sS0FBSyxFQUFFLENBQUM7WUFDUixHQUFHLEVBQUUsQ0FBQztZQUNOLFVBQVUsRUFBRSxDQUFDO1NBQ2QsQ0FBQzs7O0lBVUYsb0NBQWMsYUFBQyxPQUF5QjtJQWF4QywwQkFBSSxhQUFDLE9BQW9CO0lBWXpCLHFDQUFlLGFBQUMsT0FBK0I7SUFZL0Msa0NBQVksYUFBQyxPQUE0Qjs7Ozs7O3NCQTlNM0M7RUFvSmlDLGlCQUFpQjtTQUFyQyxXQUFXIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgQ29yZG92YSwgSW9uaWNOYXRpdmVQbHVnaW4sIFBsdWdpbiB9IGZyb20gJ0Bpb25pYy1uYXRpdmUvY29yZSc7XG5cbmV4cG9ydCBpbnRlcmZhY2UgVHJhbnNjb2RlT3B0aW9ucyB7XG4gIC8qKiBUaGUgcGF0aCB0byB0aGUgdmlkZW8gb24gdGhlIGRldmljZS4gKi9cbiAgZmlsZVVyaTogc3RyaW5nO1xuXG4gIC8qKiBUaGUgZmlsZSBuYW1lIGZvciB0aGUgdHJhbnNjb2RlZCB2aWRlbyAqL1xuICBvdXRwdXRGaWxlTmFtZTogc3RyaW5nO1xuXG4gIC8qKiBJbnN0cnVjdGlvbnMgb24gaG93IHRvIGVuY29kZSB0aGUgdmlkZW8uIEFuZHJvaWQgaXMgYWx3YXlzIG1wNCAqL1xuICBvdXRwdXRGaWxlVHlwZT86IG51bWJlcjtcblxuICAvKiogU2hvdWxkIHRoZSB2aWRlbyBiZSBwcm9jZXNzZWQgd2l0aCBxdWFpbHR5IG9yIHNwZWVkIGluIG1pbmQuIGlPUyBvbmx5ICovXG4gIG9wdGltaXplRm9yTmV0d29ya1VzZT86IG51bWJlcjtcblxuICAvKiogU2F2ZSB0aGUgbmV3IHZpZGVvIHRoZSBsaWJyYXJ5LiBOb3Qgc3VwcG9ydGVkIGluIHdpbmRvd3MuIERlZmF1bHRzIHRvIHRydWUgKi9cbiAgc2F2ZVRvTGlicmFyeT86IGJvb2xlYW47XG5cbiAgLyoqIERlbGV0ZSB0aGUgb3JpZ2luYWwgdmlkZW8uIEFuZHJvaWQgb25seS4gRGVmYXVsdHMgdG8gZmFsc2UgKi9cbiAgZGVsZXRlSW5wdXRGaWxlPzogYm9vbGVhbjtcblxuICAvKiogaU9TIG9ubHkuIERlZmF1bHRzIHRvIHRydWUgKi9cbiAgbWFpbnRhaW5Bc3BlY3RSYXRpbz86IGJvb2xlYW47XG5cbiAgLyoqIFdpZHRoIG9mIHRoZSByZXN1bHQgKi9cbiAgd2lkdGg/OiBudW1iZXI7XG5cbiAgLyoqIEhlaWdodCBvZiB0aGUgcmVzdWx0ICovXG4gIGhlaWdodD86IG51bWJlcjtcblxuICAvKiogQml0cmF0ZSBpbiBiaXRzLiBEZWZhdWx0cyB0byAxIG1lZ2FiaXQgKDEwMDAwMDApLiAqL1xuICB2aWRlb0JpdHJhdGU/OiBudW1iZXI7XG5cbiAgLyoqIEZyYW1lcyBwZXIgc2Vjb25kIG9mIHRoZSByZXN1bHQuIEFuZHJvaWQgb25seS4gRGVmYXVsdHMgdG8gMjQuICovXG4gIGZwcz86IG51bWJlcjtcblxuICAvKiogTnVtYmVyIG9mIGF1ZGlvIGNoYW5uZWxzLiBpT1Mgb25seS4gRGVmYXVsdHMgdG8gMi4gKi9cbiAgYXVkaW9DaGFubmVscz86IG51bWJlcjtcblxuICAvKiBTYW1wbGUgcmF0ZSBmb3IgdGhlIGF1ZGlvLiBpT1Mgb25seS4gRGVmYXVsdHMgdG8gNDQxMDAqL1xuICBhdWRpb1NhbXBsZVJhdGU/OiBudW1iZXI7XG5cbiAgLyoqIFNhbXBsZSByYXRlIGZvciB0aGUgYXVkaW8uIGlPUyBvbmx5LiBEZWZhdWx0cyB0byAxMjgga2lsb2JpdHMgKDEyODAwMCkuICovXG4gIGF1ZGlvQml0cmF0ZT86IG51bWJlcjtcblxuICAvKiogTm90IHN1cHBvcnRlZCBpbiB3aW5kb3dzLCBwcm9ncmVzcyBvbiB0aGUgdHJhbnNjb2RlLiBpbmZvIHdpbGwgYmUgYSBudW1iZXIgZnJvbSAwIHRvIDEwMCAqL1xuICBwcm9ncmVzcz86IChpbmZvOiBudW1iZXIpID0+IHZvaWQ7XG59XG5cbmV4cG9ydCBpbnRlcmZhY2UgVHJpbU9wdGlvbnMge1xuICAvKiogUGF0aCB0byBpbnB1dCB2aWRlby4gKi9cbiAgZmlsZVVyaTogc3RyaW5nO1xuXG4gIC8qKiBUaW1lIHRvIHN0YXJ0IHRyaW1taW5nIGluIHNlY29uZHMgKi9cbiAgdHJpbVN0YXJ0OiBudW1iZXI7XG5cbiAgLyoqIFRpbWUgdG8gZW5kIHRyaW1taW5nIGluIHNlY29uZHMgKi9cbiAgdHJpbUVuZDogbnVtYmVyO1xuXG4gIC8qKiBPdXRwdXQgZmlsZSBuYW1lICovXG4gIG91dHB1dEZpbGVOYW1lOiBzdHJpbmc7XG5cbiAgLyoqIFByb2dyZXNzIG9uIHRyYW5zY29kZS4gaW5mbyB3aWxsIGJlIGEgbnVtYmVyIGZyb20gMCB0byAxMDAgKi9cbiAgcHJvZ3Jlc3M/OiAoaW5mbzogYW55KSA9PiB2b2lkO1xufVxuXG5leHBvcnQgaW50ZXJmYWNlIENyZWF0ZVRodW1ibmFpbE9wdGlvbnMge1xuICAvKiogVGhlIHBhdGggdG8gdGhlIHZpZGVvIG9uIHRoZSBkZXZpY2UgKi9cbiAgZmlsZVVyaTogc3RyaW5nO1xuXG4gIC8qKiBUaGUgZmlsZSBuYW1lIGZvciB0aGUgSlBFRyBpbWFnZSAqL1xuICBvdXRwdXRGaWxlTmFtZTogc3RyaW5nO1xuXG4gIC8qKiBMb2NhdGlvbiBpbiB0aGUgdmlkZW8gdG8gY3JlYXRlIHRoZSB0aHVtYm5haWwgKGluIHNlY29uZHMpICovXG4gIGF0VGltZT86IG51bWJlcjtcblxuICAvKiogV2lkdGggb2YgdGhlIHRodW1ibmFpbC4gKi9cbiAgd2lkdGg/OiBudW1iZXI7XG5cbiAgLyoqIEhlaWdodCBvZiB0aGUgdGh1bWJuYWlsLiAqL1xuICBoZWlnaHQ/OiBudW1iZXI7XG5cbiAgLyoqIFF1YWxpdHkgb2YgdGhlIHRodW1ibmFpbCAoYmV0d2VlbiAxIGFuZCAxMDApLiAqL1xuICBxdWFsaXR5PzogbnVtYmVyO1xufVxuXG5leHBvcnQgaW50ZXJmYWNlIEdldFZpZGVvSW5mb09wdGlvbnMge1xuICAvKiogVGhlIHBhdGggdG8gdGhlIHZpZGVvIG9uIHRoZSBkZXZpY2UuICovXG4gIGZpbGVVcmk6IHN0cmluZztcbn1cblxuZXhwb3J0IGludGVyZmFjZSBWaWRlb0luZm8ge1xuICAvKiogV2lkdGggb2YgdGhlIHZpZGVvIGluIHBpeGVscy4gKi9cbiAgd2lkdGg6IG51bWJlcjtcblxuICAvKiogSGVpZ2h0IG9mIHRoZSB2aWRlbyBpbiBwaXhlbHMuICovXG4gIGhlaWdodDogbnVtYmVyO1xuXG4gIC8qKiBPcmllbnRhdGlvbiBvZiB0aGUgdmlkZW8uIFdpbGwgYmUgZWl0aGVyIHBvcnRyYWl0IG9yIGxhbmRzY2FwZS4gKi9cbiAgb3JpZW50YXRpb246ICdwb3J0cmFpdCcgfCAnbGFuZHNjYXBlJztcblxuICAvKiogRHVyYXRpb24gb2YgdGhlIHZpZGVvIGluIHNlY29uZHMuICovXG4gIGR1cmF0aW9uOiBudW1iZXI7XG5cbiAgLyoqIFNpemUgb2YgdGhlIHZpZGVvIGluIGJ5dGVzLiAqL1xuICBzaXplOiBudW1iZXI7XG5cbiAgLyoqIEJpdHJhdGUgb2YgdGhlIHZpZGVvIGluIGJpdHMgcGVyIHNlY29uZC4gKi9cbiAgYml0cmF0ZTogbnVtYmVyO1xufVxuXG4vKipcbiAqIEBuYW1lIFZpZGVvIEVkaXRvclxuICogQGRlc2NyaXB0aW9uIEVkaXQgdmlkZW9zIHVzaW5nIG5hdGl2ZSBkZXZpY2UgQVBJc1xuICpcbiAqIEB1c2FnZVxuICogYGBgdHlwZXNjcmlwdFxuICogaW1wb3J0IHsgVmlkZW9FZGl0b3IgfSBmcm9tICdAaW9uaWMtbmF0aXZlL3ZpZGVvLWVkaXRvci9uZ3gnO1xuICpcbiAqIGNvbnN0cnVjdG9yKHByaXZhdGUgdmlkZW9FZGl0b3I6IFZpZGVvRWRpdG9yKSB7IH1cbiAqXG4gKiAuLi5cbiAqXG4gKiB0aGlzLnZpZGVvRWRpdG9yLnRyYW5zY29kZVZpZGVvKHtcbiAqICAgZmlsZVVyaTogJy9wYXRoL3RvL2lucHV0Lm1vdicsXG4gKiAgIG91dHB1dEZpbGVOYW1lOiAnb3V0cHV0Lm1wNCcsXG4gKiAgIG91dHB1dEZpbGVUeXBlOiBWaWRlb0VkaXRvci5PdXRwdXRGaWxlVHlwZS5NUEVHNFxuICogfSlcbiAqIC50aGVuKChmaWxlVXJpOiBzdHJpbmcpID0+IGNvbnNvbGUubG9nKCd2aWRlbyB0cmFuc2NvZGUgc3VjY2VzcycsIGZpbGVVcmkpKVxuICogLmNhdGNoKChlcnJvcjogYW55KSA9PiBjb25zb2xlLmxvZygndmlkZW8gdHJhbnNjb2RlIGVycm9yJywgZXJyb3IpKTtcbiAqXG4gKiBgYGBcbiAqIEBpbnRlcmZhY2VzXG4gKiBUcmFuc2NvZGVPcHRpb25zXG4gKiBUcmltT3B0aW9uc1xuICogQ3JlYXRlVGh1bWJuYWlsT3B0aW9uc1xuICogR2V0VmlkZW9JbmZvT3B0aW9uc1xuICogVmlkZW9JbmZvXG4gKi9cbkBQbHVnaW4oe1xuICBwbHVnaW5OYW1lOiAnVmlkZW9FZGl0b3InLFxuICBwbHVnaW46ICdjb3Jkb3ZhLXBsdWdpbi12aWRlby1lZGl0b3InLFxuICBwbHVnaW5SZWY6ICdWaWRlb0VkaXRvcicsXG4gIHJlcG86ICdodHRwczovL2dpdGh1Yi5jb20vamJhdmFyaS9jb3Jkb3ZhLXBsdWdpbi12aWRlby1lZGl0b3InLFxuICBwbGF0Zm9ybXM6IFsnQW5kcm9pZCcsICdpT1MnLCAnV2luZG93cycsICdXaW5kb3dzIFBob25lIDgnXSxcbn0pXG5ASW5qZWN0YWJsZSgpXG5leHBvcnQgY2xhc3MgVmlkZW9FZGl0b3IgZXh0ZW5kcyBJb25pY05hdGl2ZVBsdWdpbiB7XG4gIE9wdGltaXplRm9yTmV0d29ya1VzZSA9IHtcbiAgICBOTzogMCxcbiAgICBZRVM6IDEsXG4gIH07XG5cbiAgT3V0cHV0RmlsZVR5cGUgPSB7XG4gICAgTTRWOiAwLFxuICAgIE1QRUc0OiAxLFxuICAgIE00QTogMixcbiAgICBRVUlDS19USU1FOiAzLFxuICB9O1xuXG4gIC8qKlxuICAgKiBUcmFuc2NvZGUgYSB2aWRlb1xuICAgKiBAcGFyYW0gb3B0aW9ucyB7VHJhbnNjb2RlT3B0aW9uc30gT3B0aW9uc1xuICAgKiBAcmV0dXJucyB7UHJvbWlzZTxzdHJpbmc+fSBSZXR1cm5zIGEgcHJvbWlzZSB0aGF0IHJlc29sdmVzIHRvIHRoZSBwYXRoIG9mIHRoZSB0cmFuc2NvZGVkIHZpZGVvXG4gICAqL1xuICBAQ29yZG92YSh7XG4gICAgY2FsbGJhY2tPcmRlcjogJ3JldmVyc2UnLFxuICB9KVxuICB0cmFuc2NvZGVWaWRlbyhvcHRpb25zOiBUcmFuc2NvZGVPcHRpb25zKTogUHJvbWlzZTxzdHJpbmc+IHtcbiAgICByZXR1cm47XG4gIH1cblxuICAvKipcbiAgICogVHJpbSBhIHZpZGVvXG4gICAqIEBwYXJhbSBvcHRpb25zIHtUcmltT3B0aW9uc30gT3B0aW9uc1xuICAgKiBAcmV0dXJucyB7UHJvbWlzZTxzdHJpbmc+fSBSZXR1cm5zIGEgcHJvbWlzZSB0aGF0IHJlc29sdmVzIHRvIHRoZSBwYXRoIG9mIHRoZSB0cmltbWVkIHZpZGVvXG4gICAqL1xuICBAQ29yZG92YSh7XG4gICAgY2FsbGJhY2tPcmRlcjogJ3JldmVyc2UnLFxuICAgIHBsYXRmb3JtczogWydpT1MnXSxcbiAgfSlcbiAgdHJpbShvcHRpb25zOiBUcmltT3B0aW9ucyk6IFByb21pc2U8c3RyaW5nPiB7XG4gICAgcmV0dXJuO1xuICB9XG5cbiAgLyoqXG4gICAqIENyZWF0ZSBhIEpQRUcgdGh1bWJuYWlsIGZyb20gYSB2aWRlb1xuICAgKiBAcGFyYW0gb3B0aW9ucyB7Q3JlYXRlVGh1bWJuYWlsT3B0aW9uc30gT3B0aW9uc1xuICAgKiBAcmV0dXJucyB7UHJvbWlzZTxzdHJpbmc+fSBSZXR1cm5zIGEgcHJvbWlzZSB0aGF0IHJlc29sdmVzIHRvIHRoZSBwYXRoIHRvIHRoZSBqcGVnIGltYWdlIG9uIHRoZSBkZXZpY2VcbiAgICovXG4gIEBDb3Jkb3ZhKHtcbiAgICBjYWxsYmFja09yZGVyOiAncmV2ZXJzZScsXG4gIH0pXG4gIGNyZWF0ZVRodW1ibmFpbChvcHRpb25zOiBDcmVhdGVUaHVtYm5haWxPcHRpb25zKTogUHJvbWlzZTxzdHJpbmc+IHtcbiAgICByZXR1cm47XG4gIH1cblxuICAvKipcbiAgICogR2V0IGluZm8gb24gYSB2aWRlbyAod2lkdGgsIGhlaWdodCwgb3JpZW50YXRpb24sIGR1cmF0aW9uLCBzaXplLCAmIGJpdHJhdGUpXG4gICAqIEBwYXJhbSBvcHRpb25zIHtHZXRWaWRlb0luZm9PcHRpb25zfSBPcHRpb25zXG4gICAqIEByZXR1cm5zIHtQcm9taXNlPFZpZGVvSW5mbz59IFJldHVybnMgYSBwcm9taXNlIHRoYXQgcmVzb2x2ZXMgdG8gYW4gb2JqZWN0IGNvbnRhaW5pbmcgaW5mbyBvbiB0aGUgdmlkZW9cbiAgICovXG4gIEBDb3Jkb3ZhKHtcbiAgICBjYWxsYmFja09yZGVyOiAncmV2ZXJzZScsXG4gIH0pXG4gIGdldFZpZGVvSW5mbyhvcHRpb25zOiBHZXRWaWRlb0luZm9PcHRpb25zKTogUHJvbWlzZTxWaWRlb0luZm8+IHtcbiAgICByZXR1cm47XG4gIH1cbn1cbiJdfQ==

      /***/
    },

    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/header/header.page.html":
    /*!*******************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/header/header.page.html ***!
      \*******************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppHeaderHeaderPageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-header translucent no-border>\r\n    <ion-toolbar>\r\n        <ion-item header-menu lines=\"none\">\r\n            <ion-avatar slot=\"start\">\r\n                <img [src]=\"loggedDetails.upload_image\" height=\"100%\" width=\"100%\" *ngIf=\"loggedDetails.upload_image\">\r\n                <img src=\"../../assets//image/user-login-icon-1.png\" height=\"100%\" width=\"100%\"\r\n                    *ngIf=\"!loggedDetails.upload_image\">\r\n            </ion-avatar>\r\n            <ion-label>\r\n                <h3 font-header class=\"ion-text-capitalize\">{{loggedDetails.stud_name}}</h3>\r\n                <p font-medium>{{city}}</p>\r\n            </ion-label>\r\n            <div class=\"notification-bell-container\" slot=\"end\">\r\n                <p class=\"text-highlight\" (click)=\"moveNotificationPage()\"\r\n                    *ngIf=\"notificationUnreadCount && notificationUnreadCount!=0\">\r\n                    {{notificationUnreadCount > 9 ? '9+': notificationUnreadCount}}</p>\r\n                <ion-icon name=\"notifications-outline\" (click)=\"moveNotificationPage()\" class=\"bell-icon\">\r\n                </ion-icon>\r\n            </div>\r\n            <ion-icon name=\"reorder-four-outline\" (click)=\"openMenu()\" slot=\"end\"></ion-icon>\r\n        </ion-item>\r\n    </ion-toolbar>\r\n</ion-header>";
      /***/
    },

    /***/
    "./src/app/header/header-routing.module.ts":
    /*!*************************************************!*\
      !*** ./src/app/header/header-routing.module.ts ***!
      \*************************************************/

    /*! exports provided: HeaderPageRoutingModule */

    /***/
    function srcAppHeaderHeaderRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "HeaderPageRoutingModule", function () {
        return HeaderPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _header_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./header.page */
      "./src/app/header/header.page.ts");

      var routes = [{
        path: '',
        component: _header_page__WEBPACK_IMPORTED_MODULE_3__["HeaderPage"]
      }];

      var HeaderPageRoutingModule = function HeaderPageRoutingModule() {
        _classCallCheck(this, HeaderPageRoutingModule);
      };

      HeaderPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], HeaderPageRoutingModule);
      /***/
    },

    /***/
    "./src/app/header/header.module.ts":
    /*!*****************************************!*\
      !*** ./src/app/header/header.module.ts ***!
      \*****************************************/

    /*! exports provided: HeaderPageModule */

    /***/
    function srcAppHeaderHeaderModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "HeaderPageModule", function () {
        return HeaderPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _header_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./header-routing.module */
      "./src/app/header/header-routing.module.ts");
      /* harmony import */


      var _header_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./header.page */
      "./src/app/header/header.page.ts");

      var HeaderPageModule = function HeaderPageModule() {
        _classCallCheck(this, HeaderPageModule);
      };

      HeaderPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _header_routing_module__WEBPACK_IMPORTED_MODULE_5__["HeaderPageRoutingModule"]],
        declarations: [_header_page__WEBPACK_IMPORTED_MODULE_6__["HeaderPage"]],
        exports: [_header_page__WEBPACK_IMPORTED_MODULE_6__["HeaderPage"]]
      })], HeaderPageModule);
      /***/
    },

    /***/
    "./src/app/header/header.page.scss":
    /*!*****************************************!*\
      !*** ./src/app/header/header.page.scss ***!
      \*****************************************/

    /*! exports provided: default */

    /***/
    function srcAppHeaderHeaderPageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = ".notification-bell-container {\n  position: relative;\n  display: flex;\n  flex-direction: row;\n  align-items: center;\n  justify-content: center;\n}\n.notification-bell-container .text-highlight {\n  position: absolute;\n  right: -3px;\n  font-size: 10px;\n  cursor: pointer;\n  top: -1px;\n  background: red;\n  border-radius: 50%;\n  z-index: 1111;\n  width: 18px;\n  height: 18px;\n  text-align: center;\n  line-height: 19px;\n  color: #ffffff;\n  font-weight: bold;\n}\n.notification-bell-container .bell-icon {\n  font-size: 30px;\n  position: relative;\n}\n.notification-bell-container .bell-icon:after {\n  content: \"\";\n  position: absolute;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvaGVhZGVyL2hlYWRlci5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxrQkFBQTtFQUNBLGFBQUE7RUFDQSxtQkFBQTtFQUNBLG1CQUFBO0VBQ0EsdUJBQUE7QUFDSjtBQUFJO0VBQ0Usa0JBQUE7RUFDQSxXQUFBO0VBQ0EsZUFBQTtFQUNBLGVBQUE7RUFDQSxTQUFBO0VBQ0EsZUFBQTtFQUNBLGtCQUFBO0VBQ0EsYUFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBQ0Esa0JBQUE7RUFDQSxpQkFBQTtFQUNBLGNBQUE7RUFDQSxpQkFBQTtBQUVOO0FBQUk7RUFDSSxlQUFBO0VBQ0Esa0JBQUE7QUFFUjtBQURRO0VBQ0UsV0FBQTtFQUNBLGtCQUFBO0FBR1YiLCJmaWxlIjoic3JjL2FwcC9oZWFkZXIvaGVhZGVyLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5ub3RpZmljYXRpb24tYmVsbC1jb250YWluZXJ7XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgZmxleC1kaXJlY3Rpb246IHJvdztcclxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgIC50ZXh0LWhpZ2hsaWdodHtcclxuICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICByaWdodDogLTNweDtcclxuICAgICAgZm9udC1zaXplOiAxMHB4O1xyXG4gICAgICBjdXJzb3I6IHBvaW50ZXI7XHJcbiAgICAgIHRvcDogLTFweDtcclxuICAgICAgYmFja2dyb3VuZDogcmVkO1xyXG4gICAgICBib3JkZXItcmFkaXVzOiA1MCU7XHJcbiAgICAgIHotaW5kZXg6IDExMTE7XHJcbiAgICAgIHdpZHRoOiAxOHB4O1xyXG4gICAgICBoZWlnaHQ6IDE4cHg7XHJcbiAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICAgbGluZS1oZWlnaHQ6IDE5cHg7XHJcbiAgICAgIGNvbG9yOiAjZmZmZmZmO1xyXG4gICAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgIH1cclxuICAgIC5iZWxsLWljb257XHJcbiAgICAgICAgZm9udC1zaXplOiAzMHB4O1xyXG4gICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgICAgICAmOmFmdGVye1xyXG4gICAgICAgICAgY29udGVudDogJyc7XHJcbiAgICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgXHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgfSJdfQ== */";
      /***/
    },

    /***/
    "./src/app/header/header.page.ts":
    /*!***************************************!*\
      !*** ./src/app/header/header.page.ts ***!
      \***************************************/

    /*! exports provided: HeaderPage */

    /***/
    function srcAppHeaderHeaderPageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "HeaderPage", function () {
        return HeaderPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _global_services_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ../global-services.service */
      "./src/app/global-services.service.ts");
      /* harmony import */


      var _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic-native/geolocation/ngx */
      "./node_modules/@ionic-native/geolocation/__ivy_ngcc__/ngx/index.js");
      /* harmony import */


      var _ionic_native_native_geocoder_ngx__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @ionic-native/native-geocoder/ngx */
      "./node_modules/@ionic-native/native-geocoder/__ivy_ngcc__/ngx/index.js");

      var HeaderPage = /*#__PURE__*/function () {
        function HeaderPage(menu, service, geolocation, nativeGeocoder, navController) {
          var _this2 = this;

          _classCallCheck(this, HeaderPage);

          this.menu = menu;
          this.service = service;
          this.geolocation = geolocation;
          this.nativeGeocoder = nativeGeocoder;
          this.navController = navController;
          this.notificationUrl = 'getNotificationText'; //=====================get Login Details=====================

          this.loggedDetails = {};
          this.city = localStorage.getItem('location'); // geocoder options

          this.nativeGeocoderOptions = {
            useLocale: true,
            maxResults: 5
          };
          this.userName = localStorage.getItem('username');
          this.subscription = this.service.getMessage().subscribe(function (message) {
            if (message.text == 'startNotificationCountFunction') {
              _this2.getnotificationsList();
            }
          });
          this.service.invoke.subscribe(function (res) {
            if (res) {
              _this2.cityChange(res);
            }
          });
        }

        _createClass(HeaderPage, [{
          key: "cityChange",
          value: function cityChange(city) {
            if (!this.city) {
              this.city = city;
              localStorage.setItem('location', this.city);
            }
          }
        }, {
          key: "ngOnInit",
          value: function ngOnInit() {
            this.userInfo(); // this.getUserPosition();

            this.service.checkGPSPermission();
          }
        }, {
          key: "openMenu",
          value: function openMenu() {
            // this.menu.enable(true, 'first');
            this.menu.open();
          }
        }, {
          key: "userInfo",
          value: function userInfo() {
            var _this3 = this;

            if (localStorage.getItem('username')) {
              var obj = {
                username: localStorage.getItem('username')
              };
              this.service.post_data("getAlumniOthersInfo", obj).subscribe(function (result) {
                var json = _this3.service.parserToJSON(result);

                var parsedJSON = JSON.parse(json.string);
                _this3.loggedDetails = parsedJSON[0];
              }, function (error) {
                console.log(error);
              });
            }
          }
        }, {
          key: "getUserPosition",
          value: function getUserPosition() {
            var _this4 = this;

            this.options = {
              enableHighAccuracy: false
            };
            this.geolocation.getCurrentPosition(this.options).then(function (pos) {
              _this4.currentPos = pos;
              _this4.latitude = pos.coords.latitude;
              _this4.longitude = pos.coords.longitude;

              _this4.getAddress(_this4.latitude, _this4.longitude);
            }, function (err) {
              console.log("error : " + err.message);
              ;
            });
          } // get address using coordinates

        }, {
          key: "getAddress",
          value: function getAddress(lat, _long) {
            var _this5 = this;

            this.nativeGeocoder.reverseGeocode(lat, _long, this.nativeGeocoderOptions).then(function (res) {
              var address = _this5.pretifyAddress(res[0]);

              var value = address.split(",");
              var count = value.length;
              _this5.city = value[count - 5];
            })["catch"](function (error) {
              alert('Error getting location' + error + JSON.stringify(error));
            });
          } // address

        }, {
          key: "pretifyAddress",
          value: function pretifyAddress(addressObj) {
            var obj = [];
            var address = "";

            for (var key in addressObj) {
              obj.push(addressObj[key]);
            }

            obj.reverse();

            for (var val in obj) {
              if (obj[val].length) address += obj[val] + ', ';
            }

            return address.slice(0, -2);
          }
        }, {
          key: "getnotificationsList",
          value: function getnotificationsList() {
            var _this6 = this;

            var obj = {
              msg_id: 0,
              username: this.userName,
              rows_in_page: 0,
              pageid: 1
            };
            this.service.post_data(this.notificationUrl, obj).subscribe(function (result) {
              var json = _this6.service.parserToJSON(result);

              var list = JSON.parse(json.string);

              if (list) {
                _this6.notificationUnreadCount = 0;

                if (list.Details.length > 0) {
                  list.Details.forEach(function (element, i) {
                    if (!JSON.parse(String(element.read_sts).toLowerCase())) {
                      _this6.notificationUnreadCount++;
                    }
                  });
                }
              }
            });
          }
        }, {
          key: "moveNotificationPage",
          value: function moveNotificationPage() {
            this.navController.navigateForward('/notification');
          }
        }]);

        return HeaderPage;
      }();

      HeaderPage.ctorParameters = function () {
        return [{
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["MenuController"]
        }, {
          type: _global_services_service__WEBPACK_IMPORTED_MODULE_3__["GlobalServicesService"]
        }, {
          type: _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_4__["Geolocation"]
        }, {
          type: _ionic_native_native_geocoder_ngx__WEBPACK_IMPORTED_MODULE_5__["NativeGeocoder"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"]
        }];
      };

      HeaderPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-header',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./header.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/header/header.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./header.page.scss */
        "./src/app/header/header.page.scss"))["default"]]
      })], HeaderPage);
      /***/
    }
  }]);
})();
//# sourceMappingURL=default~preview-filter-preview-filter-module~upload-images-videos-upload-images-videos-module-es5.js.map