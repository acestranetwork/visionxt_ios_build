(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["resourses-videos-resourses-videos-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/header/header.page.html":
/*!*******************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/header/header.page.html ***!
  \*******************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header translucent no-border>\r\n    <ion-toolbar>\r\n        <ion-item header-menu lines=\"none\">\r\n            <ion-avatar slot=\"start\">\r\n                <img [src]=\"loggedDetails.upload_image\" height=\"100%\" width=\"100%\" *ngIf=\"loggedDetails.upload_image\">\r\n                <img src=\"../../assets//image/user-login-icon-1.png\" height=\"100%\" width=\"100%\"\r\n                    *ngIf=\"!loggedDetails.upload_image\">\r\n            </ion-avatar>\r\n            <ion-label>\r\n                <h3 font-header class=\"ion-text-capitalize\">{{loggedDetails.stud_name}}</h3>\r\n                <p font-medium>{{city}}</p>\r\n            </ion-label>\r\n            <div class=\"notification-bell-container\" slot=\"end\">\r\n                <p class=\"text-highlight\" (click)=\"moveNotificationPage()\"\r\n                    *ngIf=\"notificationUnreadCount && notificationUnreadCount!=0\">\r\n                    {{notificationUnreadCount > 9 ? '9+': notificationUnreadCount}}</p>\r\n                <ion-icon name=\"notifications-outline\" (click)=\"moveNotificationPage()\" class=\"bell-icon\">\r\n                </ion-icon>\r\n            </div>\r\n            <ion-icon name=\"reorder-four-outline\" (click)=\"openMenu()\" slot=\"end\"></ion-icon>\r\n        </ion-item>\r\n    </ion-toolbar>\r\n</ion-header>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/resourses-videos/resourses-videos.page.html":
/*!***************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/resourses-videos/resourses-videos.page.html ***!
  \***************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-header>\r\n\r\n</app-header>\r\n<ion-content>\r\n    <div class=\"inner-div\">\r\n        <ion-grid>\r\n            <ion-row class=\"ion-text-center mt-3 mb-2\">\r\n                <ion-col>\r\n                    <h5 header-text>Resources</h5>\r\n                </ion-col>\r\n            </ion-row>\r\n            <ion-row *ngIf=\"resourceList.length == 0\">\r\n                <ion-col>\r\n                    <ion-card class=\"ion-margin-vertical\">\r\n                        <ion-card-content>\r\n                            No Record Found\r\n                        </ion-card-content>\r\n                    </ion-card>\r\n                </ion-col>\r\n            </ion-row>\r\n            <ion-row *ngIf=\"resourceList.length > 0\">\r\n                <ion-col size=\"6\" (click)=\"play(vdo.filename)\" *ngFor=\"let vdo of resourceList;\">\r\n                    <div ion-col-height>\r\n                        <ion-content>\r\n                            <video poster=\"../../assets/image/poster1.jpg\" preload=\"auto\" crossorigin class=\"video_tag\">\r\n                                <source [src]=\"vdo.filename\" type=\"\">\r\n                            </video>\r\n                            <!-- <ion-item class=\"fs-12 black-bg\" black-bg fill=\"clear\">\r\n                                <ion-icon name=\"play-circle-outline\"></ion-icon> &nbsp; {{vdo.filetitle}}\r\n                            </ion-item> -->\r\n                            <!-- <ion-icon name=\"play-circle-outline\" class=\"videoicon\"></ion-icon> -->\r\n                            <div class=\"vdo-title\">\r\n                                <b>\r\n                                    <ion-text color=\"dark\">{{vdo.filetitle}}</ion-text>\r\n                                </b>\r\n                            </div>\r\n                            <div class=\"overlay\">\r\n                                <ion-icon name=\"play-circle-outline\" class=\"videoicon\"></ion-icon>\r\n                            </div>\r\n                        </ion-content>\r\n                    </div>\r\n                    <!-- <ion-content>\r\n                        <video poster=\"\" preload=\"auto\" crossorigin class=\"video_tag\">\r\n                            <source [src]=\"vdo.filename\" type=\"\">\r\n                        </video>\r\n                        <ion-item class=\"fs-12 black-bg\"  fill=\"clear\">\r\n                            <ion-icon name=\"play-circle-outline\"></ion-icon> <span> &nbsp; {{vdo.filetitle}}</span>\r\n                        </ion-item>\r\n                    </ion-content> -->\r\n                    <!-- <ion-content>\r\n                        <ion-button class=\"ion-justify-content-around\" tappable fill=\"clear\" (click)=\"play(vdo.filename)\">\r\n                            <ion-icon color=\"white\" name=\"play-circle-outline\"></ion-icon>\r\n                        </ion-button>\r\n                    </ion-content> -->\r\n                </ion-col>\r\n\r\n            </ion-row>\r\n            <!-- <ion-row>\r\n                <ion-col ion-col-height>\r\n                    <ion-content>\r\n                        <ion-button class=\"ion-justify-content-around\" tappable fill=\"clear\" (click)=\"play()\">\r\n                            <ion-icon color=\"white\" name=\"play-circle-outline\"></ion-icon>\r\n                        </ion-button>\r\n                    </ion-content>\r\n                </ion-col>\r\n                <ion-col ion-col-height>\r\n                    <ion-content>\r\n                        <ion-button class=\"ion-justify-content-around\" tappable fill=\"clear\" (click)=\"play()\">\r\n                            <ion-icon color=\"white\" name=\"play-circle-outline\"></ion-icon>\r\n                        </ion-button>\r\n                    </ion-content>\r\n                </ion-col>\r\n            </ion-row>\r\n            <ion-row>\r\n                <ion-col ion-col-height>\r\n                    <ion-content>\r\n                        <ion-button class=\"ion-justify-content-around\" tappable fill=\"clear\" (click)=\"play()\">\r\n                            <ion-icon color=\"white\" name=\"play-circle-outline\"></ion-icon>\r\n                        </ion-button>\r\n                    </ion-content>\r\n                </ion-col>\r\n                <ion-col ion-col-height>\r\n                    <ion-content>\r\n                        <ion-button class=\"ion-justify-content-around\" tappable fill=\"clear\" (click)=\"play()\">\r\n                            <ion-icon color=\"white\" name=\"play-circle-outline\"></ion-icon>\r\n                        </ion-button>\r\n                    </ion-content>\r\n                </ion-col>\r\n            </ion-row> -->\r\n        </ion-grid>\r\n    </div>\r\n</ion-content>\r\n<ion-toolbar color=\"yellow\" slot=\"bottom\">\r\n    <ion-tabs>\r\n        <ion-tab-bar color=\"yellow\" slot=\"bottom\">\r\n            <ion-tab-button tab=\"share\" [routerLink]=\"['/home']\">\r\n                <ion-icon name=\"home-outline\"></ion-icon>\r\n                <ion-label>Home</ion-label>\r\n            </ion-tab-button>\r\n            <!-- <ion-tab-button tab=\"camera\">\r\n                <ion-icon name=\"bookmark\"></ion-icon>\r\n                <ion-label>Bookmark</ion-label>\r\n            </ion-tab-button> -->\r\n\r\n            <!-- <ion-tab-button [routerLink]=\"['/search']\">\r\n                <ion-icon name=\"search-outline\"></ion-icon>\r\n                <ion-label>Search</ion-label>\r\n            </ion-tab-button> -->\r\n\r\n            <!-- <ion-tab-button tab=\"share\">\r\n                <ion-icon name=\"share-social-outline\"></ion-icon>\r\n                <ion-label>Share</ion-label>\r\n            </ion-tab-button> -->\r\n        </ion-tab-bar>\r\n    </ion-tabs>\r\n</ion-toolbar>");

/***/ }),

/***/ "./src/app/header/header-routing.module.ts":
/*!*************************************************!*\
  !*** ./src/app/header/header-routing.module.ts ***!
  \*************************************************/
/*! exports provided: HeaderPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HeaderPageRoutingModule", function() { return HeaderPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _header_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./header.page */ "./src/app/header/header.page.ts");




const routes = [
    {
        path: '',
        component: _header_page__WEBPACK_IMPORTED_MODULE_3__["HeaderPage"]
    }
];
let HeaderPageRoutingModule = class HeaderPageRoutingModule {
};
HeaderPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], HeaderPageRoutingModule);



/***/ }),

/***/ "./src/app/header/header.module.ts":
/*!*****************************************!*\
  !*** ./src/app/header/header.module.ts ***!
  \*****************************************/
/*! exports provided: HeaderPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HeaderPageModule", function() { return HeaderPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _header_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./header-routing.module */ "./src/app/header/header-routing.module.ts");
/* harmony import */ var _header_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./header.page */ "./src/app/header/header.page.ts");







let HeaderPageModule = class HeaderPageModule {
};
HeaderPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _header_routing_module__WEBPACK_IMPORTED_MODULE_5__["HeaderPageRoutingModule"]
        ],
        declarations: [_header_page__WEBPACK_IMPORTED_MODULE_6__["HeaderPage"]],
        exports: [_header_page__WEBPACK_IMPORTED_MODULE_6__["HeaderPage"]]
    })
], HeaderPageModule);



/***/ }),

/***/ "./src/app/header/header.page.scss":
/*!*****************************************!*\
  !*** ./src/app/header/header.page.scss ***!
  \*****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".notification-bell-container {\n  position: relative;\n  display: flex;\n  flex-direction: row;\n  align-items: center;\n  justify-content: center;\n}\n.notification-bell-container .text-highlight {\n  position: absolute;\n  right: -3px;\n  font-size: 10px;\n  cursor: pointer;\n  top: -1px;\n  background: red;\n  border-radius: 50%;\n  z-index: 1111;\n  width: 18px;\n  height: 18px;\n  text-align: center;\n  line-height: 19px;\n  color: #ffffff;\n  font-weight: bold;\n}\n.notification-bell-container .bell-icon {\n  font-size: 30px;\n  position: relative;\n}\n.notification-bell-container .bell-icon:after {\n  content: \"\";\n  position: absolute;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvaGVhZGVyL2hlYWRlci5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxrQkFBQTtFQUNBLGFBQUE7RUFDQSxtQkFBQTtFQUNBLG1CQUFBO0VBQ0EsdUJBQUE7QUFDSjtBQUFJO0VBQ0Usa0JBQUE7RUFDQSxXQUFBO0VBQ0EsZUFBQTtFQUNBLGVBQUE7RUFDQSxTQUFBO0VBQ0EsZUFBQTtFQUNBLGtCQUFBO0VBQ0EsYUFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBQ0Esa0JBQUE7RUFDQSxpQkFBQTtFQUNBLGNBQUE7RUFDQSxpQkFBQTtBQUVOO0FBQUk7RUFDSSxlQUFBO0VBQ0Esa0JBQUE7QUFFUjtBQURRO0VBQ0UsV0FBQTtFQUNBLGtCQUFBO0FBR1YiLCJmaWxlIjoic3JjL2FwcC9oZWFkZXIvaGVhZGVyLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5ub3RpZmljYXRpb24tYmVsbC1jb250YWluZXJ7XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgZmxleC1kaXJlY3Rpb246IHJvdztcclxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgIC50ZXh0LWhpZ2hsaWdodHtcclxuICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICByaWdodDogLTNweDtcclxuICAgICAgZm9udC1zaXplOiAxMHB4O1xyXG4gICAgICBjdXJzb3I6IHBvaW50ZXI7XHJcbiAgICAgIHRvcDogLTFweDtcclxuICAgICAgYmFja2dyb3VuZDogcmVkO1xyXG4gICAgICBib3JkZXItcmFkaXVzOiA1MCU7XHJcbiAgICAgIHotaW5kZXg6IDExMTE7XHJcbiAgICAgIHdpZHRoOiAxOHB4O1xyXG4gICAgICBoZWlnaHQ6IDE4cHg7XHJcbiAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICAgbGluZS1oZWlnaHQ6IDE5cHg7XHJcbiAgICAgIGNvbG9yOiAjZmZmZmZmO1xyXG4gICAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgIH1cclxuICAgIC5iZWxsLWljb257XHJcbiAgICAgICAgZm9udC1zaXplOiAzMHB4O1xyXG4gICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgICAgICAmOmFmdGVye1xyXG4gICAgICAgICAgY29udGVudDogJyc7XHJcbiAgICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgXHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgfSJdfQ== */");

/***/ }),

/***/ "./src/app/header/header.page.ts":
/*!***************************************!*\
  !*** ./src/app/header/header.page.ts ***!
  \***************************************/
/*! exports provided: HeaderPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HeaderPage", function() { return HeaderPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _global_services_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../global-services.service */ "./src/app/global-services.service.ts");
/* harmony import */ var _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic-native/geolocation/ngx */ "./node_modules/@ionic-native/geolocation/__ivy_ngcc__/ngx/index.js");
/* harmony import */ var _ionic_native_native_geocoder_ngx__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic-native/native-geocoder/ngx */ "./node_modules/@ionic-native/native-geocoder/__ivy_ngcc__/ngx/index.js");






let HeaderPage = class HeaderPage {
    constructor(menu, service, geolocation, nativeGeocoder, navController) {
        this.menu = menu;
        this.service = service;
        this.geolocation = geolocation;
        this.nativeGeocoder = nativeGeocoder;
        this.navController = navController;
        this.notificationUrl = 'getNotificationText';
        //=====================get Login Details=====================
        this.loggedDetails = {};
        this.city = localStorage.getItem('location');
        // geocoder options
        this.nativeGeocoderOptions = {
            useLocale: true,
            maxResults: 5
        };
        this.userName = localStorage.getItem('username');
        this.subscription = this.service.getMessage().subscribe(message => {
            if (message.text == 'startNotificationCountFunction') {
                this.getnotificationsList();
            }
        });
        this.service.invoke.subscribe((res) => {
            if (res) {
                this.cityChange(res);
            }
        });
    }
    cityChange(city) {
        if (!this.city) {
            this.city = city;
            localStorage.setItem('location', this.city);
        }
    }
    ngOnInit() {
        this.userInfo();
        // this.getUserPosition();
        this.service.checkGPSPermission();
    }
    openMenu() {
        // this.menu.enable(true, 'first');
        this.menu.open();
    }
    userInfo() {
        if (localStorage.getItem('username')) {
            let obj = {
                username: localStorage.getItem('username'),
            };
            this.service.post_data("getAlumniOthersInfo", obj)
                .subscribe((result) => {
                let json = this.service.parserToJSON(result);
                var parsedJSON = JSON.parse(json.string);
                this.loggedDetails = parsedJSON[0];
            }, (error) => {
                console.log(error);
            });
        }
    }
    getUserPosition() {
        this.options = {
            enableHighAccuracy: false
        };
        this.geolocation.getCurrentPosition(this.options).then((pos) => {
            this.currentPos = pos;
            this.latitude = pos.coords.latitude;
            this.longitude = pos.coords.longitude;
            this.getAddress(this.latitude, this.longitude);
        }, (err) => {
            console.log("error : " + err.message);
            ;
        });
    }
    // get address using coordinates
    getAddress(lat, long) {
        this.nativeGeocoder.reverseGeocode(lat, long, this.nativeGeocoderOptions)
            .then((res) => {
            var address = this.pretifyAddress(res[0]);
            var value = address.split(",");
            var count = value.length;
            this.city = value[count - 5];
        })
            .catch((error) => {
            alert('Error getting location' + error + JSON.stringify(error));
        });
    }
    // address
    pretifyAddress(addressObj) {
        let obj = [];
        let address = "";
        for (let key in addressObj) {
            obj.push(addressObj[key]);
        }
        obj.reverse();
        for (let val in obj) {
            if (obj[val].length)
                address += obj[val] + ', ';
        }
        return address.slice(0, -2);
    }
    getnotificationsList() {
        const obj = {
            msg_id: 0,
            username: this.userName,
            rows_in_page: 0,
            pageid: 1
        };
        this.service.post_data(this.notificationUrl, obj).subscribe((result) => {
            let json = this.service.parserToJSON(result);
            let list = JSON.parse(json.string);
            if (list) {
                this.notificationUnreadCount = 0;
                if (list.Details.length > 0) {
                    list.Details.forEach((element, i) => {
                        if (!JSON.parse(String(element.read_sts).toLowerCase())) {
                            this.notificationUnreadCount++;
                        }
                    });
                }
            }
        });
    }
    moveNotificationPage() {
        this.navController.navigateForward('/notification');
    }
};
HeaderPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["MenuController"] },
    { type: _global_services_service__WEBPACK_IMPORTED_MODULE_3__["GlobalServicesService"] },
    { type: _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_4__["Geolocation"] },
    { type: _ionic_native_native_geocoder_ngx__WEBPACK_IMPORTED_MODULE_5__["NativeGeocoder"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"] }
];
HeaderPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-header',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./header.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/header/header.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./header.page.scss */ "./src/app/header/header.page.scss")).default]
    })
], HeaderPage);



/***/ }),

/***/ "./src/app/resourses-videos/resourses-videos-routing.module.ts":
/*!*********************************************************************!*\
  !*** ./src/app/resourses-videos/resourses-videos-routing.module.ts ***!
  \*********************************************************************/
/*! exports provided: ResoursesVideosPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ResoursesVideosPageRoutingModule", function() { return ResoursesVideosPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _resourses_videos_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./resourses-videos.page */ "./src/app/resourses-videos/resourses-videos.page.ts");




const routes = [
    {
        path: '',
        component: _resourses_videos_page__WEBPACK_IMPORTED_MODULE_3__["ResoursesVideosPage"]
    }
];
let ResoursesVideosPageRoutingModule = class ResoursesVideosPageRoutingModule {
};
ResoursesVideosPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], ResoursesVideosPageRoutingModule);



/***/ }),

/***/ "./src/app/resourses-videos/resourses-videos.module.ts":
/*!*************************************************************!*\
  !*** ./src/app/resourses-videos/resourses-videos.module.ts ***!
  \*************************************************************/
/*! exports provided: ResoursesVideosPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ResoursesVideosPageModule", function() { return ResoursesVideosPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _resourses_videos_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./resourses-videos-routing.module */ "./src/app/resourses-videos/resourses-videos-routing.module.ts");
/* harmony import */ var _resourses_videos_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./resourses-videos.page */ "./src/app/resourses-videos/resourses-videos.page.ts");
/* harmony import */ var _header_header_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../header/header.module */ "./src/app/header/header.module.ts");








let ResoursesVideosPageModule = class ResoursesVideosPageModule {
};
ResoursesVideosPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _resourses_videos_routing_module__WEBPACK_IMPORTED_MODULE_5__["ResoursesVideosPageRoutingModule"], _header_header_module__WEBPACK_IMPORTED_MODULE_7__["HeaderPageModule"]
        ],
        declarations: [_resourses_videos_page__WEBPACK_IMPORTED_MODULE_6__["ResoursesVideosPage"]]
    })
], ResoursesVideosPageModule);



/***/ }),

/***/ "./src/app/resourses-videos/resourses-videos.page.scss":
/*!*************************************************************!*\
  !*** ./src/app/resourses-videos/resourses-videos.page.scss ***!
  \*************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("[ion-col-height] {\n  width: 100%;\n  height: 150px;\n}\n\n[font-small] {\n  font-weight: bold !important;\n  --border-radius: 0px;\n}\n\n[margin-1] {\n  margin: 1.5rem;\n}\n\n.icon {\n  display: flex;\n  font-size: 45px;\n  color: #ffC977;\n  vertical-align: middle;\n}\n\n.video_tag {\n  width: 100%;\n  height: 100%;\n  -o-object-fit: cover;\n     object-fit: cover;\n  overflow: hidden;\n  position: absolute;\n  z-index: 0;\n}\n\n.black-bg {\n  background: #000000db !important;\n}\n\n.overlay {\n  position: absolute;\n  bottom: 0;\n  left: 0;\n  z-index: 1;\n  height: 20%;\n}\n\n.overlay p {\n  margin-top: 0;\n  margin-bottom: 1rem;\n  background: var(--ion-color-yellow);\n  padding: 10px;\n  position: absolute;\n  bottom: 0;\n  text-overflow: ellipsis;\n  /* width: 128%; */\n}\n\nion-content {\n  --background: #212721;\n}\n\n.videoicon {\n  color: white;\n  font-size: 60px;\n  position: absolute;\n  top: 50%;\n  left: 50%;\n  transform: translate(-50%, -50%);\n  -ms-transform: translate(-50%, -50%);\n  text-align: center;\n}\n\n.overlay {\n  position: absolute;\n  top: 0;\n  bottom: 0;\n  left: 0;\n  right: 0;\n  height: 100%;\n  width: 100%;\n  opacity: 0;\n  transition: 0.3s ease;\n  opacity: 1;\n}\n\n.vdo-title {\n  position: absolute;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcmVzb3Vyc2VzLXZpZGVvcy9yZXNvdXJzZXMtdmlkZW9zLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLFdBQUE7RUFDQSxhQUFBO0FBQ0o7O0FBWUE7RUFFSSw0QkFBQTtFQUNBLG9CQUFBO0FBVko7O0FBYUE7RUFDSSxjQUFBO0FBVko7O0FBYUE7RUFDSSxhQUFBO0VBQ0EsZUFBQTtFQUNBLGNBQUE7RUFDQSxzQkFBQTtBQVZKOztBQXVCQTtFQUNJLFdBQUE7RUFDQSxZQUFBO0VBQ0Esb0JBQUE7S0FBQSxpQkFBQTtFQUNBLGdCQUFBO0VBQ0Esa0JBQUE7RUFDQSxVQUFBO0FBcEJKOztBQXdCQTtFQUNJLGdDQUFBO0FBckJKOztBQXlCQTtFQUNJLGtCQUFBO0VBQ0EsU0FBQTtFQUNBLE9BQUE7RUFDQSxVQUFBO0VBQ0EsV0FBQTtBQXRCSjs7QUF1Qkk7RUFDSSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSxtQ0FBQTtFQUNBLGFBQUE7RUFDQSxrQkFBQTtFQUNBLFNBQUE7RUFDQSx1QkFBQTtFQUNBLGlCQUFBO0FBckJSOztBQTZCQTtFQUNJLHFCQUFBO0FBMUJKOztBQTZCQTtFQUlJLFlBQUE7RUFDQSxlQUFBO0VBQ0Esa0JBQUE7RUFDQSxRQUFBO0VBQ0EsU0FBQTtFQUNBLGdDQUFBO0VBQ0Esb0NBQUE7RUFDQSxrQkFBQTtBQTdCSjs7QUErQkk7RUFDQSxrQkFBQTtFQUNBLE1BQUE7RUFDQSxTQUFBO0VBQ0EsT0FBQTtFQUNBLFFBQUE7RUFDQSxZQUFBO0VBQ0EsV0FBQTtFQUNBLFVBQUE7RUFDQSxxQkFBQTtFQUNBLFVBQUE7QUE1Qko7O0FBK0JJO0VBQ0ksa0JBQUE7QUE1QlIiLCJmaWxlIjoic3JjL2FwcC9yZXNvdXJzZXMtdmlkZW9zL3Jlc291cnNlcy12aWRlb3MucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiW2lvbi1jb2wtaGVpZ2h0XSB7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGhlaWdodDogMTUwcHg7XHJcbiAgICAvLyBpb24tY29udGVudCB7XHJcbiAgICAvLyAgICAgLS1iYWNrZ3JvdW5kOiAjNTI1MjUyO1xyXG4gICAgLy8gICAgIGlvbi1idXR0b24ge1xyXG4gICAgLy8gICAgICAgICBmb250LXNpemU6IDMwcHg7XHJcbiAgICAvLyAgICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIC8vICAgICAgICAgbGVmdDogNTAlO1xyXG4gICAgLy8gICAgICAgICB0b3A6IDUwJTtcclxuICAgIC8vICAgICAgICAgdHJhbnNmb3JtOiB0cmFuc2xhdGUoLTUwJSwgLTUwJSk7XHJcbiAgICAvLyAgICAgfVxyXG4gICAgLy8gfVxyXG59XHJcblxyXG5bZm9udC1zbWFsbF0ge1xyXG4gICAgLy8gZm9udC1zaXplOiAxMnB4IWltcG9ydGFudDtcclxuICAgIGZvbnQtd2VpZ2h0OiBib2xkICFpbXBvcnRhbnQ7XHJcbiAgICAtLWJvcmRlci1yYWRpdXM6IDBweDtcclxufVxyXG5cclxuW21hcmdpbi0xXSB7XHJcbiAgICBtYXJnaW46IDEuNXJlbTtcclxufVxyXG5cclxuLmljb24ge1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGZvbnQtc2l6ZTogNDVweDtcclxuICAgIGNvbG9yOiAjZmZDOTc3O1xyXG4gICAgdmVydGljYWwtYWxpZ246IG1pZGRsZTtcclxufVxyXG5cclxuLy8gLnZpZGVvLXBsYXllciB7XHJcbi8vICAgICBiYWNrZ3JvdW5kOiAjMWYxZTFkO1xyXG4vLyAgICAgd2lkdGg6IDEwMCU7XHJcbi8vICAgICBoZWlnaHQ6IDEwMCU7XHJcbi8vICAgICB2aWRlbyB7XHJcbi8vICAgICAgICAgd2lkdGg6IDEwMCU7XHJcbi8vICAgICAgICAgaGVpZ2h0OiAxMDAlO1xyXG4vLyAgICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuLy8gICAgIH1cclxuLy8gfVxyXG4udmlkZW9fdGFnIHtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgb2JqZWN0LWZpdDogY292ZXI7XHJcbiAgICBvdmVyZmxvdzogaGlkZGVuO1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgei1pbmRleDogMDtcclxuICAgIC8vIGJvcmRlci1yYWRpdXM6IDEwcHg7XHJcbn1cclxuXHJcbi5ibGFjay1iZyB7XHJcbiAgICBiYWNrZ3JvdW5kOiAjMDAwMDAwZGIhaW1wb3J0YW50O1xyXG4gICAgLy8gY29sb3I6ICNmZmZmZmY7XHJcbn1cclxuXHJcbi5vdmVybGF5IHtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIGJvdHRvbTogMDtcclxuICAgIGxlZnQ6IDA7XHJcbiAgICB6LWluZGV4OiAxO1xyXG4gICAgaGVpZ2h0OiAyMCU7XHJcbiAgICBwIHtcclxuICAgICAgICBtYXJnaW4tdG9wOiAwO1xyXG4gICAgICAgIG1hcmdpbi1ib3R0b206IDFyZW07XHJcbiAgICAgICAgYmFja2dyb3VuZDogdmFyKC0taW9uLWNvbG9yLXllbGxvdyk7IC8vI2Y2Yzc2MjtcclxuICAgICAgICBwYWRkaW5nOiAxMHB4O1xyXG4gICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgICAgICBib3R0b206IDA7XHJcbiAgICAgICAgdGV4dC1vdmVyZmxvdzogZWxsaXBzaXM7XHJcbiAgICAgICAgLyogd2lkdGg6IDEyOCU7ICovXHJcbiAgICB9XHJcbn1cclxuLy8gLnZpZGVvaWNvbntcclxuLy8gICAgIGZvbnQtc2l6ZTogNjBweDtcclxuLy8gICAgIGNvbG9yOiB3aGl0ZTtcclxuLy8gICAgIG1hcmdpbjogNDVweCA2NXB4O1xyXG4vLyB9XHJcbmlvbi1jb250ZW50IHtcclxuICAgIC0tYmFja2dyb3VuZDogIzIxMjcyMTtcclxufVxyXG5cclxuLnZpZGVvaWNvbntcclxuICAgIC8vIGZvbnQtc2l6ZTogNjBweDtcclxuICAgIC8vIGNvbG9yOiB3aGl0ZTtcclxuICAgIC8vIG1hcmdpbjogMjUlIDI1JTtcclxuICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgIGZvbnQtc2l6ZTogNjBweDtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIHRvcDogNTAlO1xyXG4gICAgbGVmdDogNTAlO1xyXG4gICAgdHJhbnNmb3JtOiB0cmFuc2xhdGUoLTUwJSwgLTUwJSk7XHJcbiAgICAtbXMtdHJhbnNmb3JtOiB0cmFuc2xhdGUoLTUwJSwgLTUwJSk7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICB9XHJcbiAgICAub3ZlcmxheSB7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICB0b3A6IDA7XHJcbiAgICBib3R0b206IDA7XHJcbiAgICBsZWZ0OiAwO1xyXG4gICAgcmlnaHQ6IDA7XHJcbiAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIG9wYWNpdHk6IDA7XHJcbiAgICB0cmFuc2l0aW9uOiAuM3MgZWFzZTtcclxuICAgIG9wYWNpdHk6IDE7XHJcbiAgICB9XHJcblxyXG4gICAgLnZkby10aXRsZXtcclxuICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICB9Il19 */");

/***/ }),

/***/ "./src/app/resourses-videos/resourses-videos.page.ts":
/*!***********************************************************!*\
  !*** ./src/app/resourses-videos/resourses-videos.page.ts ***!
  \***********************************************************/
/*! exports provided: ResoursesVideosPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ResoursesVideosPage", function() { return ResoursesVideosPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _global_services_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../global-services.service */ "./src/app/global-services.service.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _ionic_native_streaming_media_ngx__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic-native/streaming-media/ngx */ "./node_modules/@ionic-native/streaming-media/__ivy_ngcc__/ngx/index.js");





let ResoursesVideosPage = class ResoursesVideosPage {
    constructor(streamingMedia, service, menu, navCtrl) {
        this.streamingMedia = streamingMedia;
        this.service = service;
        this.menu = menu;
        this.navCtrl = navCtrl;
        // ====================Variables=========================
        this.url = 'getResourceList';
        this.resourceList = [];
    }
    ngOnInit() {
        this.getResourceList();
    }
    ionViewWillEnter() {
        this.service.sendMessage('startNotificationCountFunction');
    }
    // =====================================================
    //video player
    // videoUrl = this.service.STREAMING_VIDEO_URL;
    play(vdoUrl) {
        let options = {
            successCallback: () => { },
            errorCallback: (e) => { console.log('Error streaming'); },
            orientation: 'portrait',
            shouldAutoClose: true,
            controls: true
        };
        this.streamingMedia.playVideo(vdoUrl, options);
    }
    openMenu() {
        // this.menu.enable(true, 'first');
        this.menu.open();
    }
    getResourceList() {
        const obj = {};
        this.service.post_data(this.url, obj).subscribe((result) => {
            let res = this.service.parserToJSON(result);
            // let json: any = this.service.parserToJSON(result);
            // let jons1 = JSON.stringify(json);
            // let json2 = jons1.replace(/\\n/g, '')
            // var parsedJSON = JSON.parse(JSON.parse(json2).string);
            this.resourceList = JSON.parse(res.string);
        });
    }
};
ResoursesVideosPage.ctorParameters = () => [
    { type: _ionic_native_streaming_media_ngx__WEBPACK_IMPORTED_MODULE_4__["StreamingMedia"] },
    { type: _global_services_service__WEBPACK_IMPORTED_MODULE_2__["GlobalServicesService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["MenuController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavController"] }
];
ResoursesVideosPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-resourses-videos',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./resourses-videos.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/resourses-videos/resourses-videos.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./resourses-videos.page.scss */ "./src/app/resourses-videos/resourses-videos.page.scss")).default]
    })
], ResoursesVideosPage);



/***/ })

}]);
//# sourceMappingURL=resourses-videos-resourses-videos-module-es2015.js.map