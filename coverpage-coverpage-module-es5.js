(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["coverpage-coverpage-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/coverpage/coverpage.page.html":
    /*!*************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/coverpage/coverpage.page.html ***!
      \*************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppCoverpageCoverpagePageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-content>\r\n    <ion-grid class=\"vertical-align\">\r\n        <ion-row>\r\n            <ion-col size=\"1\"></ion-col>\r\n            <ion-col ion-margin-horizontal size=\"10\">\r\n                <ion-img class=\"w-75 mx-auto ion-img-curve\" src=\"../../assets/image/logo-removebg.png\"></ion-img>\r\n            </ion-col>\r\n            <ion-col size=\"1\"></ion-col>\r\n        </ion-row>\r\n        <ion-row class=\"vertical-align\">\r\n            <ion-col align-self-center class=\"col-padd\">\r\n                <ion-button [routerLink]=\"['/login']\" expand=\"block\" color=\"btn-yellow\" size=\"large\" btn-yellow\r\n                    class=\"mt-15 btn-height\">Sign In\r\n                </ion-button>\r\n                <ion-button expand=\"block\" (click)=\"signupAlert()\" color=\"btn-transparent\" btn-transparent size=\"large\"\r\n                    class=\"mt-15 btn-height\">\r\n                    Sign Up\r\n                </ion-button>\r\n                <!-- <ion-button expand=\"block\" (click)=\"modalpop()\" color=\"btn-transparent\" btn-transparent size=\"large\"\r\n                    class=\"mt-15\">\r\n                    Test\r\n                </ion-button> -->\r\n            </ion-col>\r\n        </ion-row>\r\n    </ion-grid>\r\n</ion-content>";
      /***/
    },

    /***/
    "./src/app/coverpage/coverpage-routing.module.ts":
    /*!*******************************************************!*\
      !*** ./src/app/coverpage/coverpage-routing.module.ts ***!
      \*******************************************************/

    /*! exports provided: CoverpagePageRoutingModule */

    /***/
    function srcAppCoverpageCoverpageRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "CoverpagePageRoutingModule", function () {
        return CoverpagePageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _coverpage_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./coverpage.page */
      "./src/app/coverpage/coverpage.page.ts");

      var routes = [{
        path: '',
        component: _coverpage_page__WEBPACK_IMPORTED_MODULE_3__["CoverpagePage"]
      }];

      var CoverpagePageRoutingModule = function CoverpagePageRoutingModule() {
        _classCallCheck(this, CoverpagePageRoutingModule);
      };

      CoverpagePageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], CoverpagePageRoutingModule);
      /***/
    },

    /***/
    "./src/app/coverpage/coverpage.module.ts":
    /*!***********************************************!*\
      !*** ./src/app/coverpage/coverpage.module.ts ***!
      \***********************************************/

    /*! exports provided: CoverpagePageModule */

    /***/
    function srcAppCoverpageCoverpageModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "CoverpagePageModule", function () {
        return CoverpagePageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _coverpage_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./coverpage-routing.module */
      "./src/app/coverpage/coverpage-routing.module.ts");
      /* harmony import */


      var _coverpage_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./coverpage.page */
      "./src/app/coverpage/coverpage.page.ts");

      var CoverpagePageModule = function CoverpagePageModule() {
        _classCallCheck(this, CoverpagePageModule);
      };

      CoverpagePageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _coverpage_routing_module__WEBPACK_IMPORTED_MODULE_5__["CoverpagePageRoutingModule"]],
        declarations: [_coverpage_page__WEBPACK_IMPORTED_MODULE_6__["CoverpagePage"]]
      })], CoverpagePageModule);
      /***/
    },

    /***/
    "./src/app/coverpage/coverpage.page.scss":
    /*!***********************************************!*\
      !*** ./src/app/coverpage/coverpage.page.scss ***!
      \***********************************************/

    /*! exports provided: default */

    /***/
    function srcAppCoverpageCoverpagePageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "ion-content {\n  --background: linear-gradient(#00000099 100%, #00000099 100%), url('background.png') no-repeat center center / cover;\n}\n\n.alert-button-inner.sc-ion-alert-md {\n  color: #000;\n}\n\n.close {\n  position: absolute;\n  top: 10px;\n  right: 30px;\n  transition: all 200ms;\n  font-size: 30px;\n  font-weight: bold;\n  text-decoration: none;\n  color: #333;\n}\n\n.btn-height {\n  height: 45%;\n  border-radius: 5px;\n}\n\n.col-padd {\n  padding: 15px 25px;\n}\n\n.alert-custom-class1 ion-backdrop {\n  --backdrop-opacity: var(--ion-backdrop-opacity, 0.70);\n}\n\n.alert-custom-class1 .alert-wrapper {\n  background: var(--ion-color-light);\n  border-radius: 0px;\n  margin: 1rem;\n}\n\n.alert-custom-class1 .alert-wrapper .alert-sub-title {\n  color: black;\n  font-size: 14px;\n  padding-bottom: 100px;\n}\n\n.alert-custom-class1 .alert-wrapper .alert-message {\n  color: black;\n  text-align: center;\n  font-weight: 600;\n}\n\n.alert-custom-class1 .alert-wrapper .alert-button-group {\n  justify-content: center;\n}\n\n.alert-custom-class1 .alert-wrapper .warning {\n  background: var(--ion-color-yellow);\n  color: black;\n  text-transform: capitalize;\n  font-weight: 500;\n}\n\n.alert-custom-class1 .alert-wrapper .secondary {\n  background: var(--ion-color-dark);\n  color: white;\n  text-transform: capitalize;\n  font-weight: 500;\n}\n\n.alert-custom-class1 .alert-wrapper .Login {\n  top: 60px;\n  width: 100%;\n  position: absolute;\n  background: #ff9800;\n  color: black;\n  text-transform: capitalize;\n  font-weight: 500;\n  margin-top: 10px;\n}\n\n.alert-button-inner.sc-ion-alert-md {\n  color: unset;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY292ZXJwYWdlL2NvdmVycGFnZS5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDQSxvSEFBQTtBQUNBOztBQUVBO0VBQ0ksV0FBQTtBQUNKOztBQUVBO0VBQ0ksa0JBQUE7RUFDQSxTQUFBO0VBQ0EsV0FBQTtFQUNBLHFCQUFBO0VBQ0EsZUFBQTtFQUNBLGlCQUFBO0VBQ0EscUJBQUE7RUFDQSxXQUFBO0FBQ0o7O0FBRUE7RUFDSSxXQUFBO0VBQ0Esa0JBQUE7QUFDSjs7QUFDQTtFQUNJLGtCQUFBO0FBRUo7O0FBRUk7RUFDSSxxREFBQTtBQUNSOztBQUNJO0VBQ0ksa0NBQUE7RUFDQSxrQkFBQTtFQUNBLFlBQUE7QUFDUjs7QUFBUTtFQUNJLFlBQUE7RUFDRCxlQUFBO0VBQ0EscUJBQUE7QUFFWDs7QUFBUTtFQUNJLFlBQUE7RUFDQSxrQkFBQTtFQUVBLGdCQUFBO0FBQ1o7O0FBQ1E7RUFHSSx1QkFBQTtBQUNaOztBQUNRO0VBQ0ksbUNBQUE7RUFDQSxZQUFBO0VBQ0EsMEJBQUE7RUFDQSxnQkFBQTtBQUNaOztBQUNRO0VBQ0ksaUNBQUE7RUFDQSxZQUFBO0VBQ0EsMEJBQUE7RUFDQSxnQkFBQTtBQUNaOztBQUNRO0VBQ0ksU0FBQTtFQUNBLFdBQUE7RUFDQSxrQkFBQTtFQUNBLG1CQUFBO0VBQ0EsWUFBQTtFQUNBLDBCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxnQkFBQTtBQUNaOztBQWNJO0VBQ0ksWUFBQTtBQVhSIiwiZmlsZSI6InNyYy9hcHAvY292ZXJwYWdlL2NvdmVycGFnZS5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24tY29udGVudCB7XHJcbi0tYmFja2dyb3VuZDogbGluZWFyLWdyYWRpZW50KCMwMDAwMDA5OSAxMDAlLCAjMDAwMDAwOTkgMTAwJSksIHVybChcIi4uLy4uL2Fzc2V0cy9pbWFnZS9iYWNrZ3JvdW5kLnBuZ1wiKSBuby1yZXBlYXQgY2VudGVyIGNlbnRlciAvIGNvdmVyO1xyXG4gICBcclxufVxyXG4uYWxlcnQtYnV0dG9uLWlubmVyLnNjLWlvbi1hbGVydC1tZHtcclxuICAgIGNvbG9yOiMwMDA7XHJcbn1cclxuXHJcbi5jbG9zZSB7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICB0b3A6IDEwcHg7XHJcbiAgICByaWdodDogMzBweDtcclxuICAgIHRyYW5zaXRpb246IGFsbCAyMDBtcztcclxuICAgIGZvbnQtc2l6ZTogMzBweDtcclxuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xyXG4gICAgY29sb3I6ICMzMzM7XHJcbiAgfVxyXG5cclxuLmJ0bi1oZWlnaHQge1xyXG4gICAgaGVpZ2h0OiA0NSU7XHJcbiAgICBib3JkZXItcmFkaXVzOiA1cHg7XHJcbn1cclxuLmNvbC1wYWRkIHtcclxuICAgIHBhZGRpbmc6IDE1cHggMjVweDtcclxufVxyXG5cclxuLmFsZXJ0LWN1c3RvbS1jbGFzczEge1xyXG4gICAgaW9uLWJhY2tkcm9wIHtcclxuICAgICAgICAtLWJhY2tkcm9wLW9wYWNpdHk6IHZhcigtLWlvbi1iYWNrZHJvcC1vcGFjaXR5LCAwLjcwKTtcclxuICAgIH1cclxuICAgIC5hbGVydC13cmFwcGVyIHtcclxuICAgICAgICBiYWNrZ3JvdW5kOiB2YXIoLS1pb24tY29sb3ItbGlnaHQpO1xyXG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDBweDtcclxuICAgICAgICBtYXJnaW46IDFyZW07XHJcbiAgICAgICAgLmFsZXJ0LXN1Yi10aXRsZSB7XHJcbiAgICAgICAgICAgIGNvbG9yOiBibGFjaztcclxuICAgICAgICAgICBmb250LXNpemU6IDE0cHg7XHJcbiAgICAgICAgICAgcGFkZGluZy1ib3R0b206MTAwcHg7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC5hbGVydC1tZXNzYWdlIHtcclxuICAgICAgICAgICAgY29sb3I6IGJsYWNrO1xyXG4gICAgICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgICAgICAgIC8vIHRleHQtdHJhbnNmb3JtOiBjYXBpdGFsaXplO1xyXG4gICAgICAgICAgICBmb250LXdlaWdodDogNjAwO1xyXG4gICAgICAgIH1cclxuICAgICAgICAuYWxlcnQtYnV0dG9uLWdyb3VwIHtcclxuICAgICAgICAgICAgLXdlYmtpdC1ib3gtcGFjazogY2VudGVyO1xyXG4gICAgICAgICAgICAtbXMtZmxleC1wYWNrOiBjZW50ZXI7XHJcbiAgICAgICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgICAgIH1cclxuICAgICAgICAud2FybmluZyB7XHJcbiAgICAgICAgICAgIGJhY2tncm91bmQ6IHZhcigtLWlvbi1jb2xvci15ZWxsb3cpO1xyXG4gICAgICAgICAgICBjb2xvcjogYmxhY2s7XHJcbiAgICAgICAgICAgIHRleHQtdHJhbnNmb3JtOiBjYXBpdGFsaXplO1xyXG4gICAgICAgICAgICBmb250LXdlaWdodDogNTAwO1xyXG4gICAgICAgIH1cclxuICAgICAgICAuc2Vjb25kYXJ5IHtcclxuICAgICAgICAgICAgYmFja2dyb3VuZDogdmFyKC0taW9uLWNvbG9yLWRhcmspO1xyXG4gICAgICAgICAgICBjb2xvcjogd2hpdGU7XHJcbiAgICAgICAgICAgIHRleHQtdHJhbnNmb3JtOiBjYXBpdGFsaXplO1xyXG4gICAgICAgICAgICBmb250LXdlaWdodDogNTAwO1xyXG4gICAgICAgIH1cclxuICAgICAgICAuTG9naW57XHJcbiAgICAgICAgICAgIHRvcDogNjBweDtcclxuICAgICAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgICAgICAgICAgYmFja2dyb3VuZDogI2ZmOTgwMDtcclxuICAgICAgICAgICAgY29sb3I6IGJsYWNrO1xyXG4gICAgICAgICAgICB0ZXh0LXRyYW5zZm9ybTogY2FwaXRhbGl6ZTtcclxuICAgICAgICAgICAgZm9udC13ZWlnaHQ6IDUwMDtcclxuICAgICAgICAgICAgbWFyZ2luLXRvcDogMTBweDtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuXHJcbiAgICAvLyAuYWxlcnQtc3ViLXRpdGxlLnNjLWlvbi1hbGVydC1tZHtcclxuICAgIC8vICBmb250LXNpemU6IDE0cHg7XHJcbiAgICAvLyB9XHJcblxyXG4gICAgLy8gLmFsZXJ0LWhlYWQuc2MtaW9uLWFsZXJ0LW1kKy5hbGVydC1tZXNzYWdlLnNjLWlvbi1hbGVydC1tZCB7XHJcbiAgICAvLyAgICAgcGFkZGluZy10b3A6IHVuc2V0O1xyXG4gICAgLy8gfVxyXG4gICAgLy8gLmFsZXJ0LW1lc3NhZ2Uuc2MtaW9uLWFsZXJ0LWlvcywgLmFsZXJ0LWlucHV0LWdyb3VwLnNjLWlvbi1hbGVydC1pb3Mge1xyXG4gICAgLy8gICAgIHBhZGRpbmctdG9wOiAxNTBweDtcclxuICAgIC8vIH1cclxuICAgIC5hbGVydC1idXR0b24taW5uZXIuc2MtaW9uLWFsZXJ0LW1kIHtcclxuICAgICAgICBjb2xvcjogdW5zZXQ7XHJcbiAgICB9XHJcbiBcclxuLy8gLmlvbi1pbWctY3VydmV7XHJcbi8vICAgICBib3JkZXItcmFkaXVzOiA1cHggIWltcG9ydGFudDtcclxuLy8gfVxyXG4vLyBpb24taW1ne1xyXG4vLyAgICAgYm9yZGVyLXJhZGl1czogNXB4O1xyXG4vLyB9Il19 */";
      /***/
    },

    /***/
    "./src/app/coverpage/coverpage.page.ts":
    /*!*********************************************!*\
      !*** ./src/app/coverpage/coverpage.page.ts ***!
      \*********************************************/

    /*! exports provided: CoverpagePage */

    /***/
    function srcAppCoverpageCoverpagePageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "CoverpagePage", function () {
        return CoverpagePage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _global_services_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ../global-services.service */
      "./src/app/global-services.service.ts");

      var CoverpagePage = /*#__PURE__*/function () {
        function CoverpagePage(alertController, navCtrl, modalController, service) {
          _classCallCheck(this, CoverpagePage);

          this.alertController = alertController;
          this.navCtrl = navCtrl;
          this.modalController = modalController;
          this.service = service;
        }

        _createClass(CoverpagePage, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }, {
          key: "ionViewWillEnter",
          value: function ionViewWillEnter() {
            // this.service.checkGPSPermission();
            this.service.sendMessage('startNotificationCountFunction');
          }
        }, {
          key: "ionViewWillLeave",
          value: function ionViewWillLeave() {// this.service.checkGPSPermission();
          } // =====================PAGE FORWARD=====================

        }, {
          key: "signupAlert",
          value: function signupAlert() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
              var _this = this;

              var alert;
              return regeneratorRuntime.wrap(function _callee$(_context) {
                while (1) {
                  switch (_context.prev = _context.next) {
                    case 0:
                      _context.next = 2;
                      return this.alertController.create({
                        cssClass: 'alert-custom-class1',
                        // message: '<a class="close">&times;</a> <br><br> <img src="../../assets/image/logo.png" alt="g-maps" style="border-radius: 2px">',
                        message: 'Are you an alumni of NIFT?',
                        backdropDismiss: false,
                        header: '',
                        subHeader: '* NIFT students can sign in using CMS credentials. No need to sign up.',
                        buttons: [{
                          text: 'Login',
                          cssClass: 'Login',
                          handler: function handler() {
                            _this.navCtrl.navigateForward(['login']);
                          }
                        }, {
                          text: 'Yes',
                          cssClass: 'warning',
                          handler: function handler() {
                            _this.navCtrl.navigateForward(['aluminisignup']);
                          }
                        }, {
                          text: 'No',
                          role: 'cancel',
                          cssClass: 'secondary',
                          handler: function handler(blah) {
                            _this.navCtrl.navigateForward(['niftsignup']);
                          }
                        }, {
                          text: 'Dismiss',
                          role: 'dismiss',
                          cssClass: 'light',
                          handler: function handler() {}
                        }]
                      });

                    case 2:
                      alert = _context.sent;
                      _context.next = 5;
                      return alert.present();

                    case 5:
                    case "end":
                      return _context.stop();
                  }
                }
              }, _callee, this);
            }));
          }
        }]);

        return CoverpagePage;
      }();

      CoverpagePage.ctorParameters = function () {
        return [{
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"]
        }, {
          type: _global_services_service__WEBPACK_IMPORTED_MODULE_3__["GlobalServicesService"]
        }];
      };

      CoverpagePage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-coverpage',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./coverpage.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/coverpage/coverpage.page.html"))["default"],
        encapsulation: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewEncapsulation"].None,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./coverpage.page.scss */
        "./src/app/coverpage/coverpage.page.scss"))["default"]]
      })], CoverpagePage);
      /***/
    }
  }]);
})();
//# sourceMappingURL=coverpage-coverpage-module-es5.js.map