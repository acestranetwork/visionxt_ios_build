(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["forgot-password-forgot-password-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/forgot-password/forgot-password.page.html":
    /*!*************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/forgot-password/forgot-password.page.html ***!
      \*************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppForgotPasswordForgotPasswordPageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-header translucent no-border>\r\n    <ion-toolbar>\r\n        <ion-buttons slot=\"start\">\r\n            <ion-back-button defaultHref=\"/\"></ion-back-button>\r\n        </ion-buttons>\r\n        <ion-title>Forgot Password</ion-title>\r\n    </ion-toolbar>\r\n</ion-header>\r\n<ion-content>\r\n    <div class=\"inner-div\">\r\n        <form [formGroup]=\"UrNmeExistForm\" autocomplete=\"off\" #f=\"ngForm\" (ngSubmit)=\"ForgotPwdUserNameCheck()\"\r\n            *ngIf=\"!forgotPwdForm\">\r\n            <ion-grid>\r\n                <ion-row>\r\n                    <ion-col size=\"1\"></ion-col>\r\n                    <ion-col align-self-center>\r\n                        <ion-img src=\"../../assets/image/VidioNxtLogo.png\" class=\"w-45 mx-auto mt-3\"></ion-img>\r\n                    </ion-col>\r\n                    <ion-col size=\"1\"></ion-col>\r\n                </ion-row>\r\n                <ion-row class=\"mt-5\" *ngIf=\"!forgotPwdForm\">\r\n                    <ion-col align-self-center>\r\n                        <ion-item input-text-color>\r\n                            <ion-label color=\"light\" position=\"floating\">Username\r\n                            </ion-label>\r\n                            <ion-input formControlName=\"username\" type=\"text\" required></ion-input>\r\n                            <ion-icon name=\"person-outline\" color=\"light\" size=\"small\" class=\"ion-align-self-end\"\r\n                                slot=\"end\"></ion-icon>\r\n                        </ion-item>\r\n                        <div class=\"error\"\r\n                            *ngIf=\"UrNmeExistForm.controls['username'].errors?.required && (UrNmeExistForm.get('username').touched || f.submitted)\">\r\n                            Username is Required.</div>\r\n                        <div class=\"error\" *ngIf=\"UrNmeExistForm.controls['username'].errors?.username &&\r\n                                (UrNmeExistForm.get('username').touched || f.submitted)\">\r\n                            Username is Invalid.</div>\r\n                    </ion-col>\r\n                </ion-row>\r\n                <ion-row>\r\n                    <ion-col align-self-center>\r\n                        <ion-button type=\"submit\" expand=\"block\" color=\"btn-yellow btn-height\" size=\"large\" btn-yellow\r\n                            class=\"mt-15\">\r\n                            Send\r\n                        </ion-button>\r\n                    </ion-col>\r\n                </ion-row>\r\n            </ion-grid>\r\n        </form>\r\n\r\n        <form [formGroup]=\"resetPasswordFg\" autocomplete=\"off\" #f=\"ngForm\" (ngSubmit)=\"UpdatePwd()\"\r\n            *ngIf=\"forgotPwdForm\">\r\n            <ion-grid>\r\n                <ion-row>\r\n                    <ion-col size=\"1\"></ion-col>\r\n                    <ion-col align-self-center>\r\n                        <ion-img src=\"../../assets/image/VidioNxtLogo.png\" class=\"w-45 mx-auto mt-3\"></ion-img>\r\n                    </ion-col>\r\n                    <ion-col size=\"1\"></ion-col>\r\n                </ion-row>\r\n                <ion-row class=\"mt-5\">\r\n                    <ion-col align-self-center>\r\n                        <ion-item input-text-color>\r\n                            <ion-label color=\"light\" position=\"floating\">Username\r\n                            </ion-label>\r\n                            <ion-input formControlName=\"user_name\" type=\"text\" readonly required></ion-input>\r\n                            <ion-icon name=\"person-outline\" color=\"light\" size=\"small\" class=\"ion-align-self-end\"\r\n                                slot=\"end\"></ion-icon>\r\n                        </ion-item>\r\n                        <div class=\"error\"\r\n                            *ngIf=\"resetPasswordFg.controls['user_name'].errors?.required && (resetPasswordFg.get('user_name').touched || f.submitted)\">\r\n                            Username is Required.</div>\r\n                        <div class=\"error\" *ngIf=\"resetPasswordFg.controls['user_name'].errors?.user_name &&\r\n                                          (resetPasswordFg.get('user_name').touched || f.submitted)\">\r\n                            Username is Invalid.</div>\r\n                    </ion-col>\r\n                </ion-row>\r\n                <ion-row>\r\n                    <ion-col align-self-center>\r\n                        <ion-item input-text-color>\r\n                            <ion-label color=\"light\" position=\"floating\">Password\r\n                            </ion-label>\r\n                            <ion-input [type]=\"passwordType\" formControlName=\"pwd\" type=\"text\" required></ion-input>\r\n                            <ion-icon [name]=\"passwordIcon\" color=\"light\" size=\"small\" (click)='hideShowPassword()'\r\n                                class=\"ion-align-self-end\" slot=\"end\">\r\n                            </ion-icon>\r\n                        </ion-item>\r\n                        <div class=\"error\"\r\n                            *ngIf=\"resetPasswordFg.controls['pwd'].errors?.required && (resetPasswordFg.get('pwd').touched || f.submitted)\">\r\n                            Password is Required.</div>\r\n                        <div class=\"error\" *ngIf=\"resetPasswordFg.controls['pwd'].errors?.pwd &&\r\n                                          (resetPasswordFg.get('pwd').touched || f.submitted)\">\r\n                            Password is Invalid.</div>\r\n                    </ion-col>\r\n                </ion-row>\r\n                <ion-row>\r\n                    <ion-col align-self-center>\r\n                        <ion-item input-text-color>\r\n                            <ion-label color=\"light\" position=\"floating\">Confirm Password\r\n                            </ion-label>\r\n                            <ion-input [type]=\"cpasswordType\" formControlName=\"cnfm_pwd\" type=\"text\"\r\n                                (ionBlur)=\"MatchPassword()\" required></ion-input>\r\n                            <ion-icon [name]=\"cpasswordIcon\" color=\"light\" size=\"small\" (click)='hideShowcPassword()'\r\n                                class=\"ion-align-self-end\" slot=\"end\">\r\n                            </ion-icon>\r\n                        </ion-item>\r\n                        <div class=\"error\"\r\n                            *ngIf=\"resetPasswordFg.controls['cnfm_pwd'].errors?.required && (resetPasswordFg.get('cnfm_pwd').touched || f.submitted)\">\r\n                            Confirm Password is Required.</div>\r\n                        <div class=\"error\" *ngIf=\"resetPasswordFg.controls['cnfm_pwd'].errors?.cnfm_pwd &&\r\n                                          (resetPasswordFg.get('cnfm_pwd').touched || f.submitted)\">\r\n                            Confirm Password is Invalid.</div>\r\n                        <!-- <p class=\"mt-3 error\">{{passwordIsNotSame}}</p> -->\r\n                        <div class=\"error\" *ngIf=\"cpwdErrMessage &&\r\n                                                      (resetPasswordFg.get('cnfm_pwd').touched || f.submitted)\">\r\n                            {{cpwdErrMessage}}</div>\r\n                    </ion-col>\r\n                </ion-row>\r\n                <ion-row>\r\n                    <ion-col align-self-center class=\"col-padd\">\r\n                        <ion-button type=\"submit\" expand=\"block\" color=\"btn-yellow btn-height\" size=\"large\" btn-yellow\r\n                            class=\"mt-15\">\r\n                            Send\r\n                        </ion-button>\r\n                    </ion-col>\r\n                </ion-row>\r\n            </ion-grid>\r\n        </form>\r\n    </div>\r\n</ion-content>";
      /***/
    },

    /***/
    "./src/app/forgot-password/forgot-password-routing.module.ts":
    /*!*******************************************************************!*\
      !*** ./src/app/forgot-password/forgot-password-routing.module.ts ***!
      \*******************************************************************/

    /*! exports provided: ForgotPasswordPageRoutingModule */

    /***/
    function srcAppForgotPasswordForgotPasswordRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ForgotPasswordPageRoutingModule", function () {
        return ForgotPasswordPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _forgot_password_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./forgot-password.page */
      "./src/app/forgot-password/forgot-password.page.ts");

      var routes = [{
        path: '',
        component: _forgot_password_page__WEBPACK_IMPORTED_MODULE_3__["ForgotPasswordPage"]
      }];

      var ForgotPasswordPageRoutingModule = function ForgotPasswordPageRoutingModule() {
        _classCallCheck(this, ForgotPasswordPageRoutingModule);
      };

      ForgotPasswordPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], ForgotPasswordPageRoutingModule);
      /***/
    },

    /***/
    "./src/app/forgot-password/forgot-password.module.ts":
    /*!***********************************************************!*\
      !*** ./src/app/forgot-password/forgot-password.module.ts ***!
      \***********************************************************/

    /*! exports provided: ForgotPasswordPageModule */

    /***/
    function srcAppForgotPasswordForgotPasswordModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ForgotPasswordPageModule", function () {
        return ForgotPasswordPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _forgot_password_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./forgot-password-routing.module */
      "./src/app/forgot-password/forgot-password-routing.module.ts");
      /* harmony import */


      var _forgot_password_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./forgot-password.page */
      "./src/app/forgot-password/forgot-password.page.ts");

      var ForgotPasswordPageModule = function ForgotPasswordPageModule() {
        _classCallCheck(this, ForgotPasswordPageModule);
      };

      ForgotPasswordPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _forgot_password_routing_module__WEBPACK_IMPORTED_MODULE_5__["ForgotPasswordPageRoutingModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"]],
        declarations: [_forgot_password_page__WEBPACK_IMPORTED_MODULE_6__["ForgotPasswordPage"]]
      })], ForgotPasswordPageModule);
      /***/
    },

    /***/
    "./src/app/forgot-password/forgot-password.page.scss":
    /*!***********************************************************!*\
      !*** ./src/app/forgot-password/forgot-password.page.scss ***!
      \***********************************************************/

    /*! exports provided: default */

    /***/
    function srcAppForgotPasswordForgotPasswordPageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = ".error {\n  color: var(--ion-color-yellow);\n  font-size: 12px;\n}\n\n.btn-height {\n  height: 85%;\n  border-radius: 10px;\n}\n\n.col-padd {\n  padding: 15px 25px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvZm9yZ290LXBhc3N3b3JkL2ZvcmdvdC1wYXNzd29yZC5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSw4QkFBQTtFQUNBLGVBQUE7QUFDSjs7QUFFQTtFQUNJLFdBQUE7RUFDQSxtQkFBQTtBQUNKOztBQUNBO0VBQ0ksa0JBQUE7QUFFSiIsImZpbGUiOiJzcmMvYXBwL2ZvcmdvdC1wYXNzd29yZC9mb3Jnb3QtcGFzc3dvcmQucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmVycm9yIHtcclxuICAgIGNvbG9yOiB2YXIoLS1pb24tY29sb3IteWVsbG93KTsvLyNkMzllMDA7XHJcbiAgICBmb250LXNpemU6IDEycHg7XHJcbn1cclxuXHJcbi5idG4taGVpZ2h0IHtcclxuICAgIGhlaWdodDogODUlO1xyXG4gICAgYm9yZGVyLXJhZGl1czogMTBweDtcclxufVxyXG4uY29sLXBhZGQge1xyXG4gICAgcGFkZGluZzogMTVweCAyNXB4O1xyXG59Il19 */";
      /***/
    },

    /***/
    "./src/app/forgot-password/forgot-password.page.ts":
    /*!*********************************************************!*\
      !*** ./src/app/forgot-password/forgot-password.page.ts ***!
      \*********************************************************/

    /*! exports provided: ForgotPasswordPage */

    /***/
    function srcAppForgotPasswordForgotPasswordPageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ForgotPasswordPage", function () {
        return ForgotPasswordPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _global_services_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ../global-services.service */
      "./src/app/global-services.service.ts");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");

      var ForgotPasswordPage = /*#__PURE__*/function () {
        function ForgotPasswordPage(alertController, navCtrl, toastController, service) {
          var _this = this;

          _classCallCheck(this, ForgotPasswordPage);

          this.alertController = alertController;
          this.navCtrl = navCtrl;
          this.toastController = toastController;
          this.service = service;
          this.forgotPwdForm = false;

          this.MatchPassword = function () {
            var passwordControl = _this.resetPasswordFg.controls['pwd'].value;
            var confirmPasswordControl = _this.resetPasswordFg.controls['cnfm_pwd'].value;

            if (passwordControl !== confirmPasswordControl) {
              _this.cpwdErrMessage = "Password and confirmation password must be identical.";
            } else {
              _this.cpwdErrMessage = undefined;
            }
          }; // =====================End of Confirm Password Validation=====================
          // =====================HIDE PASSWORD=====================


          this.passwordType = 'password';
          this.passwordIcon = 'eye-off';
          this.cpasswordType = 'password';
          this.cpasswordIcon = 'eye-off';
        }

        _createClass(ForgotPasswordPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            this.formInit();
            this.UrNmeExistFormInt();
          } // =====================FORM INIT=====================

        }, {
          key: "formInit",
          value: function formInit() {
            this.resetPasswordFg = new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormGroup"]({
              'user_name': new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](null, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required),
              'pwd': new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](null, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required),
              'cnfm_pwd': new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](null, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required)
            });
          } // =====================UrNmeExistForm INIT=====================

        }, {
          key: "UrNmeExistFormInt",
          value: function UrNmeExistFormInt() {
            this.UrNmeExistForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormGroup"]({
              'username': new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](null, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required)
            });
          } //ForgotPwdUerNameCheck 

        }, {
          key: "ForgotPwdUserNameCheck",
          value: function ForgotPwdUserNameCheck() {
            var _this2 = this;

            if (this.UrNmeExistForm.valid) {
              this.service.post_data("getUsrNameExist", this.UrNmeExistForm.value).subscribe(function (result) {
                var json = _this2.service.parserToJSON(result);

                _this2.UrNmeExistCheck = JSON.parse(json.string);

                if (_this2.UrNmeExistCheck[0].Status == "EXIST") {
                  _this2.presentUrExistToast(_this2.UrNmeExistCheck[0].Message);

                  _this2.forgotPwdForm = true;

                  _this2.resetPasswordFg.get("user_name").setValue(_this2.UrNmeExistForm.value.username); // this.UpdatePwd();

                }

                if (_this2.UrNmeExistCheck[0].Status == "NOTEXIST") {
                  _this2.presentUrExistToast(_this2.UrNmeExistCheck[0].Message);
                }
              }, function (error) {
                console.log(error);
              });
            } else {}
          }
        }, {
          key: "UpdatePwd",
          value: function UpdatePwd() {
            var _this3 = this;

            if (this.resetPasswordFg.valid) {
              if (this.resetPasswordFg.value.pwd == this.resetPasswordFg.value.cnfm_pwd) {
                this.service.post_data("updateUsernamePwd", this.resetPasswordFg.value).subscribe(function (result) {
                  var json = _this3.service.parserToJSON(result);

                  _this3.ForgotPwdUrCheck = JSON.parse(json.string);

                  if (_this3.ForgotPwdUrCheck[0].Status == "SUCCESS") {
                    _this3.presentToast(_this3.ForgotPwdUrCheck[0].Message);

                    _this3.resetForm();

                    _this3.navCtrl.navigateRoot(['/login']); // this.ForgotPwdUrCheckAlert(this.ForgotPwdUrCheck[0].Message);

                  } else {}
                }, function (error) {
                  console.log(error);
                });
              } else {
                this.presentToast('Password and confirmation password must be identical');
              }
            } else {}
          }
        }, {
          key: "presentUrExistToast",
          value: function presentUrExistToast(message_content) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
              var toast;
              return regeneratorRuntime.wrap(function _callee$(_context) {
                while (1) {
                  switch (_context.prev = _context.next) {
                    case 0:
                      _context.next = 2;
                      return this.toastController.create({
                        message: message_content,
                        cssClass: 'danger',
                        position: 'top',
                        duration: 2000
                      });

                    case 2:
                      toast = _context.sent;
                      toast.present();

                    case 4:
                    case "end":
                      return _context.stop();
                  }
                }
              }, _callee, this);
            }));
          }
        }, {
          key: "presentToast",
          value: function presentToast(message_content) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
              var toast;
              return regeneratorRuntime.wrap(function _callee2$(_context2) {
                while (1) {
                  switch (_context2.prev = _context2.next) {
                    case 0:
                      _context2.next = 2;
                      return this.toastController.create({
                        message: message_content,
                        cssClass: 'danger',
                        position: 'top',
                        duration: 5000
                      });

                    case 2:
                      toast = _context2.sent;
                      toast.present();

                    case 4:
                    case "end":
                      return _context2.stop();
                  }
                }
              }, _callee2, this);
            }));
          } // =====================PAGE FORWARD=====================

        }, {
          key: "ForgotPwdUrCheckAlert",
          value: function ForgotPwdUrCheckAlert(message_content) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee3() {
              var alert;
              return regeneratorRuntime.wrap(function _callee3$(_context3) {
                while (1) {
                  switch (_context3.prev = _context3.next) {
                    case 0:
                      _context3.next = 2;
                      return this.alertController.create({
                        cssClass: 'alert-custom-class',
                        message: message_content,
                        backdropDismiss: false,
                        buttons: [{
                          text: 'Ok',
                          cssClass: 'warning',
                          handler: function handler() {}
                        }]
                      });

                    case 2:
                      alert = _context3.sent;
                      _context3.next = 5;
                      return alert.present();

                    case 5:
                    case "end":
                      return _context3.stop();
                  }
                }
              }, _callee3, this);
            }));
          } // =====================RESET FORM=====================

        }, {
          key: "resetForm",
          value: function resetForm() {
            this.resetPasswordFg.reset();
            this.UrNmeExistForm.reset();
          }
        }, {
          key: "hideShowPassword",
          value: function hideShowPassword() {
            this.passwordType = this.passwordType === 'text' ? 'password' : 'text';
            this.passwordIcon = this.passwordIcon === 'eye-off' ? 'eye' : 'eye-off';
          }
        }, {
          key: "hideShowcPassword",
          value: function hideShowcPassword() {
            this.cpasswordType = this.cpasswordType === 'text' ? 'password' : 'text';
            this.cpasswordIcon = this.cpasswordIcon === 'eye-off' ? 'eye' : 'eye-off';
          } // =====================end of HIDE PASSWORD=====================
          // =====================PAGE FORWARD=====================

        }, {
          key: "forgotPasswordAlert",
          value: function forgotPasswordAlert() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee4() {
              var alert;
              return regeneratorRuntime.wrap(function _callee4$(_context4) {
                while (1) {
                  switch (_context4.prev = _context4.next) {
                    case 0:
                      _context4.next = 2;
                      return this.alertController.create({
                        cssClass: 'alert-custom-class',
                        message: 'Reset password link has been sent to your email.',
                        backdropDismiss: false,
                        buttons: [{
                          text: 'Ok',
                          cssClass: 'warning',
                          handler: function handler() {}
                        }]
                      });

                    case 2:
                      alert = _context4.sent;
                      _context4.next = 5;
                      return alert.present();

                    case 5:
                    case "end":
                      return _context4.stop();
                  }
                }
              }, _callee4, this);
            }));
          }
        }]);

        return ForgotPasswordPage;
      }();

      ForgotPasswordPage.ctorParameters = function () {
        return [{
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"]
        }, {
          type: _global_services_service__WEBPACK_IMPORTED_MODULE_3__["GlobalServicesService"]
        }];
      };

      ForgotPasswordPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-forgot-password',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./forgot-password.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/forgot-password/forgot-password.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./forgot-password.page.scss */
        "./src/app/forgot-password/forgot-password.page.scss"))["default"]]
      })], ForgotPasswordPage);
      /***/
    }
  }]);
})();
//# sourceMappingURL=forgot-password-forgot-password-module-es5.js.map