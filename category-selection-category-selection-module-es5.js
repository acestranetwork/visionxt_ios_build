(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["category-selection-category-selection-module"], {
    /***/
    "./node_modules/ngx-color-picker/__ivy_ngcc__/fesm2015/ngx-color-picker.js":
    /*!*********************************************************************************!*\
      !*** ./node_modules/ngx-color-picker/__ivy_ngcc__/fesm2015/ngx-color-picker.js ***!
      \*********************************************************************************/

    /*! exports provided: Cmyk, ColorPickerComponent, ColorPickerDirective, ColorPickerModule, ColorPickerService, Hsla, Hsva, Rgba, SliderDirective, TextDirective */

    /***/
    function node_modulesNgxColorPicker__ivy_ngcc__Fesm2015NgxColorPickerJs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "Cmyk", function () {
        return Cmyk;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ColorPickerComponent", function () {
        return ColorPickerComponent;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ColorPickerDirective", function () {
        return ColorPickerDirective;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ColorPickerModule", function () {
        return ColorPickerModule;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ColorPickerService", function () {
        return ColorPickerService;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "Hsla", function () {
        return Hsla;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "Hsva", function () {
        return Hsva;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "Rgba", function () {
        return Rgba;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "SliderDirective", function () {
        return SliderDirective;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "TextDirective", function () {
        return TextDirective;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");

      var _c0 = ["dialogPopup"];
      var _c1 = ["hueSlider"];
      var _c2 = ["alphaSlider"];

      function ColorPickerComponent_div_2_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "div");
        }

        if (rf & 2) {
          var ctx_r1 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵclassMapInterpolate1"]("arrow arrow-", ctx_r1.cpUsePosition, "");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵstyleProp"]("top", ctx_r1.arrowTop, "px");
        }
      }

      function ColorPickerComponent_div_3_Template(rf, ctx) {
        if (rf & 1) {
          var _r17 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 26);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("newValue", function ColorPickerComponent_div_3_Template_div_newValue_0_listener($event) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r17);

            var ctx_r16 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

            return ctx_r16.onColorChange($event);
          })("dragStart", function ColorPickerComponent_div_3_Template_div_dragStart_0_listener() {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r17);

            var ctx_r18 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

            return ctx_r18.onDragStart("saturation-lightness");
          })("dragEnd", function ColorPickerComponent_div_3_Template_div_dragEnd_0_listener() {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r17);

            var ctx_r19 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

            return ctx_r19.onDragEnd("saturation-lightness");
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "div", 13);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var ctx_r2 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵstyleProp"]("background-color", ctx_r2.hueSliderColor);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("rgX", 1)("rgY", 1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵstyleProp"]("top", ctx_r2.slider == null ? null : ctx_r2.slider.v, "px")("left", ctx_r2.slider == null ? null : ctx_r2.slider.s, "px");
        }
      }

      function ColorPickerComponent_button_8_Template(rf, ctx) {
        if (rf & 1) {
          var _r21 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "button", 27);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function ColorPickerComponent_button_8_Template_button_click_0_listener($event) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r21);

            var ctx_r20 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

            return ctx_r20.onAddPresetColor($event, ctx_r20.selectedColor);
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var ctx_r3 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵclassMap"](ctx_r3.cpAddColorButtonClass);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("disabled", ctx_r3.cpPresetColors && ctx_r3.cpPresetColors.length >= ctx_r3.cpMaxPresetColorsLength);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", ctx_r3.cpAddColorButtonText, " ");
        }
      }

      function ColorPickerComponent_div_10_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "div", 28);
        }
      }

      function ColorPickerComponent_div_20_input_6_Template(rf, ctx) {
        if (rf & 1) {
          var _r25 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "input", 34);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("keyup.enter", function ColorPickerComponent_div_20_input_6_Template_input_keyup_enter_0_listener($event) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r25);

            var ctx_r24 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);

            return ctx_r24.onAcceptColor($event);
          })("newValue", function ColorPickerComponent_div_20_input_6_Template_input_newValue_0_listener($event) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r25);

            var ctx_r26 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);

            return ctx_r26.onAlphaInput($event);
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var ctx_r22 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("rg", 1)("value", ctx_r22.cmykText == null ? null : ctx_r22.cmykText.a);
        }
      }

      function ColorPickerComponent_div_20_div_16_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, "A");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }
      }

      function ColorPickerComponent_div_20_Template(rf, ctx) {
        if (rf & 1) {
          var _r28 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 29);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 30);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "input", 31);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("keyup.enter", function ColorPickerComponent_div_20_Template_input_keyup_enter_2_listener($event) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r28);

            var ctx_r27 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

            return ctx_r27.onAcceptColor($event);
          })("newValue", function ColorPickerComponent_div_20_Template_input_newValue_2_listener($event) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r28);

            var ctx_r29 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

            return ctx_r29.onCyanInput($event);
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "input", 31);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("keyup.enter", function ColorPickerComponent_div_20_Template_input_keyup_enter_3_listener($event) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r28);

            var ctx_r30 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

            return ctx_r30.onAcceptColor($event);
          })("newValue", function ColorPickerComponent_div_20_Template_input_newValue_3_listener($event) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r28);

            var ctx_r31 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

            return ctx_r31.onMagentaInput($event);
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "input", 31);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("keyup.enter", function ColorPickerComponent_div_20_Template_input_keyup_enter_4_listener($event) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r28);

            var ctx_r32 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

            return ctx_r32.onAcceptColor($event);
          })("newValue", function ColorPickerComponent_div_20_Template_input_newValue_4_listener($event) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r28);

            var ctx_r33 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

            return ctx_r33.onYellowInput($event);
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "input", 31);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("keyup.enter", function ColorPickerComponent_div_20_Template_input_keyup_enter_5_listener($event) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r28);

            var ctx_r34 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

            return ctx_r34.onAcceptColor($event);
          })("newValue", function ColorPickerComponent_div_20_Template_input_newValue_5_listener($event) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r28);

            var ctx_r35 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

            return ctx_r35.onBlackInput($event);
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](6, ColorPickerComponent_div_20_input_6_Template, 1, 2, "input", 32);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "div", 30);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "div");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](9, "C");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "div");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](11, "M");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "div");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](13, "Y");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "div");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](15, "K");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](16, ColorPickerComponent_div_20_div_16_Template, 2, 0, "div", 33);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var ctx_r8 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵstyleProp"]("display", ctx_r8.format !== 3 ? "none" : "block");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("rg", 100)("value", ctx_r8.cmykText == null ? null : ctx_r8.cmykText.c);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("rg", 100)("value", ctx_r8.cmykText == null ? null : ctx_r8.cmykText.m);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("rg", 100)("value", ctx_r8.cmykText == null ? null : ctx_r8.cmykText.y);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("rg", 100)("value", ctx_r8.cmykText == null ? null : ctx_r8.cmykText.k);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r8.cpAlphaChannel !== "disabled");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](10);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r8.cpAlphaChannel !== "disabled");
        }
      }

      function ColorPickerComponent_div_21_input_5_Template(rf, ctx) {
        if (rf & 1) {
          var _r39 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "input", 34);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("keyup.enter", function ColorPickerComponent_div_21_input_5_Template_input_keyup_enter_0_listener($event) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r39);

            var ctx_r38 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);

            return ctx_r38.onAcceptColor($event);
          })("newValue", function ColorPickerComponent_div_21_input_5_Template_input_newValue_0_listener($event) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r39);

            var ctx_r40 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);

            return ctx_r40.onAlphaInput($event);
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var ctx_r36 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("rg", 1)("value", ctx_r36.hslaText == null ? null : ctx_r36.hslaText.a);
        }
      }

      function ColorPickerComponent_div_21_div_13_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, "A");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }
      }

      function ColorPickerComponent_div_21_Template(rf, ctx) {
        if (rf & 1) {
          var _r42 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 35);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 30);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "input", 36);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("keyup.enter", function ColorPickerComponent_div_21_Template_input_keyup_enter_2_listener($event) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r42);

            var ctx_r41 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

            return ctx_r41.onAcceptColor($event);
          })("newValue", function ColorPickerComponent_div_21_Template_input_newValue_2_listener($event) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r42);

            var ctx_r43 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

            return ctx_r43.onHueInput($event);
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "input", 31);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("keyup.enter", function ColorPickerComponent_div_21_Template_input_keyup_enter_3_listener($event) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r42);

            var ctx_r44 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

            return ctx_r44.onAcceptColor($event);
          })("newValue", function ColorPickerComponent_div_21_Template_input_newValue_3_listener($event) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r42);

            var ctx_r45 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

            return ctx_r45.onSaturationInput($event);
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "input", 31);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("keyup.enter", function ColorPickerComponent_div_21_Template_input_keyup_enter_4_listener($event) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r42);

            var ctx_r46 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

            return ctx_r46.onAcceptColor($event);
          })("newValue", function ColorPickerComponent_div_21_Template_input_newValue_4_listener($event) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r42);

            var ctx_r47 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

            return ctx_r47.onLightnessInput($event);
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](5, ColorPickerComponent_div_21_input_5_Template, 1, 2, "input", 32);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "div", 30);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "div");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8, "H");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "div");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](10, "S");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "div");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](12, "L");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](13, ColorPickerComponent_div_21_div_13_Template, 2, 0, "div", 33);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var ctx_r9 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵstyleProp"]("display", ctx_r9.format !== 2 ? "none" : "block");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("rg", 360)("value", ctx_r9.hslaText == null ? null : ctx_r9.hslaText.h);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("rg", 100)("value", ctx_r9.hslaText == null ? null : ctx_r9.hslaText.s);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("rg", 100)("value", ctx_r9.hslaText == null ? null : ctx_r9.hslaText.l);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r9.cpAlphaChannel !== "disabled");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](8);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r9.cpAlphaChannel !== "disabled");
        }
      }

      function ColorPickerComponent_div_22_input_5_Template(rf, ctx) {
        if (rf & 1) {
          var _r51 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "input", 34);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("keyup.enter", function ColorPickerComponent_div_22_input_5_Template_input_keyup_enter_0_listener($event) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r51);

            var ctx_r50 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);

            return ctx_r50.onAcceptColor($event);
          })("newValue", function ColorPickerComponent_div_22_input_5_Template_input_newValue_0_listener($event) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r51);

            var ctx_r52 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);

            return ctx_r52.onAlphaInput($event);
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var ctx_r48 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("rg", 1)("value", ctx_r48.rgbaText == null ? null : ctx_r48.rgbaText.a);
        }
      }

      function ColorPickerComponent_div_22_div_13_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, "A");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }
      }

      function ColorPickerComponent_div_22_Template(rf, ctx) {
        if (rf & 1) {
          var _r54 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 37);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 30);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "input", 38);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("keyup.enter", function ColorPickerComponent_div_22_Template_input_keyup_enter_2_listener($event) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r54);

            var ctx_r53 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

            return ctx_r53.onAcceptColor($event);
          })("newValue", function ColorPickerComponent_div_22_Template_input_newValue_2_listener($event) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r54);

            var ctx_r55 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

            return ctx_r55.onRedInput($event);
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "input", 38);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("keyup.enter", function ColorPickerComponent_div_22_Template_input_keyup_enter_3_listener($event) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r54);

            var ctx_r56 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

            return ctx_r56.onAcceptColor($event);
          })("newValue", function ColorPickerComponent_div_22_Template_input_newValue_3_listener($event) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r54);

            var ctx_r57 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

            return ctx_r57.onGreenInput($event);
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "input", 38);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("keyup.enter", function ColorPickerComponent_div_22_Template_input_keyup_enter_4_listener($event) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r54);

            var ctx_r58 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

            return ctx_r58.onAcceptColor($event);
          })("newValue", function ColorPickerComponent_div_22_Template_input_newValue_4_listener($event) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r54);

            var ctx_r59 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

            return ctx_r59.onBlueInput($event);
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](5, ColorPickerComponent_div_22_input_5_Template, 1, 2, "input", 32);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "div", 30);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "div");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8, "R");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "div");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](10, "G");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "div");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](12, "B");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](13, ColorPickerComponent_div_22_div_13_Template, 2, 0, "div", 33);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var ctx_r10 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵstyleProp"]("display", ctx_r10.format !== 1 ? "none" : "block");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("rg", 255)("value", ctx_r10.rgbaText == null ? null : ctx_r10.rgbaText.r);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("rg", 255)("value", ctx_r10.rgbaText == null ? null : ctx_r10.rgbaText.g);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("rg", 255)("value", ctx_r10.rgbaText == null ? null : ctx_r10.rgbaText.b);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r10.cpAlphaChannel !== "disabled");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](8);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r10.cpAlphaChannel !== "disabled");
        }
      }

      function ColorPickerComponent_div_23_input_3_Template(rf, ctx) {
        if (rf & 1) {
          var _r63 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "input", 34);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("keyup.enter", function ColorPickerComponent_div_23_input_3_Template_input_keyup_enter_0_listener($event) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r63);

            var ctx_r62 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);

            return ctx_r62.onAcceptColor($event);
          })("newValue", function ColorPickerComponent_div_23_input_3_Template_input_newValue_0_listener($event) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r63);

            var ctx_r64 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);

            return ctx_r64.onAlphaInput($event);
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var ctx_r60 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("rg", 1)("value", ctx_r60.hexAlpha);
        }
      }

      function ColorPickerComponent_div_23_div_7_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, "A");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }
      }

      function ColorPickerComponent_div_23_Template(rf, ctx) {
        if (rf & 1) {
          var _r66 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 39);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 30);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "input", 40);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("blur", function ColorPickerComponent_div_23_Template_input_blur_2_listener() {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r66);

            var ctx_r65 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

            return ctx_r65.onHexInput(null);
          })("keyup.enter", function ColorPickerComponent_div_23_Template_input_keyup_enter_2_listener($event) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r66);

            var ctx_r67 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

            return ctx_r67.onAcceptColor($event);
          })("newValue", function ColorPickerComponent_div_23_Template_input_newValue_2_listener($event) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r66);

            var ctx_r68 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

            return ctx_r68.onHexInput($event);
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](3, ColorPickerComponent_div_23_input_3_Template, 1, 2, "input", 32);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "div", 30);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "div");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6, "Hex");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](7, ColorPickerComponent_div_23_div_7_Template, 2, 0, "div", 33);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var ctx_r11 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵstyleProp"]("display", ctx_r11.format !== 0 ? "none" : "block");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵclassProp"]("hex-alpha", ctx_r11.cpAlphaChannel === "forced");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("value", ctx_r11.hexText);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r11.cpAlphaChannel === "forced");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r11.cpAlphaChannel === "forced");
        }
      }

      function ColorPickerComponent_div_24_input_3_Template(rf, ctx) {
        if (rf & 1) {
          var _r71 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "input", 34);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("keyup.enter", function ColorPickerComponent_div_24_input_3_Template_input_keyup_enter_0_listener($event) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r71);

            var ctx_r70 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);

            return ctx_r70.onAcceptColor($event);
          })("newValue", function ColorPickerComponent_div_24_input_3_Template_input_newValue_0_listener($event) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r71);

            var ctx_r72 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);

            return ctx_r72.onAlphaInput($event);
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var ctx_r69 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("rg", 1)("value", ctx_r69.hslaText == null ? null : ctx_r69.hslaText.a);
        }
      }

      function ColorPickerComponent_div_24_Template(rf, ctx) {
        if (rf & 1) {
          var _r74 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 41);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 30);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "input", 31);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("keyup.enter", function ColorPickerComponent_div_24_Template_input_keyup_enter_2_listener($event) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r74);

            var ctx_r73 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

            return ctx_r73.onAcceptColor($event);
          })("newValue", function ColorPickerComponent_div_24_Template_input_newValue_2_listener($event) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r74);

            var ctx_r75 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

            return ctx_r75.onValueInput($event);
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](3, ColorPickerComponent_div_24_input_3_Template, 1, 2, "input", 32);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "div", 30);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "div");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6, "V");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "div");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8, "A");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var ctx_r12 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("rg", 100)("value", ctx_r12.hslaText == null ? null : ctx_r12.hslaText.l);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r12.cpAlphaChannel !== "disabled");
        }
      }

      function ColorPickerComponent_div_25_Template(rf, ctx) {
        if (rf & 1) {
          var _r77 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 42);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "span", 43);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function ColorPickerComponent_div_25_Template_span_click_1_listener() {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r77);

            var ctx_r76 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

            return ctx_r76.onFormatToggle(-1);
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "span", 43);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function ColorPickerComponent_div_25_Template_span_click_2_listener() {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r77);

            var ctx_r78 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

            return ctx_r78.onFormatToggle(1);
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }
      }

      function ColorPickerComponent_div_26_div_4_div_1_span_1_Template(rf, ctx) {
        if (rf & 1) {
          var _r85 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "span", 50);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function ColorPickerComponent_div_26_div_4_div_1_span_1_Template_span_click_0_listener($event) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r85);

            var color_r82 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]().$implicit;

            var ctx_r84 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](3);

            return ctx_r84.onRemovePresetColor($event, color_r82);
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var ctx_r83 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵclassMap"](ctx_r83.cpRemoveColorButtonClass);
        }
      }

      function ColorPickerComponent_div_26_div_4_div_1_Template(rf, ctx) {
        if (rf & 1) {
          var _r88 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 48);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function ColorPickerComponent_div_26_div_4_div_1_Template_div_click_0_listener() {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r88);

            var color_r82 = ctx.$implicit;

            var ctx_r87 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](3);

            return ctx_r87.setColorFromString(color_r82);
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, ColorPickerComponent_div_26_div_4_div_1_span_1_Template, 1, 3, "span", 49);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var color_r82 = ctx.$implicit;

          var ctx_r81 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](3);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵstyleProp"]("background-color", color_r82);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r81.cpAddColorButton);
        }
      }

      function ColorPickerComponent_div_26_div_4_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, ColorPickerComponent_div_26_div_4_div_1_Template, 2, 3, "div", 47);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var ctx_r79 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵclassMap"](ctx_r79.cpPresetColorsClass);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx_r79.cpPresetColors);
        }
      }

      function ColorPickerComponent_div_26_div_5_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var ctx_r80 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵclassMap"](ctx_r80.cpPresetEmptyMessageClass);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx_r80.cpPresetEmptyMessage);
        }
      }

      function ColorPickerComponent_div_26_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 44);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "hr");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 45);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](4, ColorPickerComponent_div_26_div_4_Template, 2, 4, "div", 46);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](5, ColorPickerComponent_div_26_div_5_Template, 2, 4, "div", 46);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var ctx_r14 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx_r14.cpPresetLabel);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r14.cpPresetColors == null ? null : ctx_r14.cpPresetColors.length);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", !(ctx_r14.cpPresetColors == null ? null : ctx_r14.cpPresetColors.length) && ctx_r14.cpAddColorButton);
        }
      }

      function ColorPickerComponent_div_27_button_1_Template(rf, ctx) {
        if (rf & 1) {
          var _r92 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "button", 53);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function ColorPickerComponent_div_27_button_1_Template_button_click_0_listener($event) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r92);

            var ctx_r91 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);

            return ctx_r91.onCancelColor($event);
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var ctx_r89 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵclassMap"](ctx_r89.cpCancelButtonClass);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx_r89.cpCancelButtonText);
        }
      }

      function ColorPickerComponent_div_27_button_2_Template(rf, ctx) {
        if (rf & 1) {
          var _r94 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "button", 53);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function ColorPickerComponent_div_27_button_2_Template_button_click_0_listener($event) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r94);

            var ctx_r93 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);

            return ctx_r93.onAcceptColor($event);
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var ctx_r90 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵclassMap"](ctx_r90.cpOKButtonClass);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx_r90.cpOKButtonText);
        }
      }

      function ColorPickerComponent_div_27_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 51);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, ColorPickerComponent_div_27_button_1_Template, 2, 4, "button", 52);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](2, ColorPickerComponent_div_27_button_2_Template, 2, 4, "button", 52);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var ctx_r15 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r15.cpCancelButton);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r15.cpOKButton);
        }
      }

      var ColorFormats;

      (function (ColorFormats) {
        ColorFormats[ColorFormats["HEX"] = 0] = "HEX";
        ColorFormats[ColorFormats["RGBA"] = 1] = "RGBA";
        ColorFormats[ColorFormats["HSLA"] = 2] = "HSLA";
        ColorFormats[ColorFormats["CMYK"] = 3] = "CMYK";
      })(ColorFormats || (ColorFormats = {}));

      var Rgba = function Rgba(r, g, b, a) {
        _classCallCheck(this, Rgba);

        this.r = r;
        this.g = g;
        this.b = b;
        this.a = a;
      };

      var Hsva = function Hsva(h, s, v, a) {
        _classCallCheck(this, Hsva);

        this.h = h;
        this.s = s;
        this.v = v;
        this.a = a;
      };

      var Hsla = function Hsla(h, s, l, a) {
        _classCallCheck(this, Hsla);

        this.h = h;
        this.s = s;
        this.l = l;
        this.a = a;
      };

      var Cmyk = function Cmyk(c, m, y, k) {
        var a = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : 1;

        _classCallCheck(this, Cmyk);

        this.c = c;
        this.m = m;
        this.y = y;
        this.k = k;
        this.a = a;
      };

      function calculateAutoPositioning(elBounds) {
        // Defaults
        var usePositionX = 'right';
        var usePositionY = 'bottom'; // Calculate collisions

        var height = elBounds.height,
            width = elBounds.width,
            top = elBounds.top,
            bottom = elBounds.bottom,
            left = elBounds.left,
            right = elBounds.right;
        var collisionTop = top - height < 0;
        var collisionBottom = bottom + height > (window.innerHeight || document.documentElement.clientHeight);
        var collisionLeft = left - width < 0;
        var collisionRight = right + width > (window.innerWidth || document.documentElement.clientWidth);
        var collisionAll = collisionTop && collisionBottom && collisionLeft && collisionRight; // Generate X & Y position values

        if (collisionBottom) {
          usePositionY = 'top';
        }

        if (collisionTop) {
          usePositionY = 'bottom';
        }

        if (collisionLeft) {
          usePositionX = 'right';
        }

        if (collisionRight) {
          usePositionX = 'left';
        } // Choose the largest gap available


        if (collisionAll) {
          var postions = ['left', 'right', 'top', 'bottom'];
          return postions.reduce(function (prev, next) {
            return elBounds[prev] > elBounds[next] ? prev : next;
          });
        }

        if (collisionLeft && collisionRight) {
          if (collisionTop) return 'bottom';
          if (collisionBottom) return 'top';
          return top > bottom ? 'top' : 'bottom';
        }

        if (collisionTop && collisionBottom) {
          if (collisionLeft) return 'right';
          if (collisionRight) return 'left';
          return left > right ? 'left' : 'right';
        }

        return "".concat(usePositionY, "-").concat(usePositionX);
      }

      function detectIE() {
        var ua = '';

        if (typeof navigator !== 'undefined') {
          ua = navigator.userAgent.toLowerCase();
        }

        var msie = ua.indexOf('msie ');

        if (msie > 0) {
          // IE 10 or older => return version number
          return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
        } // Other browser


        return false;
      }

      var TextDirective = /*#__PURE__*/function () {
        function TextDirective() {
          _classCallCheck(this, TextDirective);

          this.newValue = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        }

        _createClass(TextDirective, [{
          key: "inputChange",
          value: function inputChange(event) {
            var value = event.target.value;

            if (this.rg === undefined) {
              this.newValue.emit(value);
            } else {
              var numeric = parseFloat(value);
              this.newValue.emit({
                v: numeric,
                rg: this.rg
              });
            }
          }
        }]);

        return TextDirective;
      }();

      TextDirective.ɵfac = function TextDirective_Factory(t) {
        return new (t || TextDirective)();
      };

      TextDirective.ɵdir = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineDirective"]({
        type: TextDirective,
        selectors: [["", "text", ""]],
        hostBindings: function TextDirective_HostBindings(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("input", function TextDirective_input_HostBindingHandler($event) {
              return ctx.inputChange($event);
            });
          }
        },
        inputs: {
          rg: "rg",
          text: "text"
        },
        outputs: {
          newValue: "newValue"
        }
      });
      TextDirective.propDecorators = {
        rg: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }],
        text: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }],
        newValue: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"]
        }],
        inputChange: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["HostListener"],
          args: ['input', ['$event']]
        }]
      };
      /*@__PURE__*/

      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](TextDirective, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Directive"],
          args: [{
            selector: '[text]'
          }]
        }], function () {
          return [];
        }, {
          newValue: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"]
          }],
          inputChange: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["HostListener"],
            args: ['input', ['$event']]
          }],
          rg: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
          }],
          text: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
          }]
        });
      })();

      var SliderDirective = /*#__PURE__*/function () {
        function SliderDirective(elRef) {
          var _this = this;

          _classCallCheck(this, SliderDirective);

          this.elRef = elRef;
          this.dragEnd = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
          this.dragStart = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
          this.newValue = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();

          this.listenerMove = function (event) {
            return _this.move(event);
          };

          this.listenerStop = function () {
            return _this.stop();
          };
        }

        _createClass(SliderDirective, [{
          key: "mouseDown",
          value: function mouseDown(event) {
            this.start(event);
          }
        }, {
          key: "touchStart",
          value: function touchStart(event) {
            this.start(event);
          }
        }, {
          key: "move",
          value: function move(event) {
            event.preventDefault();
            this.setCursor(event);
          }
        }, {
          key: "start",
          value: function start(event) {
            this.setCursor(event);
            event.stopPropagation();
            document.addEventListener('mouseup', this.listenerStop);
            document.addEventListener('touchend', this.listenerStop);
            document.addEventListener('mousemove', this.listenerMove);
            document.addEventListener('touchmove', this.listenerMove);
            this.dragStart.emit();
          }
        }, {
          key: "stop",
          value: function stop() {
            document.removeEventListener('mouseup', this.listenerStop);
            document.removeEventListener('touchend', this.listenerStop);
            document.removeEventListener('mousemove', this.listenerMove);
            document.removeEventListener('touchmove', this.listenerMove);
            this.dragEnd.emit();
          }
        }, {
          key: "getX",
          value: function getX(event) {
            var position = this.elRef.nativeElement.getBoundingClientRect();
            var pageX = event.pageX !== undefined ? event.pageX : event.touches[0].pageX;
            return pageX - position.left - window.pageXOffset;
          }
        }, {
          key: "getY",
          value: function getY(event) {
            var position = this.elRef.nativeElement.getBoundingClientRect();
            var pageY = event.pageY !== undefined ? event.pageY : event.touches[0].pageY;
            return pageY - position.top - window.pageYOffset;
          }
        }, {
          key: "setCursor",
          value: function setCursor(event) {
            var width = this.elRef.nativeElement.offsetWidth;
            var height = this.elRef.nativeElement.offsetHeight;
            var x = Math.max(0, Math.min(this.getX(event), width));
            var y = Math.max(0, Math.min(this.getY(event), height));

            if (this.rgX !== undefined && this.rgY !== undefined) {
              this.newValue.emit({
                s: x / width,
                v: 1 - y / height,
                rgX: this.rgX,
                rgY: this.rgY
              });
            } else if (this.rgX === undefined && this.rgY !== undefined) {
              this.newValue.emit({
                v: y / height,
                rgY: this.rgY
              });
            } else if (this.rgX !== undefined && this.rgY === undefined) {
              this.newValue.emit({
                v: x / width,
                rgX: this.rgX
              });
            }
          }
        }]);

        return SliderDirective;
      }();

      SliderDirective.ɵfac = function SliderDirective_Factory(t) {
        return new (t || SliderDirective)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"]));
      };

      SliderDirective.ɵdir = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineDirective"]({
        type: SliderDirective,
        selectors: [["", "slider", ""]],
        hostBindings: function SliderDirective_HostBindings(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("mousedown", function SliderDirective_mousedown_HostBindingHandler($event) {
              return ctx.mouseDown($event);
            })("touchstart", function SliderDirective_touchstart_HostBindingHandler($event) {
              return ctx.touchStart($event);
            });
          }
        },
        inputs: {
          rgX: "rgX",
          rgY: "rgY",
          slider: "slider"
        },
        outputs: {
          dragEnd: "dragEnd",
          dragStart: "dragStart",
          newValue: "newValue"
        }
      });

      SliderDirective.ctorParameters = function () {
        return [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"]
        }];
      };

      SliderDirective.propDecorators = {
        rgX: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }],
        rgY: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }],
        slider: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }],
        dragEnd: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"]
        }],
        dragStart: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"]
        }],
        newValue: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"]
        }],
        mouseDown: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["HostListener"],
          args: ['mousedown', ['$event']]
        }],
        touchStart: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["HostListener"],
          args: ['touchstart', ['$event']]
        }]
      };
      /*@__PURE__*/

      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](SliderDirective, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Directive"],
          args: [{
            selector: '[slider]'
          }]
        }], function () {
          return [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"]
          }];
        }, {
          dragEnd: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"]
          }],
          dragStart: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"]
          }],
          newValue: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"]
          }],
          mouseDown: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["HostListener"],
            args: ['mousedown', ['$event']]
          }],
          touchStart: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["HostListener"],
            args: ['touchstart', ['$event']]
          }],
          rgX: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
          }],
          rgY: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
          }],
          slider: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
          }]
        });
      })();

      var SliderPosition = function SliderPosition(h, s, v, a) {
        _classCallCheck(this, SliderPosition);

        this.h = h;
        this.s = s;
        this.v = v;
        this.a = a;
      };

      var SliderDimension = function SliderDimension(h, s, v, a) {
        _classCallCheck(this, SliderDimension);

        this.h = h;
        this.s = s;
        this.v = v;
        this.a = a;
      };

      var ColorPickerService = /*#__PURE__*/function () {
        function ColorPickerService() {
          _classCallCheck(this, ColorPickerService);

          this.active = null;
        }

        _createClass(ColorPickerService, [{
          key: "setActive",
          value: function setActive(active) {
            if (this.active && this.active !== active && this.active.cpDialogDisplay !== 'inline') {
              this.active.closeDialog();
            }

            this.active = active;
          }
        }, {
          key: "hsva2hsla",
          value: function hsva2hsla(hsva) {
            var h = hsva.h,
                s = hsva.s,
                v = hsva.v,
                a = hsva.a;

            if (v === 0) {
              return new Hsla(h, 0, 0, a);
            } else if (s === 0 && v === 1) {
              return new Hsla(h, 1, 1, a);
            } else {
              var l = v * (2 - s) / 2;
              return new Hsla(h, v * s / (1 - Math.abs(2 * l - 1)), l, a);
            }
          }
        }, {
          key: "hsla2hsva",
          value: function hsla2hsva(hsla) {
            var h = Math.min(hsla.h, 1),
                s = Math.min(hsla.s, 1);
            var l = Math.min(hsla.l, 1),
                a = Math.min(hsla.a, 1);

            if (l === 0) {
              return new Hsva(h, 0, 0, a);
            } else {
              var v = l + s * (1 - Math.abs(2 * l - 1)) / 2;
              return new Hsva(h, 2 * (v - l) / v, v, a);
            }
          }
        }, {
          key: "hsvaToRgba",
          value: function hsvaToRgba(hsva) {
            var r, g, b;
            var h = hsva.h,
                s = hsva.s,
                v = hsva.v,
                a = hsva.a;
            var i = Math.floor(h * 6);
            var f = h * 6 - i;
            var p = v * (1 - s);
            var q = v * (1 - f * s);
            var t = v * (1 - (1 - f) * s);

            switch (i % 6) {
              case 0:
                r = v, g = t, b = p;
                break;

              case 1:
                r = q, g = v, b = p;
                break;

              case 2:
                r = p, g = v, b = t;
                break;

              case 3:
                r = p, g = q, b = v;
                break;

              case 4:
                r = t, g = p, b = v;
                break;

              case 5:
                r = v, g = p, b = q;
                break;

              default:
                r = 0, g = 0, b = 0;
            }

            return new Rgba(r, g, b, a);
          }
        }, {
          key: "cmykToRgb",
          value: function cmykToRgb(cmyk) {
            var r = (1 - cmyk.c) * (1 - cmyk.k);
            var g = (1 - cmyk.m) * (1 - cmyk.k);
            var b = (1 - cmyk.y) * (1 - cmyk.k);
            return new Rgba(r, g, b, cmyk.a);
          }
        }, {
          key: "rgbaToCmyk",
          value: function rgbaToCmyk(rgba) {
            var k = 1 - Math.max(rgba.r, rgba.g, rgba.b);

            if (k === 1) {
              return new Cmyk(0, 0, 0, 1, rgba.a);
            } else {
              var c = (1 - rgba.r - k) / (1 - k);
              var m = (1 - rgba.g - k) / (1 - k);
              var y = (1 - rgba.b - k) / (1 - k);
              return new Cmyk(c, m, y, k, rgba.a);
            }
          }
        }, {
          key: "rgbaToHsva",
          value: function rgbaToHsva(rgba) {
            var h, s;
            var r = Math.min(rgba.r, 1),
                g = Math.min(rgba.g, 1);
            var b = Math.min(rgba.b, 1),
                a = Math.min(rgba.a, 1);
            var max = Math.max(r, g, b),
                min = Math.min(r, g, b);
            var v = max,
                d = max - min;
            s = max === 0 ? 0 : d / max;

            if (max === min) {
              h = 0;
            } else {
              switch (max) {
                case r:
                  h = (g - b) / d + (g < b ? 6 : 0);
                  break;

                case g:
                  h = (b - r) / d + 2;
                  break;

                case b:
                  h = (r - g) / d + 4;
                  break;

                default:
                  h = 0;
              }

              h /= 6;
            }

            return new Hsva(h, s, v, a);
          }
        }, {
          key: "rgbaToHex",
          value: function rgbaToHex(rgba, allowHex8) {
            /* tslint:disable:no-bitwise */
            var hex = '#' + (1 << 24 | rgba.r << 16 | rgba.g << 8 | rgba.b).toString(16).substr(1);

            if (allowHex8) {
              hex += (1 << 8 | Math.round(rgba.a * 255)).toString(16).substr(1);
            }
            /* tslint:enable:no-bitwise */


            return hex;
          }
        }, {
          key: "normalizeCMYK",
          value: function normalizeCMYK(cmyk) {
            return new Cmyk(cmyk.c / 100, cmyk.m / 100, cmyk.y / 100, cmyk.k / 100, cmyk.a);
          }
        }, {
          key: "denormalizeCMYK",
          value: function denormalizeCMYK(cmyk) {
            return new Cmyk(Math.floor(cmyk.c * 100), Math.floor(cmyk.m * 100), Math.floor(cmyk.y * 100), Math.floor(cmyk.k * 100), cmyk.a);
          }
        }, {
          key: "denormalizeRGBA",
          value: function denormalizeRGBA(rgba) {
            return new Rgba(Math.round(rgba.r * 255), Math.round(rgba.g * 255), Math.round(rgba.b * 255), rgba.a);
          }
        }, {
          key: "stringToHsva",
          value: function stringToHsva() {
            var colorString = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';
            var allowHex8 = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;
            var hsva = null;
            colorString = (colorString || '').toLowerCase();
            var stringParsers = [{
              re: /(rgb)a?\(\s*(\d{1,3})\s*,\s*(\d{1,3})\s*%?,\s*(\d{1,3})\s*%?(?:,\s*(\d+(?:\.\d+)?)\s*)?\)/,
              parse: function parse(execResult) {
                return new Rgba(parseInt(execResult[2], 10) / 255, parseInt(execResult[3], 10) / 255, parseInt(execResult[4], 10) / 255, isNaN(parseFloat(execResult[5])) ? 1 : parseFloat(execResult[5]));
              }
            }, {
              re: /(hsl)a?\(\s*(\d{1,3})\s*,\s*(\d{1,3})%\s*,\s*(\d{1,3})%\s*(?:,\s*(\d+(?:\.\d+)?)\s*)?\)/,
              parse: function parse(execResult) {
                return new Hsla(parseInt(execResult[2], 10) / 360, parseInt(execResult[3], 10) / 100, parseInt(execResult[4], 10) / 100, isNaN(parseFloat(execResult[5])) ? 1 : parseFloat(execResult[5]));
              }
            }];

            if (allowHex8) {
              stringParsers.push({
                re: /#([a-fA-F0-9]{2})([a-fA-F0-9]{2})([a-fA-F0-9]{2})([a-fA-F0-9]{2})?$/,
                parse: function parse(execResult) {
                  return new Rgba(parseInt(execResult[1], 16) / 255, parseInt(execResult[2], 16) / 255, parseInt(execResult[3], 16) / 255, parseInt(execResult[4] || 'FF', 16) / 255);
                }
              });
            } else {
              stringParsers.push({
                re: /#([a-fA-F0-9]{2})([a-fA-F0-9]{2})([a-fA-F0-9]{2})$/,
                parse: function parse(execResult) {
                  return new Rgba(parseInt(execResult[1], 16) / 255, parseInt(execResult[2], 16) / 255, parseInt(execResult[3], 16) / 255, 1);
                }
              });
            }

            stringParsers.push({
              re: /#([a-fA-F0-9])([a-fA-F0-9])([a-fA-F0-9])$/,
              parse: function parse(execResult) {
                return new Rgba(parseInt(execResult[1] + execResult[1], 16) / 255, parseInt(execResult[2] + execResult[2], 16) / 255, parseInt(execResult[3] + execResult[3], 16) / 255, 1);
              }
            });

            for (var key in stringParsers) {
              if (stringParsers.hasOwnProperty(key)) {
                var parser = stringParsers[key];
                var match = parser.re.exec(colorString),
                    color = match && parser.parse(match);

                if (color) {
                  if (color instanceof Rgba) {
                    hsva = this.rgbaToHsva(color);
                  } else if (color instanceof Hsla) {
                    hsva = this.hsla2hsva(color);
                  }

                  return hsva;
                }
              }
            }

            return hsva;
          }
        }, {
          key: "outputFormat",
          value: function outputFormat(hsva, _outputFormat, alphaChannel) {
            if (_outputFormat === 'auto') {
              _outputFormat = hsva.a < 1 ? 'rgba' : 'hex';
            }

            switch (_outputFormat) {
              case 'hsla':
                var hsla = this.hsva2hsla(hsva);
                var hslaText = new Hsla(Math.round(hsla.h * 360), Math.round(hsla.s * 100), Math.round(hsla.l * 100), Math.round(hsla.a * 100) / 100);

                if (hsva.a < 1 || alphaChannel === 'always') {
                  return 'hsla(' + hslaText.h + ',' + hslaText.s + '%,' + hslaText.l + '%,' + hslaText.a + ')';
                } else {
                  return 'hsl(' + hslaText.h + ',' + hslaText.s + '%,' + hslaText.l + '%)';
                }

              case 'rgba':
                var rgba = this.denormalizeRGBA(this.hsvaToRgba(hsva));

                if (hsva.a < 1 || alphaChannel === 'always') {
                  return 'rgba(' + rgba.r + ',' + rgba.g + ',' + rgba.b + ',' + Math.round(rgba.a * 100) / 100 + ')';
                } else {
                  return 'rgb(' + rgba.r + ',' + rgba.g + ',' + rgba.b + ')';
                }

              default:
                var allowHex8 = alphaChannel === 'always' || alphaChannel === 'forced';
                return this.rgbaToHex(this.denormalizeRGBA(this.hsvaToRgba(hsva)), allowHex8);
            }
          }
        }]);

        return ColorPickerService;
      }();

      ColorPickerService.ɵfac = function ColorPickerService_Factory(t) {
        return new (t || ColorPickerService)();
      };

      ColorPickerService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({
        token: ColorPickerService,
        factory: ColorPickerService.ɵfac
      });

      ColorPickerService.ctorParameters = function () {
        return [];
      };
      /*@__PURE__*/


      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](ColorPickerService, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"]
        }], function () {
          return [];
        }, null);
      })();

      var ColorPickerComponent = /*#__PURE__*/function () {
        function ColorPickerComponent(elRef, cdRef, service) {
          _classCallCheck(this, ColorPickerComponent);

          this.elRef = elRef;
          this.cdRef = cdRef;
          this.service = service;
          this.isIE10 = false;
          this.dialogArrowSize = 10;
          this.dialogArrowOffset = 15;
          this.dialogInputFields = [ColorFormats.HEX, ColorFormats.RGBA, ColorFormats.HSLA, ColorFormats.CMYK];
          this.useRootViewContainer = false;
        }

        _createClass(ColorPickerComponent, [{
          key: "handleEsc",
          value: function handleEsc(event) {
            if (this.show && this.cpDialogDisplay === 'popup') {
              this.onCancelColor(event);
            }
          }
        }, {
          key: "handleEnter",
          value: function handleEnter(event) {
            if (this.show && this.cpDialogDisplay === 'popup') {
              this.onAcceptColor(event);
            }
          }
        }, {
          key: "ngOnInit",
          value: function ngOnInit() {
            var _this2 = this;

            this.slider = new SliderPosition(0, 0, 0, 0);
            var hueWidth = this.hueSlider.nativeElement.offsetWidth || 140;
            var alphaWidth = this.alphaSlider.nativeElement.offsetWidth || 140;
            this.sliderDimMax = new SliderDimension(hueWidth, this.cpWidth, 130, alphaWidth);

            if (this.cpCmykEnabled) {
              this.format = ColorFormats.CMYK;
            } else if (this.cpOutputFormat === 'rgba') {
              this.format = ColorFormats.RGBA;
            } else if (this.cpOutputFormat === 'hsla') {
              this.format = ColorFormats.HSLA;
            } else {
              this.format = ColorFormats.HEX;
            }

            this.listenerMouseDown = function (event) {
              _this2.onMouseDown(event);
            };

            this.listenerResize = function () {
              _this2.onResize();
            };

            this.openDialog(this.initialColor, false);
          }
        }, {
          key: "ngOnDestroy",
          value: function ngOnDestroy() {
            this.closeDialog();
          }
        }, {
          key: "ngAfterViewInit",
          value: function ngAfterViewInit() {
            if (this.cpWidth !== 230 || this.cpDialogDisplay === 'inline') {
              var hueWidth = this.hueSlider.nativeElement.offsetWidth || 140;
              var alphaWidth = this.alphaSlider.nativeElement.offsetWidth || 140;
              this.sliderDimMax = new SliderDimension(hueWidth, this.cpWidth, 130, alphaWidth);
              this.updateColorPicker(false);
              this.cdRef.detectChanges();
            }
          }
        }, {
          key: "openDialog",
          value: function openDialog(color) {
            var emit = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;
            this.service.setActive(this);

            if (!this.width) {
              this.cpWidth = this.directiveElementRef.nativeElement.offsetWidth;
            }

            if (!this.height) {
              this.height = 320;
            }

            this.setInitialColor(color);
            this.setColorFromString(color, emit);
            this.openColorPicker();
          }
        }, {
          key: "closeDialog",
          value: function closeDialog() {
            this.closeColorPicker();
          }
        }, {
          key: "setupDialog",
          value: function setupDialog(instance, elementRef, color, cpWidth, cpHeight, cpDialogDisplay, cpFallbackColor, cpColorMode, cpCmykEnabled, cpAlphaChannel, cpOutputFormat, cpDisableInput, cpIgnoredElements, cpSaveClickOutside, cpCloseClickOutside, cpUseRootViewContainer, cpPosition, cpPositionOffset, cpPositionRelativeToArrow, cpPresetLabel, cpPresetColors, cpPresetColorsClass, cpMaxPresetColorsLength, cpPresetEmptyMessage, cpPresetEmptyMessageClass, cpOKButton, cpOKButtonClass, cpOKButtonText, cpCancelButton, cpCancelButtonClass, cpCancelButtonText, cpAddColorButton, cpAddColorButtonClass, cpAddColorButtonText, cpRemoveColorButtonClass) {
            this.setInitialColor(color);
            this.setColorMode(cpColorMode);
            this.isIE10 = detectIE() === 10;
            this.directiveInstance = instance;
            this.directiveElementRef = elementRef;
            this.cpDisableInput = cpDisableInput;
            this.cpCmykEnabled = cpCmykEnabled;
            this.cpAlphaChannel = cpAlphaChannel;
            this.cpOutputFormat = cpOutputFormat;
            this.cpDialogDisplay = cpDialogDisplay;
            this.cpIgnoredElements = cpIgnoredElements;
            this.cpSaveClickOutside = cpSaveClickOutside;
            this.cpCloseClickOutside = cpCloseClickOutside;
            this.useRootViewContainer = cpUseRootViewContainer;
            this.width = this.cpWidth = parseInt(cpWidth, 10);
            this.height = this.cpHeight = parseInt(cpHeight, 10);
            this.cpPosition = cpPosition;
            this.cpPositionOffset = parseInt(cpPositionOffset, 10);
            this.cpOKButton = cpOKButton;
            this.cpOKButtonText = cpOKButtonText;
            this.cpOKButtonClass = cpOKButtonClass;
            this.cpCancelButton = cpCancelButton;
            this.cpCancelButtonText = cpCancelButtonText;
            this.cpCancelButtonClass = cpCancelButtonClass;
            this.fallbackColor = cpFallbackColor || '#fff';
            this.setPresetConfig(cpPresetLabel, cpPresetColors);
            this.cpPresetColorsClass = cpPresetColorsClass;
            this.cpMaxPresetColorsLength = cpMaxPresetColorsLength;
            this.cpPresetEmptyMessage = cpPresetEmptyMessage;
            this.cpPresetEmptyMessageClass = cpPresetEmptyMessageClass;
            this.cpAddColorButton = cpAddColorButton;
            this.cpAddColorButtonText = cpAddColorButtonText;
            this.cpAddColorButtonClass = cpAddColorButtonClass;
            this.cpRemoveColorButtonClass = cpRemoveColorButtonClass;

            if (!cpPositionRelativeToArrow) {
              this.dialogArrowOffset = 0;
            }

            if (cpDialogDisplay === 'inline') {
              this.dialogArrowSize = 0;
              this.dialogArrowOffset = 0;
            }

            if (cpOutputFormat === 'hex' && cpAlphaChannel !== 'always' && cpAlphaChannel !== 'forced') {
              this.cpAlphaChannel = 'disabled';
            }
          }
        }, {
          key: "setColorMode",
          value: function setColorMode(mode) {
            switch (mode.toString().toUpperCase()) {
              case '1':
              case 'C':
              case 'COLOR':
                this.cpColorMode = 1;
                break;

              case '2':
              case 'G':
              case 'GRAYSCALE':
                this.cpColorMode = 2;
                break;

              case '3':
              case 'P':
              case 'PRESETS':
                this.cpColorMode = 3;
                break;

              default:
                this.cpColorMode = 1;
            }
          }
        }, {
          key: "setInitialColor",
          value: function setInitialColor(color) {
            this.initialColor = color;
          }
        }, {
          key: "setPresetConfig",
          value: function setPresetConfig(cpPresetLabel, cpPresetColors) {
            this.cpPresetLabel = cpPresetLabel;
            this.cpPresetColors = cpPresetColors;
          }
        }, {
          key: "setColorFromString",
          value: function setColorFromString(value) {
            var emit = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;
            var update = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : true;
            var hsva;

            if (this.cpAlphaChannel === 'always' || this.cpAlphaChannel === 'forced') {
              hsva = this.service.stringToHsva(value, true);

              if (!hsva && !this.hsva) {
                hsva = this.service.stringToHsva(value, false);
              }
            } else {
              hsva = this.service.stringToHsva(value, false);
            }

            if (!hsva && !this.hsva) {
              hsva = this.service.stringToHsva(this.fallbackColor, false);
            }

            if (hsva) {
              this.hsva = hsva;
              this.sliderH = this.hsva.h;

              if (this.cpOutputFormat === 'hex' && this.cpAlphaChannel === 'disabled') {
                this.hsva.a = 1;
              }

              this.updateColorPicker(emit, update);
            }
          }
        }, {
          key: "onResize",
          value: function onResize() {
            if (this.position === 'fixed') {
              this.setDialogPosition();
            } else if (this.cpDialogDisplay !== 'inline') {
              this.closeColorPicker();
            }
          }
        }, {
          key: "onDragEnd",
          value: function onDragEnd(slider) {
            this.directiveInstance.sliderDragEnd({
              slider: slider,
              color: this.outputColor
            });
          }
        }, {
          key: "onDragStart",
          value: function onDragStart(slider) {
            this.directiveInstance.sliderDragStart({
              slider: slider,
              color: this.outputColor
            });
          }
        }, {
          key: "onMouseDown",
          value: function onMouseDown(event) {
            if (this.show && !this.isIE10 && this.cpDialogDisplay === 'popup' && event.target !== this.directiveElementRef.nativeElement && !this.isDescendant(this.elRef.nativeElement, event.target) && !this.isDescendant(this.directiveElementRef.nativeElement, event.target) && this.cpIgnoredElements.filter(function (item) {
              return item === event.target;
            }).length === 0) {
              if (this.cpSaveClickOutside) {
                this.directiveInstance.colorSelected(this.outputColor);
              } else {
                this.hsva = null;
                this.setColorFromString(this.initialColor, false);

                if (this.cpCmykEnabled) {
                  this.directiveInstance.cmykChanged(this.cmykColor);
                }

                this.directiveInstance.colorChanged(this.initialColor);
                this.directiveInstance.colorCanceled();
              }

              if (this.cpCloseClickOutside) {
                this.closeColorPicker();
              }
            }
          }
        }, {
          key: "onAcceptColor",
          value: function onAcceptColor(event) {
            event.stopPropagation();

            if (this.outputColor) {
              this.directiveInstance.colorSelected(this.outputColor);
            }

            if (this.cpDialogDisplay === 'popup') {
              this.closeColorPicker();
            }
          }
        }, {
          key: "onCancelColor",
          value: function onCancelColor(event) {
            this.hsva = null;
            event.stopPropagation();
            this.directiveInstance.colorCanceled();
            this.setColorFromString(this.initialColor, true);

            if (this.cpDialogDisplay === 'popup') {
              if (this.cpCmykEnabled) {
                this.directiveInstance.cmykChanged(this.cmykColor);
              }

              this.directiveInstance.colorChanged(this.initialColor, true);
              this.closeColorPicker();
            }
          }
        }, {
          key: "onFormatToggle",
          value: function onFormatToggle(change) {
            var availableFormats = this.dialogInputFields.length - (this.cpCmykEnabled ? 0 : 1);
            var nextFormat = ((this.dialogInputFields.indexOf(this.format) + change) % availableFormats + availableFormats) % availableFormats;
            this.format = this.dialogInputFields[nextFormat];
          }
        }, {
          key: "onColorChange",
          value: function onColorChange(value) {
            this.hsva.s = value.s / value.rgX;
            this.hsva.v = value.v / value.rgY;
            this.updateColorPicker();
            this.directiveInstance.sliderChanged({
              slider: 'lightness',
              value: this.hsva.v,
              color: this.outputColor
            });
            this.directiveInstance.sliderChanged({
              slider: 'saturation',
              value: this.hsva.s,
              color: this.outputColor
            });
          }
        }, {
          key: "onHueChange",
          value: function onHueChange(value) {
            this.hsva.h = value.v / value.rgX;
            this.sliderH = this.hsva.h;
            this.updateColorPicker();
            this.directiveInstance.sliderChanged({
              slider: 'hue',
              value: this.hsva.h,
              color: this.outputColor
            });
          }
        }, {
          key: "onValueChange",
          value: function onValueChange(value) {
            this.hsva.v = value.v / value.rgX;
            this.updateColorPicker();
            this.directiveInstance.sliderChanged({
              slider: 'value',
              value: this.hsva.v,
              color: this.outputColor
            });
          }
        }, {
          key: "onAlphaChange",
          value: function onAlphaChange(value) {
            this.hsva.a = value.v / value.rgX;
            this.updateColorPicker();
            this.directiveInstance.sliderChanged({
              slider: 'alpha',
              value: this.hsva.a,
              color: this.outputColor
            });
          }
        }, {
          key: "onHexInput",
          value: function onHexInput(value) {
            if (value === null) {
              this.updateColorPicker();
            } else {
              if (value && value[0] !== '#') {
                value = '#' + value;
              }

              var validHex = /^#([a-f0-9]{3}|[a-f0-9]{6})$/gi;

              if (this.cpAlphaChannel === 'always') {
                validHex = /^#([a-f0-9]{3}|[a-f0-9]{6}|[a-f0-9]{8})$/gi;
              }

              var valid = validHex.test(value);

              if (valid) {
                if (value.length < 5) {
                  value = '#' + value.substring(1).split('').map(function (c) {
                    return c + c;
                  }).join('');
                }

                if (this.cpAlphaChannel === 'forced') {
                  value += Math.round(this.hsva.a * 255).toString(16);
                }

                this.setColorFromString(value, true, false);
              }

              this.directiveInstance.inputChanged({
                input: 'hex',
                valid: valid,
                value: value,
                color: this.outputColor
              });
            }
          }
        }, {
          key: "onRedInput",
          value: function onRedInput(value) {
            var rgba = this.service.hsvaToRgba(this.hsva);
            var valid = !isNaN(value.v) && value.v >= 0 && value.v <= value.rg;

            if (valid) {
              rgba.r = value.v / value.rg;
              this.hsva = this.service.rgbaToHsva(rgba);
              this.sliderH = this.hsva.h;
              this.updateColorPicker();
            }

            this.directiveInstance.inputChanged({
              input: 'red',
              valid: valid,
              value: rgba.r,
              color: this.outputColor
            });
          }
        }, {
          key: "onBlueInput",
          value: function onBlueInput(value) {
            var rgba = this.service.hsvaToRgba(this.hsva);
            var valid = !isNaN(value.v) && value.v >= 0 && value.v <= value.rg;

            if (valid) {
              rgba.b = value.v / value.rg;
              this.hsva = this.service.rgbaToHsva(rgba);
              this.sliderH = this.hsva.h;
              this.updateColorPicker();
            }

            this.directiveInstance.inputChanged({
              input: 'blue',
              valid: valid,
              value: rgba.b,
              color: this.outputColor
            });
          }
        }, {
          key: "onGreenInput",
          value: function onGreenInput(value) {
            var rgba = this.service.hsvaToRgba(this.hsva);
            var valid = !isNaN(value.v) && value.v >= 0 && value.v <= value.rg;

            if (valid) {
              rgba.g = value.v / value.rg;
              this.hsva = this.service.rgbaToHsva(rgba);
              this.sliderH = this.hsva.h;
              this.updateColorPicker();
            }

            this.directiveInstance.inputChanged({
              input: 'green',
              valid: valid,
              value: rgba.g,
              color: this.outputColor
            });
          }
        }, {
          key: "onHueInput",
          value: function onHueInput(value) {
            var valid = !isNaN(value.v) && value.v >= 0 && value.v <= value.rg;

            if (valid) {
              this.hsva.h = value.v / value.rg;
              this.sliderH = this.hsva.h;
              this.updateColorPicker();
            }

            this.directiveInstance.inputChanged({
              input: 'hue',
              valid: valid,
              value: this.hsva.h,
              color: this.outputColor
            });
          }
        }, {
          key: "onValueInput",
          value: function onValueInput(value) {
            var valid = !isNaN(value.v) && value.v >= 0 && value.v <= value.rg;

            if (valid) {
              this.hsva.v = value.v / value.rg;
              this.updateColorPicker();
            }

            this.directiveInstance.inputChanged({
              input: 'value',
              valid: valid,
              value: this.hsva.v,
              color: this.outputColor
            });
          }
        }, {
          key: "onAlphaInput",
          value: function onAlphaInput(value) {
            var valid = !isNaN(value.v) && value.v >= 0 && value.v <= value.rg;

            if (valid) {
              this.hsva.a = value.v / value.rg;
              this.updateColorPicker();
            }

            this.directiveInstance.inputChanged({
              input: 'alpha',
              valid: valid,
              value: this.hsva.a,
              color: this.outputColor
            });
          }
        }, {
          key: "onLightnessInput",
          value: function onLightnessInput(value) {
            var hsla = this.service.hsva2hsla(this.hsva);
            var valid = !isNaN(value.v) && value.v >= 0 && value.v <= value.rg;

            if (valid) {
              hsla.l = value.v / value.rg;
              this.hsva = this.service.hsla2hsva(hsla);
              this.sliderH = this.hsva.h;
              this.updateColorPicker();
            }

            this.directiveInstance.inputChanged({
              input: 'lightness',
              valid: valid,
              value: hsla.l,
              color: this.outputColor
            });
          }
        }, {
          key: "onSaturationInput",
          value: function onSaturationInput(value) {
            var hsla = this.service.hsva2hsla(this.hsva);
            var valid = !isNaN(value.v) && value.v >= 0 && value.v <= value.rg;

            if (valid) {
              hsla.s = value.v / value.rg;
              this.hsva = this.service.hsla2hsva(hsla);
              this.sliderH = this.hsva.h;
              this.updateColorPicker();
            }

            this.directiveInstance.inputChanged({
              input: 'saturation',
              valid: valid,
              value: hsla.s,
              color: this.outputColor
            });
          }
        }, {
          key: "onCyanInput",
          value: function onCyanInput(value) {
            var valid = !isNaN(value.v) && value.v >= 0 && value.v <= value.rg;

            if (valid) {
              this.cmyk.c = value.v;
              this.updateColorPicker(false, true, true);
            }

            this.directiveInstance.inputChanged({
              input: 'cyan',
              valid: true,
              value: this.cmyk.c,
              color: this.outputColor
            });
          }
        }, {
          key: "onMagentaInput",
          value: function onMagentaInput(value) {
            var valid = !isNaN(value.v) && value.v >= 0 && value.v <= value.rg;

            if (valid) {
              this.cmyk.m = value.v;
              this.updateColorPicker(false, true, true);
            }

            this.directiveInstance.inputChanged({
              input: 'magenta',
              valid: true,
              value: this.cmyk.m,
              color: this.outputColor
            });
          }
        }, {
          key: "onYellowInput",
          value: function onYellowInput(value) {
            var valid = !isNaN(value.v) && value.v >= 0 && value.v <= value.rg;

            if (valid) {
              this.cmyk.y = value.v;
              this.updateColorPicker(false, true, true);
            }

            this.directiveInstance.inputChanged({
              input: 'yellow',
              valid: true,
              value: this.cmyk.y,
              color: this.outputColor
            });
          }
        }, {
          key: "onBlackInput",
          value: function onBlackInput(value) {
            var valid = !isNaN(value.v) && value.v >= 0 && value.v <= value.rg;

            if (valid) {
              this.cmyk.k = value.v;
              this.updateColorPicker(false, true, true);
            }

            this.directiveInstance.inputChanged({
              input: 'black',
              valid: true,
              value: this.cmyk.k,
              color: this.outputColor
            });
          }
        }, {
          key: "onAddPresetColor",
          value: function onAddPresetColor(event, value) {
            event.stopPropagation();

            if (!this.cpPresetColors.filter(function (color) {
              return color === value;
            }).length) {
              this.cpPresetColors = this.cpPresetColors.concat(value);
              this.directiveInstance.presetColorsChanged(this.cpPresetColors);
            }
          }
        }, {
          key: "onRemovePresetColor",
          value: function onRemovePresetColor(event, value) {
            event.stopPropagation();
            this.cpPresetColors = this.cpPresetColors.filter(function (color) {
              return color !== value;
            });
            this.directiveInstance.presetColorsChanged(this.cpPresetColors);
          } // Private helper functions for the color picker dialog status

        }, {
          key: "openColorPicker",
          value: function openColorPicker() {
            var _this3 = this;

            if (!this.show) {
              this.show = true;
              this.hidden = true;
              setTimeout(function () {
                _this3.hidden = false;

                _this3.setDialogPosition();

                _this3.cdRef.detectChanges();
              }, 0);
              this.directiveInstance.stateChanged(true);

              if (!this.isIE10) {
                document.addEventListener('mousedown', this.listenerMouseDown);
                document.addEventListener('touchstart', this.listenerMouseDown);
              }

              window.addEventListener('resize', this.listenerResize);
            }
          }
        }, {
          key: "closeColorPicker",
          value: function closeColorPicker() {
            if (this.show) {
              this.show = false;
              this.directiveInstance.stateChanged(false);

              if (!this.isIE10) {
                document.removeEventListener('mousedown', this.listenerMouseDown);
                document.removeEventListener('touchstart', this.listenerMouseDown);
              }

              window.removeEventListener('resize', this.listenerResize);

              if (!this.cdRef['destroyed']) {
                this.cdRef.detectChanges();
              }
            }
          }
        }, {
          key: "updateColorPicker",
          value: function updateColorPicker() {
            var emit = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : true;
            var update = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;
            var cmykInput = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;

            if (this.sliderDimMax) {
              if (this.cpColorMode === 2) {
                this.hsva.s = 0;
              }

              var hue, hsla, rgba;
              var lastOutput = this.outputColor;
              hsla = this.service.hsva2hsla(this.hsva);

              if (!this.cpCmykEnabled) {
                rgba = this.service.denormalizeRGBA(this.service.hsvaToRgba(this.hsva));
              } else {
                if (!cmykInput) {
                  rgba = this.service.hsvaToRgba(this.hsva);
                  this.cmyk = this.service.denormalizeCMYK(this.service.rgbaToCmyk(rgba));
                } else {
                  rgba = this.service.cmykToRgb(this.service.normalizeCMYK(this.cmyk));
                  this.hsva = this.service.rgbaToHsva(rgba);
                }

                rgba = this.service.denormalizeRGBA(rgba);
                this.sliderH = this.hsva.h;
              }

              hue = this.service.denormalizeRGBA(this.service.hsvaToRgba(new Hsva(this.sliderH || this.hsva.h, 1, 1, 1)));

              if (update) {
                this.hslaText = new Hsla(Math.round(hsla.h * 360), Math.round(hsla.s * 100), Math.round(hsla.l * 100), Math.round(hsla.a * 100) / 100);
                this.rgbaText = new Rgba(rgba.r, rgba.g, rgba.b, Math.round(rgba.a * 100) / 100);

                if (this.cpCmykEnabled) {
                  this.cmykText = new Cmyk(this.cmyk.c, this.cmyk.m, this.cmyk.y, this.cmyk.k, Math.round(this.cmyk.a * 100) / 100);
                }

                var allowHex8 = this.cpAlphaChannel === 'always';
                this.hexText = this.service.rgbaToHex(rgba, allowHex8);
                this.hexAlpha = this.rgbaText.a;
              }

              if (this.cpOutputFormat === 'auto') {
                if (this.format !== ColorFormats.RGBA && this.format !== ColorFormats.CMYK) {
                  if (this.hsva.a < 1) {
                    this.format = this.hsva.a < 1 ? ColorFormats.RGBA : ColorFormats.HEX;
                  }
                }
              }

              this.hueSliderColor = 'rgb(' + hue.r + ',' + hue.g + ',' + hue.b + ')';
              this.alphaSliderColor = 'rgb(' + rgba.r + ',' + rgba.g + ',' + rgba.b + ')';
              this.outputColor = this.service.outputFormat(this.hsva, this.cpOutputFormat, this.cpAlphaChannel);
              this.selectedColor = this.service.outputFormat(this.hsva, 'rgba', null);

              if (this.format !== ColorFormats.CMYK) {
                this.cmykColor = '';
              } else {
                if (this.cpAlphaChannel === 'always' || this.cpAlphaChannel === 'enabled' || this.cpAlphaChannel === 'forced') {
                  var alpha = Math.round(this.cmyk.a * 100) / 100;
                  this.cmykColor = "cmyka(".concat(this.cmyk.c, ",").concat(this.cmyk.m, ",").concat(this.cmyk.y, ",").concat(this.cmyk.k, ",").concat(alpha, ")");
                } else {
                  this.cmykColor = "cmyk(".concat(this.cmyk.c, ",").concat(this.cmyk.m, ",").concat(this.cmyk.y, ",").concat(this.cmyk.k, ")");
                }
              }

              this.slider = new SliderPosition((this.sliderH || this.hsva.h) * this.sliderDimMax.h - 8, this.hsva.s * this.sliderDimMax.s - 8, (1 - this.hsva.v) * this.sliderDimMax.v - 8, this.hsva.a * this.sliderDimMax.a - 8);

              if (emit && lastOutput !== this.outputColor) {
                if (this.cpCmykEnabled) {
                  this.directiveInstance.cmykChanged(this.cmykColor);
                }

                this.directiveInstance.colorChanged(this.outputColor);
              }
            }
          } // Private helper functions for the color picker dialog positioning

        }, {
          key: "setDialogPosition",
          value: function setDialogPosition() {
            if (this.cpDialogDisplay === 'inline') {
              this.position = 'relative';
            } else {
              var position = 'static',
                  transform = '',
                  style;
              var parentNode = null,
                  transformNode = null;
              var node = this.directiveElementRef.nativeElement.parentNode;
              var dialogHeight = this.dialogElement.nativeElement.offsetHeight;

              while (node !== null && node.tagName !== 'HTML') {
                style = window.getComputedStyle(node);
                position = style.getPropertyValue('position');
                transform = style.getPropertyValue('transform');

                if (position !== 'static' && parentNode === null) {
                  parentNode = node;
                }

                if (transform && transform !== 'none' && transformNode === null) {
                  transformNode = node;
                }

                if (position === 'fixed') {
                  parentNode = transformNode;
                  break;
                }

                node = node.parentNode;
              }

              var boxDirective = this.createDialogBox(this.directiveElementRef.nativeElement, position !== 'fixed');

              if (this.useRootViewContainer || position === 'fixed' && (!parentNode || parentNode instanceof HTMLUnknownElement)) {
                this.top = boxDirective.top;
                this.left = boxDirective.left;
              } else {
                if (parentNode === null) {
                  parentNode = node;
                }

                var boxParent = this.createDialogBox(parentNode, position !== 'fixed');
                this.top = boxDirective.top - boxParent.top;
                this.left = boxDirective.left - boxParent.left;
              }

              if (position === 'fixed') {
                this.position = 'fixed';
              }

              var usePosition = this.cpPosition;

              if (this.cpPosition === 'auto') {
                var dialogBounds = this.dialogElement.nativeElement.getBoundingClientRect();
                usePosition = calculateAutoPositioning(dialogBounds);
              }

              if (usePosition === 'top') {
                this.arrowTop = dialogHeight - 1;
                this.top -= dialogHeight + this.dialogArrowSize;
                this.left += this.cpPositionOffset / 100 * boxDirective.width - this.dialogArrowOffset;
              } else if (usePosition === 'bottom') {
                this.top += boxDirective.height + this.dialogArrowSize;
                this.left += this.cpPositionOffset / 100 * boxDirective.width - this.dialogArrowOffset;
              } else if (usePosition === 'top-left' || usePosition === 'left-top') {
                this.top -= dialogHeight - boxDirective.height + boxDirective.height * this.cpPositionOffset / 100;
                this.left -= this.cpWidth + this.dialogArrowSize - 2 - this.dialogArrowOffset;
              } else if (usePosition === 'top-right' || usePosition === 'right-top') {
                this.top -= dialogHeight - boxDirective.height + boxDirective.height * this.cpPositionOffset / 100;
                this.left += boxDirective.width + this.dialogArrowSize - 2 - this.dialogArrowOffset;
              } else if (usePosition === 'left' || usePosition === 'bottom-left' || usePosition === 'left-bottom') {
                this.top += boxDirective.height * this.cpPositionOffset / 100 - this.dialogArrowOffset;
                this.left -= this.cpWidth + this.dialogArrowSize - 2;
              } else {
                // usePosition === 'right' || usePosition === 'bottom-right' || usePosition === 'right-bottom'
                this.top += boxDirective.height * this.cpPositionOffset / 100 - this.dialogArrowOffset;
                this.left += boxDirective.width + this.dialogArrowSize - 2;
              }
            }
          } // Private helper functions for the color picker dialog positioning and opening

        }, {
          key: "isDescendant",
          value: function isDescendant(parent, child) {
            var node = child.parentNode;

            while (node !== null) {
              if (node === parent) {
                return true;
              }

              node = node.parentNode;
            }

            return false;
          }
        }, {
          key: "createDialogBox",
          value: function createDialogBox(element, offset) {
            return {
              top: element.getBoundingClientRect().top + (offset ? window.pageYOffset : 0),
              left: element.getBoundingClientRect().left + (offset ? window.pageXOffset : 0),
              width: element.offsetWidth,
              height: element.offsetHeight
            };
          }
        }]);

        return ColorPickerComponent;
      }();

      ColorPickerComponent.ɵfac = function ColorPickerComponent_Factory(t) {
        return new (t || ColorPickerComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["ChangeDetectorRef"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](ColorPickerService));
      };

      ColorPickerComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
        type: ColorPickerComponent,
        selectors: [["color-picker"]],
        viewQuery: function ColorPickerComponent_Query(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵstaticViewQuery"](_c0, true);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵstaticViewQuery"](_c1, true);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵstaticViewQuery"](_c2, true);
          }

          if (rf & 2) {
            var _t;

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵloadQuery"]()) && (ctx.dialogElement = _t.first);
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵloadQuery"]()) && (ctx.hueSlider = _t.first);
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵloadQuery"]()) && (ctx.alphaSlider = _t.first);
          }
        },
        hostBindings: function ColorPickerComponent_HostBindings(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("keyup.esc", function ColorPickerComponent_keyup_esc_HostBindingHandler($event) {
              return ctx.handleEsc($event);
            }, false, _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵresolveDocument"])("keyup.enter", function ColorPickerComponent_keyup_enter_HostBindingHandler($event) {
              return ctx.handleEnter($event);
            }, false, _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵresolveDocument"]);
          }
        },
        decls: 28,
        vars: 47,
        consts: [[1, "color-picker", 3, "click"], ["dialogPopup", ""], [3, "class", "top", 4, "ngIf"], ["class", "saturation-lightness", 3, "slider", "rgX", "rgY", "background-color", "newValue", "dragStart", "dragEnd", 4, "ngIf"], [1, "hue-alpha", "box"], [1, "left"], [1, "selected-color-background"], [1, "selected-color"], ["type", "button", 3, "class", "disabled", "click", 4, "ngIf"], [1, "right"], ["style", "height: 16px;", 4, "ngIf"], [1, "hue", 3, "slider", "rgX", "newValue", "dragStart", "dragEnd"], ["hueSlider", ""], [1, "cursor"], [1, "value", 3, "slider", "rgX", "newValue", "dragStart", "dragEnd"], ["valueSlider", ""], [1, "alpha", 3, "slider", "rgX", "newValue", "dragStart", "dragEnd"], ["alphaSlider", ""], ["class", "cmyk-text", 3, "display", 4, "ngIf"], ["class", "hsla-text", 3, "display", 4, "ngIf"], ["class", "rgba-text", 3, "display", 4, "ngIf"], ["class", "hex-text", 3, "hex-alpha", "display", 4, "ngIf"], ["class", "value-text", 4, "ngIf"], ["class", "type-policy", 4, "ngIf"], ["class", "preset-area", 4, "ngIf"], ["class", "button-area", 4, "ngIf"], [1, "saturation-lightness", 3, "slider", "rgX", "rgY", "newValue", "dragStart", "dragEnd"], ["type", "button", 3, "disabled", "click"], [2, "height", "16px"], [1, "cmyk-text"], [1, "box"], ["type", "number", "pattern", "[0-9]*", "min", "0", "max", "100", 3, "text", "rg", "value", "keyup.enter", "newValue"], ["type", "number", "pattern", "[0-9]+([\\.,][0-9]{1,2})?", "min", "0", "max", "1", "step", "0.1", 3, "text", "rg", "value", "keyup.enter", "newValue", 4, "ngIf"], [4, "ngIf"], ["type", "number", "pattern", "[0-9]+([\\.,][0-9]{1,2})?", "min", "0", "max", "1", "step", "0.1", 3, "text", "rg", "value", "keyup.enter", "newValue"], [1, "hsla-text"], ["type", "number", "pattern", "[0-9]*", "min", "0", "max", "360", 3, "text", "rg", "value", "keyup.enter", "newValue"], [1, "rgba-text"], ["type", "number", "pattern", "[0-9]*", "min", "0", "max", "255", 3, "text", "rg", "value", "keyup.enter", "newValue"], [1, "hex-text"], [3, "text", "value", "blur", "keyup.enter", "newValue"], [1, "value-text"], [1, "type-policy"], [1, "type-policy-arrow", 3, "click"], [1, "preset-area"], [1, "preset-label"], [3, "class", 4, "ngIf"], ["class", "preset-color", 3, "backgroundColor", "click", 4, "ngFor", "ngForOf"], [1, "preset-color", 3, "click"], [3, "class", "click", 4, "ngIf"], [3, "click"], [1, "button-area"], ["type", "button", 3, "class", "click", 4, "ngIf"], ["type", "button", 3, "click"]],
        template: function ColorPickerComponent_Template(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0, 1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function ColorPickerComponent_Template_div_click_0_listener($event) {
              return $event.stopPropagation();
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](2, ColorPickerComponent_div_2_Template, 1, 5, "div", 2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](3, ColorPickerComponent_div_3_Template, 2, 8, "div", 3);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "div", 4);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "div", 5);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](6, "div", 6);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](7, "div", 7);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](8, ColorPickerComponent_button_8_Template, 2, 5, "button", 8);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "div", 9);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](10, ColorPickerComponent_div_10_Template, 1, 0, "div", 10);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "div", 11, 12);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("newValue", function ColorPickerComponent_Template_div_newValue_11_listener($event) {
              return ctx.onHueChange($event);
            })("dragStart", function ColorPickerComponent_Template_div_dragStart_11_listener() {
              return ctx.onDragStart("hue");
            })("dragEnd", function ColorPickerComponent_Template_div_dragEnd_11_listener() {
              return ctx.onDragEnd("hue");
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](13, "div", 13);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "div", 14, 15);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("newValue", function ColorPickerComponent_Template_div_newValue_14_listener($event) {
              return ctx.onValueChange($event);
            })("dragStart", function ColorPickerComponent_Template_div_dragStart_14_listener() {
              return ctx.onDragStart("value");
            })("dragEnd", function ColorPickerComponent_Template_div_dragEnd_14_listener() {
              return ctx.onDragEnd("value");
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](16, "div", 13);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "div", 16, 17);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("newValue", function ColorPickerComponent_Template_div_newValue_17_listener($event) {
              return ctx.onAlphaChange($event);
            })("dragStart", function ColorPickerComponent_Template_div_dragStart_17_listener() {
              return ctx.onDragStart("alpha");
            })("dragEnd", function ColorPickerComponent_Template_div_dragEnd_17_listener() {
              return ctx.onDragEnd("alpha");
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](19, "div", 13);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](20, ColorPickerComponent_div_20_Template, 17, 12, "div", 18);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](21, ColorPickerComponent_div_21_Template, 14, 10, "div", 19);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](22, ColorPickerComponent_div_22_Template, 14, 10, "div", 20);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](23, ColorPickerComponent_div_23_Template, 8, 7, "div", 21);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](24, ColorPickerComponent_div_24_Template, 9, 3, "div", 22);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](25, ColorPickerComponent_div_25_Template, 3, 0, "div", 23);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](26, ColorPickerComponent_div_26_Template, 6, 3, "div", 24);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](27, ColorPickerComponent_div_27_Template, 3, 2, "div", 25);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
          }

          if (rf & 2) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵstyleProp"]("display", !ctx.show ? "none" : "block")("visibility", ctx.hidden ? "hidden" : "visible")("top", ctx.top, "px")("left", ctx.left, "px")("position", ctx.position)("height", ctx.cpHeight, "px")("width", ctx.cpWidth, "px");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵclassProp"]("open", ctx.show);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.cpDialogDisplay == "popup");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", (ctx.cpColorMode || 1) === 1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵstyleProp"]("background-color", ctx.selectedColor);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.cpAddColorButton);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.cpAlphaChannel === "disabled");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵstyleProp"]("display", (ctx.cpColorMode || 1) === 1 ? "block" : "none");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("rgX", 1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵstyleProp"]("left", ctx.slider == null ? null : ctx.slider.h, "px");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵstyleProp"]("display", (ctx.cpColorMode || 1) === 2 ? "block" : "none");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("rgX", 1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵstyleProp"]("right", ctx.slider == null ? null : ctx.slider.v, "px");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵstyleProp"]("display", ctx.cpAlphaChannel === "disabled" ? "none" : "block")("background-color", ctx.alphaSliderColor);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("rgX", 1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵstyleProp"]("left", ctx.slider == null ? null : ctx.slider.a, "px");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", !ctx.cpDisableInput && (ctx.cpColorMode || 1) === 1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", !ctx.cpDisableInput && (ctx.cpColorMode || 1) === 1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", !ctx.cpDisableInput && (ctx.cpColorMode || 1) === 1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", !ctx.cpDisableInput && (ctx.cpColorMode || 1) === 1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", !ctx.cpDisableInput && (ctx.cpColorMode || 1) === 2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", !ctx.cpDisableInput && (ctx.cpColorMode || 1) === 1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", (ctx.cpPresetColors == null ? null : ctx.cpPresetColors.length) || ctx.cpAddColorButton);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.cpOKButton || ctx.cpCancelButton);
          }
        },
        directives: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["NgIf"], SliderDirective, TextDirective, _angular_common__WEBPACK_IMPORTED_MODULE_1__["NgForOf"]],
        styles: [".color-picker{-moz-user-select:none;-ms-user-select:none;-webkit-user-select:none;background-color:#fff;border:1px solid #777;cursor:default;height:auto;position:absolute;user-select:none;width:230px;z-index:1000}.color-picker *{box-sizing:border-box;font-size:11px;margin:0}.color-picker input{color:#000;font-size:13px;height:26px;min-width:0;text-align:center;width:0}.color-picker input:-moz-submit-invalid,.color-picker input:-moz-ui-invalid,.color-picker input:invalid{box-shadow:none}.color-picker input::-webkit-inner-spin-button,.color-picker input::-webkit-outer-spin-button{-webkit-appearance:none;margin:0}.color-picker .arrow{border-style:solid;height:0;position:absolute;width:0;z-index:999999}.color-picker .arrow.arrow-top{border-color:#777 transparent transparent;border-width:10px 5px;left:8px}.color-picker .arrow.arrow-bottom{border-color:transparent transparent #777;border-width:10px 5px;left:8px;top:-20px}.color-picker .arrow.arrow-left-top,.color-picker .arrow.arrow-top-left{border-color:transparent transparent transparent #777;border-width:5px 10px;bottom:8px;right:-21px}.color-picker .arrow.arrow-right-top,.color-picker .arrow.arrow-top-right{border-color:transparent #777 transparent transparent;border-width:5px 10px;bottom:8px;left:-20px}.color-picker .arrow.arrow-bottom-left,.color-picker .arrow.arrow-left,.color-picker .arrow.arrow-left-bottom{border-color:transparent transparent transparent #777;border-width:5px 10px;right:-21px;top:8px}.color-picker .arrow.arrow-bottom-right,.color-picker .arrow.arrow-right,.color-picker .arrow.arrow-right-bottom{border-color:transparent #777 transparent transparent;border-width:5px 10px;left:-20px;top:8px}.color-picker .cursor{border:2px solid #222;border-radius:50%;cursor:default;height:16px;position:relative;width:16px}.color-picker .box{display:flex;padding:4px 8px}.color-picker .left{padding:16px 8px;position:relative}.color-picker .right{flex:1 1 auto;padding:12px 8px}.color-picker .button-area{padding:0 16px 16px;text-align:right}.color-picker .button-area button{margin-left:8px}.color-picker .preset-area{padding:4px 15px}.color-picker .preset-area .preset-label{color:#555;font-size:11px;overflow:hidden;padding:4px;text-align:left;text-overflow:ellipsis;white-space:nowrap;width:100%}.color-picker .preset-area .preset-color{border:1px solid #a9a9a9;border-radius:25%;cursor:pointer;display:inline-block;height:18px;margin:4px 6px 8px;position:relative;width:18px}.color-picker .preset-area .preset-empty-message{font-style:italic;margin-bottom:8px;margin-top:4px;min-height:18px;text-align:center}.color-picker .hex-text{font-size:11px;padding:4px 8px;width:100%}.color-picker .hex-text .box{padding:0 24px 8px 8px}.color-picker .hex-text .box div{clear:left;color:#555;flex:1 1 auto;float:left;text-align:center}.color-picker .hex-text .box input{border:1px solid #a9a9a9;flex:1 1 auto;padding:1px}.color-picker .hex-alpha .box div:first-child,.color-picker .hex-alpha .box input:first-child{flex-grow:3;margin-right:8px}.color-picker .cmyk-text,.color-picker .hsla-text,.color-picker .rgba-text,.color-picker .value-text{font-size:11px;padding:4px 8px;width:100%}.color-picker .cmyk-text .box,.color-picker .hsla-text .box,.color-picker .rgba-text .box{padding:0 24px 8px 8px}.color-picker .value-text .box{padding:0 8px 8px}.color-picker .cmyk-text .box div,.color-picker .hsla-text .box div,.color-picker .rgba-text .box div,.color-picker .value-text .box div{color:#555;flex:1 1 auto;margin-right:8px;text-align:center}.color-picker .cmyk-text .box div:last-child,.color-picker .hsla-text .box div:last-child,.color-picker .rgba-text .box div:last-child,.color-picker .value-text .box div:last-child{margin-right:0}.color-picker .cmyk-text .box input,.color-picker .hsla-text .box input,.color-picker .rgba-text .box input,.color-picker .value-text .box input{border:1px solid #a9a9a9;flex:1;float:left;margin:0 8px 0 0;padding:1px}.color-picker .cmyk-text .box input:last-child,.color-picker .hsla-text .box input:last-child,.color-picker .rgba-text .box input:last-child,.color-picker .value-text .box input:last-child{margin-right:0}.color-picker .hue-alpha{align-items:center;margin-bottom:3px}.color-picker .hue{background-image:url(\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAJYAAAAQCAYAAAD06IYnAAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH4AIWDwkUFWbCCAAAAFxJREFUaN7t0kEKg0AQAME2x83/n2qu5qCgD1iDhCoYdpnbQC9bbY1qVO/jvc6k3ad91s7/7F1/csgPrujuQ17BDYSFsBAWwgJhISyEBcJCWAgLhIWwEBYIi2f7Ar/1TCgFH2X9AAAAAElFTkSuQmCC\");direction:ltr}.color-picker .hue,.color-picker .value{background-size:100% 100%;border:none;cursor:pointer;height:16px;margin-bottom:16px;width:100%}.color-picker .value{background-image:url(\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAJYAAAAQCAYAAAD06IYnAAACTklEQVR42u3SYUcrABhA4U2SkmRJMmWSJklKJiWZZpKUJJskKUmaTFImKZOUzMySpGRmliRNJilJSpKSJEtmSpIpmWmSdO736/6D+x7OP3gUCoWCv1cqlSQlJZGcnExKSgqpqamkpaWRnp5ORkYGmZmZqFQqsrKyyM7OJicnh9zcXNRqNXl5eeTn56PRaCgoKKCwsJCioiK0Wi3FxcWUlJRQWlpKWVkZ5eXlVFRUUFlZiU6no6qqiurqampqaqitraWurg69Xk99fT0GgwGj0UhDQwONjY00NTXR3NxMS0sLra2ttLW10d7ejslkwmw209HRQWdnJ11dXXR3d9PT00Nvby99fX309/czMDDA4OAgFouFoaEhrFYrw8PDjIyMMDo6ytjYGDabjfHxcSYmJpicnGRqagq73c709DQzMzPMzs4yNzfH/Pw8DocDp9OJy+XC7XazsLDA4uIiS0tLLC8vs7KywurqKmtra3g8HrxeLz6fD7/fz/r6OhsbG2xubrK1tcX29jaBQICdnR2CwSC7u7vs7e2xv7/PwcEBh4eHHB0dcXx8zMnJCaenp5ydnXF+fs7FxQWXl5dcXV1xfX3Nzc0Nt7e33N3dEQqFuL+/5+HhgXA4TCQS4fHxkaenJ56fn3l5eeH19ZVoNMrb2xvv7+98fHwQi8WIx+N8fn6SSCT4+vri+/ubn58ffn9/+VcKgSWwBJbAElgCS2AJLIElsASWwBJYAktgCSyBJbAElsASWAJLYAksgSWwBJbAElgCS2AJLIElsP4/WH8AmJ5Z6jHS4h8AAAAASUVORK5CYII=\");direction:rtl}.color-picker .alpha{background-image:url(\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAJYAAAAQCAYAAAD06IYnAAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH4AIWDwYQlZMa3gAAAWVJREFUaN7tmEGO6jAQRCsOArHgBpyAJYGjcGocxAm4A2IHpmoWE0eBH+ezmFlNvU06shJ3W6VEelWMUQAIIF9f6qZpimsA1LYtS2uF51/u27YVAFZVRUkEoGHdPV/sIcbIEIIkUdI/9Xa7neyv61+SWFUVAVCSct00TWn2fv6u3+Ecfd3tXzy/0+nEUu+SPjo/kqzrmiQpScN6v98XewfA8/lMkiLJ2WxGSUopcT6fM6U0NX9/frfbjev1WtfrlZfLhYfDQQHG/AIOlnGwjINlHCxjHCzjYJm/TJWdCwquJXseFFzGwDNNeiKMOJTO8xQdDQaeB29+K9efeLaBo9J7vdvtJj1RjFFjfiv7qv95tjx/7leSQgh93e1ffMeIp6O+YQjho/N791t1XVOSSI7N//K+4/GoxWLBx+PB5/Op5XLJ+/3OlJJWqxU3m83ovv5iGf8KjYNlHCxjHCzjYBkHy5gf5gusvQU7U37jTAAAAABJRU5ErkJggg==\");background-size:100% 100%;border:none;cursor:pointer;direction:ltr;height:16px;width:100%}.color-picker .type-policy{background-image:url(\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABIAAAAgCAYAAAAffCjxAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAACewAAAnsB01CO3AAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAAIASURBVEiJ7ZY9axRRFIafsxMStrLQJpAgpBFhi+C9w1YSo00I6RZ/g9vZpBf/QOr4GyRgkSKNSrAadsZqQGwCkuAWyRZJsySwvhZ7N/vhzrgbLH3Ld8597jlzz50zJokyxXH8DqDVar0qi6v8BbItqSGpEcfxdlmsFWXkvX8AfAVWg3UKPEnT9GKujMzsAFgZsVaCN1VTQd77XUnrgE1kv+6935268WRpzrnHZvYRWC7YvC3pRZZl3wozqtVqiyH9IgjAspkd1Gq1xUJQtVrdB9ZKIAOthdg/Qc65LUk7wNIMoCVJO865rYFhkqjX6/d7vV4GPJwBMqofURS5JEk6FYBer/eeYb/Mo9WwFnPOvQbeAvfuAAK4BN4sAJtAG/gJIElmNuiJyba3EGNmZiPeZuEVmVell/Y/6N+CzDn3AXhEOOo7Hv/3BeAz8IzQkMPnJbuPx1wC+yYJ7/0nYIP5S/0FHKdp+rwCEEXRS/rf5Hl1Gtb2M0iSpCOpCZzPATmX1EySpHMLAsiy7MjMDoHrGSDXZnaYZdnRwBh7J91utwmczAA6CbG3GgPleX4jqUH/a1CktqRGnuc3hSCAMB32gKspkCtgb3KCQMmkjeP4WNJThrNNZval1WptTIsv7JtQ4tmIdRa8qSoEpWl6YWZNoAN0zKxZNPehpLSBZv2t+Q0CJ9lLnARQLAAAAABJRU5ErkJggg==\");background-position:50%;background-repeat:no-repeat;background-size:8px 16px;height:24px;position:absolute;right:12px;top:218px;width:16px}.color-picker .type-policy .type-policy-arrow{display:block;height:50%;width:100%}.color-picker .selected-color{border:1px solid #a9a9a9;border-radius:50%;height:40px;left:8px;position:absolute;top:16px;width:40px}.color-picker .selected-color-background{background-image:url(\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACgAAAAoCAYAAACM/rhtAAAAh0lEQVRYR+2W0QlAMQgD60zdfwOdqa8TmI/wQMr5K0I5bZLIzLOa2nt37VVVbd+dDx5obgCC3KBLwJ2ff4PnVidkf+ucIhw80HQaCLo3DMH3CRK3iFsmAWVl6hPNDwt8EvNE5q+YuEXcMgkonVM6SdyCoEvAnZ8v1Hjx817MilmxSUB5rdLJDycZgUAZUch/AAAAAElFTkSuQmCC\");border-radius:50%;height:40px;width:40px}.color-picker .saturation-lightness{background-image:url(\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOYAAACCCAYAAABSD7T3AAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH4AIWDwksPWR6lgAAIABJREFUeNrtnVuT47gRrAHN+P//Or/61Y5wONZ7mZ1u3XAeLMjJZGZVgdKsfc5xR3S0RIIUW+CHzCpc2McYo7XGv3ex7UiZd57rjyzzv+v+33X/R/+3r/f7vR386Y+TvKNcf/wdhTLPcv9qU2wZd74uth0t1821jkIZLPcsI/6nWa4XvutquU0Z85mnx80S/ZzgpnLnOtHNt7/ofx1TKXcSNzN/7qbMQ3ju7rNQmMYYd/4s2j9aa+P+gGaMcZrb1M/tdrvf7/d2v99P9/t93O/3cbvdxu12G9frdVwul3E+n8c///nP+2+//Xb66aefxl//+tfx5z//2YK5Al2rgvf4UsbpdGrB52bAvArXpuzjmiqAVSGz5eDmGYXzhbAZmCrnmzddpUU+8Y1dAOYeXCtDUwVwV7YCGH6uAmyMcZ9l5vkUaBPGMUZ7/J5w/792/fvv9Xq93263dr/fTxPECeME8nK5jM/Pz/HTTz/dv337dvrll1/GP/7xj/G3v/1t/OUvfwkVswongjdOp9PzH3U3D3zmWGnZVXn4jCqs7wC2BKP4/8tAzkZsoWx6XrqeHZymvp4ABCBJhTQwKfDT8gzrZCIqi5AhiACjBfEB2rP8/X63MM7f6/V6v9/v7Xa7bYC83W7jcrlsVHIq5ffv30+//fbb+OWXX8ZPP/00/v73v4+ff/75JSvbeu+bL2WMMaFbAlpBNM85QX+ct6qoSqkPAwuQlBVKqGNFSUOAA3Bmu7gC5hNOd15nSwvAOUW7C4giUCV8Sgn5L9hNFIqTsp0GxI0ysioyjAjkY/tGJVEpz+fz+OWXX+7fv38//f777+Pbt2/j119/HT///PP49ddfx8fHRwrmTjV779EXu2px2xhjwtdJZQcAWQIPLPISsMJaSwiD8gzIKrwSyATE5j5nAbR5c1dBUwBlsEWW0h6LqiYsqFPAQxCyRZ3wOSARxmlXMX5k64pQfvv27f75+dk+Pj5OHx8f4/v37+Pbt2/jt99+G9++fRsfHx/jcrmUFLO31gYDWblxRIs/TqfT7ousxJsAxXA2Gc7TA9XdgfdoHbFsj76X2+1WArgI1ageGwA3qupqoHsmcbI6Fu93quggFa9d7LeDtgKfAFHBJ+NEByIkcJ5KervdTmhhGcgJJSZ5vn//fj+fz+18Pp8+Pz/H5+fnmGD+/vvv4/v37+Pj42N8fn6O2+1Ws7JjjP6wraMI5E4RZ8x2vV5TSwkquotV7/d7Tz6HFWsD/qNcdw0CQ3q/321c686TwDVIdbuy73zNldhSHb8I2klZznm+InBS4U6n0302aBFsLhHDAKJVJVglfI9jhvu53W53sLANYNxAiDA6MCeUHx8f9+v12i6XS7tcLqcZW57P5yeY8/fz83Ocz+fnsSmYUyknWEG85WBst9stzSLyMdfr9Qi08iY15UZ0LlDGLhR3o5zK2j7OPUTD0E+nU3tk7Xb/16NFbhloAMuY1zjLUOO3BKeIDe+Z8s3/J4gFo4TM5jPmuRg28foUKKVSwo16TgA5npywcWLHgYl/Pz8/73/605/ab7/91m63W7tcLie0sZj4mao5gTyfz88E0f1+j8EcYzwTPEG2cqjyfHNF0M8fuqEiaOVnRzZZQNh5fwQyHg/HDGfJo89Q1zb/quu5XC6773I2XKfTqd/v9+d3wuqWva/YTdUdEV3fhIv/Viyps6YE3x3r43K5bJQS66zaxVGFsvd+//j4aF+/fm3fv39vt9utff36tf3+++/tdrudvn37ZuNLBaaCMgUzC+rZRiFowxUuJI8YMqcCp9Opq5vagaYU6lGJA1XQqejchw6Cj0Gw5nYBrGw01A2O206n04BGouNNyTfp/FwElhUey6nXrIKw7QQWddxuN2ldL5fL839gSPF8ahu/JvBO48CPSuqMf8Vp9/P53L58+dLu93s7n8/tfr8/39/v9/b5+TkhPJ3P56mQ436/j+/fv+/iSgbzer0+AZx/5+88bv6OMda6S5z6kd21fYC9dxv7cIJJ2d9AOS30fPMzyHiTM8B4DF6XUlYHp4KQW3W+1t77MNB1vGHxWq7Xa7vf78+y5/N5A+H1et29xuP5dbYtyaRu4AksbPq6936fjRzXRxBbPr/b+b18+fKljTHaBBBfn8/n0/1+H1++fBnn8zm0sB8fH5u4cr5GuBhMVk0EEn9RsctgVhM+ixlJtMA23R8B6yysAstBOgFXIKKCMIgToMqNEu2fYMH7ztc732dQKkCj1ytAZtY0Kx8pIr8GGJ+AT3V+2Hirhl++fBmXy2Wz73w+b17P8p+fn8/tUwGVleVkTyUb68DkfayWY4zxNRihU4EpLJPZVrK+u7J4/mgfKqeLW9X2REWlItL1diynbDDb3+jXgYjQqn0rrxWc+NkILP7F7xIbMvx7vV53x40xnlbWJF12ZSag/N0pW6t+ZzmOMzHjajKwDfond78zYTdfq18up97zr2q8v3IioBprRtBl0EZ9og5WBRGOdOHjIjXF7UotFbgOWnXzIJyzYvjG5IYgsmMOxHkz8OsMSrVNWeq5T8DaOcbEv1Od5rbs9aO7YvMet63EkF++fMExq+MRl4/L5bLZN/+ez+fnZ6KazuMqXSQVO5spJXflHAIzes/xJseckRJiDMog9d6VfRrqXMr6KpVV27jRwJacGovOAM1zMdQMnwK1AubK63kdCChvI1C7g0z9nf/D+Xze2Vj8H7Gx4P9duQlsYCrqyN8XqG3Hm/10Oj3jw/n+crlstuM+jPmmxT2dTuPz83Pzt2pn1XsEHX/bnPaVqVmh0xwOt0o6XLLAHePUU203wHfcrspCwmV3TryB5s0Mseeg97x/BwzCjBlbB+pRAPla0BVQuT6V6QHdBlj3d0KG147b+DqxQeUymDO43W4dQar+TIjwmAd0z8/h65vf0/yLv3Pb5XLpru/ydDo9s7ET0I+Pj6dKK9VUEIeKWQWPAOrJ8LKd4vE+t91Y3e7UFlWatg2VwJnb+HPmtvm/sfK59/OaWF3x/eP1UPHvA5DDYDpYXfb0drv1V2DkBkxtw/tEWVVlXWdC9pFYs5/jfh9dS/16vW7s6lTG+TfqsxSJHxkXXq/Xdr1eu4LsfD6P3vsT3N77DkL+zPm5jSdKL4zR3AxQd6rHkLkYlSowsrq7znzu6wSwdsMJOXmA5fBcjxtgMGBYHlr5zokhtsMCTgXLQOW4XC6dEyEMprL8mAQzXRgduix2yZzorxkYsDn3hB1VeMLGsXsVtgl2pW8S3svk0vw7R4hNaHvv4cACl5HFzwIH0Kc6zu4XjDPR/jpAVxWzO1Xk2DDb3vTcxeGU1iWZHkmIDWziWKvirCJ4Dravs6IJ/GG6cTqWdXDy+fArQDVVkLqkVjAoZIITdmmIqXwqa95N3+MGYoZQdRVNO53Y1xRkhO16vY7eu507Ca9lJnbGpxOemQhSw/AQsmmp5zU9BiU8G6wvX76M6/U6Pj4+do0Bz4CpgiknTUeDqwlKBmg3u4OVjrZ1A+rAcgaejWq6eJCvCYFDONSwOgHX4EQRw8lxbzDOdEK6gZ3Hk1b+8g2o1JFtKXyv/fEdTXuWjWXdAZiBp6ADeDrCFiim7B6ZFneeI7Gvm/PMkUDX67W7xI8b0D7/v8dA9qfN5oaCf74WZjH0mf1cmfY1Y0JUFmVrTWu8uzkNcLtEj7u5FXBTkfC6GOA5q8YMxO8KVvF6sAVGdcrUbsKODcQKkLMOMdmlxum642YrPm26AlhZW1YB1R+rrGswE8TaYAWeUMxdf+WjwSvZ2Ef3ytOyfn5+PpVPAaqOn43MtNBqvmjjxbjM4lZjZY4gqNMI5ktaW/sYKNwS+9lFQzGihmMCKPa7+Z0V6Eb0GRmobtpX8JljWu5FMLN5ja6hG9kwQgZqf5+1NH5UxzkFReCdWhJ8XdlGUkxO7HRlYRm4mVO43W7ter12TPJEw/rmEN3L5SKHIWZg9mz+pUoKOYq5bJTJdX2gme1UcxMZQFaEQIlHct32M+Y1BzGkGuzfiyAN9z+ugplZ1symCrDCYYkGxDTpI9RzBy0rHyeDUC1nWaeUaD9n4xkNyYMBDZtzZ3B++fJlY21XFDOcARJlabOyiS3uCpLI9jrZjCDkaVvcCCjwognKShWdzXZWlZMvVTgD8LpqlCLrqgbcB+qYwrgKYpT0ccCqbKyCValkEabn/FynogCrPKfqf51xJ7sGB2ZXcZmxoSOztjx300DZi7a0/2AIR0UlBag9SuDw6KcAzlaB7vHZvWpjK90dyrq6bKyDUZQbR0B05biLQkHIcSUmgIK+SwuqgHCnoio2RQU1yj+BnBy9pphVKLGyC7ZzFK1pxWK+E8IhVCWLN/uLtnUU4ayoYLoaANz8FdtaSvY4pV0BEW2ls61czqllBKpTyKgMAhrZ1cdc1RROtPmvWNkdcKZ7ZKxaWjiPLJMpp7OZKxA+rqG/oJLjxf0pnJlqLoDZo3gyU0mKGys2taKecj/d1C+rJSplBqlTyAqgR+D8KjKlmRL2gtUcAdCtsL+ijCNT1oqqqkH2OHEbG5sDFnUg5Aa+yLou2VU1ptj1S2ZQqv1ORZN9IWzRfgaRBxKoBE8UWyqlJFtrIc0AxNjSjed99CTY/XDfSzCz5M0IZoVEsWnPFNTsl8ooVC1TzbGgqFZNDSgVwKK+1sGDMKqxZCWGVMDysiEr1jVSQJUYwj5iHOlThdHt44SQg9CN+nl8D90NMIgAdgr46JqRiR9I8vRdFvbr17m/yxUMKjNLMiVUADwu2CWGhhi+F55TWM9M9cogzms1dnM4uOF/LAEYWdcqnM7yFmyq3IfwmOROd7Y1iFWtOjoY8To41mTV5IysgFFuRzsbWFGbNIIJCDv1dOo4lZG7jWBwRFtVTKuWyeCByJKOan8oZ3ep9XddNl0tDuaywLz9cXPYeDAA0SpkBO9sbVcTOVWldPv4uyzEkzxHtjvonHoSkFEWNoo1d8DhcQputd2ppNon4BzoAiJ1hBFQg0dVtdbGHHDQWushmNEQukLM2QO1G2Y8bgTXqFhcBJj7EjPgcPts8US8qPpPB/dXznOh5Z438tzH5ec6QgrOKrRRfKmysBmUDB+PhYabMlVPER+GCSITTzr7am2tArH3bgcEzPJm+cr5jJ4NnHNFDVrFXcI5Le9k5Jnw+bedbV+FfRzZIHaOOaOsLY0/7UGs58DjrGwKMIMFIGzOEW1/jGsdAtCN6hEAI4hBe9YXeRROBSVPAVPAqvIM5bx5hVKWAMP6zBRy3iescridVdFBinBxXDnG2GRY2XbCvp1lhvGtO9Bxu5h908XQu42lnSArMFdizMim8uwRCxPGnnOS8lwpnbOiDqTAjsrRN/PcoAScCbaACqVM40ylnjjTBs+bwWlAG23/UKbdkiwKWIQPGzWaczpoSlxPEj822cNWkpS7FyzsDrqpfgpG3jahw2vgbaSQAxuLWZYt7JzyNe8JoZpNAcvDFOdw0wqYT9AK1rZz/DdbSlLPp0ryIxgQJlK9AZlEq7IOXpohg9PIhrCng88JsOxiV4ZWAYfg4sikx/8ky2Z9l862uqwrfscIH8+ugTmVGyiddeVYUgEMn4GZzg14EwIsh9sx2cKKiWXReuOE5gzGOQgdlRKVVdlevqb279Xq0Qnsts2VDaBO0coezsruWtHApu6sKG4IBhN0aGU2kLrMKGRTN3HmbCDwKV14zvkMEDG4QfZVspVlaNU2mhc5TEZ3N1h/zqTheuLpW05ZWTGVjb3dbnNmxKZBnN8JqidaVLKAOyARNLS+MB54Z2+VaqoMLKroVBlngefnTPAcoHNWCSvlfA8CI0HEmBNBnBlXyMrzU7A7WVm94PPqQ2gmqKx+WDGsnvilmcSOBJqOK1nYyAIzuAyesq3UdSK3KfWcYKD95HmfYOU3qser2CtYEUA+FpfqdNvgPBZUBhDrGONRVlQsh8rLcaUCykHG0OOUwTlLBrsh5soEMGezi1E4HRVt1icp5wZEFXdibCkG8Y8vX75sbO4E0iom9z+hjSiOfy3DhpXItpVhE+UGQdvoWjtChmrGHf4YAzKgBNnGtuJxFCeGdhUAfQLLK8kBYAP6gvFJZajMG3Xkycy8KuC0q4Eyymwtwdxdv2M0mIBtK0LKnf640j00Auq4gUkdWGlhs22qJc6dZCsL19oxnlTJG4SYVRIGpD8TPFBuM6OElbS1pldid4mGAyN6ZIupbC5bXJN9fdpbThSxLUaI8IG1XIYBxW3Tjs6KQosKcxfxcQmdnwRGM10GnFcCy2XYunLMyAkdgk4mePiczsLygthcBut6goOqS7YVFXADLjaosB6s6ofcZWAZSIRYqSUkizYwttYab3vUOQ9w2HRxIIg8WwRVeE68xi4UtL3zRphxplzwuZrcqYCq1I3jPI5dnJIygEohMbPqVJSzrwzxBJTs5zN+ReUSgxikPQVF3JVBeNQxbHENrEMNvEdFZVV9lH9+ORGEsNZQpyTNc4C3AG7XF4ngzq+DrO2zbuaaOXgdaFcdkEotoSFBVX2qJ0C8OWZeG4KGlpghA0XfTOPCqV2qqwQ26QWfF2PMLhI2w1lVAa2aPsYd0za25MQRwgcZN6uQDCi+ZxiD4XEM2kZxOT41FnZnaRlcpZouzlRqqdbQVWopQoSB58RV50lBNrHi/AwXS5LrwDVlpY3Fc3ByiYGc52Trist6kOXdwInAQtJpp5QchyaquYOV7Su+fxVMaV3dc0RE2S6mUY0gLt2pMcYqrKIQ9w2l1gpQUMtQYcmmbt5DTNxdhnUCjQqtbK9SUSzvrC0mmhhE1e2FS2+oxypy/ZASutkmtjx3vcBC24PX65nbqkBCRhfjS9kIYPnee8cMagVOhI/3T1fAmdtAWZsCswTJCkQVNa0qWKSKPOpHAUhD9DrbVcyoYkwqhvh17vYAayXLQyKGYdxlUDFp494rBXRjYgO17DDYetNIUj/ezp6S0lnlpEwsWmJMkOwsKXeZKEAjIHn0EQJISaRBcO6UMINz7p/bEjjnw4ft+xmDvksxX4G2rIris7qaeKwAFMP2Oi7n4criuZwtpSUwpfLxSnORSrIqusc5ZFaXysqRWjiZ2DyAWEIL35tVSoQElFACjOeGGSE7AHEQgdo/LSvCOgGBvkxsmDbvlS3Fp5vhaB2TAGqRKrKKMrhLVpaGzEVjZ0OQxDhaCTA+QyRR1d15aQzrJntL3RibsipjG6jlgL4yqbS0sNYg1e84vhbBVrElK64CUcWYXDfKxhpIuxiVJZUxsbMy/uRBKTNRQ4kQ3LdRYLS0rJjRPlTPqY6gdJsEDc+aQXAn+HgsNUCbRuF0Oj0zwnA7bWDkbhO5Ens00qeQhS1laBMl5M/cAaxsLF8rKyql+Tf7ELLEGu/ixiimdCvo0TjfpjKwaggen4eh5v7LokLKbLuyvHhcZG8dhGrEDx7Hg93ZppJF7qBqO3iVveXEDQNInzeoe8Yq6ePaZBZ2JviM3W2UAGotekRCAGq4EkF1X3DOnR11yRsBL1tRa0PVcZiNFXZ2c34FskvomInQQ6lzpJoZbJxk43NwKJFBquJSsrByHydxKOnTxQASBmS3j+JMnsHSla3Ec6K9VWoJVn9zfjwOM7hqYAAqJQwE2a3nA48J2QGegRkpZNivSY+ys3EkKd4oJIwsvIHl3cWgLt5k4NH6OmtLWdpurOkwEMupYc7eMtDRhOcI2ui5JhVIzXzLyto/GAPuZoyo8wkoduVgJglCt7OhGbgID4Mq4si+63zUS1FuFFXFlqyaj2emHlLMcBqYu0FMuR28BbB7lOxRMSiCQXFhCKuwkhZ+pYDiGSgbsKKV8MiSRsuHSIWM9rklRiIlZZuqXjsQK8ooYJMgq3JKWVkhHbhsVxFUzthOWPkYijcbx54IKsSdT+uLr3crGKyoYgFiGR9iBk4kfloUX+JIlQRQqabmpgnhqtpQpb6RVQ1WH5DnrS4hEoGZqaerQ2dhFbz8XePxShmDbo70eISjoorO2vK8SJXI4SUmEU4zWKDzUDtWTYw7xXlbSTEj4FRg7zKnKoGRALv0Gs9Tgc1BpCywGZRQAtqVz2xrBcAMzEpfZwFSa2G5W0QBFjSMapWAEFa3HcGN7CxDzECyIkJ97qwrqWNTWVo876PPsjPkj2wvgroM5lLZKMETKVql/CvnWVFiFa/SzJUQwkoZsr67Y6vlSRV3/2tmNTOY3vnaxYwMuoPKqdzR1w7IqHymlPxaAThfU7Ko2ZXYj4AYJHL+kNdKwRQYESTRa5fsUZ/rVC1TMTyWVyYoqNtuzaHsMyv2tvoarxdfqwYgU1axFo/cnql1FGsqK+uAROV8BX4GU8WcZTATi2q7Qcyi0O0V+GhWBMNRUkn8H1SsWVE5By3Gi0ECqUeJoBfAtDa4amkdXG37AGP5Ggeb84p7UazpoKRzdFzeQ8HkoHGxprKy/Hpm5t12p47J6xTYDEz7uINEXSuxYXvFskYAc+ySxH9sf5ftKzU6IbwVBcUGg5e5FMCEXSErZR0wGayV19woM9guPjTqJdVTqR4uE4nJnLldWVkECCZLd2VLF+xtamex7IpiriSDUpvrpn9lrwGMCHyppMH+ps6LILsuFGUj1XEOXiqbqSHPUKnClpWV68kqtURVNDY4TNaocykoYeTU5ngGEQa/S1DnnE4AeXMcKjHPAmFVjCBENaeyLVNHfr3px8xUstJ94hIpfH4HKE/eDaArK6lSyVVFbdt1gxTIVk3pppVlFXi4pEhVBTObquohU85MLXn1iahvUkHJjSCMc01tLFveVVBx0DodM6jftCu7DOtIzYxrc0qp1JGP2ayYFz2Gb6HvMrO8cnGtV6Gjm3uImSfD2GpWK6uowbZGMxFKQCo1pOMtcMXFpRst+hXGoAomF3sSTBGgTglbBKWwsQ3tZqaYSp0Z1CimRDWFcCJUPYJ00BI5FkKYNoifuQxmN88SWVXWLMaUqqqgC0BmQJR6sk3u9NCf6jYLXxAfqsYEgVLAhRY2AtgtflZNFmFyhxdrLkAdWlk4D88M2ixHyepIdhMHrG/iR1ZGtq0MGpbDbRPYOXeSY1M6Ny4ZstvGSktK+XbFPATj2D371saPEsAMXhXrsZ0km/XStkhhMyBfsa6uXFZe2VCe+YMr1+GKgwrQyNYq1VRrB+EizAow6NsdNKcyVEkYeM73ys6q4kAHp6BiFklTkIrVC5oYV7uzwOGCz4UJ0Stq2lWMJy4wtb+RetL6tZFicnJmBw5UjCvXXMZVJX2MQkbf+XN5EWd78Vz8/JEsMZTBiKNzsm1inLRUQ74H4NidaqI68j5sAFgxcRveC7ieLJXfQYxjZZ2CsiWFewZXJmBIlZ1tdtrX4hSuateKso/RZOtOKW2nmq1oTzeK6dRWAWu2NRVb4hq0SXm1GvtugHrbr5IXqmSktg5CuDE2MSlPwsY5kNE2Wp3AqiZbWVLAxiBF+2iBZbuNj6MB6rsMLC7FyasaYDyo7KkoPyEtw3pEMXfPvxAJi2jAQQgjrz0rLIZSWZlIoNhwd5xK4AR9mYNjWAaLrnuImJeBVN9zBORObVvbr+mTTfFSEJLSRnHo7hEJoIi8MFqjxmvgmF5URZz4zLFgZZ8Ctu2X7ggVccKm9gVxIsOHqxXgNMKnFWZYnf1dBnOhayXq17QwFlWW09eNKyVJFmXqaONGA5aCegMbJ3UUkGY1ic3nKWgjq8qfVYGQG1gRt6rs62a6HiqqUOqdesK5NmX4nGofJoiE1d0dF9lVVkvT1/kEEaaCoYOwFpcVcoLM+7669PxC9rWqktH0sWUYld0VCpuBZ/stVRcGgy9WX2+U1Qthi9SzAqSxzZsy+OiFzBYnySGV6Gku44rD8BCOZBV3BvD5+AKRHNwMEsB6EzHnJpkTAeiUlEGkcECeB6GDZTp5YEJTlvdrknxYjTllMkfNtXwDjM7uVjK5JXUUn43rrqpK2jytaxHW0M5G8DC8rtHMYs7KSgduVQMGTYFqFvVS6rkD3sDJ46afdYFwoq11AOKCBLhvwoUgc8IGANycR6knZrdJPdsuxnyjfd3FovTlRMdEdtOl5CMV5EHsXQBis7TOwvIDZaGj2Vnpbh7cpK63VwYEMLwqbjzyl699sawFFkF1yqjUU31HfC6sW1ZFVFuXVXVgz9keEaw0ys1lWfm+azQAQSWA+hKYVfsZjPncAcUB9oIayy/UZXRNckDGji77GsWbvBo6tPrWPqOyVkBUq+INeqpzNdYs/u0ifh5qmpqIW+33JVSUcwY70KL4U9lYdU6ljtSls7lmfi9g3YzeQfVkaGFaV3ODCnaD2N8wsEDFklE3RzM3ZghdYkWHsszq70FIecnKkVkt8ezMzRq9bkGuKojRLBVSod3Y1yPqKgYW7JRQTPVyy5xIYLjOgxgT52RKJUY1dOrIiRd4futQx/A5AcSmEjz0vFWrkLzvbWAu9HOWbGgxFk1VNTpnBKk6TgwisI/HcxYXP1uAWO72ULFlBTq+aSu2VTUs6hrxM2CF+hEor1VIA9ZmFUaab1lSSgZsVs4sxzHlVLoJHr9H4DhONTkI1XC0/wiY2NoWAG5RlnHFnq6oLccpQddMuJ/O17JVA5OHLi0BqCztq7Y1++ucCd98qLI8MIHBV/cKjxQTme3hFBS3MyCqnDsuym2o80HjvFFTtrURmNaGJsmVahImjTsUXKtQZTAVs7Mvv8/+fzUrZAXcLJ6M4koe6XP0b6SmWWNDzyUpQ8bl+LtWx4tuqZ36cRYV3yuVxPNwvIiqiQCSmu7srgTzR6nkyhpCarXwFy1vGd5iP2cY06lFr5Njhhg1Y6+NB28ftbK83s8rf7kLJbKwDFPbLg25a0AdZJEiqr5phixKMDlRUtcssq1hriLqGoH+zeNgVm9OemjsETV8JdF0NHnkIFxWY1OB4Yrp7rtWJ7NgAAAPXklEQVQ3oNs5nplyVf8u2FoLu1JrHveaZWQjqAkshtFa2gzsSG3Zpkbvg3HafF9slPPlldjFlK80Gysm8Mr4MPhneNWENPGjAIpmilTPATdTRTXlCBYHYAQuPwA36xIpWtGN4q3Y2MhiGsUpuSSnlEJRD8PorC7CFYVw+F51qThgabxsTxWzCGY0ZSsb3lfqAy0OPNjNy8xiQQKsHYFQ2HBZVvVbBuq3m1oWKajqaonsM6uZUr6CjXWNZ0l5E3h3jURma6kP3MJIiy1Lm+kahQq41N2iZja5sjtlLYNZHZrH6qUGm4vMbDp6Rw2CFmvuyFkrBcCyMtFqBaECmsHoK9BZ2LA/lJcRqSaDqnaWbrZdGaz3DLgIvBln4woGztbyJGqslwxkhhHrTjTYFXCtOoKS8uLdofVdAbOylGU6nlYpXWZts4nXBq6WxJitMNokHUJnbnJplQm+aGpY2a5GMV2QD1hRubBPFKdumf5OHkLHz0F9luE5kjBjRa0nFE5CUGqHw32MmjZ6xkgINVnSnZ1VZStK2qKlRaLlQgK7uTq7JFXJwM+3SOEKyhZNI+tJ0I5qMYy9k2qJD7dVWdqKXa0CKNR0Ccjg+B2IYu2fcBZJZkMFgM11r0X92wilghFGgzVnexlqB7xL9mS29SiYUVY2nXOZjNBRsyDsQPRWW5hrZ4XcdC4HVWRbjgJr4sFofK5SzjQ7rhI1UebdPdEbj6sqIvTZQZ5va08rABsAW0UxeWytAk7A2KJ9ZpxzCioB24XFtYAeXYxr6anSqhLgppEqWbGwLunTgrV+IjWlL29ljaAl4EQMGsErp4apeZiquwRXLXAqOCeru32mmydc6oWTSWpFAGdzeTB8RTHVMEtlM90CbbQCYhPjq3egYr1FGdYIQjiuDGZ5zZ/AzobKGOyLxti6c4Rwtv2anyWlLICnlLhxJRXt6A5ebDBWFNONbxWZ2d02mnu4S9YECpeppV1zSWRBWxHYzVIv1CXSouwqqX3jBBBDZdYQbpTQW4ZQlS8r5kH4suSRmg2++3JN10x1PaAmEkmtYlEdeGpJEM6kOuCqCR22oSujj5IV2HdT0zj5prLKTjXFAPjdQlyq7xIBxAQP5yMczG4VxAKw0n6ilZ2QBce2pLulkuxxqnoIzFfgqyqjil9S1VNwBrFmeyeops8yOjZUybZdfS8CuaTIJumzs5tODaNtLpFDQ/PcJGweLhmeL1nB0KqiUDScsiUVD89Di3HtrKtSULw3RLiygZD+7sF8JTObgYsrGvDNUFRGl1iy0Ll1YkUc2aJYMog920I8qW6YDCg1Mqk0JHJFKXkbgbRreI+qpYNOZHrVcDUba7pjsphSJNtK6upgRNAVoOS0mugBeN4bIZgHhuPZ/s1ENaX6KsVr+YNrh1Nb7ipR0PE5zbNRegCbrHRUw6Yf07dLBJl1f8KB9as2V1nNqAsl62LBBhehwalerkHmB1JFIEZKSEusdl5JQj1nJlHXSCF342gJ9CYGrXelknJIXqVP8sD+qtplCR3XH2qfKq0ygMp+KnVkKxNlZ8m2YkIlVMiCnXUwl7qznBKSvQz3m3Pt6oQbXO5b5FixCh/fHxUQW/AEcK6zCNqKQnL9sywqmKuwvqSYzT/aPVNNpVyhvRW21aqciCsjdWvBwILUvh5VyCzbWoC1pJjJ680CWsl+udKB6T5RwG1mlohnlpbg47iz5U9ha0FGtmRLFYBtO99y97Ap0z+ZDTAog6kSLZsMHg/IFkkgp6CpvU2U0cYVSdnmkjwBdOmXbxTWNWzuIbipMioVxEckZEoahSOiy2M3K0jcC1LhVDwaqG0ZvkcWqCnrG4GIxykrqlbWdw6LQyBaZR8HmLRIhQWsHswD42ZXVLNkf9l+FlW0HVQ2lwFsC/Z1FdzlQR0KaPfo+Fdfu+/dwVRICu1CGR7AEIiAhc+AZUF0kOBaPxmUqg4i64vQnU4nFDYJ9Nz+1fVXveH9qmr+kPILx8oKcRV/BFbxbE0JMT0kSD4w6L/lNY8ocsqagVdU3A3MjxhxcGuqzsPH4irpaow1q6OyrVjvp9Npc59E91LldboYVzJWdimWfAW2SNEKcDaX2FmBLLA/uKxlmhh613Is1URQApbKfttwxL02q6Onx5pQxSbPojAg+v5hAnN6LHVRDXIsvKtRjiS0qJUyZTAXVbAK82ElFJWaQdVoqUC1Unt7BVaTQudM6SuqexjQJN4+0icaxv/utbKv83ETbT8H8gjcOKxOJmbUa6OOVXht3dFY6rHv9XoNzFLceEA1o8+pKm0LAHPHZ2rYKjFq0hfZFixsqHJgD3eD5n+U0kb1mFjXkn2lvMSSOsNE/CdIAKF0Sytq6urOHUN5gwg4GZosgbmggM5ucra2qrS2Ig1cbiBBcxYzgzUDNLCvL8GbZXNp6ORy3LmS+Kk83zRIAK6A1ioKa2I9NapIuiUFdfC9766PFZUtqUr6KbWk+zZU1a/ZrIXEztrjTOfz7hwKziCeXIaraHtbZIMz+2pGgazCmw4qWAFvEdhodYp0Xq0pV7G1YWYWbO4qhGq42+Z8BYtrLWvluNPpZAeaFFS1vubPgbgxsqcpnAaszBovKaFoDQ8BGtjfUOl4NAG2nmQV04feJgumvX2fsrQEWZghL0JnVdYkn3DOZIeRN86RqPWCmsvGVqEMRnwxQAxwS8EMYo3IzmY2+BCcLp4MKiuyuhImamlbZFcNoNl7tp+RHd18ZjQIRKyXdFRhN98/hyKqwXWNo7O1wiaXoHN108REZZWEq6grnIfjzeg8jdRf1XEL4kkXa5bBjKxoKaljBjeHlVxQ4GaycpW4lDOAKtnTxHAtOfzOtZwHAM7sqVXkV6yu6kap1nHkXKqWF/4XHqjenNKqBjpR3l1ch3Ejg1+EsgdQhsdG0B4FM9sWAVWpuAyiwTPleZxt9VyZVS2qXfReWqTAilpr9ApoWTjxymit7NwV4JTriZyOA9B0k7HFfULourmKYHVnRQvqGL5HMHdqFcR2qWpmcK6eTwx2dipWrviDilr+fKWq3OWRWdHKwA4eu8wjchbeRzFilqjjZN3ufCpfkJ0/scVpnYk6L0PI77lxdWCZ87WiWm7B/AGquQSnujGKsB8CJmiJq8q1pKIVWyqOiTK66r18BN8r74/AE71fdC3yPS2MxdOpnE1tlVxD9JmVOoggN+r4PjAXVFPa3Eg5jVJGFVUGNolH20GVrUB7BOySWq6WqYQdWR92pcFMYMwckbSgCKCqD67DiiWu1g8MQC9ByfcFqW1L+jL714qNCuznoSxt0da2gtWN1G8F0BK0NN0nuimelUF9dIdAfjO44UT3CjQLoUeLHJFTO3gmpRuIIOvwBQCbqNeo3qtZ9iF6xVK13GRlo4zqimq+CGdTiR1uRY8oqgE02hZBa79kZXPMquxRHKla2saZWN4mRqZUj0vLCKhkjKnqOQHNuSZVJoKvAqS1wpEquvWDC1B2ypwrCPsRMEPVTODMLJMDv6qeKXwi2JYV5Sq4qKyvgGsHCLiuj2jR59V8gMqSJ2FJZRXEHVRHj3sFPrct6OpqlW1GpatQdt0GvwfM6n63InsGVFhJGaBqgqqIV6IsXllZgySPq4R3bnt3wi5cv+cN2yqQLW1T95KYVsWWtKk4cB9W53WQQflQYR6Wl4HaJZjvVE0D5yvq+RKgZCs5qdBEP5sD94cAvQLlSgNaSMAtHx88BuNQ41zdFsX30zKbcs0MLD/ihkpQzl0wiTqKLTfbKmCmyYICnK0IbaieC4CG9iSyLQ7cIMGQwau6TKoq60Apl3WN40LZpca1CKKK9VQyyIEn8w0F8F6CL2h8o3ixGwC7s7EWzCOqmcApYxYD4jsAzVS0sl2t98pA7vrKophCVSonbYpgH6mvSn24pTBV4sdtV3BtMq5k82y+IADvUJ0uAlkCVTxIaPm+UNu/qkV4F1TzHXCGrXIAqItBKypqK99VtAOVs64O4ObX7pHLVCpYHcRmwvLR7TvYAKBBN58LGVzDuFz+hQbWgncQyCZAk+VbsPSouf93261iZgmfCpwRbAvqmSqriU2PwhjaoOyYqtIegVXViTsmyta6bGySpY3gyRrpIyAeaWDDxtpsXwKyalMDKNP7YBXMqEskUsi2uC8FNAPxAKTVfT1o6VzM0E0jF+1rWcUuHvdyg7vgoFplX8HpvHpMCOMRUPHzZkInsqlFKNX/EIO52E0SxSzOwob2VmRLW5D1XIU0rbgM1AzWgyC7fe8G7xUAK/taEBat7luqtyP7EmsaJQOj5F+mrnZfCuYCfBUAWwShyd6pMY/vAHG1UqOYpbI/gy5T0CMKm+UO3gFuC85dgfDVeguPDfITrIBLsLrcgdh3CFgFZjaKJ4Iv3F8ANEqvuxR1tVKOgLoCa1jxboBAkj6v7j/icFbA7f4rfRnQDLRViG13i0vqBQrYVqBbADZT0ZpiHoSzvQpopKIFS3sE1HfBWlHXd0H7LnArqvougMtljHBgZnh3Eoz/BKjLML4Z2Aq0+hEJr9jaVUBbvNzCIUiroC7AWmmFw4o5AK3MtB5VypZMSFgs05JyGVwlwBqsEGAAa2ZU1CjUexXGsE4rKriilBvFzOKKo3AuAroE6QFQU3u8YpNXwS5k+1TZt5UrwouN4KiUEw+k3ZWDp1RXHNRqXb21Ts39945yZSg3VnZFNQ9CF3XeZyr5DgBXKiwCMa2MxeTDYXgP1Fsf9QNKZc0k81RJk3r6EQ3rCmBVyLL75EjZ1pIVDHoFtiOAHoB0BdTVylqBsKKKS+AeBXJVLY+CXASuGvO/Auq7GuEjDfGKg1oKa1z/dmmi9I9SUGNhl0AtfulHAawoYrnSkmNXAVuGEhrEVXvUF+A5Ct2PqNOjDetyna4CmeUolmeXLN4Aq7C5Sj10Q7yjgl+t6CNxSRHmI5X+CpwreYB3Qfdqna4q21KdBuc4GoZsn49ZOOiVinwHqK9WzjvgeweEh2AU5+vtxZ9Cd9Wqkh49V18E5oj6vVyn0RStAyGIO5edXRKd5B0VGVXq2yr3xYp+5Ut+C4QJ4P1N339pQMjRejj4vb/Dcr6rQc3O/0rjmtZpeYCBiCHfCemRbNhbK/pNUPc3wfKy5f2D7OlL3/uPhve/oU4T0F8f+VNM2vyoiv0jK+KHQfdHq+0bncz4oz73/+Y6LbKw1o/5B7eOf1Rl/0du9B9tn/9bvrf/j+v0h6ttn2tp/r/4819y4/zv5391uvzzfwDifz6phT1MPgAAAABJRU5ErkJggg==\");background-size:100% 100%;border:none;cursor:pointer;direction:ltr;height:130px;touch-action:manipulation;width:100%}.color-picker .cp-add-color-button-class{background:transparent;border:0;cursor:pointer;display:inline;margin:3px -3px;padding:0;position:absolute}.color-picker .cp-add-color-button-class:hover{text-decoration:underline}.color-picker .cp-add-color-button-class:disabled{color:#999;cursor:not-allowed}.color-picker .cp-add-color-button-class:disabled:hover{text-decoration:none}.color-picker .cp-remove-color-button-class{background:#fff;border-radius:50%;box-shadow:1px 1px 5px #333;cursor:pointer;display:block;height:10px;position:absolute;right:-5px;text-align:center;top:-5px;width:10px}.color-picker .cp-remove-color-button-class:before{bottom:3.5px;content:\"x\";display:inline-block;font-size:10px;position:relative}"],
        encapsulation: 2
      });

      ColorPickerComponent.ctorParameters = function () {
        return [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"]
        }, {
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ChangeDetectorRef"]
        }, {
          type: ColorPickerService
        }];
      };

      ColorPickerComponent.propDecorators = {
        dialogElement: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"],
          args: ['dialogPopup', {
            "static": true
          }]
        }],
        hueSlider: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"],
          args: ['hueSlider', {
            "static": true
          }]
        }],
        alphaSlider: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"],
          args: ['alphaSlider', {
            "static": true
          }]
        }],
        handleEsc: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["HostListener"],
          args: ['document:keyup.esc', ['$event']]
        }],
        handleEnter: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["HostListener"],
          args: ['document:keyup.enter', ['$event']]
        }]
      };
      /*@__PURE__*/

      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](ColorPickerComponent, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
          args: [{
            selector: 'color-picker',
            template: "<div #dialogPopup class=\"color-picker\" [class.open]=\"show\" [style.display]=\"!show ? 'none' : 'block'\" [style.visibility]=\"hidden ? 'hidden' : 'visible'\" [style.top.px]=\"top\" [style.left.px]=\"left\" [style.position]=\"position\" [style.height.px]=\"cpHeight\" [style.width.px]=\"cpWidth\" (click)=\"$event.stopPropagation()\">\n  <div *ngIf=\"cpDialogDisplay=='popup'\" class=\"arrow arrow-{{cpUsePosition}}\" [style.top.px]=\"arrowTop\"></div>\n\n  <div *ngIf=\"(cpColorMode ||\xA01) === 1\" class=\"saturation-lightness\" [slider] [rgX]=\"1\" [rgY]=\"1\" [style.background-color]=\"hueSliderColor\" (newValue)=\"onColorChange($event)\" (dragStart)=\"onDragStart('saturation-lightness')\" (dragEnd)=\"onDragEnd('saturation-lightness')\">\n    <div class=\"cursor\" [style.top.px]=\"slider?.v\" [style.left.px]=\"slider?.s\"></div>\n  </div>\n\n  <div class=\"hue-alpha box\">\n    <div class=\"left\">\n      <div class=\"selected-color-background\"></div>\n\n      <div class=\"selected-color\" [style.background-color]=\"selectedColor\"></div>\n\n      <button *ngIf=\"cpAddColorButton\" type=\"button\" class=\"{{cpAddColorButtonClass}}\" [disabled]=\"cpPresetColors && cpPresetColors.length >= cpMaxPresetColorsLength\" (click)=\"onAddPresetColor($event, selectedColor)\">\n        {{cpAddColorButtonText}}\n      </button>\n    </div>\n\n    <div class=\"right\">\n      <div *ngIf=\"cpAlphaChannel==='disabled'\" style=\"height: 16px;\"></div>\n\n      <div #hueSlider class=\"hue\" [slider] [rgX]=\"1\" [style.display]=\"(cpColorMode ||\xA01) === 1 ? 'block' : 'none'\" (newValue)=\"onHueChange($event)\" (dragStart)=\"onDragStart('hue')\" (dragEnd)=\"onDragEnd('hue')\">\n        <div class=\"cursor\" [style.left.px]=\"slider?.h\"></div>\n      </div>\n\n      <div #valueSlider class=\"value\" [slider] [rgX]=\"1\" [style.display]=\"(cpColorMode ||\xA01) === 2 ? 'block': 'none'\" (newValue)=\"onValueChange($event)\" (dragStart)=\"onDragStart('value')\" (dragEnd)=\"onDragEnd('value')\">\n        <div class=\"cursor\" [style.right.px]=\"slider?.v\"></div>\n      </div>\n\n      <div #alphaSlider class=\"alpha\" [slider] [rgX]=\"1\" [style.display]=\"cpAlphaChannel === 'disabled' ? 'none' : 'block'\" [style.background-color]=\"alphaSliderColor\" (newValue)=\"onAlphaChange($event)\" (dragStart)=\"onDragStart('alpha')\" (dragEnd)=\"onDragEnd('alpha')\">\n        <div class=\"cursor\" [style.left.px]=\"slider?.a\"></div>\n      </div>\n    </div>\n  </div>\n\n  <div *ngIf=\"!cpDisableInput && (cpColorMode ||\xA01) === 1\" class=\"cmyk-text\" [style.display]=\"format !== 3 ? 'none' : 'block'\">\n    <div class=\"box\">\n      <input type=\"number\" pattern=\"[0-9]*\" min=\"0\" max=\"100\" [text] [rg]=\"100\" [value]=\"cmykText?.c\" (keyup.enter)=\"onAcceptColor($event)\" (newValue)=\"onCyanInput($event)\" />\n      <input type=\"number\" pattern=\"[0-9]*\" min=\"0\" max=\"100\" [text] [rg]=\"100\" [value]=\"cmykText?.m\" (keyup.enter)=\"onAcceptColor($event)\" (newValue)=\"onMagentaInput($event)\" />\n      <input type=\"number\" pattern=\"[0-9]*\" min=\"0\" max=\"100\" [text] [rg]=\"100\" [value]=\"cmykText?.y\" (keyup.enter)=\"onAcceptColor($event)\" (newValue)=\"onYellowInput($event)\" />\n      <input type=\"number\" pattern=\"[0-9]*\" min=\"0\" max=\"100\" [text] [rg]=\"100\" [value]=\"cmykText?.k\" (keyup.enter)=\"onAcceptColor($event)\" (newValue)=\"onBlackInput($event)\" />\n      <input *ngIf=\"cpAlphaChannel!=='disabled'\" type=\"number\" pattern=\"[0-9]+([\\.,][0-9]{1,2})?\" min=\"0\" max=\"1\" step=\"0.1\" [text] [rg]=\"1\" [value]=\"cmykText?.a\" (keyup.enter)=\"onAcceptColor($event)\" (newValue)=\"onAlphaInput($event)\" />\n    </div>\n\n     <div class=\"box\">\n      <div>C</div><div>M</div><div>Y</div><div>K</div><div *ngIf=\"cpAlphaChannel!=='disabled'\" >A</div>\n    </div>\n  </div>\n\n  <div *ngIf=\"!cpDisableInput && (cpColorMode ||\xA01) === 1 \" class=\"hsla-text\" [style.display]=\"format !== 2 ? 'none' : 'block'\">\n    <div class=\"box\">\n      <input type=\"number\" pattern=\"[0-9]*\" min=\"0\" max=\"360\" [text] [rg]=\"360\" [value]=\"hslaText?.h\" (keyup.enter)=\"onAcceptColor($event)\" (newValue)=\"onHueInput($event)\" />\n      <input type=\"number\" pattern=\"[0-9]*\" min=\"0\" max=\"100\" [text] [rg]=\"100\" [value]=\"hslaText?.s\" (keyup.enter)=\"onAcceptColor($event)\" (newValue)=\"onSaturationInput($event)\" />\n      <input type=\"number\" pattern=\"[0-9]*\" min=\"0\" max=\"100\" [text] [rg]=\"100\" [value]=\"hslaText?.l\" (keyup.enter)=\"onAcceptColor($event)\" (newValue)=\"onLightnessInput($event)\" />\n      <input *ngIf=\"cpAlphaChannel!=='disabled'\" type=\"number\" pattern=\"[0-9]+([\\.,][0-9]{1,2})?\" min=\"0\" max=\"1\" step=\"0.1\" [text] [rg]=\"1\" [value]=\"hslaText?.a\" (keyup.enter)=\"onAcceptColor($event)\" (newValue)=\"onAlphaInput($event)\" />\n    </div>\n\n    <div class=\"box\">\n      <div>H</div><div>S</div><div>L</div><div *ngIf=\"cpAlphaChannel!=='disabled'\">A</div>\n    </div>\n  </div>\n\n  <div *ngIf=\"!cpDisableInput && (cpColorMode ||\xA01) === 1 \" [style.display]=\"format !== 1 ? 'none' : 'block'\" class=\"rgba-text\">\n    <div class=\"box\">\n      <input type=\"number\" pattern=\"[0-9]*\" min=\"0\" max=\"255\" [text] [rg]=\"255\" [value]=\"rgbaText?.r\" (keyup.enter)=\"onAcceptColor($event)\" (newValue)=\"onRedInput($event)\" />\n      <input type=\"number\" pattern=\"[0-9]*\" min=\"0\" max=\"255\" [text] [rg]=\"255\" [value]=\"rgbaText?.g\" (keyup.enter)=\"onAcceptColor($event)\" (newValue)=\"onGreenInput($event)\" />\n      <input type=\"number\" pattern=\"[0-9]*\" min=\"0\" max=\"255\" [text] [rg]=\"255\" [value]=\"rgbaText?.b\" (keyup.enter)=\"onAcceptColor($event)\" (newValue)=\"onBlueInput($event)\" />\n      <input *ngIf=\"cpAlphaChannel!=='disabled'\" type=\"number\" pattern=\"[0-9]+([\\.,][0-9]{1,2})?\" min=\"0\" max=\"1\" step=\"0.1\" [text] [rg]=\"1\" [value]=\"rgbaText?.a\" (keyup.enter)=\"onAcceptColor($event)\" (newValue)=\"onAlphaInput($event)\" />\n    </div>\n\n    <div class=\"box\">\n      <div>R</div><div>G</div><div>B</div><div *ngIf=\"cpAlphaChannel!=='disabled'\" >A</div>\n    </div>\n  </div>\n\n  <div *ngIf=\"!cpDisableInput && (cpColorMode ||\xA01) === 1\" class=\"hex-text\" [class.hex-alpha]=\"cpAlphaChannel==='forced'\"\n    [style.display]=\"format !== 0 ? 'none' : 'block'\">\n    <div class=\"box\">\n      <input [text] [value]=\"hexText\" (blur)=\"onHexInput(null)\" (keyup.enter)=\"onAcceptColor($event)\" (newValue)=\"onHexInput($event)\"/>\n      <input *ngIf=\"cpAlphaChannel==='forced'\" type=\"number\" pattern=\"[0-9]+([\\.,][0-9]{1,2})?\" min=\"0\" max=\"1\" step=\"0.1\" [text] [rg]=\"1\" [value]=\"hexAlpha\" (keyup.enter)=\"onAcceptColor($event)\" (newValue)=\"onAlphaInput($event)\"/>\n    </div>\n\n    <div class=\"box\">\n      <div>Hex</div>\n      <div *ngIf=\"cpAlphaChannel==='forced'\">A</div>\n    </div>\n  </div>\n\n  <div *ngIf=\"!cpDisableInput && (cpColorMode ||\xA01) === 2\" class=\"value-text\">\n    <div class=\"box\">\n      <input type=\"number\" pattern=\"[0-9]*\" min=\"0\" max=\"100\" [text] [rg]=\"100\" [value]=\"hslaText?.l\" (keyup.enter)=\"onAcceptColor($event)\" (newValue)=\"onValueInput($event)\" />\n      <input *ngIf=\"cpAlphaChannel!=='disabled'\" type=\"number\" pattern=\"[0-9]+([\\.,][0-9]{1,2})?\" min=\"0\" max=\"1\" step=\"0.1\"  [text] [rg]=\"1\" [value]=\"hslaText?.a\" (keyup.enter)=\"onAcceptColor($event)\" (newValue)=\"onAlphaInput($event)\" />\n    </div>\n\n    <div class=\"box\">\n      <div>V</div><div>A</div>\n    </div>\n  </div>\n\n  <div *ngIf=\"!cpDisableInput && (cpColorMode ||\xA01) === 1\" class=\"type-policy\">\n    <span class=\"type-policy-arrow\" (click)=\"onFormatToggle(-1)\"></span>\n    <span class=\"type-policy-arrow\" (click)=\"onFormatToggle(1)\"></span>\n  </div>\n\n  <div *ngIf=\"cpPresetColors?.length || cpAddColorButton\" class=\"preset-area\">\n    <hr>\n\n    <div class=\"preset-label\">{{cpPresetLabel}}</div>\n\n    <div *ngIf=\"cpPresetColors?.length\" class=\"{{cpPresetColorsClass}}\">\n      <div *ngFor=\"let color of cpPresetColors\" class=\"preset-color\" [style.backgroundColor]=\"color\" (click)=\"setColorFromString(color)\">\n        <span *ngIf=\"cpAddColorButton\" class=\"{{cpRemoveColorButtonClass}}\" (click)=\"onRemovePresetColor($event, color)\"></span>\n      </div>\n    </div>\n\n    <div *ngIf=\"!cpPresetColors?.length && cpAddColorButton\" class=\"{{cpPresetEmptyMessageClass}}\">{{cpPresetEmptyMessage}}</div>\n  </div>\n\n  <div *ngIf=\"cpOKButton || cpCancelButton\" class=\"button-area\">\n    <button *ngIf=\"cpCancelButton\" type=\"button\" class=\"{{cpCancelButtonClass}}\" (click)=\"onCancelColor($event)\">{{cpCancelButtonText}}</button>\n\n    <button *ngIf=\"cpOKButton\" type=\"button\" class=\"{{cpOKButtonClass}}\" (click)=\"onAcceptColor($event)\">{{cpOKButtonText}}</button>\n  </div>\n</div>\n",
            encapsulation: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewEncapsulation"].None,
            styles: [".color-picker{-moz-user-select:none;-ms-user-select:none;-webkit-user-select:none;background-color:#fff;border:1px solid #777;cursor:default;height:auto;position:absolute;user-select:none;width:230px;z-index:1000}.color-picker *{box-sizing:border-box;font-size:11px;margin:0}.color-picker input{color:#000;font-size:13px;height:26px;min-width:0;text-align:center;width:0}.color-picker input:-moz-submit-invalid,.color-picker input:-moz-ui-invalid,.color-picker input:invalid{box-shadow:none}.color-picker input::-webkit-inner-spin-button,.color-picker input::-webkit-outer-spin-button{-webkit-appearance:none;margin:0}.color-picker .arrow{border-style:solid;height:0;position:absolute;width:0;z-index:999999}.color-picker .arrow.arrow-top{border-color:#777 transparent transparent;border-width:10px 5px;left:8px}.color-picker .arrow.arrow-bottom{border-color:transparent transparent #777;border-width:10px 5px;left:8px;top:-20px}.color-picker .arrow.arrow-left-top,.color-picker .arrow.arrow-top-left{border-color:transparent transparent transparent #777;border-width:5px 10px;bottom:8px;right:-21px}.color-picker .arrow.arrow-right-top,.color-picker .arrow.arrow-top-right{border-color:transparent #777 transparent transparent;border-width:5px 10px;bottom:8px;left:-20px}.color-picker .arrow.arrow-bottom-left,.color-picker .arrow.arrow-left,.color-picker .arrow.arrow-left-bottom{border-color:transparent transparent transparent #777;border-width:5px 10px;right:-21px;top:8px}.color-picker .arrow.arrow-bottom-right,.color-picker .arrow.arrow-right,.color-picker .arrow.arrow-right-bottom{border-color:transparent #777 transparent transparent;border-width:5px 10px;left:-20px;top:8px}.color-picker .cursor{border:2px solid #222;border-radius:50%;cursor:default;height:16px;position:relative;width:16px}.color-picker .box{display:flex;padding:4px 8px}.color-picker .left{padding:16px 8px;position:relative}.color-picker .right{flex:1 1 auto;padding:12px 8px}.color-picker .button-area{padding:0 16px 16px;text-align:right}.color-picker .button-area button{margin-left:8px}.color-picker .preset-area{padding:4px 15px}.color-picker .preset-area .preset-label{color:#555;font-size:11px;overflow:hidden;padding:4px;text-align:left;text-overflow:ellipsis;white-space:nowrap;width:100%}.color-picker .preset-area .preset-color{border:1px solid #a9a9a9;border-radius:25%;cursor:pointer;display:inline-block;height:18px;margin:4px 6px 8px;position:relative;width:18px}.color-picker .preset-area .preset-empty-message{font-style:italic;margin-bottom:8px;margin-top:4px;min-height:18px;text-align:center}.color-picker .hex-text{font-size:11px;padding:4px 8px;width:100%}.color-picker .hex-text .box{padding:0 24px 8px 8px}.color-picker .hex-text .box div{clear:left;color:#555;flex:1 1 auto;float:left;text-align:center}.color-picker .hex-text .box input{border:1px solid #a9a9a9;flex:1 1 auto;padding:1px}.color-picker .hex-alpha .box div:first-child,.color-picker .hex-alpha .box input:first-child{flex-grow:3;margin-right:8px}.color-picker .cmyk-text,.color-picker .hsla-text,.color-picker .rgba-text,.color-picker .value-text{font-size:11px;padding:4px 8px;width:100%}.color-picker .cmyk-text .box,.color-picker .hsla-text .box,.color-picker .rgba-text .box{padding:0 24px 8px 8px}.color-picker .value-text .box{padding:0 8px 8px}.color-picker .cmyk-text .box div,.color-picker .hsla-text .box div,.color-picker .rgba-text .box div,.color-picker .value-text .box div{color:#555;flex:1 1 auto;margin-right:8px;text-align:center}.color-picker .cmyk-text .box div:last-child,.color-picker .hsla-text .box div:last-child,.color-picker .rgba-text .box div:last-child,.color-picker .value-text .box div:last-child{margin-right:0}.color-picker .cmyk-text .box input,.color-picker .hsla-text .box input,.color-picker .rgba-text .box input,.color-picker .value-text .box input{border:1px solid #a9a9a9;flex:1;float:left;margin:0 8px 0 0;padding:1px}.color-picker .cmyk-text .box input:last-child,.color-picker .hsla-text .box input:last-child,.color-picker .rgba-text .box input:last-child,.color-picker .value-text .box input:last-child{margin-right:0}.color-picker .hue-alpha{align-items:center;margin-bottom:3px}.color-picker .hue{background-image:url(\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAJYAAAAQCAYAAAD06IYnAAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH4AIWDwkUFWbCCAAAAFxJREFUaN7t0kEKg0AQAME2x83/n2qu5qCgD1iDhCoYdpnbQC9bbY1qVO/jvc6k3ad91s7/7F1/csgPrujuQ17BDYSFsBAWwgJhISyEBcJCWAgLhIWwEBYIi2f7Ar/1TCgFH2X9AAAAAElFTkSuQmCC\");direction:ltr}.color-picker .hue,.color-picker .value{background-size:100% 100%;border:none;cursor:pointer;height:16px;margin-bottom:16px;width:100%}.color-picker .value{background-image:url(\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAJYAAAAQCAYAAAD06IYnAAACTklEQVR42u3SYUcrABhA4U2SkmRJMmWSJklKJiWZZpKUJJskKUmaTFImKZOUzMySpGRmliRNJilJSpKSJEtmSpIpmWmSdO736/6D+x7OP3gUCoWCv1cqlSQlJZGcnExKSgqpqamkpaWRnp5ORkYGmZmZqFQqsrKyyM7OJicnh9zcXNRqNXl5eeTn56PRaCgoKKCwsJCioiK0Wi3FxcWUlJRQWlpKWVkZ5eXlVFRUUFlZiU6no6qqiurqampqaqitraWurg69Xk99fT0GgwGj0UhDQwONjY00NTXR3NxMS0sLra2ttLW10d7ejslkwmw209HRQWdnJ11dXXR3d9PT00Nvby99fX309/czMDDA4OAgFouFoaEhrFYrw8PDjIyMMDo6ytjYGDabjfHxcSYmJpicnGRqagq73c709DQzMzPMzs4yNzfH/Pw8DocDp9OJy+XC7XazsLDA4uIiS0tLLC8vs7KywurqKmtra3g8HrxeLz6fD7/fz/r6OhsbG2xubrK1tcX29jaBQICdnR2CwSC7u7vs7e2xv7/PwcEBh4eHHB0dcXx8zMnJCaenp5ydnXF+fs7FxQWXl5dcXV1xfX3Nzc0Nt7e33N3dEQqFuL+/5+HhgXA4TCQS4fHxkaenJ56fn3l5eeH19ZVoNMrb2xvv7+98fHwQi8WIx+N8fn6SSCT4+vri+/ubn58ffn9/+VcKgSWwBJbAElgCS2AJLIElsASWwBJYAktgCSyBJbAElsASWAJLYAksgSWwBJbAElgCS2AJLIElsP4/WH8AmJ5Z6jHS4h8AAAAASUVORK5CYII=\");direction:rtl}.color-picker .alpha{background-image:url(\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAJYAAAAQCAYAAAD06IYnAAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH4AIWDwYQlZMa3gAAAWVJREFUaN7tmEGO6jAQRCsOArHgBpyAJYGjcGocxAm4A2IHpmoWE0eBH+ezmFlNvU06shJ3W6VEelWMUQAIIF9f6qZpimsA1LYtS2uF51/u27YVAFZVRUkEoGHdPV/sIcbIEIIkUdI/9Xa7neyv61+SWFUVAVCSct00TWn2fv6u3+Ecfd3tXzy/0+nEUu+SPjo/kqzrmiQpScN6v98XewfA8/lMkiLJ2WxGSUopcT6fM6U0NX9/frfbjev1WtfrlZfLhYfDQQHG/AIOlnGwjINlHCxjHCzjYJm/TJWdCwquJXseFFzGwDNNeiKMOJTO8xQdDQaeB29+K9efeLaBo9J7vdvtJj1RjFFjfiv7qv95tjx/7leSQgh93e1ffMeIp6O+YQjho/N791t1XVOSSI7N//K+4/GoxWLBx+PB5/Op5XLJ+/3OlJJWqxU3m83ovv5iGf8KjYNlHCxjHCzjYBkHy5gf5gusvQU7U37jTAAAAABJRU5ErkJggg==\");background-size:100% 100%;border:none;cursor:pointer;direction:ltr;height:16px;width:100%}.color-picker .type-policy{background-image:url(\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABIAAAAgCAYAAAAffCjxAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAACewAAAnsB01CO3AAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAAIASURBVEiJ7ZY9axRRFIafsxMStrLQJpAgpBFhi+C9w1YSo00I6RZ/g9vZpBf/QOr4GyRgkSKNSrAadsZqQGwCkuAWyRZJsySwvhZ7N/vhzrgbLH3Ld8597jlzz50zJokyxXH8DqDVar0qi6v8BbItqSGpEcfxdlmsFWXkvX8AfAVWg3UKPEnT9GKujMzsAFgZsVaCN1VTQd77XUnrgE1kv+6935268WRpzrnHZvYRWC7YvC3pRZZl3wozqtVqiyH9IgjAspkd1Gq1xUJQtVrdB9ZKIAOthdg/Qc65LUk7wNIMoCVJO865rYFhkqjX6/d7vV4GPJwBMqofURS5JEk6FYBer/eeYb/Mo9WwFnPOvQbeAvfuAAK4BN4sAJtAG/gJIElmNuiJyba3EGNmZiPeZuEVmVell/Y/6N+CzDn3AXhEOOo7Hv/3BeAz8IzQkMPnJbuPx1wC+yYJ7/0nYIP5S/0FHKdp+rwCEEXRS/rf5Hl1Gtb2M0iSpCOpCZzPATmX1EySpHMLAsiy7MjMDoHrGSDXZnaYZdnRwBh7J91utwmczAA6CbG3GgPleX4jqUH/a1CktqRGnuc3hSCAMB32gKspkCtgb3KCQMmkjeP4WNJThrNNZval1WptTIsv7JtQ4tmIdRa8qSoEpWl6YWZNoAN0zKxZNPehpLSBZv2t+Q0CJ9lLnARQLAAAAABJRU5ErkJggg==\");background-position:50%;background-repeat:no-repeat;background-size:8px 16px;height:24px;position:absolute;right:12px;top:218px;width:16px}.color-picker .type-policy .type-policy-arrow{display:block;height:50%;width:100%}.color-picker .selected-color{border:1px solid #a9a9a9;border-radius:50%;height:40px;left:8px;position:absolute;top:16px;width:40px}.color-picker .selected-color-background{background-image:url(\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACgAAAAoCAYAAACM/rhtAAAAh0lEQVRYR+2W0QlAMQgD60zdfwOdqa8TmI/wQMr5K0I5bZLIzLOa2nt37VVVbd+dDx5obgCC3KBLwJ2ff4PnVidkf+ucIhw80HQaCLo3DMH3CRK3iFsmAWVl6hPNDwt8EvNE5q+YuEXcMgkonVM6SdyCoEvAnZ8v1Hjx817MilmxSUB5rdLJDycZgUAZUch/AAAAAElFTkSuQmCC\");border-radius:50%;height:40px;width:40px}.color-picker .saturation-lightness{background-image:url(\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOYAAACCCAYAAABSD7T3AAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH4AIWDwksPWR6lgAAIABJREFUeNrtnVuT47gRrAHN+P//Or/61Y5wONZ7mZ1u3XAeLMjJZGZVgdKsfc5xR3S0RIIUW+CHzCpc2McYo7XGv3ex7UiZd57rjyzzv+v+33X/R/+3r/f7vR386Y+TvKNcf/wdhTLPcv9qU2wZd74uth0t1821jkIZLPcsI/6nWa4XvutquU0Z85mnx80S/ZzgpnLnOtHNt7/ofx1TKXcSNzN/7qbMQ3ju7rNQmMYYd/4s2j9aa+P+gGaMcZrb1M/tdrvf7/d2v99P9/t93O/3cbvdxu12G9frdVwul3E+n8c///nP+2+//Xb66aefxl//+tfx5z//2YK5Al2rgvf4UsbpdGrB52bAvArXpuzjmiqAVSGz5eDmGYXzhbAZmCrnmzddpUU+8Y1dAOYeXCtDUwVwV7YCGH6uAmyMcZ9l5vkUaBPGMUZ7/J5w/792/fvv9Xq93263dr/fTxPECeME8nK5jM/Pz/HTTz/dv337dvrll1/GP/7xj/G3v/1t/OUvfwkVswongjdOp9PzH3U3D3zmWGnZVXn4jCqs7wC2BKP4/8tAzkZsoWx6XrqeHZymvp4ABCBJhTQwKfDT8gzrZCIqi5AhiACjBfEB2rP8/X63MM7f6/V6v9/v7Xa7bYC83W7jcrlsVHIq5ffv30+//fbb+OWXX8ZPP/00/v73v4+ff/75JSvbeu+bL2WMMaFbAlpBNM85QX+ct6qoSqkPAwuQlBVKqGNFSUOAA3Bmu7gC5hNOd15nSwvAOUW7C4giUCV8Sgn5L9hNFIqTsp0GxI0ysioyjAjkY/tGJVEpz+fz+OWXX+7fv38//f777+Pbt2/j119/HT///PP49ddfx8fHRwrmTjV779EXu2px2xhjwtdJZQcAWQIPLPISsMJaSwiD8gzIKrwSyATE5j5nAbR5c1dBUwBlsEWW0h6LqiYsqFPAQxCyRZ3wOSARxmlXMX5k64pQfvv27f75+dk+Pj5OHx8f4/v37+Pbt2/jt99+G9++fRsfHx/jcrmUFLO31gYDWblxRIs/TqfT7ousxJsAxXA2Gc7TA9XdgfdoHbFsj76X2+1WArgI1ageGwA3qupqoHsmcbI6Fu93quggFa9d7LeDtgKfAFHBJ+NEByIkcJ5KervdTmhhGcgJJSZ5vn//fj+fz+18Pp8+Pz/H5+fnmGD+/vvv4/v37+Pj42N8fn6O2+1Ws7JjjP6wraMI5E4RZ8x2vV5TSwkquotV7/d7Tz6HFWsD/qNcdw0CQ3q/321c686TwDVIdbuy73zNldhSHb8I2klZznm+InBS4U6n0302aBFsLhHDAKJVJVglfI9jhvu53W53sLANYNxAiDA6MCeUHx8f9+v12i6XS7tcLqcZW57P5yeY8/fz83Ocz+fnsSmYUyknWEG85WBst9stzSLyMdfr9Qi08iY15UZ0LlDGLhR3o5zK2j7OPUTD0E+nU3tk7Xb/16NFbhloAMuY1zjLUOO3BKeIDe+Z8s3/J4gFo4TM5jPmuRg28foUKKVSwo16TgA5npywcWLHgYl/Pz8/73/605/ab7/91m63W7tcLie0sZj4mao5gTyfz88E0f1+j8EcYzwTPEG2cqjyfHNF0M8fuqEiaOVnRzZZQNh5fwQyHg/HDGfJo89Q1zb/quu5XC6773I2XKfTqd/v9+d3wuqWva/YTdUdEV3fhIv/Viyps6YE3x3r43K5bJQS66zaxVGFsvd+//j4aF+/fm3fv39vt9utff36tf3+++/tdrudvn37ZuNLBaaCMgUzC+rZRiFowxUuJI8YMqcCp9Opq5vagaYU6lGJA1XQqejchw6Cj0Gw5nYBrGw01A2O206n04BGouNNyTfp/FwElhUey6nXrIKw7QQWddxuN2ldL5fL839gSPF8ahu/JvBO48CPSuqMf8Vp9/P53L58+dLu93s7n8/tfr8/39/v9/b5+TkhPJ3P56mQ436/j+/fv+/iSgbzer0+AZx/5+88bv6OMda6S5z6kd21fYC9dxv7cIJJ2d9AOS30fPMzyHiTM8B4DF6XUlYHp4KQW3W+1t77MNB1vGHxWq7Xa7vf78+y5/N5A+H1et29xuP5dbYtyaRu4AksbPq6936fjRzXRxBbPr/b+b18+fKljTHaBBBfn8/n0/1+H1++fBnn8zm0sB8fH5u4cr5GuBhMVk0EEn9RsctgVhM+ixlJtMA23R8B6yysAstBOgFXIKKCMIgToMqNEu2fYMH7ztc732dQKkCj1ytAZtY0Kx8pIr8GGJ+AT3V+2Hirhl++fBmXy2Wz73w+b17P8p+fn8/tUwGVleVkTyUb68DkfayWY4zxNRihU4EpLJPZVrK+u7J4/mgfKqeLW9X2REWlItL1diynbDDb3+jXgYjQqn0rrxWc+NkILP7F7xIbMvx7vV53x40xnlbWJF12ZSag/N0pW6t+ZzmOMzHjajKwDfond78zYTdfq18up97zr2q8v3IioBprRtBl0EZ9og5WBRGOdOHjIjXF7UotFbgOWnXzIJyzYvjG5IYgsmMOxHkz8OsMSrVNWeq5T8DaOcbEv1Od5rbs9aO7YvMet63EkF++fMExq+MRl4/L5bLZN/+ez+fnZ6KazuMqXSQVO5spJXflHAIzes/xJseckRJiDMog9d6VfRrqXMr6KpVV27jRwJacGovOAM1zMdQMnwK1AubK63kdCChvI1C7g0z9nf/D+Xze2Vj8H7Gx4P9duQlsYCrqyN8XqG3Hm/10Oj3jw/n+crlstuM+jPmmxT2dTuPz83Pzt2pn1XsEHX/bnPaVqVmh0xwOt0o6XLLAHePUU203wHfcrspCwmV3TryB5s0Mseeg97x/BwzCjBlbB+pRAPla0BVQuT6V6QHdBlj3d0KG147b+DqxQeUymDO43W4dQar+TIjwmAd0z8/h65vf0/yLv3Pb5XLpru/ydDo9s7ET0I+Pj6dKK9VUEIeKWQWPAOrJ8LKd4vE+t91Y3e7UFlWatg2VwJnb+HPmtvm/sfK59/OaWF3x/eP1UPHvA5DDYDpYXfb0drv1V2DkBkxtw/tEWVVlXWdC9pFYs5/jfh9dS/16vW7s6lTG+TfqsxSJHxkXXq/Xdr1eu4LsfD6P3vsT3N77DkL+zPm5jSdKL4zR3AxQd6rHkLkYlSowsrq7znzu6wSwdsMJOXmA5fBcjxtgMGBYHlr5zokhtsMCTgXLQOW4XC6dEyEMprL8mAQzXRgduix2yZzorxkYsDn3hB1VeMLGsXsVtgl2pW8S3svk0vw7R4hNaHvv4cACl5HFzwIH0Kc6zu4XjDPR/jpAVxWzO1Xk2DDb3vTcxeGU1iWZHkmIDWziWKvirCJ4Dravs6IJ/GG6cTqWdXDy+fArQDVVkLqkVjAoZIITdmmIqXwqa95N3+MGYoZQdRVNO53Y1xRkhO16vY7eu507Ca9lJnbGpxOemQhSw/AQsmmp5zU9BiU8G6wvX76M6/U6Pj4+do0Bz4CpgiknTUeDqwlKBmg3u4OVjrZ1A+rAcgaejWq6eJCvCYFDONSwOgHX4EQRw8lxbzDOdEK6gZ3Hk1b+8g2o1JFtKXyv/fEdTXuWjWXdAZiBp6ADeDrCFiim7B6ZFneeI7Gvm/PMkUDX67W7xI8b0D7/v8dA9qfN5oaCf74WZjH0mf1cmfY1Y0JUFmVrTWu8uzkNcLtEj7u5FXBTkfC6GOA5q8YMxO8KVvF6sAVGdcrUbsKODcQKkLMOMdmlxum642YrPm26AlhZW1YB1R+rrGswE8TaYAWeUMxdf+WjwSvZ2Ef3ytOyfn5+PpVPAaqOn43MtNBqvmjjxbjM4lZjZY4gqNMI5ktaW/sYKNwS+9lFQzGihmMCKPa7+Z0V6Eb0GRmobtpX8JljWu5FMLN5ja6hG9kwQgZqf5+1NH5UxzkFReCdWhJ8XdlGUkxO7HRlYRm4mVO43W7ter12TPJEw/rmEN3L5SKHIWZg9mz+pUoKOYq5bJTJdX2gme1UcxMZQFaEQIlHct32M+Y1BzGkGuzfiyAN9z+ugplZ1symCrDCYYkGxDTpI9RzBy0rHyeDUC1nWaeUaD9n4xkNyYMBDZtzZ3B++fJlY21XFDOcARJlabOyiS3uCpLI9jrZjCDkaVvcCCjwognKShWdzXZWlZMvVTgD8LpqlCLrqgbcB+qYwrgKYpT0ccCqbKyCValkEabn/FynogCrPKfqf51xJ7sGB2ZXcZmxoSOztjx300DZi7a0/2AIR0UlBag9SuDw6KcAzlaB7vHZvWpjK90dyrq6bKyDUZQbR0B05biLQkHIcSUmgIK+SwuqgHCnoio2RQU1yj+BnBy9pphVKLGyC7ZzFK1pxWK+E8IhVCWLN/uLtnUU4ayoYLoaANz8FdtaSvY4pV0BEW2ls61czqllBKpTyKgMAhrZ1cdc1RROtPmvWNkdcKZ7ZKxaWjiPLJMpp7OZKxA+rqG/oJLjxf0pnJlqLoDZo3gyU0mKGys2taKecj/d1C+rJSplBqlTyAqgR+D8KjKlmRL2gtUcAdCtsL+ijCNT1oqqqkH2OHEbG5sDFnUg5Aa+yLou2VU1ptj1S2ZQqv1ORZN9IWzRfgaRBxKoBE8UWyqlJFtrIc0AxNjSjed99CTY/XDfSzCz5M0IZoVEsWnPFNTsl8ooVC1TzbGgqFZNDSgVwKK+1sGDMKqxZCWGVMDysiEr1jVSQJUYwj5iHOlThdHt44SQg9CN+nl8D90NMIgAdgr46JqRiR9I8vRdFvbr17m/yxUMKjNLMiVUADwu2CWGhhi+F55TWM9M9cogzms1dnM4uOF/LAEYWdcqnM7yFmyq3IfwmOROd7Y1iFWtOjoY8To41mTV5IysgFFuRzsbWFGbNIIJCDv1dOo4lZG7jWBwRFtVTKuWyeCByJKOan8oZ3ep9XddNl0tDuaywLz9cXPYeDAA0SpkBO9sbVcTOVWldPv4uyzEkzxHtjvonHoSkFEWNoo1d8DhcQputd2ppNon4BzoAiJ1hBFQg0dVtdbGHHDQWushmNEQukLM2QO1G2Y8bgTXqFhcBJj7EjPgcPts8US8qPpPB/dXznOh5Z438tzH5ec6QgrOKrRRfKmysBmUDB+PhYabMlVPER+GCSITTzr7am2tArH3bgcEzPJm+cr5jJ4NnHNFDVrFXcI5Le9k5Jnw+bedbV+FfRzZIHaOOaOsLY0/7UGs58DjrGwKMIMFIGzOEW1/jGsdAtCN6hEAI4hBe9YXeRROBSVPAVPAqvIM5bx5hVKWAMP6zBRy3iescridVdFBinBxXDnG2GRY2XbCvp1lhvGtO9Bxu5h908XQu42lnSArMFdizMim8uwRCxPGnnOS8lwpnbOiDqTAjsrRN/PcoAScCbaACqVM40ylnjjTBs+bwWlAG23/UKbdkiwKWIQPGzWaczpoSlxPEj822cNWkpS7FyzsDrqpfgpG3jahw2vgbaSQAxuLWZYt7JzyNe8JoZpNAcvDFOdw0wqYT9AK1rZz/DdbSlLPp0ryIxgQJlK9AZlEq7IOXpohg9PIhrCng88JsOxiV4ZWAYfg4sikx/8ky2Z9l862uqwrfscIH8+ugTmVGyiddeVYUgEMn4GZzg14EwIsh9sx2cKKiWXReuOE5gzGOQgdlRKVVdlevqb279Xq0Qnsts2VDaBO0coezsruWtHApu6sKG4IBhN0aGU2kLrMKGRTN3HmbCDwKV14zvkMEDG4QfZVspVlaNU2mhc5TEZ3N1h/zqTheuLpW05ZWTGVjb3dbnNmxKZBnN8JqidaVLKAOyARNLS+MB54Z2+VaqoMLKroVBlngefnTPAcoHNWCSvlfA8CI0HEmBNBnBlXyMrzU7A7WVm94PPqQ2gmqKx+WDGsnvilmcSOBJqOK1nYyAIzuAyesq3UdSK3KfWcYKD95HmfYOU3qser2CtYEUA+FpfqdNvgPBZUBhDrGONRVlQsh8rLcaUCykHG0OOUwTlLBrsh5soEMGezi1E4HRVt1icp5wZEFXdibCkG8Y8vX75sbO4E0iom9z+hjSiOfy3DhpXItpVhE+UGQdvoWjtChmrGHf4YAzKgBNnGtuJxFCeGdhUAfQLLK8kBYAP6gvFJZajMG3Xkycy8KuC0q4Eyymwtwdxdv2M0mIBtK0LKnf640j00Auq4gUkdWGlhs22qJc6dZCsL19oxnlTJG4SYVRIGpD8TPFBuM6OElbS1pldid4mGAyN6ZIupbC5bXJN9fdpbThSxLUaI8IG1XIYBxW3Tjs6KQosKcxfxcQmdnwRGM10GnFcCy2XYunLMyAkdgk4mePiczsLygthcBut6goOqS7YVFXADLjaosB6s6ofcZWAZSIRYqSUkizYwttYab3vUOQ9w2HRxIIg8WwRVeE68xi4UtL3zRphxplzwuZrcqYCq1I3jPI5dnJIygEohMbPqVJSzrwzxBJTs5zN+ReUSgxikPQVF3JVBeNQxbHENrEMNvEdFZVV9lH9+ORGEsNZQpyTNc4C3AG7XF4ngzq+DrO2zbuaaOXgdaFcdkEotoSFBVX2qJ0C8OWZeG4KGlpghA0XfTOPCqV2qqwQ26QWfF2PMLhI2w1lVAa2aPsYd0za25MQRwgcZN6uQDCi+ZxiD4XEM2kZxOT41FnZnaRlcpZouzlRqqdbQVWopQoSB58RV50lBNrHi/AwXS5LrwDVlpY3Fc3ByiYGc52Trist6kOXdwInAQtJpp5QchyaquYOV7Su+fxVMaV3dc0RE2S6mUY0gLt2pMcYqrKIQ9w2l1gpQUMtQYcmmbt5DTNxdhnUCjQqtbK9SUSzvrC0mmhhE1e2FS2+oxypy/ZASutkmtjx3vcBC24PX65nbqkBCRhfjS9kIYPnee8cMagVOhI/3T1fAmdtAWZsCswTJCkQVNa0qWKSKPOpHAUhD9DrbVcyoYkwqhvh17vYAayXLQyKGYdxlUDFp494rBXRjYgO17DDYetNIUj/ezp6S0lnlpEwsWmJMkOwsKXeZKEAjIHn0EQJISaRBcO6UMINz7p/bEjjnw4ft+xmDvksxX4G2rIris7qaeKwAFMP2Oi7n4criuZwtpSUwpfLxSnORSrIqusc5ZFaXysqRWjiZ2DyAWEIL35tVSoQElFACjOeGGSE7AHEQgdo/LSvCOgGBvkxsmDbvlS3Fp5vhaB2TAGqRKrKKMrhLVpaGzEVjZ0OQxDhaCTA+QyRR1d15aQzrJntL3RibsipjG6jlgL4yqbS0sNYg1e84vhbBVrElK64CUcWYXDfKxhpIuxiVJZUxsbMy/uRBKTNRQ4kQ3LdRYLS0rJjRPlTPqY6gdJsEDc+aQXAn+HgsNUCbRuF0Oj0zwnA7bWDkbhO5Ens00qeQhS1laBMl5M/cAaxsLF8rKyql+Tf7ELLEGu/ixiimdCvo0TjfpjKwaggen4eh5v7LokLKbLuyvHhcZG8dhGrEDx7Hg93ZppJF7qBqO3iVveXEDQNInzeoe8Yq6ePaZBZ2JviM3W2UAGotekRCAGq4EkF1X3DOnR11yRsBL1tRa0PVcZiNFXZ2c34FskvomInQQ6lzpJoZbJxk43NwKJFBquJSsrByHydxKOnTxQASBmS3j+JMnsHSla3Ec6K9VWoJVn9zfjwOM7hqYAAqJQwE2a3nA48J2QGegRkpZNivSY+ys3EkKd4oJIwsvIHl3cWgLt5k4NH6OmtLWdpurOkwEMupYc7eMtDRhOcI2ui5JhVIzXzLyto/GAPuZoyo8wkoduVgJglCt7OhGbgID4Mq4si+63zUS1FuFFXFlqyaj2emHlLMcBqYu0FMuR28BbB7lOxRMSiCQXFhCKuwkhZ+pYDiGSgbsKKV8MiSRsuHSIWM9rklRiIlZZuqXjsQK8ooYJMgq3JKWVkhHbhsVxFUzthOWPkYijcbx54IKsSdT+uLr3crGKyoYgFiGR9iBk4kfloUX+JIlQRQqabmpgnhqtpQpb6RVQ1WH5DnrS4hEoGZqaerQ2dhFbz8XePxShmDbo70eISjoorO2vK8SJXI4SUmEU4zWKDzUDtWTYw7xXlbSTEj4FRg7zKnKoGRALv0Gs9Tgc1BpCywGZRQAtqVz2xrBcAMzEpfZwFSa2G5W0QBFjSMapWAEFa3HcGN7CxDzECyIkJ97qwrqWNTWVo876PPsjPkj2wvgroM5lLZKMETKVql/CvnWVFiFa/SzJUQwkoZsr67Y6vlSRV3/2tmNTOY3vnaxYwMuoPKqdzR1w7IqHymlPxaAThfU7Ko2ZXYj4AYJHL+kNdKwRQYESTRa5fsUZ/rVC1TMTyWVyYoqNtuzaHsMyv2tvoarxdfqwYgU1axFo/cnql1FGsqK+uAROV8BX4GU8WcZTATi2q7Qcyi0O0V+GhWBMNRUkn8H1SsWVE5By3Gi0ECqUeJoBfAtDa4amkdXG37AGP5Ggeb84p7UazpoKRzdFzeQ8HkoHGxprKy/Hpm5t12p47J6xTYDEz7uINEXSuxYXvFskYAc+ySxH9sf5ftKzU6IbwVBcUGg5e5FMCEXSErZR0wGayV19woM9guPjTqJdVTqR4uE4nJnLldWVkECCZLd2VLF+xtamex7IpiriSDUpvrpn9lrwGMCHyppMH+ps6LILsuFGUj1XEOXiqbqSHPUKnClpWV68kqtURVNDY4TNaocykoYeTU5ngGEQa/S1DnnE4AeXMcKjHPAmFVjCBENaeyLVNHfr3px8xUstJ94hIpfH4HKE/eDaArK6lSyVVFbdt1gxTIVk3pppVlFXi4pEhVBTObquohU85MLXn1iahvUkHJjSCMc01tLFveVVBx0DodM6jftCu7DOtIzYxrc0qp1JGP2ayYFz2Gb6HvMrO8cnGtV6Gjm3uImSfD2GpWK6uowbZGMxFKQCo1pOMtcMXFpRst+hXGoAomF3sSTBGgTglbBKWwsQ3tZqaYSp0Z1CimRDWFcCJUPYJ00BI5FkKYNoifuQxmN88SWVXWLMaUqqqgC0BmQJR6sk3u9NCf6jYLXxAfqsYEgVLAhRY2AtgtflZNFmFyhxdrLkAdWlk4D88M2ixHyepIdhMHrG/iR1ZGtq0MGpbDbRPYOXeSY1M6Ny4ZstvGSktK+XbFPATj2D371saPEsAMXhXrsZ0km/XStkhhMyBfsa6uXFZe2VCe+YMr1+GKgwrQyNYq1VRrB+EizAow6NsdNKcyVEkYeM73ys6q4kAHp6BiFklTkIrVC5oYV7uzwOGCz4UJ0Stq2lWMJy4wtb+RetL6tZFicnJmBw5UjCvXXMZVJX2MQkbf+XN5EWd78Vz8/JEsMZTBiKNzsm1inLRUQ74H4NidaqI68j5sAFgxcRveC7ieLJXfQYxjZZ2CsiWFewZXJmBIlZ1tdtrX4hSuateKso/RZOtOKW2nmq1oTzeK6dRWAWu2NRVb4hq0SXm1GvtugHrbr5IXqmSktg5CuDE2MSlPwsY5kNE2Wp3AqiZbWVLAxiBF+2iBZbuNj6MB6rsMLC7FyasaYDyo7KkoPyEtw3pEMXfPvxAJi2jAQQgjrz0rLIZSWZlIoNhwd5xK4AR9mYNjWAaLrnuImJeBVN9zBORObVvbr+mTTfFSEJLSRnHo7hEJoIi8MFqjxmvgmF5URZz4zLFgZZ8Ctu2X7ggVccKm9gVxIsOHqxXgNMKnFWZYnf1dBnOhayXq17QwFlWW09eNKyVJFmXqaONGA5aCegMbJ3UUkGY1ic3nKWgjq8qfVYGQG1gRt6rs62a6HiqqUOqdesK5NmX4nGofJoiE1d0dF9lVVkvT1/kEEaaCoYOwFpcVcoLM+7669PxC9rWqktH0sWUYld0VCpuBZ/stVRcGgy9WX2+U1Qthi9SzAqSxzZsy+OiFzBYnySGV6Gku44rD8BCOZBV3BvD5+AKRHNwMEsB6EzHnJpkTAeiUlEGkcECeB6GDZTp5YEJTlvdrknxYjTllMkfNtXwDjM7uVjK5JXUUn43rrqpK2jytaxHW0M5G8DC8rtHMYs7KSgduVQMGTYFqFvVS6rkD3sDJ46afdYFwoq11AOKCBLhvwoUgc8IGANycR6knZrdJPdsuxnyjfd3FovTlRMdEdtOl5CMV5EHsXQBis7TOwvIDZaGj2Vnpbh7cpK63VwYEMLwqbjzyl699sawFFkF1yqjUU31HfC6sW1ZFVFuXVXVgz9keEaw0ys1lWfm+azQAQSWA+hKYVfsZjPncAcUB9oIayy/UZXRNckDGji77GsWbvBo6tPrWPqOyVkBUq+INeqpzNdYs/u0ifh5qmpqIW+33JVSUcwY70KL4U9lYdU6ljtSls7lmfi9g3YzeQfVkaGFaV3ODCnaD2N8wsEDFklE3RzM3ZghdYkWHsszq70FIecnKkVkt8ezMzRq9bkGuKojRLBVSod3Y1yPqKgYW7JRQTPVyy5xIYLjOgxgT52RKJUY1dOrIiRd4futQx/A5AcSmEjz0vFWrkLzvbWAu9HOWbGgxFk1VNTpnBKk6TgwisI/HcxYXP1uAWO72ULFlBTq+aSu2VTUs6hrxM2CF+hEor1VIA9ZmFUaab1lSSgZsVs4sxzHlVLoJHr9H4DhONTkI1XC0/wiY2NoWAG5RlnHFnq6oLccpQddMuJ/O17JVA5OHLi0BqCztq7Y1++ucCd98qLI8MIHBV/cKjxQTme3hFBS3MyCqnDsuym2o80HjvFFTtrURmNaGJsmVahImjTsUXKtQZTAVs7Mvv8/+fzUrZAXcLJ6M4koe6XP0b6SmWWNDzyUpQ8bl+LtWx4tuqZ36cRYV3yuVxPNwvIiqiQCSmu7srgTzR6nkyhpCarXwFy1vGd5iP2cY06lFr5Njhhg1Y6+NB28ftbK83s8rf7kLJbKwDFPbLg25a0AdZJEiqr5phixKMDlRUtcssq1hriLqGoH+zeNgVm9OemjsETV8JdF0NHnkIFxWY1OB4Yrp7rtWJ7NgAAAPXklEQVQ3oNs5nplyVf8u2FoLu1JrHveaZWQjqAkshtFa2gzsSG3Zpkbvg3HafF9slPPlldjFlK80Gysm8Mr4MPhneNWENPGjAIpmilTPATdTRTXlCBYHYAQuPwA36xIpWtGN4q3Y2MhiGsUpuSSnlEJRD8PorC7CFYVw+F51qThgabxsTxWzCGY0ZSsb3lfqAy0OPNjNy8xiQQKsHYFQ2HBZVvVbBuq3m1oWKajqaonsM6uZUr6CjXWNZ0l5E3h3jURma6kP3MJIiy1Lm+kahQq41N2iZja5sjtlLYNZHZrH6qUGm4vMbDp6Rw2CFmvuyFkrBcCyMtFqBaECmsHoK9BZ2LA/lJcRqSaDqnaWbrZdGaz3DLgIvBln4woGztbyJGqslwxkhhHrTjTYFXCtOoKS8uLdofVdAbOylGU6nlYpXWZts4nXBq6WxJitMNokHUJnbnJplQm+aGpY2a5GMV2QD1hRubBPFKdumf5OHkLHz0F9luE5kjBjRa0nFE5CUGqHw32MmjZ6xkgINVnSnZ1VZStK2qKlRaLlQgK7uTq7JFXJwM+3SOEKyhZNI+tJ0I5qMYy9k2qJD7dVWdqKXa0CKNR0Ccjg+B2IYu2fcBZJZkMFgM11r0X92wilghFGgzVnexlqB7xL9mS29SiYUVY2nXOZjNBRsyDsQPRWW5hrZ4XcdC4HVWRbjgJr4sFofK5SzjQ7rhI1UebdPdEbj6sqIvTZQZ5va08rABsAW0UxeWytAk7A2KJ9ZpxzCioB24XFtYAeXYxr6anSqhLgppEqWbGwLunTgrV+IjWlL29ljaAl4EQMGsErp4apeZiquwRXLXAqOCeru32mmydc6oWTSWpFAGdzeTB8RTHVMEtlM90CbbQCYhPjq3egYr1FGdYIQjiuDGZ5zZ/AzobKGOyLxti6c4Rwtv2anyWlLICnlLhxJRXt6A5ebDBWFNONbxWZ2d02mnu4S9YECpeppV1zSWRBWxHYzVIv1CXSouwqqX3jBBBDZdYQbpTQW4ZQlS8r5kH4suSRmg2++3JN10x1PaAmEkmtYlEdeGpJEM6kOuCqCR22oSujj5IV2HdT0zj5prLKTjXFAPjdQlyq7xIBxAQP5yMczG4VxAKw0n6ilZ2QBce2pLulkuxxqnoIzFfgqyqjil9S1VNwBrFmeyeops8yOjZUybZdfS8CuaTIJumzs5tODaNtLpFDQ/PcJGweLhmeL1nB0KqiUDScsiUVD89Di3HtrKtSULw3RLiygZD+7sF8JTObgYsrGvDNUFRGl1iy0Ll1YkUc2aJYMog920I8qW6YDCg1Mqk0JHJFKXkbgbRreI+qpYNOZHrVcDUba7pjsphSJNtK6upgRNAVoOS0mugBeN4bIZgHhuPZ/s1ENaX6KsVr+YNrh1Nb7ipR0PE5zbNRegCbrHRUw6Yf07dLBJl1f8KB9as2V1nNqAsl62LBBhehwalerkHmB1JFIEZKSEusdl5JQj1nJlHXSCF342gJ9CYGrXelknJIXqVP8sD+qtplCR3XH2qfKq0ygMp+KnVkKxNlZ8m2YkIlVMiCnXUwl7qznBKSvQz3m3Pt6oQbXO5b5FixCh/fHxUQW/AEcK6zCNqKQnL9sywqmKuwvqSYzT/aPVNNpVyhvRW21aqciCsjdWvBwILUvh5VyCzbWoC1pJjJ680CWsl+udKB6T5RwG1mlohnlpbg47iz5U9ha0FGtmRLFYBtO99y97Ap0z+ZDTAog6kSLZsMHg/IFkkgp6CpvU2U0cYVSdnmkjwBdOmXbxTWNWzuIbipMioVxEckZEoahSOiy2M3K0jcC1LhVDwaqG0ZvkcWqCnrG4GIxykrqlbWdw6LQyBaZR8HmLRIhQWsHswD42ZXVLNkf9l+FlW0HVQ2lwFsC/Z1FdzlQR0KaPfo+Fdfu+/dwVRICu1CGR7AEIiAhc+AZUF0kOBaPxmUqg4i64vQnU4nFDYJ9Nz+1fVXveH9qmr+kPILx8oKcRV/BFbxbE0JMT0kSD4w6L/lNY8ocsqagVdU3A3MjxhxcGuqzsPH4irpaow1q6OyrVjvp9Npc59E91LldboYVzJWdimWfAW2SNEKcDaX2FmBLLA/uKxlmhh613Is1URQApbKfttwxL02q6Onx5pQxSbPojAg+v5hAnN6LHVRDXIsvKtRjiS0qJUyZTAXVbAK82ElFJWaQdVoqUC1Unt7BVaTQudM6SuqexjQJN4+0icaxv/utbKv83ETbT8H8gjcOKxOJmbUa6OOVXht3dFY6rHv9XoNzFLceEA1o8+pKm0LAHPHZ2rYKjFq0hfZFixsqHJgD3eD5n+U0kb1mFjXkn2lvMSSOsNE/CdIAKF0Sytq6urOHUN5gwg4GZosgbmggM5ucra2qrS2Ig1cbiBBcxYzgzUDNLCvL8GbZXNp6ORy3LmS+Kk83zRIAK6A1ioKa2I9NapIuiUFdfC9766PFZUtqUr6KbWk+zZU1a/ZrIXEztrjTOfz7hwKziCeXIaraHtbZIMz+2pGgazCmw4qWAFvEdhodYp0Xq0pV7G1YWYWbO4qhGq42+Z8BYtrLWvluNPpZAeaFFS1vubPgbgxsqcpnAaszBovKaFoDQ8BGtjfUOl4NAG2nmQV04feJgumvX2fsrQEWZghL0JnVdYkn3DOZIeRN86RqPWCmsvGVqEMRnwxQAxwS8EMYo3IzmY2+BCcLp4MKiuyuhImamlbZFcNoNl7tp+RHd18ZjQIRKyXdFRhN98/hyKqwXWNo7O1wiaXoHN108REZZWEq6grnIfjzeg8jdRf1XEL4kkXa5bBjKxoKaljBjeHlVxQ4GaycpW4lDOAKtnTxHAtOfzOtZwHAM7sqVXkV6yu6kap1nHkXKqWF/4XHqjenNKqBjpR3l1ch3Ejg1+EsgdQhsdG0B4FM9sWAVWpuAyiwTPleZxt9VyZVS2qXfReWqTAilpr9ApoWTjxymit7NwV4JTriZyOA9B0k7HFfULourmKYHVnRQvqGL5HMHdqFcR2qWpmcK6eTwx2dipWrviDilr+fKWq3OWRWdHKwA4eu8wjchbeRzFilqjjZN3ufCpfkJ0/scVpnYk6L0PI77lxdWCZ87WiWm7B/AGquQSnujGKsB8CJmiJq8q1pKIVWyqOiTK66r18BN8r74/AE71fdC3yPS2MxdOpnE1tlVxD9JmVOoggN+r4PjAXVFPa3Eg5jVJGFVUGNolH20GVrUB7BOySWq6WqYQdWR92pcFMYMwckbSgCKCqD67DiiWu1g8MQC9ByfcFqW1L+jL714qNCuznoSxt0da2gtWN1G8F0BK0NN0nuimelUF9dIdAfjO44UT3CjQLoUeLHJFTO3gmpRuIIOvwBQCbqNeo3qtZ9iF6xVK13GRlo4zqimq+CGdTiR1uRY8oqgE02hZBa79kZXPMquxRHKla2saZWN4mRqZUj0vLCKhkjKnqOQHNuSZVJoKvAqS1wpEquvWDC1B2ypwrCPsRMEPVTODMLJMDv6qeKXwi2JYV5Sq4qKyvgGsHCLiuj2jR59V8gMqSJ2FJZRXEHVRHj3sFPrct6OpqlW1GpatQdt0GvwfM6n63InsGVFhJGaBqgqqIV6IsXllZgySPq4R3bnt3wi5cv+cN2yqQLW1T95KYVsWWtKk4cB9W53WQQflQYR6Wl4HaJZjvVE0D5yvq+RKgZCs5qdBEP5sD94cAvQLlSgNaSMAtHx88BuNQ41zdFsX30zKbcs0MLD/ihkpQzl0wiTqKLTfbKmCmyYICnK0IbaieC4CG9iSyLQ7cIMGQwau6TKoq60Apl3WN40LZpca1CKKK9VQyyIEn8w0F8F6CL2h8o3ixGwC7s7EWzCOqmcApYxYD4jsAzVS0sl2t98pA7vrKophCVSonbYpgH6mvSn24pTBV4sdtV3BtMq5k82y+IADvUJ0uAlkCVTxIaPm+UNu/qkV4F1TzHXCGrXIAqItBKypqK99VtAOVs64O4ObX7pHLVCpYHcRmwvLR7TvYAKBBN58LGVzDuFz+hQbWgncQyCZAk+VbsPSouf93261iZgmfCpwRbAvqmSqriU2PwhjaoOyYqtIegVXViTsmyta6bGySpY3gyRrpIyAeaWDDxtpsXwKyalMDKNP7YBXMqEskUsi2uC8FNAPxAKTVfT1o6VzM0E0jF+1rWcUuHvdyg7vgoFplX8HpvHpMCOMRUPHzZkInsqlFKNX/EIO52E0SxSzOwob2VmRLW5D1XIU0rbgM1AzWgyC7fe8G7xUAK/taEBat7luqtyP7EmsaJQOj5F+mrnZfCuYCfBUAWwShyd6pMY/vAHG1UqOYpbI/gy5T0CMKm+UO3gFuC85dgfDVeguPDfITrIBLsLrcgdh3CFgFZjaKJ4Iv3F8ANEqvuxR1tVKOgLoCa1jxboBAkj6v7j/icFbA7f4rfRnQDLRViG13i0vqBQrYVqBbADZT0ZpiHoSzvQpopKIFS3sE1HfBWlHXd0H7LnArqvougMtljHBgZnh3Eoz/BKjLML4Z2Aq0+hEJr9jaVUBbvNzCIUiroC7AWmmFw4o5AK3MtB5VypZMSFgs05JyGVwlwBqsEGAAa2ZU1CjUexXGsE4rKriilBvFzOKKo3AuAroE6QFQU3u8YpNXwS5k+1TZt5UrwouN4KiUEw+k3ZWDp1RXHNRqXb21Ts39945yZSg3VnZFNQ9CF3XeZyr5DgBXKiwCMa2MxeTDYXgP1Fsf9QNKZc0k81RJk3r6EQ3rCmBVyLL75EjZ1pIVDHoFtiOAHoB0BdTVylqBsKKKS+AeBXJVLY+CXASuGvO/Auq7GuEjDfGKg1oKa1z/dmmi9I9SUGNhl0AtfulHAawoYrnSkmNXAVuGEhrEVXvUF+A5Ct2PqNOjDetyna4CmeUolmeXLN4Aq7C5Sj10Q7yjgl+t6CNxSRHmI5X+CpwreYB3Qfdqna4q21KdBuc4GoZsn49ZOOiVinwHqK9WzjvgeweEh2AU5+vtxZ9Cd9Wqkh49V18E5oj6vVyn0RStAyGIO5edXRKd5B0VGVXq2yr3xYp+5Ut+C4QJ4P1N339pQMjRejj4vb/Dcr6rQc3O/0rjmtZpeYCBiCHfCemRbNhbK/pNUPc3wfKy5f2D7OlL3/uPhve/oU4T0F8f+VNM2vyoiv0jK+KHQfdHq+0bncz4oz73/+Y6LbKw1o/5B7eOf1Rl/0du9B9tn/9bvrf/j+v0h6ttn2tp/r/4819y4/zv5391uvzzfwDifz6phT1MPgAAAABJRU5ErkJggg==\");background-size:100% 100%;border:none;cursor:pointer;direction:ltr;height:130px;touch-action:manipulation;width:100%}.color-picker .cp-add-color-button-class{background:transparent;border:0;cursor:pointer;display:inline;margin:3px -3px;padding:0;position:absolute}.color-picker .cp-add-color-button-class:hover{text-decoration:underline}.color-picker .cp-add-color-button-class:disabled{color:#999;cursor:not-allowed}.color-picker .cp-add-color-button-class:disabled:hover{text-decoration:none}.color-picker .cp-remove-color-button-class{background:#fff;border-radius:50%;box-shadow:1px 1px 5px #333;cursor:pointer;display:block;height:10px;position:absolute;right:-5px;text-align:center;top:-5px;width:10px}.color-picker .cp-remove-color-button-class:before{bottom:3.5px;content:\"x\";display:inline-block;font-size:10px;position:relative}"]
          }]
        }], function () {
          return [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"]
          }, {
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ChangeDetectorRef"]
          }, {
            type: ColorPickerService
          }];
        }, {
          handleEsc: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["HostListener"],
            args: ['document:keyup.esc', ['$event']]
          }],
          handleEnter: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["HostListener"],
            args: ['document:keyup.enter', ['$event']]
          }],
          dialogElement: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"],
            args: ['dialogPopup', {
              "static": true
            }]
          }],
          hueSlider: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"],
            args: ['hueSlider', {
              "static": true
            }]
          }],
          alphaSlider: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"],
            args: ['alphaSlider', {
              "static": true
            }]
          }]
        });
      })();

      var ColorPickerDirective = /*#__PURE__*/function () {
        function ColorPickerDirective(injector, cfr, appRef, vcRef, elRef, _service) {
          _classCallCheck(this, ColorPickerDirective);

          this.injector = injector;
          this.cfr = cfr;
          this.appRef = appRef;
          this.vcRef = vcRef;
          this.elRef = elRef;
          this._service = _service;
          this.dialogCreated = false;
          this.ignoreChanges = false;
          this.viewAttachedToAppRef = false;
          this.cpWidth = '230px';
          this.cpHeight = 'auto';
          this.cpToggle = false;
          this.cpDisabled = false;
          this.cpIgnoredElements = [];
          this.cpFallbackColor = '';
          this.cpColorMode = 'color';
          this.cpCmykEnabled = false;
          this.cpOutputFormat = 'auto';
          this.cpAlphaChannel = 'enabled';
          this.cpDisableInput = false;
          this.cpDialogDisplay = 'popup';
          this.cpSaveClickOutside = true;
          this.cpCloseClickOutside = true;
          this.cpUseRootViewContainer = false;
          this.cpPosition = 'auto';
          this.cpPositionOffset = '0%';
          this.cpPositionRelativeToArrow = false;
          this.cpOKButton = false;
          this.cpOKButtonText = 'OK';
          this.cpOKButtonClass = 'cp-ok-button-class';
          this.cpCancelButton = false;
          this.cpCancelButtonText = 'Cancel';
          this.cpCancelButtonClass = 'cp-cancel-button-class';
          this.cpPresetLabel = 'Preset colors';
          this.cpPresetColorsClass = 'cp-preset-colors-class';
          this.cpMaxPresetColorsLength = 6;
          this.cpPresetEmptyMessage = 'No colors added';
          this.cpPresetEmptyMessageClass = 'preset-empty-message';
          this.cpAddColorButton = false;
          this.cpAddColorButtonText = 'Add color';
          this.cpAddColorButtonClass = 'cp-add-color-button-class';
          this.cpRemoveColorButtonClass = 'cp-remove-color-button-class';
          this.cpInputChange = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"](true);
          this.cpToggleChange = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"](true);
          this.cpSliderChange = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"](true);
          this.cpSliderDragEnd = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"](true);
          this.cpSliderDragStart = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"](true);
          this.colorPickerOpen = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"](true);
          this.colorPickerClose = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"](true);
          this.colorPickerCancel = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"](true);
          this.colorPickerSelect = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"](true);
          this.colorPickerChange = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"](false);
          this.cpCmykColorChange = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"](true);
          this.cpPresetColorsChange = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"](true);
        }

        _createClass(ColorPickerDirective, [{
          key: "handleClick",
          value: function handleClick() {
            this.inputFocus();
          }
        }, {
          key: "handleFocus",
          value: function handleFocus() {
            this.inputFocus();
          }
        }, {
          key: "handleInput",
          value: function handleInput(event) {
            this.inputChange(event);
          }
        }, {
          key: "ngOnDestroy",
          value: function ngOnDestroy() {
            if (this.cmpRef != null) {
              if (this.viewAttachedToAppRef) {
                this.appRef.detachView(this.cmpRef.hostView);
              }

              this.cmpRef.destroy();
              this.cmpRef = null;
              this.dialog = null;
            }
          }
        }, {
          key: "ngOnChanges",
          value: function ngOnChanges(changes) {
            if (changes.cpToggle && !this.cpDisabled) {
              if (changes.cpToggle.currentValue) {
                this.openDialog();
              } else if (!changes.cpToggle.currentValue) {
                this.closeDialog();
              }
            }

            if (changes.colorPicker) {
              if (this.dialog && !this.ignoreChanges) {
                if (this.cpDialogDisplay === 'inline') {
                  this.dialog.setInitialColor(changes.colorPicker.currentValue);
                }

                this.dialog.setColorFromString(changes.colorPicker.currentValue, false);

                if (this.cpUseRootViewContainer && this.cpDialogDisplay !== 'inline') {
                  this.cmpRef.changeDetectorRef.detectChanges();
                }
              }

              this.ignoreChanges = false;
            }

            if (changes.cpPresetLabel || changes.cpPresetColors) {
              if (this.dialog) {
                this.dialog.setPresetConfig(this.cpPresetLabel, this.cpPresetColors);
              }
            }
          }
        }, {
          key: "openDialog",
          value: function openDialog() {
            if (!this.dialogCreated) {
              var vcRef = this.vcRef;
              this.dialogCreated = true;
              this.viewAttachedToAppRef = false;

              if (this.cpUseRootViewContainer && this.cpDialogDisplay !== 'inline') {
                var classOfRootComponent = this.appRef.componentTypes[0];
                var appInstance = this.injector.get(classOfRootComponent, _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injector"].NULL);

                if (appInstance !== _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injector"].NULL) {
                  vcRef = appInstance.vcRef || appInstance.viewContainerRef || this.vcRef;

                  if (vcRef === this.vcRef) {
                    console.warn('You are using cpUseRootViewContainer, ' + 'but the root component is not exposing viewContainerRef!' + 'Please expose it by adding \'public vcRef: ViewContainerRef\' to the constructor.');
                  }
                } else {
                  this.viewAttachedToAppRef = true;
                }
              }

              var compFactory = this.cfr.resolveComponentFactory(ColorPickerComponent);

              if (this.viewAttachedToAppRef) {
                this.cmpRef = compFactory.create(this.injector);
                this.appRef.attachView(this.cmpRef.hostView);
                document.body.appendChild(this.cmpRef.hostView.rootNodes[0]);
              } else {
                var injector = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ReflectiveInjector"].fromResolvedProviders([], vcRef.parentInjector);

                this.cmpRef = vcRef.createComponent(compFactory, 0, injector, []);
              }

              this.cmpRef.instance.setupDialog(this, this.elRef, this.colorPicker, this.cpWidth, this.cpHeight, this.cpDialogDisplay, this.cpFallbackColor, this.cpColorMode, this.cpCmykEnabled, this.cpAlphaChannel, this.cpOutputFormat, this.cpDisableInput, this.cpIgnoredElements, this.cpSaveClickOutside, this.cpCloseClickOutside, this.cpUseRootViewContainer, this.cpPosition, this.cpPositionOffset, this.cpPositionRelativeToArrow, this.cpPresetLabel, this.cpPresetColors, this.cpPresetColorsClass, this.cpMaxPresetColorsLength, this.cpPresetEmptyMessage, this.cpPresetEmptyMessageClass, this.cpOKButton, this.cpOKButtonClass, this.cpOKButtonText, this.cpCancelButton, this.cpCancelButtonClass, this.cpCancelButtonText, this.cpAddColorButton, this.cpAddColorButtonClass, this.cpAddColorButtonText, this.cpRemoveColorButtonClass);
              this.dialog = this.cmpRef.instance;

              if (this.vcRef !== vcRef) {
                this.cmpRef.changeDetectorRef.detectChanges();
              }
            } else if (this.dialog) {
              this.dialog.openDialog(this.colorPicker);
            }
          }
        }, {
          key: "closeDialog",
          value: function closeDialog() {
            if (this.dialog && this.cpDialogDisplay === 'popup') {
              this.dialog.closeDialog();
            }
          }
        }, {
          key: "cmykChanged",
          value: function cmykChanged(value) {
            this.cpCmykColorChange.emit(value);
          }
        }, {
          key: "stateChanged",
          value: function stateChanged(state) {
            this.cpToggleChange.emit(state);

            if (state) {
              this.colorPickerOpen.emit(this.colorPicker);
            } else {
              this.colorPickerClose.emit(this.colorPicker);
            }
          }
        }, {
          key: "colorChanged",
          value: function colorChanged(value) {
            var ignore = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;
            this.ignoreChanges = ignore;
            this.colorPickerChange.emit(value);
          }
        }, {
          key: "colorSelected",
          value: function colorSelected(value) {
            this.colorPickerSelect.emit(value);
          }
        }, {
          key: "colorCanceled",
          value: function colorCanceled() {
            this.colorPickerCancel.emit();
          }
        }, {
          key: "inputFocus",
          value: function inputFocus() {
            var element = this.elRef.nativeElement;
            var ignored = this.cpIgnoredElements.filter(function (item) {
              return item === element;
            });

            if (!this.cpDisabled && !ignored.length) {
              if (typeof document !== 'undefined' && element === document.activeElement) {
                this.openDialog();
              } else if (!this.dialog || !this.dialog.show) {
                this.openDialog();
              } else {
                this.closeDialog();
              }
            }
          }
        }, {
          key: "inputChange",
          value: function inputChange(event) {
            if (this.dialog) {
              this.dialog.setColorFromString(event.target.value, true);
            } else {
              this.colorPicker = event.target.value;
              this.colorPickerChange.emit(this.colorPicker);
            }
          }
        }, {
          key: "inputChanged",
          value: function inputChanged(event) {
            this.cpInputChange.emit(event);
          }
        }, {
          key: "sliderChanged",
          value: function sliderChanged(event) {
            this.cpSliderChange.emit(event);
          }
        }, {
          key: "sliderDragEnd",
          value: function sliderDragEnd(event) {
            this.cpSliderDragEnd.emit(event);
          }
        }, {
          key: "sliderDragStart",
          value: function sliderDragStart(event) {
            this.cpSliderDragStart.emit(event);
          }
        }, {
          key: "presetColorsChanged",
          value: function presetColorsChanged(value) {
            this.cpPresetColorsChange.emit(value);
          }
        }]);

        return ColorPickerDirective;
      }();

      ColorPickerDirective.ɵfac = function ColorPickerDirective_Factory(t) {
        return new (t || ColorPickerDirective)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injector"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["ComponentFactoryResolver"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["ApplicationRef"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewContainerRef"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](ColorPickerService));
      };

      ColorPickerDirective.ɵdir = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineDirective"]({
        type: ColorPickerDirective,
        selectors: [["", "colorPicker", ""]],
        hostBindings: function ColorPickerDirective_HostBindings(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function ColorPickerDirective_click_HostBindingHandler() {
              return ctx.handleClick();
            })("focus", function ColorPickerDirective_focus_HostBindingHandler() {
              return ctx.handleFocus();
            })("input", function ColorPickerDirective_input_HostBindingHandler($event) {
              return ctx.handleInput($event);
            });
          }
        },
        inputs: {
          cpWidth: "cpWidth",
          cpHeight: "cpHeight",
          cpToggle: "cpToggle",
          cpDisabled: "cpDisabled",
          cpIgnoredElements: "cpIgnoredElements",
          cpFallbackColor: "cpFallbackColor",
          cpColorMode: "cpColorMode",
          cpCmykEnabled: "cpCmykEnabled",
          cpOutputFormat: "cpOutputFormat",
          cpAlphaChannel: "cpAlphaChannel",
          cpDisableInput: "cpDisableInput",
          cpDialogDisplay: "cpDialogDisplay",
          cpSaveClickOutside: "cpSaveClickOutside",
          cpCloseClickOutside: "cpCloseClickOutside",
          cpUseRootViewContainer: "cpUseRootViewContainer",
          cpPosition: "cpPosition",
          cpPositionOffset: "cpPositionOffset",
          cpPositionRelativeToArrow: "cpPositionRelativeToArrow",
          cpOKButton: "cpOKButton",
          cpOKButtonText: "cpOKButtonText",
          cpOKButtonClass: "cpOKButtonClass",
          cpCancelButton: "cpCancelButton",
          cpCancelButtonText: "cpCancelButtonText",
          cpCancelButtonClass: "cpCancelButtonClass",
          cpPresetLabel: "cpPresetLabel",
          cpPresetColorsClass: "cpPresetColorsClass",
          cpMaxPresetColorsLength: "cpMaxPresetColorsLength",
          cpPresetEmptyMessage: "cpPresetEmptyMessage",
          cpPresetEmptyMessageClass: "cpPresetEmptyMessageClass",
          cpAddColorButton: "cpAddColorButton",
          cpAddColorButtonText: "cpAddColorButtonText",
          cpAddColorButtonClass: "cpAddColorButtonClass",
          cpRemoveColorButtonClass: "cpRemoveColorButtonClass",
          colorPicker: "colorPicker",
          cpPresetColors: "cpPresetColors"
        },
        outputs: {
          cpInputChange: "cpInputChange",
          cpToggleChange: "cpToggleChange",
          cpSliderChange: "cpSliderChange",
          cpSliderDragEnd: "cpSliderDragEnd",
          cpSliderDragStart: "cpSliderDragStart",
          colorPickerOpen: "colorPickerOpen",
          colorPickerClose: "colorPickerClose",
          colorPickerCancel: "colorPickerCancel",
          colorPickerSelect: "colorPickerSelect",
          colorPickerChange: "colorPickerChange",
          cpCmykColorChange: "cpCmykColorChange",
          cpPresetColorsChange: "cpPresetColorsChange"
        },
        exportAs: ["ngxColorPicker"],
        features: [_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵNgOnChangesFeature"]]
      });

      ColorPickerDirective.ctorParameters = function () {
        return [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injector"]
        }, {
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ComponentFactoryResolver"]
        }, {
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ApplicationRef"]
        }, {
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewContainerRef"]
        }, {
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"]
        }, {
          type: ColorPickerService
        }];
      };

      ColorPickerDirective.propDecorators = {
        colorPicker: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }],
        cpWidth: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }],
        cpHeight: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }],
        cpToggle: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }],
        cpDisabled: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }],
        cpIgnoredElements: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }],
        cpFallbackColor: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }],
        cpColorMode: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }],
        cpCmykEnabled: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }],
        cpOutputFormat: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }],
        cpAlphaChannel: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }],
        cpDisableInput: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }],
        cpDialogDisplay: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }],
        cpSaveClickOutside: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }],
        cpCloseClickOutside: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }],
        cpUseRootViewContainer: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }],
        cpPosition: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }],
        cpPositionOffset: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }],
        cpPositionRelativeToArrow: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }],
        cpOKButton: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }],
        cpOKButtonText: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }],
        cpOKButtonClass: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }],
        cpCancelButton: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }],
        cpCancelButtonText: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }],
        cpCancelButtonClass: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }],
        cpPresetLabel: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }],
        cpPresetColors: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }],
        cpPresetColorsClass: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }],
        cpMaxPresetColorsLength: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }],
        cpPresetEmptyMessage: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }],
        cpPresetEmptyMessageClass: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }],
        cpAddColorButton: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }],
        cpAddColorButtonText: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }],
        cpAddColorButtonClass: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }],
        cpRemoveColorButtonClass: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }],
        cpInputChange: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"]
        }],
        cpToggleChange: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"]
        }],
        cpSliderChange: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"]
        }],
        cpSliderDragEnd: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"]
        }],
        cpSliderDragStart: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"]
        }],
        colorPickerOpen: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"]
        }],
        colorPickerClose: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"]
        }],
        colorPickerCancel: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"]
        }],
        colorPickerSelect: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"]
        }],
        colorPickerChange: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"]
        }],
        cpCmykColorChange: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"]
        }],
        cpPresetColorsChange: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"]
        }],
        handleClick: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["HostListener"],
          args: ['click']
        }],
        handleFocus: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["HostListener"],
          args: ['focus']
        }],
        handleInput: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["HostListener"],
          args: ['input', ['$event']]
        }]
      };
      /*@__PURE__*/

      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](ColorPickerDirective, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Directive"],
          args: [{
            selector: '[colorPicker]',
            exportAs: 'ngxColorPicker'
          }]
        }], function () {
          return [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injector"]
          }, {
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ComponentFactoryResolver"]
          }, {
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ApplicationRef"]
          }, {
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewContainerRef"]
          }, {
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"]
          }, {
            type: ColorPickerService
          }];
        }, {
          cpWidth: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
          }],
          cpHeight: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
          }],
          cpToggle: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
          }],
          cpDisabled: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
          }],
          cpIgnoredElements: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
          }],
          cpFallbackColor: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
          }],
          cpColorMode: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
          }],
          cpCmykEnabled: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
          }],
          cpOutputFormat: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
          }],
          cpAlphaChannel: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
          }],
          cpDisableInput: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
          }],
          cpDialogDisplay: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
          }],
          cpSaveClickOutside: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
          }],
          cpCloseClickOutside: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
          }],
          cpUseRootViewContainer: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
          }],
          cpPosition: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
          }],
          cpPositionOffset: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
          }],
          cpPositionRelativeToArrow: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
          }],
          cpOKButton: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
          }],
          cpOKButtonText: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
          }],
          cpOKButtonClass: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
          }],
          cpCancelButton: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
          }],
          cpCancelButtonText: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
          }],
          cpCancelButtonClass: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
          }],
          cpPresetLabel: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
          }],
          cpPresetColorsClass: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
          }],
          cpMaxPresetColorsLength: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
          }],
          cpPresetEmptyMessage: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
          }],
          cpPresetEmptyMessageClass: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
          }],
          cpAddColorButton: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
          }],
          cpAddColorButtonText: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
          }],
          cpAddColorButtonClass: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
          }],
          cpRemoveColorButtonClass: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
          }],
          cpInputChange: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"]
          }],
          cpToggleChange: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"]
          }],
          cpSliderChange: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"]
          }],
          cpSliderDragEnd: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"]
          }],
          cpSliderDragStart: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"]
          }],
          colorPickerOpen: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"]
          }],
          colorPickerClose: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"]
          }],
          colorPickerCancel: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"]
          }],
          colorPickerSelect: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"]
          }],
          colorPickerChange: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"]
          }],
          cpCmykColorChange: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"]
          }],
          cpPresetColorsChange: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"]
          }],
          handleClick: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["HostListener"],
            args: ['click']
          }],
          handleFocus: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["HostListener"],
            args: ['focus']
          }],
          handleInput: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["HostListener"],
            args: ['input', ['$event']]
          }],
          colorPicker: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
          }],
          cpPresetColors: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
          }]
        });
      })();

      var ColorPickerModule = function ColorPickerModule() {
        _classCallCheck(this, ColorPickerModule);
      };

      ColorPickerModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineNgModule"]({
        type: ColorPickerModule
      });
      ColorPickerModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjector"]({
        factory: function ColorPickerModule_Factory(t) {
          return new (t || ColorPickerModule)();
        },
        providers: [ColorPickerService],
        imports: [[_angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"]]]
      });

      (function () {
        (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsetNgModuleScope"](ColorPickerModule, {
          declarations: function declarations() {
            return [ColorPickerComponent, ColorPickerDirective, TextDirective, SliderDirective];
          },
          imports: function imports() {
            return [_angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"]];
          },
          exports: function exports() {
            return [ColorPickerDirective];
          }
        });
      })();
      /*@__PURE__*/


      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](ColorPickerModule, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"],
          args: [{
            imports: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"]],
            exports: [ColorPickerDirective],
            providers: [ColorPickerService],
            declarations: [ColorPickerComponent, ColorPickerDirective, TextDirective, SliderDirective],
            entryComponents: [ColorPickerComponent]
          }]
        }], null, null);
      })();
      /**
       * Generated bundle index. Do not edit.
       */
      //# sourceMappingURL=ngx-color-picker.js.map

      /***/

    },

    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/category-selection/category-selection.page.html":
    /*!*******************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/category-selection/category-selection.page.html ***!
      \*******************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppCategorySelectionCategorySelectionPageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<app-header></app-header>\r\n<ion-content>\r\n    <form [formGroup]=\"imageUploadForm\">\r\n        <ion-item background>\r\n            <div>\r\n                <div class=\"video-player1\" *ngIf=\"type == 'video'\">\r\n                    <video #videoElement id=\"videoElement\" [poster]=\"posterImage\" controls playsinline\r\n                        controlsList=\"nodownload\" webkit-playsinline preload=\"auto\" muted=\"true\" crossorigin>\r\n                        <source [src]=\"mediaPath\" />\r\n                    </video>\r\n                </div>\r\n                <img *ngIf=\"type == 'image'\" id=\"image\" #imageSrc [src]=\"mediaURL\" class=\"img-preview\" />\r\n                <ion-fab fab-zoom *ngIf=\"type == 'image'\" horizontal=\"end\" vertical=\"bottom\" slot=\"fixed\">\r\n                    <ion-fab-button (click)=\"openViewer()\" color=\"light\">\r\n                        <ion-icon name=\"search\"></ion-icon>\r\n                    </ion-fab-button>\r\n                </ion-fab>\r\n            </div>\r\n        </ion-item>\r\n\r\n        <ion-row class=\"progress-breadcrumbs-padding\">\r\n            <ion-progress-bar class=\"progress\" color=\"warning\" [value]=\"progressValue\"></ion-progress-bar>\r\n        </ion-row>\r\n        <ion-row class=\"progress-breadcrumbs-padding\">\r\n            <ion-label style=\"display: inline-block; color: #fff; font-size: 15px; font-weight: bold;\"\r\n                *ngFor=\"let crumb of breadCrumbFieldsArray\">\r\n                {{crumb.val!=null && crumb.val!=''?(crumb.val +'>'):''}}\r\n            </ion-label>\r\n        </ion-row>\r\n        <ion-row class=\"tab_bar\">\r\n            <!-- <ion-toolbar> -->\r\n            <!-- <ion-tabs> -->\r\n            <!-- <ion-tab-bar> -->\r\n            <ion-tab-button\r\n                [ngStyle]=\"{'background-color': activeTab==root.sno ? '#rgb(41 41 39)' : '#rgb(41 41 39)', 'border-bottom': activeTab==root.sno ? '2px solid rgb(255 215 0)' : 'rgb(41 41 39)' }\"\r\n                *ngFor=\"let root of rootList\" (click)=\"getLevel1List(root.sno,root.categ_name)\">\r\n                <ion-label [ngStyle]=\"{'color': activeTab==root.sno ? 'rgb(255 215 0)' : 'white'}\"\r\n                    style=\"margin-bottom: 13px\">\r\n                    {{root.categ_name}}</ion-label>\r\n            </ion-tab-button>\r\n            <!-- </ion-tab-bar> -->\r\n            <!-- </ion-tabs> -->\r\n            <!-- </ion-toolbar> -->\r\n        </ion-row>\r\n        <ion-row style=\"margin-bottom: -15px;\">\r\n            <ion-col size=\"4\">\r\n                <ion-item class=\"cat-dropdown\"\r\n                    [ngClass]=\"imageUploadForm.get('level1Val').value?'border-yellow':(imageUploadForm.get('level1Val').dirty || imageUploadForm.get('level1Val').touched?'border-red':'')\">\r\n                    <!-- <ion-label style=\"display:none\">Select Option</ion-label> -->\r\n                    <ion-select interface=\"popover\" class=\"ion-slct-1\" placeholder=\"Add tag\" formControlName=\"level1Val\"\r\n                        (ionChange)=\"getLevel2List()\">\r\n                        <ion-select-option [value]=\"level1.sno\" *ngFor=\"let level1 of level1List\" appNoArrow>\r\n                            {{level1.categ_name}}\r\n                        </ion-select-option>\r\n                    </ion-select>\r\n                    <!-- <small *ngFor=\"let validation of validationMessages.level1Val\">\r\n                        <span class=\"error\"\r\n                            *ngIf=\"imageUploadForm.get('level1Val').hasError(validation.type) && (imageUploadForm.get('level1Val').dirty || imageUploadForm.get('level1Val').touched)\">\r\n                            {{ validation.message }}\r\n                        </span>\r\n                    </small> -->\r\n                    <div class=\"arrow row\" slot=\"end\">\r\n                        <ion-icon class=\"up-arrow\" name=\"caret-up-sharp\"></ion-icon>\r\n                        <ion-icon class=\"down-arrow\" name=\"caret-down-sharp\"></ion-icon>\r\n                    </div>\r\n                </ion-item>\r\n            </ion-col>\r\n            <ion-col size=\"4\">\r\n                <ion-item class=\"cat-dropdown\"\r\n                    [ngClass]=\"imageUploadForm.get('level2Val').value?'border-yellow':(imageUploadForm.get('level2Val').dirty || imageUploadForm.get('level2Val').touched?'border-red':'')\">\r\n                    <!-- <ion-label style=\"display:none\">Select Option</ion-label> -->\r\n                    <ion-select interface=\"popover\" class=\"ion-slct-1\" placeholder=\"Add tag\" formControlName=\"level2Val\"\r\n                        (ionChange)=\"getLevel3List()\" appNoArrow>\r\n                        <ion-select-option [value]=\"level2.sno\" *ngFor=\"let level2 of level2List\">\r\n                            {{level2.categ_name}}\r\n                        </ion-select-option>\r\n                    </ion-select>\r\n                    <div class=\"arrow row\" slot=\"end\">\r\n                        <ion-icon class=\"up-arrow\" name=\"caret-up-sharp\"></ion-icon>\r\n                        <ion-icon class=\"down-arrow\" name=\"caret-down-sharp\"></ion-icon>\r\n                    </div>\r\n                    <!-- <small *ngFor=\"let validation of validationMessages.level2Val\">\r\n                        <span class=\"error\"\r\n                            *ngIf=\"imageUploadForm.get('level2Val').hasError(validation.type) && (imageUploadForm.get('level2Val').dirty || imageUploadForm.get('level2Val').touched)\">\r\n                            {{ validation.message }}\r\n                        </span>\r\n                    </small> -->\r\n                </ion-item>\r\n            </ion-col>\r\n            <ion-col size=\"4\">\r\n                <ion-item class=\"cat-dropdown\"\r\n                    [ngClass]=\"imageUploadForm.get('level3Val').value?'border-yellow':(imageUploadForm.get('level3Val').dirty || imageUploadForm.get('level3Val').touched?'border-red':'')\">\r\n                    <!-- <ion-label style=\"display:none\">Select Option</ion-label> -->\r\n                    <ion-select interface=\"popover\" class=\"ion-slct-1\" placeholder=\"Add tag\" formControlName=\"level3Val\"\r\n                        (ionChange)=\"getDynamicFormControlList()\" appNoArrow>\r\n                        <ion-select-option [value]=\"level3.sno\" *ngFor=\"let level3 of level3List\">\r\n                            {{level3.categ_name}}\r\n                        </ion-select-option>\r\n                    </ion-select>\r\n                    <div class=\"arrow row\" slot=\"end\">\r\n                        <ion-icon class=\"up-arrow\" name=\"caret-up-sharp\"></ion-icon>\r\n                        <ion-icon class=\"down-arrow\" name=\"caret-down-sharp\"></ion-icon>\r\n                    </div>\r\n                    <!-- <small *ngFor=\"let validation of validationMessages.level3Val\">\r\n                        <span class=\"error\"\r\n                            *ngIf=\"imageUploadForm.get('level3Val').hasError(validation.type) && (imageUploadForm.get('level3Val').dirty || imageUploadForm.get('level3Val').touched)\">\r\n                            {{ validation.message }}\r\n                        </span>\r\n                    </small> -->\r\n                </ion-item>\r\n            </ion-col>\r\n        </ion-row>\r\n\r\n        <div formArrayName=\"level4Val\">\r\n            <ion-row *ngFor=\"let formarray of imageUploadForm.get('level4Val').controls;let i=index;\"\r\n                [formGroupName]=\"i\">\r\n                <ion-col *ngIf=\"focused==i\" size=\"1\" class=\"col-text-icon col-border\">\r\n                    <ion-icon name=\"cafe-sharp\"></ion-icon>\r\n                </ion-col>\r\n                <ion-col *ngIf=\"focused!=i\" size=\"1\" class=\"\"></ion-col>\r\n                <ion-col *ngIf=\"focused!=i\" size=\"4\" class=\"col-text-white\">\r\n                    <ion-label>{{formarray.get('labelName').value}}</ion-label>\r\n                </ion-col>\r\n                <ion-col *ngIf=\"focused==i\" size=\"4\" class=\"col-text-yellow\">\r\n                    <ion-label>{{formarray.get('labelName').value}}</ion-label>\r\n                </ion-col>\r\n                <ion-col size=\"7\" style=\"margin-bottom: -16px;\" *ngIf=\"formarray.get('ctrlType').value =='ddl'\">\r\n                    <ion-item class=\"cat-dropdown\"\r\n                        [ngClass]=\"formarray.get('labelValue').value?'border-yellow':(formarray.get('labelValue').dirty || formarray.get('labelValue').touched?'border-red':'')\">\r\n                        <!-- <ion-label style=\"display:none\">select option</ion-label> -->\r\n                        <ion-select interface=\"popover\" class=\"ion-slct-2\" placeholder=\"Add tag\"\r\n                            formControlName=\"labelValue\" (ionFocus)=\"focused=i\"\r\n                            (ionChange)=\"progressBarAndBreadCrumb(formarray.get('labelName').value,formarray.get('labelValue').value,i+5)\"\r\n                            appNoArrow>\r\n                            <ion-select-option [value]=\"list.categ_name\"\r\n                                *ngFor=\"let list of formarray.get('categList').value\">\r\n                                {{list.categ_name}}\r\n                            </ion-select-option>\r\n                            <!-- <small *ngFor=\"let validation of validationMessages.colorVal\">\r\n                                <span class=\"error\"\r\n                                    *ngIf=\"formarray.get('labelValue').hasError(validation.type) && (formarray.get('labelValue').dirty || formarray.get('labelValue').touched)\">\r\n                                    {{ validation.message }}\r\n                                </span>\r\n                            </small> -->\r\n                        </ion-select>\r\n                        <div class=\"arrow row\" slot=\"end\">\r\n                            <ion-icon class=\"up-arrow\" name=\"caret-up-sharp\"></ion-icon>\r\n                            <ion-icon class=\"down-arrow\" name=\"caret-down-sharp\"></ion-icon>\r\n                        </div>\r\n                    </ion-item>\r\n                </ion-col>\r\n                <ion-col size=\"7\" *ngIf=\"formarray.get('ctrlType').value =='chk'\" class=\"col-mr\"\r\n                    [ngClass]=\"checkboxValidation[i].borderColor=='red'?'border-red':(checkboxValidation[i].borderColor=='yellow'?'border-yellow':'border-white')\">\r\n                    <ion-item *ngFor=\"let list of formarray.get('categList').value;let j=index;\" class=\"chk-item\">\r\n                        <ion-label>{{list.categ_name}}</ion-label>\r\n                        <ion-checkbox slot=\"end\" [checked]=\"list.status\" color=\"yellow\"\r\n                            (ionChange)=\"checkbox(i,j,$event.target.checked);progressBarAndBreadCrumb(formarray.get('labelName').value,formarray.get('labelValue').value,i+5);focused=i\">\r\n                        </ion-checkbox>\r\n                    </ion-item>\r\n                </ion-col>\r\n            </ion-row>\r\n        </div>\r\n\r\n        <!-- <ion-row *ngIf=\"imageUploadForm.get('level4Val').controls.length!=0\" class=\"\">\r\n            <ion-col *ngIf=\"focused=='color'\" size=\"1\" class=\"col-text-icon col-border\">\r\n                <ion-icon name=\"cafe-sharp\"></ion-icon>\r\n            </ion-col>\r\n            <ion-col *ngIf=\"focused!='color'\" size=\"1\"></ion-col>\r\n            <ion-col *ngIf=\"focused!='color'\" size=\"4\" class=\"col-text-white\">\r\n                <ion-label>Color</ion-label>\r\n            </ion-col>\r\n            <ion-col *ngIf=\"focused=='color'\" size=\"4\" class=\"col-text-yellow\">\r\n                <ion-label>Color</ion-label>\r\n            </ion-col>\r\n            <ion-col size=\"7\" style=\"margin-bottom: -16px;\">\r\n                <ion-item class=\"cat-dropdown\">\r\n                    <ion-input type=\"text\" formControlName=\"colorVal\" (ionFocus)=\"focused='color'\" debounce=\"1000\"\r\n                        (ionChange)=\"progressBarAndBreadCrumb('colorVal',imageUploadForm.get('colorVal').value,imageUploadForm.get('level4Val').controls.length+5)\"\r\n                        required>\r\n                    </ion-input>\r\n                </ion-item>\r\n            </ion-col>\r\n        </ion-row> -->\r\n        <ion-row *ngIf=\"imageUploadForm.get('level4Val').controls.length!=0\" style=\"margin-bottom: 3px;\">\r\n            <ion-col *ngIf=\"focused=='swatch'\" size=\"1\" class=\"col-text-icon col-border\">\r\n                <ion-icon name=\"cafe-sharp\"></ion-icon>\r\n            </ion-col>\r\n            <ion-col *ngIf=\"focused!='swatch'\" size=\"1\"></ion-col>\r\n            <ion-col *ngIf=\"focused!='swatch'\" size=\"4\" class=\"col-text-white\">\r\n                <ion-label>Color</ion-label>\r\n            </ion-col>\r\n            <ion-col *ngIf=\"focused=='swatch'\" size=\"4\" class=\"col-text-yellow\">\r\n                <ion-label>Color</ion-label>\r\n            </ion-col>\r\n            <ion-col size=\"7\" style=\"margin-bottom: -16px;\">\r\n                <div class=\"swatch-item\" [cpOKButton]=\"true\" [cpSaveClickOutside]=\"false\"\r\n                    [cpOKButtonClass]=\"'btn btn-primary btn-xs'\" [(colorPicker)]=\"color\" [cpCancelButton]=\"true\"\r\n                    [cpCloseClickOutside]=\"false\" [cpCancelButtonClass]=\"'btn btn-primary btn-xs'\"\r\n                    (colorPickerSelect)=\"colorPickerSelect($event)\" (click)=\"focused='swatch'\">\r\n\r\n                    <!-- <ion-icon name=\"color-palette-sharp\" class=\"swatch-icon\" style=\"margin-bottom: -3px;\"\r\n                        [style.color]=\"color\">\r\n                    </ion-icon> -->\r\n                    <ion-icon name=\"add-sharp\" class=\"swatch-icon\" style=\"margin-bottom: -3px;\"></ion-icon>\r\n                    <svg width=\"35\" height=\"25\" viewBox=\"-51 0 512 512.00001\" xmlns=\"http://www.w3.org/2000/svg\"\r\n                        [ngStyle]=\"{'fill': color?color:'white'}\">\r\n                        <path\r\n                            d=\"m373.1875 338.714844-52.570312 35.484375 18.390624 68.878906 71.539063-47.734375zm0 0\" />\r\n                        <path d=\"m300.78125 228.96875-31.808594-48.214844 15.757813 59.019532zm0 0\" />\r\n                        <path\r\n                            d=\"m318.371094 255.625-24.929688 16.777344 18.460938 69.152344 43.699218-29.496094zm0 0\" />\r\n                        <path d=\"m253.976562 280.050781 7.820313-2.375-7.820313-39.316406zm0 0\" />\r\n                        <path d=\"m253.976562 382.761719 33.394532-9.289063-17.332032-64.925781-16.0625 4.878906zm0 0\" />\r\n                        <path d=\"m295.609375 404.324219-41.632813 11.582031v69.671875l59.113282-15.78125zm0 0\" />\r\n                        <path d=\"m0 344.550781h222.042969v67.585938h-222.042969zm0 0\" />\r\n                        <path d=\"m0 444.070312h222.042969v67.929688h-222.042969zm0 0\" />\r\n                        <path\r\n                            d=\"m0 0v213.097656h222.042969v-101.320312c0-61.632813-50.144531-111.777344-111.777344-111.777344zm120.464844 61.90625c13.273437 7.679688 17.824218 24.730469 10.144531 38.003906-5.148437 8.902344-14.511719 13.882813-24.121094 13.882813-4.726562 0-9.507812-1.203125-13.886719-3.734375-13.273437-7.679688-17.824218-24.730469-10.144531-38.007813 7.679688-13.273437 24.726563-17.828125 38.007813-10.148437zm0 0\" />\r\n                        <path d=\"m0 245.027344h222.042969v67.589844h-222.042969zm0 0\" /></svg>\r\n                    <ion-label class=\"swatch-label swatch-icon\">Click to add swatch</ion-label>\r\n                </div>\r\n            </ion-col>\r\n        </ion-row>\r\n        <ion-row class=\"swatch-item-row-content\" *ngIf=\"color && imageUploadForm.get('level4Val').controls.length!=0\">\r\n            <ion-col size=\"7\" class=\"\"></ion-col>\r\n            <ion-col size=\"3\">\r\n                <!-- <ion-icon name=\"logo-tableau\" class=\"icon-swatch\" [style.color]=\"color\"></ion-icon> -->\r\n                <svg width=\"82\" height=\"83\" viewBox=\"0 0 82 83\" fill=\"#ffffffa6\" xmlns=\"http://www.w3.org/2000/svg\">\r\n                    <path\r\n                        d=\"M16 17C16 16.4477 16.4477 16 17 16H65C65.5523 16 66 16.4477 66 17V65C66 65.5523 65.5523 66 65 66H17C16.4477 66 16 65.5523 16 65V17Z\"\r\n                        fill=\"white\" fill-opacity=\"0.71\" [ngStyle]=\"{'fill': color}\" />\r\n                    <path\r\n                        d=\"M0 17C0 16.4477 0.447715 16 1 16H10C10.5523 16 11 16.4477 11 17V17C11 17.5523 10.5523 18 10 18H1C0.447715 18 0 17.5523 0 17V17Z\"\r\n                        fill=\"white\" fill-opacity=\"0.71\" [ngStyle]=\"{'fill': color}\" />\r\n                    <path\r\n                        d=\"M71 17C71 16.4477 71.4477 16 72 16H81C81.5523 16 82 16.4477 82 17V17C82 17.5523 81.5523 18 81 18H72C71.4477 18 71 17.5523 71 17V17Z\"\r\n                        fill=\"white\" fill-opacity=\"0.71\" [ngStyle]=\"{'fill': color}\" />\r\n                    <path\r\n                        d=\"M71 64C71 63.4477 71.4477 63 72 63H81C81.5523 63 82 63.4477 82 64V64C82 64.5523 81.5523 65 81 65H72C71.4477 65 71 64.5523 71 64V64Z\"\r\n                        fill=\"white\" fill-opacity=\"0.71\" [ngStyle]=\"{'fill': color}\" />\r\n                    <path\r\n                        d=\"M0 65C0 64.4477 0.447715 64 1 64H10C10.5523 64 11 64.4477 11 65V65C11 65.5523 10.5523 66 10 66H1C0.447715 66 0 65.5523 0 65V65Z\"\r\n                        fill=\"white\" fill-opacity=\"0.71\" [ngStyle]=\"{'fill': color}\" />\r\n                    <path\r\n                        d=\"M16.9999 0.0103555C17.5522 0.00463633 18.0045 0.447691 18.0102 0.999946L18.1034 9.99946C18.1092 10.5517 17.6661 11.004 17.1138 11.0098V11.0098C16.5616 11.0155 16.1093 10.5724 16.1035 10.0202L16.0103 1.02066C16.0046 0.468402 16.4477 0.0160747 16.9999 0.0103555V0.0103555Z\"\r\n                        fill=\"white\" fill-opacity=\"0.71\" [ngStyle]=\"{'fill': color}\" />\r\n                    <path\r\n                        d=\"M16.9999 71.0104C17.5522 71.0046 18.0045 71.4477 18.0102 71.9999L18.1034 80.9995C18.1092 81.5517 17.6661 82.004 17.1138 82.0098V82.0098C16.5616 82.0155 16.1093 81.5724 16.1035 81.0202L16.0103 72.0207C16.0046 71.4684 16.4477 71.0161 16.9999 71.0104V71.0104Z\"\r\n                        fill=\"white\" fill-opacity=\"0.71\" [ngStyle]=\"{'fill': color}\" />\r\n                    <path\r\n                        d=\"M64.9999 71.0104C65.5522 71.0046 66.0045 71.4477 66.0102 71.9999L66.1034 80.9995C66.1092 81.5517 65.6661 82.004 65.1138 82.0098V82.0098C64.5616 82.0155 64.1093 81.5724 64.1035 81.0202L64.0103 72.0207C64.0046 71.4684 64.4477 71.0161 64.9999 71.0104V71.0104Z\"\r\n                        fill=\"white\" fill-opacity=\"0.71\" [ngStyle]=\"{'fill': color}\" />\r\n                    <path\r\n                        d=\"M64.9999 0.0103555C65.5522 0.00463633 66.0045 0.447691 66.0102 0.999946L66.1034 9.99946C66.1092 10.5517 65.6661 11.004 65.1138 11.0098V11.0098C64.5616 11.0155 64.1093 10.5724 64.1035 10.0202L64.0103 1.02066C64.0046 0.468402 64.4477 0.0160747 64.9999 0.0103555V0.0103555Z\"\r\n                        fill=\"white\" fill-opacity=\"0.71\" [ngStyle]=\"{'fill': color}\" />\r\n                </svg>\r\n            </ion-col>\r\n            <ion-col size=\"2\" class=\"\"></ion-col>\r\n        </ion-row>\r\n        <ion-row *ngIf=\"color && imageUploadForm.get('level4Val').controls.length!=0\">\r\n            <ion-col size=\"7\" class=\"\"></ion-col>\r\n            <ion-col size=\"3\" style=\"text-align: center;\">\r\n                <ion-label class=\"swatch-label-color\">{{color}}</ion-label>\r\n                <!-- <small *ngFor=\"let validation of validationMessages.swatch\">\r\n                    <span class=\"error\"\r\n                        *ngIf=\"imageUploadForm.get('swatch').hasError(validation.type) && (imageUploadForm.get('swatch').dirty || imageUploadForm.get('swatch').touched)\">\r\n                        {{ validation.message }}\r\n                    </span>\r\n                </small> -->\r\n            </ion-col>\r\n            <ion-col size=\"2\" class=\"\"></ion-col>\r\n        </ion-row>\r\n        <ion-row *ngIf=\"imageUploadForm.get('level4Val').controls.length!=0\" class=\"\">\r\n            <ion-col size=\"1\" [ngClass]=\"focused=='tags'?'col-text-icon col-border':''\">\r\n                <ion-icon name=\"cafe-sharp\" *ngIf=\"focused=='tags'\"></ion-icon>\r\n            </ion-col>\r\n            <ion-col size=\"4\" [ngClass]=\"focused=='tags'?'col-text-yellow':'col-text-white'\">\r\n                <ion-label>Add #Tags</ion-label>\r\n            </ion-col>\r\n            <ion-col size=\"7\" style=\"margin-bottom: -16px;\">\r\n                <ion-item class=\"cat-dropdown\">\r\n                    <ion-textarea placeholder=\"#Trends,#Trending, #Niche\" maxlength=\"50\"\r\n                        (ionChange)=\"textareaMaxLengthValidation()\" (ionFocus)=\"focused='tags'\" formControlName=\"tags\">\r\n                    </ion-textarea>\r\n                </ion-item>\r\n                <ion-label class=\"command-tag-count\" *ngIf=\"imageUploadForm.get('tags').value\">\r\n                    {{imageUploadForm.get('tags').value?imageUploadForm.get('tags').value.length:0}}/50\r\n                </ion-label>\r\n            </ion-col>\r\n        </ion-row>\r\n        <ion-row *ngIf=\"imageUploadForm.get('level4Val').controls.length!=0\">\r\n            <ion-col size=\"2\">\r\n            </ion-col>\r\n            <ion-col size=\"8\" class=\"col-text\">\r\n                <ion-text color=\"light\"><small>* All fields must be filled</small></ion-text>\r\n            </ion-col>\r\n            <ion-col size=\"2\">\r\n            </ion-col>\r\n        </ion-row>\r\n    </form>\r\n</ion-content>\r\n<ion-toolbar *ngIf=\"showToolbar == true\" color=\"yellow\" slot=\"bottom\">\r\n    <ion-tabs>\r\n        <ion-tab-bar color=\"yellow\" slot=\"bottom\">\r\n            <ion-tab-button (click)=\"goBack()\">\r\n                <ion-icon name=\"caret-back-outline\"></ion-icon>\r\n                <ion-label>Back</ion-label>\r\n            </ion-tab-button>\r\n            <ion-tab-button (click)=\"uploadFile()\">\r\n                <ion-icon name=\"checkmark-circle-outline\"></ion-icon>\r\n                <ion-label>Upload</ion-label>\r\n            </ion-tab-button>\r\n            <!-- <ion-tab-button (click)=\"set()\">\r\n                <ion-label>Check</ion-label>\r\n            </ion-tab-button> -->\r\n        </ion-tab-bar>\r\n    </ion-tabs>\r\n</ion-toolbar>";
      /***/
    },

    /***/
    "./src/app/category-selection/category-selection-routing.module.ts":
    /*!*************************************************************************!*\
      !*** ./src/app/category-selection/category-selection-routing.module.ts ***!
      \*************************************************************************/

    /*! exports provided: CategorySelectionPageRoutingModule */

    /***/
    function srcAppCategorySelectionCategorySelectionRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "CategorySelectionPageRoutingModule", function () {
        return CategorySelectionPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _category_selection_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./category-selection.page */
      "./src/app/category-selection/category-selection.page.ts");

      var routes = [{
        path: '',
        component: _category_selection_page__WEBPACK_IMPORTED_MODULE_3__["CategorySelectionPage"]
      }];

      var CategorySelectionPageRoutingModule = function CategorySelectionPageRoutingModule() {
        _classCallCheck(this, CategorySelectionPageRoutingModule);
      };

      CategorySelectionPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], CategorySelectionPageRoutingModule);
      /***/
    },

    /***/
    "./src/app/category-selection/category-selection.module.ts":
    /*!*****************************************************************!*\
      !*** ./src/app/category-selection/category-selection.module.ts ***!
      \*****************************************************************/

    /*! exports provided: CategorySelectionPageModule */

    /***/
    function srcAppCategorySelectionCategorySelectionModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "CategorySelectionPageModule", function () {
        return CategorySelectionPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _category_selection_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./category-selection-routing.module */
      "./src/app/category-selection/category-selection-routing.module.ts");
      /* harmony import */


      var _category_selection_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./category-selection.page */
      "./src/app/category-selection/category-selection.page.ts");
      /* harmony import */


      var _header_header_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! ../header/header.module */
      "./src/app/header/header.module.ts");
      /* harmony import */


      var ngx_color_picker__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
      /*! ngx-color-picker */
      "./node_modules/ngx-color-picker/__ivy_ngcc__/fesm2015/ngx-color-picker.js");

      var CategorySelectionPageModule = function CategorySelectionPageModule() {
        _classCallCheck(this, CategorySelectionPageModule);
      };

      CategorySelectionPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _category_selection_routing_module__WEBPACK_IMPORTED_MODULE_5__["CategorySelectionPageRoutingModule"], _header_header_module__WEBPACK_IMPORTED_MODULE_7__["HeaderPageModule"], ngx_color_picker__WEBPACK_IMPORTED_MODULE_8__["ColorPickerModule"]],
        declarations: [_category_selection_page__WEBPACK_IMPORTED_MODULE_6__["CategorySelectionPage"]]
      })], CategorySelectionPageModule);
      /***/
    },

    /***/
    "./src/app/category-selection/category-selection.page.scss":
    /*!*****************************************************************!*\
      !*** ./src/app/category-selection/category-selection.page.scss ***!
      \*****************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppCategorySelectionCategorySelectionPageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "[background] {\n  margin: 0 -15px;\n}\n[background] ion-content {\n  --background: #525252;\n}\n[background-text] {\n  margin: 0;\n  background: var(--ion-color-yellow);\n  text-align: center;\n}\n[ion-col-height] {\n  width: 100%;\n  z-index: 100;\n}\n.categories {\n  background: #292927;\n  display: flex;\n  flex-direction: row;\n  flex-wrap: wrap;\n  justify-content: center;\n}\n.categories ion-button,\n.categories ion-item {\n  margin: 2px;\n  border-radius: 30px;\n  width: 95px;\n  font-size: 12px;\n}\n.un-selected {\n  --background: transparent;\n  border: #ffffff 2px solid;\n  color: white !important;\n}\n.selected {\n  background-color: var(--ion-color-yellow);\n  color: #000000;\n  border: var(--ion-color-yellow);\n}\nion-content {\n  --background: #292927;\n}\n.video-player1 {\n  background: #1f1e1d;\n  width: 100%;\n  height: 300px;\n}\n.video-player1 video {\n  width: 100%;\n  height: 100%;\n  position: absolute;\n}\n.img-preview {\n  -o-object-fit: contain;\n     object-fit: contain;\n  -o-object-position: 50% 75%;\n     object-position: 50% 75%;\n  width: 100%;\n  background-color: var(--ion-color-pantoneblack);\n  height: 300px;\n}\n.collapse-content {\n  width: 50vw;\n  margin: auto;\n  box-shadow: 10px 10px 5px 0px rgba(0, 0, 0, 0.75);\n}\n.collapse {\n  background: #fff;\n}\n.collapse a {\n  display: block;\n  font-size: 1rem;\n  font-weight: 500;\n  padding: 0.9rem 1.8rem;\n  color: #fff;\n  position: relative;\n}\n.collapse a.instagram {\n  background: #db2e7d;\n}\n.collapse a.twitter {\n  background: #1dabdd;\n}\n.collapse a.dribbble {\n  background: #444;\n}\n.collapse a.youtube {\n  background: #ff0000;\n}\n.collapse a:before {\n  content: \"\";\n  border-top: 7px solid #fff;\n  border-left: 7px solid transparent;\n  border-right: 7px solid transparent;\n  position: absolute;\n  top: 25px;\n  right: 30px;\n}\n.collapse + .collapse a {\n  border-top: 1px solid rgba(255, 255, 255, 0.7);\n}\nh3 {\n  margin-bottom: 15px;\n}\n.collapse:target .content {\n  max-height: 15em;\n}\n.collapse:target a:before {\n  transform: rotate(-90deg);\n}\n[fab-zoom] {\n  bottom: 20px;\n  right: calc(20px + var(--ion-safe-area-right, 0px));\n}\n.progress {\n  height: 10px;\n  --buffer-background:white;\n}\n.cat-dropdown {\n  --background: #ffffff00;\n  color: #ffffffa6 !important;\n  border: 2px solid #fff;\n  max-height: 65%;\n  border-radius: 7px;\n  font-size: 11px;\n  border: 1px solid #ffffffcc;\n  --placeholder-color: #ffffff94 ;\n  --highlight-background: rgb(0 0 0 / 0%);\n}\n.cat-dropdown ion-label {\n  margin-top: -5px;\n}\n.cat-dropdown ion-input {\n  margin-top: -9px;\n}\n.cat-dropdown:active {\n  border: 1px solid #ffd70099;\n  --placeholder-color: #ffffffe3 ;\n}\n.cat-dropdown:active ion-label {\n  color: #fff;\n}\n.cat-dropdown:active ion-input {\n  color: #fff;\n}\n.cat-dropdown:active .col-text {\n  color: #ffd700;\n}\n.col-border {\n  border-left: 3px solid #ffd700;\n  max-height: 44px;\n}\n.col-text {\n  --border-color: #595a58;\n  text-align: center;\n  top: 10px;\n  color: #ffffffa6;\n  font-size: 12px;\n}\n.col-text-yellow {\n  text-align: center;\n  color: #ffd700;\n}\n.col-text-white {\n  text-align: center;\n  color: #fff;\n}\n.col-text-icon {\n  text-align: center;\n  font-size: 21px;\n  color: #ffd700;\n}\n.color-picker-align {\n  max-width: 100%;\n}\n.progress-bar {\n  margin: 0px;\n}\n.progress-breadcrumbs-padding {\n  padding: 10px;\n  line-height: 25px;\n}\nion-select {\n  margin-bottom: 15px;\n  max-width: 100%;\n  --padding-end: 0px;\n  --padding-start: 0px;\n}\n.tab_bar {\n  padding: 5px;\n  margin-bottom: 7px;\n}\n.color-input {\n  margin-top: 16px;\n}\n.command-tag-count {\n  font-size: 12px;\n  margin-left: 200px;\n  color: #ffffff61;\n}\n/* <source :http://webdesignerhut.com/examples/pure-css-tabs/> */\n/* <source :http://webdesignerhut.com/examples/pure-css-tabs/> */\n.tabs {\n  max-width: 90%;\n  float: none;\n  list-style: none;\n  padding: 0;\n  margin: 0px auto;\n}\n.tabs:after {\n  content: \"\";\n  display: table;\n  clear: both;\n}\n.tabs input[type=radio] {\n  display: none;\n}\n.tabs label {\n  display: block;\n  float: left;\n  width: 33.3333%;\n  color: #ccc;\n  font-size: 14px;\n  font-weight: normal;\n  text-decoration: none;\n  text-align: center;\n  line-height: 3;\n  cursor: pointer;\n  /* Safari 3.1 to 6.0 */\n  transition: all 0.5s;\n}\n.tabs label span {\n  display: none;\n}\n.tabs label i {\n  padding: 5px;\n  margin-right: 0;\n}\n.tab-content {\n  display: none;\n  width: 100%;\n  float: left;\n  box-sizing: border-box;\n  background-color: #292927;\n}\n.tab-content * {\n  -webkit-animation: scale 0.7s ease-in-out;\n  animation: scale 0.7s ease-in-out;\n}\n@-webkit-keyframes scale {\n  0% {\n    transform: scale(0.9);\n    opacity: 0;\n  }\n  50% {\n    transform: scale(1.01);\n    opacity: 0.5;\n  }\n  100% {\n    transform: scale(1);\n    opacity: 1;\n  }\n}\n@keyframes scale {\n  0% {\n    transform: scale(0.9);\n    opacity: 0;\n  }\n  50% {\n    transform: scale(1.01);\n    opacity: 0.5;\n  }\n  100% {\n    transform: scale(1);\n    opacity: 1;\n  }\n}\n.tabs [id^=tab]:checked + label {\n  background: #ffffff00;\n  border-bottom: 4px solid #ffd700;\n  color: #ffd700;\n}\n#tab1:checked ~ #tab-content1,\n#tab2:checked ~ #tab-content2,\n#tab3:checked ~ #tab-content3 {\n  display: block;\n}\n@media (min-width: 768px) {\n  .tabs i {\n    padding: 5px;\n    margin-right: 10px;\n  }\n\n  .tabs label span {\n    display: inline-block;\n  }\n\n  .tabs {\n    max-width: 950px;\n    margin: 50px auto;\n  }\n}\n.swatch-icon {\n  color: #c1c1c1;\n  font-size: 20px;\n  margin-left: 5px;\n}\n.swatch-icon:hover {\n  cursor: pointer;\n}\n.swatch-label {\n  color: #ffffff94;\n  font-size: 12px;\n}\n.swatch-label-color {\n  color: #fff;\n  font-size: 12px;\n  margin-left: -25px;\n}\n.swatch-label-color:hover {\n  cursor: pointer;\n}\n.swatch-item {\n  height: 40px;\n}\n#swatch-radio {\n  display: none;\n}\n.chk-mr {\n  margin-top: 25px;\n}\n.color-picker {\n  top: -358.009px !important;\n  left: -80px !important;\n  position: absolute !important;\n}\nion-select::part(icon) {\n  display: none !important;\n}\n.select-text {\n  min-width: 250px;\n}\n.swatch-item {\n  padding: 0px 0px;\n  margin: 5px -5px;\n}\n::-webkit-input-placeholder {\n  font-size: 14px;\n}\n::-moz-placeholder {\n  font-size: 14px;\n}\n/* firefox 19+ */\n:-ms-input-placeholder {\n  font-size: 14px;\n}\n/* ie */\ninput:-moz-placeholder {\n  font-size: 14px;\n}\n.border-red {\n  border: 1px solid red;\n}\n.border-yellow {\n  border: 1px solid #ffd700;\n}\n.ion-color-yellow {\n  --ion-color-contrast:#fff;\n}\n.arrow {\n  color: #b5b5b4;\n  font-size: 10px;\n}\n.up-arrow {\n  position: absolute;\n  margin-top: -10px;\n}\n.down-arrow {\n  margin-right: 5px;\n  margin-bottom: 2px;\n}\n.icon-swatch {\n  font-size: 40px;\n}\n.border-green {\n  border: 1px solid;\n}\n.chk-item {\n  --background: transparent;\n  --color: white;\n  --border-color: transparent;\n}\n.border-white {\n  border: 1px solid #ffffffcc;\n}\n.col-mr {\n  margin: 10px 0px;\n  border-radius: 7px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY2F0ZWdvcnktc2VsZWN0aW9uL2NhdGVnb3J5LXNlbGVjdGlvbi5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQVE7RUFDSSxlQUFBO0FBQ1o7QUFBWTtFQUNJLHFCQUFBO0FBRWhCO0FBRVE7RUFDSSxTQUFBO0VBQ0EsbUNBQUE7RUFDQSxrQkFBQTtBQUNaO0FBRVE7RUFDSSxXQUFBO0VBR0EsWUFBQTtBQURaO0FBS1E7RUFDSSxtQkFBQTtFQUNBLGFBQUE7RUFDQSxtQkFBQTtFQUNBLGVBQUE7RUFDQSx1QkFBQTtBQUZaO0FBSVk7O0VBRUksV0FBQTtFQUNBLG1CQUFBO0VBQ0EsV0FBQTtFQUNBLGVBQUE7QUFGaEI7QUF3QlE7RUFDSSx5QkFBQTtFQUNBLHlCQUFBO0VBQ0EsdUJBQUE7QUFyQlo7QUF3QlE7RUFDSSx5Q0FBQTtFQUNBLGNBQUE7RUFDQSwrQkFBQTtBQXJCWjtBQXdCUTtFQUNJLHFCQUFBO0FBckJaO0FBeUJRO0VBQ0ksbUJBQUE7RUFDQSxXQUFBO0VBQ0EsYUFBQTtBQXRCWjtBQXVCWTtFQUNJLFdBQUE7RUFDQSxZQUFBO0VBQ0Esa0JBQUE7QUFyQmhCO0FBeUJRO0VBRUksc0JBQUE7S0FBQSxtQkFBQTtFQUNBLDJCQUFBO0tBQUEsd0JBQUE7RUFDQSxXQUFBO0VBRUEsK0NBQUE7RUFDQSxhQUFBO0FBeEJaO0FBMkJRO0VBQ0ksV0FBQTtFQUNBLFlBQUE7RUFDQSxpREFBQTtBQXhCWjtBQTJCUTtFQUNJLGdCQUFBO0FBeEJaO0FBMkJRO0VBQ0ksY0FBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtFQUNBLHNCQUFBO0VBQ0EsV0FBQTtFQUNBLGtCQUFBO0FBeEJaO0FBMkJRO0VBQ0ksbUJBQUE7QUF4Qlo7QUEyQlE7RUFDSSxtQkFBQTtBQXhCWjtBQTJCUTtFQUNJLGdCQUFBO0FBeEJaO0FBMkJRO0VBQ0ksbUJBQUE7QUF4Qlo7QUEyQlE7RUFDSSxXQUFBO0VBQ0EsMEJBQUE7RUFDQSxrQ0FBQTtFQUNBLG1DQUFBO0VBQ0Esa0JBQUE7RUFDQSxTQUFBO0VBQ0EsV0FBQTtBQXhCWjtBQStCUTtFQUNJLDhDQUFBO0FBNUJaO0FBK0JRO0VBQ0ksbUJBQUE7QUE1Qlo7QUErQlE7RUFDSSxnQkFBQTtBQTVCWjtBQStCUTtFQUNJLHlCQUFBO0FBNUJaO0FBK0JRO0VBQ0ksWUFBQTtFQUNBLG1EQUFBO0FBNUJaO0FBbUNRO0VBQ0ksWUFBQTtFQUVBLHlCQUFBO0FBakNaO0FBb0NRO0VBQ0ksdUJBQUE7RUFDQSwyQkFBQTtFQUNBLHNCQUFBO0VBQ0EsZUFBQTtFQUVBLGtCQUFBO0VBQ0EsZUFBQTtFQUNBLDJCQUFBO0VBQ0EsK0JBQUE7RUFDQSx1Q0FBQTtBQWxDWjtBQTBDWTtFQUNJLGdCQUFBO0FBeENoQjtBQTBDWTtFQUNJLGdCQUFBO0FBeENoQjtBQW9EUTtFQUNJLDJCQUFBO0VBQ0EsK0JBQUE7QUFqRFo7QUFrRFk7RUFDSSxXQUFBO0FBaERoQjtBQWtEWTtFQUNJLFdBQUE7QUFoRGhCO0FBbURRO0VBQ0ksY0FBQTtBQWhEWjtBQWtEUTtFQUNJLDhCQUFBO0VBQ0EsZ0JBQUE7QUEvQ1o7QUFpRFE7RUFDSSx1QkFBQTtFQUNBLGtCQUFBO0VBQ0EsU0FBQTtFQUNBLGdCQUFBO0VBQ0EsZUFBQTtBQTlDWjtBQWdEUTtFQUNJLGtCQUFBO0VBRUEsY0FBQTtBQTlDWjtBQWdEUTtFQUNJLGtCQUFBO0VBRUEsV0FBQTtBQTlDWjtBQWdEUTtFQUNJLGtCQUFBO0VBRUEsZUFBQTtFQUNBLGNBQUE7QUE5Q1o7QUFnRFE7RUFDSSxlQUFBO0FBN0NaO0FBK0NRO0VBQ0ksV0FBQTtBQTVDWjtBQThDUTtFQUNJLGFBQUE7RUFDQSxpQkFBQTtBQTNDWjtBQTZDUTtFQUNJLG1CQUFBO0VBQ0EsZUFBQTtFQUNBLGtCQUFBO0VBQ0Esb0JBQUE7QUExQ1o7QUE0Q1E7RUFDSSxZQUFBO0VBQ0Esa0JBQUE7QUF6Q1o7QUEyQ1E7RUFDSSxnQkFBQTtBQXhDWjtBQTBDUTtFQUNJLGVBQUE7RUFDQSxrQkFBQTtFQUNBLGdCQUFBO0FBdkNaO0FBNkNRLGdFQUFBO0FBRUEsZ0VBQUE7QUFFQTtFQUNJLGNBQUE7RUFDQSxXQUFBO0VBQ0EsZ0JBQUE7RUFDQSxVQUFBO0VBQ0EsZ0JBQUE7QUE1Q1o7QUErQ1E7RUFDSSxXQUFBO0VBQ0EsY0FBQTtFQUNBLFdBQUE7QUE1Q1o7QUE4Q1E7RUFDSSxhQUFBO0FBM0NaO0FBNkNRO0VBQ0ksY0FBQTtFQUNBLFdBQUE7RUFDQSxlQUFBO0VBQ0EsV0FBQTtFQUNBLGVBQUE7RUFDQSxtQkFBQTtFQUNBLHFCQUFBO0VBQ0Esa0JBQUE7RUFDQSxjQUFBO0VBQ0EsZUFBQTtFQUc4QixzQkFBQTtFQUM5QixvQkFBQTtBQTNDWjtBQTZDUTtFQUNJLGFBQUE7QUExQ1o7QUE0Q1E7RUFDSSxZQUFBO0VBQ0EsZUFBQTtBQXpDWjtBQWdEUTtFQUNJLGFBQUE7RUFDQSxXQUFBO0VBQ0EsV0FBQTtFQUVBLHNCQUFBO0VBQ0EseUJBQUE7QUE5Q1o7QUFnRFE7RUFDSSx5Q0FBQTtFQUVBLGlDQUFBO0FBN0NaO0FBK0NRO0VBQ0E7SUFDSSxxQkFBQTtJQUNBLFVBQUE7RUE1Q1Y7RUE4Q007SUFDSSxzQkFBQTtJQUNBLFlBQUE7RUE1Q1Y7RUE4Q007SUFDSSxtQkFBQTtJQUNBLFVBQUE7RUE1Q1Y7QUFDRjtBQWdDUTtFQUNBO0lBQ0kscUJBQUE7SUFDQSxVQUFBO0VBNUNWO0VBOENNO0lBQ0ksc0JBQUE7SUFDQSxZQUFBO0VBNUNWO0VBOENNO0lBQ0ksbUJBQUE7SUFDQSxVQUFBO0VBNUNWO0FBQ0Y7QUE4Q1E7RUFDSSxxQkFBQTtFQUVBLGdDQUFBO0VBQ0EsY0FBQTtBQTdDWjtBQStDUTs7O0VBR0ksY0FBQTtBQTVDWjtBQThDUTtFQUNJO0lBQ0ksWUFBQTtJQUNBLGtCQUFBO0VBM0NkOztFQTZDVTtJQUNJLHFCQUFBO0VBMUNkOztFQTRDVTtJQUNBLGdCQUFBO0lBQ0EsaUJBQUE7RUF6Q1Y7QUFDRjtBQTJDUTtFQUNJLGNBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7QUF6Q1o7QUEyQ1E7RUFDSSxlQUFBO0FBeENaO0FBMENRO0VBQ0ksZ0JBQUE7RUFDQSxlQUFBO0FBdkNaO0FBeUNRO0VBQ0ksV0FBQTtFQUNBLGVBQUE7RUFDQSxrQkFBQTtBQXRDWjtBQXdDUTtFQUNJLGVBQUE7QUFyQ1o7QUF1Q1E7RUFDSSxZQUFBO0FBcENaO0FBc0NRO0VBQ0ksYUFBQTtBQW5DWjtBQW1ESTtFQUNJLGdCQUFBO0FBaERSO0FBbURJO0VBQ0ksMEJBQUE7RUFDQSxzQkFBQTtFQUNBLDZCQUFBO0FBaERSO0FBd0RJO0VBQ0ksd0JBQUE7QUFyRFI7QUEwREk7RUFDSSxnQkFBQTtBQXZEUjtBQTBERztFQUNDLGdCQUFBO0VBQ0EsZ0JBQUE7QUF2REo7QUEwREE7RUFBOEIsZUFBQTtBQXREOUI7QUF1REE7RUFBcUIsZUFBQTtBQW5EckI7QUFtRHdDLGdCQUFBO0FBQ3hDO0VBQXlCLGVBQUE7QUEvQ3pCO0FBK0M0QyxPQUFBO0FBQzVDO0VBQXlCLGVBQUE7QUEzQ3pCO0FBb0ZBO0VBQ0kscUJBQUE7QUFqRko7QUFtRkE7RUFDSSx5QkFBQTtBQWhGSjtBQWtGQTtFQUNJLHlCQUFBO0FBL0VKO0FBaUZBO0VBQ0ksY0FBQTtFQUNBLGVBQUE7QUE5RUo7QUFnRkE7RUFDSSxrQkFBQTtFQUNBLGlCQUFBO0FBN0VKO0FBK0VBO0VBQ0ksaUJBQUE7RUFDQSxrQkFBQTtBQTVFSjtBQThFQTtFQUNJLGVBQUE7QUEzRUo7QUE2RUE7RUFDSSxpQkFBQTtBQTFFSjtBQTRFQTtFQUNJLHlCQUFBO0VBQ0EsY0FBQTtFQUNBLDJCQUFBO0FBekVKO0FBMkVBO0VBQ0ksMkJBQUE7QUF4RUo7QUEwRUE7RUFDSSxnQkFBQTtFQUNBLGtCQUFBO0FBdkVKIiwiZmlsZSI6InNyYy9hcHAvY2F0ZWdvcnktc2VsZWN0aW9uL2NhdGVnb3J5LXNlbGVjdGlvbi5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIgICAgICAgIFtiYWNrZ3JvdW5kXSB7XHJcbiAgICAgICAgICAgIG1hcmdpbjogMCAtMTVweDtcclxuICAgICAgICAgICAgaW9uLWNvbnRlbnQge1xyXG4gICAgICAgICAgICAgICAgLS1iYWNrZ3JvdW5kOiAjNTI1MjUyO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIFxyXG4gICAgICAgIFtiYWNrZ3JvdW5kLXRleHRdIHtcclxuICAgICAgICAgICAgbWFyZ2luOiAwO1xyXG4gICAgICAgICAgICBiYWNrZ3JvdW5kOiB2YXIoLS1pb24tY29sb3IteWVsbG93KTtcclxuICAgICAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgICAgIH1cclxuICAgICAgICBcclxuICAgICAgICBbaW9uLWNvbC1oZWlnaHRdIHtcclxuICAgICAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgICAgIC8vIGhlaWdodDogMTcwcHg7XHJcbiAgICAgICAgICAgIC8vIG1heC1oZWlnaHQ6MjUwcHg7XHJcbiAgICAgICAgICAgIHotaW5kZXg6IDEwMDtcclxuICAgICAgICB9XHJcbiAgICAgICAgXHJcbiAgICAgICAgLy8gLy9leGFtcGxlXHJcbiAgICAgICAgLmNhdGVnb3JpZXMge1xyXG4gICAgICAgICAgICBiYWNrZ3JvdW5kOiAjMjkyOTI3O1xyXG4gICAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgICAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xyXG4gICAgICAgICAgICBmbGV4LXdyYXA6IHdyYXA7XHJcbiAgICAgICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgICAgICAgICAvLyBtYXJnaW46IDI1cHQgMDtcclxuICAgICAgICAgICAgaW9uLWJ1dHRvbixcclxuICAgICAgICAgICAgaW9uLWl0ZW0ge1xyXG4gICAgICAgICAgICAgICAgbWFyZ2luOiAycHg7XHJcbiAgICAgICAgICAgICAgICBib3JkZXItcmFkaXVzOiAzMHB4O1xyXG4gICAgICAgICAgICAgICAgd2lkdGg6IDk1cHg7XHJcbiAgICAgICAgICAgICAgICBmb250LXNpemU6IDEycHg7XHJcbiAgICAgICAgICAgICAgICAvLyB3aWR0aDogODBweDtcclxuICAgICAgICAgICAgICAgIC8vIGZvbnQtc2l6ZTogMTBweDtcclxuICAgICAgICAgICAgICAgIC8vIGhlaWdodDogNDBweDtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgICBcclxuICAgICAgICAvL2V4YW1wbGVcclxuICAgICAgICAvLyAuY2F0ZWdvcmllcyB7XHJcbiAgICAgICAgLy8gICAgIGJhY2tncm91bmQ6ICMyOTI5Mjc7XHJcbiAgICAgICAgLy8gICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgLy8gICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XHJcbiAgICAgICAgLy8gICAgIGZsZXgtd3JhcDogd3JhcDtcclxuICAgICAgICAvLyAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICAgICAgLy8gICAgIC8vIG1hcmdpbjogMjVwdCAwO1xyXG4gICAgICAgIC8vICAgICBkaXYge1xyXG4gICAgICAgIC8vICAgICAgICAgbWFyZ2luOiA1cHg7XHJcbiAgICAgICAgLy8gICAgICAgICBib3JkZXItcmFkaXVzOiAzMHB4O1xyXG4gICAgICAgIC8vICAgICAgICAgd2lkdGg6IDkwcHg7XHJcbiAgICAgICAgLy8gICAgICAgICAvLyBoZWlnaHQ6IDQwcHg7XHJcbiAgICAgICAgLy8gICAgIH1cclxuICAgICAgICAvLyB9XHJcbiAgICAgICAgLnVuLXNlbGVjdGVkIHtcclxuICAgICAgICAgICAgLS1iYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcclxuICAgICAgICAgICAgYm9yZGVyOiAjZmZmZmZmIDJweCBzb2xpZDtcclxuICAgICAgICAgICAgY29sb3I6IHdoaXRlIWltcG9ydGFudDtcclxuICAgICAgICB9XHJcbiAgICAgICAgXHJcbiAgICAgICAgLnNlbGVjdGVkIHtcclxuICAgICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogdmFyKC0taW9uLWNvbG9yLXllbGxvdyk7IC8vI0Y2Qzc2MjtcclxuICAgICAgICAgICAgY29sb3I6ICMwMDAwMDA7XHJcbiAgICAgICAgICAgIGJvcmRlcjogdmFyKC0taW9uLWNvbG9yLXllbGxvdyk7IC8vI0Y2Qzc2MiAycHggc29saWQ7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIFxyXG4gICAgICAgIGlvbi1jb250ZW50IHtcclxuICAgICAgICAgICAgLS1iYWNrZ3JvdW5kOiAjMjkyOTI3O1xyXG4gICAgICAgICAgICAvLyAtLWJhY2tncm91bmQ6ICNmZmZmZmY7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIFxyXG4gICAgICAgIC52aWRlby1wbGF5ZXIxIHtcclxuICAgICAgICAgICAgYmFja2dyb3VuZDogIzFmMWUxZDtcclxuICAgICAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgICAgIGhlaWdodDogMzAwcHg7XHJcbiAgICAgICAgICAgIHZpZGVvIHtcclxuICAgICAgICAgICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgICAgICAgICAgICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgICAgICAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIFxyXG4gICAgICAgIC5pbWctcHJldmlldyB7XHJcbiAgICAgICAgICAgIC8vIG9iamVjdC1maXQ6IGNvdmVyO1xyXG4gICAgICAgICAgICBvYmplY3QtZml0OiBjb250YWluO1xyXG4gICAgICAgICAgICBvYmplY3QtcG9zaXRpb246IDUwJSA3NSU7XHJcbiAgICAgICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgICAgICAgICAvLyBoZWlnaHQ6IDI1MHB4O1xyXG4gICAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiB2YXIoLS1pb24tY29sb3ItcGFudG9uZWJsYWNrKTsgLy9saWdodGVuKHZhcigtLWlvbi1jb2xvci15ZWxsb3cpLCA3MCUpO1xyXG4gICAgICAgICAgICBoZWlnaHQ6IDMwMHB4O1xyXG4gICAgICAgIH1cclxuICAgICAgICBcclxuICAgICAgICAuY29sbGFwc2UtY29udGVudCB7XHJcbiAgICAgICAgICAgIHdpZHRoOiA1MHZ3O1xyXG4gICAgICAgICAgICBtYXJnaW46IGF1dG87XHJcbiAgICAgICAgICAgIGJveC1zaGFkb3c6IDEwcHggMTBweCA1cHggMHB4IHJnYmEoMCwgMCwgMCwgMC43NSk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIFxyXG4gICAgICAgIC5jb2xsYXBzZSB7XHJcbiAgICAgICAgICAgIGJhY2tncm91bmQ6ICNmZmY7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIFxyXG4gICAgICAgIC5jb2xsYXBzZSBhIHtcclxuICAgICAgICAgICAgZGlzcGxheTogYmxvY2s7XHJcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMXJlbTtcclxuICAgICAgICAgICAgZm9udC13ZWlnaHQ6IDUwMDtcclxuICAgICAgICAgICAgcGFkZGluZzogMC45cmVtIDEuOHJlbTtcclxuICAgICAgICAgICAgY29sb3I6ICNmZmY7XHJcbiAgICAgICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgICAgICB9XHJcbiAgICAgICAgXHJcbiAgICAgICAgLmNvbGxhcHNlIGEuaW5zdGFncmFtIHtcclxuICAgICAgICAgICAgYmFja2dyb3VuZDogI2RiMmU3ZDtcclxuICAgICAgICB9XHJcbiAgICAgICAgXHJcbiAgICAgICAgLmNvbGxhcHNlIGEudHdpdHRlciB7XHJcbiAgICAgICAgICAgIGJhY2tncm91bmQ6ICMxZGFiZGQ7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIFxyXG4gICAgICAgIC5jb2xsYXBzZSBhLmRyaWJiYmxlIHtcclxuICAgICAgICAgICAgYmFja2dyb3VuZDogIzQ0NDtcclxuICAgICAgICB9XHJcbiAgICAgICAgXHJcbiAgICAgICAgLmNvbGxhcHNlIGEueW91dHViZSB7XHJcbiAgICAgICAgICAgIGJhY2tncm91bmQ6ICNmZjAwMDA7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIFxyXG4gICAgICAgIC5jb2xsYXBzZSBhOmJlZm9yZSB7XHJcbiAgICAgICAgICAgIGNvbnRlbnQ6IFwiXCI7XHJcbiAgICAgICAgICAgIGJvcmRlci10b3A6IDdweCBzb2xpZCAjZmZmO1xyXG4gICAgICAgICAgICBib3JkZXItbGVmdDogN3B4IHNvbGlkIHRyYW5zcGFyZW50O1xyXG4gICAgICAgICAgICBib3JkZXItcmlnaHQ6IDdweCBzb2xpZCB0cmFuc3BhcmVudDtcclxuICAgICAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICAgICAgICB0b3A6IDI1cHg7XHJcbiAgICAgICAgICAgIHJpZ2h0OiAzMHB4O1xyXG4gICAgICAgIH1cclxuICAgICAgICBcclxuICAgICAgICAvLyAuY29udGVudCB7XHJcbiAgICAgICAgLy8gICB0cmFuc2l0aW9uOiAwLjNzIGxpbmVhciAwcztcclxuICAgICAgICAvLyAgIG92ZXJmbG93OiBoaWRkZW47XHJcbiAgICAgICAgLy8gfVxyXG4gICAgICAgIC5jb2xsYXBzZSsuY29sbGFwc2UgYSB7XHJcbiAgICAgICAgICAgIGJvcmRlci10b3A6IDFweCBzb2xpZCByZ2JhKDI1NSwgMjU1LCAyNTUsIDAuNyk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIFxyXG4gICAgICAgIGgzIHtcclxuICAgICAgICAgICAgbWFyZ2luLWJvdHRvbTogMTVweDtcclxuICAgICAgICB9XHJcbiAgICAgICAgXHJcbiAgICAgICAgLmNvbGxhcHNlOnRhcmdldCAuY29udGVudCB7XHJcbiAgICAgICAgICAgIG1heC1oZWlnaHQ6IDE1ZW07XHJcbiAgICAgICAgfVxyXG4gICAgICAgIFxyXG4gICAgICAgIC5jb2xsYXBzZTp0YXJnZXQgYTpiZWZvcmUge1xyXG4gICAgICAgICAgICB0cmFuc2Zvcm06IHJvdGF0ZSgtOTBkZWcpO1xyXG4gICAgICAgIH1cclxuICAgICAgICBcclxuICAgICAgICBbZmFiLXpvb21dIHtcclxuICAgICAgICAgICAgYm90dG9tOiAyMHB4O1xyXG4gICAgICAgICAgICByaWdodDogY2FsYygyMHB4ICsgdmFyKC0taW9uLXNhZmUtYXJlYS1yaWdodCwgMHB4KSk7XHJcbiAgICAgICAgICAgIC8vIGlvbi1mYWItYnV0dG9uIHtcclxuICAgICAgICAgICAgLy8gICAgIHdpZHRoOiA0MHB4O1xyXG4gICAgICAgICAgICAvLyAgICAgaGVpZ2h0OiA0MHB4O1xyXG4gICAgICAgICAgICAvLyB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAucHJvZ3Jlc3N7XHJcbiAgICAgICAgICAgIGhlaWdodDogMTBweDtcclxuICAgICAgICAgICAgLy8gd2lkdGg6IDkwJTtcclxuICAgICAgICAgICAgLS1idWZmZXItYmFja2dyb3VuZDp3aGl0ZTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIC5jYXQtZHJvcGRvd24ge1xyXG4gICAgICAgICAgICAtLWJhY2tncm91bmQ6ICNmZmZmZmYwMDtcclxuICAgICAgICAgICAgY29sb3I6ICNmZmZmZmZhNiAhaW1wb3J0YW50O1xyXG4gICAgICAgICAgICBib3JkZXI6IDJweCBzb2xpZCAjZmZmO1xyXG4gICAgICAgICAgICBtYXgtaGVpZ2h0OiA2NSU7XHJcbiAgICAgICAgICAgIC8vIC0tYm9yZGVyLWNvbG9yOiAjNTk1YTU4O1xyXG4gICAgICAgICAgICBib3JkZXItcmFkaXVzOiA3cHg7XHJcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMTFweDtcclxuICAgICAgICAgICAgYm9yZGVyOiAxcHggc29saWQgI2ZmZmZmZmNjO1xyXG4gICAgICAgICAgICAtLXBsYWNlaG9sZGVyLWNvbG9yOiAjZmZmZmZmOTQgO1xyXG4gICAgICAgICAgICAtLWhpZ2hsaWdodC1iYWNrZ3JvdW5kOiByZ2IoMCAwIDAgLyAwJSk7XHJcbiAgICAgICAgICAgICAgXHJcbiAgICAgICAgICAgIC8vIC0tYmFja2dyb3VuZDogI2Y4ZjlmYTI2O1xyXG4gICAgICAgICAgICAvLyBjb2xvcjogd2hpdGUgIWltcG9ydGFudDtcclxuICAgICAgICAgICAgLy8gYm9yZGVyOiAycHggc29saWQgI2ZmZjtcclxuICAgICAgICAgICAgLy8gaGVpZ2h0OiA4NSU7XHJcbiAgICAgICAgICAgIC8vIC0tYm9yZGVyLWNvbG9yOiAjNTk1YTU4O1xyXG4gICAgICAgICAgICAvLyBib3JkZXItcmFkaXVzOiA3cHg7XHJcbiAgICAgICAgICAgIGlvbi1sYWJlbCB7XHJcbiAgICAgICAgICAgICAgICBtYXJnaW4tdG9wOiAtNXB4O1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGlvbi1pbnB1dCB7XHJcbiAgICAgICAgICAgICAgICBtYXJnaW4tdG9wOiAtOXB4O1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC8vIC5jYXQtZHJvcGRvd246aG92ZXIge1xyXG4gICAgICAgIC8vICAgICBib3JkZXI6IDFweCBzb2xpZCAjZmZkNzAwOTk7XHJcbiAgICAgICAgLy8gICAgIGlvbi1sYWJlbCB7XHJcbiAgICAgICAgLy8gICAgICAgICBjb2xvcjogI2ZmZjtcclxuICAgICAgICAvLyAgICAgfVxyXG4gICAgICAgIC8vICAgICBpb24taW5wdXQge1xyXG4gICAgICAgIC8vICAgICAgICAgY29sb3I6ICNmZmY7XHJcbiAgICAgICAgLy8gICAgIH1cclxuICAgICAgICAvLyB9XHJcbiAgICAgICAgLmNhdC1kcm9wZG93bjphY3RpdmUge1xyXG4gICAgICAgICAgICBib3JkZXI6IDFweCBzb2xpZCAjZmZkNzAwOTk7XHJcbiAgICAgICAgICAgIC0tcGxhY2Vob2xkZXItY29sb3I6ICNmZmZmZmZlMyA7XHJcbiAgICAgICAgICAgIGlvbi1sYWJlbCB7XHJcbiAgICAgICAgICAgICAgICBjb2xvcjogI2ZmZjtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBpb24taW5wdXQge1xyXG4gICAgICAgICAgICAgICAgY29sb3I6ICNmZmY7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgICAgLmNhdC1kcm9wZG93bjphY3RpdmUgLmNvbC10ZXh0IHtcclxuICAgICAgICAgICAgY29sb3I6ICNmZmQ3MDA7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC5jb2wtYm9yZGVyIHtcclxuICAgICAgICAgICAgYm9yZGVyLWxlZnQ6IDNweCBzb2xpZCAjZmZkNzAwO1xyXG4gICAgICAgICAgICBtYXgtaGVpZ2h0OiA0NHB4O1xyXG4gICAgICAgIH1cclxuICAgICAgICAuY29sLXRleHQge1xyXG4gICAgICAgICAgICAtLWJvcmRlci1jb2xvcjogIzU5NWE1ODtcclxuICAgICAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgICAgICAgICB0b3A6IDEwcHg7XHJcbiAgICAgICAgICAgIGNvbG9yOiAjZmZmZmZmYTY7XHJcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMTJweDtcclxuICAgICAgICB9XHJcbiAgICAgICAgLmNvbC10ZXh0LXllbGxvdyB7XHJcbiAgICAgICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICAgICAgICAgLy8gdG9wOiAxMHB4O1xyXG4gICAgICAgICAgICBjb2xvcjogI2ZmZDcwMDtcclxuICAgICAgICB9XHJcbiAgICAgICAgLmNvbC10ZXh0LXdoaXRlIHtcclxuICAgICAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgICAgICAgICAvLyB0b3A6IDEwcHg7XHJcbiAgICAgICAgICAgIGNvbG9yOiAjZmZmO1xyXG4gICAgICAgIH1cclxuICAgICAgICAuY29sLXRleHQtaWNvbiB7XHJcbiAgICAgICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICAgICAgICAgLy8gdG9wOiAxMHB4O1xyXG4gICAgICAgICAgICBmb250LXNpemU6IDIxcHg7XHJcbiAgICAgICAgICAgIGNvbG9yOiAjZmZkNzAwO1xyXG4gICAgICAgIH1cclxuICAgICAgICAuY29sb3ItcGlja2VyLWFsaWduIHtcclxuICAgICAgICAgICAgbWF4LXdpZHRoOiAxMDAlO1xyXG4gICAgICAgIH1cclxuICAgICAgICAucHJvZ3Jlc3MtYmFyIHtcclxuICAgICAgICAgICAgbWFyZ2luOiAwcHg7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC5wcm9ncmVzcy1icmVhZGNydW1icy1wYWRkaW5nIHtcclxuICAgICAgICAgICAgcGFkZGluZzogMTBweDtcclxuICAgICAgICAgICAgbGluZS1oZWlnaHQ6IDI1cHg7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlvbi1zZWxlY3Qge1xyXG4gICAgICAgICAgICBtYXJnaW4tYm90dG9tOiAxNXB4O1xyXG4gICAgICAgICAgICBtYXgtd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgICAgIC0tcGFkZGluZy1lbmQ6IDBweDtcclxuICAgICAgICAgICAgLS1wYWRkaW5nLXN0YXJ0OiAwcHg7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC50YWJfYmFyIHtcclxuICAgICAgICAgICAgcGFkZGluZzogNXB4O1xyXG4gICAgICAgICAgICBtYXJnaW4tYm90dG9tOiA3cHg7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC5jb2xvci1pbnB1dCB7XHJcbiAgICAgICAgICAgIG1hcmdpbi10b3A6IDE2cHg7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC5jb21tYW5kLXRhZy1jb3VudCB7XHJcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMTJweDtcclxuICAgICAgICAgICAgbWFyZ2luLWxlZnQ6IDIwMHB4O1xyXG4gICAgICAgICAgICBjb2xvcjogI2ZmZmZmZjYxO1xyXG4gICAgICAgIH1cclxuICAgICBcclxuXHJcbiAgICAgICAgLy8gVEFCU1xyXG4gICAgICAgXHJcbiAgICAgICAgLyogPHNvdXJjZSA6aHR0cDovL3dlYmRlc2lnbmVyaHV0LmNvbS9leGFtcGxlcy9wdXJlLWNzcy10YWJzLz4gKi9cclxuXHJcbiAgICAgICAgLyogPHNvdXJjZSA6aHR0cDovL3dlYmRlc2lnbmVyaHV0LmNvbS9leGFtcGxlcy9wdXJlLWNzcy10YWJzLz4gKi9cclxuXHJcbiAgICAgICAgLnRhYnMge1xyXG4gICAgICAgICAgICBtYXgtd2lkdGg6IDkwJTtcclxuICAgICAgICAgICAgZmxvYXQ6IG5vbmU7XHJcbiAgICAgICAgICAgIGxpc3Qtc3R5bGU6IG5vbmU7XHJcbiAgICAgICAgICAgIHBhZGRpbmc6IDA7XHJcbiAgICAgICAgICAgIG1hcmdpbjogMHB4IGF1dG87XHJcbiAgICAgICAgICAgIC8vIGJvcmRlci1ib3R0b206IDRweCBzb2xpZCAjY2NjO1xyXG4gICAgICAgIH1cclxuICAgICAgICAudGFiczphZnRlciB7XHJcbiAgICAgICAgICAgIGNvbnRlbnQ6ICcnO1xyXG4gICAgICAgICAgICBkaXNwbGF5OiB0YWJsZTtcclxuICAgICAgICAgICAgY2xlYXI6IGJvdGg7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC50YWJzIGlucHV0W3R5cGU9cmFkaW9dIHtcclxuICAgICAgICAgICAgZGlzcGxheTpub25lO1xyXG4gICAgICAgIH1cclxuICAgICAgICAudGFicyBsYWJlbCB7XHJcbiAgICAgICAgICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gICAgICAgICAgICBmbG9hdDogbGVmdDtcclxuICAgICAgICAgICAgd2lkdGg6IDMzLjMzMzMlO1xyXG4gICAgICAgICAgICBjb2xvcjogI2NjYztcclxuICAgICAgICAgICAgZm9udC1zaXplOiAxNHB4O1xyXG4gICAgICAgICAgICBmb250LXdlaWdodDogbm9ybWFsO1xyXG4gICAgICAgICAgICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XHJcbiAgICAgICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICAgICAgICAgbGluZS1oZWlnaHQ6IDM7XHJcbiAgICAgICAgICAgIGN1cnNvcjogcG9pbnRlcjtcclxuICAgICAgICAgICAgLy8gYm94LXNoYWRvdzogaW5zZXQgMCA0cHggI2NjYztcclxuICAgICAgICAgICAgLy8gYm9yZGVyLWJvdHRvbTogNHB4IHNvbGlkICNjY2M7XHJcbiAgICAgICAgICAgIC13ZWJraXQtdHJhbnNpdGlvbjogYWxsIDAuNXM7IC8qIFNhZmFyaSAzLjEgdG8gNi4wICovXHJcbiAgICAgICAgICAgIHRyYW5zaXRpb246IGFsbCAwLjVzO1xyXG4gICAgICAgIH1cclxuICAgICAgICAudGFicyBsYWJlbCBzcGFuIHtcclxuICAgICAgICAgICAgZGlzcGxheTogbm9uZTtcclxuICAgICAgICB9XHJcbiAgICAgICAgLnRhYnMgbGFiZWwgaSB7XHJcbiAgICAgICAgICAgIHBhZGRpbmc6IDVweDtcclxuICAgICAgICAgICAgbWFyZ2luLXJpZ2h0OiAwO1xyXG4gICAgICAgIH1cclxuICAgICAgICAvLyAudGFicyBsYWJlbDpob3ZlciB7ICBcclxuICAgICAgICAvLyAgICAgY29sb3I6ICNmZmQ3MDA7XHJcbiAgICAgICAgLy8gICAgIGJveC1zaGFkb3c6IGluc2V0IDAgNHB4ICNmZmQ3MDA7XHJcbiAgICAgICAgLy8gICAgIGJvcmRlci1ib3R0b206IDRweCBzb2xpZCAjZmZkNzAwO1xyXG4gICAgICAgIC8vIH1cclxuICAgICAgICAudGFiLWNvbnRlbnQge1xyXG4gICAgICAgICAgICBkaXNwbGF5OiBub25lO1xyXG4gICAgICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgICAgICAgZmxvYXQ6IGxlZnQ7XHJcbiAgICAgICAgICAgIC8vIHBhZGRpbmc6IDE1cHg7XHJcbiAgICAgICAgICAgIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XHJcbiAgICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6IzI5MjkyNztcclxuICAgICAgICB9XHJcbiAgICAgICAgLnRhYi1jb250ZW50ICoge1xyXG4gICAgICAgICAgICAtd2Via2l0LWFuaW1hdGlvbjogc2NhbGUgMC43cyBlYXNlLWluLW91dDtcclxuICAgICAgICAgICAgLW1vei1hbmltYXRpb246IHNjYWxlIDAuN3MgZWFzZS1pbi1vdXQ7XHJcbiAgICAgICAgICAgIGFuaW1hdGlvbjogc2NhbGUgMC43cyBlYXNlLWluLW91dDtcclxuICAgICAgICB9XHJcbiAgICAgICAgQGtleWZyYW1lcyBzY2FsZSB7XHJcbiAgICAgICAgMCUge1xyXG4gICAgICAgICAgICB0cmFuc2Zvcm06IHNjYWxlKDAuOSk7XHJcbiAgICAgICAgICAgIG9wYWNpdHk6IDA7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICA1MCUge1xyXG4gICAgICAgICAgICB0cmFuc2Zvcm06IHNjYWxlKDEuMDEpO1xyXG4gICAgICAgICAgICBvcGFjaXR5OiAwLjU7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAxMDAlIHtcclxuICAgICAgICAgICAgdHJhbnNmb3JtOiBzY2FsZSgxKTtcclxuICAgICAgICAgICAgb3BhY2l0eTogMTtcclxuICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC50YWJzIFtpZF49XCJ0YWJcIl06Y2hlY2tlZCArIGxhYmVsIHtcclxuICAgICAgICAgICAgYmFja2dyb3VuZDogI2ZmZmZmZjAwO1xyXG4gICAgICAgICAgICAvLyBib3gtc2hhZG93OiBpbnNldCAwIDRweCAjZmZkNzAwO1xyXG4gICAgICAgICAgICBib3JkZXItYm90dG9tOiA0cHggc29saWQgI2ZmZDcwMDtcclxuICAgICAgICAgICAgY29sb3I6ICNmZmQ3MDA7XHJcbiAgICAgICAgfVxyXG4gICAgICAgICN0YWIxOmNoZWNrZWQgfiAjdGFiLWNvbnRlbnQxLFxyXG4gICAgICAgICN0YWIyOmNoZWNrZWQgfiAjdGFiLWNvbnRlbnQyLFxyXG4gICAgICAgICN0YWIzOmNoZWNrZWQgfiAjdGFiLWNvbnRlbnQzIHtcclxuICAgICAgICAgICAgZGlzcGxheTogYmxvY2s7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIEBtZWRpYSAobWluLXdpZHRoOiA3NjhweCkge1xyXG4gICAgICAgICAgICAudGFicyBpIHtcclxuICAgICAgICAgICAgICAgIHBhZGRpbmc6IDVweDtcclxuICAgICAgICAgICAgICAgIG1hcmdpbi1yaWdodDogMTBweDtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAudGFicyBsYWJlbCBzcGFuIHtcclxuICAgICAgICAgICAgICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAudGFicyB7XHJcbiAgICAgICAgICAgIG1heC13aWR0aDogOTUwcHg7XHJcbiAgICAgICAgICAgIG1hcmdpbjogNTBweCBhdXRvO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC5zd2F0Y2gtaWNvbiB7XHJcbiAgICAgICAgICAgIGNvbG9yOiAjYzFjMWMxO1xyXG4gICAgICAgICAgICBmb250LXNpemU6IDIwcHg7XHJcbiAgICAgICAgICAgIG1hcmdpbi1sZWZ0OiA1cHg7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC5zd2F0Y2gtaWNvbjpob3ZlciB7XHJcbiAgICAgICAgICAgIGN1cnNvcjogcG9pbnRlcjtcclxuICAgICAgICB9XHJcbiAgICAgICAgLnN3YXRjaC1sYWJlbCB7XHJcbiAgICAgICAgICAgIGNvbG9yOiAjZmZmZmZmOTQ7XHJcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMTJweDtcclxuICAgICAgICB9XHJcbiAgICAgICAgLnN3YXRjaC1sYWJlbC1jb2xvciB7XHJcbiAgICAgICAgICAgIGNvbG9yOiAjZmZmO1xyXG4gICAgICAgICAgICBmb250LXNpemU6IDEycHg7XHJcbiAgICAgICAgICAgIG1hcmdpbi1sZWZ0OiAtMjVweDtcclxuICAgICAgICB9XHJcbiAgICAgICAgLnN3YXRjaC1sYWJlbC1jb2xvcjpob3ZlciB7XHJcbiAgICAgICAgICAgIGN1cnNvcjogcG9pbnRlcjtcclxuICAgICAgICB9XHJcbiAgICAgICAgLnN3YXRjaC1pdGVte1xyXG4gICAgICAgICAgICBoZWlnaHQ6IDQwcHg7XHJcbiAgICAgICAgfVxyXG4gICAgICAgICNzd2F0Y2gtcmFkaW8ge1xyXG4gICAgICAgICAgICBkaXNwbGF5OiBub25lO1xyXG4gICAgICAgIH1cclxuICAgICAgICAvLyAuc3dhdGNoLWl0ZW0tcm93LWNvbnRlbnR7XHJcbiAgICAgICAgLy8gICAgIGRpc3BsYXk6IG5vbmU7XHJcbiAgICAgICAgLy8gfVxyXG4gICAgICAgIC8vICNzd2F0Y2gtcmFkaW86Y2hlY2tlZCB+ICNzd2F0Y2gtaXRlbS1yb3cge1xyXG4gICAgICAgIC8vICAgICBkaXNwbGF5OiBibG9jaztcclxuICAgICAgICAvLyB9XHJcbiAgICAvLyAgICAuaW9uLXRhYi1idG57XHJcbiAgICAvLyAgICAgLS1iYWNrZ3JvdW5kOiNmZmQ3MDA7XHJcbiAgICAvLyAgICAgLS1iYWNrZ3JvdW5kLWZvY3VzZWQ6I2ZmZDcwMDtcclxuICAgIC8vICAgICAtLWNvbG9yOiMyOTI5Mjc7XHJcbiAgICAvLyAgICAgLS1jb2xvci1mb2N1c2VkOiMyOTI5Mjc7XHJcbiAgICAvLyAgICAgLS1jb2xvci1zZWxlY3RlZDojMjkyOTI3O1xyXG4gICAgLy8gICAgfVxyXG5cclxuICAgIC5jaGstbXJ7XHJcbiAgICAgICAgbWFyZ2luLXRvcDogMjVweDtcclxuICAgIH1cclxuXHJcbiAgICAuY29sb3ItcGlja2VyIHtcclxuICAgICAgICB0b3A6IC0zNTguMDA5cHggIWltcG9ydGFudDtcclxuICAgICAgICBsZWZ0OiAtODBweCAhaW1wb3J0YW50O1xyXG4gICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZSAhaW1wb3J0YW50O1xyXG4gICAgfVxyXG4gICAgLy8gLmlvbi1zbGN0LTF7XHJcbiAgICAvLyAgICAgbWluLXdpZHRoOiAxNnB4O1xyXG4gICAgLy8gfVxyXG4gICAgLy8gLmlvbi1zbGN0LTJ7XHJcbiAgICAvLyAgICAgICAgIG1pbi13aWR0aDogMjM1cHg7XHJcbiAgICAvLyB9XHJcbiAgICBpb24tc2VsZWN0OjpwYXJ0KGljb24pIHtcclxuICAgICAgICBkaXNwbGF5OiBub25lICFpbXBvcnRhbnQ7XHJcbiAgICAgICB9XHJcbiAgICAvLyAuYWxlcnQtY3VzdG9tLWNsYXNzLWluZm8ge1xyXG4gICAgLy8gICAgIC0tYmFja2dyb3VuZDogYXF1YTtcclxuICAgIC8vICAgfVxyXG4gICAgLnNlbGVjdC10ZXh0IHtcclxuICAgICAgICBtaW4td2lkdGg6IDI1MHB4O1xyXG4gICAgfVxyXG5cclxuICAgLnN3YXRjaC1pdGVte1xyXG4gICAgcGFkZGluZzogMHB4IDBweDtcclxuICAgIG1hcmdpbjogNXB4IC01cHg7XHJcbiAgICB9XHJcblxyXG46Oi13ZWJraXQtaW5wdXQtcGxhY2Vob2xkZXIgeyBmb250LXNpemU6IDE0cHg7IH1cclxuOjotbW96LXBsYWNlaG9sZGVyIHsgZm9udC1zaXplOiAxNHB4OyB9IC8qIGZpcmVmb3ggMTkrICovXHJcbjotbXMtaW5wdXQtcGxhY2Vob2xkZXIgeyBmb250LXNpemU6IDE0cHg7IH0gLyogaWUgKi9cclxuaW5wdXQ6LW1vei1wbGFjZWhvbGRlciB7IGZvbnQtc2l6ZTogMTRweDsgfVxyXG5cclxuXHJcbi8vIC5hbGVydC13cmFwcGVyLnNjLWlvbi1hbGVydC1tZHtcclxuLy8gICAgIGJhY2tncm91bmQ6dHJhbnNwYXJlbnQ7XHJcbi8vIH1cclxuLy8gLmFsZXJ0LXRpdGxlLnNjLWlvbi1hbGVydC1tZHtcclxuLy8gICAgIGNvbG9yOiAjZjlmOWY5O1xyXG4vLyB9XHJcbi8vIC5hbGVydC1yYWRpby1pbm5lci5zYy1pb24tYWxlcnQtbWR7XHJcbi8vICAgICBjb2xvcjogI2Y5ZjlmOTtcclxuLy8gfVxyXG4vLyAuYWxlcnQtcmFkaW8tbGFiZWwuc2MtaW9uLWFsZXJ0LW1ke1xyXG4vLyAgICAgY29sb3I6ICNmOWY5Zjk7XHJcbi8vIH1cclxuLy8gLmFsZXJ0LWJ1dHRvbi5zYy1pb24tYWxlcnQtbWQge1xyXG4vLyAgICAgY29sb3I6ICNmOWY5Zjk7XHJcbi8vIH1cclxuLy8gW2FyaWEtY2hlY2tlZD10cnVlXS5zYy1pb24tYWxlcnQtbWQgLmFsZXJ0LXJhZGlvLWxhYmVsLnNjLWlvbi1hbGVydC1tZHtcclxuLy8gICAgIGNvbG9yOiAjZjlmOWY5O1xyXG4vLyB9XHJcbi8vIC5hbGVydC13cmFwcGVyLnNjLWlvbi1hbGVydC1pb3N7XHJcbi8vICAgICBiYWNrZ3JvdW5kOnRyYW5zcGFyZW50O1xyXG4vLyB9XHJcbi8vIC5hbGVydC10aXRsZS5zYy1pb24tYWxlcnQtaW9ze1xyXG4vLyAgICAgY29sb3I6ICNmOWY5Zjk7XHJcbi8vIH1cclxuLy8gLmFsZXJ0LXJhZGlvLWlubmVyLnNjLWlvbi1hbGVydC1pb3N7XHJcbi8vICAgICBjb2xvcjogI2Y5ZjlmOTtcclxuLy8gfVxyXG4vLyAuYWxlcnQtcmFkaW8tbGFiZWwuc2MtaW9uLWFsZXJ0LWlvc3tcclxuLy8gICAgIGNvbG9yOiAjZjlmOWY5O1xyXG4vLyB9XHJcbi8vIC5hbGVydC1idXR0b24uc2MtaW9uLWFsZXJ0LWlvcyB7XHJcbi8vICAgICBjb2xvcjogI2Y5ZjlmOTtcclxuLy8gfVxyXG4vLyBbYXJpYS1jaGVja2VkPXRydWVdLnNjLWlvbi1hbGVydC1pb3MgLmFsZXJ0LXJhZGlvLWxhYmVsLnNjLWlvbi1hbGVydC1pb3N7XHJcbi8vICAgICBjb2xvcjogI2Y5ZjlmOTtcclxuLy8gfVxyXG5cclxuXHJcbi5ib3JkZXItcmVke1xyXG4gICAgYm9yZGVyOiAxcHggc29saWQgcmVkO1xyXG59XHJcbi5ib3JkZXIteWVsbG93e1xyXG4gICAgYm9yZGVyOiAxcHggc29saWQgI2ZmZDcwMDtcclxufVxyXG4uaW9uLWNvbG9yLXllbGxvdyB7XHJcbiAgICAtLWlvbi1jb2xvci1jb250cmFzdDojZmZmO1xyXG59XHJcbi5hcnJvd3tcclxuICAgIGNvbG9yOiAjYjViNWI0O1xyXG4gICAgZm9udC1zaXplOiAxMHB4O1xyXG59XHJcbi51cC1hcnJvd3tcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIG1hcmdpbi10b3A6IC0xMHB4O1xyXG59XHJcbi5kb3duLWFycm93e1xyXG4gICAgbWFyZ2luLXJpZ2h0OiA1cHg7XHJcbiAgICBtYXJnaW4tYm90dG9tOiAycHg7XHJcbn1cclxuLmljb24tc3dhdGNoe1xyXG4gICAgZm9udC1zaXplOiA0MHB4O1xyXG59XHJcbi5ib3JkZXItZ3JlZW57XHJcbiAgICBib3JkZXI6IDFweCBzb2xpZCA7XHJcbn1cclxuLmNoay1pdGVte1xyXG4gICAgLS1iYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcclxuICAgIC0tY29sb3I6IHdoaXRlO1xyXG4gICAgLS1ib3JkZXItY29sb3I6IHRyYW5zcGFyZW50O1xyXG59XHJcbi5ib3JkZXItd2hpdGV7XHJcbiAgICBib3JkZXI6IDFweCBzb2xpZCAjZmZmZmZmY2M7XHJcbn1cclxuLmNvbC1tcntcclxuICAgIG1hcmdpbjogMTBweCAwcHg7XHJcbiAgICBib3JkZXItcmFkaXVzOiA3cHg7XHJcbn1cclxuXHJcbi8vIC5hbGVydC1zdWItdGl0bGUuc2MtaW9uLWFsZXJ0LW1ke1xyXG4vLyAgICAgZm9udC1zaXplOiB1bnNldDtcclxuLy8gICAgfVxyXG5cclxuLy8gICAgLmFsZXJ0LWhlYWQuc2MtaW9uLWFsZXJ0LW1kKy5hbGVydC1tZXNzYWdlLnNjLWlvbi1hbGVydC1tZCB7XHJcbi8vICAgICAgICBwYWRkaW5nLXRvcDogdW5zZXQ7XHJcbi8vICAgIH1cclxuLy8gICAgLmFsZXJ0LW1lc3NhZ2Uuc2MtaW9uLWFsZXJ0LWlvcywgLmFsZXJ0LWlucHV0LWdyb3VwLnNjLWlvbi1hbGVydC1pb3Mge1xyXG4vLyAgICAgICAgcGFkZGluZy10b3A6IHVuc2V0O1xyXG4vLyAgICB9XHJcbi8vICAgIC5hbGVydC1idXR0b24taW5uZXIuc2MtaW9uLWFsZXJ0LW1kIHtcclxuLy8gICAgICAgIGNvbG9yOiB1bnNldDtcclxuLy8gICAgfSJdfQ== */";
      /***/
    },

    /***/
    "./src/app/category-selection/category-selection.page.ts":
    /*!***************************************************************!*\
      !*** ./src/app/category-selection/category-selection.page.ts ***!
      \***************************************************************/

    /*! exports provided: CategorySelectionPage */

    /***/
    function srcAppCategorySelectionCategorySelectionPageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "CategorySelectionPage", function () {
        return CategorySelectionPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _global_services_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ../global-services.service */
      "./src/app/global-services.service.ts");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _ionic_native_ionic_webview_ngx__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @ionic-native/ionic-webview/ngx */
      "./node_modules/@ionic-native/ionic-webview/__ivy_ngcc__/ngx/index.js");
      /* harmony import */


      var _ionic_native_keyboard__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! @ionic-native/keyboard */
      "./node_modules/@ionic-native/keyboard/index.js");
      /* harmony import */


      var ngx_ionic_image_viewer__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! ngx-ionic-image-viewer */
      "./node_modules/ngx-ionic-image-viewer/__ivy_ngcc__/fesm2015/ngx-ionic-image-viewer.js");
      /* harmony import */


      var _ionic_native_File_ngx__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
      /*! @ionic-native/File/ngx */
      "./node_modules/@ionic-native/file/__ivy_ngcc__/ngx/index.js");
      /* harmony import */


      var ionic_cache__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
      /*! ionic-cache */
      "./node_modules/ionic-cache/__ivy_ngcc__/dist/index.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");

      var CategorySelectionPage = /*#__PURE__*/function () {
        function CategorySelectionPage(file, service, menu, navCtrl, popoverController, alertController, router, route, webview, modalController, toastController, cache, location, platform, formBuilder) {
          _classCallCheck(this, CategorySelectionPage);

          this.file = file;
          this.service = service;
          this.menu = menu;
          this.navCtrl = navCtrl;
          this.popoverController = popoverController;
          this.alertController = alertController;
          this.router = router;
          this.route = route;
          this.webview = webview;
          this.modalController = modalController;
          this.toastController = toastController;
          this.cache = cache;
          this.location = location;
          this.platform = platform;
          this.formBuilder = formBuilder;
          this.testList = [{
            testID: 1,
            testName: 'Sports',
            checked: false
          }, {
            testID: 2,
            testName: 'Culture',
            checked: false
          }, {
            testID: 3,
            testName: 'Fashion Week',
            checked: false
          }, {
            testID: 4,
            testName: "Candid",
            checked: false
          }, {
            testID: 5,
            testName: "Sports",
            checked: false
          }, {
            testID: 6,
            testName: "Culture",
            checked: false
          }, {
            testID: 7,
            testName: "Fashion",
            checked: false
          }, {
            testID: 8,
            testName: "Candid",
            checked: false
          }, {
            testID: 9,
            testName: "Sports",
            checked: false
          }, {
            testID: 10,
            testName: "Culture",
            checked: false
          }, {
            testID: 11,
            testName: "Fashion",
            checked: false
          }, {
            testID: 12,
            testName: "Candid",
            checked: false
          }];
          this.selectedArray = [];
          this.showToolbar = true;
          this.posterImage = '';
          this.progressValue = 0.0;
          this.color = "";
          this.singleArrayUrl = "getImgCategory_SingleList";
          this.multiArrayUrl = "getImgCategory_MutipleList";
          this.rootList = [];
          this.level1List = [];
          this.level2List = [];
          this.level3List = [];
          this.dynamicFormFieldsList = [];
          this.breadCrumbFieldsArray = [];
          this.checkboxValidation = [];
          this.toggleCategoryIcon = 'add';
          this.toggleIcon = 'add';

          this.toggleGroup = function (value) {
            this.isGroupShown = value;
            this.toggleIcon = this.toggleIcon == 'add' ? 'remove' : 'add';
          };

          this.toggleCategory = function (value) {
            this.isCategoryShown = value;
            this.toggleCategoryIcon = this.toggleCategoryIcon == 'add' ? 'remove' : 'add';
          };
        }

        _createClass(CategorySelectionPage, [{
          key: "setActive",
          value: function setActive(e) {}
        }, {
          key: "ngOnInit",
          value: function ngOnInit() {
            var _this4 = this;

            this.route.params.subscribe(function (params) {
              _this4.address = params['location'];
              _this4.latitude = params['latitude'];
              _this4.longitude = params['longitude'];
              _this4.type = params['type'];
              _this4.fileInfo = params['fileinfo'];
              _this4.page = params['page'];
              _this4.mediaPath = params['mediaPath']; // alert("line101" + this.address)
              // alert("line102" + this.mediaPath)
            });
            this.getRootList();

            _ionic_native_keyboard__WEBPACK_IMPORTED_MODULE_6__["Keyboard"].onKeyboardShow().subscribe(function () {
              _this4.showToolbar = false;
            });

            _ionic_native_keyboard__WEBPACK_IMPORTED_MODULE_6__["Keyboard"].onKeyboardHide().subscribe(function () {
              _this4.showToolbar = true;
            }); // setInterval(() => {
            //   if (this.progressValue != 1.0) {
            //     this.progressValue = this.progressValue + 0.1;
            //   }
            // }, 1000)


            this.formInit();
          }
        }, {
          key: "ionViewWillEnter",
          value: function ionViewWillEnter() {
            if (this.type == 'video') {
              if (localStorage.getItem('videoThumbnailPosterImage')) {
                this.posterImage = this.service.checkDomSanitizerForIOS(this.webview.convertFileSrc(JSON.parse(localStorage.getItem('videoThumbnailPosterImage'))));
              }

              this.convertFileUritoBase64(this.mediaPath);
              var video = this.myVideo.nativeElement;
              this.mediaPath = this.service.checkDomSanitizerForIOS(this.mediaPath); // video.src = this.webview.convertFileSrc(this.mediaPath);

              if (this.platform.is('android')) {
                this.mediaPath = this.service.checkDomSanitizerForIOS(this.webview.convertFileSrc(this.mediaPath));
                video.src = this.webview.convertFileSrc(this.mediaPath);
                video.load();
              }
            } else {
              if (this.mediaPath.indexOf("data:") !== -1) {
                this.mediaURL = this.mediaPath;
              } else {
                this.mediaURL = this.service.checkDomSanitizerForIOS(this.webview.convertFileSrc(this.mediaPath));
              }
            }

            this.service.sendMessage('startNotificationCountFunction');
          }
        }, {
          key: "formInit",
          value: function formInit() {
            this.imageUploadForm = this.formBuilder.group({
              rootId: new _angular_forms__WEBPACK_IMPORTED_MODULE_11__["FormControl"]('1', _angular_forms__WEBPACK_IMPORTED_MODULE_11__["Validators"].required),
              img_categ: new _angular_forms__WEBPACK_IMPORTED_MODULE_11__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_11__["Validators"].required),
              level1Val: new _angular_forms__WEBPACK_IMPORTED_MODULE_11__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_11__["Validators"].required),
              level2Val: new _angular_forms__WEBPACK_IMPORTED_MODULE_11__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_11__["Validators"].required),
              level3Val: new _angular_forms__WEBPACK_IMPORTED_MODULE_11__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_11__["Validators"].required),
              level4Val: this.formBuilder.array([]),
              colorVal: new _angular_forms__WEBPACK_IMPORTED_MODULE_11__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_11__["Validators"].required),
              // swatch: new FormControl('', Validators.required),
              tags: new _angular_forms__WEBPACK_IMPORTED_MODULE_11__["FormControl"]('')
            });
          }
        }, {
          key: "dynamicValues",
          value: function dynamicValues() {
            return this.imageUploadForm.get("level4Val");
          }
        }, {
          key: "set",
          value: function set() {} //category array list
          // getRootwithLevel1List() {
          //   this.service.post_data(this.multiArrayUrl, { parentid: 0 }).subscribe((result) => {
          //     let json: any = this.service.parserToJSON(result);
          //     let parsedJSON = JSON.parse(json.string);
          //     this.rootList = parsedJSON.category_list;
          //     this.level1List = parsedJSON.Womenswear;
          //   })
          // }

        }, {
          key: "getRootList",
          value: function getRootList() {
            var _this5 = this;

            this.service.post_data(this.singleArrayUrl, {
              parentid: 0
            }).subscribe(function (result) {
              var json = _this5.service.parserToJSON(result);

              var parsedJSON = JSON.parse(json.string);
              _this5.rootList = parsedJSON;
              _this5.activeTab = parsedJSON[0].sno;

              _this5.getLevel1List(parsedJSON[0].sno, parsedJSON[0].categ_name);

              _this5.progressBarAndBreadCrumb('root', parsedJSON[0].categ_name, 1);
            });
          }
        }, {
          key: "getLevel1List",
          value: function getLevel1List(id, name) {
            var _this6 = this;

            // if (this.activeTab != id) {
            this.activeTab = id;
            this.rootId = id;
            this.rootValue = name;
            this.imageUploadForm.get('rootId').patchValue(id);
            this.imageUploadForm.get('img_categ').patchValue(name);
            this.progressBarAndBreadCrumb('root', name, 1);
            this.service.post_data(this.singleArrayUrl, {
              parentid: this.imageUploadForm.get('rootId').value
            }).subscribe(function (result) {
              _this6.formReset();

              var json = _this6.service.parserToJSON(result);

              var parsedJSON = JSON.parse(json.string);
              _this6.level1List = parsedJSON;

              _this6.getLevel2List();
            }); // }
          }
        }, {
          key: "getLevel2List",
          value: function getLevel2List() {
            var _this7 = this;

            if (this.level1List.length != 0) {
              setTimeout(function () {
                _this7.service.post_data(_this7.singleArrayUrl, {
                  parentid: _this7.imageUploadForm.get('level1Val').value
                }).subscribe(function (result) {
                  var json = _this7.service.parserToJSON(result);

                  _this7.imageUploadForm.get('level2Val').reset();

                  var parsedJSON = JSON.parse(json.string);
                  _this7.level2List = parsedJSON;

                  if (_this7.imageUploadForm.get('level1Val').value) {
                    var val = _this7.level1List.filter(function (a) {
                      return a.sno == _this7.imageUploadForm.get('level1Val').value;
                    })[0];

                    _this7.progressBarAndBreadCrumb('level1Val', val.categ_name, 2);
                  }
                });
              }, 500);
            } else {
              this.level2List = [];
              this.getLevel3List();
            }
          }
        }, {
          key: "getLevel3List",
          value: function getLevel3List() {
            var _this8 = this;

            if (this.level2List.length != 0) {
              setTimeout(function () {
                _this8.service.post_data(_this8.singleArrayUrl, {
                  parentid: _this8.imageUploadForm.get('level2Val').value
                }).subscribe(function (result) {
                  _this8.imageUploadForm.get('level3Val').reset();

                  var json = _this8.service.parserToJSON(result);

                  var parsedJSON = JSON.parse(json.string);
                  _this8.level3List = parsedJSON;

                  if (_this8.imageUploadForm.get('level2Val').value) {
                    var val = _this8.level2List.filter(function (a) {
                      return a.sno == _this8.imageUploadForm.get('level2Val').value;
                    })[0];

                    _this8.progressBarAndBreadCrumb('level2Val', val.categ_name, 3);
                  }
                });
              }, 500);
            } else {
              this.level3List = [];
              this.getDynamicFormControlList();
            }
          }
        }, {
          key: "getDynamicFormControlList",
          value: function getDynamicFormControlList() {
            var _this9 = this;

            if (this.level3List.length != 0) {
              setTimeout(function () {
                _this9.service.post_data(_this9.multiArrayUrl, {
                  parentid: _this9.imageUploadForm.get('level3Val').value
                }).subscribe(function (result) {
                  var json = _this9.service.parserToJSON(result);

                  var FormArray = _this9.imageUploadForm.get('level4Val');

                  _this9.dynamicValuesReset();

                  if (JSON.parse(json.string).length != 0) {
                    JSON.parse(json.string).forEach(function (element, i) {
                      element.categ_list.forEach(function (ele) {
                        if (element.ctrl_type == 'chk') {
                          ele.status = false;
                        }
                      });

                      var form = _this9.formBuilder.group({
                        labelName: element.categ_name,
                        labelValue: new _angular_forms__WEBPACK_IMPORTED_MODULE_11__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_11__["Validators"].required),
                        categList: new Array(element.categ_list),
                        ctrlType: element.ctrl_type
                      });

                      var obj = {
                        borderColor: 'white'
                      };

                      _this9.checkboxValidation.push(obj);

                      FormArray.push(form);
                    });
                  }

                  _this9.dynamicFormFieldsList = JSON.parse(json.string);

                  if (_this9.imageUploadForm.get('level3Val').value) {
                    var val = _this9.level3List.filter(function (a) {
                      return a.sno == _this9.imageUploadForm.get('level3Val').value;
                    })[0];

                    _this9.progressBarAndBreadCrumb('level3Val', val.categ_name, 4);
                  }
                });
              }, 500);
            } else {
              this.dynamicValuesReset();
            }
          }
        }, {
          key: "checkbox",
          value: function checkbox(i, j, v) {
            var FormArray = this.imageUploadForm.get('level4Val');
            var FormArray2 = FormArray.at(i).get('categList');
            FormArray2.value.forEach(function (element, a) {
              if (j == a) {
                element.status = v;
              }
            });
            var ii = 0;
            var arr = [];
            FormArray2.value.forEach(function (element, b) {
              if (element.status) {
                ii = 1;
                arr.push(element.categ_name);
              }
            });
            FormArray.at(i).get('labelValue').patchValue(arr);

            if (ii == 0) {
              this.checkboxValidation[i].borderColor = 'red';
            } else if (ii == 1) {
              this.checkboxValidation[i].borderColor = 'yellow';
            }
          }
        }, {
          key: "colorPickerSelect",
          value: function colorPickerSelect(e) {
            this.imageUploadForm.get('colorVal').patchValue(e);
            this.progressBarAndBreadCrumb('color', e, this.imageUploadForm.get('level4Val').value.length + 5);
          }
        }, {
          key: "progressBarAndBreadCrumb",
          value: function progressBarAndBreadCrumb(label, val, i) {
            var _this10 = this;

            if (String(val) == 'Womenswear' || String(val) == 'Menswear' || String(val) == 'Kidswear') {
              this.breadCrumbFieldsArray = [];
            }

            var index = this.breadCrumbFieldsArray.findIndex(function (a) {
              return a.label == label;
            });

            if (index > -1) {
              this.breadCrumbFieldsArray.splice(index, 1);
            }

            var validValues = 0;
            var Form = this.imageUploadForm.controls;
            var FormArray = this.imageUploadForm.get('level4Val');
            var objFields = Object.keys(Form).length - 1;
            var formArrayFields = FormArray.length;
            var totalFeildsLength = objFields + formArrayFields;
            Object.keys(Form).forEach(function (field, i) {
              if (field != 'level4Val') {
                if (_this10.imageUploadForm.get(field).valid) {
                  validValues++;
                }
              }
            });
            FormArray.controls.forEach(function (field, j) {
              if (field.valid) {
                validValues++;
              }
            });
            this.progressValue = Math.round(validValues / totalFeildsLength * 10) / 10;
            var obj = {
              label: label,
              val: val,
              id: i
            };
            this.breadCrumbFieldsArray.push(obj);
            this.breadCrumbFieldsArray.sort(function (a, b) {
              return a.id - b.id;
            });

            if (Number(i) == 1 || Number(i) == 2 || Number(i) == 3) {
              this.breadCrumbFieldsArray.splice(Number(i), this.breadCrumbFieldsArray.length);
            }
          }
        }, {
          key: "dynamicValuesReset",
          value: function dynamicValuesReset() {
            this.color = '';
            this.imageUploadForm.get('colorVal').reset();
            this.imageUploadForm.get('tags').reset();
            this.checkboxValidation = [];
            var FormArray = this.imageUploadForm.get('level4Val');

            while (FormArray.length != 0) {
              FormArray.removeAt(0);
            }
          }
        }, {
          key: "formReset",
          value: function formReset() {
            this.imageUploadForm.reset();
            this.dynamicValuesReset();

            if (this.rootId && this.rootValue) {
              this.imageUploadForm.get('rootId').patchValue(this.rootId);
              this.imageUploadForm.get('img_categ').patchValue(this.rootValue);
            }
          } // ===================Validation part===============

        }, {
          key: "validateAllFormFields",
          value: function validateAllFormFields(formGroup) {
            var _this11 = this;

            Object.keys(formGroup.controls).forEach(function (field) {
              var control = formGroup.get(field);

              if (control instanceof _angular_forms__WEBPACK_IMPORTED_MODULE_11__["FormControl"]) {
                if (control.invalid) {
                  control.markAsTouched({
                    onlySelf: true
                  });
                }
              } else if (control instanceof _angular_forms__WEBPACK_IMPORTED_MODULE_11__["FormGroup"]) {
                _this11.validateAllFormFields(control);
              } else if (control instanceof _angular_forms__WEBPACK_IMPORTED_MODULE_11__["FormArray"]) {
                control.controls.forEach(function (element, i) {
                  _this11.validateAllFormFields(element);
                });
              }
            });
          }
        }, {
          key: "textareaMaxLengthValidation",
          value: function textareaMaxLengthValidation() {
            if (this.imageUploadForm.get('tags').value.length > 50) {
              this.imageUploadForm.get('tags').patchValue(this.imageUploadForm.get('tags').value.slice(0, 50));
            }
          }
        }, {
          key: "checkBoxValidation",
          value: function checkBoxValidation() {
            var _this12 = this;

            var FormArray = this.imageUploadForm.get('level4Val');

            if (FormArray.length > 0) {
              FormArray.value.forEach(function (element, a) {
                if (element.ctrlType == 'chk') {
                  var ii = 0;
                  element.categList.forEach(function (ele, b) {
                    if (ele.status) {
                      ii = 1;
                    }
                  });

                  if (ii == 0) {
                    _this12.checkboxValidation[a].borderColor = 'red';
                  }
                }
              });
            }
          } // ==================

        }, {
          key: "presentToast",
          value: function presentToast(message_content) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
              var toast;
              return regeneratorRuntime.wrap(function _callee$(_context) {
                while (1) {
                  switch (_context.prev = _context.next) {
                    case 0:
                      _context.next = 2;
                      return this.toastController.create({
                        message: message_content,
                        position: 'top',
                        duration: 2000
                      });

                    case 2:
                      toast = _context.sent;
                      toast.present();

                    case 4:
                    case "end":
                      return _context.stop();
                  }
                }
              }, _callee, this);
            }));
          }
        }, {
          key: "presentAlert5",
          value: function presentAlert5(type, data) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
              var alert;
              return regeneratorRuntime.wrap(function _callee2$(_context2) {
                while (1) {
                  switch (_context2.prev = _context2.next) {
                    case 0:
                      _context2.next = 2;
                      return this.alertController.create({
                        cssClass: 'my-custom-class',
                        header: 'Error',
                        subHeader: type,
                        message: data,
                        buttons: ['OK']
                      });

                    case 2:
                      alert = _context2.sent;
                      _context2.next = 5;
                      return alert.present();

                    case 5:
                    case "end":
                      return _context2.stop();
                  }
                }
              }, _callee2, this);
            }));
          } // =====================UPLOAD FILE=====================

        }, {
          key: "uploadFile",
          value: function uploadFile() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee3() {
              var _this13 = this;

              var username, stud_type, filetype, contentType, base64String, mediaType, city, latitude_longitude, baseVale, loc, value, splitted, FormArray, obj, _alert;

              return regeneratorRuntime.wrap(function _callee3$(_context3) {
                while (1) {
                  switch (_context3.prev = _context3.next) {
                    case 0:
                      if (!this.imageUploadForm.valid) {
                        _context3.next = 27;
                        break;
                      }

                      // if (this.selectedArray.length == 0) {
                      //   const alert = await this.alertController.create({
                      //     cssClass: 'alert-custom-class',
                      //     header: 'Error',
                      //     message: 'Please Select the Any Category',
                      //     backdropDismiss: false,
                      //     buttons: [
                      //       {
                      //         text: 'OK',
                      //         cssClass: 'warning',
                      //       }
                      //     ]
                      //   });
                      //   await alert.present();
                      // } else {
                      username = localStorage.getItem('username');
                      stud_type = localStorage.getItem('stud_type');
                      latitude_longitude = [];
                      value = String(this.address).split(",");

                      if (this.platform.is('ios')) {
                        city = value[value.length - 2];
                        loc = value[value.length - 2] + ',' + value[value.length - 1];
                      } else {
                        city = value[4];
                        loc = value[3] + ',' + city;
                      }

                      if (this.platform.is('ios') && this.type == 'video') {
                        baseVale = String(this.mediaPath).split(' ')[4];
                      } else {
                        baseVale = this.mediaPath;
                      } // alert('line507 ---' + baseVale.indexOf("data:"))


                      if (!(baseVale.indexOf("data:") !== -1)) {
                        _context3.next = 11;
                        break;
                      }

                      mediaType = baseVale;
                      _context3.next = 14;
                      break;

                    case 11:
                      _context3.next = 13;
                      return this.service.imagetobase64(baseVale);

                    case 13:
                      mediaType = _context3.sent;

                    case 14:
                      // alert('line510 ---' + mediaType)
                      splitted = mediaType.split(",");
                      base64String = splitted[1].toString();

                      if (this.type == 'image') {
                        contentType = "image/*";
                        filetype = "jpg";
                      } else {
                        if (this.platform.is('ios')) {
                          contentType = "video/*";
                          filetype = "MOV";
                        } else {
                          contentType = "video/*";
                          filetype = "mp4";
                        }
                      } // alert('line525 ---' + base64String)


                      FormArray = this.imageUploadForm.get('level4Val');
                      FormArray.value.forEach(function (element, i) {
                        delete element.categList;
                      }); // alert('line539 ---' + localStorage.getItem('videoThumbnailPosterImage'))
                      // alert('line540 ---' + localStorage.getItem('videoThumbnailPosterImage') ? JSON.parse(localStorage.getItem('videoThumbnailPosterImage').replace('data:image/jpeg;base64,', '')) : '')
                      // alert('line539 ---Ready')

                      obj = {
                        'image_video': this.type == 'image' ? "IMAGE" : "VIDEO",
                        'city': city,
                        'location': loc,
                        'latitude_longitude': '',
                        'stud_type': stud_type,
                        'username': username,
                        // 'img_categ': '',
                        'file_ext': filetype,
                        'contentType': contentType,
                        'base64String': base64String,
                        'fileinfo': this.fileInfo,
                        'base64StringThump': localStorage.getItem('videoThumbnailPosterImage') ? JSON.parse(localStorage.getItem('videoThumbnailPosterImage').replace('data:image/jpeg;base64,', '')) : ''
                      };
                      this.imageUploadForm.value.level4Val = JSON.stringify(this.imageUploadForm.value.level4Val);
                      latitude_longitude.push(this.latitude);
                      latitude_longitude.push(this.longitude); // let imgCategArrayOne = [];
                      // this.selectedArray.forEach(function (value) {
                      //   imgCategArrayOne.push(value.categ_name);
                      // });
                      // this.imgCategArray = imgCategArrayOne;
                      // obj.img_categ = this.imgCategArray;

                      obj.latitude_longitude = latitude_longitude;
                      this.service.post_data("uploadFileBase64", Object.assign(obj, this.imageUploadForm.value)).subscribe(function (result) {
                        localStorage.removeItem('originalImg');
                        localStorage.removeItem('editedFilters');
                        localStorage.removeItem('videoFileSize');
                        localStorage.removeItem('fileSize');
                        localStorage.removeItem('videoThumbnailPosterImage');

                        var json = _this13.service.parserToJSON(result);

                        _this13.UploadFileList = JSON.parse(json.string);

                        _this13.submitAlert(_this13.UploadFileList[0].Message);
                      }, function (error) {
                        console.log(error);
                        alert(error);
                      }); // }

                      _context3.next = 34;
                      break;

                    case 27:
                      _context3.next = 29;
                      return this.alertController.create({
                        cssClass: 'alert-custom-class-info',
                        header: 'Info',
                        message: 'Please fill all mandatory fields',
                        backdropDismiss: false,
                        buttons: [{
                          text: 'OK',
                          cssClass: 'warning'
                        }]
                      });

                    case 29:
                      _alert = _context3.sent;
                      _context3.next = 32;
                      return _alert.present();

                    case 32:
                      this.validateAllFormFields(this.imageUploadForm);
                      this.checkBoxValidation();

                    case 34:
                    case "end":
                      return _context3.stop();
                  }
                }
              }, _callee3, this);
            }));
          }
        }, {
          key: "submitAlert",
          value: function submitAlert(message_content) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee4() {
              var _this14 = this;

              var alert;
              return regeneratorRuntime.wrap(function _callee4$(_context4) {
                while (1) {
                  switch (_context4.prev = _context4.next) {
                    case 0:
                      this.cache.clearAll(); // if (this.selectedArray.length == 0) {
                      //   const alert = await this.alertController.create({
                      //     cssClass: 'alert-custom-class',
                      //     header: 'Error',
                      //     message: 'Please Select the Any Category',
                      //     backdropDismiss: false,
                      //     buttons: [
                      //       {
                      //         text: 'OK',
                      //         cssClass: 'warning',
                      //       }
                      //     ]
                      //   });
                      //   await alert.present();
                      // } else {

                      _context4.next = 3;
                      return this.alertController.create({
                        cssClass: 'alert-custom-class',
                        message: message_content,
                        backdropDismiss: false,
                        buttons: [{
                          text: 'OK',
                          cssClass: 'warning',
                          handler: function handler() {
                            _this14.navCtrl.navigateForward(['home']);
                          }
                        }]
                      });

                    case 3:
                      alert = _context4.sent;
                      _context4.next = 6;
                      return alert.present();

                    case 6:
                    case "end":
                      return _context4.stop();
                  }
                }
              }, _callee4, this);
            }));
          }
        }, {
          key: "openMenu",
          value: function openMenu() {
            this.menu.open();
          }
        }, {
          key: "goBack",
          value: function goBack() {
            // if (this.page == 'home') {
            //   this.router.navigate(['/home', { 'type': this.type, 'page': "home" }]);
            // } else {
            // this.router.navigate(['/preview-filter', { 'type': this.type, 'page': "preview" }], );
            this.location.back(); // }
          }
        }, {
          key: "openViewer",
          value: function openViewer() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee5() {
              var modal;
              return regeneratorRuntime.wrap(function _callee5$(_context5) {
                while (1) {
                  switch (_context5.prev = _context5.next) {
                    case 0:
                      _context5.next = 2;
                      return this.modalController.create({
                        component: ngx_ionic_image_viewer__WEBPACK_IMPORTED_MODULE_7__["ViewerModalComponent"],
                        componentProps: {
                          src: this.webview.convertFileSrc(this.mediaPath),
                          title: 'Photo Viewer'
                        },
                        cssClass: 'ion-img-viewer',
                        keyboardClose: true,
                        showBackdrop: true
                      });

                    case 2:
                      modal = _context5.sent;
                      _context5.next = 5;
                      return modal.present();

                    case 5:
                      return _context5.abrupt("return", _context5.sent);

                    case 6:
                    case "end":
                      return _context5.stop();
                  }
                }
              }, _callee5, this);
            }));
          } // =====================End of Category list=====================

        }, {
          key: "convertFileUritoBase64",
          value: function convertFileUritoBase64(ImagePath) {
            var _this15 = this;

            var filePath;
            var removePath = String(ImagePath).replace(' (see http://g.co/ng/security#xss)', '');
            var splitPath = removePath.split('/');
            var imageName = splitPath[splitPath.length - 1];

            if (this.platform.is('ios') && ImagePath.indexOf("SafeValue") !== -1) {
              filePath = String(ImagePath.split(imageName)[0]).split(' ')[4]; // alert('IOS----' + filePath);
            } else {
              filePath = ImagePath.split(imageName)[0];
            } // alert('line 686' + imageName + '-----' + filePath);


            this.file.readAsDataURL(filePath, imageName).then(function (base64) {
              // alert('line 689' + base64);
              if (_this15.platform.is('ios')) {
                _this15.mediaPath = _this15.service.checkDomSanitizerForIOS(base64);
              } else {
                _this15.mediaPath = _this15.service.checkDomSanitizerForIOS(_this15.webview.convertFileSrc(base64));
              }
            }, function (error) {
              console.log("CROP ERROR -> " + JSON.stringify(error));
              alert(error);
            })["catch"](function (e) {
              alert(e);
            });
          }
        }, {
          key: "ionViewWillLeave",
          value: function ionViewWillLeave() {
            if (this.type != 'video') {
              this.service.changeMessage(this.mediaPath);
            }
          }
        }]);

        return CategorySelectionPage;
      }();

      CategorySelectionPage.ctorParameters = function () {
        return [{
          type: _ionic_native_File_ngx__WEBPACK_IMPORTED_MODULE_8__["File"]
        }, {
          type: _global_services_service__WEBPACK_IMPORTED_MODULE_2__["GlobalServicesService"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["MenuController"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavController"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["PopoverController"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["AlertController"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"]
        }, {
          type: _ionic_native_ionic_webview_ngx__WEBPACK_IMPORTED_MODULE_5__["WebView"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ModalController"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ToastController"]
        }, {
          type: ionic_cache__WEBPACK_IMPORTED_MODULE_9__["CacheService"]
        }, {
          type: _angular_common__WEBPACK_IMPORTED_MODULE_10__["Location"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["Platform"]
        }, {
          type: _angular_forms__WEBPACK_IMPORTED_MODULE_11__["FormBuilder"]
        }];
      };

      CategorySelectionPage.propDecorators = {
        myVideo: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"],
          args: ['videoElement', {
            "static": false
          }]
        }]
      };
      CategorySelectionPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-category-selection',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./category-selection.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/category-selection/category-selection.page.html"))["default"],
        encapsulation: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewEncapsulation"].None,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./category-selection.page.scss */
        "./src/app/category-selection/category-selection.page.scss"))["default"]]
      })], CategorySelectionPage);
      /***/
    }
  }]);
})();
//# sourceMappingURL=category-selection-category-selection-module-es5.js.map