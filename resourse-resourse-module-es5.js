(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["resourse-resourse-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/header/header.page.html":
    /*!*******************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/header/header.page.html ***!
      \*******************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppHeaderHeaderPageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-header translucent no-border>\r\n    <ion-toolbar>\r\n        <ion-item header-menu lines=\"none\">\r\n            <ion-avatar slot=\"start\">\r\n                <img [src]=\"loggedDetails.upload_image\" height=\"100%\" width=\"100%\" *ngIf=\"loggedDetails.upload_image\">\r\n                <img src=\"../../assets//image/user-login-icon-1.png\" height=\"100%\" width=\"100%\"\r\n                    *ngIf=\"!loggedDetails.upload_image\">\r\n            </ion-avatar>\r\n            <ion-label>\r\n                <h3 font-header class=\"ion-text-capitalize\">{{loggedDetails.stud_name}}</h3>\r\n                <p font-medium>{{city}}</p>\r\n            </ion-label>\r\n            <div class=\"notification-bell-container\" slot=\"end\">\r\n                <p class=\"text-highlight\" (click)=\"moveNotificationPage()\"\r\n                    *ngIf=\"notificationUnreadCount && notificationUnreadCount!=0\">\r\n                    {{notificationUnreadCount > 9 ? '9+': notificationUnreadCount}}</p>\r\n                <ion-icon name=\"notifications-outline\" (click)=\"moveNotificationPage()\" class=\"bell-icon\">\r\n                </ion-icon>\r\n            </div>\r\n            <ion-icon name=\"reorder-four-outline\" (click)=\"openMenu()\" slot=\"end\"></ion-icon>\r\n        </ion-item>\r\n    </ion-toolbar>\r\n</ion-header>";
      /***/
    },

    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/resourse/resourse.page.html":
    /*!***********************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/resourse/resourse.page.html ***!
      \***********************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppResourseResoursePageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "\r\n<app-header>\r\n\r\n</app-header>\r\n<ion-content>\r\n    <div class=\"inner-div\">\r\n        <ion-grid class=\"pt-3\">\r\n            <ion-row class=\"ion-text-center mt-3 mb-2\">\r\n                <ion-col>\r\n                    <h5 header-text>Resources</h5>\r\n                </ion-col>\r\n            </ion-row>\r\n            <ion-row>\r\n                <ion-col>\r\n                    <ion-item lines=\"none\" [routerLink]=\"['/resourses-videos']\">\r\n                        <ion-icon margin-1 class=\"icon mx-auto\" color=\"yellow\" name=\"albums-outline\">\r\n                        </ion-icon>\r\n                        <!-- <ion-label>Images</ion-label> -->\r\n                    </ion-item>\r\n                </ion-col>\r\n                <ion-col>\r\n                    <ion-item lines=\"none\" class=\"align-content-center\" [routerLink]=\"['/resourses-videos']\">\r\n                        <ion-icon margin-1 name=\"albums-outline\" class=\"icon mx-auto\"></ion-icon>\r\n                        <!-- <span class=\"text\">Videos</span> -->\r\n                    </ion-item>\r\n                </ion-col>\r\n            </ion-row>\r\n            <ion-row>\r\n                <ion-col>\r\n                    <ion-item lines=\"none\" [routerLink]=\"['/resourses-videos']\">\r\n                        <ion-icon margin-1 class=\"icon mx-auto\" color=\"yellow\" name=\"albums-outline\">\r\n                        </ion-icon>\r\n                        <!-- <ion-label>Images</ion-label> -->\r\n                    </ion-item>\r\n                </ion-col>\r\n                <ion-col>\r\n                    <ion-item lines=\"none\" class=\"align-content-center\" [routerLink]=\"['/resourses-videos']\">\r\n                        <ion-icon margin-1 name=\"albums-outline\" class=\"icon mx-auto\"></ion-icon>\r\n                        <!-- <span class=\"text\">Videos</span> -->\r\n                    </ion-item>\r\n                </ion-col>\r\n            </ion-row>\r\n            <ion-row>\r\n                <ion-col>\r\n                    <ion-item lines=\"none\" [routerLink]=\"['/resourses-videos']\">\r\n                        <ion-icon margin-1 class=\"icon mx-auto\" color=\"yellow\" name=\"albums-outline\">\r\n                        </ion-icon>\r\n                        <!-- <ion-label>Images</ion-label> -->\r\n                    </ion-item>\r\n                </ion-col>\r\n                <ion-col>\r\n                    <ion-item lines=\"none\" class=\"align-content-center\" [routerLink]=\"['/resourses-videos']\">\r\n                        <ion-icon margin-1 name=\"albums-outline\" class=\"icon mx-auto\"></ion-icon>\r\n                        <!-- <span class=\"text\">Videos</span> -->\r\n                    </ion-item>\r\n                </ion-col>\r\n            </ion-row>\r\n        </ion-grid>\r\n    </div>\r\n</ion-content>\r\n<ion-toolbar color=\"yellow\" slot=\"bottom\">\r\n    <ion-tabs>\r\n        <ion-tab-bar color=\"yellow\" slot=\"bottom\">\r\n            <ion-tab-button tab=\"share\">\r\n                <ion-icon name=\"caret-back-outline\"></ion-icon>\r\n                <ion-label>Back</ion-label>\r\n            </ion-tab-button>\r\n            \r\n            <ion-tab-button tab=\"camera\">\r\n                <ion-icon name=\"bookmark\"></ion-icon>\r\n                <ion-label>Bookmark</ion-label>\r\n            </ion-tab-button>\r\n\r\n            <ion-tab-button tab=\"search\">\r\n                <ion-icon name=\"search-outline\"></ion-icon>\r\n                <ion-label>Search</ion-label>\r\n            </ion-tab-button>\r\n\r\n         \r\n        </ion-tab-bar>\r\n    </ion-tabs>\r\n</ion-toolbar>";
      /***/
    },

    /***/
    "./src/app/header/header-routing.module.ts":
    /*!*************************************************!*\
      !*** ./src/app/header/header-routing.module.ts ***!
      \*************************************************/

    /*! exports provided: HeaderPageRoutingModule */

    /***/
    function srcAppHeaderHeaderRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "HeaderPageRoutingModule", function () {
        return HeaderPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _header_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./header.page */
      "./src/app/header/header.page.ts");

      var routes = [{
        path: '',
        component: _header_page__WEBPACK_IMPORTED_MODULE_3__["HeaderPage"]
      }];

      var HeaderPageRoutingModule = function HeaderPageRoutingModule() {
        _classCallCheck(this, HeaderPageRoutingModule);
      };

      HeaderPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], HeaderPageRoutingModule);
      /***/
    },

    /***/
    "./src/app/header/header.module.ts":
    /*!*****************************************!*\
      !*** ./src/app/header/header.module.ts ***!
      \*****************************************/

    /*! exports provided: HeaderPageModule */

    /***/
    function srcAppHeaderHeaderModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "HeaderPageModule", function () {
        return HeaderPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _header_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./header-routing.module */
      "./src/app/header/header-routing.module.ts");
      /* harmony import */


      var _header_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./header.page */
      "./src/app/header/header.page.ts");

      var HeaderPageModule = function HeaderPageModule() {
        _classCallCheck(this, HeaderPageModule);
      };

      HeaderPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _header_routing_module__WEBPACK_IMPORTED_MODULE_5__["HeaderPageRoutingModule"]],
        declarations: [_header_page__WEBPACK_IMPORTED_MODULE_6__["HeaderPage"]],
        exports: [_header_page__WEBPACK_IMPORTED_MODULE_6__["HeaderPage"]]
      })], HeaderPageModule);
      /***/
    },

    /***/
    "./src/app/header/header.page.scss":
    /*!*****************************************!*\
      !*** ./src/app/header/header.page.scss ***!
      \*****************************************/

    /*! exports provided: default */

    /***/
    function srcAppHeaderHeaderPageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = ".notification-bell-container {\n  position: relative;\n  display: flex;\n  flex-direction: row;\n  align-items: center;\n  justify-content: center;\n}\n.notification-bell-container .text-highlight {\n  position: absolute;\n  right: -3px;\n  font-size: 10px;\n  cursor: pointer;\n  top: -1px;\n  background: red;\n  border-radius: 50%;\n  z-index: 1111;\n  width: 18px;\n  height: 18px;\n  text-align: center;\n  line-height: 19px;\n  color: #ffffff;\n  font-weight: bold;\n}\n.notification-bell-container .bell-icon {\n  font-size: 30px;\n  position: relative;\n}\n.notification-bell-container .bell-icon:after {\n  content: \"\";\n  position: absolute;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvaGVhZGVyL2hlYWRlci5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxrQkFBQTtFQUNBLGFBQUE7RUFDQSxtQkFBQTtFQUNBLG1CQUFBO0VBQ0EsdUJBQUE7QUFDSjtBQUFJO0VBQ0Usa0JBQUE7RUFDQSxXQUFBO0VBQ0EsZUFBQTtFQUNBLGVBQUE7RUFDQSxTQUFBO0VBQ0EsZUFBQTtFQUNBLGtCQUFBO0VBQ0EsYUFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBQ0Esa0JBQUE7RUFDQSxpQkFBQTtFQUNBLGNBQUE7RUFDQSxpQkFBQTtBQUVOO0FBQUk7RUFDSSxlQUFBO0VBQ0Esa0JBQUE7QUFFUjtBQURRO0VBQ0UsV0FBQTtFQUNBLGtCQUFBO0FBR1YiLCJmaWxlIjoic3JjL2FwcC9oZWFkZXIvaGVhZGVyLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5ub3RpZmljYXRpb24tYmVsbC1jb250YWluZXJ7XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgZmxleC1kaXJlY3Rpb246IHJvdztcclxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgIC50ZXh0LWhpZ2hsaWdodHtcclxuICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICByaWdodDogLTNweDtcclxuICAgICAgZm9udC1zaXplOiAxMHB4O1xyXG4gICAgICBjdXJzb3I6IHBvaW50ZXI7XHJcbiAgICAgIHRvcDogLTFweDtcclxuICAgICAgYmFja2dyb3VuZDogcmVkO1xyXG4gICAgICBib3JkZXItcmFkaXVzOiA1MCU7XHJcbiAgICAgIHotaW5kZXg6IDExMTE7XHJcbiAgICAgIHdpZHRoOiAxOHB4O1xyXG4gICAgICBoZWlnaHQ6IDE4cHg7XHJcbiAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICAgbGluZS1oZWlnaHQ6IDE5cHg7XHJcbiAgICAgIGNvbG9yOiAjZmZmZmZmO1xyXG4gICAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgIH1cclxuICAgIC5iZWxsLWljb257XHJcbiAgICAgICAgZm9udC1zaXplOiAzMHB4O1xyXG4gICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgICAgICAmOmFmdGVye1xyXG4gICAgICAgICAgY29udGVudDogJyc7XHJcbiAgICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgXHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgfSJdfQ== */";
      /***/
    },

    /***/
    "./src/app/header/header.page.ts":
    /*!***************************************!*\
      !*** ./src/app/header/header.page.ts ***!
      \***************************************/

    /*! exports provided: HeaderPage */

    /***/
    function srcAppHeaderHeaderPageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "HeaderPage", function () {
        return HeaderPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _global_services_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ../global-services.service */
      "./src/app/global-services.service.ts");
      /* harmony import */


      var _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic-native/geolocation/ngx */
      "./node_modules/@ionic-native/geolocation/__ivy_ngcc__/ngx/index.js");
      /* harmony import */


      var _ionic_native_native_geocoder_ngx__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @ionic-native/native-geocoder/ngx */
      "./node_modules/@ionic-native/native-geocoder/__ivy_ngcc__/ngx/index.js");

      var HeaderPage = /*#__PURE__*/function () {
        function HeaderPage(menu, service, geolocation, nativeGeocoder, navController) {
          var _this = this;

          _classCallCheck(this, HeaderPage);

          this.menu = menu;
          this.service = service;
          this.geolocation = geolocation;
          this.nativeGeocoder = nativeGeocoder;
          this.navController = navController;
          this.notificationUrl = 'getNotificationText'; //=====================get Login Details=====================

          this.loggedDetails = {};
          this.city = localStorage.getItem('location'); // geocoder options

          this.nativeGeocoderOptions = {
            useLocale: true,
            maxResults: 5
          };
          this.userName = localStorage.getItem('username');
          this.subscription = this.service.getMessage().subscribe(function (message) {
            if (message.text == 'startNotificationCountFunction') {
              _this.getnotificationsList();
            }
          });
          this.service.invoke.subscribe(function (res) {
            if (res) {
              _this.cityChange(res);
            }
          });
        }

        _createClass(HeaderPage, [{
          key: "cityChange",
          value: function cityChange(city) {
            if (!this.city) {
              this.city = city;
              localStorage.setItem('location', this.city);
            }
          }
        }, {
          key: "ngOnInit",
          value: function ngOnInit() {
            this.userInfo(); // this.getUserPosition();

            this.service.checkGPSPermission();
          }
        }, {
          key: "openMenu",
          value: function openMenu() {
            // this.menu.enable(true, 'first');
            this.menu.open();
          }
        }, {
          key: "userInfo",
          value: function userInfo() {
            var _this2 = this;

            if (localStorage.getItem('username')) {
              var obj = {
                username: localStorage.getItem('username')
              };
              this.service.post_data("getAlumniOthersInfo", obj).subscribe(function (result) {
                var json = _this2.service.parserToJSON(result);

                var parsedJSON = JSON.parse(json.string);
                _this2.loggedDetails = parsedJSON[0];
              }, function (error) {
                console.log(error);
              });
            }
          }
        }, {
          key: "getUserPosition",
          value: function getUserPosition() {
            var _this3 = this;

            this.options = {
              enableHighAccuracy: false
            };
            this.geolocation.getCurrentPosition(this.options).then(function (pos) {
              _this3.currentPos = pos;
              _this3.latitude = pos.coords.latitude;
              _this3.longitude = pos.coords.longitude;

              _this3.getAddress(_this3.latitude, _this3.longitude);
            }, function (err) {
              console.log("error : " + err.message);
              ;
            });
          } // get address using coordinates

        }, {
          key: "getAddress",
          value: function getAddress(lat, _long) {
            var _this4 = this;

            this.nativeGeocoder.reverseGeocode(lat, _long, this.nativeGeocoderOptions).then(function (res) {
              var address = _this4.pretifyAddress(res[0]);

              var value = address.split(",");
              var count = value.length;
              _this4.city = value[count - 5];
            })["catch"](function (error) {
              alert('Error getting location' + error + JSON.stringify(error));
            });
          } // address

        }, {
          key: "pretifyAddress",
          value: function pretifyAddress(addressObj) {
            var obj = [];
            var address = "";

            for (var key in addressObj) {
              obj.push(addressObj[key]);
            }

            obj.reverse();

            for (var val in obj) {
              if (obj[val].length) address += obj[val] + ', ';
            }

            return address.slice(0, -2);
          }
        }, {
          key: "getnotificationsList",
          value: function getnotificationsList() {
            var _this5 = this;

            var obj = {
              msg_id: 0,
              username: this.userName,
              rows_in_page: 0,
              pageid: 1
            };
            this.service.post_data(this.notificationUrl, obj).subscribe(function (result) {
              var json = _this5.service.parserToJSON(result);

              var list = JSON.parse(json.string);

              if (list) {
                _this5.notificationUnreadCount = 0;

                if (list.Details.length > 0) {
                  list.Details.forEach(function (element, i) {
                    if (!JSON.parse(String(element.read_sts).toLowerCase())) {
                      _this5.notificationUnreadCount++;
                    }
                  });
                }
              }
            });
          }
        }, {
          key: "moveNotificationPage",
          value: function moveNotificationPage() {
            this.navController.navigateForward('/notification');
          }
        }]);

        return HeaderPage;
      }();

      HeaderPage.ctorParameters = function () {
        return [{
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["MenuController"]
        }, {
          type: _global_services_service__WEBPACK_IMPORTED_MODULE_3__["GlobalServicesService"]
        }, {
          type: _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_4__["Geolocation"]
        }, {
          type: _ionic_native_native_geocoder_ngx__WEBPACK_IMPORTED_MODULE_5__["NativeGeocoder"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"]
        }];
      };

      HeaderPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-header',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./header.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/header/header.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./header.page.scss */
        "./src/app/header/header.page.scss"))["default"]]
      })], HeaderPage);
      /***/
    },

    /***/
    "./src/app/resourse/resourse-routing.module.ts":
    /*!*****************************************************!*\
      !*** ./src/app/resourse/resourse-routing.module.ts ***!
      \*****************************************************/

    /*! exports provided: ResoursePageRoutingModule */

    /***/
    function srcAppResourseResourseRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ResoursePageRoutingModule", function () {
        return ResoursePageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _resourse_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./resourse.page */
      "./src/app/resourse/resourse.page.ts");

      var routes = [{
        path: '',
        component: _resourse_page__WEBPACK_IMPORTED_MODULE_3__["ResoursePage"]
      }];

      var ResoursePageRoutingModule = function ResoursePageRoutingModule() {
        _classCallCheck(this, ResoursePageRoutingModule);
      };

      ResoursePageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], ResoursePageRoutingModule);
      /***/
    },

    /***/
    "./src/app/resourse/resourse.module.ts":
    /*!*********************************************!*\
      !*** ./src/app/resourse/resourse.module.ts ***!
      \*********************************************/

    /*! exports provided: ResoursePageModule */

    /***/
    function srcAppResourseResourseModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ResoursePageModule", function () {
        return ResoursePageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _resourse_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./resourse-routing.module */
      "./src/app/resourse/resourse-routing.module.ts");
      /* harmony import */


      var _resourse_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./resourse.page */
      "./src/app/resourse/resourse.page.ts");
      /* harmony import */


      var _header_header_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! ../header/header.module */
      "./src/app/header/header.module.ts");

      var ResoursePageModule = function ResoursePageModule() {
        _classCallCheck(this, ResoursePageModule);
      };

      ResoursePageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _resourse_routing_module__WEBPACK_IMPORTED_MODULE_5__["ResoursePageRoutingModule"], _header_header_module__WEBPACK_IMPORTED_MODULE_7__["HeaderPageModule"]],
        declarations: [_resourse_page__WEBPACK_IMPORTED_MODULE_6__["ResoursePage"]]
      })], ResoursePageModule);
      /***/
    },

    /***/
    "./src/app/resourse/resourse.page.scss":
    /*!*********************************************!*\
      !*** ./src/app/resourse/resourse.page.scss ***!
      \*********************************************/

    /*! exports provided: default */

    /***/
    function srcAppResourseResoursePageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "[font-small] {\n  font-weight: bold !important;\n  --border-radius: 0px;\n}\n\n[margin-1] {\n  margin: 2.25rem;\n}\n\n.icon {\n  display: flex;\n  font-size: 70px;\n  color: #ffC977;\n  vertical-align: middle;\n}\n\n[ion-col-height] {\n  width: 100%;\n  height: 150px;\n}\n\n[ion-col-height] ion-content {\n  --background: var(--ion-color-lightyellow);\n}\n\n.img_size {\n  width: 100%;\n  -o-object-fit: cover;\n     object-fit: cover;\n  -o-object-position: center;\n     object-position: center;\n  background-color: white;\n  height: 150px;\n}\n\n[over] {\n  z-index: 1;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcmVzb3Vyc2UvcmVzb3Vyc2UucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBRUksNEJBQUE7RUFDQSxvQkFBQTtBQUFKOztBQUdBO0VBQ0ksZUFBQTtBQUFKOztBQUdBO0VBQ0ksYUFBQTtFQUNBLGVBQUE7RUFDQSxjQUFBO0VBQ0Esc0JBQUE7QUFBSjs7QUFHQTtFQUNJLFdBQUE7RUFDQSxhQUFBO0FBQUo7O0FBRUk7RUFDSSwwQ0FBQTtBQUFSOztBQUlBO0VBQ0ksV0FBQTtFQUNBLG9CQUFBO0tBQUEsaUJBQUE7RUFDQSwwQkFBQTtLQUFBLHVCQUFBO0VBQ0EsdUJBQUE7RUFDQSxhQUFBO0FBREo7O0FBSUE7RUFDSSxVQUFBO0FBREoiLCJmaWxlIjoic3JjL2FwcC9yZXNvdXJzZS9yZXNvdXJzZS5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJbZm9udC1zbWFsbF0ge1xyXG4gICAgLy8gZm9udC1zaXplOiAxMnB4IWltcG9ydGFudDtcclxuICAgIGZvbnQtd2VpZ2h0OiBib2xkICFpbXBvcnRhbnQ7XHJcbiAgICAtLWJvcmRlci1yYWRpdXM6IDBweDtcclxufVxyXG5cclxuW21hcmdpbi0xXSB7XHJcbiAgICBtYXJnaW46IDIuMjVyZW07XHJcbn1cclxuXHJcbi5pY29uIHtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBmb250LXNpemU6IDcwcHg7XHJcbiAgICBjb2xvcjogI2ZmQzk3NztcclxuICAgIHZlcnRpY2FsLWFsaWduOiBtaWRkbGU7XHJcbn1cclxuXHJcbltpb24tY29sLWhlaWdodF0ge1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBoZWlnaHQ6IDE1MHB4O1xyXG4gICAgLy8gYmFja2dyb3VuZDogI2ZmZmZmZjtcclxuICAgIGlvbi1jb250ZW50IHtcclxuICAgICAgICAtLWJhY2tncm91bmQ6IHZhcigtLWlvbi1jb2xvci1saWdodHllbGxvdyk7XHJcbiAgICB9XHJcbn1cclxuXHJcbi5pbWdfc2l6ZSB7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIG9iamVjdC1maXQ6IGNvdmVyO1xyXG4gICAgb2JqZWN0LXBvc2l0aW9uOiBjZW50ZXI7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiBsaWdodGVuKHdoaXRlLCA3MCUpO1xyXG4gICAgaGVpZ2h0OiAxNTBweDtcclxufVxyXG5cclxuW292ZXJdIHtcclxuICAgIHotaW5kZXg6IDE7XHJcbn0iXX0= */";
      /***/
    },

    /***/
    "./src/app/resourse/resourse.page.ts":
    /*!*******************************************!*\
      !*** ./src/app/resourse/resourse.page.ts ***!
      \*******************************************/

    /*! exports provided: ResoursePage */

    /***/
    function srcAppResourseResoursePageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ResoursePage", function () {
        return ResoursePage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _global_services_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ../global-services.service */
      "./src/app/global-services.service.ts");

      var ResoursePage = /*#__PURE__*/function () {
        function ResoursePage(menu, service) {
          _classCallCheck(this, ResoursePage);

          this.menu = menu;
          this.service = service;
        }

        _createClass(ResoursePage, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }, {
          key: "openMenu",
          value: function openMenu() {
            // this.menu.enable(true, 'first');
            this.menu.open();
          }
        }]);

        return ResoursePage;
      }();

      ResoursePage.ctorParameters = function () {
        return [{
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["MenuController"]
        }, {
          type: _global_services_service__WEBPACK_IMPORTED_MODULE_3__["GlobalServicesService"]
        }];
      };

      ResoursePage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-resourse',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./resourse.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/resourse/resourse.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./resourse.page.scss */
        "./src/app/resourse/resourse.page.scss"))["default"]]
      })], ResoursePage); ///////////
      // import { Component, OnInit, ElementRef, ViewChild, AfterViewInit } from '@angular/core';
      // import { GlobalServicesService } from '../global-services.service';
      // import { ActionSheetController, MenuController, AlertController, NavController, PopoverController } from '@ionic/angular';
      // import { Router, ActivatedRoute, ParamMap } from '@angular/router';
      // import { Crop, CropOptions } from '@ionic-native/crop/ngx';
      // import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
      // import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
      // import { File, FileEntry } from '@ionic-native/File/ngx';
      // import { Platform } from '@ionic/angular';
      // import { WebView } from '@ionic-native/ionic-webview/ngx';
      // import { Geolocation, GeolocationOptions, Geoposition, PositionError } from '@ionic-native/geolocation/ngx';
      // import { NativeGeocoder, NativeGeocoderResult, NativeGeocoderOptions } from '@ionic-native/native-geocoder/ngx';
      // @Component({
      //   selector: 'app-preview-filter',
      //   templateUrl: './preview-filter.page.html',
      //   styleUrls: ['./preview-filter.page.scss'],
      // })
      // export class PreviewFilterPage implements OnInit {
      //   @ViewChild('videoElement', { static: false }) myVideo: ElementRef;
      //   @ViewChild('imageSrc') imageSrc: ElementRef;
      //   location: any;
      //   tempPath: string;
      //   page: string;
      //   originalImage: any = "../../assets/image/4.jpg";
      //   enableEditMode: boolean = false;
      //   croppedImagepath = "";
      //   isLoading = false;
      //   constructor(private file: File, private crop: Crop, public actionSheetController: ActionSheetController, public alertController: AlertController, public service: GlobalServicesService, private menu: MenuController, public navCtrl: NavController, private route: ActivatedRoute,
      //     private router: Router, private androidPermissions: AndroidPermissions, private webview: WebView, private geolocation: Geolocation, private nativeGeocoder: NativeGeocoder) {
      //     this.getUserPosition();
      //   }
      //   ngOnInit() {
      //     // =====================Check app permissions===================== 
      //     this.checkPermissions();
      //     // =====================Get params value===================== 
      //     this.route.params.subscribe((params) => {
      //       this.location = params['location'];
      //       this.type = params['type'];
      //       this.tempPath = params['tempPath'];
      //       this.page = params['page'];
      //       this.originalImage = this.tempPath;
      //     });
      //     if (this.type == 'video') {
      //       alert('VideoPlaybackQuality' + this.tempPath);
      //       this.openFile(this.tempPath);
      //     } else {
      //       this.originalImage = this.webview.convertFileSrc(this.originalImage); // convert to weburl
      //     }
      //     // if (this.originalImage == undefined) {
      //     //   this.service.get('imageUrlPath').then(result => {
      //     //     if (result != null) {
      //     //       this.tempPath = result;
      //     //       if (this.originalImage && this.type == 'video') {
      //     //         this.openFile(result);
      //     //       } else {
      //     //         this.originalImage = this.webview.convertFileSrc(result);
      //     //       }
      //     //     }
      //     //   }).catch(e => {
      //     //     console.log('error: ' + e);
      //     //   });
      //     // }
      //   }
      //   ngAfterViewInit() {
      //     if (this.type == 'video') {
      //       let video = this.myVideo.nativeElement;
      //       video.src = this.webview.convertFileSrc(this.originalImage);
      //       // video.src = "../../assets/videos/sample.mp4";
      //       video.play();
      //     } else {
      //       this.originalImage = this.webview.convertFileSrc(this.originalImage);
      //     }
      //     // =====================Get params value=====================  
      //     // if (this.originalImage == undefined) {
      //     //   this.service.get('imageUrlPath').then(result => {
      //     //     if (result != null) {
      //     //       this.tempPath = result;
      //     //       if (this.originalImage && this.type == 'video') {
      //     //         this.openFile(result);
      //     //       } else {
      //     //         this.originalImage = this.webview.convertFileSrc(result);
      //     //       }
      //     //     }
      //     //   }).catch(e => {
      //     //     console.log('error: ' + e);
      //     //   });
      //     // } else {
      //     //   if (this.originalImage && this.type == 'video') {
      //     //     this.openFile(this.originalImage);
      //     //   } else {
      //     //     this.originalImage = this.webview.convertFileSrc(this.originalImage); // convert to weburl
      //     //   }
      //     // }
      //   }
      //   // =====================Open the video file===================== 
      //   openFile(file) {
      //     console.log(this.myVideo, document.getElementById('videoElement'));
      //     let video = this.myVideo.nativeElement;
      //     video.src = this.webview.convertFileSrc(file);
      //     video.play();
      //   }
      //   // =====================Check app permissions===================== 
      //   checkPermissions() {
      //     this.androidPermissions.requestPermissions(
      //       [
      //         this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE,
      //         this.androidPermissions.PERMISSION.WRITE_EXTERNAL_STORAGE
      //       ]
      //     );
      //   }
      //   // =====================Edit the Image===================== 
      //   // When Sliders Values Change, Update the CSS Variables
      //   property;
      //   type;
      //   filter: string = '';
      //   transform: string = '';
      //   rotate = "";
      //   rotateX = "";
      //   rotateY = "";
      //   brightness = "";
      //   contrast = "";
      //   saturate = "";
      //   hue_rotate = "";
      //   cntProperty: string = "";
      //   cntValue: string = "0";
      //   cntType: string = "";
      //   cropOptions: CropOptions = {
      //     quality: 50, targetHeight: 1280, targetWidth: 720
      //   }
      //   // Add filters to the image
      //   setFilter(property, value, unit, type) {
      //     // console.log(property, value,unit);
      //     this.cntProperty = property;
      //     this.cntValue = value;
      //     this.cntType = type;
      //     console.log(this.cntValue);
      //     var img = document.getElementById("image") as HTMLImageElement;
      //     if (property == 'rotateY') {
      //       this.rotateY = (property + "(" + value + unit + ")");
      //     }
      //     if (property == 'rotateX') {
      //       this.rotateX = (property + "(" + value + unit + ")");
      //     }
      //     if (property == 'rotate') {
      //       this.rotate = (property + "(" + value + unit + ")");
      //       // this.DrawImage(this.cntType, this.cntProperty, this.cntValue);
      //     }
      //     if (property == 'brightness') {
      //       this.brightness = (property + "(" + value + unit + ")");
      //       console.log(this.brightness);
      //     }
      //     if (property == 'contrast') {
      //       this.contrast = (property + "(" + value + unit + ")");
      //     }
      //     if (property == 'saturate') {
      //       this.saturate = (property + "(" + value + unit + ")");
      //     }
      //     if (property == 'hue-rotate') {
      //       this.hue_rotate = (property + "(" + value + unit + ")");
      //     }
      //     console.log(this.filter, this.type);
      //     if (type == 'transform') {
      //       this.transform = this.rotateY + ' ' + this.rotateX + ' ' + this.rotate;
      //       console.log(this.transform);
      //       // img.style.transform = this.transform;  //(property + "(" + value + unit + ")");
      //       this.DrawImage(this.cntType, this.cntProperty, this.cntValue);
      //     }
      //     if (type == 'filter') {
      //       // console.log(this.filter, this.transform);
      //       this.filter = this.hue_rotate + ' ' + this.saturate + ' ' + this.brightness + ' ' + this.contrast;
      //       img.style.filter = this.filter;
      //       // context.filter = this.filter;
      //     }
      //   }
      //   // Save the changes and convert to the image
      //   saveChanges() {
      //     this.property = null;
      //     if (this.cntType == 'filter')
      //       this.DrawImage(this.cntType, this.cntProperty, this.cntValue);
      //   }
      //   // Convert to the image
      //   canvasImage;
      //   DrawImage(type, property, value) {
      //     console.log(type, property, value)
      //     var img = document.getElementById("image") as HTMLImageElement;
      //     var data, ofilter = this.filter;
      //     var canvas = document.createElement('canvas') as HTMLCanvasElement;
      //     var context = canvas.getContext('2d');
      //     if (type == 'filter') {
      //       canvas.width = img.offsetWidth;
      //       canvas.height = img.offsetHeight;
      //       context.filter = ofilter;
      //       context.drawImage(img, 0, 0, canvas.width, canvas.height);
      //       data = canvas.toDataURL("image/jpg", 1.0);
      //     }
      //     if (type == 'transform') {
      //       canvas.width = img.offsetWidth;
      //       canvas.height = img.offsetHeight;
      //       context.filter = ofilter;
      //       if (property == 'rotateY' && value == '180') {
      //         context.translate(canvas.width, 0);
      //         context.scale(-1, 1);
      //         context.drawImage(img, 0, 0, canvas.width, canvas.height);
      //         data = canvas.toDataURL("image/jpg", 1.0);
      //         img.src = data;
      //       }
      //       if (property == 'rotateY' && value == '360') {
      //         img.src = this.originalImage;
      //       }
      //       if (property == 'rotate' && value == '-90') {
      //         var image = document.createElement("img");
      //         var ratio;
      //         image.onload = function () {
      //           canvas.width = 100;
      //           canvas.height = 150;
      //           ratio = canvas.height / canvas.width;
      //           console.log(ratio);
      //           context.drawImage(img, 0, 0, canvas.width, canvas.height);
      //         }
      //         context.clearRect(0, 0, canvas.width, canvas.height);
      //         context.save();
      //         context.translate(canvas.width / 2, canvas.height / 2);
      //         context.rotate(270 * Math.PI / 180);
      //         if ((270 - 90) % 180 == 0)
      //           context.scale(ratio, ratio);
      //         context.translate(-canvas.width / 2, -canvas.height / 2);
      //         context.drawImage(img, 0, 0, canvas.width, canvas.height);
      //         data = canvas.toDataURL("image/jpg", 1.0);
      //         img.src = data;
      //         // this.originalImage = data;
      //       }
      //       if (property == 'rotate' && value == '0') {
      //         // context.drawImage(img, 0, 0, canvas.width, canvas.height);
      //         img.src = this.originalImage;
      //       }
      //     }
      //     // this.originalImage = data; 
      //     this.canvasImage = data;
      //   }
      //   //  Select the filter property
      //   selectedFilterProperty(value) {
      //     this.property = value;
      //   }
      //   // =====================Image Crop===================== 
      //   // Crop the image
      //   cropImage(imgPath) {
      //     let imgPathUrl = imgPath;
      //     // let imgPathUrl = (this.canvasImage); 
      //     this.crop.crop(imgPathUrl, this.cropOptions)
      //       .then(
      //         newPath => {
      //           // alert(newPath);
      //           // alert(newPath.split('?')[0]);
      //           this.showCroppedImage(newPath.split('?')[0])
      //         },
      //         error => {
      //           console.log("CROP ERROR -> " + JSON.stringify(error));
      //           alert("CROP ERROR-cropImage: " + JSON.stringify(error));
      //         }
      //       );
      //   }
      //   // Show and save the cropped image
      //   showCroppedImage(ImagePath) {
      //     this.isLoading = true;
      //     var copyPath = ImagePath;
      //     var splitPath = copyPath.split('/');
      //     var imageName = splitPath[splitPath.length - 1];
      //     var filePath = ImagePath.split(imageName)[0];
      //     this.tempPath = ImagePath;
      //     // alert(ImagePath); 
      //     this.file.readAsDataURL(filePath, imageName).then(base64 => {
      //       this.originalImage = base64;
      //     }, error => {
      //       console.log("CROP ERROR -> " + JSON.stringify(error));
      //       alert("CROP ERROR - showCroppedImage: " + JSON.stringify(error));
      //     });
      //   }
      //   // =====================Enable Image Edit ===================== 
      //   enableEdit() {
      //     this.enableEditMode = true;
      //   }
      //   // =====================Forward the video data to next page =====================  
      //   submitFileData() {
      //     // alert('submitFileData --- ' + this.location);
      //     this.service.set('imageUrlPath', this.tempPath).then(result => {
      //       console.log('Data is saved');
      //       // alert('perview img'+result);
      //     }).catch(e => {
      //       console.log("error: " + e);
      //     });
      //     // console.log(this.canvasImage ? "this.canvasImage" : "this.originalImage");
      //     // alert('category-selection' + this.canvasImage ? this.canvasImage : this.originalImage);
      //     alert(this.tempPath);
      //     var media;
      //     if (this.originalImage.indexOf("http:") !== -1) {
      //       media = this.canvasImage ? this.canvasImage : this.tempPath;
      //     } else {
      //       media = this.tempPath;
      //     }
      //     if (this.type == 'video') {
      //       this.router.navigate(['/category-selection', { 'location': this.location, 'mediaPath': this.tempPath, 'type': this.type, 'page': this.page }]);
      //     } else {
      //       this.router.navigate(['/category-selection', { 'location': this.location, 'mediaPath': media, 'type': this.type, 'page': this.page }]);
      //     }
      //   }
      //   //=====================Open Menu=====================
      //   openMenu() {
      //     // this.menu.enable(true, 'first');
      //     this.menu.open();
      //   }
      //   //=====================Goto Back=====================
      //   goBack() {
      //     if (this.page == 'home') {
      //       this.router.navigate(['/home', { 'type': this.type, 'page': this.page }]);
      //     } else {
      //       this.router.navigate(['/upload-images-videos', { 'type': this.type, 'page': this.page }]);
      //     }
      //   }
      //   //=====================Get Current Location=====================
      //   options: GeolocationOptions;
      //   currentPos: Geoposition;
      //   locationTraces = [];
      //   latitude: any = 0; //latitude
      //   longitude: any = 0; //longitude
      //   address: string;
      //   getUserPosition() {
      //     this.options = {
      //       enableHighAccuracy: false
      //     };
      //     this.geolocation.getCurrentPosition(this.options).then((pos: Geoposition) => {
      //       this.currentPos = pos;
      //       this.latitude = pos.coords.latitude;
      //       this.longitude = pos.coords.longitude;
      //       this.getAddress(this.latitude, this.longitude);
      //       console.log(pos);
      //     }, (err: PositionError) => {
      //       console.log("error : " + err.message);
      //       ;
      //     })
      //   }
      //   // geocoder options
      //   nativeGeocoderOptions: NativeGeocoderOptions = {
      //     useLocale: true,
      //     maxResults: 5
      //   };
      //   // get address using coordinates
      //   getAddress(lat, long) {
      //     this.nativeGeocoder.reverseGeocode(lat, long, this.nativeGeocoderOptions)
      //       .then((res: NativeGeocoderResult[]) => {
      //         this.address = this.pretifyAddress(res[0]);
      //         this.location = this.pretifyAddress(res[0]);
      //       })
      //       .catch((error: any) => {
      //         alert('Error getting location' + error + JSON.stringify(error));
      //       });
      //   }
      //   // address
      //   pretifyAddress(addressObj) {
      //     let obj = [];
      //     let address = "";
      //     for (let key in addressObj) {
      //       obj.push(addressObj[key]);
      //     }
      //     obj.reverse();
      //     for (let val in obj) {
      //       if (obj[val].length)
      //         address += obj[val] + ', ';
      //     }
      //     return address.slice(0, -2);
      //   }
      // }

      /***/
    }
  }]);
})();
//# sourceMappingURL=resourse-resourse-module-es5.js.map