(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["search-search-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/search/search.page.html":
/*!*******************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/search/search.page.html ***!
  \*******************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-header>\r\n</app-header>\r\n<ion-content>\r\n    <div class=\"inner-div\">\r\n        <ion-grid>\r\n            <ion-row searchbar>\r\n                <ion-col>\r\n                    <ion-searchbar color=\"yellow\" [(ngModel)]=\"homeSearch\" showCancelButton=\"focus\"\r\n                        (ionChange)=\"HomeSearch()\" debounce=\"1000\">\r\n                    </ion-searchbar>\r\n                </ion-col>\r\n            </ion-row>\r\n        </ion-grid>\r\n        <ion-grid class=\"pt-3\">\r\n            <ion-row *ngIf=\"SeearchedLists.length == 0\" >\r\n                <ion-col>\r\n                    <ion-card class=\"ion-margin-vertical\">\r\n                        <ion-card-content>\r\n                            No Record Found\r\n                        </ion-card-content>\r\n                    </ion-card>\r\n                </ion-col>\r\n            </ion-row>\r\n            <ion-row *ngIf=\"SeearchedLists.length > 0\" >\r\n                <ion-col (click)=\"gotoInnerPage(post)\" size=6 *ngFor=\"let post of SeearchedLists\">\r\n                    <img *ngIf=\"post.image_video == 'IMAGE'\" [src]=\"post.upload_image\" class=\"img_size\" crossorigin>\r\n                    <div *ngIf=\"post.image_video == 'VIDEO'\" ion-col-height>\r\n                        <ion-content>\r\n                            <img [src]=\"post.upload_thump\" crossorigin class=\"video_tag\">\r\n                            <!-- <ion-item class=\"fs-12 black-bg\" black-bg fill=\"clear\">\r\n                                <ion-icon name=\"play-circle-outline\"></ion-icon> <span> &nbsp; Play</span>\r\n\r\n                            </ion-item> -->\r\n                            <!-- <ion-icon name=\"play-circle-outline\" class=\"videoicon\"></ion-icon> -->\r\n                            <div class=\"overlay\">\r\n                                <ion-icon name=\"play-circle-outline\" class=\"videoicon\"></ion-icon>\r\n                            </div>\r\n                        </ion-content>\r\n                        <!-- <ion-content>\r\n                            <video poster=\"\" preload=\"auto\" crossorigin class=\"video_tag\">\r\n                                <source [src]=\"post.upload_image\" type=\"\">\r\n                            </video>\r\n                            <ion-item class=\"fs-12 black-bg\" fill=\"clear\">\r\n                                <ion-icon name=\"play-circle-outline\"></ion-icon> <span> &nbsp; Play</span>\r\n                            </ion-item>\r\n                        </ion-content> -->\r\n                    </div>\r\n                </ion-col>\r\n            </ion-row>\r\n        </ion-grid>\r\n        <ion-grid *ngIf=\"loadMoreBtn\">\r\n            <ion-row class=\"ion-justify-content-center\">\r\n                <ion-col>\r\n                    <div class=\"ion-text-center\">\r\n                        <ion-button color=\"yellow btn-curve\" size=\"small\" btn-yellow class=\"mt-10\" (click)=\"loadData()\">Load\r\n                            More &nbsp;<ion-icon name=\"arrow-down-outline\"></ion-icon>\r\n                        </ion-button>\r\n                    </div>\r\n                </ion-col>\r\n            </ion-row>\r\n        </ion-grid>\r\n        <!-- <ion-infinite-scroll threshold=\"100px\" (ionInfinite)=\"loadData($event)\">\r\n            <ion-infinite-scroll-content class=\"loadingspinner\" loadingSpinner=\"bubbles\" loadingText=\"Loading more data...\">\r\n            </ion-infinite-scroll-content>\r\n        </ion-infinite-scroll> -->\r\n    </div>\r\n\r\n</ion-content>\r\n\r\n<ion-toolbar *ngIf=\"showToolbar == true\" color=\"yellow\" slot=\"bottom\">\r\n    <ion-tabs>\r\n        <ion-tab-bar color=\"yellow\" slot=\"bottom\">\r\n            <ion-tab-button [routerLink]=\"['/home']\">\r\n                <ion-icon name=\"home-outline\"></ion-icon>\r\n                <ion-label>Home</ion-label>\r\n            </ion-tab-button>\r\n        </ion-tab-bar>\r\n    </ion-tabs>\r\n</ion-toolbar>\r\n<!-- <ion-tabs>\r\n    <ion-tab-bar color=\"yellow\" slot=\"bottom\">\r\n        <ion-tab-button [routerLink]=\"['/home']\">\r\n            <ion-icon name=\"home-outline\"></ion-icon>\r\n            <ion-label>Home</ion-label>\r\n        </ion-tab-button>\r\n    </ion-tab-bar>\r\n</ion-tabs> -->");

/***/ }),

/***/ "./src/app/search/search-routing.module.ts":
/*!*************************************************!*\
  !*** ./src/app/search/search-routing.module.ts ***!
  \*************************************************/
/*! exports provided: SearchPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SearchPageRoutingModule", function() { return SearchPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _search_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./search.page */ "./src/app/search/search.page.ts");




const routes = [
    {
        path: '',
        component: _search_page__WEBPACK_IMPORTED_MODULE_3__["SearchPage"]
    }
];
let SearchPageRoutingModule = class SearchPageRoutingModule {
};
SearchPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], SearchPageRoutingModule);



/***/ }),

/***/ "./src/app/search/search.module.ts":
/*!*****************************************!*\
  !*** ./src/app/search/search.module.ts ***!
  \*****************************************/
/*! exports provided: SearchPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SearchPageModule", function() { return SearchPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _search_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./search-routing.module */ "./src/app/search/search-routing.module.ts");
/* harmony import */ var _search_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./search.page */ "./src/app/search/search.page.ts");
/* harmony import */ var _header_header_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../header/header.module */ "./src/app/header/header.module.ts");








let SearchPageModule = class SearchPageModule {
};
SearchPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _search_routing_module__WEBPACK_IMPORTED_MODULE_5__["SearchPageRoutingModule"], _header_header_module__WEBPACK_IMPORTED_MODULE_7__["HeaderPageModule"]
        ],
        declarations: [_search_page__WEBPACK_IMPORTED_MODULE_6__["SearchPage"]]
    })
], SearchPageModule);



/***/ }),

/***/ "./src/app/search/search.page.scss":
/*!*****************************************!*\
  !*** ./src/app/search/search.page.scss ***!
  \*****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("[searchbar] {\n  background: white;\n  margin: -15px !important;\n  padding: 1rem;\n}\n[searchbar] ion-searchbar {\n  width: 90%;\n  --border-radius: 50px;\n  margin: 0 auto;\n}\n.searchbar-md .searchbar-input {\n  --background: var(--ion-color-warning) !important;\n}\n[ion-col-height] {\n  width: 100%;\n  height: 150px;\n}\n[ion-col-height] ion-content {\n  --background: #525252;\n}\n[ion-col-height] ion-content ion-button {\n  font-size: 30px;\n  position: absolute;\n  left: 50%;\n  top: 50%;\n  transform: translate(-50%, -50%);\n}\n.video_tag {\n  width: 100%;\n  height: 100%;\n  -o-object-fit: cover;\n     object-fit: cover;\n  overflow: hidden;\n  position: absolute;\n  z-index: 0;\n}\n.overlay {\n  position: absolute;\n  bottom: 0;\n  left: 0;\n  z-index: 1;\n  height: 20%;\n}\n.overlay p {\n  margin-top: 0;\n  margin-bottom: 1rem;\n  background: var(--ion-color-yellow);\n  padding: 10px;\n  position: absolute;\n  bottom: 0;\n  text-overflow: ellipsis;\n  /* width: 128%; */\n}\n.img_size {\n  width: 100%;\n  -o-object-fit: cover;\n     object-fit: cover;\n  -o-object-position: center;\n     object-position: center;\n  background-color: transparent;\n  height: 150px;\n}\n.videoicon {\n  color: white;\n  font-size: 60px;\n  position: absolute;\n  top: 50%;\n  left: 50%;\n  transform: translate(-50%, -50%);\n  -ms-transform: translate(-50%, -50%);\n  text-align: center;\n}\n.overlay {\n  position: absolute;\n  top: 0;\n  bottom: 0;\n  left: 0;\n  right: 0;\n  height: 100%;\n  width: 100%;\n  opacity: 0;\n  transition: 0.3s ease;\n  opacity: 1;\n}\n.btn-curve {\n  border-radius: 10px;\n  --border-radius: 10px;\n  --ion-color-contrast: #212721;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc2VhcmNoL3NlYXJjaC5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxpQkFBQTtFQUNBLHdCQUFBO0VBQ0EsYUFBQTtBQUNKO0FBQUk7RUFDSSxVQUFBO0VBQ0EscUJBQUE7RUFDQSxjQUFBO0FBRVI7QUFNSTtFQUNJLGlEQUFBO0FBSFI7QUFTQTtFQUNJLFdBQUE7RUFDQSxhQUFBO0FBTko7QUFRSTtFQUNJLHFCQUFBO0FBTlI7QUFPUTtFQUNJLGVBQUE7RUFDQSxrQkFBQTtFQUNBLFNBQUE7RUFDQSxRQUFBO0VBQ0EsZ0NBQUE7QUFMWjtBQVNBO0VBQ0ksV0FBQTtFQUNBLFlBQUE7RUFDQSxvQkFBQTtLQUFBLGlCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxrQkFBQTtFQUNBLFVBQUE7QUFOSjtBQVFBO0VBQ0ksa0JBQUE7RUFDQSxTQUFBO0VBQ0EsT0FBQTtFQUNBLFVBQUE7RUFDQSxXQUFBO0FBTEo7QUFNSTtFQUNJLGFBQUE7RUFDQSxtQkFBQTtFQUNBLG1DQUFBO0VBQ0EsYUFBQTtFQUNBLGtCQUFBO0VBQ0EsU0FBQTtFQUNBLHVCQUFBO0VBQ0EsaUJBQUE7QUFKUjtBQU9BO0VBQ0ksV0FBQTtFQUNBLG9CQUFBO0tBQUEsaUJBQUE7RUFDQSwwQkFBQTtLQUFBLHVCQUFBO0VBQ0EsNkJBQUE7RUFDQSxhQUFBO0FBSko7QUFXQTtFQUlJLFlBQUE7RUFDQSxlQUFBO0VBQ0Esa0JBQUE7RUFDQSxRQUFBO0VBQ0EsU0FBQTtFQUNBLGdDQUFBO0VBQ0Esb0NBQUE7RUFDQSxrQkFBQTtBQVhKO0FBYUk7RUFDQSxrQkFBQTtFQUNBLE1BQUE7RUFDQSxTQUFBO0VBQ0EsT0FBQTtFQUNBLFFBQUE7RUFDQSxZQUFBO0VBQ0EsV0FBQTtFQUNBLFVBQUE7RUFDQSxxQkFBQTtFQUNBLFVBQUE7QUFWSjtBQWFJO0VBQ0ksbUJBQUE7RUFDQSxxQkFBQTtFQUNBLDZCQUFBO0FBVlIiLCJmaWxlIjoic3JjL2FwcC9zZWFyY2gvc2VhcmNoLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIltzZWFyY2hiYXJdIHtcclxuICAgIGJhY2tncm91bmQ6IHdoaXRlO1xyXG4gICAgbWFyZ2luOiAtMTVweCFpbXBvcnRhbnQ7XHJcbiAgICBwYWRkaW5nOiAxcmVtO1xyXG4gICAgaW9uLXNlYXJjaGJhciB7XHJcbiAgICAgICAgd2lkdGg6IDkwJTtcclxuICAgICAgICAtLWJvcmRlci1yYWRpdXM6IDUwcHg7XHJcbiAgICAgICAgbWFyZ2luOiAwIGF1dG87XHJcbiAgICB9XHJcbn1cclxuXHJcbi8vIC5zZWFyY2hiYXItaW5wdXQuc2MtaW9uLXNlYXJjaGJhci1tZCB7XHJcbi8vICAgICBiYWNrZ3JvdW5kOiAjZmZjMTA3N2QgIWltcG9ydGFudDtcclxuLy8gfVxyXG4uc2VhcmNoYmFyLW1kIHtcclxuICAgIC5zZWFyY2hiYXItaW5wdXQge1xyXG4gICAgICAgIC0tYmFja2dyb3VuZDogdmFyKC0taW9uLWNvbG9yLXdhcm5pbmcpICFpbXBvcnRhbnQ7XHJcbiAgICAgICAgLy9iYWNrZ3JvdW5kLWNvbG9yOiAjMDA5MTk2O1xyXG4gICAgfVxyXG59XHJcblxyXG5cclxuW2lvbi1jb2wtaGVpZ2h0XSB7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGhlaWdodDogMTUwcHg7XHJcbiAgICAvLyBiYWNrZ3JvdW5kOiAjZmZmZmZmO1xyXG4gICAgaW9uLWNvbnRlbnQge1xyXG4gICAgICAgIC0tYmFja2dyb3VuZDogIzUyNTI1MjtcclxuICAgICAgICBpb24tYnV0dG9uIHtcclxuICAgICAgICAgICAgZm9udC1zaXplOiAzMHB4O1xyXG4gICAgICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgICAgICAgIGxlZnQ6IDUwJTtcclxuICAgICAgICAgICAgdG9wOiA1MCU7XHJcbiAgICAgICAgICAgIHRyYW5zZm9ybTogdHJhbnNsYXRlKC01MCUsIC01MCUpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxufVxyXG4udmlkZW9fdGFnIHtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgb2JqZWN0LWZpdDogY292ZXI7XHJcbiAgICBvdmVyZmxvdzogaGlkZGVuO1xyXG4gICAgcG9zaXRpb246YWJzb2x1dGU7XHJcbiAgICB6LWluZGV4OjA7XHJcbn1cclxuLm92ZXJsYXkge1xyXG4gICAgcG9zaXRpb246YWJzb2x1dGU7XHJcbiAgICBib3R0b206IDA7XHJcbiAgICBsZWZ0OiAwO1xyXG4gICAgei1pbmRleDoxO1xyXG4gICAgaGVpZ2h0OiAyMCU7XHJcbiAgICBwe1xyXG4gICAgICAgIG1hcmdpbi10b3A6IDA7XHJcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogMXJlbTtcclxuICAgICAgICBiYWNrZ3JvdW5kOiB2YXIoLS1pb24tY29sb3IteWVsbG93KTsvLyNmNmM3NjI7XHJcbiAgICAgICAgcGFkZGluZzogMTBweDtcclxuICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgICAgYm90dG9tOiAwO1xyXG4gICAgICAgIHRleHQtb3ZlcmZsb3c6IGVsbGlwc2lzO1xyXG4gICAgICAgIC8qIHdpZHRoOiAxMjglOyAqL1xyXG4gICAgfVxyXG59XHJcbi5pbWdfc2l6ZSB7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIG9iamVjdC1maXQ6IGNvdmVyO1xyXG4gICAgb2JqZWN0LXBvc2l0aW9uOiBjZW50ZXI7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDtcclxuICAgIGhlaWdodDogMTUwcHg7XHJcbn1cclxuLy8gLnZpZGVvaWNvbntcclxuLy8gICAgIGZvbnQtc2l6ZTogNjBweDtcclxuLy8gICAgIGNvbG9yOiB3aGl0ZTtcclxuLy8gICAgIG1hcmdpbjogNDVweCA2NXB4O1xyXG4vLyB9XHJcbi52aWRlb2ljb257XHJcbiAgICAvLyBmb250LXNpemU6IDYwcHg7XHJcbiAgICAvLyBjb2xvcjogd2hpdGU7XHJcbiAgICAvLyBtYXJnaW46IDI1JSAyNSU7XHJcbiAgICBjb2xvcjogd2hpdGU7XHJcbiAgICBmb250LXNpemU6IDYwcHg7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICB0b3A6IDUwJTtcclxuICAgIGxlZnQ6IDUwJTtcclxuICAgIHRyYW5zZm9ybTogdHJhbnNsYXRlKC01MCUsIC01MCUpO1xyXG4gICAgLW1zLXRyYW5zZm9ybTogdHJhbnNsYXRlKC01MCUsIC01MCUpO1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgfVxyXG4gICAgLm92ZXJsYXkge1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgdG9wOiAwO1xyXG4gICAgYm90dG9tOiAwO1xyXG4gICAgbGVmdDogMDtcclxuICAgIHJpZ2h0OiAwO1xyXG4gICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBvcGFjaXR5OiAwO1xyXG4gICAgdHJhbnNpdGlvbjogLjNzIGVhc2U7XHJcbiAgICBvcGFjaXR5OiAxO1xyXG4gICAgfVxyXG5cclxuICAgIC5idG4tY3VydmV7XHJcbiAgICAgICAgYm9yZGVyLXJhZGl1czogMTBweDtcclxuICAgICAgICAtLWJvcmRlci1yYWRpdXM6IDEwcHg7XHJcbiAgICAgICAgLS1pb24tY29sb3ItY29udHJhc3Q6ICMyMTI3MjE7XHJcbiAgICAgfSJdfQ== */");

/***/ }),

/***/ "./src/app/search/search.page.ts":
/*!***************************************!*\
  !*** ./src/app/search/search.page.ts ***!
  \***************************************/
/*! exports provided: SearchPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SearchPage", function() { return SearchPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _global_services_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../global-services.service */ "./src/app/global-services.service.ts");
/* harmony import */ var _ionic_native_keyboard__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic-native/keyboard */ "./node_modules/@ionic-native/keyboard/index.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");







let SearchPage = class SearchPage {
    constructor(menuCtrl, service, router) {
        this.menuCtrl = menuCtrl;
        this.service = service;
        this.router = router;
        // =====================Hide Toolbar when keypad is open=====================
        this.showToolbar = true;
        this.SeearchedLists = [];
        this.page = 1;
        this.itemsPerPage = 6;
        this.loadMoreBtn = false;
        this.onHomeSearch = () => {
            var username = localStorage.getItem('username');
            let obj = {
                'pageid': this.page,
                'rows_in_page': 12,
                'username': username,
                'image_video': 'ALL',
                'image_category': this.homeSearch
            };
            this.service.post_data("getTrendSpotterImagesVideosWithPaging", obj)
                .subscribe((result) => {
                let json = this.service.parserToJSON(result);
                let jons1 = JSON.stringify(json);
                let json2 = jons1.replace(/\\n/g, '');
                var parsedJSON = JSON.parse(JSON.parse(json2).string);
                // let search_result = JSON.parse(json.string);
                this.totalRecordCount = parsedJSON.Total_Records;
                if (this.totalRecordCount >= this.SeearchedLists.length) {
                    this.SeearchedLists = this.SeearchedLists.concat(parsedJSON.Details);
                    setTimeout(() => {
                        if (this.totalRecordCount > this.SeearchedLists.length) {
                            this.loadMoreBtn = true;
                        }
                        else {
                            this.loadMoreBtn = false;
                        }
                    }, 500);
                }
            }, (error) => {
                console.log(error);
            });
        };
    }
    ngOnInit() {
        _ionic_native_keyboard__WEBPACK_IMPORTED_MODULE_4__["Keyboard"].onKeyboardShow().subscribe(() => {
            this.showToolbar = false;
        });
        _ionic_native_keyboard__WEBPACK_IMPORTED_MODULE_4__["Keyboard"].onKeyboardHide().subscribe(() => {
            this.showToolbar = true;
        });
    }
    ionViewWillEnter() {
        this.service.sendMessage('startNotificationCountFunction');
    }
    loadData() {
        // setTimeout(() => {
        this.page = this.page + 1;
        this.loadMoreBtn = false;
        this.HomeSearch();
        //   event.target.complete();
        //   if (this.SeearchedLists.length == this.totalRecordCount) {
        //     event.target.disabled = true;
        //   }
        // }, 700);
    }
    // ===================== ON CHANGE SEARCH EVENTS =====================
    HomeSearch() {
        if (this.homeSearch) {
            this.onHomeSearch();
        }
        else {
            this.SeearchedLists = [];
            this.loadMoreBtn = false;
            this.page = 1;
        }
    }
    gotoInnerPage(data) {
        this.router.navigate(['/media-info', { 'data': JSON.stringify(data), 'page': 'search' }]);
    }
    //=====================Open Menu=====================
    openMenu() {
        // this.menu.enable(true, 'first');
        this.menuCtrl.open();
    }
};
SearchPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["MenuController"] },
    { type: _global_services_service__WEBPACK_IMPORTED_MODULE_3__["GlobalServicesService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"] }
];
SearchPage.propDecorators = {
    infiniteScroll: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"], args: [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["IonInfiniteScroll"],] }]
};
SearchPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-search',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./search.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/search/search.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./search.page.scss */ "./src/app/search/search.page.scss")).default]
    })
], SearchPage);



/***/ })

}]);
//# sourceMappingURL=search-search-module-es2015.js.map