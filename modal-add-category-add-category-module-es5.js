(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["modal-add-category-add-category-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/modal/add-category/add-category.page.html":
    /*!*************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/modal/add-category/add-category.page.html ***!
      \*************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppModalAddCategoryAddCategoryPageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-content padding color=\"white\">\r\n    <ion-grid>\r\n        <ion-row>\r\n            <ion-col>\r\n                <ion-button color=\"dark\" class=\"ion-float-right\" fill=\"clear\" (click)=\"ClosePopover()\">\r\n                    <ion-icon name=\"close-outline\"></ion-icon>\r\n                </ion-button>\r\n            </ion-col>\r\n        </ion-row>\r\n        <ion-row>\r\n            <ion-col>\r\n                <h5 class=\"ion-text-center\">Add Category</h5>\r\n            </ion-col>\r\n        </ion-row>\r\n        <ion-row>\r\n            <ion-col>\r\n                <div>\r\n                    <ion-item input-text-color>\r\n                        <ion-label color=\"dark\" position=\"floating\">Enter Category\r\n                        </ion-label>\r\n                        <ion-input color=\"dark\"></ion-input>\r\n                    </ion-item>\r\n                </div>\r\n            </ion-col>\r\n        </ion-row>\r\n        <ion-row>\r\n            <ion-col>\r\n                <div>\r\n                    <ion-button (click)=\"ClosePopover()\" expand=\"block\" color=\"yellow\" size=\"default\" btn-yellow class=\"mt-15\">Add\r\n                    </ion-button>\r\n                </div>\r\n            </ion-col>\r\n        </ion-row>\r\n    </ion-grid>\r\n</ion-content>";
      /***/
    },

    /***/
    "./src/app/modal/add-category/add-category-routing.module.ts":
    /*!*******************************************************************!*\
      !*** ./src/app/modal/add-category/add-category-routing.module.ts ***!
      \*******************************************************************/

    /*! exports provided: AddCategoryPageRoutingModule */

    /***/
    function srcAppModalAddCategoryAddCategoryRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "AddCategoryPageRoutingModule", function () {
        return AddCategoryPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _add_category_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./add-category.page */
      "./src/app/modal/add-category/add-category.page.ts");

      var routes = [{
        path: '',
        component: _add_category_page__WEBPACK_IMPORTED_MODULE_3__["AddCategoryPage"]
      }];

      var AddCategoryPageRoutingModule = function AddCategoryPageRoutingModule() {
        _classCallCheck(this, AddCategoryPageRoutingModule);
      };

      AddCategoryPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], AddCategoryPageRoutingModule);
      /***/
    },

    /***/
    "./src/app/modal/add-category/add-category.module.ts":
    /*!***********************************************************!*\
      !*** ./src/app/modal/add-category/add-category.module.ts ***!
      \***********************************************************/

    /*! exports provided: AddCategoryPageModule */

    /***/
    function srcAppModalAddCategoryAddCategoryModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "AddCategoryPageModule", function () {
        return AddCategoryPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _add_category_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./add-category-routing.module */
      "./src/app/modal/add-category/add-category-routing.module.ts");
      /* harmony import */


      var _add_category_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./add-category.page */
      "./src/app/modal/add-category/add-category.page.ts");

      var AddCategoryPageModule = function AddCategoryPageModule() {
        _classCallCheck(this, AddCategoryPageModule);
      };

      AddCategoryPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _add_category_routing_module__WEBPACK_IMPORTED_MODULE_5__["AddCategoryPageRoutingModule"]],
        declarations: [_add_category_page__WEBPACK_IMPORTED_MODULE_6__["AddCategoryPage"]]
      })], AddCategoryPageModule);
      /***/
    },

    /***/
    "./src/app/modal/add-category/add-category.page.scss":
    /*!***********************************************************!*\
      !*** ./src/app/modal/add-category/add-category.page.scss ***!
      \***********************************************************/

    /*! exports provided: default */

    /***/
    function srcAppModalAddCategoryAddCategoryPageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL21vZGFsL2FkZC1jYXRlZ29yeS9hZGQtY2F0ZWdvcnkucGFnZS5zY3NzIn0= */";
      /***/
    },

    /***/
    "./src/app/modal/add-category/add-category.page.ts":
    /*!*********************************************************!*\
      !*** ./src/app/modal/add-category/add-category.page.ts ***!
      \*********************************************************/

    /*! exports provided: AddCategoryPage */

    /***/
    function srcAppModalAddCategoryAddCategoryPageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "AddCategoryPage", function () {
        return AddCategoryPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");

      var AddCategoryPage = /*#__PURE__*/function () {
        function AddCategoryPage(popover) {
          _classCallCheck(this, AddCategoryPage);

          this.popover = popover;
        }

        _createClass(AddCategoryPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }, {
          key: "ClosePopover",
          value: function ClosePopover() {
            this.popover.dismiss();
          }
        }]);

        return AddCategoryPage;
      }();

      AddCategoryPage.ctorParameters = function () {
        return [{
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["PopoverController"]
        }];
      };

      AddCategoryPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-add-category',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./add-category.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/modal/add-category/add-category.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./add-category.page.scss */
        "./src/app/modal/add-category/add-category.page.scss"))["default"]]
      })], AddCategoryPage);
      /***/
    }
  }]);
})();
//# sourceMappingURL=modal-add-category-add-category-module-es5.js.map