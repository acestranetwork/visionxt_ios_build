(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["home-home-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/header/header.page.html":
    /*!*******************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/header/header.page.html ***!
      \*******************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppHeaderHeaderPageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-header translucent no-border>\r\n    <ion-toolbar>\r\n        <ion-item header-menu lines=\"none\">\r\n            <ion-avatar slot=\"start\">\r\n                <img [src]=\"loggedDetails.upload_image\" height=\"100%\" width=\"100%\" *ngIf=\"loggedDetails.upload_image\">\r\n                <img src=\"../../assets//image/user-login-icon-1.png\" height=\"100%\" width=\"100%\"\r\n                    *ngIf=\"!loggedDetails.upload_image\">\r\n            </ion-avatar>\r\n            <ion-label>\r\n                <h3 font-header class=\"ion-text-capitalize\">{{loggedDetails.stud_name}}</h3>\r\n                <p font-medium>{{city}}</p>\r\n            </ion-label>\r\n            <div class=\"notification-bell-container\" slot=\"end\">\r\n                <p class=\"text-highlight\" (click)=\"moveNotificationPage()\"\r\n                    *ngIf=\"notificationUnreadCount && notificationUnreadCount!=0\">\r\n                    {{notificationUnreadCount > 9 ? '9+': notificationUnreadCount}}</p>\r\n                <ion-icon name=\"notifications-outline\" (click)=\"moveNotificationPage()\" class=\"bell-icon\">\r\n                </ion-icon>\r\n            </div>\r\n            <ion-icon name=\"reorder-four-outline\" (click)=\"openMenu()\" slot=\"end\"></ion-icon>\r\n        </ion-item>\r\n    </ion-toolbar>\r\n</ion-header>";
      /***/
    },

    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/home/home.page.html":
    /*!***************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/home/home.page.html ***!
      \***************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppHomeHomePageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<app-header>\r\n</app-header>\r\n<ion-content>\r\n    <div class=\"inner-div\">\r\n        <ion-grid class=\"\">\r\n            <!-- pt-3 -->\r\n            <ion-row class=\"ion-text-center\">\r\n                <ion-col>\r\n                    <ion-button rippleEffect=\"false\" [routerLink]=\"['/upload-images-videos']\"\r\n                        [queryParams]=\"{type:'image'}\" class=\"ion-text-capitalize btn-curve\" color=\"yellow\"\r\n                        size=\"default\">\r\n                        Upload Image\r\n                    </ion-button>\r\n                </ion-col>\r\n                <ion-col>\r\n                    <ion-button rippleEffect=\"false\" [routerLink]=\"['/upload-images-videos']\"\r\n                        [queryParams]=\"{type:'video'}\" class=\"ion-text-capitalize btn-curve\" color=\"yellow\"\r\n                        size=\"default\">\r\n                        Upload Video\r\n                    </ion-button>\r\n                </ion-col>\r\n            </ion-row>\r\n        </ion-grid>\r\n        <ion-grid>\r\n            <ion-row>\r\n                <ion-col (click)=\"gotoInnerPage(post)\" size=6 *ngFor=\"let post of  postedList\">\r\n                    <img *ngIf=\"post.image_video == 'IMAGE'\" [src]=\"post.upload_image\" class=\"img_size\" crossorigin>\r\n                    <div *ngIf=\"post.image_video == 'VIDEO'\" black-bg ion-col-height>\r\n                        <ion-content>\r\n                            <!-- <video preload=\"auto\" controls crossorigin class=\"video_tag\">\r\n                                <source [src]=\"post.upload_image\" type=\"\">\r\n                            </video> -->\r\n                            <!-- <video [poster]=\"post.upload_thump\" preload=\"auto\" crossorigin class=\"video_tag\">\r\n                                <source [src]=\"post.upload_image\" type=\"\">\r\n                            </video> -->\r\n                            <img [src]=\"post.upload_thump\" crossorigin class=\"video_tag\">\r\n                            <!-- <ion-item class=\"fs-12 black-bg\" black-bg fill=\"clear\"> -->\r\n                            <!-- <ion-icon name=\"play-circle-outline\" class=\"videoicon\"></ion-icon> -->\r\n                            <!-- </ion-item> -->\r\n                            <div class=\"overlay\">\r\n                                <ion-icon name=\"play-circle-outline\" class=\"videoicon\"></ion-icon>\r\n                            </div>\r\n                        </ion-content>\r\n                    </div>\r\n                </ion-col>\r\n            </ion-row>\r\n            <ion-row>\r\n                <ion-col>\r\n                    <ion-item lines=\"none\" [routerLink]=\"['/uploadedimages']\" class=\"border-round\">\r\n                        <ion-icon margin-1 class=\"icon mx-auto\" color=\"yellow\" name=\"images-outline\">\r\n                        </ion-icon>\r\n                    </ion-item>\r\n                </ion-col>\r\n                <ion-col>\r\n                    <ion-item lines=\"none\" class=\"align-content-center\" [routerLink]=\"['/uploadedvideos']\"\r\n                        class=\"border-round\">\r\n                        <ion-icon margin-1 name=\"albums-outline\" class=\"icon mx-auto\"></ion-icon>\r\n                    </ion-item>\r\n                </ion-col>\r\n            </ion-row>\r\n        </ion-grid>\r\n    </div>\r\n    <!-- for getting image width and height -->\r\n    <!-- <img id='getImagePreview' *ngIf=\"imagePreview\" [src]=\"imagePreview\" class=\"w-100\" style=\"visibility: hidden;\" /> -->\r\n</ion-content>\r\n<ion-toolbar color=\"yellow\" slot=\"bottom\">\r\n    <ion-tabs>\r\n        <ion-tab-bar color=\"yellow\" slot=\"bottom\">\r\n            <ion-tab-button (click)=\"presentActionSheet()\">\r\n                <ion-icon name=\"camera-outline\"></ion-icon>\r\n                <ion-label>Camera</ion-label>\r\n            </ion-tab-button>\r\n            <ion-tab-button [routerLink]=\"['/search']\">\r\n                <ion-icon name=\"search-outline\"></ion-icon>\r\n                <ion-label>Search</ion-label>\r\n            </ion-tab-button>\r\n        </ion-tab-bar>\r\n    </ion-tabs>\r\n</ion-toolbar>";
      /***/
    },

    /***/
    "./src/app/header/header-routing.module.ts":
    /*!*************************************************!*\
      !*** ./src/app/header/header-routing.module.ts ***!
      \*************************************************/

    /*! exports provided: HeaderPageRoutingModule */

    /***/
    function srcAppHeaderHeaderRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "HeaderPageRoutingModule", function () {
        return HeaderPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _header_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./header.page */
      "./src/app/header/header.page.ts");

      var routes = [{
        path: '',
        component: _header_page__WEBPACK_IMPORTED_MODULE_3__["HeaderPage"]
      }];

      var HeaderPageRoutingModule = function HeaderPageRoutingModule() {
        _classCallCheck(this, HeaderPageRoutingModule);
      };

      HeaderPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], HeaderPageRoutingModule);
      /***/
    },

    /***/
    "./src/app/header/header.module.ts":
    /*!*****************************************!*\
      !*** ./src/app/header/header.module.ts ***!
      \*****************************************/

    /*! exports provided: HeaderPageModule */

    /***/
    function srcAppHeaderHeaderModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "HeaderPageModule", function () {
        return HeaderPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _header_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./header-routing.module */
      "./src/app/header/header-routing.module.ts");
      /* harmony import */


      var _header_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./header.page */
      "./src/app/header/header.page.ts");

      var HeaderPageModule = function HeaderPageModule() {
        _classCallCheck(this, HeaderPageModule);
      };

      HeaderPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _header_routing_module__WEBPACK_IMPORTED_MODULE_5__["HeaderPageRoutingModule"]],
        declarations: [_header_page__WEBPACK_IMPORTED_MODULE_6__["HeaderPage"]],
        exports: [_header_page__WEBPACK_IMPORTED_MODULE_6__["HeaderPage"]]
      })], HeaderPageModule);
      /***/
    },

    /***/
    "./src/app/header/header.page.scss":
    /*!*****************************************!*\
      !*** ./src/app/header/header.page.scss ***!
      \*****************************************/

    /*! exports provided: default */

    /***/
    function srcAppHeaderHeaderPageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = ".notification-bell-container {\n  position: relative;\n  display: flex;\n  flex-direction: row;\n  align-items: center;\n  justify-content: center;\n}\n.notification-bell-container .text-highlight {\n  position: absolute;\n  right: -3px;\n  font-size: 10px;\n  cursor: pointer;\n  top: -1px;\n  background: red;\n  border-radius: 50%;\n  z-index: 1111;\n  width: 18px;\n  height: 18px;\n  text-align: center;\n  line-height: 19px;\n  color: #ffffff;\n  font-weight: bold;\n}\n.notification-bell-container .bell-icon {\n  font-size: 30px;\n  position: relative;\n}\n.notification-bell-container .bell-icon:after {\n  content: \"\";\n  position: absolute;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvaGVhZGVyL2hlYWRlci5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxrQkFBQTtFQUNBLGFBQUE7RUFDQSxtQkFBQTtFQUNBLG1CQUFBO0VBQ0EsdUJBQUE7QUFDSjtBQUFJO0VBQ0Usa0JBQUE7RUFDQSxXQUFBO0VBQ0EsZUFBQTtFQUNBLGVBQUE7RUFDQSxTQUFBO0VBQ0EsZUFBQTtFQUNBLGtCQUFBO0VBQ0EsYUFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBQ0Esa0JBQUE7RUFDQSxpQkFBQTtFQUNBLGNBQUE7RUFDQSxpQkFBQTtBQUVOO0FBQUk7RUFDSSxlQUFBO0VBQ0Esa0JBQUE7QUFFUjtBQURRO0VBQ0UsV0FBQTtFQUNBLGtCQUFBO0FBR1YiLCJmaWxlIjoic3JjL2FwcC9oZWFkZXIvaGVhZGVyLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5ub3RpZmljYXRpb24tYmVsbC1jb250YWluZXJ7XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgZmxleC1kaXJlY3Rpb246IHJvdztcclxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgIC50ZXh0LWhpZ2hsaWdodHtcclxuICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICByaWdodDogLTNweDtcclxuICAgICAgZm9udC1zaXplOiAxMHB4O1xyXG4gICAgICBjdXJzb3I6IHBvaW50ZXI7XHJcbiAgICAgIHRvcDogLTFweDtcclxuICAgICAgYmFja2dyb3VuZDogcmVkO1xyXG4gICAgICBib3JkZXItcmFkaXVzOiA1MCU7XHJcbiAgICAgIHotaW5kZXg6IDExMTE7XHJcbiAgICAgIHdpZHRoOiAxOHB4O1xyXG4gICAgICBoZWlnaHQ6IDE4cHg7XHJcbiAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICAgbGluZS1oZWlnaHQ6IDE5cHg7XHJcbiAgICAgIGNvbG9yOiAjZmZmZmZmO1xyXG4gICAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgIH1cclxuICAgIC5iZWxsLWljb257XHJcbiAgICAgICAgZm9udC1zaXplOiAzMHB4O1xyXG4gICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgICAgICAmOmFmdGVye1xyXG4gICAgICAgICAgY29udGVudDogJyc7XHJcbiAgICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgXHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgfSJdfQ== */";
      /***/
    },

    /***/
    "./src/app/header/header.page.ts":
    /*!***************************************!*\
      !*** ./src/app/header/header.page.ts ***!
      \***************************************/

    /*! exports provided: HeaderPage */

    /***/
    function srcAppHeaderHeaderPageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "HeaderPage", function () {
        return HeaderPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _global_services_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ../global-services.service */
      "./src/app/global-services.service.ts");
      /* harmony import */


      var _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic-native/geolocation/ngx */
      "./node_modules/@ionic-native/geolocation/__ivy_ngcc__/ngx/index.js");
      /* harmony import */


      var _ionic_native_native_geocoder_ngx__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @ionic-native/native-geocoder/ngx */
      "./node_modules/@ionic-native/native-geocoder/__ivy_ngcc__/ngx/index.js");

      var HeaderPage = /*#__PURE__*/function () {
        function HeaderPage(menu, service, geolocation, nativeGeocoder, navController) {
          var _this = this;

          _classCallCheck(this, HeaderPage);

          this.menu = menu;
          this.service = service;
          this.geolocation = geolocation;
          this.nativeGeocoder = nativeGeocoder;
          this.navController = navController;
          this.notificationUrl = 'getNotificationText'; //=====================get Login Details=====================

          this.loggedDetails = {};
          this.city = localStorage.getItem('location'); // geocoder options

          this.nativeGeocoderOptions = {
            useLocale: true,
            maxResults: 5
          };
          this.userName = localStorage.getItem('username');
          this.subscription = this.service.getMessage().subscribe(function (message) {
            if (message.text == 'startNotificationCountFunction') {
              _this.getnotificationsList();
            }
          });
          this.service.invoke.subscribe(function (res) {
            if (res) {
              _this.cityChange(res);
            }
          });
        }

        _createClass(HeaderPage, [{
          key: "cityChange",
          value: function cityChange(city) {
            if (!this.city) {
              this.city = city;
              localStorage.setItem('location', this.city);
            }
          }
        }, {
          key: "ngOnInit",
          value: function ngOnInit() {
            this.userInfo(); // this.getUserPosition();

            this.service.checkGPSPermission();
          }
        }, {
          key: "openMenu",
          value: function openMenu() {
            // this.menu.enable(true, 'first');
            this.menu.open();
          }
        }, {
          key: "userInfo",
          value: function userInfo() {
            var _this2 = this;

            if (localStorage.getItem('username')) {
              var obj = {
                username: localStorage.getItem('username')
              };
              this.service.post_data("getAlumniOthersInfo", obj).subscribe(function (result) {
                var json = _this2.service.parserToJSON(result);

                var parsedJSON = JSON.parse(json.string);
                _this2.loggedDetails = parsedJSON[0];
              }, function (error) {
                console.log(error);
              });
            }
          }
        }, {
          key: "getUserPosition",
          value: function getUserPosition() {
            var _this3 = this;

            this.options = {
              enableHighAccuracy: false
            };
            this.geolocation.getCurrentPosition(this.options).then(function (pos) {
              _this3.currentPos = pos;
              _this3.latitude = pos.coords.latitude;
              _this3.longitude = pos.coords.longitude;

              _this3.getAddress(_this3.latitude, _this3.longitude);
            }, function (err) {
              console.log("error : " + err.message);
              ;
            });
          } // get address using coordinates

        }, {
          key: "getAddress",
          value: function getAddress(lat, _long) {
            var _this4 = this;

            this.nativeGeocoder.reverseGeocode(lat, _long, this.nativeGeocoderOptions).then(function (res) {
              var address = _this4.pretifyAddress(res[0]);

              var value = address.split(",");
              var count = value.length;
              _this4.city = value[count - 5];
            })["catch"](function (error) {
              alert('Error getting location' + error + JSON.stringify(error));
            });
          } // address

        }, {
          key: "pretifyAddress",
          value: function pretifyAddress(addressObj) {
            var obj = [];
            var address = "";

            for (var key in addressObj) {
              obj.push(addressObj[key]);
            }

            obj.reverse();

            for (var val in obj) {
              if (obj[val].length) address += obj[val] + ', ';
            }

            return address.slice(0, -2);
          }
        }, {
          key: "getnotificationsList",
          value: function getnotificationsList() {
            var _this5 = this;

            var obj = {
              msg_id: 0,
              username: this.userName,
              rows_in_page: 0,
              pageid: 1
            };
            this.service.post_data(this.notificationUrl, obj).subscribe(function (result) {
              var json = _this5.service.parserToJSON(result);

              var list = JSON.parse(json.string);

              if (list) {
                _this5.notificationUnreadCount = 0;

                if (list.Details.length > 0) {
                  list.Details.forEach(function (element, i) {
                    if (!JSON.parse(String(element.read_sts).toLowerCase())) {
                      _this5.notificationUnreadCount++;
                    }
                  });
                }
              }
            });
          }
        }, {
          key: "moveNotificationPage",
          value: function moveNotificationPage() {
            this.navController.navigateForward('/notification');
          }
        }]);

        return HeaderPage;
      }();

      HeaderPage.ctorParameters = function () {
        return [{
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["MenuController"]
        }, {
          type: _global_services_service__WEBPACK_IMPORTED_MODULE_3__["GlobalServicesService"]
        }, {
          type: _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_4__["Geolocation"]
        }, {
          type: _ionic_native_native_geocoder_ngx__WEBPACK_IMPORTED_MODULE_5__["NativeGeocoder"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"]
        }];
      };

      HeaderPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-header',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./header.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/header/header.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./header.page.scss */
        "./src/app/header/header.page.scss"))["default"]]
      })], HeaderPage);
      /***/
    },

    /***/
    "./src/app/home/home-routing.module.ts":
    /*!*********************************************!*\
      !*** ./src/app/home/home-routing.module.ts ***!
      \*********************************************/

    /*! exports provided: HomePageRoutingModule */

    /***/
    function srcAppHomeHomeRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "HomePageRoutingModule", function () {
        return HomePageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _home_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./home.page */
      "./src/app/home/home.page.ts");

      var routes = [{
        path: '',
        component: _home_page__WEBPACK_IMPORTED_MODULE_3__["HomePage"],
        children: [{
          path: 'search',
          children: [{
            path: '',
            loadChildren: function loadChildren() {
              return Promise.all(
              /*! import() | search-search-module */
              [__webpack_require__.e("default~category-selection-category-selection-module~search-search-module~trendspotter-trendspotter-~2a9b515e"), __webpack_require__.e("search-search-module")]).then(__webpack_require__.bind(null,
              /*! ../search/search.module */
              "./src/app/search/search.module.ts")).then(function (m) {
                return m.SearchPageModule;
              });
            }
          }]
        }]
      }];

      var HomePageRoutingModule = function HomePageRoutingModule() {
        _classCallCheck(this, HomePageRoutingModule);
      };

      HomePageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], HomePageRoutingModule);
      /***/
    },

    /***/
    "./src/app/home/home.module.ts":
    /*!*************************************!*\
      !*** ./src/app/home/home.module.ts ***!
      \*************************************/

    /*! exports provided: HomePageModule */

    /***/
    function srcAppHomeHomeModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "HomePageModule", function () {
        return HomePageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _home_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./home.page */
      "./src/app/home/home.page.ts");
      /* harmony import */


      var _ionic_storage__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! @ionic/storage */
      "./node_modules/@ionic/storage/__ivy_ngcc__/fesm2015/ionic-storage.js");
      /* harmony import */


      var _home_routing_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! ./home-routing.module */
      "./src/app/home/home-routing.module.ts");
      /* harmony import */


      var _header_header_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
      /*! ../header/header.module */
      "./src/app/header/header.module.ts");
      /* harmony import */


      var _advertisement_modalpop_advertisement_modalpop_module__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
      /*! ../advertisement-modalpop/advertisement-modalpop.module */
      "./src/app/advertisement-modalpop/advertisement-modalpop.module.ts");

      var HomePageModule = function HomePageModule() {
        _classCallCheck(this, HomePageModule);
      };

      HomePageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["IonicModule"], _home_routing_module__WEBPACK_IMPORTED_MODULE_7__["HomePageRoutingModule"], _ionic_storage__WEBPACK_IMPORTED_MODULE_6__["IonicStorageModule"], _header_header_module__WEBPACK_IMPORTED_MODULE_8__["HeaderPageModule"], _advertisement_modalpop_advertisement_modalpop_module__WEBPACK_IMPORTED_MODULE_9__["AdvertisementModalpopPageModule"]],
        declarations: [_home_page__WEBPACK_IMPORTED_MODULE_5__["HomePage"]]
      })], HomePageModule);
      /***/
    },

    /***/
    "./src/app/home/home.page.scss":
    /*!*************************************!*\
      !*** ./src/app/home/home.page.scss ***!
      \*************************************/

    /*! exports provided: default */

    /***/
    function srcAppHomeHomePageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "[font-small] {\n  font-weight: bold !important;\n  --border-radius: 0px;\n}\n\n[margin-1] {\n  margin: 2.25rem;\n}\n\n.icon {\n  display: flex;\n  font-size: 70px;\n  color: var(--ion-color-yellow);\n  vertical-align: middle;\n}\n\n[ion-col-height] {\n  width: 100%;\n  height: 150px;\n}\n\n[ion-col-height] ion-content {\n  --background: #212721;\n}\n\n[ion-col-height] ion-content ion-button {\n  font-size: 30px;\n  position: absolute;\n  left: 50%;\n  top: 50%;\n  transform: translate(-50%, -50%);\n}\n\n.img_size {\n  width: 100%;\n  -o-object-fit: cover;\n     object-fit: cover;\n  -o-object-position: center;\n     object-position: center;\n  background-color: transparent;\n  height: 150px;\n}\n\n[over] {\n  z-index: 1;\n}\n\n.video_tag {\n  width: 100%;\n  height: 100%;\n  -o-object-fit: cover;\n     object-fit: cover;\n  overflow: hidden;\n  position: absolute;\n  z-index: 0;\n}\n\n.overlay {\n  position: absolute;\n  bottom: 0;\n  left: 0;\n  z-index: 1;\n  height: 20%;\n}\n\n.overlay p {\n  margin-top: 0;\n  margin-bottom: 1rem;\n  background: var(--ion-color-yellow);\n  padding: 10px;\n  position: absolute;\n  bottom: 0;\n  text-overflow: ellipsis;\n  /* width: 128%; */\n}\n\n.inner-div {\n  position: absolute !important;\n  min-height: 95% !important;\n  display: block;\n}\n\n.border-round {\n  border-radius: 10px;\n}\n\n.btn-curve {\n  border-radius: 10px;\n  font-weight: bold !important;\n  --ion-color-contrast: #212721;\n}\n\n.videoicon {\n  color: white;\n  font-size: 60px;\n  position: absolute;\n  top: 50%;\n  left: 50%;\n  transform: translate(-50%, -50%);\n  -ms-transform: translate(-50%, -50%);\n  text-align: center;\n}\n\n.overlay {\n  position: absolute;\n  top: 0;\n  bottom: 0;\n  left: 0;\n  right: 0;\n  height: 100%;\n  width: 100%;\n  opacity: 0;\n  transition: 0.3s ease;\n  opacity: 1;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvaG9tZS9ob21lLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUVJLDRCQUFBO0VBQ0Esb0JBQUE7QUFBSjs7QUFHQTtFQUNJLGVBQUE7QUFBSjs7QUFHQTtFQUNJLGFBQUE7RUFDQSxlQUFBO0VBQ0EsOEJBQUE7RUFDQSxzQkFBQTtBQUFKOztBQUdBO0VBQ0ksV0FBQTtFQUNBLGFBQUE7QUFBSjs7QUFFSTtFQUNJLHFCQUFBO0FBQVI7O0FBQ1E7RUFDSSxlQUFBO0VBQ0Esa0JBQUE7RUFDQSxTQUFBO0VBQ0EsUUFBQTtFQUNBLGdDQUFBO0FBQ1o7O0FBSUE7RUFDSSxXQUFBO0VBQ0Esb0JBQUE7S0FBQSxpQkFBQTtFQUNBLDBCQUFBO0tBQUEsdUJBQUE7RUFDQSw2QkFBQTtFQUNBLGFBQUE7QUFESjs7QUFLQTtFQUNJLFVBQUE7QUFGSjs7QUFLQTtFQUNJLFdBQUE7RUFDQSxZQUFBO0VBQ0Esb0JBQUE7S0FBQSxpQkFBQTtFQUNBLGdCQUFBO0VBQ0Esa0JBQUE7RUFDQSxVQUFBO0FBRko7O0FBTUE7RUFDSSxrQkFBQTtFQUNBLFNBQUE7RUFDQSxPQUFBO0VBQ0EsVUFBQTtFQUNBLFdBQUE7QUFISjs7QUFJSTtFQUNJLGFBQUE7RUFDQSxtQkFBQTtFQUNBLG1DQUFBO0VBQ0EsYUFBQTtFQUNBLGtCQUFBO0VBQ0EsU0FBQTtFQUNBLHVCQUFBO0VBQ0EsaUJBQUE7QUFGUjs7QUFNQTtFQUNJLDZCQUFBO0VBQ0EsMEJBQUE7RUFDQSxjQUFBO0FBSEo7O0FBWUE7RUFDSSxtQkFBQTtBQVRKOztBQVdBO0VBQ0ksbUJBQUE7RUFDQSw0QkFBQTtFQUNBLDZCQUFBO0FBUko7O0FBV0E7RUFJSSxZQUFBO0VBQ0EsZUFBQTtFQUNBLGtCQUFBO0VBQ0EsUUFBQTtFQUNBLFNBQUE7RUFDQSxnQ0FBQTtFQUNBLG9DQUFBO0VBQ0Esa0JBQUE7QUFYSjs7QUFhSTtFQUNBLGtCQUFBO0VBQ0EsTUFBQTtFQUNBLFNBQUE7RUFDQSxPQUFBO0VBQ0EsUUFBQTtFQUNBLFlBQUE7RUFDQSxXQUFBO0VBQ0EsVUFBQTtFQUNBLHFCQUFBO0VBQ0EsVUFBQTtBQVZKIiwiZmlsZSI6InNyYy9hcHAvaG9tZS9ob21lLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIltmb250LXNtYWxsXSB7XHJcbiAgICAvLyBmb250LXNpemU6IDEycHghaW1wb3J0YW50O1xyXG4gICAgZm9udC13ZWlnaHQ6IGJvbGQgIWltcG9ydGFudDtcclxuICAgIC0tYm9yZGVyLXJhZGl1czogMHB4O1xyXG59XHJcblxyXG5bbWFyZ2luLTFdIHtcclxuICAgIG1hcmdpbjogMi4yNXJlbTtcclxufVxyXG5cclxuLmljb24ge1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGZvbnQtc2l6ZTogNzBweDtcclxuICAgIGNvbG9yOiB2YXIoLS1pb24tY29sb3IteWVsbG93KTsgLy8jZmZDOTc3O1xyXG4gICAgdmVydGljYWwtYWxpZ246IG1pZGRsZTtcclxufVxyXG5cclxuW2lvbi1jb2wtaGVpZ2h0XSB7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGhlaWdodDogMTUwcHg7XHJcbiAgICAvLyBiYWNrZ3JvdW5kOiAjZmZmZmZmO1xyXG4gICAgaW9uLWNvbnRlbnQge1xyXG4gICAgICAgIC0tYmFja2dyb3VuZDogIzIxMjcyMTtcclxuICAgICAgICBpb24tYnV0dG9uIHtcclxuICAgICAgICAgICAgZm9udC1zaXplOiAzMHB4O1xyXG4gICAgICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgICAgICAgIGxlZnQ6IDUwJTtcclxuICAgICAgICAgICAgdG9wOiA1MCU7XHJcbiAgICAgICAgICAgIHRyYW5zZm9ybTogdHJhbnNsYXRlKC01MCUsIC01MCUpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxufVxyXG5cclxuLmltZ19zaXplIHtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgb2JqZWN0LWZpdDogY292ZXI7XHJcbiAgICBvYmplY3QtcG9zaXRpb246IGNlbnRlcjtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50O1xyXG4gICAgaGVpZ2h0OiAxNTBweDtcclxuICAgIC8vIGJvcmRlci1yYWRpdXM6IDEwcHg7XHJcbn1cclxuXHJcbltvdmVyXSB7XHJcbiAgICB6LWluZGV4OiAxO1xyXG59XHJcblxyXG4udmlkZW9fdGFnIHtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgb2JqZWN0LWZpdDogY292ZXI7XHJcbiAgICBvdmVyZmxvdzogaGlkZGVuO1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgei1pbmRleDogMDtcclxuICAgIC8vIGJvcmRlci1yYWRpdXM6IDEwcHg7XHJcbn1cclxuXHJcbi5vdmVybGF5IHtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIGJvdHRvbTogMDtcclxuICAgIGxlZnQ6IDA7XHJcbiAgICB6LWluZGV4OiAxO1xyXG4gICAgaGVpZ2h0OiAyMCU7XHJcbiAgICBwIHtcclxuICAgICAgICBtYXJnaW4tdG9wOiAwO1xyXG4gICAgICAgIG1hcmdpbi1ib3R0b206IDFyZW07XHJcbiAgICAgICAgYmFja2dyb3VuZDogdmFyKC0taW9uLWNvbG9yLXllbGxvdyk7IC8vI2Y2Yzc2MjtcclxuICAgICAgICBwYWRkaW5nOiAxMHB4O1xyXG4gICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgICAgICBib3R0b206IDA7XHJcbiAgICAgICAgdGV4dC1vdmVyZmxvdzogZWxsaXBzaXM7XHJcbiAgICAgICAgLyogd2lkdGg6IDEyOCU7ICovXHJcbiAgICB9XHJcbn1cclxuXHJcbi5pbm5lci1kaXYge1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlIWltcG9ydGFudDtcclxuICAgIG1pbi1oZWlnaHQ6IDk1JSFpbXBvcnRhbnQ7XHJcbiAgICBkaXNwbGF5OiBibG9jaztcclxufVxyXG5cclxuLy8gLnZpZGVvaWNvbntcclxuLy8gICAgIGZvbnQtc2l6ZTogNjBweDtcclxuLy8gICAgIGNvbG9yOiB3aGl0ZTtcclxuLy8gICAgIG1hcmdpbjogMjUlIDI1JTtcclxuLy8gfVxyXG5cclxuLmJvcmRlci1yb3VuZCB7XHJcbiAgICBib3JkZXItcmFkaXVzOiAxMHB4O1xyXG59XHJcbi5idG4tY3VydmV7XHJcbiAgICBib3JkZXItcmFkaXVzOiAxMHB4O1xyXG4gICAgZm9udC13ZWlnaHQ6IGJvbGQgIWltcG9ydGFudDtcclxuICAgIC0taW9uLWNvbG9yLWNvbnRyYXN0OiAjMjEyNzIxO1xyXG59XHJcblxyXG4udmlkZW9pY29ue1xyXG4gICAgLy8gZm9udC1zaXplOiA2MHB4O1xyXG4gICAgLy8gY29sb3I6IHdoaXRlO1xyXG4gICAgLy8gbWFyZ2luOiAyNSUgMjUlO1xyXG4gICAgY29sb3I6IHdoaXRlO1xyXG4gICAgZm9udC1zaXplOiA2MHB4O1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgdG9wOiA1MCU7XHJcbiAgICBsZWZ0OiA1MCU7XHJcbiAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZSgtNTAlLCAtNTAlKTtcclxuICAgIC1tcy10cmFuc2Zvcm06IHRyYW5zbGF0ZSgtNTAlLCAtNTAlKTtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIH1cclxuICAgIC5vdmVybGF5IHtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIHRvcDogMDtcclxuICAgIGJvdHRvbTogMDtcclxuICAgIGxlZnQ6IDA7XHJcbiAgICByaWdodDogMDtcclxuICAgIGhlaWdodDogMTAwJTtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgb3BhY2l0eTogMDtcclxuICAgIHRyYW5zaXRpb246IC4zcyBlYXNlO1xyXG4gICAgb3BhY2l0eTogMTtcclxuICAgIH0iXX0= */";
      /***/
    },

    /***/
    "./src/app/home/home.page.ts":
    /*!***********************************!*\
      !*** ./src/app/home/home.page.ts ***!
      \***********************************/

    /*! exports provided: HomePage */

    /***/
    function srcAppHomeHomePageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "HomePage", function () {
        return HomePage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _global_services_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ../global-services.service */
      "./src/app/global-services.service.ts");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @ionic-native/camera/ngx */
      "./node_modules/@ionic-native/camera/__ivy_ngcc__/ngx/index.js");
      /* harmony import */


      var _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! @ionic-native/geolocation/ngx */
      "./node_modules/@ionic-native/geolocation/__ivy_ngcc__/ngx/index.js");
      /* harmony import */


      var _ionic_native_media_capture_ngx__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! @ionic-native/media-capture/ngx */
      "./node_modules/@ionic-native/media-capture/__ivy_ngcc__/ngx/index.js");
      /* harmony import */


      var _ionic_native_file_ngx__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
      /*! @ionic-native/file/ngx */
      "./node_modules/@ionic-native/file/__ivy_ngcc__/ngx/index.js");
      /* harmony import */


      var _ionic_storage__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
      /*! @ionic/storage */
      "./node_modules/@ionic/storage/__ivy_ngcc__/fesm2015/ionic-storage.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _ionic_native_native_geocoder_ngx__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(
      /*! @ionic-native/native-geocoder/ngx */
      "./node_modules/@ionic-native/native-geocoder/__ivy_ngcc__/ngx/index.js");
      /* harmony import */


      var _ionic_native_ionic_webview_ngx__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(
      /*! @ionic-native/ionic-webview/ngx */
      "./node_modules/@ionic-native/ionic-webview/__ivy_ngcc__/ngx/index.js");
      /* harmony import */


      var _advertisement_modalpop_advertisement_modalpop_page__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(
      /*! ../advertisement-modalpop/advertisement-modalpop.page */
      "./src/app/advertisement-modalpop/advertisement-modalpop.page.ts"); // declare var CordovaExif: any;


      var HomePage = /*#__PURE__*/function () {
        function HomePage(storage, mediaCapture, file, actionSheetController, alertController, service, menu, navCtrl, camera, geolocation, route, router, nativeGeocoder, webview, modalController, datepipe) {
          _classCallCheck(this, HomePage);

          this.storage = storage;
          this.mediaCapture = mediaCapture;
          this.file = file;
          this.actionSheetController = actionSheetController;
          this.alertController = alertController;
          this.service = service;
          this.menu = menu;
          this.navCtrl = navCtrl;
          this.camera = camera;
          this.geolocation = geolocation;
          this.route = route;
          this.router = router;
          this.nativeGeocoder = nativeGeocoder;
          this.webview = webview;
          this.modalController = modalController;
          this.datepipe = datepipe;
          this.originalImage = null;
          this.fileInfo = {};
          this.notificationUrl = "getNotificationImage";
          this.locationTraces = [];
          this.latitude = 0; //latitude

          this.longitude = 0; //longitude

          this.imageMaxWidth = 720;
          this.imageMaxheight = 1024;
          this.cameraOptions = {
            destinationType: this.camera.DestinationType.FILE_URI,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE,
            // allowEdit: true,
            correctOrientation: true,
            sourceType: this.camera.PictureSourceType.CAMERA,
            quality: 50
          };
          this.commentsInput = "";
          this.posterImage = this.service.checkDomSanitizerForIOS('../../assets/image/play_overlay2.png');
          this.userName = localStorage.getItem('username');
        }

        _createClass(HomePage, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            this.getnotificationDetails();
          }
        }, {
          key: "ionViewDidEnter",
          value: function ionViewDidEnter() {}
        }, {
          key: "ionViewWillEnter",
          value: function ionViewWillEnter() {
            this.service.sendMessage('startNotificationCountFunction');
            this.getRecentlyPostedList();
          } // =====================Actionsheet for mode of camera=====================

        }, {
          key: "presentActionSheet",
          value: function presentActionSheet() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
              var _this6 = this;

              var actionSheet;
              return regeneratorRuntime.wrap(function _callee$(_context) {
                while (1) {
                  switch (_context.prev = _context.next) {
                    case 0:
                      _context.next = 2;
                      return this.actionSheetController.create({
                        header: 'Camera Mode',
                        cssClass: 'my-custom-class',
                        buttons: [{
                          text: 'Camera',
                          role: 'camera',
                          icon: 'camera-outline',
                          handler: function handler() {
                            _this6.takeSnap(); // this.router.navigate(['/category-selection', { 'location': 'location', 'tempPath': '' , 'type': 'image', 'page':'home' }]);
                            //  this.router.navigate(['/preview-filter', { 'location': 'location', 'tempPath': '' , 'type': 'image' }]);

                          }
                        }, {
                          text: 'Video',
                          icon: 'videocam-outline',
                          handler: function handler() {
                            _this6.captureVideo();
                          }
                        }]
                      });

                    case 2:
                      actionSheet = _context.sent;
                      _context.next = 5;
                      return actionSheet.present();

                    case 5:
                    case "end":
                      return _context.stop();
                  }
                }
              }, _callee, this);
            }));
          } //=====================Video Capture=====================

        }, {
          key: "captureVideo",
          value: function captureVideo() {
            var _this7 = this;

            var options = {
              limit: 1,
              duration: 30,
              quality: 50
            };
            this.mediaCapture.captureVideo(options).then(function (res) {
              var capturedFile = res[0];
              _this7.fileInfo = {};
              capturedFile.getFormatData(function (data) {
                _this7.fileInfo.resolution = data.width + 'x' + data.height + 'px';
                _this7.fileInfo.duration = data.duration;
                _this7.fileInfo.bitrate = data.bitrate;
                _this7.fileInfo.codecs = data.codecs;
              }, function (error) {
                console.log(error, 'getFormatData');
              });
              _this7.fileInfo.fileName = capturedFile.name;
              _this7.fileInfo.time = _this7.datepipe.transform(new Date(capturedFile.lastModifiedDate), 'yyyy/MM/dd hh:mm:ss a');
              _this7.fileInfo.size = Math.round(capturedFile.size / 1024 / 1024 * 100) / 100 + 'MB';
              _this7.fileInfo.type = capturedFile.type;

              if (capturedFile.fullPath.indexOf("file://") !== -1) {
                _this7.moveToPreview(_this7.address, capturedFile.fullPath, 'video');
              } else {
                _this7.moveToPreview(_this7.address, "file://" + capturedFile.fullPath, 'video');
              }
            }, function (err) {
              return console.error(err);
            });
          } //=====================Capture Image=====================

        }, {
          key: "takeSnap",
          value: function takeSnap() {
            var _this8 = this;

            this.camera.getPicture(this.cameraOptions).then(function (imageData) {
              _this8.fileInfo = {};

              _this8.file.resolveLocalFilesystemUrl(imageData).then(function (fileEntry) {
                fileEntry.getMetadata(function (metadata) {
                  var fileSize = metadata.size / 1024 / 1024;
                  _this8.fileInfo.fileName = fileEntry.name;
                  _this8.fileInfo.time = _this8.datepipe.transform(new Date(metadata.modificationTime), 'yyyy/MM/dd hh:mm:ss a');
                  _this8.fileInfo.size = Math.round(fileSize * 100) / 100 + 'MB';
                });
              }); //   if(CordovaExif){
              //   CordovaExif.readData(imageData, (res) => {
              //     this.fileInfo.resolution = res.ImageWidth + 'x' + res.ImageHeight + 'px';
              //     this.fileInfo.device = res.Make;
              //     this.fileInfo.model = res.Model;
              //     this.fileInfo.flash = res.Flash == 'Flash fired, compulsory flash mode' ? 'Flash Fired' : 'No Flash';
              //     this.fileInfo.iso = res.ISOSpeedRatings;
              //     this.fileInfo.whiteBalance = res.WhiteBalance;
              //     this.fileInfo.orientation = res.Orientation == 1 ? 'Portrait' : 'Landscape';
              //     this.fileInfo.latitude = res.GPSLatitude ? res.GPSLatitude : null;
              //     this.fileInfo.longitude = res.GPSLongitude ? res.GPSLongitude : null;
              //     // this.fileInfo.time = res.DateTime;
              //   })
              // }


              if (imageData.indexOf("file://") !== -1) {
                _this8.imagePreview = _this8.webview.convertFileSrc(imageData);

                _this8.moveToPreview(_this8.address, imageData, 'image');
              } else {
                _this8.imagePreview = _this8.webview.convertFileSrc("file://" + imageData);

                _this8.moveToPreview(_this8.address, "file://" + imageData, 'image');
              }
            }, function (error) {
              console.log(error);
            });
          } //=====================Move to Preview And Filter Page=====================

        }, {
          key: "moveToPreview",
          value: function moveToPreview(location, tempPath, type) {
            var _this9 = this;

            setTimeout(function () {
              if (tempPath) {
                if (localStorage.getItem('originalImg') != undefined) {
                  localStorage.removeItem('originalImg');
                }

                if (localStorage.getItem('editedFilters') != undefined) {
                  localStorage.removeItem('editedFilters');
                }

                if (localStorage.getItem('videoFileSize') != undefined) {
                  localStorage.removeItem('videoFileSize');
                }

                if (localStorage.removeItem('fileSize') != undefined) {
                  localStorage.removeItem('fileSize');
                }

                if (type == 'video') {
                  _this9.router.navigate(['/preview-filter', {
                    'fileinfo': JSON.stringify(_this9.fileInfo),
                    'location': location,
                    'latitude': _this9.latitude,
                    'longitude': _this9.longitude,
                    'tempPath': tempPath,
                    'type': type,
                    'page': 'home'
                  }]);
                } else {
                  _this9.service.changeMessage(tempPath);

                  _this9.router.navigate(['/preview-filter', {
                    'fileinfo': JSON.stringify(_this9.fileInfo),
                    'location': location,
                    'latitude': _this9.latitude,
                    'longitude': _this9.longitude,
                    'tempPath': tempPath,
                    'type': type,
                    'page': 'home'
                  }]);
                }
              } else {
                _this9.presentAlert();
              }
            }, 1000);
          }
        }, {
          key: "presentAlert3",
          value: function presentAlert3() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
              var alert;
              return regeneratorRuntime.wrap(function _callee2$(_context2) {
                while (1) {
                  switch (_context2.prev = _context2.next) {
                    case 0:
                      _context2.next = 2;
                      return this.alertController.create({
                        cssClass: 'my-custom-class',
                        header: 'Error',
                        subHeader: 'Image size Invalid',
                        message: 'file too big, please select less than 8 MB',
                        buttons: ['OK']
                      });

                    case 2:
                      alert = _context2.sent;
                      _context2.next = 5;
                      return alert.present();

                    case 5:
                    case "end":
                      return _context2.stop();
                  }
                }
              }, _callee2, this);
            }));
          }
        }, {
          key: "presentAlert4",
          value: function presentAlert4() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee3() {
              var alert;
              return regeneratorRuntime.wrap(function _callee3$(_context3) {
                while (1) {
                  switch (_context3.prev = _context3.next) {
                    case 0:
                      _context3.next = 2;
                      return this.alertController.create({
                        cssClass: 'my-custom-class',
                        header: 'Error',
                        subHeader: 'Video size Invalid',
                        message: 'file too big, please select less than 15 MB',
                        buttons: ['OK']
                      });

                    case 2:
                      alert = _context3.sent;
                      _context3.next = 5;
                      return alert.present();

                    case 5:
                    case "end":
                      return _context3.stop();
                  }
                }
              }, _callee3, this);
            }));
          } //=====================Open Menu=====================

        }, {
          key: "openMenu",
          value: function openMenu() {
            this.menu.open();
          } //=====================Validate File Empty=====================

        }, {
          key: "presentAlert",
          value: function presentAlert() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee4() {
              var alert;
              return regeneratorRuntime.wrap(function _callee4$(_context4) {
                while (1) {
                  switch (_context4.prev = _context4.next) {
                    case 0:
                      _context4.next = 2;
                      return this.alertController.create({
                        cssClass: 'my-custom-class',
                        header: 'Error',
                        message: 'Please Select the File',
                        backdropDismiss: false,
                        buttons: [{
                          text: 'OK',
                          cssClass: 'warning'
                        }]
                      });

                    case 2:
                      alert = _context4.sent;
                      _context4.next = 5;
                      return alert.present();

                    case 5:
                    case "end":
                      return _context4.stop();
                  }
                }
              }, _callee4, this);
            }));
          }
        }, {
          key: "presentAlert2",
          value: function presentAlert2() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee5() {
              var alert;
              return regeneratorRuntime.wrap(function _callee5$(_context5) {
                while (1) {
                  switch (_context5.prev = _context5.next) {
                    case 0:
                      _context5.next = 2;
                      return this.alertController.create({
                        cssClass: 'my-custom-class',
                        header: 'Error',
                        subHeader: 'Image resolution size Invalid',
                        message: 'please select the image resolution 200DPI size of 1280px X 720px',
                        buttons: ['OK']
                      });

                    case 2:
                      alert = _context5.sent;
                      _context5.next = 5;
                      return alert.present();

                    case 5:
                    case "end":
                      return _context5.stop();
                  }
                }
              }, _callee5, this);
            }));
          }
        }, {
          key: "userInfo",
          value: function userInfo() {
            var _this10 = this;

            if (localStorage.getItem('username')) {
              var obj = {
                username: localStorage.getItem('username')
              };
              this.service.post_data("getAlumniOthersInfo", obj).subscribe(function (result) {
                var json = _this10.service.parserToJSON(result);

                var parsedJSON = JSON.parse(json.string);

                if (parsedJSON[0].Status == "SUCCESS") {
                  _this10.loggedDetails = parsedJSON[0];
                }
              }, function (error) {
                console.log(error);
              });
            }
          }
        }, {
          key: "getRecentlyPostedList",
          value: function getRecentlyPostedList() {
            var _this11 = this;

            if (localStorage.getItem('username')) {
              var obj = {
                username: localStorage.getItem('username'),
                pageid: 1,
                rows_in_page: 6,
                image_video: 'ALL',
                image_category: 'ALL'
              };
              this.service.post_data("getTrendSpotterImagesVideosRecentWithPaging", obj).subscribe(function (result) {
                var json = _this11.service.parserToJSON(result);

                var jons1 = JSON.stringify(json);
                var json2 = jons1.replace(/\\n/g, '');
                var parsedJSON = JSON.parse(JSON.parse(json2).string);

                if (parsedJSON.Total_Records > 0) {
                  _this11.postedList = parsedJSON.Details;
                }
              }, function (error) {
                console.log(error);
              });
            }
          }
        }, {
          key: "gotoInnerPage",
          value: function gotoInnerPage(data) {
            this.router.navigate(['/media-info', {
              'data': JSON.stringify(data),
              'page': 'home'
            }]);
          }
        }, {
          key: "getnotificationDetails",
          value: function getnotificationDetails() {
            var _this12 = this;

            var obj = {
              msg_id: 0,
              username: this.userName
            };
            this.service.post_data(this.notificationUrl, obj).subscribe(function (result) {
              var json = _this12.service.parserToJSON(result);

              if (JSON.parse(json.string).length > 0) {
                if (JSON.parse(json.string)[0].img_url) {
                  localStorage.setItem('notification_img_url', JSON.parse(json.string)[0].img_url);

                  _this12.modalpop();
                }
              }
            });
          }
        }, {
          key: "modalpop",
          value: function modalpop() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee6() {
              var modal;
              return regeneratorRuntime.wrap(function _callee6$(_context6) {
                while (1) {
                  switch (_context6.prev = _context6.next) {
                    case 0:
                      _context6.next = 2;
                      return this.modalController.create({
                        component: _advertisement_modalpop_advertisement_modalpop_page__WEBPACK_IMPORTED_MODULE_13__["AdvertisementModalpopPage"],
                        cssClass: 'my-custom-modal-class',
                        backdropDismiss: false
                      });

                    case 2:
                      modal = _context6.sent;
                      _context6.next = 5;
                      return modal.present();

                    case 5:
                      return _context6.abrupt("return", _context6.sent);

                    case 6:
                    case "end":
                      return _context6.stop();
                  }
                }
              }, _callee6, this);
            }));
          }
        }]);

        return HomePage;
      }();

      HomePage.ctorParameters = function () {
        return [{
          type: _ionic_storage__WEBPACK_IMPORTED_MODULE_9__["Storage"]
        }, {
          type: _ionic_native_media_capture_ngx__WEBPACK_IMPORTED_MODULE_7__["MediaCapture"]
        }, {
          type: _ionic_native_file_ngx__WEBPACK_IMPORTED_MODULE_8__["File"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ActionSheetController"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["AlertController"]
        }, {
          type: _global_services_service__WEBPACK_IMPORTED_MODULE_3__["GlobalServicesService"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["MenuController"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["NavController"]
        }, {
          type: _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_5__["Camera"]
        }, {
          type: _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_6__["Geolocation"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_10__["ActivatedRoute"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_10__["Router"]
        }, {
          type: _ionic_native_native_geocoder_ngx__WEBPACK_IMPORTED_MODULE_11__["NativeGeocoder"]
        }, {
          type: _ionic_native_ionic_webview_ngx__WEBPACK_IMPORTED_MODULE_12__["WebView"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ModalController"]
        }, {
          type: _angular_common__WEBPACK_IMPORTED_MODULE_2__["DatePipe"]
        }];
      };

      HomePage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-home',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./home.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/home/home.page.html"))["default"],
        providers: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["DatePipe"]],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./home.page.scss */
        "./src/app/home/home.page.scss"))["default"]]
      })], HomePage);
      /***/
    }
  }]);
})();
//# sourceMappingURL=home-home-module-es5.js.map