(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["trendspotter-trendspotter-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/trendspotter/trendspotter.page.html":
    /*!*******************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/trendspotter/trendspotter.page.html ***!
      \*******************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppTrendspotterTrendspotterPageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<app-header>\r\n\r\n</app-header>\r\n\r\n<ion-content>\r\n\r\n    <div class=\"inner-div\">\r\n\r\n        <ion-grid>\r\n            <ion-row class=\"ion-text-center mt-3 mb-2\">\r\n                <ion-col>\r\n                    <h5 header-text>Trend Spotters</h5>\r\n                </ion-col>\r\n            </ion-row>\r\n        </ion-grid>\r\n        <ion-grid search-box *ngIf=\"isEnableSearch ==  true\">\r\n            <ion-row>\r\n                <ion-col>\r\n                    <ion-icon color=\"light\" size=\"large\" (click)=\"enableSearch(false)\" name=\"close-circle-outline\">\r\n                    </ion-icon>\r\n                </ion-col>\r\n            </ion-row>\r\n            <ion-row>\r\n                <ion-col class=\"d-block\">\r\n                </ion-col>\r\n            </ion-row>\r\n            <ion-row>\r\n                <ion-col>\r\n\r\n                    <ion-searchbar color=\"light\" showCancelButton=\"focus\" [(ngModel)]='city_or_studname'\r\n                        cancelButtonText=\"Custom Cancel\" type=\"text\" debounce=\"500\" (ionChange)=\"searchList()\">\r\n                    </ion-searchbar>\r\n                </ion-col>\r\n            </ion-row>\r\n        </ion-grid>\r\n        <ion-grid>\r\n            <ion-row *ngIf=\"trendSpotterList.length == 0\">\r\n                <ion-col>\r\n                    <ion-card class=\"ion-margin-vertical\">\r\n                        <ion-card-content>\r\n                            No Record Found\r\n                        </ion-card-content>\r\n                    </ion-card>\r\n                </ion-col>\r\n            </ion-row>\r\n            <ion-row class=\"ts-list\" *ngIf=\"trendSpotterList.length > 0\">\r\n                <ion-col size=6 class=\"w-100\" *ngFor=\"let spotter of trendSpotterList\">\r\n                    <ion-avatar>\r\n                        <img [src]=\"spotter.upload_image\" height=\"100%\" width=\"100%\" *ngIf=\"spotter.upload_image\">\r\n                        <img src=\"../../assets//image/no_user_yellow.png\" height=\"100%\" width=\"100%\"\r\n                            *ngIf=\"!spotter.upload_image\">\r\n                    </ion-avatar>\r\n                    <ion-label f-14 class=\"over-flow-text ion-text-capitalize ion-text-center\" color=\"white\">\r\n                        {{spotter.stud_name}}\r\n                    </ion-label>\r\n                    <ion-label f-10 class=\"over-flow-text ion-text-capitalize ion-text-center\" color=\"white\">\r\n                        {{spotter.city}}\r\n                    </ion-label>\r\n                </ion-col>\r\n            </ion-row>\r\n            <!-- <ion-infinite-scroll [disabled]=\"Scroll\" (ionInfinite)=\"loadData()\">\r\n                <ion-infinite-scroll-content class=\"loadingspinner\" loadingSpinner=\"bubbles\"\r\n                    loadingText=\"Loading more data...\">\r\n                </ion-infinite-scroll-content>\r\n            </ion-infinite-scroll> -->\r\n        </ion-grid>\r\n        <ion-grid *ngIf=\"loadMoreBtn\">\r\n            <ion-row class=\"ion-justify-content-center\">\r\n                <ion-col>\r\n                    <div class=\"ion-text-center\">\r\n                        <ion-button color=\"yellow btn-curve\" size=\"small\" btn-yellow class=\"mt-10\" (click)=\"loadMore()\">\r\n                            Load\r\n                            More &nbsp;<ion-icon name=\"arrow-down-outline\"></ion-icon>\r\n                        </ion-button>\r\n                    </div>\r\n                </ion-col>\r\n            </ion-row>\r\n        </ion-grid>\r\n    </div>\r\n</ion-content>\r\n<ion-toolbar *ngIf=\"showToolbar == true\" color=\"yellow\" slot=\"bottom\">\r\n    <ion-tabs>\r\n        <ion-tab-bar color=\"yellow\" slot=\"bottom\">\r\n            <ion-tab-button [routerLink]=\"['/home']\">\r\n                <ion-icon name=\"caret-back-outline\"></ion-icon>\r\n                <ion-label>Back</ion-label>\r\n            </ion-tab-button>\r\n            <ion-tab-button (click)=\"enableSearch(true)\">\r\n                <ion-icon name=\"search-outline\"></ion-icon>\r\n                <ion-label>Search</ion-label>\r\n            </ion-tab-button>\r\n        </ion-tab-bar>\r\n    </ion-tabs>\r\n</ion-toolbar>";
      /***/
    },

    /***/
    "./src/app/trendspotter/trendspotter-routing.module.ts":
    /*!*************************************************************!*\
      !*** ./src/app/trendspotter/trendspotter-routing.module.ts ***!
      \*************************************************************/

    /*! exports provided: TrendspotterPageRoutingModule */

    /***/
    function srcAppTrendspotterTrendspotterRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "TrendspotterPageRoutingModule", function () {
        return TrendspotterPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _trendspotter_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./trendspotter.page */
      "./src/app/trendspotter/trendspotter.page.ts");

      var routes = [{
        path: '',
        component: _trendspotter_page__WEBPACK_IMPORTED_MODULE_3__["TrendspotterPage"]
      }];

      var TrendspotterPageRoutingModule = function TrendspotterPageRoutingModule() {
        _classCallCheck(this, TrendspotterPageRoutingModule);
      };

      TrendspotterPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], TrendspotterPageRoutingModule);
      /***/
    },

    /***/
    "./src/app/trendspotter/trendspotter.module.ts":
    /*!*****************************************************!*\
      !*** ./src/app/trendspotter/trendspotter.module.ts ***!
      \*****************************************************/

    /*! exports provided: TrendspotterPageModule */

    /***/
    function srcAppTrendspotterTrendspotterModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "TrendspotterPageModule", function () {
        return TrendspotterPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _trendspotter_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./trendspotter-routing.module */
      "./src/app/trendspotter/trendspotter-routing.module.ts");
      /* harmony import */


      var _trendspotter_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./trendspotter.page */
      "./src/app/trendspotter/trendspotter.page.ts");
      /* harmony import */


      var _header_header_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! ../header/header.module */
      "./src/app/header/header.module.ts");

      var TrendspotterPageModule = function TrendspotterPageModule() {
        _classCallCheck(this, TrendspotterPageModule);
      };

      TrendspotterPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _trendspotter_routing_module__WEBPACK_IMPORTED_MODULE_5__["TrendspotterPageRoutingModule"], _header_header_module__WEBPACK_IMPORTED_MODULE_7__["HeaderPageModule"]],
        declarations: [_trendspotter_page__WEBPACK_IMPORTED_MODULE_6__["TrendspotterPage"]]
      })], TrendspotterPageModule);
      /***/
    },

    /***/
    "./src/app/trendspotter/trendspotter.page.scss":
    /*!*****************************************************!*\
      !*** ./src/app/trendspotter/trendspotter.page.scss ***!
      \*****************************************************/

    /*! exports provided: default */

    /***/
    function srcAppTrendspotterTrendspotterPageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "ion-col {\n  display: flex;\n  flex-flow: column;\n  align-items: center;\n  padding: 5px;\n}\n\n[f-14] {\n  font-size: 15px !important;\n}\n\n[f-10] {\n  font-size: 13px !important;\n}\n\n.bg-list {\n  background-color: transparent;\n}\n\n[search-box] {\n  background: #ffffff1f;\n  padding: 10px;\n  margin: 10px;\n  border: 1px solid #ffffff3b;\n}\n\n[search-box] ion-list {\n  margin: -5px 0;\n  width: 94%;\n  padding: 0;\n}\n\nion-slide {\n  width: 45% !important;\n}\n\n.ts-list ion-label {\n  white-space: nowrap;\n  overflow: hidden;\n  text-overflow: ellipsis;\n}\n\n.btn-curve {\n  border-radius: 10px;\n  --border-radius: 10px;\n  --ion-color-contrast: #212721;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvdHJlbmRzcG90dGVyL3RyZW5kc3BvdHRlci5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxhQUFBO0VBQ0EsaUJBQUE7RUFDQSxtQkFBQTtFQUNBLFlBQUE7QUFDSjs7QUFFQTtFQUNJLDBCQUFBO0FBQ0o7O0FBRUE7RUFDSSwwQkFBQTtBQUNKOztBQUVBO0VBQ0ksNkJBQUE7QUFDSjs7QUFFQTtFQUNJLHFCQUFBO0VBQ0EsYUFBQTtFQUNBLFlBQUE7RUFDQSwyQkFBQTtBQUNKOztBQUFJO0VBQ0EsY0FBQTtFQUNBLFVBQUE7RUFDQSxVQUFBO0FBRUo7O0FBS0E7RUFFSSxxQkFBQTtBQUhKOztBQU9RO0VBQ0ksbUJBQUE7RUFDQSxnQkFBQTtFQUNBLHVCQUFBO0FBSlo7O0FBU0k7RUFDSSxtQkFBQTtFQUNBLHFCQUFBO0VBQ0EsNkJBQUE7QUFOUiIsImZpbGUiOiJzcmMvYXBwL3RyZW5kc3BvdHRlci90cmVuZHNwb3R0ZXIucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaW9uLWNvbCB7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgZmxleC1mbG93OiBjb2x1bW47XHJcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgcGFkZGluZzogNXB4O1xyXG59XHJcblxyXG5bZi0xNF0ge1xyXG4gICAgZm9udC1zaXplOiAxNXB4IWltcG9ydGFudDtcclxufVxyXG5cclxuW2YtMTBdIHtcclxuICAgIGZvbnQtc2l6ZTogMTNweCAhaW1wb3J0YW50O1xyXG59XHJcblxyXG4uYmctbGlzdHtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50O1xyXG59XHJcblxyXG5bc2VhcmNoLWJveF17XHJcbiAgICBiYWNrZ3JvdW5kOiAjZmZmZmZmMWY7XHJcbiAgICBwYWRkaW5nOiAxMHB4O1xyXG4gICAgbWFyZ2luOiAxMHB4O1xyXG4gICAgYm9yZGVyOiAxcHggc29saWQgI2ZmZmZmZjNiO1xyXG4gICAgaW9uLWxpc3R7XHJcbiAgICBtYXJnaW46IC01cHggMDtcclxuICAgIHdpZHRoOiA5NCU7XHJcbiAgICBwYWRkaW5nOiAwO1xyXG4gICAgfVxyXG59XHJcbi8vIGlvbi1zbGlkZSB7XHJcbi8vICAgICBoZWlnaHQ6IGNhbGMoMTAwdmggLSAxNDBweCk7XHJcbi8vICAgfVxyXG5cclxuaW9uLXNsaWRlIHtcclxuICAgIC8vIG1hcmdpbi1yaWdodDogMTVweCAhaW1wb3J0YW50O1xyXG4gICAgd2lkdGg6IDQ1JSAhaW1wb3J0YW50O1xyXG4gICAgfVxyXG5cclxuICAgIC50cy1saXN0e1xyXG4gICAgICAgIGlvbi1sYWJlbHtcclxuICAgICAgICAgICAgd2hpdGUtc3BhY2U6IG5vd3JhcDtcclxuICAgICAgICAgICAgb3ZlcmZsb3c6IGhpZGRlbjtcclxuICAgICAgICAgICAgdGV4dC1vdmVyZmxvdzogZWxsaXBzaXM7XHJcbiAgICAgICAgfVxyXG4gICAgICAgXHJcbiAgICB9XHJcblxyXG4gICAgLmJ0bi1jdXJ2ZXtcclxuICAgICAgICBib3JkZXItcmFkaXVzOiAxMHB4O1xyXG4gICAgICAgIC0tYm9yZGVyLXJhZGl1czogMTBweDtcclxuICAgICAgICAtLWlvbi1jb2xvci1jb250cmFzdDogIzIxMjcyMTtcclxuICAgICB9Il19 */";
      /***/
    },

    /***/
    "./src/app/trendspotter/trendspotter.page.ts":
    /*!***************************************************!*\
      !*** ./src/app/trendspotter/trendspotter.page.ts ***!
      \***************************************************/

    /*! exports provided: TrendspotterPage */

    /***/
    function srcAppTrendspotterTrendspotterPageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "TrendspotterPage", function () {
        return TrendspotterPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _ionic_native_keyboard__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @ionic-native/keyboard */
      "./node_modules/@ionic-native/keyboard/index.js");
      /* harmony import */


      var _global_services_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! ../global-services.service */
      "./src/app/global-services.service.ts");

      var TrendspotterPage = /*#__PURE__*/function () {
        function TrendspotterPage(menu, service) {
          _classCallCheck(this, TrendspotterPage);

          this.menu = menu;
          this.service = service;
          this.itemss = [];
          this.slideOpt = {
            loop: true
          };
          this.url = 'getTrendSpotterSearchWithPaging';
          this.trendSpotterList = [];
          this.trendSpotterListCount = 0;
          this.city_or_studname = '';
          this.showToolbar = true;
          this.pageNo = 0;
          this.Scroll = false;
          this.loadMoreBtn = false;
          this.isItemAvailable = false;
          this.items = [];
          this.selectedItem = "";
        }

        _createClass(TrendspotterPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            var _this = this;

            _ionic_native_keyboard__WEBPACK_IMPORTED_MODULE_3__["Keyboard"].onKeyboardShow().subscribe(function () {
              _this.showToolbar = false;
            });

            _ionic_native_keyboard__WEBPACK_IMPORTED_MODULE_3__["Keyboard"].onKeyboardHide().subscribe(function () {
              _this.showToolbar = true;
            });

            this.loadMore();
          }
        }, {
          key: "ionViewWillEnter",
          value: function ionViewWillEnter() {
            this.service.sendMessage('startNotificationCountFunction');
          }
        }, {
          key: "enableSearch",
          value: function enableSearch(value) {
            this.isEnableSearch = value;

            if (!value && this.city_or_studname) {
              this.city_or_studname = '';
              this.pageNo = 1;
              this.trendSpotterList = [];
              this.getTrendSpotterList();
            }

            this.Content.scrollToTop(); // this.loaderRefresh();
          }
        }, {
          key: "openMenu",
          value: function openMenu() {
            this.menu.open();
          }
        }, {
          key: "ItemAvailableClose",
          value: function ItemAvailableClose(selected) {
            this.isItemAvailable = false;
            this.selectedItem = selected;
          }
        }, {
          key: "searchList",
          value: function searchList() {
            this.pageNo = 1;
            this.trendSpotterList = [];
            this.getTrendSpotterList();
          }
        }, {
          key: "loadMore",
          value: function loadMore() {
            this.pageNo = this.pageNo + 1;
            this.loadMoreBtn = false;
            this.getTrendSpotterList();
          }
        }, {
          key: "getTrendSpotterList",
          value: function getTrendSpotterList() {
            var _this2 = this;

            var obj = {
              pageid: this.pageNo,
              rows_in_page: 20,
              city_or_studname: this.city_or_studname
            };
            this.service.post_data(this.url, obj).subscribe(function (result) {
              var res = _this2.service.parserToJSON(result);

              _this2.trendSpotterListCount = JSON.parse(res.string).Total_Records;

              if (_this2.trendSpotterListCount >= _this2.trendSpotterList.length) {
                _this2.trendSpotterList = _this2.trendSpotterList.concat(JSON.parse(res.string).Details);
                setTimeout(function () {
                  if (_this2.trendSpotterListCount > _this2.trendSpotterList.length) {
                    _this2.loadMoreBtn = true;
                  } else {
                    _this2.loadMoreBtn = false;
                  }
                }, 500);
              }
            }); // this.loaderRefresh();
          }
        }]);

        return TrendspotterPage;
      }();

      TrendspotterPage.ctorParameters = function () {
        return [{
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["MenuController"]
        }, {
          type: _global_services_service__WEBPACK_IMPORTED_MODULE_4__["GlobalServicesService"]
        }];
      };

      TrendspotterPage.propDecorators = {
        infiniteScroll: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"],
          args: [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["IonInfiniteScroll"]]
        }],
        Content: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"],
          args: [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["IonContent"]]
        }],
        slider: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"],
          args: ['slides', {
            "static": true
          }]
        }]
      };
      TrendspotterPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-trendspotter',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./trendspotter.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/trendspotter/trendspotter.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./trendspotter.page.scss */
        "./src/app/trendspotter/trendspotter.page.scss"))["default"]]
      })], TrendspotterPage);
      /***/
    }
  }]);
})();
//# sourceMappingURL=trendspotter-trendspotter-module-es5.js.map