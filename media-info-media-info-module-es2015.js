(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["media-info-media-info-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/media-info/media-info.page.html":
/*!***************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/media-info/media-info.page.html ***!
  \***************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-content>\r\n    <ion-list class=\"ion-no-padding\">\r\n        <!-- <ion-img-viewer title=\"Image Preview\" text=\"Photo by Mayur Gala on Unsplash\" [src]=\"imgUrl\" scheme=\"dark\"></ion-img-viewer> -->\r\n        <ion-img *ngIf=\"data.image_video == 'IMAGE'\" [src]=\"data.upload_image\" class=\"img_size\"></ion-img>\r\n        <div class=\"video-player\" *ngIf=\"data.image_video == 'VIDEO'\">\r\n            <video [poster]=\"data.upload_thump\" controls playsinline webkit-playsinline preload=\"auto\" muted=\"true\"\r\n                crossorigin controlsList=\"nodownload\">\r\n                <source [src]=\"data.upload_image\"  />\r\n            </video>\r\n        </div>\r\n        <ion-buttons back_funct slot=\"start\">\r\n            <ion-back-button color=\"white\" [defaultHref]=\"backUrl\"></ion-back-button>\r\n        </ion-buttons>\r\n        <!-- <ion-fab fab-zoom-2 *ngIf=\"data.image_video == 'VIDEO'\" >\r\n            <ion-fab-button (click)=\"FileInformationAlert()\" color=\"light\">\r\n                <ion-icon name=\"information\"></ion-icon>\r\n            </ion-fab-button>\r\n        </ion-fab> -->\r\n        <!-- <ion-fab fab-share >\r\n            <ion-fab-button color=\"yellow\">\r\n                <ion-icon name=\"share-social-outline\"></ion-icon>\r\n            </ion-fab-button>\r\n            <ion-fab-list side=\"start\">\r\n                <ion-fab-button (click)=\"twitterShare()\" color=\"light\">\r\n                    <ion-icon name=\"logo-twitter\"></ion-icon>\r\n                </ion-fab-button>\r\n                <ion-fab-button (click)=\"instagramShare()\" color=\"light\">\r\n                    <ion-icon name=\"logo-instagram\"></ion-icon>\r\n                </ion-fab-button>\r\n                <ion-fab-button (click)=\"facebookShare()\" color=\"light\">\r\n                    <ion-icon name=\"logo-facebook\"></ion-icon>\r\n                </ion-fab-button>\r\n            </ion-fab-list>\r\n        </ion-fab> -->\r\n        <!-- <ion-fab fab-zoom *ngIf=\"data.image_video == 'IMAGE'\"   >\r\n            <ion-fab-button (click)=\"openViewer(data.upload_image,data.city)\" color=\"light\">\r\n                <ion-icon name=\"search\"></ion-icon>\r\n            </ion-fab-button>\r\n        </ion-fab> -->\r\n        <!-- <ion-fab fab-zoom-1 *ngIf=\"data.image_video == 'IMAGE'\">\r\n            <ion-fab-button (click)=\"FileInformationAlert()\" color=\"light\">\r\n                <ion-icon name=\"information\"></ion-icon>\r\n            </ion-fab-button>\r\n        </ion-fab> -->\r\n        <!-- <ion-fab vertical=\"center\" horizontal=\"end\" >\r\n            <ion-fab-button class=\"paddng\">\r\n              <ion-icon name=\"add\"></ion-icon>\r\n            </ion-fab-button>\r\n        </ion-fab> -->\r\n\r\n<!-- Share -->\r\n\r\n        <!-- <ion-fab vertical=\"top\" class=\"fab-post-top\" horizontal=\"end\" slot=\"fixed\">\r\n            <ion-fab-button class=\"paddng\" color=\"yellow\">\r\n                <ion-icon name=\"share-social-outline\"></ion-icon>\r\n            </ion-fab-button>\r\n            <ion-fab-list side=\"start\">\r\n                <ion-fab-button (click)=\"twitterShare()\" color=\"light\">\r\n                    <ion-icon name=\"logo-twitter\"></ion-icon>\r\n                </ion-fab-button>\r\n                <ion-fab-button (click)=\"instagramShare()\" color=\"light\">\r\n                    <ion-icon name=\"logo-instagram\"></ion-icon>\r\n                </ion-fab-button>\r\n                <ion-fab-button (click)=\"facebookShare()\" color=\"light\">\r\n                    <ion-icon name=\"logo-facebook\"></ion-icon>\r\n                </ion-fab-button>\r\n            </ion-fab-list>\r\n        </ion-fab> -->\r\n\r\n<!-- Share -->\r\n\r\n\r\n        <ion-fab class=\"fab-post\" vertical=\"top\" horizontal=\"end\" slot=\"fixed\">\r\n            <ion-fab-button class=\"paddng\" (click)=\"FileInformationAlert()\" color=\"light\">\r\n                <ion-icon name=\"information\"></ion-icon>\r\n            </ion-fab-button>\r\n            <ion-fab-button class=\"paddng\" *ngIf=\"data.image_video == 'IMAGE'\"\r\n                (click)=\"openViewer(data.upload_image,data.city)\" color=\"light\">\r\n                <ion-icon name=\"search\"></ion-icon>\r\n            </ion-fab-button>\r\n        </ion-fab>\r\n        <!-- <ion-fab vertical=\"top\" horizontal=\"end\" slot=\"fixed\">\r\n            \r\n        </ion-fab> -->\r\n\r\n    </ion-list>\r\n    <ion-list class=\"p-0\" background-text>\r\n        <ion-item class=\"ion-text-center item-shadow\" lines=\"none\" header-menu>\r\n            <ion-label class=\"m-0 mediashare\">\r\n                Tags\r\n            </ion-label>\r\n        </ion-item>\r\n    </ion-list>\r\n    <!-- <ion-list header-menu class=\"categories\">\r\n        <div class=\"un-selected\" *ngFor=\"let category of categories\">\r\n            <ion-label class=\"ion-text-center p-2\">\r\n                {{category}}\r\n            </ion-label>\r\n        </div>\r\n    </ion-list> -->\r\n    <!-- <div class=\"shadow p-3 m-2 comments\">\r\n            <h5>Comments</h5>\r\n            <p *ngIf=\"!data.img_comments\"> No comments found\r\n            </p>\r\n            <p *ngIf=\"data.img_comments\"> {{data.img_comments}}\r\n            </p>\r\n        </div> -->\r\n    <div class=\"shadow m-2 comments\">\r\n        <ion-label class=\"p-3 breadcrumbs-font\"><small\r\n                *ngIf=\"data.img_categ && data.img_categ!=undefined\">{{data.img_categ}} ></small> <small\r\n                *ngIf=\"data.level1Val && data.level1Val!=undefined\">{{data.level1Val}} > </small> <small\r\n                *ngIf=\"data.level2Val && data.level2Val!=undefined\">{{data.level2Val}} > </small> <small\r\n                *ngIf=\"data.level3Val && data.level3Val!=undefined\">{{data.level3Val}}</small></ion-label>\r\n        <ion-grid>\r\n            <div *ngIf=\"level4Val_Value!=[]\">\r\n                <ion-row *ngFor=\"let level4Values of level4Val_Value\">\r\n                    <ion-col size=\"4\" class=\"gray-bc border-top\"\r\n                        *ngIf=\"level4Values.labelValue && level4Values.labelValue!=undefined\">\r\n                        <ion-label>{{level4Values.labelName}}</ion-label>\r\n                    </ion-col>\r\n                    <ion-col size=\"8\" class=\"border-top\"\r\n                        *ngIf=\"level4Values.labelValue && level4Values.labelValue!=undefined\">\r\n                        <ion-label>{{level4Values.labelValue}}</ion-label>\r\n                    </ion-col>\r\n                </ion-row>\r\n            </div>\r\n            <ion-row *ngIf=\"data.tags && data.tags!=undefined && data.tags!=null && data.tags!='null'\">\r\n                <ion-col size=\"4\" class=\"gray-bc border-top\">\r\n                    <ion-label>#Tags</ion-label>\r\n                </ion-col>\r\n                <ion-col size=\"8\" class=\"border-top\">\r\n                    <ion-label>{{data.tags}}</ion-label>\r\n                </ion-col>\r\n            </ion-row>\r\n            <ion-row *ngIf=\"data.colorVal && data.colorVal!=undefined\">\r\n                <ion-col size=\"4\" class=\"gray-bc border-top\">\r\n                    <ion-label>Color</ion-label>\r\n                </ion-col>\r\n                <ion-col size=\"8\" class=\"border-top\">\r\n                    <ion-label>{{data.colorVal}}</ion-label>\r\n                </ion-col>\r\n            </ion-row>\r\n            <!-- <ion-row *ngIf=\"data.swatch && data.swatch!=undefined\">\r\n                <ion-col size=\"4\" class=\"gray-bc border-top\">\r\n                    <ion-label>Swatch</ion-label>\r\n                </ion-col>\r\n                <ion-col size=\"8\" class=\"border-top\">\r\n                    <ion-label>{{data.swatch}}</ion-label>\r\n                </ion-col>\r\n            </ion-row> -->\r\n        </ion-grid>\r\n        <br>\r\n        <!-- <ion-grid *ngIf=\"data.image_video && data.image_video=='IMAGE'\">\r\n            <ion-row *ngIf=\"fileinfo.fileName && fileinfo.fileName!=undefined\">\r\n                <ion-col size=\"5\">\r\n                    <small><b>File Name :</b></small>\r\n                </ion-col>\r\n                <ion-col size=\"7\">\r\n                    <small>{{fileinfo.fileName}}</small>\r\n                </ion-col>\r\n            </ion-row>\r\n            <ion-row *ngIf=\"fileinfo.time && fileinfo.time!=undefined\">\r\n                <ion-col size=\"5\">\r\n                    <small><b>Time :</b></small>\r\n                </ion-col>\r\n                <ion-col size=\"7\">\r\n                    <small>{{fileinfo.time}}</small>\r\n                </ion-col>\r\n            </ion-row>\r\n            <ion-row *ngIf=\"fileinfo.resolution && fileinfo.resolution!=undefined\">\r\n                <ion-col size=\"5\">\r\n                    <small><b>Resolution :</b></small>\r\n                </ion-col>\r\n                <ion-col size=\"7\">\r\n                    <small>{{fileinfo.resolution}}</small>\r\n                </ion-col>\r\n            </ion-row>\r\n            <ion-row *ngIf=\"fileinfo.size && fileinfo.size!=undefined\">\r\n                <ion-col size=\"5\">\r\n                    <small><b>Size :</b></small>\r\n                </ion-col>\r\n                <ion-col size=\"7\">\r\n                    <small>{{fileinfo.size}}</small>\r\n                </ion-col>\r\n            </ion-row>\r\n            <ion-row *ngIf=\"fileinfo.device && fileinfo.device!=undefined\">\r\n                <ion-col size=\"5\">\r\n                    <small><b>Device :</b></small>\r\n                </ion-col>\r\n                <ion-col size=\"7\">\r\n                    <small>{{fileinfo.device}}</small>\r\n                </ion-col>\r\n            </ion-row>\r\n            <ion-row *ngIf=\"fileinfo.model && fileinfo.model!=undefined\">\r\n                <ion-col size=\"5\">\r\n                    <small><b>Model :</b></small>\r\n                </ion-col>\r\n                <ion-col size=\"7\">\r\n                    <small>{{fileinfo.model}}</small>\r\n                </ion-col>\r\n            </ion-row>\r\n            <ion-row *ngIf=\"fileinfo.flash && fileinfo.flash!=undefined\">\r\n                <ion-col size=\"5\">\r\n                    <small><b>Flash :</b></small>\r\n                </ion-col>\r\n                <ion-col size=\"7\">\r\n                    <small>{{fileinfo.flash}}</small>\r\n                </ion-col>\r\n            </ion-row>\r\n            <ion-row *ngIf=\"fileinfo.whiteBalance && fileinfo.whiteBalance!=undefined\">\r\n                <ion-col size=\"5\">\r\n                    <small><b>White Blance :</b></small>\r\n                </ion-col>\r\n                <ion-col size=\"7\">\r\n                    <small>{{fileinfo.whiteBalance}}</small>\r\n                </ion-col>\r\n            </ion-row>\r\n            <ion-row *ngIf=\"fileinfo.iso && fileinfo.iso!=undefined\">\r\n                <ion-col size=\"5\">\r\n                    <small><b>ISO :</b></small>\r\n                </ion-col>\r\n                <ion-col size=\"7\">\r\n                    <small>{{fileinfo.iso}}</small>\r\n                </ion-col>\r\n            </ion-row>\r\n            <ion-row *ngIf=\"fileinfo.orientation && fileinfo.orientation!=undefined\">\r\n                <ion-col size=\"5\">\r\n                    <small><b>Orientation :</b></small>\r\n                </ion-col>\r\n                <ion-col size=\"7\">\r\n                    <small>{{fileinfo.orientation}}</small>\r\n                </ion-col>\r\n            </ion-row>\r\n            <ion-row *ngIf=\"data.location && data.location!=undefined\">\r\n                <ion-col size=\"5\">\r\n                    <small><b>Location :</b></small>\r\n                </ion-col>\r\n                <ion-col size=\"7\">\r\n                    <small>{{data.location?data.location:'No location found'}}</small>\r\n                </ion-col>\r\n            </ion-row>\r\n            <ion-row *ngIf=\"latitude\">\r\n                <ion-col size=\"5\">\r\n                    <small><b>Latitude :</b></small>\r\n                </ion-col>\r\n                <ion-col size=\"7\">\r\n                    <small>{{latitude}}</small>\r\n                </ion-col>\r\n            </ion-row>\r\n            <ion-row *ngIf=\"longitude\">\r\n                <ion-col size=\"5\">\r\n                    <small><b>Longitude :</b></small>\r\n                </ion-col>\r\n                <ion-col size=\"7\">\r\n\r\n                    <small>{{longitude}}</small>\r\n                </ion-col>\r\n            </ion-row>\r\n        </ion-grid>\r\n        <ion-grid *ngIf=\"data.image_video && data.image_video=='VIDEO'\">\r\n            <ion-row *ngIf=\"fileinfo.fileName && fileinfo.fileName!=undefined\">\r\n                <ion-col size=\"5\">\r\n                    <small><b>File Name :</b></small>\r\n                </ion-col>\r\n                <ion-col size=\"7\">\r\n                    <small>{{fileinfo.fileName}}</small>\r\n                </ion-col>\r\n            </ion-row>\r\n            <ion-row *ngIf=\"fileinfo.time && fileinfo.time!=undefined\">\r\n                <ion-col size=\"5\">\r\n                    <small><b>Time :</b></small>\r\n                </ion-col>\r\n                <ion-col size=\"7\">\r\n                    <small>{{fileinfo.time}}</small>\r\n                </ion-col>\r\n            </ion-row>\r\n            <ion-row *ngIf=\"fileinfo.resolution && fileinfo.resolution!=undefined\">\r\n                <ion-col size=\"5\">\r\n                    <small><b>Resolution :</b></small>\r\n                </ion-col>\r\n                <ion-col size=\"7\">\r\n                    <small>{{fileinfo.resolution}}</small>\r\n                </ion-col>\r\n            </ion-row>\r\n            <ion-row *ngIf=\"fileinfo.size && fileinfo.size!=undefined\">\r\n                <ion-col size=\"5\">\r\n                    <small><b>Size :</b></small>\r\n                </ion-col>\r\n                <ion-col size=\"7\">\r\n                    <small>{{fileinfo.size}}</small>\r\n                </ion-col>\r\n            </ion-row>\r\n            <ion-row *ngIf=\"data.location && data.location!=undefined && data.location!='undefined'\">\r\n                <ion-col size=\"5\">\r\n                    <small><b>Location :</b></small>\r\n                </ion-col>\r\n                <ion-col size=\"7\">\r\n                    <small>{{data.location?data.location:'No location found'}}</small>\r\n                </ion-col>\r\n            </ion-row>\r\n            <ion-row *ngIf=\"latitude\">\r\n                <ion-col size=\"5\">\r\n                    <small><b>Latitude :</b></small>\r\n                </ion-col>\r\n                <ion-col size=\"7\">\r\n                    <small>{{latitude}}</small>\r\n                </ion-col>\r\n            </ion-row>\r\n            <ion-row *ngIf=\"longitude\">\r\n                <ion-col size=\"5\">\r\n                    <small><b>Longitude :</b></small>\r\n                </ion-col>\r\n                <ion-col size=\"7\">\r\n\r\n                    <small>{{longitude}}</small>\r\n                </ion-col>\r\n            </ion-row>\r\n        </ion-grid> -->\r\n    </div>\r\n</ion-content>");

/***/ }),

/***/ "./src/app/media-info/media-info-routing.module.ts":
/*!*********************************************************!*\
  !*** ./src/app/media-info/media-info-routing.module.ts ***!
  \*********************************************************/
/*! exports provided: MediaInfoPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MediaInfoPageRoutingModule", function() { return MediaInfoPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _media_info_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./media-info.page */ "./src/app/media-info/media-info.page.ts");




const routes = [
    {
        path: '',
        component: _media_info_page__WEBPACK_IMPORTED_MODULE_3__["MediaInfoPage"]
    }
];
let MediaInfoPageRoutingModule = class MediaInfoPageRoutingModule {
};
MediaInfoPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], MediaInfoPageRoutingModule);



/***/ }),

/***/ "./src/app/media-info/media-info.module.ts":
/*!*************************************************!*\
  !*** ./src/app/media-info/media-info.module.ts ***!
  \*************************************************/
/*! exports provided: MediaInfoPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MediaInfoPageModule", function() { return MediaInfoPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _media_info_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./media-info-routing.module */ "./src/app/media-info/media-info-routing.module.ts");
/* harmony import */ var _media_info_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./media-info.page */ "./src/app/media-info/media-info.page.ts");







let MediaInfoPageModule = class MediaInfoPageModule {
};
MediaInfoPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _media_info_routing_module__WEBPACK_IMPORTED_MODULE_5__["MediaInfoPageRoutingModule"]
        ],
        declarations: [_media_info_page__WEBPACK_IMPORTED_MODULE_6__["MediaInfoPage"]]
    })
], MediaInfoPageModule);



/***/ }),

/***/ "./src/app/media-info/media-info.page.scss":
/*!*************************************************!*\
  !*** ./src/app/media-info/media-info.page.scss ***!
  \*************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("[background] {\n  margin: 0 -15px;\n}\n[background] ion-content ion-button {\n  font-size: 30px;\n  position: absolute;\n  left: 50%;\n  top: 50%;\n  transform: translate(-50%, -50%);\n}\n[background-text] {\n  margin: 0;\n  background: var(--ion-color-yellow);\n  text-align: center;\n}\n[ion-col-height] {\n  width: 100%;\n  height: 170px;\n  max-height: 250px;\n  z-index: 100;\n}\n.categories {\n  display: flex;\n  flex-direction: row;\n  flex-wrap: wrap;\n  justify-content: center;\n}\n.categories div {\n  margin: 5px;\n  border-radius: 30px;\n}\n.un-selected {\n  background-color: #ffffff;\n  border: #ffffff 2px solid;\n  color: #000000;\n  font-size: 11px;\n}\n.selected {\n  background-color: var(--ion-color-yellow);\n  color: #000000;\n  border: var(--ion-color-yellow);\n}\nion-content {\n  --background: #292927;\n}\n.video-player {\n  background: #1f1e1d;\n  width: 100%;\n  height: 300px !important;\n}\n.video-player video {\n  width: 100%;\n  height: 300px !important;\n  position: absolute;\n}\n.img-preview {\n  -o-object-fit: contain;\n     object-fit: contain;\n  width: 100%;\n  height: 100%;\n}\n.img_size {\n  width: 100%;\n  -o-object-fit: cover !important;\n     object-fit: cover !important;\n  -o-object-position: center;\n     object-position: center;\n  background-color: white;\n  height: 300px;\n}\n[back_funct] {\n  position: absolute;\n  top: 10px;\n  z-index: 999;\n}\n[role_funct] {\n  position: absolute;\n  bottom: 10px;\n  z-index: 999;\n}\n[role_funct] p {\n  color: var(--ion-color-white);\n}\n[role_funct] h3 {\n  font-weight: bolder !important;\n  font-size: 1.2em !important;\n  color: var(--ion-color-yellow);\n}\n.margin-rl {\n  margin: 0 15px;\n}\n.mediashare {\n  margin: 5px 0px 0px !important;\n  font-weight: bolder;\n}\n.comments {\n  background-color: #ffffff !important;\n  border-left: 9px solid var(--ion-color-yellow);\n  opacity: 0.9;\n}\n.comments p {\n  font-size: 11px;\n}\n.item-shadow {\n  --box-shadow: 0 1rem 3rem rgba(0, 0, 0, 0.175);\n}\n.fab-zoom, .fab-zoom-ios {\n  bottom: 150px;\n  right: calc(20px + var(--ion-safe-area-right, 0px));\n}\n.fab-zoom ion-fab-button, .fab-zoom-ios ion-fab-button {\n  width: 40px;\n  height: 40px;\n}\n[fab-zoom-1] {\n  bottom: 30px;\n  right: calc(20px + var(--ion-safe-area-right, 0px));\n}\n[fab-zoom-1] ion-fab-button {\n  width: 40px;\n  height: 40px;\n}\n[fab-zoom-2] {\n  bottom: 150px;\n  right: calc(20px + var(--ion-safe-area-right, 0px));\n}\n[fab-zoom-2] ion-fab-button {\n  width: 40px;\n  height: 40px;\n}\n[fab-share] {\n  bottom: 90px;\n  right: calc(20px + var(--ion-safe-area-right, 0px));\n}\n[fab-share] ion-fab-button {\n  width: 40px;\n  height: 40px;\n}\n.pad {\n  padding: 10px 15px 10px;\n}\n.pad p {\n  margin-bottom: 0px;\n}\nion-grid ion-label {\n  font-size: 15px;\n}\nion-grid ion-col {\n  border-bottom: 0.5px solid #2929271a;\n  padding: 8px;\n}\n.gray-bc {\n  background: #2929271f;\n}\n.border-top {\n  border-top: 0.5px solid #2929271a;\n}\n.breadcrumbs-font {\n  font-size: 18px;\n}\n.paddng {\n  margin: 10px 0px;\n  height: 40px;\n  width: 45px;\n}\n.fab-post-top {\n  margin-top: 50px;\n}\n.fab-post {\n  margin-top: 20px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbWVkaWEtaW5mby9tZWRpYS1pbmZvLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGVBQUE7QUFDSjtBQUVRO0VBQ0ksZUFBQTtFQUNBLGtCQUFBO0VBQ0EsU0FBQTtFQUNBLFFBQUE7RUFDQSxnQ0FBQTtBQUFaO0FBSUE7RUFDSSxTQUFBO0VBQ0EsbUNBQUE7RUFDQSxrQkFBQTtBQURKO0FBR0E7RUFDSSxXQUFBO0VBQ0EsYUFBQTtFQUNBLGlCQUFBO0VBQ0EsWUFBQTtBQUFKO0FBR0E7RUFFSSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSxlQUFBO0VBQ0EsdUJBQUE7QUFESjtBQUdJO0VBQ0ksV0FBQTtFQUNBLG1CQUFBO0FBRFI7QUFNQTtFQUNJLHlCQUFBO0VBQ0EseUJBQUE7RUFDQSxjQUFBO0VBQ0EsZUFBQTtBQUhKO0FBS0E7RUFDSSx5Q0FBQTtFQUNBLGNBQUE7RUFDQSwrQkFBQTtBQUZKO0FBSUE7RUFDSSxxQkFBQTtBQURKO0FBR0E7RUFDSSxtQkFBQTtFQUNBLFdBQUE7RUFDQSx3QkFBQTtBQUFKO0FBQ0k7RUFDSSxXQUFBO0VBQ0Esd0JBQUE7RUFDQSxrQkFBQTtBQUNSO0FBRUE7RUFDSSxzQkFBQTtLQUFBLG1CQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7QUFDSjtBQUNBO0VBQ0ksV0FBQTtFQUNBLCtCQUFBO0tBQUEsNEJBQUE7RUFDQSwwQkFBQTtLQUFBLHVCQUFBO0VBQ0EsdUJBQUE7RUFDQSxhQUFBO0FBRUo7QUFBQTtFQUNJLGtCQUFBO0VBQ0EsU0FBQTtFQUNBLFlBQUE7QUFHSjtBQURBO0VBQ0ksa0JBQUE7RUFDQSxZQUFBO0VBQ0EsWUFBQTtBQUlKO0FBSEk7RUFDSSw2QkFBQTtBQUtSO0FBSEk7RUFDSSw4QkFBQTtFQUNBLDJCQUFBO0VBQ0EsOEJBQUE7QUFLUjtBQUZBO0VBQ0ksY0FBQTtBQUtKO0FBSEE7RUFDSSw4QkFBQTtFQUNBLG1CQUFBO0FBTUo7QUFKQTtFQUlJLG9DQUFBO0VBQ0EsOENBQUE7RUFDQSxZQUFBO0FBSUo7QUFUSTtFQUNJLGVBQUE7QUFXUjtBQUxBO0VBQ0ksOENBQUE7QUFRSjtBQU5BO0VBQ0ksYUFBQTtFQUNBLG1EQUFBO0FBU0o7QUFSSTtFQUNJLFdBQUE7RUFDQSxZQUFBO0FBVVI7QUFQQTtFQUNJLFlBQUE7RUFDQSxtREFBQTtBQVVKO0FBVEk7RUFDSSxXQUFBO0VBQ0EsWUFBQTtBQVdSO0FBUkE7RUFDSSxhQUFBO0VBQ0EsbURBQUE7QUFXSjtBQVZJO0VBQ0ksV0FBQTtFQUNBLFlBQUE7QUFZUjtBQVJBO0VBQ0ksWUFBQTtFQUNBLG1EQUFBO0FBV0o7QUFWSTtFQUNJLFdBQUE7RUFDQSxZQUFBO0FBWVI7QUFUQTtFQUNJLHVCQUFBO0FBWUo7QUFYSTtFQUNJLGtCQUFBO0FBYVI7QUFUSTtFQUNJLGVBQUE7QUFZUjtBQVZJO0VBQ0ksb0NBQUE7RUFDQSxZQUFBO0FBWVI7QUFUQTtFQUNJLHFCQUFBO0FBWUo7QUFWQTtFQUNJLGlDQUFBO0FBYUo7QUFYQTtFQUNJLGVBQUE7QUFjSjtBQVpBO0VBQ0ksZ0JBQUE7RUFDQSxZQUFBO0VBQ0EsV0FBQTtBQWVKO0FBYkE7RUFDSSxnQkFBQTtBQWdCSjtBQWRBO0VBQ0ksZ0JBQUE7QUFpQkoiLCJmaWxlIjoic3JjL2FwcC9tZWRpYS1pbmZvL21lZGlhLWluZm8ucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiW2JhY2tncm91bmRdIHtcclxuICAgIG1hcmdpbjogMCAtMTVweDtcclxuICAgIGlvbi1jb250ZW50IHtcclxuICAgICAgICAvLyAtLWJhY2tncm91bmQ6ICM1MjUyNTI7XHJcbiAgICAgICAgaW9uLWJ1dHRvbiB7XHJcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMzBweDtcclxuICAgICAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICAgICAgICBsZWZ0OiA1MCU7XHJcbiAgICAgICAgICAgIHRvcDogNTAlO1xyXG4gICAgICAgICAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZSgtNTAlLCAtNTAlKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuW2JhY2tncm91bmQtdGV4dF0ge1xyXG4gICAgbWFyZ2luOiAwOyBcclxuICAgIGJhY2tncm91bmQ6IHZhcigtLWlvbi1jb2xvci15ZWxsb3cpO1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG59XHJcbltpb24tY29sLWhlaWdodF0ge1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBoZWlnaHQ6IDE3MHB4O1xyXG4gICAgbWF4LWhlaWdodDogMjUwcHg7XHJcbiAgICB6LWluZGV4OiAxMDA7XHJcbn1cclxuLy9leGFtcGxlXHJcbi5jYXRlZ29yaWVzIHtcclxuICAgIC8vIGJhY2tncm91bmQ6ICMyOTI5Mjc7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgZmxleC1kaXJlY3Rpb246IHJvdztcclxuICAgIGZsZXgtd3JhcDogd3JhcDtcclxuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgLy8gbWFyZ2luOiAyNXB0IDA7XHJcbiAgICBkaXYge1xyXG4gICAgICAgIG1hcmdpbjogNXB4O1xyXG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDMwcHg7XHJcbiAgICAgICAgLy8gd2lkdGg6IDkwcHg7XHJcbiAgICAgICAgLy8gaGVpZ2h0OiA0MHB4O1xyXG4gICAgfVxyXG59XHJcbi51bi1zZWxlY3RlZCB7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmZmZmO1xyXG4gICAgYm9yZGVyOiAjZmZmZmZmIDJweCBzb2xpZDtcclxuICAgIGNvbG9yOiAjMDAwMDAwO1xyXG4gICAgZm9udC1zaXplOiAxMXB4O1xyXG59XHJcbi5zZWxlY3RlZCB7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB2YXIoLS1pb24tY29sb3IteWVsbG93KTsgLy8jRjZDNzYyO1xyXG4gICAgY29sb3I6ICMwMDAwMDA7XHJcbiAgICBib3JkZXI6IHZhcigtLWlvbi1jb2xvci15ZWxsb3cpOyAvLyNGNkM3NjIgMnB4IHNvbGlkO1xyXG59XHJcbmlvbi1jb250ZW50IHtcclxuICAgIC0tYmFja2dyb3VuZDogIzI5MjkyNztcclxufVxyXG4udmlkZW8tcGxheWVyIHtcclxuICAgIGJhY2tncm91bmQ6ICMxZjFlMWQ7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGhlaWdodDogMzAwcHghaW1wb3J0YW50O1xyXG4gICAgdmlkZW8ge1xyXG4gICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgICAgIGhlaWdodDogMzAwcHghaW1wb3J0YW50O1xyXG4gICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIH1cclxufVxyXG4uaW1nLXByZXZpZXcge1xyXG4gICAgb2JqZWN0LWZpdDogY29udGFpbjtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgaGVpZ2h0OiAxMDAlO1xyXG59XHJcbi5pbWdfc2l6ZSB7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIG9iamVjdC1maXQ6IGNvdmVyIWltcG9ydGFudDtcclxuICAgIG9iamVjdC1wb3NpdGlvbjogY2VudGVyO1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogbGlnaHRlbih3aGl0ZSwgNzAlKTtcclxuICAgIGhlaWdodDogMzAwcHg7XHJcbn1cclxuW2JhY2tfZnVuY3RdIHtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIHRvcDogMTBweDtcclxuICAgIHotaW5kZXg6IDk5OTtcclxufVxyXG5bcm9sZV9mdW5jdF0ge1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgYm90dG9tOiAxMHB4O1xyXG4gICAgei1pbmRleDogOTk5O1xyXG4gICAgcCB7XHJcbiAgICAgICAgY29sb3I6IHZhcigtLWlvbi1jb2xvci13aGl0ZSk7XHJcbiAgICB9XHJcbiAgICBoMyB7XHJcbiAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGRlciAhaW1wb3J0YW50O1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMS4yZW0gIWltcG9ydGFudDtcclxuICAgICAgICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLXllbGxvdyk7XHJcbiAgICB9XHJcbn1cclxuLm1hcmdpbi1ybCB7XHJcbiAgICBtYXJnaW46IDAgMTVweDtcclxufVxyXG4ubWVkaWFzaGFyZSB7XHJcbiAgICBtYXJnaW46IDVweCAwcHggMHB4ICFpbXBvcnRhbnQ7XHJcbiAgICBmb250LXdlaWdodDogYm9sZGVyO1xyXG59XHJcbi5jb21tZW50cyB7XHJcbiAgICBwIHtcclxuICAgICAgICBmb250LXNpemU6IDExcHg7XHJcbiAgICB9XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmZmZmICFpbXBvcnRhbnQ7XHJcbiAgICBib3JkZXItbGVmdDogOXB4IHNvbGlkIHZhcigtLWlvbi1jb2xvci15ZWxsb3cpOyAvLyAjZjZjNzYyZWI7XHJcbiAgICBvcGFjaXR5OiAwLjk7XHJcbn1cclxuLml0ZW0tc2hhZG93IHtcclxuICAgIC0tYm94LXNoYWRvdzogMCAxcmVtIDNyZW0gcmdiYSgwLCAwLCAwLCAwLjE3NSk7XHJcbn1cclxuLmZhYi16b29tLC5mYWItem9vbS1pb3Mge1xyXG4gICAgYm90dG9tOiAxNTBweDtcclxuICAgIHJpZ2h0OiBjYWxjKDIwcHggKyB2YXIoLS1pb24tc2FmZS1hcmVhLXJpZ2h0LCAwcHgpKTtcclxuICAgIGlvbi1mYWItYnV0dG9uIHtcclxuICAgICAgICB3aWR0aDogNDBweDtcclxuICAgICAgICBoZWlnaHQ6IDQwcHg7XHJcbiAgICB9XHJcbn1cclxuW2ZhYi16b29tLTFdIHtcclxuICAgIGJvdHRvbTogMzBweDtcclxuICAgIHJpZ2h0OiBjYWxjKDIwcHggKyB2YXIoLS1pb24tc2FmZS1hcmVhLXJpZ2h0LCAwcHgpKTtcclxuICAgIGlvbi1mYWItYnV0dG9uIHtcclxuICAgICAgICB3aWR0aDogNDBweDtcclxuICAgICAgICBoZWlnaHQ6IDQwcHg7XHJcbiAgICB9XHJcbn1cclxuW2ZhYi16b29tLTJdIHtcclxuICAgIGJvdHRvbTogMTUwcHg7XHJcbiAgICByaWdodDogY2FsYygyMHB4ICsgdmFyKC0taW9uLXNhZmUtYXJlYS1yaWdodCwgMHB4KSk7XHJcbiAgICBpb24tZmFiLWJ1dHRvbiB7XHJcbiAgICAgICAgd2lkdGg6IDQwcHg7XHJcbiAgICAgICAgaGVpZ2h0OiA0MHB4O1xyXG4gICAgfVxyXG59XHJcblxyXG5bZmFiLXNoYXJlXSB7XHJcbiAgICBib3R0b206IDkwcHg7XHJcbiAgICByaWdodDogY2FsYygyMHB4ICsgdmFyKC0taW9uLXNhZmUtYXJlYS1yaWdodCwgMHB4KSk7XHJcbiAgICBpb24tZmFiLWJ1dHRvbiB7XHJcbiAgICAgICAgd2lkdGg6IDQwcHg7XHJcbiAgICAgICAgaGVpZ2h0OiA0MHB4O1xyXG4gICAgfVxyXG59XHJcbi5wYWQge1xyXG4gICAgcGFkZGluZzogMTBweCAxNXB4IDEwcHg7XHJcbiAgICBwIHtcclxuICAgICAgICBtYXJnaW4tYm90dG9tOiAwcHg7XHJcbiAgICB9XHJcbn1cclxuaW9uLWdyaWQge1xyXG4gICAgaW9uLWxhYmVsIHtcclxuICAgICAgICBmb250LXNpemU6IDE1cHg7XHJcbiAgICB9XHJcbiAgICBpb24tY29sIHtcclxuICAgICAgICBib3JkZXItYm90dG9tOiAwLjVweCBzb2xpZCAjMjkyOTI3MWE7XHJcbiAgICAgICAgcGFkZGluZzogOHB4O1xyXG4gICAgfVxyXG59XHJcbi5ncmF5LWJjIHtcclxuICAgIGJhY2tncm91bmQ6ICMyOTI5MjcxZjtcclxufVxyXG4uYm9yZGVyLXRvcCB7XHJcbiAgICBib3JkZXItdG9wOiAwLjVweCBzb2xpZCAjMjkyOTI3MWE7XHJcbn1cclxuLmJyZWFkY3J1bWJzLWZvbnQge1xyXG4gICAgZm9udC1zaXplOiAxOHB4O1xyXG59XHJcbi5wYWRkbmd7XHJcbiAgICBtYXJnaW46IDEwcHggMHB4O1xyXG4gICAgaGVpZ2h0OiA0MHB4O1xyXG4gICAgd2lkdGg6IDQ1cHg7XHJcbn1cclxuLmZhYi1wb3N0LXRvcHtcclxuICAgIG1hcmdpbi10b3A6IDUwcHg7XHJcbn1cclxuLmZhYi1wb3N0e1xyXG4gICAgbWFyZ2luLXRvcDogMjBweDtcclxufSJdfQ== */");

/***/ }),

/***/ "./src/app/media-info/media-info.page.ts":
/*!***********************************************!*\
  !*** ./src/app/media-info/media-info.page.ts ***!
  \***********************************************/
/*! exports provided: MediaInfoPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MediaInfoPage", function() { return MediaInfoPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var ngx_ionic_image_viewer__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ngx-ionic-image-viewer */ "./node_modules/ngx-ionic-image-viewer/__ivy_ngcc__/fesm2015/ngx-ionic-image-viewer.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _global_services_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../global-services.service */ "./src/app/global-services.service.ts");
/* harmony import */ var _file_information_file_information_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../file-information/file-information.page */ "./src/app/file-information/file-information.page.ts");


// import { SocialSharing } from '@ionic-native/social-sharing/ngx';





let MediaInfoPage = class MediaInfoPage {
    constructor(service, 
    // private socialSharing: SocialSharing,
    modalController, route, alertController) {
        this.service = service;
        this.modalController = modalController;
        this.route = route;
        this.alertController = alertController;
        this.data = {};
        this.fileinfo = {};
        this.latitude = '';
        this.longitude = '';
        this.categories = [];
    }
    ngOnInit() {
        this.route.params.subscribe((params) => {
            this.page = params['page'];
            this.data = JSON.parse(params['data']);
        });
        //===============Category==============
        this.latitude = String(this.data.lati_longi).split(',')[0];
        this.longitude = String(this.data.lati_longi).split(',')[1];
        this.fileinfo = JSON.parse(this.data.fileinfo);
        if (this.data.level4Val != "") {
            if (JSON.parse(this.data.level4Val).length > 0) {
                this.level4Val_Value = JSON.parse(this.data.level4Val);
            }
            else {
                this.level4Val_Value = [];
            }
        }
        else {
            this.level4Val_Value = [];
        }
        this.categories = this.data.img_categ.split(',');
        //===============backURL===================
        if (this.page == 'image') {
            this.backUrl = 'uploadedimages';
        }
        else if (this.page == 'video') {
            this.backUrl = 'uploadedvideos';
        }
        else {
            this.backUrl = 'home';
        }
    }
    // =====================Social Media Sharing=====================
    //Twitter
    twitterShare() {
        // this.socialSharing.shareViaTwitter("Shared Post", this.data.upload_image, null)
        //   .then(() => {
        //   }).catch((err) => {
        //     alert("Twitter not found on your phone");
        //   });
    }
    //Instagram
    instagramShare() {
        // this.socialSharing.shareViaInstagram("Shared Post", this.data.upload_image)
        //   .then(() => {
        //   }).catch((err) => {
        //     alert("An error occurred " + err);
        //   });
    }
    //Facebook
    facebookShare() {
        // this.socialSharing.shareViaFacebookWithPasteMessageHint("Shared Post", this.data.upload_image, null, 'Copia Pega!')
        //   .then(() => {
        //   }).catch((err) => {
        //     alert("An error occurred " + err);
        //   });
    }
    // =====================Image Viewer=====================
    openViewer(image, loc) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const modal = yield this.modalController.create({
                component: ngx_ionic_image_viewer__WEBPACK_IMPORTED_MODULE_2__["ViewerModalComponent"],
                componentProps: {
                    src: image,
                    title: 'Photo Viewer',
                    text: 'Location -' + loc // optional
                },
                cssClass: 'ion-img-viewer',
                keyboardClose: true,
                showBackdrop: true
            });
            return yield modal.present();
        });
    }
    // =====================File Information ALert=====================
    FileInformationAlert() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const modal = yield this.modalController.create({
                component: _file_information_file_information_page__WEBPACK_IMPORTED_MODULE_6__["FileInformationPage"],
                componentProps: {
                    title: 'File Information',
                    fileinfo: this.fileinfo,
                    data: this.data,
                    latitude: this.latitude,
                    longitude: this.longitude,
                },
                cssClass: 'my-custom-class',
                keyboardClose: true,
                showBackdrop: true
            });
            return yield modal.present();
            // const alert = await this.alertController.create({
            //   cssClass: 'alert-custom-class',
            //   header: 'File Information',
            //   message: '<ion-grid *ngIf="data.image_video && data.image_video=="IMAGE"> <ion-row *ngIf="fileinfo.fileName && fileinfo.fileName!=undefined"> <ion-col size="5"> <small><b>File Name :</b></small> </ion-col> <ion-col size="7"> <small>'+this.fileinfo.fileName+'</small>  </ion-col> </ion-row> <ion-row *ngIf="fileinfo.time && fileinfo.time!=undefined">   <ion-col size="5">   <small><b>Time :</b></small> </ion-col>  <ion-col size="7"> <small>{{fileinfo.time}}</small> </ion-col> </ion-row><ion-row *ngIf="fileinfo.resolution && fileinfo.resolution!=undefined"> <ion-col size="5"> <small><b>Resolution :</b></small>  </ion-col>  <ion-col size="7">   <small>{{fileinfo.resolution}}</small> </ion-col> </ion-row> <ion-row *ngIf="fileinfo.size && fileinfo.size!=undefined">   <ion-col size="5"> <small><b>Size :</b></small>  </ion-col> <ion-col size="7"> <small>{{fileinfo.size}}</small> </ion-col> </ion-row> <ion-row *ngIf="fileinfo.device && fileinfo.device!=undefined">  <ion-col size="5">   <small><b>Device :</b></small> </ion-col> <ion-col size="7">   <small>{{fileinfo.device}}</small>  </ion-col>  </ion-row> <ion-row *ngIf="fileinfo.model && fileinfo.model!=undefined">  <ion-col size="5"> <small><b>Model :</b></small> </ion-col> <ion-col size="7"> <small>{{fileinfo.model}}</small>  </ion-col> </ion-row> <ion-row *ngIf="fileinfo.flash && fileinfo.flash!=undefined"> <ion-col size="5"> <small><b>Flash :</b></small> </ion-col> <ion-col size="7"><small>{{fileinfo.flash}}</small>  </ion-col> </ion-row>  <ion-row *ngIf="fileinfo.whiteBalance && fileinfo.whiteBalance!=undefined"> <ion-col size="5"> <small><b>White Blance :</b></small>  </ion-col> <ion-col size="7"> <small>{{fileinfo.whiteBalance}}</small>  </ion-col>  </ion-row> <ion-row *ngIf="fileinfo.iso && fileinfo.iso!=undefined">  <ion-col size="5"> <small><b>ISO :</b></small> </ion-col>  <ion-col size="7"> <small>{{fileinfo.iso}}</small>   </ion-col>  </ion-row><ion-row *ngIf="fileinfo.orientation && fileinfo.orientation!=undefined"> <ion-col size="5">  <small><b>Orientation :</b></small> </ion-col> <ion-col size="7">  <small>{{fileinfo.orientation}}</small> </ion-col> </ion-row>  <ion-row *ngIf="data.location && data.location!=undefined"> <ion-col size="5">  <small><b>Location :</b></small>  </ion-col> <ion-col size="7">  <small>{{data.location?data.location:"No location found}}</small>  </ion-col>  </ion-row> <ion-row *ngIf="latitude"> <ion-col size="5"> <small><b>Latitude :</b></small>  </ion-col>  <ion-col size="7">  <small>{{latitude}}</small> </ion-col> </ion-row> <ion-row *ngIf="longitude"> <ion-col size="5">   <small><b>Longitude :</b></small>  </ion-col> <ion-col size="7"> <small>{{longitude}}</small>  </ion-col> </ion-row> </ion-grid>' ,
            //   // message: '<a class="close">&times;</a> <br><br> <img src="../../assets/image/logo.png" alt="g-maps" style="border-radius: 2px">',
            //   backdropDismiss: false,
            //   buttons: [
            //     {
            //       text: 'Close',
            //       role: 'cancel',
            //       cssClass: 'secondary',
            //       handler: (blah) => {
            //       }
            //     }
            //   ]
            // });
            // await alert.present();
        });
    }
};
MediaInfoPage.ctorParameters = () => [
    { type: _global_services_service__WEBPACK_IMPORTED_MODULE_5__["GlobalServicesService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ModalController"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["AlertController"] }
];
MediaInfoPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-media-info',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./media-info.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/media-info/media-info.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./media-info.page.scss */ "./src/app/media-info/media-info.page.scss")).default]
    })
], MediaInfoPage);



/***/ })

}]);
//# sourceMappingURL=media-info-media-info-module-es2015.js.map