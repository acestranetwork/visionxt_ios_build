(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["header-header-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/header/header.page.html":
    /*!*******************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/header/header.page.html ***!
      \*******************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppHeaderHeaderPageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-header translucent no-border>\r\n    <ion-toolbar>\r\n        <ion-item header-menu lines=\"none\">\r\n            <ion-avatar slot=\"start\">\r\n                <img [src]=\"loggedDetails.upload_image\" height=\"100%\" width=\"100%\" *ngIf=\"loggedDetails.upload_image\">\r\n                <img src=\"../../assets//image/user-login-icon-1.png\" height=\"100%\" width=\"100%\"\r\n                    *ngIf=\"!loggedDetails.upload_image\">\r\n            </ion-avatar>\r\n            <ion-label>\r\n                <h3 font-header class=\"ion-text-capitalize\">{{loggedDetails.stud_name}}</h3>\r\n                <p font-medium>{{city}}</p>\r\n            </ion-label>\r\n            <div class=\"notification-bell-container\" slot=\"end\">\r\n                <p class=\"text-highlight\" (click)=\"moveNotificationPage()\"\r\n                    *ngIf=\"notificationUnreadCount && notificationUnreadCount!=0\">\r\n                    {{notificationUnreadCount > 9 ? '9+': notificationUnreadCount}}</p>\r\n                <ion-icon name=\"notifications-outline\" (click)=\"moveNotificationPage()\" class=\"bell-icon\">\r\n                </ion-icon>\r\n            </div>\r\n            <ion-icon name=\"reorder-four-outline\" (click)=\"openMenu()\" slot=\"end\"></ion-icon>\r\n        </ion-item>\r\n    </ion-toolbar>\r\n</ion-header>";
      /***/
    },

    /***/
    "./src/app/header/header-routing.module.ts":
    /*!*************************************************!*\
      !*** ./src/app/header/header-routing.module.ts ***!
      \*************************************************/

    /*! exports provided: HeaderPageRoutingModule */

    /***/
    function srcAppHeaderHeaderRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "HeaderPageRoutingModule", function () {
        return HeaderPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _header_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./header.page */
      "./src/app/header/header.page.ts");

      var routes = [{
        path: '',
        component: _header_page__WEBPACK_IMPORTED_MODULE_3__["HeaderPage"]
      }];

      var HeaderPageRoutingModule = function HeaderPageRoutingModule() {
        _classCallCheck(this, HeaderPageRoutingModule);
      };

      HeaderPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], HeaderPageRoutingModule);
      /***/
    },

    /***/
    "./src/app/header/header.module.ts":
    /*!*****************************************!*\
      !*** ./src/app/header/header.module.ts ***!
      \*****************************************/

    /*! exports provided: HeaderPageModule */

    /***/
    function srcAppHeaderHeaderModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "HeaderPageModule", function () {
        return HeaderPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _header_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./header-routing.module */
      "./src/app/header/header-routing.module.ts");
      /* harmony import */


      var _header_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./header.page */
      "./src/app/header/header.page.ts");

      var HeaderPageModule = function HeaderPageModule() {
        _classCallCheck(this, HeaderPageModule);
      };

      HeaderPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _header_routing_module__WEBPACK_IMPORTED_MODULE_5__["HeaderPageRoutingModule"]],
        declarations: [_header_page__WEBPACK_IMPORTED_MODULE_6__["HeaderPage"]],
        exports: [_header_page__WEBPACK_IMPORTED_MODULE_6__["HeaderPage"]]
      })], HeaderPageModule);
      /***/
    },

    /***/
    "./src/app/header/header.page.scss":
    /*!*****************************************!*\
      !*** ./src/app/header/header.page.scss ***!
      \*****************************************/

    /*! exports provided: default */

    /***/
    function srcAppHeaderHeaderPageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = ".notification-bell-container {\n  position: relative;\n  display: flex;\n  flex-direction: row;\n  align-items: center;\n  justify-content: center;\n}\n.notification-bell-container .text-highlight {\n  position: absolute;\n  right: -3px;\n  font-size: 10px;\n  cursor: pointer;\n  top: -1px;\n  background: red;\n  border-radius: 50%;\n  z-index: 1111;\n  width: 18px;\n  height: 18px;\n  text-align: center;\n  line-height: 19px;\n  color: #ffffff;\n  font-weight: bold;\n}\n.notification-bell-container .bell-icon {\n  font-size: 30px;\n  position: relative;\n}\n.notification-bell-container .bell-icon:after {\n  content: \"\";\n  position: absolute;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvaGVhZGVyL2hlYWRlci5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxrQkFBQTtFQUNBLGFBQUE7RUFDQSxtQkFBQTtFQUNBLG1CQUFBO0VBQ0EsdUJBQUE7QUFDSjtBQUFJO0VBQ0Usa0JBQUE7RUFDQSxXQUFBO0VBQ0EsZUFBQTtFQUNBLGVBQUE7RUFDQSxTQUFBO0VBQ0EsZUFBQTtFQUNBLGtCQUFBO0VBQ0EsYUFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBQ0Esa0JBQUE7RUFDQSxpQkFBQTtFQUNBLGNBQUE7RUFDQSxpQkFBQTtBQUVOO0FBQUk7RUFDSSxlQUFBO0VBQ0Esa0JBQUE7QUFFUjtBQURRO0VBQ0UsV0FBQTtFQUNBLGtCQUFBO0FBR1YiLCJmaWxlIjoic3JjL2FwcC9oZWFkZXIvaGVhZGVyLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5ub3RpZmljYXRpb24tYmVsbC1jb250YWluZXJ7XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgZmxleC1kaXJlY3Rpb246IHJvdztcclxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgIC50ZXh0LWhpZ2hsaWdodHtcclxuICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICByaWdodDogLTNweDtcclxuICAgICAgZm9udC1zaXplOiAxMHB4O1xyXG4gICAgICBjdXJzb3I6IHBvaW50ZXI7XHJcbiAgICAgIHRvcDogLTFweDtcclxuICAgICAgYmFja2dyb3VuZDogcmVkO1xyXG4gICAgICBib3JkZXItcmFkaXVzOiA1MCU7XHJcbiAgICAgIHotaW5kZXg6IDExMTE7XHJcbiAgICAgIHdpZHRoOiAxOHB4O1xyXG4gICAgICBoZWlnaHQ6IDE4cHg7XHJcbiAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICAgbGluZS1oZWlnaHQ6IDE5cHg7XHJcbiAgICAgIGNvbG9yOiAjZmZmZmZmO1xyXG4gICAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgIH1cclxuICAgIC5iZWxsLWljb257XHJcbiAgICAgICAgZm9udC1zaXplOiAzMHB4O1xyXG4gICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgICAgICAmOmFmdGVye1xyXG4gICAgICAgICAgY29udGVudDogJyc7XHJcbiAgICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgXHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgfSJdfQ== */";
      /***/
    },

    /***/
    "./src/app/header/header.page.ts":
    /*!***************************************!*\
      !*** ./src/app/header/header.page.ts ***!
      \***************************************/

    /*! exports provided: HeaderPage */

    /***/
    function srcAppHeaderHeaderPageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "HeaderPage", function () {
        return HeaderPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _global_services_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ../global-services.service */
      "./src/app/global-services.service.ts");
      /* harmony import */


      var _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic-native/geolocation/ngx */
      "./node_modules/@ionic-native/geolocation/__ivy_ngcc__/ngx/index.js");
      /* harmony import */


      var _ionic_native_native_geocoder_ngx__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @ionic-native/native-geocoder/ngx */
      "./node_modules/@ionic-native/native-geocoder/__ivy_ngcc__/ngx/index.js");

      var HeaderPage = /*#__PURE__*/function () {
        function HeaderPage(menu, service, geolocation, nativeGeocoder, navController) {
          var _this = this;

          _classCallCheck(this, HeaderPage);

          this.menu = menu;
          this.service = service;
          this.geolocation = geolocation;
          this.nativeGeocoder = nativeGeocoder;
          this.navController = navController;
          this.notificationUrl = 'getNotificationText'; //=====================get Login Details=====================

          this.loggedDetails = {};
          this.city = localStorage.getItem('location'); // geocoder options

          this.nativeGeocoderOptions = {
            useLocale: true,
            maxResults: 5
          };
          this.userName = localStorage.getItem('username');
          this.subscription = this.service.getMessage().subscribe(function (message) {
            if (message.text == 'startNotificationCountFunction') {
              _this.getnotificationsList();
            }
          });
          this.service.invoke.subscribe(function (res) {
            if (res) {
              _this.cityChange(res);
            }
          });
        }

        _createClass(HeaderPage, [{
          key: "cityChange",
          value: function cityChange(city) {
            if (!this.city) {
              this.city = city;
              localStorage.setItem('location', this.city);
            }
          }
        }, {
          key: "ngOnInit",
          value: function ngOnInit() {
            this.userInfo(); // this.getUserPosition();

            this.service.checkGPSPermission();
          }
        }, {
          key: "openMenu",
          value: function openMenu() {
            // this.menu.enable(true, 'first');
            this.menu.open();
          }
        }, {
          key: "userInfo",
          value: function userInfo() {
            var _this2 = this;

            if (localStorage.getItem('username')) {
              var obj = {
                username: localStorage.getItem('username')
              };
              this.service.post_data("getAlumniOthersInfo", obj).subscribe(function (result) {
                var json = _this2.service.parserToJSON(result);

                var parsedJSON = JSON.parse(json.string);
                _this2.loggedDetails = parsedJSON[0];
              }, function (error) {
                console.log(error);
              });
            }
          }
        }, {
          key: "getUserPosition",
          value: function getUserPosition() {
            var _this3 = this;

            this.options = {
              enableHighAccuracy: false
            };
            this.geolocation.getCurrentPosition(this.options).then(function (pos) {
              _this3.currentPos = pos;
              _this3.latitude = pos.coords.latitude;
              _this3.longitude = pos.coords.longitude;

              _this3.getAddress(_this3.latitude, _this3.longitude);
            }, function (err) {
              console.log("error : " + err.message);
              ;
            });
          } // get address using coordinates

        }, {
          key: "getAddress",
          value: function getAddress(lat, _long) {
            var _this4 = this;

            this.nativeGeocoder.reverseGeocode(lat, _long, this.nativeGeocoderOptions).then(function (res) {
              var address = _this4.pretifyAddress(res[0]);

              var value = address.split(",");
              var count = value.length;
              _this4.city = value[count - 5];
            })["catch"](function (error) {
              alert('Error getting location' + error + JSON.stringify(error));
            });
          } // address

        }, {
          key: "pretifyAddress",
          value: function pretifyAddress(addressObj) {
            var obj = [];
            var address = "";

            for (var key in addressObj) {
              obj.push(addressObj[key]);
            }

            obj.reverse();

            for (var val in obj) {
              if (obj[val].length) address += obj[val] + ', ';
            }

            return address.slice(0, -2);
          }
        }, {
          key: "getnotificationsList",
          value: function getnotificationsList() {
            var _this5 = this;

            var obj = {
              msg_id: 0,
              username: this.userName,
              rows_in_page: 0,
              pageid: 1
            };
            this.service.post_data(this.notificationUrl, obj).subscribe(function (result) {
              var json = _this5.service.parserToJSON(result);

              var list = JSON.parse(json.string);

              if (list) {
                _this5.notificationUnreadCount = 0;

                if (list.Details.length > 0) {
                  list.Details.forEach(function (element, i) {
                    if (!JSON.parse(String(element.read_sts).toLowerCase())) {
                      _this5.notificationUnreadCount++;
                    }
                  });
                }
              }
            });
          }
        }, {
          key: "moveNotificationPage",
          value: function moveNotificationPage() {
            this.navController.navigateForward('/notification');
          }
        }]);

        return HeaderPage;
      }();

      HeaderPage.ctorParameters = function () {
        return [{
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["MenuController"]
        }, {
          type: _global_services_service__WEBPACK_IMPORTED_MODULE_3__["GlobalServicesService"]
        }, {
          type: _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_4__["Geolocation"]
        }, {
          type: _ionic_native_native_geocoder_ngx__WEBPACK_IMPORTED_MODULE_5__["NativeGeocoder"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"]
        }];
      };

      HeaderPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-header',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./header.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/header/header.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./header.page.scss */
        "./src/app/header/header.page.scss"))["default"]]
      })], HeaderPage);
      /***/
    }
  }]);
})();
//# sourceMappingURL=header-header-module-es5.js.map