(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["upload-images-videos-upload-images-videos-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/upload-images-videos/upload-images-videos.page.html":
/*!***********************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/upload-images-videos/upload-images-videos.page.html ***!
  \***********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-header>\r\n\r\n</app-header>\r\n<ion-content>\r\n    <div class=\"inner-div\">\r\n        <ion-grid class=\"pt-3\">\r\n            <ion-row class=\"ion-text-center\">\r\n                <ion-col>\r\n                    <h5 header-text>Upload Files</h5>\r\n                </ion-col>\r\n            </ion-row>\r\n            <ion-row *ngIf=\"!imageUrlPath && type\" class=\"ion-text-center\">\r\n                <ion-col (click)=\"takeSnap()\">\r\n                    <div class=\"drop-zone file-input-container\">\r\n                        <label>\r\n                            <ion-icon name=\"logo-dropbox\"></ion-icon>\r\n                            <h6>Tap To Drop Files Here</h6>\r\n                        </label>\r\n                    </div>\r\n                </ion-col>\r\n            </ion-row>\r\n            <ion-row *ngIf=\"imagePreview && type == 'image'\" class=\"ion-text-center\">\r\n                <ion-col>\r\n                    <!-- {{imagePreview}} -->\r\n                    <img id='preview' [src]=\"imagePreview\" class=\"w-100 img-preview\" />\r\n                    <!-- <img id='preview'  src=\"../../assets/image/images.jpg\" class=\"w-100 img-preview\" /> -->\r\n                </ion-col>\r\n            </ion-row>\r\n            <ion-row *ngIf=\"imagePreview && type == 'video'\" class=\"ion-text-center\">\r\n                <ion-col>\r\n                    <div class=\"video-player\">\r\n                        <video #videoElement id=\"videoElement\" muted defaultMuted controls playsinline\r\n                            webkit-playsinline preload=\"auto\" width='100%' [poster]=\"posterImage\"\r\n                            controlsList=\"nodownload\">\r\n                            <source [src]=\"imagePreview\" />\r\n                            <!-- <source src=\"../../assets/videos/sample.mp4\" type=\"video/mp4\" /> -->\r\n                        </video>\r\n                    </div>\r\n                </ion-col>\r\n            </ion-row>\r\n            <ion-card class=\"ion-margin-vertical\" *ngIf=\"fileName && fileSize\">\r\n                <ion-card-content>\r\n                    <b>File Name :</b>{{fileName}}<br>\r\n                    <b>File Size :</b>{{fileSize+'MB'}}\r\n                </ion-card-content>\r\n            </ion-card>\r\n            <ion-card class=\"ion-margin-vertical\" *ngIf=\"type == 'image'\">\r\n                <ion-card-content>\r\n                    <b>Note:</b> The image resolution should be greater than 1024px X 720px\r\n                </ion-card-content>\r\n            </ion-card>\r\n        </ion-grid>\r\n    </div>\r\n</ion-content>\r\n<ion-toolbar color=\"yellow\" slot=\"bottom\">\r\n    <ion-tabs>\r\n        <ion-tab-bar color=\"yellow\" slot=\"bottom\">\r\n            <ion-tab-button (click)=\"goback()\">\r\n                <ion-icon name=\"caret-back-outline\"></ion-icon>\r\n                <ion-label>Back</ion-label>\r\n            </ion-tab-button>\r\n            <ion-tab-button (click)=\"moveToPreview(location,imageUrlPath,type)\">\r\n                <ion-icon name=\"checkmark-circle-outline\"></ion-icon>\r\n                <ion-label>Next</ion-label>\r\n            </ion-tab-button>\r\n            <ion-tab-button (click)=\"deleteImage(imageUrlPath)\">\r\n                <ion-icon name=\"trash\"></ion-icon>\r\n                <ion-label>Delete</ion-label>\r\n            </ion-tab-button>\r\n        </ion-tab-bar>\r\n    </ion-tabs>\r\n</ion-toolbar>");

/***/ }),

/***/ "./src/app/upload-images-videos/upload-images-videos-routing.module.ts":
/*!*****************************************************************************!*\
  !*** ./src/app/upload-images-videos/upload-images-videos-routing.module.ts ***!
  \*****************************************************************************/
/*! exports provided: UploadImagesVideosPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UploadImagesVideosPageRoutingModule", function() { return UploadImagesVideosPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _upload_images_videos_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./upload-images-videos.page */ "./src/app/upload-images-videos/upload-images-videos.page.ts");




const routes = [
    {
        path: '',
        component: _upload_images_videos_page__WEBPACK_IMPORTED_MODULE_3__["UploadImagesVideosPage"]
    }
];
let UploadImagesVideosPageRoutingModule = class UploadImagesVideosPageRoutingModule {
};
UploadImagesVideosPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], UploadImagesVideosPageRoutingModule);



/***/ }),

/***/ "./src/app/upload-images-videos/upload-images-videos.module.ts":
/*!*********************************************************************!*\
  !*** ./src/app/upload-images-videos/upload-images-videos.module.ts ***!
  \*********************************************************************/
/*! exports provided: UploadImagesVideosPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UploadImagesVideosPageModule", function() { return UploadImagesVideosPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _upload_images_videos_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./upload-images-videos-routing.module */ "./src/app/upload-images-videos/upload-images-videos-routing.module.ts");
/* harmony import */ var _upload_images_videos_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./upload-images-videos.page */ "./src/app/upload-images-videos/upload-images-videos.page.ts");
/* harmony import */ var ng2_file_upload__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ng2-file-upload */ "./node_modules/ng2-file-upload/__ivy_ngcc__/fesm2015/ng2-file-upload.js");
/* harmony import */ var _header_header_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../header/header.module */ "./src/app/header/header.module.ts");









let UploadImagesVideosPageModule = class UploadImagesVideosPageModule {
};
UploadImagesVideosPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _upload_images_videos_routing_module__WEBPACK_IMPORTED_MODULE_5__["UploadImagesVideosPageRoutingModule"],
            ng2_file_upload__WEBPACK_IMPORTED_MODULE_7__["FileUploadModule"], _header_header_module__WEBPACK_IMPORTED_MODULE_8__["HeaderPageModule"]
        ],
        declarations: [_upload_images_videos_page__WEBPACK_IMPORTED_MODULE_6__["UploadImagesVideosPage"]]
    })
], UploadImagesVideosPageModule);



/***/ }),

/***/ "./src/app/upload-images-videos/upload-images-videos.page.scss":
/*!*********************************************************************!*\
  !*** ./src/app/upload-images-videos/upload-images-videos.page.scss ***!
  \*********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".drop-zone {\n  background: transparent;\n  border: dotted 3px var(--ion-color-yellow);\n  height: calc(100vh - 425px);\n  display: flex;\n  align-items: center;\n  justify-content: center;\n  margin: 20px 0;\n  color: var(--ion-color-white);\n  flex-flow: column;\n  align-items: center;\n}\n.drop-zone ion-icon {\n  font: 75pt bold arial;\n}\n.file-input-container {\n  align-items: center;\n}\n.file-input-container input[type=file] {\n  display: none;\n}\n.file-input-container label {\n  border: 0;\n  padding: 6px 12px;\n  cursor: pointer;\n}\n.nv-file-over {\n  border: dotted 3px red;\n}\n[font-small] {\n  font-weight: bold !important;\n  --border-radius: 0px;\n}\n[margin-1] {\n  margin: 2.25rem;\n}\n/* DOES NOT WORK - not specific enough */\n.popover-content {\n  background: #222;\n}\n/* Works - pass \"my-custom-class\" in cssClass to increase specificity */\n.my-custom-class .popover-content {\n  background: #222;\n}\n.video-player {\n  background: #1f1e1d;\n  width: 100%;\n  height: calc(100vh - 350px);\n}\n.video-player video {\n  width: 100%;\n  height: 100%;\n}\n.img-preview {\n  height: calc(100vh - 350px);\n  -o-object-fit: scale-down;\n     object-fit: scale-down;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvdXBsb2FkLWltYWdlcy12aWRlb3MvdXBsb2FkLWltYWdlcy12aWRlb3MucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQTBCQTtFQUNJLHVCQUFBO0VBQ0EsMENBQUE7RUFDQSwyQkFBQTtFQUNBLGFBQUE7RUFDQSxtQkFBQTtFQUNBLHVCQUFBO0VBQ0EsY0FBQTtFQUNBLDZCQUFBO0VBQ0EsaUJBQUE7RUFDQSxtQkFBQTtBQXpCSjtBQTBCSTtFQUNJLHFCQUFBO0FBeEJSO0FBNEJBO0VBQ0ksbUJBQUE7QUF6Qko7QUEyQkk7RUFDSSxhQUFBO0FBekJSO0FBMkJJO0VBQ0ksU0FBQTtFQUNBLGlCQUFBO0VBQ0EsZUFBQTtBQXpCUjtBQTZCQTtFQUNJLHNCQUFBO0FBMUJKO0FBNkJBO0VBRUksNEJBQUE7RUFDQSxvQkFBQTtBQTNCSjtBQThCQTtFQUNJLGVBQUE7QUEzQko7QUErQkEsd0NBQUE7QUFFQTtFQUNJLGdCQUFBO0FBN0JKO0FBaUNBLHVFQUFBO0FBRUE7RUFDSSxnQkFBQTtBQS9CSjtBQWtDQTtFQUNJLG1CQUFBO0VBQ0EsV0FBQTtFQUNBLDJCQUFBO0FBL0JKO0FBZ0NJO0VBQ0ksV0FBQTtFQUNBLFlBQUE7QUE5QlI7QUFpQ0E7RUFDSSwyQkFBQTtFQUNBLHlCQUFBO0tBQUEsc0JBQUE7QUE5QkoiLCJmaWxlIjoic3JjL2FwcC91cGxvYWQtaW1hZ2VzLXZpZGVvcy91cGxvYWQtaW1hZ2VzLXZpZGVvcy5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIvLyAuYXJlYSB7XHJcbi8vICAgICB3aWR0aDogMTAwJTtcclxuLy8gICAgIHBhZGRpbmc6IDE1cHg7XHJcbi8vICAgICBoZWlnaHQ6IDUwJTtcclxuLy8gICAgIC8vIG1hcmdpbjogMTVweDtcclxuLy8gICAgIC8vIGJvcmRlcjogMXB4IHNvbGlkICMzMzM7XHJcbi8vICAgICBiYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcclxuLy8gfVxyXG4vLyAjZHJvcFpvbmUge1xyXG4vLyAgICAgYm9yZGVyOiAycHggZGFzaGVkIHZhcigtLWlvbi1jb2xvci15ZWxsb3cpO1xyXG4vLyAgICAgLXdlYmtpdC1ib3JkZXItcmFkaXVzOiA1cHg7XHJcbi8vICAgICBib3JkZXItcmFkaXVzOiA1cHg7XHJcbi8vICAgICBwYWRkaW5nOiA1MHB4O1xyXG4vLyAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4vLyAgICAgZm9udDogMTVwdCBib2xkIGFyaWFsO1xyXG4vLyAgICAgY29sb3I6IHZhcigtLWlvbi1jb2xvci13aGl0ZSk7XHJcbi8vICAgICBkaXNwbGF5OiBmbGV4O1xyXG4vLyAgICAgZmxleC1mbG93OiBjb2x1bW47XHJcbi8vICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4vLyAgICAgaW9uLWljb24ge1xyXG4vLyAgICAgICAgIGZvbnQ6IDc1cHQgYm9sZCBhcmlhbDtcclxuLy8gICAgIH1cclxuLy8gfVxyXG4vLyAuZHJvcC1maWxlLW92ZXIge1xyXG4vLyAgICAgYmFja2dyb3VuZDogdHJhbnNwYXJlbnQ7XHJcbi8vIH1cclxuLmRyb3Atem9uZSB7XHJcbiAgICBiYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcclxuICAgIGJvcmRlcjogZG90dGVkIDNweCB2YXIoLS1pb24tY29sb3IteWVsbG93KTtcclxuICAgIGhlaWdodDogY2FsYygxMDB2aCAtIDQyNXB4KTtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICBtYXJnaW46IDIwcHggMDtcclxuICAgIGNvbG9yOiB2YXIoLS1pb24tY29sb3Itd2hpdGUpO1xyXG4gICAgZmxleC1mbG93OiBjb2x1bW47XHJcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgaW9uLWljb24ge1xyXG4gICAgICAgIGZvbnQ6IDc1cHQgYm9sZCBhcmlhbDtcclxuICAgIH1cclxufVxyXG5cclxuLmZpbGUtaW5wdXQtY29udGFpbmVyIHtcclxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICAvLyBkaXNwbGF5OiBpbmxpbmUtZmxleDtcclxuICAgIGlucHV0W3R5cGU9XCJmaWxlXCJdIHtcclxuICAgICAgICBkaXNwbGF5OiBub25lO1xyXG4gICAgfVxyXG4gICAgbGFiZWwge1xyXG4gICAgICAgIGJvcmRlcjogMDtcclxuICAgICAgICBwYWRkaW5nOiA2cHggMTJweDtcclxuICAgICAgICBjdXJzb3I6IHBvaW50ZXI7XHJcbiAgICB9XHJcbn1cclxuXHJcbi5udi1maWxlLW92ZXIge1xyXG4gICAgYm9yZGVyOiBkb3R0ZWQgM3B4IHJlZDtcclxufVxyXG5cclxuW2ZvbnQtc21hbGxdIHtcclxuICAgIC8vIGZvbnQtc2l6ZTogMTJweCFpbXBvcnRhbnQ7XHJcbiAgICBmb250LXdlaWdodDogYm9sZCAhaW1wb3J0YW50O1xyXG4gICAgLS1ib3JkZXItcmFkaXVzOiAwcHg7XHJcbn1cclxuXHJcblttYXJnaW4tMV0ge1xyXG4gICAgbWFyZ2luOiAyLjI1cmVtO1xyXG59XHJcblxyXG5cclxuLyogRE9FUyBOT1QgV09SSyAtIG5vdCBzcGVjaWZpYyBlbm91Z2ggKi9cclxuXHJcbi5wb3BvdmVyLWNvbnRlbnQge1xyXG4gICAgYmFja2dyb3VuZDogIzIyMjtcclxufVxyXG5cclxuXHJcbi8qIFdvcmtzIC0gcGFzcyBcIm15LWN1c3RvbS1jbGFzc1wiIGluIGNzc0NsYXNzIHRvIGluY3JlYXNlIHNwZWNpZmljaXR5ICovXHJcblxyXG4ubXktY3VzdG9tLWNsYXNzIC5wb3BvdmVyLWNvbnRlbnQge1xyXG4gICAgYmFja2dyb3VuZDogIzIyMjtcclxufVxyXG5cclxuLnZpZGVvLXBsYXllciB7XHJcbiAgICBiYWNrZ3JvdW5kOiAjMWYxZTFkO1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBoZWlnaHQ6IGNhbGMoMTAwdmggLSAzNTBweCk7XHJcbiAgICB2aWRlbyB7XHJcbiAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgfVxyXG59XHJcbi5pbWctcHJldmlld3tcclxuICAgIGhlaWdodDogY2FsYygxMDB2aCAtIDM1MHB4KTtcclxuICAgIG9iamVjdC1maXQ6IHNjYWxlLWRvd247XHJcbn0iXX0= */");

/***/ }),

/***/ "./src/app/upload-images-videos/upload-images-videos.page.ts":
/*!*******************************************************************!*\
  !*** ./src/app/upload-images-videos/upload-images-videos.page.ts ***!
  \*******************************************************************/
/*! exports provided: UploadImagesVideosPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UploadImagesVideosPage", function() { return UploadImagesVideosPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _global_services_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../global-services.service */ "./src/app/global-services.service.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _modal_preview_preview_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../modal/preview/preview.page */ "./src/app/modal/preview/preview.page.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic-native/camera/ngx */ "./node_modules/@ionic-native/camera/__ivy_ngcc__/ngx/index.js");
/* harmony import */ var _ionic_native_ionic_webview_ngx__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @ionic-native/ionic-webview/ngx */ "./node_modules/@ionic-native/ionic-webview/__ivy_ngcc__/ngx/index.js");
/* harmony import */ var _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @ionic-native/geolocation/ngx */ "./node_modules/@ionic-native/geolocation/__ivy_ngcc__/ngx/index.js");
/* harmony import */ var _ionic_native_native_geocoder_ngx__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @ionic-native/native-geocoder/ngx */ "./node_modules/@ionic-native/native-geocoder/__ivy_ngcc__/ngx/index.js");
/* harmony import */ var _ionic_native_file_ngx__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @ionic-native/file/ngx */ "./node_modules/@ionic-native/file/__ivy_ngcc__/ngx/index.js");
/* harmony import */ var _ionic_native_video_editor__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @ionic-native/video-editor */ "./node_modules/@ionic-native/video-editor/index.js");













// declare var CordovaExif: any;
let UploadImagesVideosPage = class UploadImagesVideosPage {
    constructor(service, menu, alertController, navCtrl, popoverController, router, route, camera, webview, geolocation, nativeGeocoder, file, platform, datepipe) {
        this.service = service;
        this.menu = menu;
        this.alertController = alertController;
        this.navCtrl = navCtrl;
        this.popoverController = popoverController;
        this.router = router;
        this.route = route;
        this.camera = camera;
        this.webview = webview;
        this.geolocation = geolocation;
        this.nativeGeocoder = nativeGeocoder;
        this.file = file;
        this.platform = platform;
        this.datepipe = datepipe;
        this.page = "";
        this.fileInfo = {};
        this.fileName = '';
        this.fileSize = '';
        this.imageMaxWidth = 720;
        this.imageMaxheight = 1024;
        this.isLoading = false;
        this.posterImage = 'assets/images/video_thumbnail.jpg';
        // geocoder options
        this.nativeGeocoderOptions = {
            useLocale: true,
            maxResults: 5
        };
    }
    ngOnInit() {
        //=====================Get params value=====================
        this.route.queryParams.subscribe(params => {
            this.type = params['type'];
            this.page = params['page'] ? params['page'] : "";
        });
        this.getUserPosition();
    }
    ionViewWillEnter() {
        this.service.sendMessage('startNotificationCountFunction');
    }
    ionViewWillLeave() {
        if (this.posterImageUrl) {
            this.file.resolveLocalFilesystemUrl(this.posterImageUrl).then((fileEntry) => {
                fileEntry.remove(() => {
                }, (error) => {
                    console.log(error);
                });
            }).catch((e) => {
                console.log(e);
            });
        }
    }
    goback() {
        this.type = undefined;
        this.imagePreview = undefined;
        this.imageUrlPath = undefined;
        this.router.navigate(['/home']);
    }
    //capture image with location
    takeSnap() {
        if (this.type == 'image') {
            const cameraoptions = {
                destinationType: this.camera.DestinationType.FILE_URI,
                sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
                encodingType: this.camera.EncodingType.JPEG,
                mediaType: this.camera.MediaType.PICTURE,
                correctOrientation: true,
                quality: 50,
            };
            this.camera.getPicture(cameraoptions).then((imageData) => {
                //   if(CordovaExif){
                //   CordovaExif.readData(imageData, (res) => {
                //     console.log('CordovaExif', res)
                //     this.fileInfo.resolution = res.ImageWidth + 'X' + res.ImageHeight + 'px';
                //     this.fileInfo.device = res.Make;
                //     this.fileInfo.model = res.Model;
                //     this.fileInfo.flash = res.Flash == 'Flash fired, compulsory flash mode' ? 'Flash Fired' : 'No Flash';
                //     this.fileInfo.iso = res.ISOSpeedRatings;
                //     this.fileInfo.whiteBalance = res.WhiteBalance;
                //     this.fileInfo.orientation = res.Orientation == 1 ? 'Portrait' : 'Landscape';
                //     this.fileInfo.latitude = res.GPSLatitude ? res.GPSLatitude : null;
                //     this.fileInfo.longitude = res.GPSLongitude ? res.GPSLongitude : null;
                //     // this.fileInfo.time = res.DateTime;
                //     this.fileInfo.ImageHeight = res.ImageHeight;
                //     this.fileInfo.ImageWidth = res.ImageWidth;
                //   })
                // }
                if (imageData.indexOf("file://") !== -1) {
                    this.imageUrlPath = imageData;
                    this.imagePreview = this.service.checkDomSanitizerForIOS(this.webview.convertFileSrc(imageData));
                }
                else if (imageData.indexOf("content://") !== -1) {
                    this.imageUrlPath = imageData;
                    this.imagePreview = this.service.checkDomSanitizerForIOS(this.webview.convertFileSrc(imageData));
                }
                else {
                    this.imageUrlPath = "file://" + imageData;
                    this.imagePreview = this.service.checkDomSanitizerForIOS(this.webview.convertFileSrc("file://" + imageData));
                }
                if (this.imageUrlPath) {
                    this.file.resolveLocalFilesystemUrl(this.imageUrlPath).then(fileEntry => {
                        fileEntry.getMetadata((metadata) => {
                            let fileSize = metadata.size / 1024 / 1024;
                            this.fileName = fileEntry.name;
                            this.fileSize = Math.round(fileSize * 100) / 100;
                            this.fileInfo.fileName = fileEntry.name;
                            this.fileInfo.time = this.datepipe.transform(new Date(metadata.modificationTime), 'yyyy/MM/dd hh:mm:ss a');
                            this.fileInfo.size = Math.round(fileSize * 100) / 100 + 'MB';
                            localStorage.setItem('fileSize', JSON.stringify(fileSize));
                        });
                    });
                }
            }, (err) => {
                // Handle error
                console.log(err);
            });
        }
        else {
            const cameraoptions2 = {
                destinationType: this.camera.DestinationType.FILE_URI,
                mediaType: this.camera.MediaType.VIDEO,
                sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
            };
            this.camera.getPicture(cameraoptions2).then((videoData) => {
                let GetVideoInfoOptions = { fileUri: 'file://' + videoData };
                _ionic_native_video_editor__WEBPACK_IMPORTED_MODULE_12__["VideoEditor"].getVideoInfo(GetVideoInfoOptions).then((data) => {
                    this.fileInfo.resolution = data.width + 'x' + data.height + 'px';
                    this.fileInfo.duration = data.duration;
                    this.fileInfo.bitrate = data.bitrate;
                    this.fileInfo.width = data.width;
                    this.fileInfo.height = data.height;
                    this.fileInfo.orientation = data.orientation;
                }).catch((e) => {
                    console.log(e);
                    alert(e);
                });
                let splitPath = videoData.split('/');
                let imageNamewithext = splitPath[splitPath.length - 1];
                let imageName = imageNamewithext.split('.')[0];
                let option = { fileUri: videoData, width: this.fileInfo.width, height: this.fileInfo.height, atTime: 0, outputFileName: imageName, quality: 100 };
                // let option: CreateThumbnailOptions = { fileUri: videoData, width: 120, height: 200, atTime: 0, outputFileName: imageName, quality: 100 };
                _ionic_native_video_editor__WEBPACK_IMPORTED_MODULE_12__["VideoEditor"].createThumbnail(option).then(res => {
                    this.posterImageUrl = "file://" + res;
                    this.convertFileUritoBase64("file://" + res);
                    this.posterImage = this.service.checkDomSanitizerForIOS(this.webview.convertFileSrc(res));
                }, (error) => {
                    console.log(error);
                    alert('finally Err==line181' + error);
                });
                if (videoData.indexOf("file://") !== -1 || videoData.indexOf("content://") !== -1) {
                    this.imageUrlPath = videoData;
                    this.imagePreview = this.service.checkDomSanitizerForIOS(this.webview.convertFileSrc(videoData));
                }
                else {
                    this.imageUrlPath = "file://" + videoData;
                    this.imagePreview = this.service.checkDomSanitizerForIOS(this.webview.convertFileSrc("file://" + videoData));
                }
                if (this.imageUrlPath) {
                    this.file.resolveLocalFilesystemUrl(this.imageUrlPath).then(fileEntry => {
                        fileEntry.getMetadata((metadata) => {
                            let fileMetaSize = metadata.size / 1024 / 1024;
                            this.fileName = fileEntry.name;
                            this.fileSize = Math.round(fileMetaSize * 100) / 100;
                            this.fileInfo.fileName = fileEntry.name;
                            this.fileInfo.time = this.datepipe.transform(new Date(metadata.modificationTime), 'yyyy/MM/dd hh:mm:ss a');
                            this.fileInfo.size = Math.round(fileMetaSize * 100) / 100 + 'MB';
                            localStorage.setItem('videoFileSize', JSON.stringify(fileMetaSize));
                        });
                    });
                }
            }, (err) => {
                // Handle error
                console.log(err);
            });
        }
    }
    convertFileUritoBase64(ImagePath) {
        this.isLoading = true;
        var copyPath = ImagePath;
        var splitPath = copyPath.split('/');
        var imageName = splitPath[splitPath.length - 1];
        var filePath = ImagePath.split(imageName)[0];
        this.file.readAsDataURL(filePath, imageName).then(base64 => {
            // if (localStorage.getItem('videoThumbnailPosterImage')) {
            //   localStorage.removeItem('videoThumbnailPosterImage');
            // }
            localStorage.setItem('videoThumbnailPosterImage', JSON.stringify(base64));
        }, error => {
            console.log("CROP ERROR -> " + JSON.stringify(error));
        });
    }
    deleteImage(path) {
        if (path) {
            this.imageUrlPath = "";
            this.imagePreview = "";
            this.fileName = '';
            this.fileSize = '';
            if (localStorage.getItem('videoFileSize')) {
                localStorage.removeItem('videoFileSize');
            }
            if (localStorage.getItem('fileSize')) {
                localStorage.removeItem('fileSize');
            }
        }
        else {
            this.presentAlert();
        }
    }
    moveToPreview(location, tempPath, type) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            let datauri;
            datauri = (tempPath.indexOf('?') !== -1) ? tempPath.split('?')[0] : tempPath;
            if (localStorage.getItem('originalImg') != undefined) {
                localStorage.removeItem('originalImg');
            }
            if (localStorage.getItem('editedFilters') != undefined) {
                localStorage.removeItem('editedFilters');
            }
            let allowed_size = type == "image" ? 8 : 15;
            if (tempPath) {
                if (type == 'video') {
                    if (localStorage.getItem('videoFileSize')) {
                        if (JSON.parse(localStorage.getItem('videoFileSize')) <= allowed_size) {
                            // localStorage.removeItem('videoFileSize');
                            this.router.navigate(['/category-selection', { 'fileinfo': JSON.stringify(this.fileInfo), 'location': location, 'latitude': this.latitude, 'longitude': this.longitude, 'mediaPath': datauri, 'type': this.type, 'page': 'upload' }]);
                        }
                        else {
                            // localStorage.removeItem('videoFileSize');
                            this.presentAlert4();
                        }
                    }
                }
                else {
                    // console.log("width" + this.fileInfo.ImageWidth + "width:" + this.fileInfo.ImageHeight); 
                    // if (this.fileInfo.ImageWidth >= this.imageMaxWidth && this.fileInfo.ImageHeight >= this.imageMaxheight) {
                    if (localStorage.getItem('fileSize')) {
                        if (JSON.parse(localStorage.getItem('fileSize')) <= allowed_size) {
                            this.service.changeMessage(datauri);
                            this.router.navigate(['/preview-filter', { 'fileinfo': JSON.stringify(this.fileInfo), 'location': location, 'latitude': this.latitude, 'longitude': this.longitude, 'tempPath': datauri, 'type': type, 'page': 'upload' }]);
                        }
                        else {
                            // localStorage.removeItem('fileSize');
                            this.presentAlert3();
                        }
                    }
                    // } else {
                    //   this.presentAlert2();
                    // }
                }
            }
            else {
                this.presentAlert();
            }
        });
    }
    presentPreviewPopover(ev) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const popover = yield this.popoverController.create({
                component: _modal_preview_preview_page__WEBPACK_IMPORTED_MODULE_5__["PreviewPage"],
                cssClass: 'my-custom-class',
                event: ev,
                backdropDismiss: false,
                translucent: true
            });
            return yield popover.present();
        });
    }
    presentAlert() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const alert = yield this.alertController.create({
                cssClass: 'my-custom-class',
                header: 'Error',
                message: 'Please Select the File',
                backdropDismiss: false,
                buttons: [
                    {
                        text: 'OK',
                        cssClass: 'warning',
                    }
                ]
            });
            yield alert.present();
        });
    }
    presentAlert2() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const alert = yield this.alertController.create({
                cssClass: 'my-custom-class',
                header: 'Note',
                subHeader: 'Image resolution size invalid',
                message: 'please select the image resolution 720px X 1024px',
                backdropDismiss: false,
                buttons: [
                    {
                        text: 'OK',
                        cssClass: 'warning',
                    }
                ]
            });
            yield alert.present();
        });
    }
    presentAlert3() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const alert = yield this.alertController.create({
                cssClass: 'my-custom-class',
                header: 'Note',
                subHeader: 'Image size Invalid',
                message: 'file too big, please select less than 8 MB',
                buttons: ['OK']
            });
            yield alert.present();
        });
    }
    presentAlert4() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const alert = yield this.alertController.create({
                cssClass: 'my-custom-class',
                header: 'Note',
                subHeader: 'Video size Invalid',
                message: 'file too big, please select less than 15 MB',
                buttons: ['OK']
            });
            yield alert.present();
        });
    }
    presentAlert5(type, data) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const alert = yield this.alertController.create({
                cssClass: 'my-custom-class',
                header: 'Error',
                subHeader: type,
                message: data,
                buttons: ['OK']
            });
            yield alert.present();
        });
    }
    openMenu() {
        this.menu.open();
    }
    getUserPosition() {
        this.options = {
            enableHighAccuracy: false
        };
        this.geolocation.getCurrentPosition(this.options).then((pos) => {
            this.currentPos = pos;
            this.latitude = pos.coords.latitude;
            this.longitude = pos.coords.longitude;
            this.getAddress(this.latitude, this.longitude);
        }, (err) => {
            console.log("error : " + err.message);
            ;
        });
    }
    // get address using coordinates
    getAddress(lat, long) {
        this.nativeGeocoder.reverseGeocode(lat, long, this.nativeGeocoderOptions)
            .then((res) => {
            var address = this.pretifyAddress(res[0]);
            this.location = this.pretifyAddress(res[0]);
            var value = address.split(",");
            var count = value.length;
            // country = value[count - 1];
            // state = value[count - 2];
            this.city = value[count - 5];
            // alert(address);
            // alert(this.city);
        })
            .catch((error) => {
            alert('Error getting location' + error + JSON.stringify(error));
        });
    }
    // address
    pretifyAddress(addressObj) {
        let obj = [];
        let address = "";
        for (let key in addressObj) {
            obj.push(addressObj[key]);
        }
        obj.reverse();
        for (let val in obj) {
            if (obj[val].length)
                address += obj[val] + ', ';
        }
        return address.slice(0, -2);
    }
};
UploadImagesVideosPage.ctorParameters = () => [
    { type: _global_services_service__WEBPACK_IMPORTED_MODULE_3__["GlobalServicesService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["MenuController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["AlertController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["NavController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["PopoverController"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_6__["ActivatedRoute"] },
    { type: _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_7__["Camera"] },
    { type: _ionic_native_ionic_webview_ngx__WEBPACK_IMPORTED_MODULE_8__["WebView"] },
    { type: _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_9__["Geolocation"] },
    { type: _ionic_native_native_geocoder_ngx__WEBPACK_IMPORTED_MODULE_10__["NativeGeocoder"] },
    { type: _ionic_native_file_ngx__WEBPACK_IMPORTED_MODULE_11__["File"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["Platform"] },
    { type: _angular_common__WEBPACK_IMPORTED_MODULE_2__["DatePipe"] }
];
UploadImagesVideosPage.propDecorators = {
    myVideo: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"], args: ['videoElement', { static: false },] }]
};
UploadImagesVideosPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-upload-images-videos',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./upload-images-videos.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/upload-images-videos/upload-images-videos.page.html")).default,
        providers: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["DatePipe"]],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./upload-images-videos.page.scss */ "./src/app/upload-images-videos/upload-images-videos.page.scss")).default]
    })
], UploadImagesVideosPage);



/***/ })

}]);
//# sourceMappingURL=upload-images-videos-upload-images-videos-module-es2015.js.map